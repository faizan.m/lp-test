#!/bin/bash

check_docker_service_status(){
  echo "Checking Docker service status..."
  if [ $(systemctl show --property ActiveState docker) = "ActiveState=active" ];
    then echo "Docker Service Running...";
    return 1
 else
   echo "Docker service not Running...";
   return 0
 fi
}

start_docker_service(){
  echo "Starting Docker Service"
  systemctl start docker
  check_docker_service_status
  if [ $? -eq 0 ];
    then echo "Error starting Docker Service"
    # Exit gracefully without trying to run further commands
    exit 1
  else
    echo "Docker Service Started Sucessfully"
  fi
}

check_all_containers_are_running(){
  declare -a containers=("server_postgres"
                  "server_backend_app"
                  "server_nginx"
                  "server_redis"
                  "server_celery")
  for container in "${containers[@]}"
  do
    docker ps --filter "name=server_*" --format "{{.Names}}"|grep -q "$container"
    if [ $? -eq 0 ];
      then
        running="$(docker inspect -f {{.State.Running}} $container)"
        if [ $running = "true" ];
          then echo "$container Running"
        else
          echo "Unable to start container $container . Exiting.."
          sleep 1s
          exit 1
        fi
    else
      echo "Unable to start container $container . Exiting.."
      sleep 1s
      exit 1
    fi

  done
}
# Check Docker Service Status
check_docker_service_status

# start Docker Service if not running
if [ $? -eq 0 ];
  then start_docker_service
fi

# # Stop running containers
echo "Stopping running containers...."
docker-compose down
if [ $? -ne 0 ]; then
    echo "The command we just ran seems to have failed. Exiting..."
    # Exit gracefully without trying to run further commands
    exit 1
fi
# # Start containers
echo "Starting containers...."
docker-compose up -d
sleep 5s

# Check running conatiners
check_all_containers_are_running
echo "Application started Sucessfully.."
