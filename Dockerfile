FROM node:12.14.1 as builder

RUN mkdir -p /app /app/src /app/static

COPY client/ /app/
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
ARG ENV_VAR="dev"

RUN npm install

RUN if [ "${ENV_VAR}" = "dev" ] ; then \
        echo "Dev ENV command is running" &&  \
        npm run build-dev;  \
    else \
        echo "Prod ENV command is running" && \
        npm run build; \
    fi

FROM nginx:1.13.3

RUN rm -rf /usr/share/nginx/html/*

COPY config/nginx/conf.d/prod.conf /etc/nginx/conf.default
COPY --from=builder /app/dist/app /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
