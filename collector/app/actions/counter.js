// @flow
import type { GetState, Dispatch } from '../reducers/types';
// eslint-disable-next-line import/no-cycle
import { CALL_API } from '../middleware/ApiMiddleware';

export const IS_FETCHING = 'IS_FETCHING';
export const FETCHING_COMPLETE = 'FETCHING_COMPLETE';
export const ERROR = 'ERROR';

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

export function increment() {
  return {
    type: INCREMENT_COUNTER
  };
}

export function decrement() {
  return {
    type: DECREMENT_COUNTER
  };
}

export function incrementIfOdd() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();

    if (counter % 2 === 0) {
      return;
    }

    dispatch(increment());
  };
}

export function incrementAsync(delay: number = 1000) {
  return (dispatch: Dispatch) => {
    setTimeout(() => {
      dispatch(increment());
    }, delay);
  };
}

export const getData = () => ({
  [CALL_API]: {
    endpoint: `https://jsonplaceholder.typicode.com/todos/1`,
    method: 'get',
    authenticated: false,
    types: [FETCH_DATA_REQUEST, FETCH_DATA_SUCCESS, FETCH_DATA_FAILURE]
  }
});
