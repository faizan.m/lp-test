import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type stateType = {
  +counter: number,
  appState: any,
};

export type Action = {
  +type: string
};

export type GetState = () => IAppReduxStore;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;

export interface IAppReduxStore {
  +counter: number,
  appState: IAppState
}

export interface IAppState {
  data: any;
  idFetching: Boolean;
}