import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes.json';
import App from './containers/App';
import HomePage from './containers/HomePage';
import TestPage from './containers/TestPage';
import AuthenticationPage from './containers/AuthenticationPage';
import IntegrationsPage from './containers/IntegrationsPage';
import ServicesPage from './containers/servicesPage';
import CounterPage from './containers/CounterPage';
import ServiceLoginCheck from './containers/ServiceLoginCheckPage';

export default () => (
  <App>
    <Switch>
    <Route path={routes.INTEGRATIONS} component={IntegrationsPage} />
    <Route path={routes.SERVICES} component={ServicesPage} />
    <Route path={routes.TEST} component={TestPage} />
    <Route path={routes.COUNTER} component={CounterPage} />
    <Route path={routes.HOME} component={HomePage} />
    <Route path={routes.AUTHENTICATION} component={AuthenticationPage} />
    <Route path={routes.SERVICELOGIN} component={ServiceLoginCheck} />
    </Switch>
  </App>
);
