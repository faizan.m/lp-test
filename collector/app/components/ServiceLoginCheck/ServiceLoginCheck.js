// @flow
import React, { Component } from 'react';
import { Formik } from 'formik';
import { withRouter } from 'react-router';
import routes from '../../constants/routes.json';
import styles from './ServiceLoginCheck.css';
import LocalStorageJSON from '../../../backend/db';
import inputCredentials from '../../../backend/powershell-scripts/takeCredentials';
import checkAccountLogonAsServicePermission from '../../../backend/powershell-scripts/check-log-on-as-service-rights';
import changeServiceAccount from '../../../backend/powershell-scripts/changeServiceAccount';
import grantLogAsServicePermission from '../../../backend/powershell-scripts/grant-logon-as-service-pem';

type Props = {};
type State = {};
const storage = new LocalStorageJSON();

class ServiceLoginCheck extends Component<Props, State> {
  props: Props;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }
  getEmptyState = () => ({
    validationMessage: '',
    username: '',
    password: '',
    manualSetting: false,
    error: ''
  });

  componentDidMount() {
    const token = storage.tokenExists();
    if (token) {
      // eslint-disable-next-line react/destructuring-assignment,react/prop-types
      this.props.history.push(routes.SERVICES);
    }
  }

  checkCredentials = () => {
    this.setState({ error: '' });
    inputCredentials().then(result => {
      if (result.verified) {
        console.info('verified', result.verified);
        this.setState({ username: result.username, password: result.password });
        this.checkPermission(result.username);
      } else {
        // setTimeout(() => {
        //   this.checkCredentials();
        // }, 2000);
        this.setState({ error: `Something went wrong, Please try again.` });
      }
    });
  };
  checkPermission = (username, first = true) => {
    checkAccountLogonAsServicePermission(username).then(result => {
      if (result.havePermission) {
        this.changeServiceAccount();
      } else {
        grantLogAsServicePermission(username);
        if (first) {
          this.checkPermission(username, false);
        } else {
          this.setState({ 
            error: `Auto permission upgrade is not working. Please follow above steps for manual upgrade.` ,
            manualSetting: true });
        }
      }
    });
  };

  changeServiceAccount = () => {
    changeServiceAccount(
      'acelacollectorservice.exe',
      this.state.username,
      this.state.password
    ).then(result => {
      if (result.success) {
        this.props.history.push(routes.AUTHENTICATION);
      }
    });
  };
  onCLickCounter = () => {
    // eslint-disable-next-line react/destructuring-assignment,react/prop-types
    this.props.history.push('/test');
  };

  render() {
    return (
      <div
        className={
          this.state.manualSetting
            ? `${styles.containerFull}`
            : styles.container
        }
        data-tid="container"
      >
        <h2>Acela Collector</h2>
        {this.state.error && (
          <div className={styles.errorManual}>{this.state.error}</div>
        )}

        {this.state.manualSetting === false && (
          <div className={styles.logOnService}>
            <p className={styles.subHeading}>Service Account Setup</p>
            <div className={styles.line1}>
              <ul className={styles.UlMessage}>
                <li className={styles.LiMessage}>
                  Please specify an account with <b>"Administrator"</b> previleges on
                  the server you intend to monitor. 
                </li>
                <li className={styles.LiMessage}>
                Domain Admin/Local Administrator recommended.
                </li>
                <li className={styles.LiMessage}>
                  Service will be installed that run as this user account.
                </li>
                {/* <li>
                  Windows will require permission to run this application as
                  windows service.
                </li> */}
              </ul>
            </div>
            <div className={styles.line2}>Please</div>
            <button
              className={`${styles.link} link`}
              type="submit"
              onClick={e => this.checkCredentials()}
            >
              click here
            </button>{' '}
            to complete this setup.
            {/* <div className={styles.goManualSetup}>
              Not working then please try manual setup :{' '}
              <button
                className={`${styles.manualLink} link`}
                type="submit"
                onClick={e =>
                  this.setState({ manualSetting: !this.state.manualSetting })
                }
              >
                click here
              </button>{' '}
            </div> */}
          </div>
        )}
        {this.state.manualSetting === true && (
          <div className={styles.logOnService}>
            <div className="back-button" title="back">
              <a
                onClick={e =>
                  this.setState({ manualSetting: !this.state.manualSetting })
                }
              >
                {' '}
                Back
              </a>
            </div>
            <p className={styles.subHeading}>Login as a service</p>
            <div className={styles.line1}>
              To update the local security policy:
              <ul>
                <li>
                  1. Log on to the Collector host as a Local Administrator.
                </li>
                <li>
                  2. From CMD, PowerShell, or Run launch <b>secpol.msc</b>.
                </li>
                <li>
                  3. Expand “Local Policy” and click “User Rights Assignment”.
                </li>
                <li>
                  4. Right-click “Log on as a service” and select “Properties”.
                </li>
                <li>
                  5. Click “Add User or Group” to add the account which is to
                  run the Collector services. In our case, it's a Local System
                  account.
                </li>
                <li>
                  6. In the “Select Users or Groups” dialog, find the user
                  account and click “OK”.
                </li>
                <li>
                  7. In the “Log on as a service” properties window, click “OK”
                  to save your changes.
                </li>
              </ul>
            </div>
            <div className={styles.line2}>Please</div>
            <button
              className={`${styles.link} link`}
              type="submit"
              onClick={e => this.checkCredentials()}
            >
              click here
            </button>{' '}
            once you complete above steps
          </div>
        )}
      </div>
    );
  }
}
export default withRouter(ServiceLoginCheck);
