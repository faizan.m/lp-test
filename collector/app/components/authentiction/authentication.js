// @flow
import React, { Component } from 'react';
import { Formik } from 'formik';
import { withRouter } from 'react-router';
import routes from '../../constants/routes.json';
import styles from './authentication.css';
import LocalStorageJSON from '../../../backend/db';

type Props = {};
type State = {};
const storage = new LocalStorageJSON();

class Authentication extends Component<Props, State> {
  props: Props;

  componentDidMount() {
    const token = storage.tokenExists();
    if (token) {
      // eslint-disable-next-line react/destructuring-assignment,react/prop-types
      this.props.history.push(routes.SERVICES);
    }
  }

  onCLickCounter = () => {
    // eslint-disable-next-line react/destructuring-assignment,react/prop-types
    this.props.history.push('/test');
  };

  render() {
    return (
      <div className={styles.container} data-tid="container">
        <h2>Acela Collector</h2>
      <Formik
          initialValues={{ token: '' }}
          validate={values => {
            const errors = {};
            if (!values.token) {
              errors.token = 'Token required';
            }
            return errors;
          }}
          onSubmit={(values, { setErrors }) => {
            // eslint-disable-next-line promise/catch-or-return,react/prop-types,react/destructuring-assignment
            this.props.validateCollectorToken(values.token).then(action => {
              // eslint-disable-next-line promise/always-return
              if (action && action.type === 'FETCH_AUTH_SUCCESS') {
                console.info('success', action, 'result');
                if (
                  action &&
                  action.response &&
                  action.response.data &&
                  action.response.data.is_valid
                ) {
                  storage.setToken(values.token);
                  const token = storage.tokenExists();
                  if (token) {
                    // eslint-disable-next-line react/destructuring-assignment,react/prop-types
                    this.props.history.push(routes.INTEGRATIONS);
                  }
                } else {
                  setErrors({
                    token: 'Invalid token'
                  });
                }
              } else {
                console.info('error', action);
                setErrors({
                  token: 'Invalid token'
                });
              }
            });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit} className={styles.credForm}>
              <div className={'fieldBox'}>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className={'fieldLabel'}>Agent Token</label>
                <div className={'field'}>
                  <input
                    type="token"
                    name="token"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.token}
                    placeholder="Agent Token"
                    className={'inputBox'}
                  />
                  <div className="error-message">
                    {errors.token && touched.token && errors.token}
                  </div>
                </div>
              </div>

              <button
                className={`${'primary'}  ${styles.submit}`}
                type="submit"
              >
                Validate
              </button>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}
export default withRouter(Authentication);
