// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Counter.css';
import routes from '../constants/routes.json';
import { Formik } from 'formik';
import localStorageJSON from './../../backend/db';
import { connect } from 'react-redux';

let storage = new localStorageJSON();
type Props = {
  increment: () => void,
  incrementIfOdd: () => void,
  incrementAsync: () => void,
  decrement: () => void,
  counter: number
};
type State = {
  validate: Boolean;
};

class Counter extends Component<Props> {
  props: Props;
  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    validate: false,
  });

  onCLickCounter =() =>{
    this.props.getData().then(action => {
      if (action.type === 'FETCH_DATA_SUCCESS') {
        console.info('success',action)
      } else {
        console.info('error',action)

      }
    });
  }

  render() {
    const {
      increment,
      incrementIfOdd,
      incrementAsync,
      decrement,
      counter
    } = this.props;
    return (
      <div>

        <div className={styles.backButton} data-tid="backButton">
          <Link to={routes.HOME}>
            <i className="fa fa-arrow-left fa-3x" />
          </Link>
        </div>
        <div className={styles.container} data-tid="container">
        <h3>Counter</h3>
{
  this.state.validate === false && (

        <Formik
      initialValues={{ name: '', token: '' }}
      validate={values => {
        const errors = {};
        if (!values.name) {
          errors.name = 'API Key required';
        }
        if (!values.token) {
          errors.token = 'API Secret required';
        }
        return errors;
      }}
      onSubmit={(values,{setErrors}) => {
        if (isValid) {
          console.info("Valid!!!");
          storage.setToken(values.token);
          const token = storage.tokenExists();
          if (token) {
             this.props.history.push('/counter')
          }
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
        <form onSubmit={handleSubmit}>

<div className={styles.fieldBox}>
            <label className={styles.fieldLabel}>API Key</label>
            <div  className={styles.fiel}>
              <input
                type="name"
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
                placeholder="API Key"
                className={styles.inputBox}
              />
              <div  className={styles.errorMsg}>{errors.name && touched.name && errors.name}</div>
            </div>
          </div>
          <div className={styles.fieldBox}>
            <label className={styles.fieldLabel}>API Secret</label>
            <div  className={styles.field}>
              <input
                type="token"
                name="token"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.token}
                placeholder="API Secret"
                className={styles.inputBox}
              />
              <div  className={styles.errorMsg}>{errors.token && touched.token && errors.token}</div>
            </div>
          </div>

          <button className={styles.submit} type="submit" disabled={isSubmitting}>
            Validate
          </button>
        </form>
      )}
    </Formik>
  )
      }
        {
    this.state.validate=== true  && (
      <h3> Service Started</h3>

    )
  }
    </div>

    <button className={`${'primary'}`} onClick={this.onCLickCounter} type="button">
    HOME
          </button>
    </div>
    );
  }
}

export default (Counter);
