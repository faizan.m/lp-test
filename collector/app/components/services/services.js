// @flow
import React, { Component } from 'react';
import { Formik } from 'formik';
import styles from './services.css';
import SolarwindsService from '../../../backend/solarwindsService';
import Spinner from '../../sharedcomponents/Spinner/index';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes';
import HostInfoService from '../../../backend/hostInfoService';
import HeartBeatService from '../../../backend/heartBeatService';
import { getDateTimeWithFormat } from '../../utils/DateUtils';

const solarwinds = new SolarwindsService();
const hostInfoService = new HostInfoService();
const heartBeatService = new HeartBeatService();

type Props = {
  getDataAuth: any
};

class Services extends Component<Props> {
  props: Props;

  // eslint-disable-next-line flowtype/no-weak-types
  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  componentDidMount() {
    const credentials = solarwinds.getCredentials();
    if (credentials) {
      this.setState({
        host: credentials.server,
        port: credentials.port,
        username: credentials.auth.username,
        password: credentials.auth.password
      });
    }
    const isAuthenticationCompleted = solarwinds.isAuthenticationCompleted();
    if (isAuthenticationCompleted) {
      this.setState({ isAuthenticationCompleted });
    }
    this.checkIntegrationCompleted();
    setInterval(
      () => {
        this.checkIntegrationCompleted();
        this.setState({ state: this.state })
      },
      20000
    );
  }

  checkIntegrationCompleted = () => {
    if (!this.state.isIntegrationCompleted) {
      const isIntegrationCompleted = solarwinds.isIntegrationCompleted();
      if (isIntegrationCompleted) {
        this.setState({ isIntegrationCompleted });
      }
    }
  }
  getEmptyState = () => ({
    validationMessage: '',
    fetching: false,
    edit: false,
    protocol: 'https://',
    host: '',
    port: 17778,
    username: '',
    password: '',
    isAuthenticationCompleted: false,
    isIntegrationCompleted: false

  });

  onCLickservices = () => {
    this.props.getDataAuth();
  };

  onCLickEdit = () => {
    this.setState({
      isAuthenticationCompleted: false,
      edit: true
    });
  };

  getLastRunTimestampSolarwinds = () => {
    const utcSeconds = solarwinds.getLastRunTimestamp();
    return getDateTimeWithFormat(utcSeconds)
  }

  getLastRunTimestampHostInfoService = () => {
    const utcSeconds = hostInfoService.getLastRunTimestamp();
    return getDateTimeWithFormat(utcSeconds)
  }
  getLastRunTimestampHeartBeatService = () => {
    const utcSeconds = heartBeatService.getLastRunTimestamp();
    return getDateTimeWithFormat(utcSeconds)
  }
  onCLickCancel = () => {
    this.setState({
      isAuthenticationCompleted: true,
      edit: false
    });
  };

  getCollectorName = () => {
    const name = heartBeatService.storage.getCollectorName();
    return name ? name : '';
  }
  render() {
    return (
      <div className="main-container">
        {/* <div className={styles.backButton} data-tid="backButton">
          <Link to={routes.AUTHENTICATION}>
            <i className="fa fa-arrow-left fa-3x" />
          </Link>
        </div> */}
        <div title={`Collector Name: ${this.getCollectorName()}`} className={styles.collectorName}>
        Collector: {this.getCollectorName()}
        </div>
        <div className={styles.container} data-tid="container">
          <div className={styles.solarwindHeading}>Services Integration</div>

          <div className={styles.SolarwindsCredentialsVerifiedBOX}>
            <ul className={styles.listingUL}>
              <li className={styles.listingLI}>
                <span
                  className={styles.listingULSpan}
                  style={{ color: '#3BB54A' }}
                >
                  <i className="fas fa-check-circle"></i>
                </span>
                Heartbeat Service{' '}
                <span className={styles.status}> (Running) </span>
                <div className={styles.dateStamp}>Last synced: {this.getLastRunTimestampHeartBeatService()}</div>
              </li>
              <li className={styles.listingLI}>
                <span
                  className={styles.listingULSpan}
                  style={{ color: '#3BB54A' }}
                >
                  <i className="fas fa-check-circle"></i>
                </span>
                Host Information Service{' '}
                <span className={styles.status}> (Running) </span>
                <div className={styles.dateStamp}>Last synced: {this.getLastRunTimestampHostInfoService()}</div>

              </li>
              <li
                className={styles.listingLI}
                onClick={e => this.props.history.push(routes.INTEGRATIONS)}
              >
                <span
                  className={styles.listingULSpan}
                  style={{
                    color:
                      this.state.isIntegrationCompleted === true
                        ? '#3BB54A'
                        : '#E3E9ED'
                  }}
                >
                  <i className="fas fa-check-circle"></i>
                </span>
                Solarwinds Service
                {this.state.isIntegrationCompleted === true ? (
                  <span className={styles.status}> (Running) </span>
                ) : (
                    <span className={styles.statusError}> (Configuration Pending) </span>
                  )}
                <div className={styles.dateStamp}>Last synced: {this.getLastRunTimestampSolarwinds()}</div>
              </li>
            </ul>
          </div>
        </div>
        <Spinner show={this.state.fetching} />
      </div>
    );
  }
}

export default Services;
