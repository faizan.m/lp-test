import React, { Component } from 'react';

import loader from './loading.svg';

import styles from './style.css';

interface ISpinnerProps {
  show: boolean;
  className?: string;
}

class Spinner extends Component<ISpinnerProps> {
  render() {
    if (this.props.show) {
      return (
        <div className={styles.loader}>
          <div className={`${styles.spinner} ${this.props.className}`}>
            <img src={loader} />
          </div>{' '}
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Spinner;
