export const getIdToken = () => {
  return localStorage.getItem('ID_TOKEN');
};