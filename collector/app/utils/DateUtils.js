import { format } from 'date-fns'

export const getDateTimeWithFormat = (utcSeconds, dateFormat = 'MM/dd/yyyy hh:mm a') => {
    const showDate = utcSeconds? format(new Date(utcSeconds), dateFormat): null; // The 0 there is the key, which sets the date to the epoch
    return showDate ? showDate : 'N.A.';
};