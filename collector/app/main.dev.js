/* eslint global-require: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import { app, BrowserWindow,ipcMain } from 'electron';
import { autoUpdater } from 'electron-updater';
import log from 'electron-log';

// WARNING: DONOT REMOVE THIS IMPORT. WINDOWS SERVICE WILL NOT WORK WITHOUT THIS
const service = require('./windowsService');
const { setAcelaBaseURL } = require('../backend/urls');

export default class AppUpdater {
  constructor() {
    log.transports.file.level = 'info';
    autoUpdater.logger = log;
    autoUpdater.autoDownload = true;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

let mainWindow = null;

if (process.env.NODE_ENV === 'production') {
  setAcelaBaseURL('https://dev.acela.io/api/v1');
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  setAcelaBaseURL('http://dev.acela.io:8000/api/v1');
}

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  require('electron-debug')();
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return Promise.all(
    extensions.map(name => installer.default(installer[name], forceDownload))
  ).catch(console.log);
};

const createWindow = async () => {
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    await installExtensions();
  }

  mainWindow = new BrowserWindow({
    show: false,
    width: 500,
    height: 700,
    webPreferences: {
      nodeIntegration: true
    }
  });

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
      mainWindow.focus();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  // const menuBuilder = new MenuBuilder(mainWindow);
  // menuBuilder.buildMenu();

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  new AppUpdater();
};

/**
 * Add event listeners...
 */
app.requestSingleInstanceLock();
app.on('second-instance', () => {
    app.quit();
});
app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('ready', createWindow);

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});


ipcMain.on('app_version', (event) => {
  log.info('app_version tt');
  event.sender.send('app_version', { version: app.getVersion() });
});

autoUpdater.on('update-available', () => {
  log.info('update-available tt');
  mainWindow.webContents.send('update_available');
});
 
const getFixedValue=(inputValue)=>{
  const value = inputValue ? inputValue.toFixed(2): 0;
  return value;
}

const bytes2Size= (byteVal)=>{
  var units=["Bytes", "KB", "MB", "GB", "TB"];
  var kounter=0;
  var kb= 1024;
  var div=byteVal/1;
  while(div>=kb){
      kounter++;
      div= div/kb;
  }
  return div.toFixed(1) + " " + units[kounter];
}
autoUpdater.on('download-progress', (progressObj) => {
  log.info('update-progress tt 1');
  let log_message = "";
  log_message = log_message + ' Downloaded <b> ' + getFixedValue(progressObj.percent) + '% </b><br>';
  log_message = log_message + ' (' + bytes2Size(progressObj.transferred) + "/" + bytes2Size(progressObj.total) + ')';
  log.info(log_message);
  console.info(log_message);
  mainWindow.webContents.send('message', log_message);
})
autoUpdater.on('update-downloaded', () => {
  log.info('update-downloaded tt');
  mainWindow.webContents.send('update_downloaded');
});

ipcMain.on('restart_app', () => {
  autoUpdater.quitAndInstall();
});

autoUpdater.on('error', (error) => {
  log.info('error 11', error);
});