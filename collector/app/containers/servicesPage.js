import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Services from '../components/services/services';
import * as AuthenticationActions from '../actions/authentication';

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(AuthenticationActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Services);
