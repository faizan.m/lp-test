import { connect } from 'react-redux';
import Authentication from '../components/authentiction/authentication';
import { validateCollectorToken } from '../actions/authentication';

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

const mapDispatchToProps = dispatch => {
  return {
    validateCollectorToken: (token) =>
      dispatch(validateCollectorToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);
