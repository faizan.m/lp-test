import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Integrations from '../components/integrations/integrations';
import * as AuthenticationActions from '../actions/authentication';

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(AuthenticationActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Integrations);
