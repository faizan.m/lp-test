import { connect } from 'react-redux';
import ServiceLoginCheck from '../components/ServiceLoginCheck/ServiceLoginCheck';
import { validateCollectorToken } from '../actions/authentication';

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

const mapDispatchToProps = dispatch => {
  return {
    validateCollectorToken: (token) =>
      dispatch(validateCollectorToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceLoginCheck);
