const LocalStorageJSON = require('./db');

const storage = new LocalStorageJSON();

function setAcelaBaseURL(baseUrl) {
  storage.setAcelaServerBaseURL(baseUrl);
}

function getAcelaUrls() {
  const baseUrl = storage.getAcelaServerBaseURL();
  const TOKEN_VALIDATION_URL = `${baseUrl}/collector-tokens`;
  const HEARTBEAT_POST_URL = `${baseUrl}/collector-heartbeat`;
  const SERVICES_POST_URL = `${baseUrl}/collector-services`;
  const DATA_POST_URL = `${baseUrl}/collector-data`;
  const FETCH_PENDING_QUERIES = `${baseUrl}/collector-queries`;
  const UPDATE_QUERY_RESULT = `${baseUrl}/collector-queries-update`;
  return {
    TOKEN_VALIDATION_URL,
    HEARTBEAT_POST_URL,
    SERVICES_POST_URL,
    DATA_POST_URL,
    FETCH_PENDING_QUERIES,
    UPDATE_QUERY_RESULT
  };
}
module.exports = { setAcelaBaseURL, getAcelaUrls };
