const electron = require('electron');
const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { EventLogger } = require('node-windows');
const log = new EventLogger('Acela Collector Service');
const userDataPath = "C:\\Windows\\system32\\config\\systemprofile\\AppData\\Local";
const dbPath  = path.join(userDataPath, 'db.json');
const adapter = new FileSync(dbPath);
const db = low(adapter);

class LocalStorageJSON {
  constructor() {
    this.db = db;
    db.defaults({ serviceConfigs: {}, token: null }).write();
  }

  setToken = token => {
    db.set('token', token).write();
  };

  getToken = () => {
    const token = db.get('token').value();
    return token;
  };

  refreshDB = () => {
    db.read();
  };

  tokenExists = () => {
    const exists = db.get('token').value();
    if (exists === null) {
      return false;
    }
    return true;
  };

  addServiceAuthConfig = (serviceName, credentials) => {
    db.set(`serviceConfigs.${serviceName}.authConfig`, credentials).write();
  };

  getServiceAuthConfig = serviceName => {
    return db.get(`serviceConfigs.${serviceName}.authConfig`).value();
  };

  addServiceMetaConfig = (serviceName, config) => {
    db.set(`serviceConfigs.${serviceName}.metaConfig`, config).write();
  };

  getServiceMetaConfig = serviceName => {
    return db.get(`serviceConfigs.${serviceName}.metaConfig`).value();
  };

  getServiceConfigs = () => db.get('serviceConfigs').value();

  updateServiceConfig(serviceName, config, updatedOn) {
    // eslint-disable-next-line no-param-reassign
    config.lastUpdated = updatedOn;
    this.addServiceMetaConfig(serviceName, config);
  }

  getServiceLastRunTimestamp = serviceName => {
    return db.get(`serviceConfigs.${serviceName}.lastRunAt`).value();
  };

  updateServiceLastRunTimestamp = (serviceName, timestamp) => {
    db.set(`serviceConfigs.${serviceName}.lastRunAt`, timestamp).write();
  };

  getServicesLastRunTimestamps = () => {
    const services = Object.keys(db.get('serviceConfigs').value());
    const result = {};
    // eslint-disable-next-line guard-for-in,no-restricted-syntax
    for (const serviceName in services) {
      result[services[serviceName]] = {};
      result[services[serviceName]].lastRunAt = this.getServiceLastRunTimestamp(
        services[serviceName]
      );
    }
    return result;
  };

  setAcelaServerBaseURL = baseURL => {
    db.set(`AcelaBaseURL`, baseURL).write();
  };

  getAcelaServerBaseURL = () => {
    return db.get(`AcelaBaseURL`).value();
  };

  setDeviceCount = (serviceName, count) => {
    db.set(`serviceConfigs.${serviceName}.devicesSyncCount`, count).write();
  };

  getDeviceCount = serviceName => {
    return db.get(`serviceConfigs.${serviceName}.devicesSyncCount`).value();
  };

  setCollectorName = name => {
    db.set(`collectorName`, name).write();
  };

  getCollectorName = () => {
    return db.get('collectorName').value();
  };

  // setPendingQueriesFlag = value => {
  //   db.set('pendingQueries', true).write();
  // }
  //
  // getPendingQueriesFlag = () => {
  //   return db.get('pendingQueries').value();
  // }
}

module.exports = LocalStorageJSON;
