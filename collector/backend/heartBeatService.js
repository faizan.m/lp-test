const axios = require('axios');

const { EventLogger } = require('node-windows');
const BaseService = require('./baseService');

const log = new EventLogger('Acela Collector Service');
const POLLING_INTERVAL_IN_SEC = 60;
const SERVICE_NAME = 'HeartBeatService';

class HeartBeatService extends BaseService {
  constructor() {
    super(SERVICE_NAME, POLLING_INTERVAL_IN_SEC);
  }

  syncServiceIntegrationConfigs = latestServiceConfigs => {
    const ExistingServiceConfigs = this.storage.getServiceConfigs();

    // eslint-disable-next-line no-restricted-syntax
    for (const integrationName of Object.keys(latestServiceConfigs)) {
      if (ExistingServiceConfigs.metaConfig === undefined) {
        this.storage.addServiceMetaConfig(
          integrationName,
          latestServiceConfigs[integrationName].meta_config
        );
      } else if (
        Date.parse(latestServiceConfigs[integrationName].updated_on) >
        Date.parse(ExistingServiceConfigs.metaConfig.lastUpdated)
      ) {
        this.storage.updateServiceConfig(
          integrationName,
          latestServiceConfigs[integrationName].meta_config,
          latestServiceConfigs[integrationName].updated_on
        );
      }
    }
  };

  sendCollectorHeartBeat = payload => {
    try {
      return axios.post(this.urls.HEARTBEAT_POST_URL, payload);
    } catch (error) {
      log.warn(error);
    }
  };

  checkPreCondition = () => {
    log.info(`DB object ${JSON.stringify(this.storage.db.source)}`);
    return this.storage.tokenExists();
  };

  process = () => {
    // Call Acela heartbeat webhook
    const serviceMetaData = this.storage.getServicesLastRunTimestamps();
    const payload = {
      token: this.getCollectorToken(),
      service_meta_data: serviceMetaData,
      collector_version: require('.././package.json').version
    };
    this.sendCollectorHeartBeat(payload)
      .then(response => {
        // eslint-disable-next-line promise/always-return
        if (response.data) {
          this.syncServiceIntegrationConfigs(response.data.integrations);
          this.storage.setCollectorName(response.data.collector_name);
          // this.storage.setPendingQueriesFlag(response.data.pending_collector_queries);
          log.info('Heart Beat sent!');
          console.info(response.data);
        }
      })
      .catch(error => {
        log.warn(error);
      });
  };
}
module.exports = HeartBeatService;
