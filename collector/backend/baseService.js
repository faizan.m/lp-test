const { EventLogger } = require('node-windows');
const LocalStorageJSON = require('./db');
const { getAcelaUrls } = require('./urls');

const log = new EventLogger('Acela Collector Service');

class BaseService {
  constructor(name, poolingInterval) {
    this.storage = new LocalStorageJSON();
    this.name = name;
    this.poolingInterval = poolingInterval;
    this.urls = getAcelaUrls();
  }

  getCollectorToken = () => {
    return this.storage.getToken();
  };

  getLastRunTimestamp = () => {
    const lastRunAt = this.storage.getServiceLastRunTimestamp(this.name);
    return lastRunAt;
  };

  updateLastRunAtTimestamp = () => {
    const date = new Date();
    const timestamp = date.getTime();
    this.storage.updateServiceLastRunTimestamp(this.name, timestamp);
  };

  shouldServiceRun = () => {
    const date = new Date();
    const currentTimestamp = date.getTime();
    const serviceLastRunTimestamp = this.getLastRunTimestamp();
    if (serviceLastRunTimestamp === undefined) {
      return true;
    }
    return (
      currentTimestamp - serviceLastRunTimestamp > this.poolingInterval * 1000
    );
  };

  checkPreCondition = () => {};

  process = () => {};

  poll = () => {
    this.storage.refreshDB();
    log.info(`In ${this.name} poll.`);
    if (this.checkPreCondition() && this.shouldServiceRun()) {
      this.process();
      this.updateLastRunAtTimestamp();
    }
  };
}

module.exports = BaseService;
