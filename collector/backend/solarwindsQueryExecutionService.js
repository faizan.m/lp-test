const axios = require('axios');
const { EventLogger } = require('node-windows');

const solarwind = require('./solarwindsClient');
const SolarwindsService = require('./solarwindsService');

const log = new EventLogger('Acela Collector Service');
const POLLING_INTERVAL_IN_SEC = 60;
const SERVICE_NAME = 'Solarwinds Query Execution Service';

class SolarwindsQueryExecutionService extends SolarwindsService {
  constructor() {
    super();
    this.name = SERVICE_NAME;
    this.poolingInterval = POLLING_INTERVAL_IN_SEC;
  }

  getQueries = async () => {
    const payload = { token: this.getCollectorToken() };
    try {
      return await axios.post(this.urls.FETCH_PENDING_QUERIES, payload);
    } catch (error) {
      console.error(error);
    }
  };

  executeQueries = async queries => {
    const results = [];
    const authConfig = new SolarwindsService().getCredentials();
    if (authConfig === undefined) {
      throw new Error('Auth Config not found.');
    }
    const orion = solarwind(authConfig);
    for (const query of queries) {
      const result = await this.callApi(orion, query.payload.query);
      const query_id = query.id;
      results.push({ query_id, result });
    }
    return results;
  };

  updateQueryResults = results => {
    const payload = { token: this.getCollectorToken(), queries: results };
    axios
      .post(this.urls.UPDATE_QUERY_RESULT, payload)
      .then(response => {
        // eslint-disable-next-line promise/always-return
        if (response.status === 200) {
          log.info('Queries Updated!');
        }
      })
      .catch(error => {
        log.info(`Error in Updating queries ${error}`);
      });
  };

  checkPreCondition = () => {
    return new SolarwindsService().checkPreCondition();
  };

  process = async () => {
    const queries = await this.getQueries();
    if (typeof queries.data !== 'undefined' && queries.data.length > 0) {
      this.executeQueries(queries.data)
        .then(results => {
          this.updateQueryResults(results);
        })
        .catch(error => {
          log.info('Error fetching solarwinds data.');
        });
    }
  };
}

module.exports = SolarwindsQueryExecutionService;
