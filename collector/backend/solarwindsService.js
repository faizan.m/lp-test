const axios = require('axios');
const { EventLogger } = require('node-windows');
const BaseService = require('./baseService');

const solarwind = require('./solarwindsClient');

const log = new EventLogger('Acela Collector Service');
const POLLING_INTERVAL_IN_SEC = 3 * 60;
const SERVICE_NAME = 'solarwinds';

class SolarwindsService extends BaseService {
  constructor() {
    super(SERVICE_NAME, POLLING_INTERVAL_IN_SEC);
  }

  callApi = (orion, query) => {
    /* Makes API call to the solarwinds Server.
    param orion: Solarwinds credentials Object
    param query: Query to be fetched
    returns: Promise
    */
    return new Promise((resolve, reject) => {
      // validate that password matches bambi (the deer)
      try {
        orion.query({ query }, function(result) {
          if (result.status.code === 200 && result.err === undefined) {
            const message = '';

            resolve({ data: result.results, message });
          } else {
            let message = result.err;
            if (result.status.code === 403) {
              message = 'Invalid Credentials.';
            } else if (result.err && result.err.code === 'ETIMEDOUT') {
              message = 'Unable to connect to the Host/IP.';
            } else if (result.err && result.err.code === 'ENOTFOUND') {
              message = 'Host address not found.';
            } else if (!result.err) {
              message = result.status.message;
            }
            resolve({ data: null, message });
          }
        });
      } catch (err) {
        log.info(err);
        reject(err);
      }
    });
  };

  validate = (server, port, username, password) => {
    /* Sample Config:
            {
            server: "127.0.0.1",
            port: 17778,
            auth: {
                    username: "admin",
                    password: "password"
                }
            }
        */
    const authConfig = {
      server,
      port,
      auth: {
        username,
        password
      }
    };
    const orion = solarwind(authConfig);
    let isValid = false;

    return new Promise((resolve, reject) => {
      // validate that password matches bambi (the deer)
      try {
        orion.query({ query: 'SELECT NodeID, URI from Orion.Nodes' }, function(
          result
        ) {
          if (result.status.code === 200 && result.err === undefined) {
            isValid = true;
            const message = 'Credentials Verified!';
            resolve({ isValid, message });
          } else {
            let message = result.err;
            if (result.status.code === 403) {
              message = 'Invalid Credentials.';
            } else if (result.err && result.err.code === 'ENOTFOUND') {
              message = 'Host address not found.';
            } else if (result.err && result.err.code === 'ETIMEDOUT') {
              message = 'Unable to connect to the Host/IP.';
            } else if (!result.err) {
              message = result.status.message;
            }
            resolve({ isValid, message });
          }
        });
      } catch (err) {
        log.info(err);
        reject(err);
      }
    });
  };

  saveCredentials = (server, port, username, password) => {
    const credentials = {
      server,
      port,
      auth: {
        username,
        password
      }
    };
    this.storage.addServiceAuthConfig(this.name, credentials);
  };

  getCredentials = () => {
    const creds = this.storage.getServiceAuthConfig(this.name);
    return creds;
  };

  getMetaConfig = () => {
    const metaConfig = this.storage.getServiceMetaConfig(this.name);
    return metaConfig;
  };

  saveMetaConfig = config => {
    this.storage.addServiceMetaConfig(this.name, config);
  };

  isAuthenticationCompleted = () => {
    const authConfig = this.getCredentials();
    if (authConfig === undefined) {
      return false;
    }
    return true;
  };

  isIntegrationCompleted = () => {
    const metaConfig = this.getMetaConfig();
    const authConfig = this.getCredentials();
    if (metaConfig === undefined || authConfig === undefined) {
      return false;
    }
    return true;
  };

  addServiceIntegration = () => {
    // Add solarwinds service integration in Acela.
    // eslint-disable-next-line camelcase
    const service_type = this.name;
    const payload = { token: this.getCollectorToken(), service_type };
    let integrated = false;
    axios
      .post(this.urls.SERVICES_POST_URL, payload)
      .then(response => {
        // eslint-disable-next-line promise/always-return
        if (response.status === 201) {
          log.info('Solarwinds integrtion created.');
          integrated = true;
          return integrated;
        }
      })
      .catch(error => {
        log.info(`Error in creating Solarwinds Integration ${error}`);
        return integrated;
      });
  };

  sendServiceData = serviceData => {
    const payload = {
      token: this.getCollectorToken(),
      service_data: { service_name: this.name, data: serviceData }
    };
    axios
      .post(this.urls.DATA_POST_URL, payload)
      .then(response => {
        // eslint-disable-next-line promise/always-return
        if (response.status === 200) {
          log.info('Solarwinds Data Sent!');
        }
      })
      .catch(error => {
        log.info(error);
      });
  };

  getData = async () => {
    // Get solarwinds data
    const authConfig = this.getCredentials();
    if (authConfig === undefined) {
      throw new Error('Auth Config not found.');
    }
    const metaConfig = this.getMetaConfig();
    if (metaConfig === undefined) {
      throw new Error('Meta Config not found.');
    }
    const deviceMappings = metaConfig.device_mappings;
    const orion = solarwind(authConfig);

    // eslint-disable-next-line no-restricted-syntax
    for (const [, config] of Object.entries(deviceMappings)) {
      // eslint-disable-next-line no-restricted-syntax
      for (const [, query] of Object.entries(config.queries)) {
        if (query.is_enabled) {
          // eslint-disable-next-line no-await-in-loop
          const result = await this.callApi(orion, query.query);
          if (result.data) {
            log.info(`Solarwinds Data Fetched.`);
            query.data = result.data;
            if (query.is_device_query) {
              const devicesCount = Object.keys(result.data).length;
              // set device count in db
              this.storage.setDeviceCount(this.name, devicesCount);
            }
          } else {
            log.info(`No Data found.`);
            query.data = [];
          }
        }
      }
    }
    return metaConfig;
  };

  getDeviceCount = () => {
    return this.storage.getDeviceCount(this.name);
  };

  checkPreCondition = () => {
    return this.isIntegrationCompleted();
  };

  process = async () => {
    if (this.isIntegrationCompleted()) {
      this.getData()
        // eslint-disable-next-line promise/always-return
        .then(serviceData => {
          log.info('Sending Service Data.');
          this.sendServiceData(serviceData);
        })
        // eslint-disable-next-line no-unused-vars
        .catch(error => {
          log.info('Error fetching solarwinds data.');
        });
    }
  };
}
module.exports = SolarwindsService;
