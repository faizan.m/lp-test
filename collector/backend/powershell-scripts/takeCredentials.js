const { spawn } = require('child_process');

let child;
const scriptPath = require('path').join(
  process.cwd(),
  './powershellScripts/validateLocalAccount.ps1'
);
console.info('__dirname', __dirname);
console.info('process', process.cwd());
// eslint-disable-next-line import/prefer-default-export
function inputCredentials() {
  // eslint-disable-next-line prefer-const
  let output = [];
  spawn('powershell.exe', [
    'Set-ExecutionPolicy -Scope CurrentUser Unrestricted'
  ]);
  child = spawn('powershell.exe', [scriptPath]);
  return new Promise((resolve, reject) => {
    try {
      child.stdout.on('data', function(data) {
        const cleanedData = data.toString().trim();
        if (cleanedData) {
          output.push(cleanedData);
        }
      });
      child.stderr.on('data', function(data) {
        console.log(`Powershell Errors: ${data}`);
      });
      child.on('exit', function() {
        console.log('Powershell Script finished', output);
        const userPass= output[1] && output[1].split(/\r?\n/)
        let verified = false;
        const username = userPass && userPass[0];
        const password = userPass && userPass[1];
        if (output && output[0] === 'True') {
          verified = true;
        }
        resolve({ verified, username, password });
      });
      child.stdin.end(); // end input
    } catch (err) {
      log.info(err);
      reject(err);
    }
  });
}

module.exports = inputCredentials;
