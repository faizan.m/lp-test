const { spawn } = require('child_process');

let child;
const scriptPath = require('path').join(
  process.cwd(),
  './powershellScripts/change-service-account.ps1'
);

const resultCodes = {
  0: 'Success',
  1: 'Not Supported',
  2: 'Access Denied',
  3: 'Dependent Services Running',
  4: 'Invalid Service Control',
  5: 'Service Cannot Accept Control',
  6: 'Service Not Active',
  7: 'Service Request Timeout',
  8: 'Unknown Failure',
  9: 'Path Not Found',
  10: 'Service Already Running',
  11: 'Service Database Locked',
  12: 'Service Dependency Deleted',
  13: 'Service Dependency Failure',
  14: 'Service Disabled',
  15: 'Service Logon Failure',
  16: 'Service Marked For Deletion',
  17: 'Service No Thread',
  18: 'Status Circular Dependency',
  19: 'Status Duplicate Name',
  20: 'Status Invalid Name',
  21: 'Status Invalid Parameter',
  22: 'Status Invalid Service Account',
  23: 'Status Service Exists',
  24: 'Service Already Paused',
  25: 'Service Not Found'
};
// eslint-disable-next-line import/prefer-default-export
function changeServiceAccount(serviceName, username, password) {
  spawn('powershell.exe', [
    'Set-ExecutionPolicy -Scope CurrentUser Unrestricted'
  ]);
  // eslint-disable-next-line prefer-const
  child = spawn('powershell.exe', [
    scriptPath,
    `-name ${serviceName} -username ${username} -password ${password}`
  ]);
  return new Promise((resolve, reject) => {
    try {
      const output = [];
      child.stdout.on('data', function(data) {
        const response = data.toString().trim();
        if (response) {
          console.log(resultCodes[Number(response)]);
        }
        const cleanedData = data.toString().trim();
        if (cleanedData) {
          output.push(cleanedData);
        }
      });
      child.stderr.on('data', function(data) {
        console.log(`Powershell Errors: ${data}`);
      });
      child.on('exit', function() {
        console.log('Powershell Script finished');
        let success = false;
        let message = '';
        if (output) {
          if (Number(output[0]) === 0) {
            success = true;
          }
          message = resultCodes[Number(output[0])];
        }
        resolve({ success, message });
      });
      child.stdin.end(); // end input
    } catch (err) {
      log.info(err);
      reject(err);
    }
  });
}

module.exports = changeServiceAccount;
