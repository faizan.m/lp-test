const { spawn } = require('child_process');

let child;
const scriptPath = require('path').join(
  process.cwd(),
  './powershellScripts/add-logon-service-permission.ps1'
);

function grantLogAsServicePermission(username) {
  // eslint-disable-next-line prefer-const
  spawn('powershell.exe', [
    'Set-ExecutionPolicy -Scope CurrentUser Unrestricted'
  ]);
  child = spawn('powershell.exe', [scriptPath, username]);
  const output = [];
  child.stdout.on('data', function(data) {
    const cleanedData = data.toString().trim();
    if (cleanedData) {
      output.push(cleanedData);
    }
  });
  child.stderr.on('data', function(data) {
    console.log(`Powershell Errors: ${data}`);

  });
  child.on('exit', function() {
    console.log('Powershell Script finished');
  });
  child.stdin.end(); // end input
}
module.exports = grantLogAsServicePermission;
