$cred = Get-Credential #Read credentials
$username = $cred.username
$password = $cred.GetNetworkCredential().password
$computer = $env:COMPUTERNAME
Add-Type -AssemblyName System.DirectoryServices.AccountManagement
$obj = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',
$computer)
$obj.ValidateCredentials($username, $password)
Write-Host($username)
Write-Host($password)
