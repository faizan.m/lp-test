$PSversion = $PSVersionTable.PSVersion.Major
if ($PSversion > 2){
  Import-Module $PSScriptRoot\UserRights.psm1
} else {
  $scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
  Import-Module $scriptPath\UserRights.psm1
}

Grant-UserRight -Account $args -Right SeServiceLogonRight
