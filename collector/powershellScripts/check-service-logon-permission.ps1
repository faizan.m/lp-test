$PSversion = $PSVersionTable.PSVersion.Major
if ($PSversion > 2){
  Import-Module $PSScriptRoot\UserRights.psm1
} else {
  $scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
  Import-Module $scriptPath\UserRights.psm1
}

 $accountWithServiceRights = Get-AccountsWithUserRight -Right SeServiceLogonRight
 $userNames = @()
 foreach ($account in $accountWithServiceRights)
 {
   $userNames = $userNames + $account.Split("\")[-1]
 }
 $userNames -contains $args

