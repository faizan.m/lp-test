<#
.Synopsis
  Installs a Windows service on a remote computer.
  Sets the credentials for the service to run under.
  Grants SeServiceLogonRight to the account.
  Sets service recovery options.
.Parameter computerName
  Defines the name of the computer which will host the service.
  Default is the local computer on which the script is run.
.Parameter name
  Defines the name (in Services Manager) of the service.
  Mandatory parameter
.Parameter display
  Defines the display name (in Services Manager) of the service.
  Default is to match the service name
.Parameter description
  Defines the description (in Services Manager) of the service.
  Default is to match the service display name
.Parameter path
  Defines the path to the service executable (including command arguments).
.Parameter startup
  Defines the startup type of the service.
  Can be set to: Automatic, Manual or Disabled
.Parameter username
  Defines the username under which the service should run.
  Use the form: domain\username.
.Parameter password
  Defines the password for the user under which the service should run.
.Parameter recoverReset
  Defines the number of days to wait before resetting the fail count (0 = never).
.Parameter recoverDelay
  Defines the number of milliseconds to wait before taking recovery action.
.Parameter recoverActions
  Defines the recovery actions to take when the service stops unexpectedly.
.Parameter recoverCommand
  Defines a command to run when the service stops unexpectedly.
  eg: "echo 'crash count: %1%'>> C:\Services\TeamCityConfigMonitor\crash.log"
.Parameter removeOnly
  If set, removes the service specified by $name on host specified by $computerName. No service will be installed and configured.
.Example
  Usage:
  .\ServiceDeploy.ps1 -computerName hostname.domain.com -name TeamCityConfigMonitor -display "TeamCity Config Monitor" -path "C:\Services\TeamCityConfigMonitor\TeamCityConfigMonitor.exe" -username "domain\username" -password "password"
#>
param(
  [string] $computerName = "Atul.Local",
  [string] $name,
  [string] $username = $null,
  [string] $password = $null
)


Invoke-Command -Script {
  param(
    [string] $computerName,
    [string] $name,
    [string] $username,
    [string] $password
  )
  $returnCodes = @{
    0 = "Success"; 1 = "Not Supported"; 2 = "Access Denied"; 3 = "Dependent Services Running";
    4 = "Invalid Service Control"; 5 = "Service Cannot Accept Control"; 6 = "Service Not Active";
    7 = "Service Request Timeout"; 8 = "Unknown Failure"; 9 = "Path Not Found"; 10 = "Service Already Running";
    11 = "Service Database Locked"; 12 = "Service Dependency Deleted"; 13 = "Service Dependency Failure";
    14 = "Service Disabled"; 15 = "Service Logon Failure"; 16 = "Service Marked For Deletion";
    17 = "Service No Thread"; 18 = "Status Circular Dependency"; 19 = "Status Duplicate Name";
    20 = "Status Invalid Name"; 21 = "Status Invalid Parameter"; 22 = "Status Invalid Service Account";
    23 = "Status Service Exists"; 24 = "Service Already Paused"; 25 = "Service Not Found";
  }
  # Remove service if it exists.
  if ((Get-Service $name -ErrorAction SilentlyContinue))
  {
    $service = Get-WmiObject -Class Win32_Service -Filter "Name='$name'"
    # Set the credentials under which the service should run if a username and password were provided.
    if (($username) -and ($password))
    {
      if (!$username.Contains("\"))
      {
        $username = ".\{0}" -f $username
      }
      # Configure service to use provided credentials
      #      Write-Host ("Changing service {0} ({1}) to run under user account: {2}." -f $service.Name,
      #      $service.DisplayName,
      #      $username)
      $changeStatus = $service.Change($null, $null, $null, $null, $null, $null, $username, $password, $null, $null, $null)
      switch ([int]$changeStatus.ReturnValue)
      {
        0 {
          Write-Host ($( $changeStatus.ReturnValue ))
        }
        default {
          Write-Host ($( $changeStatus.ReturnValue ))
        }
      }
    }
    else
    {
      Write-Host ("Service: {0}, is set to run under user account: {1}." -f $name, $service.StartName)
    }

  }

  else
  {
    Write-Host (25)
  }
} -ArgumentList $computerName, $name, $username, $password
