echo "Check/Install required packages..."
yes | pip install --quiet pylint==1.8.1 pyflakes==1.6.0 pycodestyle==2.3.1
echo "Check pycodestyle..."
pycodestyle --statistics --show-source --show-pep8 --count  --config=pycodestyle.config .
echo "Check pylint... "
pylint backend/ --rcfile=pylint.config
echo "Check pyflakes..."
pyflakes .
