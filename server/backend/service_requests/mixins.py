from rest_framework.exceptions import ValidationError

from accounts.models import UserType


class ServiceRequestModelMixin(object):
    def get_created_by_for_service_ticket(self, user, **kwargs):
        created_by = None
        if user.type == UserType.CUSTOMER:
            created_by = user.crm_id
        elif user.type == UserType.PROVIDER:
            created_by = user.profile.customer_user_instance_id
        if not created_by:
            raise ValidationError(
                "Profile settings has not been configured. Please contact the site administrator."
            )
        return created_by

    def is_user_customer_user(self, user) -> bool:
        if user.type == UserType.PROVIDER:
            return False
        elif user.type == UserType.CUSTOMER:
            return True
