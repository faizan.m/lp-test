import base64

from django_downloadview import HTTPDownloadView
from rest_framework import generics, status
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from typing import Dict, List, Optional, Union
from accounts.custom_permissions import (
    IsProviderOrCustomerUser,
    IsCustomerUser,
    IsCustomerMSCustomer,
)
from accounts.models import UserType
from core import utils
from core.utils import get_customer_cw_id, get_customer_id
from document.services import QuoteService
from service_requests.mixins import ServiceRequestModelMixin
from service_requests.serializers import (
    ServiceTicketSerializer,
    ServiceTicketDetailSerializer,
    NoteSerializer,
    UploadDocumentSerializer,
    ServiceTicketUpdateSerializer,
)
from service_requests.services import ServiceRequestService


class ServiceTicketListCreateAPIView(
    ServiceRequestModelMixin, generics.ListCreateAPIView
):
    serializer_class = ServiceTicketSerializer
    permission_classes = (
        IsAuthenticated,
        IsProviderOrCustomerUser,
        IsCustomerMSCustomer,
    )
    pagination_class = None

    def get_queryset(self):
        closed: str = self.request.query_params.get("closed", "false")
        customer_crm_id: int = utils.get_customer_cw_id(
            self.request, **self.kwargs
        )
        user_is_customer_user: bool = self.is_user_customer_user(
            self.request.user
        )
        if closed == "true":
            data: List[
                Dict
            ] = ServiceRequestService.list_managed_service_board_closed_tickets(
                self.request.provider.id,
                customer_crm_id,
                user_is_customer_user=user_is_customer_user,
            )
        else:
            data: List[
                Dict
            ] = ServiceRequestService.list_managed_service_board_open_tickets(
                self.request.provider.id,
                customer_crm_id,
                user_is_customer_user=user_is_customer_user,
            )
        return data

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_crm_id: int = utils.get_customer_cw_id(request, **kwargs)
        creator_crm_id: int = self.get_created_by_for_service_ticket(
            request.user, **kwargs
        )
        ticket: Dict = ServiceRequestService.create_service_ticket(
            self.request.provider.id,
            customer_crm_id,
            creator_crm_id,
            serializer.validated_data,
        )
        headers = self.get_success_headers(serializer.data)
        return Response(
            ticket, status=status.HTTP_201_CREATED, headers=headers
        )


class ServiceTicketRetrieveUpdateAPIView(
    ServiceRequestModelMixin, generics.RetrieveUpdateAPIView
):
    permission_classes = (
        IsAuthenticated,
        IsProviderOrCustomerUser,
        IsCustomerMSCustomer,
    )

    def get_object(self):
        ticket_crm_id: int = self.kwargs.get("pk")
        if not ticket_crm_id:
            raise NotFound("Invalid ticket Id")
        is_user_customer_user: bool = self.is_user_customer_user(
            self.request.user
        )
        ticket: Dict = ServiceRequestService.get_service_ticket_details(
            self.request.provider.id,
            ticket_crm_id,
            user_is_customer_user=is_user_customer_user,
        )
        return ticket

    def perform_update(self, serializer):
        action: str = self.request.query_params.get("action", "")
        if action == "close":
            ticket_crm_id: int = self.kwargs.get("pk")
            if not ticket_crm_id:
                raise NotFound("Invalid ticket ID")
            customer_crm_id: int = utils.get_customer_cw_id(
                self.request, **self.kwargs
            )
            ServiceRequestService.close_service_ticket(
                self.request.provider.id, customer_crm_id, ticket_crm_id
            )
            return Response(status=status.HTTP_200_OK)
        else:
            raise NotFound({"action": "Required field not found."})

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return ServiceTicketUpdateSerializer
        else:
            return ServiceTicketDetailSerializer


class ServiceTicketNoteCreateAPIView(
    ServiceRequestModelMixin, generics.CreateAPIView
):
    serializer_class = NoteSerializer
    permission_classes = (
        IsAuthenticated,
        IsProviderOrCustomerUser,
        IsCustomerMSCustomer,
    )

    def perform_create(self, serializer):
        ticket_crm_id: int = self.kwargs.get("pk")
        creator_crm_id: int = self.get_created_by_for_service_ticket(
            self.request.user, **self.kwargs
        )
        user_is_customer_user: bool = self.is_user_customer_user(
            self.request.user
        )
        if user_is_customer_user:
            # Customer user can only add discussion notes.
            is_internal_note: bool = False
        else:
            # Provider user can add both internal & discussion notes.
            is_internal_note: bool = serializer.validated_data.pop(
                "is_internal_note"
            )
        ServiceRequestService.create_service_ticket_note(
            self.request.provider.erp_client,
            ticket_crm_id,
            creator_crm_id,
            serializer.validated_data,
            is_internal_note=is_internal_note,
        )


class ServiceTicketStatus(APIView, IsCustomerMSCustomer):
    def get(self, request, *args, **kwargs):
        state = request.query_params.get("status", "closed")
        data = ServiceRequestService.get_service_ticket_statuses(
            request.provider.id, state
        )
        return Response(data=data, status=status.HTTP_200_OK)


class ConnectwiseTicketDocumentsDownloadView(HTTPDownloadView, APIView):
    """
    API View to return stream of document.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        file_ = self.get_file()
        data = base64.b64encode(file_.file.read())
        response = Response(data)
        return response

    def get_request_factory(self):
        return self.request.provider.erp_client.client._get

    def get_url(self):
        document_id = self.kwargs.get("document_id")
        url = self.request.provider.erp_client.get_document_download_url(
            document_id
        )
        return url


class ServiceTicketDocumentCreateAPIView(generics.CreateAPIView):
    serializer_class = UploadDocumentSerializer
    permission_classes = (
        IsAuthenticated,
        IsProviderOrCustomerUser,
        IsCustomerMSCustomer,
    )

    def perform_create(self, serializer):
        serializer.is_valid(raise_exception=True)
        ticket_id = self.kwargs.get("pk")
        ServiceRequestService.create_service_ticket_documents(
            self.request.provider.erp_client,
            ticket_id,
            serializer.validated_data,
        )


class ServiceTicketNoteRetrieveAPIView(APIView):
    permission_classes = (
        IsAuthenticated,
        IsProviderOrCustomerUser,
        IsCustomerMSCustomer,
    )

    def get(self, request, **kwargs):
        """
        Returns Service Ticket Note for Customer Provider.
        """
        ticket_note = request.provider.ticket_note
        return Response({"ticket_note": ticket_note})


class ServiceDashboardAPIView(APIView):
    """API View to return Service dashboard metrics for provider and customer."""

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        dashboard = {}
        if request.user.type == UserType.PROVIDER:
            dashboard = ServiceRequestService.get_service_dashboard_metrics(
                request.provider
            )
        elif request.user.type == UserType.CUSTOMER:
            dashboard = ServiceRequestService.get_service_dashboard_metrics(
                request.provider, request.customer.crm_id
            )

        return Response(dashboard)


class LifecycleManagementDashboardAPIView(APIView):
    """
    API View to return Lifecycle chart metrics.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        dashboard = {}
        if request.user.type == UserType.PROVIDER:
            dashboard = (
                ServiceRequestService.get_lifecyclemanagement_dashboard(
                    request.provider
                )
            )
        elif request.user.type == UserType.CUSTOMER:
            dashboard = (
                ServiceRequestService.get_lifecyclemanagement_dashboard(
                    request.provider, request.customer.crm_id
                )
            )

        return Response(dashboard)


class OrdersDashboardAPIView(APIView):
    """
    API View to Order dashboard chart metrics.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        dashboard = {}
        if request.user.type == UserType.PROVIDER:
            dashboard = QuoteService.get_orders_dashboard(request.provider)
        elif request.user.type == UserType.CUSTOMER:
            dashboard = QuoteService.get_orders_dashboard(
                request.provider, request.customer.crm_id
            )

        return Response(dashboard)


class OpenQuotesDashboardAPIView(APIView):
    """
    API View to Open Quotes dashboard chart.
    """

    permission_classes = (IsAuthenticated, IsCustomerUser)

    def get(self, request, **kwargs):
        customer_id = get_customer_id(request, **kwargs)
        dashboard = QuoteService.get_quotes(request.provider.id, customer_id)

        return Response(dashboard)


class QuoteDocumentsAPIView(APIView):
    """
    API View to List Quote Documents.
    """

    permission_classes = (IsAuthenticated, IsCustomerUser)

    def get(self, request, **kwargs):
        quote_id = self.kwargs.get("quote_id")
        customer_id = get_customer_cw_id(request, **kwargs)
        dashboard = QuoteService.get_quote_document(
            request.provider.id, customer_id, quote_id
        )

        return Response(dashboard)
