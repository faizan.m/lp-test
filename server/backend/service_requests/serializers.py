from django.core.validators import FileExtensionValidator
from rest_framework import serializers

from accounts.models import User
from rest_framework.exceptions import ValidationError


class NoteSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    text = serializers.CharField()
    contact_id = serializers.IntegerField(required=False, read_only=True)
    contact_name = serializers.CharField(required=False, read_only=True)
    member_id = serializers.IntegerField(required=False, read_only=True)
    member_name = serializers.CharField(required=False, read_only=True)
    created_by = serializers.CharField(required=False, read_only=True)
    alternate_created_by = serializers.CharField(
        required=False, read_only=True
    )
    created_on = serializers.DateTimeField(read_only=True)
    last_updated_on = serializers.DateTimeField(read_only=True)
    is_internal_note = serializers.BooleanField(required=False)


class DocumentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=False)
    file_name = serializers.CharField(required=False)
    server_file_name = serializers.CharField(required=False)


class ServiceTicketSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    summary = serializers.CharField()
    description = serializers.CharField(required=False)
    record_type = serializers.CharField(required=False, read_only=True)
    primary_contact = serializers.CharField(required=False)
    board = serializers.CharField(required=False, read_only=True)
    board_id = serializers.IntegerField(required=False, read_only=True)
    status = serializers.CharField(required=False, read_only=True)
    status_id = serializers.IntegerField(required=False, read_only=True)
    company_id = serializers.IntegerField(required=False, read_only=True)
    priority = serializers.CharField(required=False, read_only=True)
    priority_id = serializers.IntegerField(required=False, read_only=True)
    severity = serializers.CharField(required=False, read_only=True)
    impact = serializers.CharField(required=False, read_only=True)
    owner = serializers.CharField(required=False, read_only=True)
    resource = serializers.CharField(required=False, read_only=True)
    resource_phone_number = serializers.CharField(
        required=False, read_only=True
    )
    update = serializers.CharField(required=False, read_only=True)
    created_on = serializers.CharField(required=False, read_only=True)
    last_updated_on = serializers.CharField(required=False, read_only=True)
    can_close = serializers.BooleanField(required=False, read_only=True)

    def validate_primary_contact(self, contact_id):
        request = self.context.get("request")
        provider = request.provider
        try:
            user = User.default_manager.get(provider=provider, id=contact_id)
        except User.DoesNotExist:
            raise ValidationError("Invalid Primary Contact")
        return user.crm_id


class ServiceTicketDetailSerializer(ServiceTicketSerializer):
    notes = NoteSerializer(many=True, required=False)
    documents = DocumentSerializer(many=True, required=False)


class ServiceTicketUpdateSerializer(serializers.Serializer):
    pass


class UploadDocumentSerializer(serializers.Serializer):
    title = serializers.CharField()
    file = serializers.FileField(
        validators=[
            FileExtensionValidator(
                [
                    "png",
                    "jpeg",
                    "jpg",
                    "pdf",
                    "txt",
                    "text",
                    "mpp" "doc",
                    "docx",
                    "docm",
                    "xls",
                    "xlsx",
                    "ppt",
                    "zip",
                    "csv",
                    "dxf",
                    "psd",
                    "vdx",
                    "vsd",
                    "vsdx",
                    "pptx",
                    "pptm",
                ]
            )
        ]
    )


class TicketDocumentUploadSerializer(serializers.Serializer):
    documents = serializers.ListField(child=UploadDocumentSerializer())
