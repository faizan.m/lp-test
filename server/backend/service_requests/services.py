from django.contrib.postgres.fields.jsonb import KeyTextTransform
from django.db.models import Case, DateField, When, Q
from django.db.models.functions import Cast
from django.utils import timezone
from typing import List, Dict, Union, Optional
from accounts.models import Provider
from core.exceptions import InvalidConfigurations
from core.models import DeviceManufacturerData
from core.services import DeviceService
from caching.service import CacheService, caching


class ServiceRequestService(object):
    @classmethod
    def list_all_service_tickets(cls, erp_client, customer_id):
        """
        Returns list of service tickets
        """
        service_tickets = erp_client.get_service_tickets(customer_id)
        return service_tickets

    @classmethod
    def get_provider_services_board_configs(cls, provider):
        """
        Returns Managed services configs from Provider Connectwise Board mapping configs.
        """

        if not provider.integration_statuses["crm_board_mapping_configured"]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        managed_services_configs = (
            provider.erp_integration.other_config.get("board_mapping")
            .get("Service Board Mapping")
            .get("Managed Services")
        )
        professional_services_configs = (
            provider.erp_integration.other_config.get("board_mapping")
            .get("Service Board Mapping")
            .get("Professional Services")
        )
        projects_configs = (
            provider.erp_integration.other_config.get("board_mapping")
            .get("Service Board Mapping")
            .get("Projects")
        )
        return (
            managed_services_configs,
            professional_services_configs,
            projects_configs,
        )

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.OPEN_SERVICE_REQUEST_TICKETS,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def list_managed_service_board_open_tickets(
        cls,
        provider_id: int,
        customer_crm_id: int,
        user_is_customer_user: bool,
    ):
        """
        Returns list of service tickets for managed services board with open status
        """
        provider: Provider = Provider.objects.get(id=provider_id)
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        boards: List[int] = managed_services_configs.get("board")
        open_statuses: List[int] = managed_services_configs.get("open_status")
        if not (boards and open_statuses):
            raise InvalidConfigurations(
                "Managed Services Board not Configured Properly."
            )
        service_tickets: List[Dict] = provider.erp_client.get_service_tickets(
            customer_crm_id, boards, open_statuses
        )

        for service_ticket in service_tickets[:20]:
            # Only list discussion (non-internal) notes for customer user.
            if user_is_customer_user:
                _internal_flag_filter: bool = False
                _external_flag_filter: bool = True
            # List both internal & discussion notes for provider user.
            else:
                _internal_flag_filter: bool = False
                _external_flag_filter: bool = False
            notes: List[Dict] = ServiceRequestService.get_ticket_notes(
                provider.erp_client,
                service_ticket.get("id"),
                external_flag_filter=_external_flag_filter,
                internal_flag_filter=_internal_flag_filter,
            )
            if notes:
                service_ticket["update"] = notes[0]["text"]
            else:
                service_ticket["update"] = None
        for service_ticket in service_tickets:
            service_ticket["can_close"] = True
        return service_tickets

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.CLOSED_SERVICE_REQUEST_TICKETS,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def list_managed_service_board_closed_tickets(
        cls, provider_id: int, customer_id: int, user_is_customer_user: bool
    ) -> List[Dict]:
        """
        Returns list of service tickets for managed services board with closed status
        """
        provider: Provider = Provider.objects.get(id=provider_id)
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        boards: List[int] = managed_services_configs.get("board")
        closed_statuses: List[int] = managed_services_configs.get(
            "closed_status"
        )
        if not (boards and closed_statuses):
            raise InvalidConfigurations(
                "Managed Services Board not Configured Properly."
            )
        service_tickets: List[Dict] = provider.erp_client.get_service_tickets(
            customer_id, boards, closed_statuses
        )
        for service_ticket in service_tickets[:20]:
            # Only list discussion (non-internal) notes for customer user.
            if user_is_customer_user:
                _internal_flag_filter: bool = False
                _external_flag_filter: bool = True
            # List both internal & discussion notes for provider user.
            else:
                _internal_flag_filter: bool = False
                _external_flag_filter: bool = False
            notes: List[Dict] = ServiceRequestService.get_ticket_notes(
                provider.erp_client,
                service_ticket.get("id"),
                internal_flag_filter=_internal_flag_filter,
                external_flag_filter=_external_flag_filter,
            )
            if notes:
                service_ticket["update"] = notes[0]["text"]
            else:
                service_ticket["update"] = None
        for service_ticket in service_tickets:
            service_ticket["can_close"] = False
        return service_tickets

    @classmethod
    def get_open_managed_services_tickets_count(
        cls, provider, customer_id=None
    ):
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        boards = managed_services_configs.get("board")
        open_statuses = managed_services_configs.get("open_status")
        if not (boards and open_statuses):
            raise InvalidConfigurations(
                "Managed Services Board not Configured Properly."
            )
        count = provider.erp_client.get_service_tickets_count(
            boards, open_statuses, customer_id
        )
        return count

    # @classmethod
    # def get_open_professional_services_tickets_count(cls, provider, customer_id=None):
    #     _, professional_services_config, _ = cls.get_provider_services_board_configs(
    #         provider
    #     )
    #     boards = professional_services_config.get("board")
    #     open_statuses = professional_services_config.get("open_status")
    #     if not (boards and open_statuses):
    #         raise InvalidConfigurations(
    #             "Professional Services Board not Configured Properly."
    #         )
    #     count = provider.erp_client.get_service_tickets_count(
    #         boards, open_statuses, customer_id
    #     )
    #     return count

    @classmethod
    def get_open_projects_count(cls, provider, customer_id):
        _, _, projects_config = cls.get_provider_services_board_configs(
            provider
        )
        boards = projects_config.get("board")
        open_statuses = projects_config.get("open_status")
        if not (boards and open_statuses):
            raise InvalidConfigurations(
                "Projects Board not Configured Properly."
            )
        count = provider.erp_client.get_projects_count(
            boards, open_statuses, customer_id
        )
        return count

    @classmethod
    def get_service_ticket(cls, erp_client, ticket_id):
        """
        Returns a single service ticket details
        """
        service_ticket = erp_client.get_service_ticket(ticket_id)
        return service_ticket

    @classmethod
    def get_ticket_notes(
        cls,
        erp_client,
        ticket_id,
        external_flag_filter=True,
        internal_flag_filter=False,
    ) -> List[Dict]:
        """
        Returns notes for a ticket
        """
        notes: List[Dict] = erp_client.get_service_ticket_notes(
            ticket_id,
            external_flag_filter=external_flag_filter,
            internal_flag_filter=internal_flag_filter,
        )
        return notes

    @classmethod
    def get_service_ticket_details(
        cls, provider_id: int, ticket_crm_id: int, user_is_customer_user: bool
    ) -> Dict:
        provider: Provider = Provider.objects.get(id=provider_id)
        erp_client: Connectwise = provider.erp_client
        ticket: Dict = cls.get_service_ticket(erp_client, ticket_crm_id)
        # Only list discussion (non-internal) notes for customer user.
        if user_is_customer_user:
            _internal_flag_filter: bool = False
            _external_flag_filter: bool = True
        # List both internal & discussion notes for provider user.
        else:
            _internal_flag_filter: bool = False
            _external_flag_filter: bool = False
        ticket["notes"] = ServiceRequestService.get_ticket_notes(
            erp_client,
            ticket_crm_id,
            external_flag_filter=_external_flag_filter,
            internal_flag_filter=_internal_flag_filter,
        )
        if ticket["notes"]:
            ticket["description"] = ticket["notes"][-1]["text"]
        ticket["documents"] = ServiceRequestService.get_ticket_documents(
            erp_client, ticket_crm_id
        )
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        open_statuses = managed_services_configs.get("open_status")
        if ticket.get("status_id") in open_statuses:
            ticket["can_close"] = True
        else:
            ticket["can_close"] = False
        return ticket

    @classmethod
    def create_service_ticket_note(
        cls,
        erp_client,
        ticket_crm_id: int,
        creator_crm_id: int,
        data: Dict,
        is_internal_note: bool = True,
    ) -> Dict:
        if is_internal_note:
            note: Dict = erp_client.create_ticket_internal_note(
                ticket_crm_id, creator_crm_id, data
            )
        else:
            note: Dict = erp_client.create_ticket_discussion_note(
                ticket_crm_id, creator_crm_id, data
            )
        return note

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.OPEN_SERVICE_REQUEST_TICKETS,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def create_service_ticket(
        cls,
        provider_id: int,
        customer_crm_id: int,
        creator_crm_id: int,
        data: Dict,
    ) -> Dict:
        provider: Provider = Provider.objects.get(id=provider_id)
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        boards: List[int] = managed_services_configs.get("board")
        erp_client = provider.erp_client
        # Create Ticket
        ticket: Dict = erp_client.create_service_ticket(
            customer_crm_id, boards[0], creator_crm_id, data
        )
        # Add Description as ticket note
        note_payload: Dict = dict(text=data["description"])
        # cls.create_service_ticket_note(
        #     erp_client, ticket["id"], creator_crm_id, note_payload
        # )
        provider.erp_client.create_ticket_discussion_note(
            ticket["id"], creator_crm_id, note_payload
        )
        CacheService.invalidate_data(
            [
                CacheService.CACHE_KEYS.SERVICE_DASHBOARD.format(
                    provider.id, customer_crm_id
                )
            ]
        )
        return ticket

    @classmethod
    def get_ticket_documents(cls, erp_client, ticket_id):
        documents = erp_client.get_service_ticket_documents(ticket_id)
        return documents

    @classmethod
    def create_service_ticket_documents(cls, erp_client, ticket_id, document):
        erp_client.create_service_ticket_document(
            ticket_id, document["file"].name, document["file"].file
        )

    @classmethod
    def get_service_dashboard_metrics(cls, provider, customer_id=None):
        cache_key = CacheService.CACHE_KEYS.SERVICE_DASHBOARD.format(
            provider.id, customer_id
        )
        if CacheService.exists(cache_key) and isinstance(customer_id, int):
            return CacheService.get_data(cache_key)
        dashboard = {
            # "open_ps_ticket": cls.get_open_professional_services_tickets_count(
            #     provider, customer_id
            # ),
            "open_ms_ticket": cls.get_open_managed_services_tickets_count(
                provider, customer_id
            ),
            "open_projects": cls.get_open_projects_count(
                provider, customer_id
            ),
        }
        if isinstance(customer_id, int):
            CacheService.set_data(cache_key, dashboard)
        return dashboard

    @classmethod
    def get_lifecyclemanagement_dashboard(cls, provider, customer_id=None):
        devices = DeviceService.get_device_list(
            provider_id=provider.id,
            customer_id=customer_id,
            erp_client=provider.erp_client,
            category_id=provider.get_all_device_categories(),
        )
        expiring_devices = list(
            filter(
                lambda device: device.get("contract_status")
                == str(DeviceManufacturerData.EXPIRING),
                devices,
            )
        )
        expired_devices = list(
            filter(
                lambda device: device.get("contract_status")
                == str(DeviceManufacturerData.EXPIRED),
                devices,
            )
        )
        na_devices = list(
            filter(
                lambda device: device.get("contract_status")
                == str(DeviceManufacturerData.NOT_AVAILABLE),
                devices,
            )
        )
        serials = {
            device.get("serial_number")
            for device in devices
            if device.get("serial_number") is not None
        }
        provider_id = provider.id

        eol_device_count = 0
        if serials:
            queryset = DeviceManufacturerData.objects.exclude(
                data__eox_data__isnull=True
            ).annotate(
                eol_date=KeyTextTransform(
                    "EOL_date", KeyTextTransform("eox_data", "data")
                )
            )
            current_date = timezone.now().date()
            casted_date = Cast("eol_date", output_field=DateField())
            conditions = [
                When(
                    (Q(eol_date__isnull=False) & ~Q(eol_date__iexact="")),
                    then=casted_date,
                )
            ]
            eol_device_count = len(
                queryset.annotate(
                    eol_date_casted=Case(
                        *conditions,
                        default=current_date,
                        output_field=DateField()
                    )
                ).filter(
                    provider_id=provider_id,
                    eol_date_casted__isnull=False,
                    eol_date_casted__lt=current_date,
                    device_serial_number__in=serials,
                )
            )
        expiring_device_count = len(expiring_devices)
        expired_device_count = len(expired_devices)
        active_device_count = len(devices)
        na_device_count = len(na_devices)

        dashboard = {
            "expiring_support": expiring_device_count,
            "expired_support": expired_device_count,
            "end_of_life": eol_device_count,
            "data_not_available": na_device_count,
            "active_devices": active_device_count,
            "supported_devices": active_device_count,
        }
        return dashboard

    @classmethod
    def get_service_ticket_statuses(cls, provider_id, status="closed"):
        provider = Provider.objects.get(id=provider_id)
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        boards = managed_services_configs.get("board")
        if status == "closed":
            statuses = managed_services_configs.get("closed_status")
        else:
            statuses = managed_services_configs.get("open_status")
        if not (boards and statuses):
            raise InvalidConfigurations(
                "Managed Services Board not Configured Properly."
            )
        boards = boards[0]
        return provider.erp_client.get_board_statuses(boards, statuses)

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.OPEN_SERVICE_REQUEST_TICKETS,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    @caching(
        cache_key=CacheService.CACHE_KEYS.CLOSED_SERVICE_REQUEST_TICKETS,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    @caching(
        cache_key=CacheService.CACHE_KEYS.SERVICE_DASHBOARD,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def close_service_ticket(cls, provider_id, customer_id, ticket_id):
        provider = Provider.objects.get(id=provider_id)
        (
            managed_services_configs,
            _,
            _,
        ) = cls.get_provider_services_board_configs(provider)
        status_id = managed_services_configs.get("ticket_closed_status")
        if not status_id:
            raise InvalidConfigurations(
                "Managed Services Board not Configured Properly for Close Ticket."
            )
        if isinstance(status_id, list):
            status_id = status_id[0]
        provider = Provider.objects.get(id=provider_id)
        return provider.erp_client.update_service_ticket_status(
            ticket_id, status_id
        )

    @classmethod
    def list_all_closed_service_tickets(cls, provider, customer_id, filters):
        """
        Return all closed service tickets, irrespective of the board
        """
        tickets = provider.erp_client.get_closed_service_tickets(
            customer_id, filters
        )
        return tickets
