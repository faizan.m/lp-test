#!/bin/bash

export $(xargs -0 -a "/proc/1/environ")
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export BUCKET=$AWS_STORAGE_BUCKET_NAME
export HOSTNAME=$POSTGRES_HOST
export PORT=$POSTGRES_PORT
echo "DB backup Start on-$(date +"%d_%m_%Y")"
export PASSWORD=$POSTGRES_PASSWORD
export USER=$POSTGRES_USER
export DB_NAME=$POSTGRES_DB
export FOLDER_NAME=$BACKUP_FOLDER_NAME

PGPASSWORD=$PASSWORD pg_dump -h $HOSTNAME -p $PORT -U $USER -Fc --file=$FOLDER_NAME-$(date +"%d_%m_%Y").sql $DB_NAME

gzip $FOLDER_NAME-$(date +"%d_%m_%Y").sql

DATE_VALUE=$(date +"%d")
DATE_ARRAY=(01 10 20 30)
if [[ " ${DATE_ARRAY[*]} " =~ " $DATE_VALUE " ]]; then
  S3_KEY=$BUCKET/permanent_backups/$FOLDER_NAME-$(date +"%d_%m_%Y").sql.gz
else
  S3_KEY=$BUCKET/backups/$FOLDER_NAME-$(date +"%d_%m_%Y").sql.gz
fi

/usr/bin/aws s3 cp $FOLDER_NAME-$(date +"%d_%m_%Y").sql.gz s3://$S3_KEY --sse AES256
echo "DB backup successfully on-$(date +"%d_%m_%Y")"
rm -f $FOLDER_NAME-$(date +"%d_%m_%Y").sql.gz
