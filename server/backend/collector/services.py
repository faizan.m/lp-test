import datetime
from typing import Dict, List, Union

from dateutil import parser
from django.conf import settings
from django.contrib.postgres.fields.jsonb import KeyTransform
from django.db.models import QuerySet

from accounts.models import Provider
from collector.models import (
    CollectorServiceIntegration,
    CollectorServiceType,
    CustomerCollector,
    NCMCiscoCDPData,
)
from config_compliance.services import ComplianceService
from core.integrations.utils import prepare_response
from core.models import DeviceInfo
from core.tasks.services import TaskService
from core.utils import (
    convert_date_object_to_date_string,
    convert_date_string_format,
)

# Get an instance of a logger
from utils import dict_merge, get_app_logger

logger = get_app_logger(__name__)


class SolarwindsDeviceConfigService:
    """
    Service class for Device Configuration coming from Solarwinds
    """

    @classmethod
    def update_device_config(
        cls, provider: Provider, collector_id: int, query_meta: Dict
    ):
        """
        Update Device Configuration coming from Solarwinds in Device Info
        """
        configurations: List[Dict[str, str]] = prepare_response(
            query_meta.get("data", []), query_meta.get("field_mapping", {})
        )
        config_by_node_ids: Dict[str, Dict[str, str]] = {
            config.get("node_id"): config for config in configurations
        }
        diff_found: bool = False
        for node_id, config_data in config_by_node_ids.items():
            queryset: Union[
                QuerySet, List[DeviceInfo]
            ] = DeviceInfo.objects.annotate(
                is_managed=KeyTransform(
                    "is_managed",
                    KeyTransform(settings.SOLARWINDS, "monitoring_data"),
                ),
                node_id=KeyTransform(
                    "node_id",
                    KeyTransform(settings.SOLARWINDS, "monitoring_meta_data"),
                ),
            ).filter(
                is_managed=True, node_id__isnull=False, provider=provider
            )
            try:
                records: DeviceInfo = queryset.filter(node_id=node_id)
                for record in records:
                    config_meta_data: Dict[str, str] = {
                        "config_id": config_data.get("config_id"),
                        "download_time": config_data.get("download_time"),
                        "config_title": config_data.get("config_title"),
                        "collector_id": collector_id,
                    }
                    config: Dict[str, str] = {
                        "config_last_synced_date": convert_date_object_to_date_string(
                            datetime.datetime.now(), settings.ISO_FORMAT
                        ),
                        "config": config_data.get("config"),
                        "config_last_updated_on": convert_date_object_to_date_string(
                            parser.parse(
                                config_data.get("config_last_updated_on")
                            ),
                            settings.ISO_FORMAT,
                        ),
                    }
                    existing_config: str = record.configuration.get(
                        settings.SOLARWINDS, {}
                    ).get("config", "")
                    new_config: str = config_data.get("config", "")
                    if new_config != existing_config:
                        diff_found = True
                    record.configuration.update({settings.SOLARWINDS: config})
                    record.config_meta_data.update(
                        {settings.SOLARWINDS: config_meta_data}
                    )
                    record.save()
                    logger.info(
                        f"Configuration data updated for Provider {provider} "
                        f"Serial Number {DeviceInfo.serial_number}"
                    )
                if diff_found:
                    ComplianceService.refresh_compliance_stats_data(
                        provider.id
                    )
            except DeviceInfo.DoesNotExist:
                logger.info(
                    f"No DeviceInfo record for Provider {provider} Node Id: {node_id}"
                )


class SolarwindsService:
    """
    Service class for Solarwinds integration
    """

    @classmethod
    def _get_monitoring_data(cls, data):
        monitoring_data = dict(
            os_version=data.get("os_version"),
            host_name=data.get("host_name"),
            device_name=data.get("device_name"),
            up_time=data.get("up_time"),
            sys_name=data.get("sys_name"),
            ios_image=data.get("ios_image"),
            is_managed=True,
            updated_on=convert_date_object_to_date_string(
                datetime.datetime.utcnow(), settings.ISO_FORMAT
            ),
        )
        return monitoring_data

    @classmethod
    def _get_monitoring_meta_data(cls, data):
        config_data = dict(
            node_id=data.get("node_id"), collector_id=data.get("collector_id")
        )
        return config_data

    @classmethod
    def update_device_info(
        cls,
        provider,
        category_id,
        device_infos_to_update,
        device_infos_to_create,
        devices_data,
        collector_id,
    ):
        """
        This function updates `DeviceInfo` with the corresponding monitoring_data.
        """

        def _update_device_info_record(device_info_obj, device):
            existing_monitoring_data = device_info_obj.monitoring_data or {}
            new_monitoring_data = cls._get_monitoring_data(device)
            monitoring_data_updates = {
                f"{settings.SOLARWINDS}": new_monitoring_data,
                "last_updated_by": settings.SOLARWINDS,
                "is_managed": True,
            }
            existing_monitoring_data.update(monitoring_data_updates)
            existing_monitoring_meta_data = (
                device_info_obj.monitoring_meta_data or {}
            )
            new_monitoring_meta_data = cls._get_monitoring_meta_data(device)
            existing_monitoring_meta_data.get(settings.SOLARWINDS, {}).update(
                new_monitoring_meta_data
            )
            device_info_obj.monitoring_data = existing_monitoring_data
            device_info_obj.monitoring_meta_data = (
                existing_monitoring_meta_data
            )
            device_info_obj.save()
            logger.info(
                f"Updated Solarwinds Data for Serial Number: {device_info_obj.serial_number}"
            )

        def _create_device_info_record(serial_number, device):
            monitoring_data_from_source = cls._get_monitoring_data(device)
            monitoring_data = {
                f"{settings.SOLARWINDS}": monitoring_data_from_source,
                "created_by": settings.SOLARWINDS,
                "last_updated_by": settings.SOLARWINDS,
                "is_managed": True,
            }
            monitoring_meta_data = {
                f"{settings.SOLARWINDS}": cls._get_monitoring_meta_data(device)
            }
            DeviceInfo.objects.create(
                provider=provider,
                device_crm_id=device_infos_to_create.get(serial_number)
                or device_infos_to_update.get(serial_number),
                monitoring_data=monitoring_data,
                serial_number=serial_number,
                category_id=category_id,
                monitoring_meta_data=monitoring_meta_data,
            )
            logger.info(
                f"Created Device Info object from Solarwinds for Serial Number: {serial_number}"
            )

        for device in devices_data:
            serial_number = device.get("serial_number")
            device.update({"collector_id": collector_id})
            if serial_number in device_infos_to_update:
                device_info_obj = (
                    DeviceInfo.objects.select_for_update()
                    .filter(
                        provider=provider,
                        category_id=category_id,
                        device_crm_id=device_infos_to_update.get(
                            serial_number
                        ),
                    )
                    .first()
                )
                if not device_info_obj:
                    _create_device_info_record(serial_number, device)
                else:
                    _update_device_info_record(device_info_obj, device)

            elif serial_number in device_infos_to_create:
                _create_device_info_record(serial_number, device)

        # Mark devices as not managed
        managed_devices_id = list(
            {**device_infos_to_create, **device_infos_to_update}.values()
        )
        devices_not_managed = (
            DeviceInfo.objects.select_for_update()
            .filter(category_id=category_id)
            .exclude(device_crm_id__in=managed_devices_id)
        )

        for _device in devices_not_managed:
            if _device.monitoring_data:
                _device.monitoring_data.get(settings.SOLARWINDS, {})[
                    "is_managed"
                ] = False
                _device.save()

    @classmethod
    def handle_device_data(
        cls, provider, customer, query_meta, manufacturer_id, collector_id
    ):
        """
        Parse the solarwinds data coming from collector and take actions
        Args:
            provider: Provider Object
            customer: Customer Object
            query_meta: Payload from collector
            manufacturer_id: Manufacturer Id
            collector_id: Collector ID
        Returns: None

        """
        category_id = provider.get_device_category_id_by_manufacturer(
            manufacturer_id
        )
        devices = query_meta.get("data", [])
        if not devices:
            return
        devices = prepare_response(
            devices, query_meta.get("field_mapping", {})
        )
        serial_numbers = [x.get("serial_number") for x in devices]
        # List of devices from CW by company ids.
        cw_devices = provider.erp_client.get_devices(
            customer_id=customer.crm_id, category_id=category_id
        )
        existing_device_serial_numbers = {
            device.get("serial_number"): device.get("id")
            for device in cw_devices
        }
        devices_to_create = list(
            filter(
                lambda device: device.get("serial_number")
                not in existing_device_serial_numbers,
                devices,
            )
        )
        devices_to_update = list(
            filter(
                lambda device: device.get("serial_number")
                in existing_device_serial_numbers,
                devices,
            )
        )
        # update crm id in the devices to be updated
        [
            device.update(
                {
                    "id": existing_device_serial_numbers.get(
                        device.get("serial_number")
                    )
                }
            )
            for device in devices_to_update
        ]
        # update manufacturer_id in devices to create
        [
            device.update({"manufacturer_id": manufacturer_id})
            for device in devices_to_create
        ]
        updated_device_ids = {}
        created_device_ids = {}
        # update device name in connectwise
        if devices_to_update:
            result = provider.erp_client.update_device_names(devices_to_update)
            for _cw_res in result:
                if _cw_res.status_code == 200 and _cw_res.json():
                    message = _cw_res.json()
                    _device_id = message.get("id")
                    _serial_number = message.get("serialNumber")
                    updated_device_ids[_serial_number] = _device_id

        # create new devices in connectwise
        if devices_to_create:
            batch_requests_items = (
                provider.erp_client.batch_create_device_data(
                    category_id, {customer.crm_id: devices_to_create}
                )
            )
            result = provider.erp_client.batch_create_devices(
                batch_requests_items
            )
            for _cw_res in result:
                if _cw_res.status_code == 201 and _cw_res.json():
                    message = _cw_res.json()
                    _device_id = message.get("id")
                    _serial_number = message.get("serialNumber")
                    created_device_ids[_serial_number] = _device_id

        # update Device Info table with the latest devices
        cls.update_device_info(
            provider,
            category_id,
            updated_device_ids,
            created_device_ids,
            devices,
            collector_id,
        )
        if devices_to_create or devices_to_update:
            TaskService.sync_device_data(
                provider.id, manufacturer_id, serial_numbers
            )
            # Update Device Count
            collector = CustomerCollector.objects.select_for_update().get(
                id=collector_id
            )
            payload = {
                settings.SOLARWINDS: {
                    "created_devices_count": len(devices_to_create),
                    "updated_devices_count": len(devices_to_update),
                    "serial_numbers": serial_numbers,
                }
            }
            updated_meta_data = dict_merge(collector.other_metadata, payload)
            collector.other_metadata = updated_meta_data
            collector.save()

    @classmethod
    def handle_cisco_cdp_data(cls, provider, collector_id, query_meta):
        """
        Parse the NCM Cisco CDP data coming from collector and store in Acela DB
        Args:
            provider: Provider Object
            collector_id: Collector Id
            query_meta: Payload sent by collector

        Returns: None

        """
        records = query_meta.get("data")
        for record in records:
            node_id = record.pop("NodeID", None)
            if node_id is None:
                continue
            entity_id = record.pop("EntityID", None)
            _, created = NCMCiscoCDPData.objects.update_or_create(
                {
                    "data": record,
                    "collector_id": collector_id,
                    "node_id": node_id,
                },
                entity_id=entity_id,
                provider=provider,
            )
            if created:
                logger.debug(
                    f"New NCM_Cisco_CDP_Data record created with entity id: {entity_id}"
                )
            else:
                logger.debug(
                    f"NCM_Cisco_CDP_Data record updated with entity id: {entity_id}"
                )

    @classmethod
    def handle_solarwinds_collector_data(
        cls, provider, customer, data, collector_id
    ):
        """
        Business logic to parse and store the solarwinds collector data
        Args:
            data: collector data

        Returns: None
        """
        data = data or {}
        NCM_Cisco_CDP_query_Id = "Cisco_2"
        Config_query_Id = "Cisco_3"
        for manufacturer_id, mnf_config in data.get(
            "device_mappings", {}
        ).items():
            queries = mnf_config.get("queries")
            for query_id, query_meta in queries.items():
                if query_meta.get("is_device_query"):
                    cls.handle_device_data(
                        provider,
                        customer,
                        query_meta,
                        manufacturer_id,
                        collector_id,
                    )
                elif query_id == NCM_Cisco_CDP_query_Id:
                    cls.handle_cisco_cdp_data(
                        provider, collector_id, query_meta
                    )
                elif query_id == Config_query_Id:
                    SolarwindsDeviceConfigService.update_device_config(
                        provider, collector_id, query_meta
                    )


class HostInfoService:
    @classmethod
    def _get_host_info(cls, payload):
        host_metadata = dict(
            hostname=payload.get("hostname"),
            architecture=payload.get("architecture"),
            ip_address=payload.get("ipAddress"),
            platform=payload.get("platform"),
            os_type=payload.get("osType"),
            uptime=payload.get("uptime"),
        )
        return host_metadata

    @classmethod
    def update_host_info(cls, provider, customer, data, collector_id):
        """
        Update collector host info
        Args:
            provider: Provider Object
            customer: Customer Object
            data: Host Info payload
            collector_id: Collector Id
        """
        try:
            collector = CustomerCollector.objects.select_for_update().get(
                id=collector_id
            )
            host_metadata = cls._get_host_info(data)
            collector.host_metadata = host_metadata
            collector.save()
        except CustomerCollector.DoesNotExist:
            logger.debug(
                f"No Customer Collector found with ID: {collector_id}"
            )


class CollectorService:
    @classmethod
    def parse_collector_data(
        cls, provider, customer, serializer_data, collector_id
    ):
        """
        Read the data sent by collector and call the appropriate handler for the service
        Args:
            customer: Customer Object
            provider: Provider Object
            serializer_data: Collector Payload
            collector_id: CustomerCollector Id

        Returns: None

        """

        service_data_handlers = {
            CollectorServiceType.SOLARWINDS: SolarwindsService.handle_solarwinds_collector_data,
            CollectorServiceType.HOST_INFO_SERVICE: HostInfoService.update_host_info,
        }
        handler_method = service_data_handlers.get(
            serializer_data.get("service_name")
        )
        data = serializer_data.get("data", {})
        if handler_method:
            handler_method(provider, customer, data, collector_id)

    @classmethod
    def validate_collector_token(cls, token):
        """
        Validate if the token belongs to a active or pending collector
        Args:
            token: token

        Returns: True or False

        """
        is_valid = False
        try:
            token = CustomerCollector.objects.get(
                token=token, status__in=[CustomerCollector.PENDING]
            )
            token.status = CustomerCollector.VALIDATED
            token.save()
            is_valid = True
        except CustomerCollector.DoesNotExist:
            return is_valid
        return is_valid

    @classmethod
    def create_service_integration(cls, provider, collector, integration_type):
        """
        Create Collector service Integration with default settings if exists
        Args:
            provider: Provider
            collector:
            integration_type:
        """
        meta_config = {}
        if integration_type.name == CollectorServiceType.SOLARWINDS:
            default_device_mapping = provider.collector_settings.settings.get(
                "solarwinds", {}
            ).get("device_mappings")
            if default_device_mapping:
                integration_meta_config = integration_type.meta_config
                for _, mapping in default_device_mapping.items():
                    if mapping["solarwinds_vendor"].lower() == "cisco":
                        mapping["queries"] = integration_meta_config["Cisco"][
                            "queries"
                        ]
                meta_config = dict(device_mappings=default_device_mapping)
        CollectorServiceIntegration.objects.create(
            collector=collector,
            integration_type=integration_type,
            meta_config=meta_config,
        )
