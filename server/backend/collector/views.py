from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderOwner
from collector.models import (
    CollectorQuery,
    CollectorServiceIntegration,
    CollectorSettings,
    CustomerCollector,
    CollectorServiceType,
)
from collector.permissions import IsCollectorServiceAllowed
from collector.serializer import (
    CollectorHeartBeatAPISerializer,
    CollectorQueryListWebhookSerializer,
    CollectorQuerySerializer,
    CollectorQueryUpdateSerializer,
    CollectorServiceIntegrationCreateSerializer,
    CollectorSettingsSerializer,
    CollectorUninstallSerializer,
    CustomerCollectorCreateSerializer,
    CustomerCollectorListSerializer,
    CustomerCollectorRetrieveUpdateSerializer,
    ValidateCollectorTokenSerializer,
    CollectorServiceIntegrationSerializer,
    CollectorDataWebhookSerializer,
    CollectorServiceIntegrationUpdate,
    CollectorServiceTypeSerializer,
)
from collector.services import CollectorService
from collector.utils import generate_token
from custom_storages import get_storage_class
from utils import GetCustomerMixin, dict_merge, get_app_logger

logger = get_app_logger(__name__)


class TokenView(APIView):
    def get_permissions(self):
        if self.request.method == "GET":
            return [IsAuthenticated()]
        return []

    def get(self, request, *args, **kwargs):
        token = {"token": generate_token()}
        return Response(data=token)

    def post(self, request, *args, **kwargs):
        serializer = ValidateCollectorTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        is_valid = CollectorService.validate_collector_token(
            serializer.validated_data["token"]
        )
        data = dict(is_valid=is_valid)
        return Response(data, status=status.HTTP_200_OK)


class CustomerCollectorListCreateAPIView(
    GetCustomerMixin, generics.ListCreateAPIView
):
    permission_classes = (IsAuthenticated, IsCollectorServiceAllowed)

    def get_queryset(self):
        customer = self._get_customer()
        queryset = (
            CustomerCollector.objects.select_related("customer")
            .prefetch_related("integrations", "integrations__integration_type")
            .filter(customer=customer, provider=self.request.provider)
            .order_by("-updated_on")
        )
        return queryset

    def perform_create(self, serializer):
        customer = self._get_customer()
        serializer.save(customer=customer, provider=self.request.provider)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CustomerCollectorListSerializer
        elif self.request.method == "POST":
            return CustomerCollectorCreateSerializer


class CustomerCollectorRetrieveUpdateDeleteAPIView(
    GetCustomerMixin, generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated, IsCollectorServiceAllowed)
    serializer_class = CustomerCollectorRetrieveUpdateSerializer

    def get_queryset(self):
        customer = self._get_customer()
        queryset = (
            CustomerCollector.objects.select_related("customer")
            .prefetch_related("integrations")
            .filter(customer=customer, provider=self.request.provider)
            .order_by("-updated_on")
        )
        return queryset


class CollectorServiceTypeListAPIView(generics.ListAPIView):
    serializer_class = CollectorServiceTypeSerializer
    queryset = CollectorServiceType.objects.all()
    pagination_class = None


class CollectorSettingsRetrieveUpdateAPIView(
    generics.RetrieveAPIView, generics.UpdateAPIView
):
    """
    Class based View to provide create API for CollectorSettings
    """

    serializer_class = CollectorSettingsSerializer
    permission_classes = (IsAuthenticated, IsProviderOwner)

    def get_object(self):
        try:
            return self.request.provider.collector_settings
        except CollectorSettings.DoesNotExist:
            return None

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


# Webhooks


class CollectorHeartBeatAPIView(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorHeartBeatAPISerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        collector = None
        try:
            collector = CustomerCollector.objects.select_for_update().get(
                token=serializer.validated_data["token"]
            )
        except CustomerCollector.DoesNotExist:
            raise ValidationError({"token": "Invalid Collector Token"})
        data = {"integrations": {}}
        if collector:
            data.update({"collector_name": collector.name})
            if collector.status == CustomerCollector.VALIDATED:
                collector.status = CustomerCollector.ACTIVE
            if serializer.validated_data.get("service_meta_data"):
                updated_meta_data = dict_merge(
                    collector.other_metadata,
                    serializer.validated_data.get("service_meta_data"),
                )
                collector.other_metadata = updated_meta_data
            if serializer.validated_data.get("collector_version"):
                collector.current_version = serializer.validated_data.get(
                    "collector_version"
                )
            collector.last_heart_beat = timezone.now()
            collector.save()
            service_integrations = CollectorServiceIntegration.objects.filter(
                collector=collector
            )
            service_integrations_serializer = (
                CollectorServiceIntegrationSerializer(
                    service_integrations, many=True
                )
            )
            # Prepare service config response for agent
            for integration in service_integrations_serializer.data:
                if integration.get("integration_completed"):
                    integration["meta_config"][
                        "acela_integration_id"
                    ] = integration["id"]
                    data["integrations"][
                        integration["integration_type"]
                    ] = integration
        return Response(data=data, status=status.HTTP_200_OK)


class CollectorServiceIntegrationCreateAPIView(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorServiceIntegrationCreateSerializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)
        collector = serializer.validated_data["token"]
        integration_type = serializer.validated_data["service_type"]
        provider = collector.provider
        CollectorService.create_service_integration(
            provider, collector, integration_type
        )
        return Response(status=status.HTTP_201_CREATED)


class CollectorServiceIntegrationUpdateAPIView(
    GetCustomerMixin, generics.RetrieveUpdateAPIView
):
    lookup_url_kwarg = "integration_id"
    serializer_class = CollectorServiceIntegrationUpdate
    # permission_classes = (IsAuthenticated, IsProviderOwner)

    def get_queryset(self):
        customer = self._get_customer()
        queryset = CollectorServiceIntegration.objects.filter(
            collector__provider=self.request.provider,
            collector__customer=customer,
            collector_id=self.kwargs.get("collector_id"),
        )
        return queryset


class CollectorDataWebhook(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorDataWebhookSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        collector = serializer.validated_data["token"]
        provider = collector.provider
        customer = collector.customer
        CollectorService.parse_collector_data(
            provider,
            customer,
            serializer.validated_data["service_data"],
            collector.id,
        )
        return Response(status=status.HTTP_200_OK)


class CollectorQueryListCreateAPIView(
    GetCustomerMixin, generics.ListCreateAPIView
):
    serializer_class = CollectorQuerySerializer
    queryset = CollectorQuery.objects.select_related("collector").all()

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        provider = self.request.provider
        customer = self._get_customer()
        collector = self.kwargs.get("collector_id")
        queryset = queryset.filter(
            collector__provider=provider,
            collector__customer=customer,
            collector_id=collector,
        ).order_by("-updated_on")
        return queryset

    def perform_create(self, serializer):
        collector_id = self.kwargs.get("collector_id")
        serializer.save(collector_id=collector_id)


class CollectorQueryRetrieveDeleteAPIView(
    GetCustomerMixin, generics.RetrieveDestroyAPIView
):
    serializer_class = CollectorQuerySerializer
    queryset = CollectorQuery.objects.select_related("collector").all()

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        provider = self.request.provider
        customer = self._get_customer()
        collector = self.kwargs.get("collector_id")
        queryset = queryset.filter(
            collector__provider=provider,
            collector__customer=customer,
            collector_id=collector,
        )
        return queryset


class CollectorQueryListWebhook(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorQueryListWebhookSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        collector = serializer.validated_data["token"]
        queryset = CollectorQuery.objects.filter(
            collector=collector, status=CollectorQuery.PENDING
        )
        result_serializer = CollectorQuerySerializer(queryset, many=True)
        data = result_serializer.data
        return Response(data=data, status=status.HTTP_200_OK)


class CollectorQueryUpdateWebhook(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorQueryUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        queries = {
            query["query_id"]: query["result"]
            for query in serializer.validated_data["queries"]
        }
        if queries:
            collector_query_objects = (
                CollectorQuery.objects.select_for_update().filter(
                    id__in=list(queries.keys())
                )
            )
            for collector_query in collector_query_objects:
                collector_query.result = queries.get(collector_query.id)
                collector_query.status = CollectorQuery.COMPLETED
                collector_query.save()
                logger.info(
                    f"Updated {collector_query} and status marked as {CollectorQuery.COMPLETED}"
                )
        return Response(status=status.HTTP_200_OK)


class UninstallCollectorAPIView(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = CollectorUninstallSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        collector = serializer.validated_data["token"]
        collector.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CollectorDownloadURLAPIView(APIView):
    def get(self, request, *args, **kwargs):
        storage = get_storage_class()
        file_path = f"collectors/{settings.COLLECTOR_APP_NAME}"
        download_url = storage.url(file_path)
        return Response(
            data=dict(download_url=download_url), status=status.HTTP_200_OK
        )
