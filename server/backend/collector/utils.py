import secrets

from django.conf import settings


def generate_token(length=None):
    """
    Generate hexadecimal tokens
    Args:
        length: Length oof token (Int)

    Returns: (String) Token

    """
    length = length or settings.COLLECTOR_TOKEN_LENGTH
    return secrets.token_hex(length)
