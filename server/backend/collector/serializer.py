from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from collector.models import (
    CollectorQuery,
    CollectorServiceIntegration,
    CollectorServiceType,
    CollectorSettings,
    CustomerCollector,
)
from utils import DynamicFieldsModelSerializer


class ValidateTokenMixin:
    def validate_token(self, token):
        try:
            collector = CustomerCollector.objects.get(
                token=token,
                status__in=[
                    CustomerCollector.VALIDATED,
                    CustomerCollector.ACTIVE,
                ],
            )
        except CustomerCollector.DoesNotExist:
            raise ValidationError("Invalid Collector Token")

        return collector


class ValidateCollectorTokenSerializer(serializers.Serializer):
    token = serializers.CharField()


class CollectorHeartBeatAPISerializer(serializers.Serializer):
    token = serializers.CharField()
    service_meta_data = serializers.JSONField(allow_null=True, required=False)
    collector_version = serializers.CharField(required=False)


class CustomerCollectorCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerCollector
        exclude = ("customer", "provider")


class CustomerCollectorUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerCollector
        fields = ()


class CollectorServiceTypeSerializer(
    DynamicFieldsModelSerializer, serializers.ModelSerializer
):
    class Meta:
        model = CollectorServiceType
        fields = ("name", "meta_config")


class CollectorServiceIntegrationCreateSerializer(
    ValidateTokenMixin, serializers.Serializer
):
    token = serializers.CharField()
    service_type = serializers.CharField()

    def validate_service_type(self, service_type):
        try:
            service_type = CollectorServiceType.objects.get(name=service_type)
        except CollectorServiceType.DoesNotExist:
            raise ValidationError("Invalid Service Type")
        return service_type

    def validate(self, attrs):
        try:
            CollectorServiceIntegration.objects.get(
                collector=attrs.get("token"),
                integration_type=attrs.get("service_type"),
            )
            raise ValidationError("Service Already Integrated")
        except CollectorServiceIntegration.DoesNotExist:
            pass
        return attrs


class CollectorServiceIntegrationUpdate(serializers.ModelSerializer):
    collector = serializers.StringRelatedField()
    integration_type = serializers.StringRelatedField()

    class Meta:
        model = CollectorServiceIntegration
        fields = "__all__"


class CollectorServiceIntegrationSerializer(serializers.ModelSerializer):
    collector = serializers.StringRelatedField()
    integration_type = serializers.StringRelatedField()

    class Meta:
        model = CollectorServiceIntegration
        fields = (
            "id",
            "collector",
            "integration_type",
            "meta_config",
            "integration_completed",
            "created_on",
            "updated_on",
        )


class CustomerCollectorListSerializer(serializers.ModelSerializer):
    customer = serializers.StringRelatedField()
    integrations = CollectorServiceIntegrationSerializer(many=True)
    heartbeat_status = serializers.ReadOnlyField()

    class Meta:
        model = CustomerCollector
        fields = "__all__"


class CustomerCollectorRetrieveUpdateSerializer(serializers.ModelSerializer):
    customer = serializers.StringRelatedField()
    integrations = CollectorServiceIntegrationSerializer(many=True)
    heartbeat_status = serializers.ReadOnlyField()

    class Meta:
        model = CustomerCollector
        fields = "__all__"


class _CollectorDataSerializer(serializers.Serializer):
    service_name = serializers.CharField()
    data = serializers.JSONField()


class CollectorDataWebhookSerializer(
    ValidateTokenMixin, serializers.Serializer
):
    token = serializers.CharField()
    service_data = _CollectorDataSerializer()


class CollectorSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectorSettings
        exclude = ("provider",)


class CollectorQuerySerializer(serializers.ModelSerializer):
    collector = serializers.StringRelatedField()

    class Meta:
        model = CollectorQuery
        fields = "__all__"


class _CollectorQueryResultSerializer(serializers.Serializer):
    query_id = serializers.IntegerField()
    result = serializers.JSONField()


class CollectorQueryUpdateSerializer(
    ValidateTokenMixin, serializers.Serializer
):
    token = serializers.CharField()
    queries = _CollectorQueryResultSerializer(many=True)


class CollectorQueryListWebhookSerializer(
    ValidateTokenMixin, serializers.Serializer
):
    token = serializers.CharField()


class CollectorUninstallSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate_token(self, token):
        try:
            collector = CustomerCollector.objects.get(token=token)
        except CustomerCollector.DoesNotExist:
            raise ValidationError("Invalid Collector Token")

        return collector
