import datetime
import json
import os

from cached_property import cached_property
from django.db.models import JSONField
from django.db import models

# Create your models here.
from accounts.model_utils import TimeStampedModel
from custom_storages import get_storage_class
from utils import get_app_logger

logger = get_app_logger(__name__)


def file_upload_url(instance, filename):
    file_path = f"collectors/{filename}"
    return file_path


class CustomerCollector(TimeStampedModel):
    PENDING = "pending"
    VALIDATED = "validated"
    ACTIVE = "active"
    INACTIVE = "inactive"
    STATUSES = (
        (PENDING, PENDING),
        (ACTIVE, ACTIVE),
        (INACTIVE, INACTIVE),
        (VALIDATED, VALIDATED),
    )
    name = models.CharField(max_length=200, null=True, blank=True)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    current_version = models.CharField(max_length=30, null=True, blank=True)
    customer = models.ForeignKey(
        "accounts.Customer",
        on_delete=models.CASCADE,
        related_name="collectors",
    )
    token = models.CharField(max_length=300, unique=True)
    status = models.CharField(choices=STATUSES, max_length=30, default=PENDING)
    last_heart_beat = models.DateTimeField(null=True, blank=True)
    host_metadata = JSONField(default=dict)
    other_metadata = JSONField(default=dict)

    def __str__(self):
        return str(self.name)

    @staticmethod
    def _get_heartbeat_status(
        current_time: datetime.datetime, last_heartbeat: datetime.datetime
    ) -> str:
        NOT_RESPONDING: str = "Not Responding"
        INACTIVE: str = "Inactive"
        ACTIVE: str = "Active"
        NOT_AVAILABLE: str = "Not Available"

        status = ACTIVE
        if last_heartbeat is None:
            status = NOT_AVAILABLE
        else:
            delta_mins: int = int(
                (current_time - last_heartbeat).total_seconds() // 60
            )
            ONE_DAY_IN_MINS: int = 24 * 60
            TWO_DAY_IN_MINS: int = ONE_DAY_IN_MINS * 2
            if delta_mins > TWO_DAY_IN_MINS:
                status = INACTIVE
            elif delta_mins > ONE_DAY_IN_MINS:
                status = NOT_RESPONDING
        return status

    @cached_property
    def heartbeat_status(self):
        current_time = datetime.datetime.now(datetime.timezone.utc)
        status = self._get_heartbeat_status(current_time, self.last_heart_beat)
        return status

    class Meta:
        indexes = [models.Index(fields=["token"])]


class CollectorServiceType(TimeStampedModel):
    """
    Model for Service Types
    """

    SOLARWINDS = "solarwinds"
    HOST_INFO_SERVICE = "HostInfoService"
    name = models.CharField(max_length=100)

    @cached_property
    def meta_config(self):
        configs_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "integration_configs",
            self.name.lower(),
            "meta_config.json",
        )
        try:
            with open(configs_file) as config:
                json_config = json.load(config)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        return json_config

    def __str__(self):
        return f"{self.name}"


class CollectorServiceIntegration(TimeStampedModel):
    """
    Model for different types of Service Integration for a Collector.
    """

    collector = models.ForeignKey(
        "collector.CustomerCollector",
        related_name="integrations",
        on_delete=models.CASCADE,
    )
    integration_type = models.ForeignKey(
        "collector.CollectorServiceType",
        related_name="providers",
        on_delete=models.CASCADE,
    )
    meta_config = JSONField(null=True, blank=True)

    def __str__(self):
        return f"{self.collector} {self.integration_type}"

    @property
    def type(self):
        return self.integration_type.name

    @property
    def integration_completed(self):
        return bool(self.meta_config)

    class Meta:
        unique_together = ("collector", "integration_type")


class NCMCiscoCDPData(TimeStampedModel):
    entity_id = models.IntegerField()
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    node_id = models.CharField(max_length=300)
    collector = models.ForeignKey(
        "collector.CustomerCollector", on_delete=models.SET_NULL, null=True
    )
    data = JSONField(default=dict)

    def __str__(self):
        return str(self.entity_id)

    class Meta:
        unique_together = ("entity_id", "provider")


class CollectorSettings(TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="collector_settings",
    )
    settings = JSONField(default=dict)


class CollectorQuery(TimeStampedModel):
    PENDING = "Pending"
    COMPLETED = "Completed"

    STATUS_CHOICES = ((PENDING, PENDING), (COMPLETED, COMPLETED))
    collector = models.ForeignKey(
        "collector.CustomerCollector",
        related_name="queries",
        on_delete=models.CASCADE,
    )
    description = models.CharField(max_length=100, null=True, blank=True)
    payload = JSONField(default=dict)
    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default=PENDING
    )
    result = JSONField(default=dict)

    def __str__(self):
        return f"Collector Query: {self.id}"
