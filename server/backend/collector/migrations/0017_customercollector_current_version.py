# Generated by Django 2.0.5 on 2020-05-18 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collector', '0016_customercollector_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='customercollector',
            name='current_version',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
