from rest_framework.permissions import BasePermission


class IsCollectorServiceAllowed(BasePermission):
    def has_permission(self, request, view):
        if request.provider and not request.customer:
            return True
        return request.customer.collector_service_allowed
