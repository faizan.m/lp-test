def rename_data_keys(data, name_mapping):
    """
    Map data keys to the header keys for easy conversion to csv
    """
    updated_data = []
    for _data in data:
        updated_data.append(
            {
                name_mapping.get(key): value
                for key, value in _data.items()
                if key in name_mapping.keys()
            }
        )
    return updated_data
