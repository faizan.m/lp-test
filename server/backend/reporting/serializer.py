from rest_framework import serializers


class CustomerAnalysisReportSerializer(serializers.Serializer):
    report_type = serializers.CharField()
    report_sub_type = serializers.CharField(required=False)
    start_date = serializers.DateTimeField(required=False)
    end_date = serializers.DateTimeField(required=False)


class SOWLinkedProjectsAuditReportSerializer(serializers.Serializer):
    project_ids = serializers.ListField(required=True)
    start_date = serializers.DateTimeField(required=False)
    end_date = serializers.DateTimeField(required=False)
