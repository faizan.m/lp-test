import csv
import datetime
import os
import tempfile

import pandas
from box import Box
from django.conf import settings

from accounts.models import Customer
from caching.service import caching, CacheService
from core.exceptions import DeviceMonitoringAPINotConfigured, NotConfigured
from core.integrations.erp.connectwise.connectwise import (
    CWConditions,
    Condition,
    Operators,
)
from core.services import LogicMonitorIntegrationService
from core.utils import convert_date_string_to_date_object, generate_random_id
from document.services import QuoteService
from reporting.utils import rename_data_keys
from service_requests.services import ServiceRequestService
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)

REPORT_FORMAT = Box(csv="csv", pdf="pdf")


class ServiceRequestReport:
    """
    Service layer to handle Service Requests related reports
    """

    TICKET_STATUSES = Box(closed="closed", open="open")

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SERVICE_TICKET_LISTING,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def _get_service_tickets_report_data(
        cls,
        provider,
        customer_crm_id,
        start_date=None,
        end_date=None,
        ticket_status=TICKET_STATUSES.closed,
    ):
        """
        Fetch service tickets data for the use of reporting
        :param provider: Provider object
        :param customer_crm_id: Customer CRM ID.
        :param ticket_status: Filter ticket by the status.
        :param end_date: End date filter
        :param start_date: Start date filter
        """
        tickets = []
        filters = []
        if start_date and end_date:
            start_date = datetime.datetime.strftime(
                start_date, settings.ISO_FORMAT
            )
            end_date = datetime.datetime.strftime(
                end_date, settings.ISO_FORMAT
            )
            filters = [
                CWConditions(
                    datetime_conditions=[
                        Condition(
                            "closedDate", Operators.GREATER_THEN, start_date
                        ),
                        Condition(
                            "closedDate", Operators.LESS_THEN_EQUALS, end_date
                        ),
                    ]
                )
            ]
        if ticket_status == ServiceRequestReport.TICKET_STATUSES.closed:
            tickets = ServiceRequestService.list_all_closed_service_tickets(
                provider, customer_crm_id, filters
            )
        elif ticket_status == ServiceRequestReport.TICKET_STATUSES.open:
            tickets = (
                ServiceRequestService.list_managed_service_board_open_tickets(
                    provider.id, customer_crm_id
                )
            )
        return tickets

    @classmethod
    def export_service_ticket_data(
        cls,
        provider,
        customer_crm_id,
        start_date=None,
        end_date=None,
        ticket_status=TICKET_STATUSES.closed,
    ):
        """
        Export Service Tickets data as a report in csv/pdf format.
        This will give per customer data.

        :param provider: Provider object
        :param customer_crm_id: Customer CRM ID.
        :param ticket_status: Filter ticket by the status.
        :param report_format: Type of report to be generated.
        :param end_date:
        :param start_date:
        """
        service_tickets = cls._get_service_tickets_report_data(
            provider, customer_crm_id, start_date, end_date, ticket_status
        )
        service_tickets = cls._add_derived_data(service_tickets)
        headers = [
            "Ticket #",
            "Date Entered",
            "Date Closed",
            "Days Open",
            "Summary",
            "Board Name",
            "Service Type",
            "Service Sub Type",
        ]
        data_keys = [
            "id",
            "created_on",
            "closed_on",
            "days_open",
            "summary",
            "board",
            "service_type",
            "service_subtype",
        ]
        rename_mapping = dict(zip(data_keys, headers))
        service_tickets = rename_data_keys(service_tickets, rename_mapping)
        result = Box(data=service_tickets, headers=headers)
        return result

    @classmethod
    def get_ticket_by_service_type_summary_report(
        cls,
        provider,
        customer_crm_id,
        start_date=None,
        end_date=None,
        ticket_status=TICKET_STATUSES.closed,
    ):
        """
        Prepares Service Ticket summary report
        """
        service_tickets = cls._get_service_tickets_report_data(
            provider, customer_crm_id, start_date, end_date, ticket_status
        )
        tickets_df = pandas.DataFrame(service_tickets)
        if tickets_df.empty:
            report_data, headers = [], []
        else:
            res = tickets_df.groupby("service_type").count()
            res = res.sort_values(by=["id"], ascending=False)["id"].to_dict()
            report_data = [
                {"Type": key, "Count": value} for key, value in res.items()
            ]
            headers = ["Type", "Count"]
        result = Box(data=report_data, headers=headers)
        return result

    @classmethod
    def _add_derived_data(cls, tickets):
        """
        Add fields derived from the data
        :param tickets: list of tickets data
        :return: list of tickets data
        """
        for ticket in tickets:
            created_on = datetime.datetime.strptime(
                ticket.get("created_on"), settings.ISO_FORMAT
            )
            closed_on = datetime.datetime.strptime(
                ticket.get("closed_on"), settings.ISO_FORMAT
            )
            days_open = closed_on - created_on
            ticket["days_open"] = days_open
        return tickets


class OpportunityReportService:
    """
    Service class for Opportunity Reports
    """

    @classmethod
    def get_opportunity_report_data(cls, provider, customer_crm_id):
        """
        Fetch opportunity data for opportunity reports
        """
        customer = Customer.objects.get(
            crm_id=customer_crm_id, provider=provider
        )
        opportunities = QuoteService.get_quotes(
            provider.id, customer.id, open_only=True
        )
        return opportunities

    @classmethod
    def get_opportunity_report(
        cls, provider, customer_crm_id, *args, **kwargs
    ):
        """
        Fetches opportunity data and  renames the keys as per the headers.
        """
        opportunities = cls.get_opportunity_report_data(
            provider, customer_crm_id
        )
        headers = ["Status", "Stage", "Opportunity Name", "Date Created"]
        data_keys = ["status_name", "stage_name", "name", "created_on"]
        rename_mapping = dict(zip(data_keys, headers))
        opportunities = rename_data_keys(opportunities, rename_mapping)
        result = Box(data=opportunities, headers=headers)
        return result


class AlertReportService:
    """
    Service class for Alert Reports
    """

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.ALERTS_LISTING,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def get_alert_data(
        cls,
        provider,
        customer_crm_id,
        start_date,
        end_date,
        date_field="began",
    ):
        """
        Fetch data for alerts
        """
        # fetch group for the customer
        group_mapping = (
            LogicMonitorIntegrationService.get_customer_group_mapping(provider)
        )
        if not group_mapping.get(str(customer_crm_id)):
            return []
        alerts = LogicMonitorIntegrationService.get_alerts_by_group(
            provider, group_mapping.get(str(customer_crm_id))
        )
        if start_date and end_date:
            alerts = list(
                filter(
                    lambda alert: (
                        start_date.date()
                        < convert_date_string_to_date_object(
                            alert.get(date_field), settings.ISO_FORMAT
                        )
                        <= end_date.date()
                    ),
                    alerts,
                )
            )
        # filter alerts by date
        return alerts

    @classmethod
    def export_alerts_data(
        cls, provider, customer_crm_id, start_date=None, end_date=None
    ):
        """
        Fetch alerts data and prepare report
        """
        # fetch alerts data
        alerts = cls.get_alert_data(
            provider, customer_crm_id, start_date, end_date
        )
        headers = [
            "Device",
            "Datasource",
            "Instance",
            "Datapoint",
            "Thresholds",
            "Value(seconds)",
            "Began",
        ]
        data_keys = [
            "device",
            "datasource",
            "instance",
            "datapoint",
            "threshold",
            "value",
            "began",
        ]
        rename_mapping = dict(zip(data_keys, headers))
        alerts = rename_data_keys(alerts, rename_mapping)
        result = Box(data=alerts, headers=headers)
        return result

    @classmethod
    def _get_alerts_aggregated_data(cls, alerts):
        """
        Returns aggregated data for alerts
        """
        alerts_df = pandas.DataFrame(alerts)
        alerts_df["value"] = pandas.to_numeric(
            alerts_df["value"], errors="coerce"
        )
        alerts_df.fillna(0, inplace=True)
        alerts_df["not_escalated"] = alerts_df["value"].apply(
            lambda x: 1 if x < 600 else 0
        )
        alerts_df["escalated"] = alerts_df["value"].apply(
            lambda x: 1 if x >= 600 else 0
        )
        alerts_df = (
            alerts_df.groupby("device")
            .agg(
                {
                    "value": ["max", "sum"],
                    "device": ["count"],
                    "not_escalated": ["sum"],
                    "escalated": ["sum"],
                }
            )
            .reset_index()
        )
        alerts_df.columns = [
            "device",
            "longest_downtime",
            "total_hours_down",
            "alerts",
            "not_escalate",
            "escalate",
        ]
        alerts_df["total_hours_down"] = alerts_df["total_hours_down"]
        return alerts_df

    @classmethod
    def get_alert_count_summary_report(
        cls, provider, customer_crm_id, start_date=None, end_date=None
    ):
        """
        Alert count summary sorted by alert count
        """

        alerts = cls.get_alert_data(
            provider, customer_crm_id, start_date, end_date
        )
        if alerts:
            alerts_df = cls._get_alerts_aggregated_data(alerts)
            alerts_df = alerts_df.sort_values("alerts", ascending=False)
            headers = [
                "Device",
                "Alerts",
                "Longest Downtime",
                "Total Hours Down",
                "Not Escalate",
                "Escalate",
            ]
            data_keys = [
                "device",
                "alerts",
                "longest_downtime",
                "total_hours_down",
                "not_escalate",
                "escalate",
            ]
            alerts_summary = alerts_df.to_dict("records")
            rename_mapping = dict(zip(data_keys, headers))
            alerts_summary = rename_data_keys(alerts_summary, rename_mapping)
        else:
            alerts_summary, headers = [], []
        result = Box(data=alerts_summary, headers=headers)
        return result

    @classmethod
    def get_longest_individual_downtime_summary_report(
        cls, provider, customer_crm_id, start_date=None, end_date=None
    ):
        """
        Alert Summary by longest downtime
        """
        alerts = cls.get_alert_data(
            provider, customer_crm_id, start_date, end_date
        )
        if alerts:
            alerts_df = cls._get_alerts_aggregated_data(alerts)
            alerts_df = alerts_df.sort_values(
                "longest_downtime", ascending=False
            )
            headers = [
                "Device",
                "Alerts",
                "Longest Downtime",
                "Total Hours Down",
                "Not Escalate",
                "Escalate",
            ]
            data_keys = [
                "device",
                "alerts",
                "longest_downtime",
                "total_hours_down",
                "not_escalate",
                "escalate",
            ]
            alerts_summary = alerts_df.to_dict("records")
            rename_mapping = dict(zip(data_keys, headers))
            alerts_summary = rename_data_keys(alerts_summary, rename_mapping)
        else:
            alerts_summary, headers = [], []
        result = Box(data=alerts_summary, headers=headers)
        return result

    @classmethod
    def get_total_downtime_summary_report(
        cls, provider, customer_crm_id, start_date=None, end_date=None
    ):
        """
        Alert Summary by total downtime
        """
        alerts = cls.get_alert_data(
            provider, customer_crm_id, start_date, end_date
        )
        if alerts:
            alerts_df = cls._get_alerts_aggregated_data(alerts)
            alerts_df = alerts_df.sort_values(
                "total_hours_down", ascending=False
            )
            headers = [
                "Device",
                "Alerts",
                "Longest Downtime",
                "Total Hours Down",
                "Not Escalate",
                "Escalate",
            ]
            data_keys = [
                "device",
                "alerts",
                "longest_downtime",
                "total_hours_down",
                "not_escalate",
                "escalate",
            ]
            alerts_summary = alerts_df.to_dict("records")
            rename_mapping = dict(zip(data_keys, headers))
            alerts_summary = rename_data_keys(alerts_summary, rename_mapping)
        else:
            alerts_summary, headers = [], []
        result = Box(data=alerts_summary, headers=headers)
        return result


class CSVCreationService:
    @classmethod
    def create_csv(cls, data, headers, customer_name=None, date_range=None):
        """
        Prepares a CSV file from data. data is a list of dictionaries.
        :param data: Data to be written. This should be a list of dictionary with the keys matching the
        header names.
        :param headers: Column names of the files
        :param customer_name: Customer Name
        :param date_range: Date range text
        :return: Path to csv file
        """
        TEMP_DIR = tempfile.gettempdir()
        file_name = f"{generate_random_id(15)}.csv"
        file_path = os.path.join(TEMP_DIR, file_name)
        try:
            with open(file_path, "w") as output_file:
                file_writer = csv.writer(output_file)
                if customer_name:
                    file_writer.writerow(["Customer Name", customer_name])
                if date_range:
                    file_writer.writerow(["Date Range", date_range])
                file_writer.writerow([])
                dict_writer = csv.DictWriter(output_file, headers)
                dict_writer.writeheader()
                dict_writer.writerows(data)
            logger.info("Csv file generated at path {0}".format(file_path))
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return None, None
        return file_path, file_name


class ExcelCreationService:
    """
    Create Excel workbook from csv files
    """

    @classmethod
    def create_excel(cls, csv_paths, customer_name, date_range):
        TEMP_DIR = tempfile.gettempdir()
        file_name = f"{generate_random_id(15)}.xlsx"
        file_path = os.path.join(TEMP_DIR, file_name)
        with pandas.ExcelWriter(file_path) as writer:
            for report_name, csv_path in csv_paths.items():
                df = pandas.read_csv(csv_path, skiprows=3)
                df.to_excel(
                    writer, sheet_name=report_name, startrow=3, index=False
                )
                worksheet = writer.sheets[report_name]
                worksheet.write(1, 1, "Customer Name")
                worksheet.write(1, 2, customer_name)
                worksheet.write(2, 1, "Date Range")
                worksheet.write(2, 2, f"{date_range}")
        return file_path, file_name

    @classmethod
    def create_bundle_reports_excel(cls, csv_paths, customer_name):
        TEMP_DIR = tempfile.gettempdir()
        file_name = f"{generate_random_id(15)}.xlsx"
        file_path = os.path.join(TEMP_DIR, file_name)
        with pandas.ExcelWriter(file_path) as writer:
            for report_name, csv_path in csv_paths.items():
                df = pandas.read_csv(csv_path)
                df.to_excel(writer, sheet_name=report_name, index=False)
                worksheet = writer.sheets[report_name]
        return file_path, file_name


class CustomerAnalysisReportsService:
    # Report Sub Types
    TICKET_DATA_EXPORT_REPORT = Box(
        label="Ticket Data Export",
        type="ticket_data_export",
        display_datepicker=True,
    )
    TICKET_BY_SERVICE_TYPE_SUMMARY_REPORT = Box(
        label="Ticket by Service Type Summary",
        type="ticket_by_service_type_summary",
        display_datepicker=True,
    )
    OPPORTUNITY_REPORT = Box(
        label="Open Opportunity Report",
        type="open_opportunity_report",
        display_datepicker=False,
    )
    ALERT_DATA_EXPORT_REPORT = Box(
        label="Alert Data Export",
        type="alert_data_export",
        display_datepicker=True,
    )
    ALERT_COUNT_SUMMARY_REPORT = Box(
        label="Alert Count Summary",
        type="alert_count_summary",
        display_datepicker=True,
    )
    ALERT_LONGEST_INDIVIDUAL_DOWNTIME_SUMMARY = Box(
        label="Longest Ind. Downtime Summary",
        type="alert_longest_individual_downtime_summary",
        display_datepicker=False,
    )
    ALERT_TOTAL_DOWNTIME_SUMMARY_REPORT = Box(
        label="Total Downtime Summary",
        type="alert_total_downtime_summary_report",
        display_datepicker=False,
    )
    reports = Box(
        ticket_data_export=TICKET_DATA_EXPORT_REPORT,
        ticket_by_service_type_summary=TICKET_BY_SERVICE_TYPE_SUMMARY_REPORT,
        open_opportunity_report=OPPORTUNITY_REPORT,
        alert_data_export=ALERT_DATA_EXPORT_REPORT,
        alert_count_summary=ALERT_COUNT_SUMMARY_REPORT,
        alert_longest_individual_downtime_summary=ALERT_LONGEST_INDIVIDUAL_DOWNTIME_SUMMARY,
        alert_total_downtime_summary_report=ALERT_TOTAL_DOWNTIME_SUMMARY_REPORT,
    )
    reports_methods_mapping = Box(
        ticket_data_export=ServiceRequestReport.export_service_ticket_data,
        ticket_by_service_type_summary=ServiceRequestReport.get_ticket_by_service_type_summary_report,
        open_opportunity_report=OpportunityReportService.get_opportunity_report,
        alert_data_export=AlertReportService.export_alerts_data,
        alert_count_summary=AlertReportService.get_alert_count_summary_report,
        alert_longest_individual_downtime_summary=AlertReportService.get_longest_individual_downtime_summary_report,
        alert_total_downtime_summary_report=AlertReportService.get_total_downtime_summary_report,
    )
    # Report Types
    ERP_REPORTS = Box(
        label="ERP Reports",
        type="erp_reports",
        sub_types=[
            TICKET_DATA_EXPORT_REPORT,
            TICKET_BY_SERVICE_TYPE_SUMMARY_REPORT,
            OPPORTUNITY_REPORT,
        ],
    )
    DEVICE_MONITORING_REPORTS = Box(
        label="Device Monitoring Reports",
        type="device_monitoring_reports",
        sub_types=[
            ALERT_DATA_EXPORT_REPORT,
            ALERT_COUNT_SUMMARY_REPORT,
            ALERT_LONGEST_INDIVIDUAL_DOWNTIME_SUMMARY,
            ALERT_TOTAL_DOWNTIME_SUMMARY_REPORT,
        ],
    )

    @classmethod
    def get_customer_analysis_reports_meta(cls):
        """
        Returns the report types and sub types along with the required metadata.
        :return: List if dicts
        """
        reports_meta = [cls.ERP_REPORTS, cls.DEVICE_MONITORING_REPORTS]
        return reports_meta

    @classmethod
    def _get_csv_report(
        cls, provider, customer_crm_id, report_sub_type, start_date, end_date
    ):
        report_data = cls.reports_methods_mapping.get(report_sub_type)(
            provider, customer_crm_id, start_date, end_date
        )
        if not report_data.data:
            return None, None
        customer_name = Customer.objects.get(
            crm_id=customer_crm_id, provider=provider
        ).name
        if start_date and end_date:
            date_range = f"{start_date.date()} - {end_date.date()}"
        else:
            date_range = None
        report_path, file_name = CSVCreationService.create_csv(
            report_data.data, report_data.headers, customer_name, date_range
        )
        return report_path, file_name

    @classmethod
    def generate_report(
        cls,
        payload,
        provider,
        customer_crm_id,
        report_format=REPORT_FORMAT.csv,
    ):
        """
        Parses payload and calls the method to generate report
        :param payload: request data
        :param provider: Provider object
        :param customer_crm_id: Customer crm id
        :param report_format: report format csv/pdf
        :return: path to the generated report file
        """
        report_type = payload.get("report_type")
        report_sub_type = payload.get("report_sub_type")
        if report_type == cls.DEVICE_MONITORING_REPORTS.type:
            dm_client = (
                LogicMonitorIntegrationService.get_monitoring_api_client(
                    provider
                )
            )
            if not dm_client:
                raise DeviceMonitoringAPINotConfigured()
        elif report_type == cls.ERP_REPORTS.type:
            if not provider.is_configured:
                raise NotConfigured()
        start_date = payload.get("start_date")
        end_date = payload.get("end_date")
        if report_sub_type:
            report_path, file_name = cls._get_csv_report(
                provider,
                customer_crm_id,
                report_sub_type,
                start_date,
                end_date,
            )
            return report_path, file_name
        csv_paths = {}
        sub_types = []
        reports = cls.get_customer_analysis_reports_meta()
        for report in reports:
            if report.type == report_type:
                sub_types = report.sub_types

        for report_sub_type in sub_types:
            report_path, file_name = cls._get_csv_report(
                provider,
                customer_crm_id,
                report_sub_type.type,
                start_date,
                end_date,
            )
            if report_path:
                csv_paths.update({report_sub_type.label: report_path})
        customer_name = Customer.objects.get(
            crm_id=customer_crm_id, provider=provider
        ).name
        if start_date and end_date:
            date_range = f"{start_date.date()} - {end_date.date()}"
        else:
            date_range = None
        if csv_paths:
            report_path, file_name = ExcelCreationService.create_excel(
                csv_paths, customer_name, date_range
            )
            return report_path, file_name
        else:
            return None, None
