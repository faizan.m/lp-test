from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from core import utils
from core.renderers import ProjectAuditCSVRenderer
from document.services import SOWDashboardService
from project_management.services import ProjectService
from reporting.serializer import (
    CustomerAnalysisReportSerializer,
    SOWLinkedProjectsAuditReportSerializer,
)
from reporting.services import CustomerAnalysisReportsService
from storage.file_upload import FileUploadService


class CustomerAnalysisReportListView(APIView):
    """
    Returns the medata for Customer Analysis Reports
    """

    def get(self, request, *args, **kwargs):
        data = (
            CustomerAnalysisReportsService.get_customer_analysis_reports_meta()
        )
        return Response(data=data, status=status.HTTP_200_OK)


class CustomerAnalysisReportView(APIView):
    """
    Generate Customer analysis report
    Returns:
        On Success:
        {
        "file_path": "path to report"
        }
        If no data present:
        {
            "message": "No data found for this query."
        }

    """

    def post(self, request, **kwargs):
        provider = request.provider
        serializer = CustomerAnalysisReportSerializer(data=request.data)
        serializer.is_valid(True)
        customer_crm_id = utils.get_customer_cw_id(self.request, **kwargs)
        file_path, file_name = CustomerAnalysisReportsService.generate_report(
            payload=serializer.validated_data,
            provider=provider,
            customer_crm_id=customer_crm_id,
        )
        if not file_path:
            data = dict(message="No data found for this query.")
        else:
            url = FileUploadService.upload_file(file_path)
            data = dict(file_path=url, file_name=file_name)
        return Response(data=data, status=status.HTTP_200_OK)


class SOWLinkedProjectsAuditReportAPIView(APIView):
    renderer_classes = (ProjectAuditCSVRenderer,)

    def post(self, request, *args, **kwargs):
        provider = request.provider
        serializer = SOWLinkedProjectsAuditReportSerializer(data=request.data)
        serializer.is_valid(True)
        data = ProjectService.export_raw_data(
            provider=provider, payload=serializer.validated_data
        )
        headers = {
            "Content-Disposition": "attachment; filename=Projects Audit Report.csv"
        }
        if data:
            return Response(data=data, status=status.HTTP_200_OK, headers=headers)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
