# application level helper functions
import collections
import logging
import math
import uuid
from datetime import datetime
from enum import Enum
from typing import List, Dict, Optional, Union, Any

from django.conf import settings
from pytz import timezone as pytz_timezone
from rest_framework import serializers

from storage.file_upload import FileUploadService


# Following names are forbidden in file names on Linux and Windows
FORBIDDEN_CHARS_LIST: List[str] = [
    "/",
    "?",
    "\\",
    '"',
    "|",
    "*",
    "<",
    ">",
]


def get_app_logger(module_path):
    """
    Returns logger instance to be used in application
    :return:
    """
    return logging.getLogger(f"backend.{module_path}")


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        fields = self.context["request"].query_params.get("fields")
        if fields:
            fields = fields.split(",")
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class DynamicPaginationMixin(object):
    """
    Controls pagination enable disable option using query param no_page.
    If no_page is passed in query params, data is returned without pagination
    """

    def paginate_queryset(self, queryset):
        if "no_page" in self.request.query_params:
            return None

        return super().paginate_queryset(queryset)


def dict_merge(dct, merge_dct):
    """Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (
            k in dct
            and isinstance(dct[k], dict)
            and isinstance(merge_dct[k], collections.Mapping)
        ):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]
    return dct


class GetCustomerMixin(object):
    """
    Returns Customer object based on the request user type
    """

    def _get_customer(self):
        from rest_framework.generics import get_object_or_404
        from accounts.models import UserType, Customer

        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
            customer = get_object_or_404(Customer, id=customer_id)
        else:
            customer = self.request.customer
        return customer


def get_valid_file_name(original_file_name):
    """
    @param original_file_name: Name of the file to be formatted.
    @return: Formatted file name suitable for Linux and Windows.
    """
    return (
        original_file_name.replace("/", "-")
        .replace("?", "-")
        .replace("*", "-")
        .replace("\\", "-")
        .replace(":", "-")
        .replace("|", "-")
        .replace('"', "-")
        .replace("<", "-")
        .replace(">", "-")
    )


def get_uploaded_file_link(original_file_path):
    """
    This function uploads the file to S3 and returns the uploaded file link.

    Args:
        original_file_path: Local file path of the file.

    Returns:
        Link of file uploaded on S3.
    """
    if settings.DEBUG:
        return original_file_path
    else:
        uploaded_file_url = FileUploadService.upload_file(original_file_path)
        return uploaded_file_url


def convert_local_files_to_remote_attachments(
    files=None, remote_attachments=None
):
    """
    This function replaces local file paths in files with remote urls and appends it to remote_attachments.

    Args:
        files: List containing a tuple of form [(file_name, file_path)].
        remote_attachments: List containing tuple of form [(file_name, file_ulr)].

    Returns:
        Modified files and remote_attachments.
    """
    if not remote_attachments:
        remote_attachments = []
    if not files:
        files = []
        return files, remote_attachments

    if settings.DEBUG:
        return files, remote_attachments
    else:
        for file_name, file_path in files:
            file_url = get_uploaded_file_link(file_path)
            remote_attachments.append((file_name, file_url))
        files = []
        return files, remote_attachments


class DbOperation(Enum):
    CREATE = "create"
    UPDATE = "update"
    DELETE = "delete"


def millify(n):
    """
    https://stackoverflow.com/questions/3154460/python-human-readable-large-numbers

    """
    millnames = ["", " K", " M", " B", " T"]

    n = float(n)
    millidx = max(
        0,
        min(
            len(millnames) - 1,
            int(math.floor(0 if n == 0 else math.log10(abs(n)) / 3)),
        ),
    )

    return "{:.0f}{}".format(n / 10 ** (3 * millidx), millnames[millidx])


def is_valid_uuid(input_id) -> bool:
    """
    Check if input_id is valid UUID or not
    """
    try:
        uuid.UUID(input_id)
        return True
    except ValueError:
        return False


def name_contains_forbidden_characters(name: str) -> bool:
    """
    File name should not contain any forbidden characters
    """
    if any([char in name for char in FORBIDDEN_CHARS_LIST]):
        return True
    else:
        return False


def add_hyphens_to_phone_number(phone_number: str) -> str:
    """
    Adds hyphens to number
    E.g. If number is: 1234567890, returns: 123-45-7890,
         if number is: 123456789011, returns: 12-345-678-9011

    Args:
        phone_number: original phone number

    Returns:
        Phone number with added dash
    """
    phone_number = (
        format(int(phone_number[:-1]), ",").replace(",", "-")
        + phone_number[-1]
    )
    return phone_number


def format_phone_number(original_phone_number: str) -> str:
    """
    Format phone number
    E.g. If phone_number (with country code) = 911234567890, returns: +91 123-456-7890.
         If phone_number (length is 10) e.g. 1234567890, returns: 123-456-7890

    Args:
        original_phone_number: original phone number

    Returns: Formatted phone number
    """
    # Handles the case when country code is already included
    original_phone_number: str = (
        original_phone_number.replace("-", "")
        .replace(" ", "")
        .replace("(", "")
        .replace(")", "")
        .replace("/", "")
        .replace(".", "")
    )
    if original_phone_number.startswith("+"):
        phone_number: str = original_phone_number[-10:]
        country_code_length: int = len(original_phone_number) - len(
            phone_number
        )
        country_code: str = original_phone_number[:country_code_length]
        phone_number = add_hyphens_to_phone_number(phone_number)
        return country_code + " " + phone_number
    # Last 10 digits are part of phone number, initial digits are coutry code
    elif len(original_phone_number) > 10:
        phone_number: str = original_phone_number[-10:]
        country_code_length: int = len(original_phone_number) - len(
            phone_number
        )
        country_code: str = original_phone_number[:country_code_length]
        phone_number = add_hyphens_to_phone_number(phone_number)
        return "+" + country_code + " " + phone_number
    else:
        return add_hyphens_to_phone_number(original_phone_number)


def convert_utc_datetime_to_provider_timezone(
    utc_datetime: datetime, provider_timezone_name: str
) -> datetime:
    provider_timezone = pytz_timezone(provider_timezone_name)
    provider_datetime = utc_datetime.astimezone(provider_timezone)
    return provider_datetime


def add_comma_to_number(num: Union[float, int]) -> str:
    """
    Adds commas to number at 1000th place.
    E.g. If num = 1234567, returns: 1,234,567
    If num = 1234567.123, returns: 1,234,567.123

    Args:
        num: Number (Must be float or integer)

    Returns:
        Number with added commas at 1000th places.
    """
    num: str = "{:,}".format(num)
    return num


def add_comma_to_number_in_string_format(num: str) -> str:
    """
    Adds comma at thousandth place to a number stored in string.
    num must be an integer or float stored in string, otherwise num itself is returned.
    E.g. If num=123456.789, returns: 123,456.789
    If num=123456, returns: 123,456

    Args:
        num: Number stored in string. (Must be float or int)

    Returns:
        Number with commas at thousandth place.
    """
    # If an integer
    if num.isdigit():
        num_with_commas: str = add_comma_to_number(int(num))
        return num_with_commas
    # If a float
    elif "." in num and num.replace(".", "").isdigit():
        integer_part, decimal_part = num.split(".")
        integer_part_with_commas: str = add_comma_to_number(int(integer_part))
        return f"{integer_part_with_commas}.{decimal_part}"
    else:
        return num


def change_precision_of_number(
    num: Union[int, float], decimal_places: int
) -> str:
    """
    Changes precision of a number.
    E.g. If num = 123, decimal_places = 2,  returns: 123.00
    If num = 123.456, decimal_places = 2, returns: 123.46

    Args:
        num: Original number (Must be integer or float)
        decimal_places: No of decimal places upto which precision is needed.

    Returns:
        Number with decimal precision
    """
    decimal_places = 0 if decimal_places < 0 else decimal_places
    # Precision is defined like {:.2f}, {:.3f}, etc.
    precision: str = "{:." + str(decimal_places) + "f}"
    num_with_precision: str = precision.format(num)
    return num_with_precision


def get_customer_contact_details(provider, customer_crm_id: int):
    customer_contact_details: Dict = dict()
    customer_contact: Dict = provider.erp_client.get_default_customer_contacts(
        [customer_crm_id]
    )
    if customer_contact:
        customer_contact: Dict = customer_contact[0]
        first_name: str = customer_contact.get("first_name", "")
        last_name: str = customer_contact.get("last_name", "")
        customer_contact_details.update(
            contact_name=f"{first_name} {last_name}"
            if (first_name and last_name)
            else "",
            phone_number=format_phone_number(
                customer_contact.get("default_phone_number")
            )
            if customer_contact.get("default_phone_number", "")
            else "",
        )
    return customer_contact_details


def add_page_break_in_html(html: str) -> str:
    """
    Adds page break in html wherever @{Page Break} placeholder is used.
    NOTE: This dependes on the placeholder values embedded by UI using Quill editor. Won't work if editor is changed,
    or placeholder value is changed.

    Args:
        html: HTML string

    Returns:
        Formatted html
    """
    html: str = html.replace(
        '<span class="ql-mention-denotation-char">@</span>{Page Break}</span>',
        '<span class="ql-mention-denotation-char"></span><div style="page-break-after:always;"></div></span>',
    )
    return html


def group_list_of_items_by_key(
    items: List[Any], key_function: callable
) -> Dict[str, List[Any]]:
    """
    Group list of items by key.

    Args:
        items: List of items to group.
        key_function: Function that takes an item and returns a key.

    Returns:
        Dictionary of items grouped by key.
    """
    items_grouped_by_key: Dict[str, List[Any]] = dict()
    for item in items:
        key: str = key_function(item)
        if key in items_grouped_by_key:
            items_grouped_by_key[key].append(item)
        else:
            items_grouped_by_key[key] = [item]
    return items_grouped_by_key


def generate_pdf_using_pdf_shift(
    html: str,
    file_name: str,
    local_path: bool = False,
    header: Optional[Dict] = None,
    footer: Optional[Dict] = None,
    options: Optional[Dict] = None,
) -> Dict[str, str]:
    """
    Generate PDF using PDFShift.

    Args:
        html: PDF HTML
        file_name: File name
        local_path: Get local render path if True else get URL.
        header: Header HTML
        footer: Footer HTML
        options: Rendering options

    Returns:
        PDF details
    """
    import tempfile
    import os
    from core.integrations.pdfshift.pdfshift import PdfShift

    if local_path:
        TEMP_DIR = tempfile.gettempdir()
        file_path: str = os.path.join(TEMP_DIR, file_name)
        upload_to_s3: bool = False
    else:
        file_path: str = file_name
        upload_to_s3: bool = True

    default_options: Dict = dict(
        format="A3",
        margin="80px 10px 20px 10px",
    )
    if header:
        default_options.update(header=header)
    if footer:
        default_options.update(footer=footer)
    if options:
        default_options.update(options)
    file_url: str = PdfShift().convert(
        html, file_path, upload_to_s3=upload_to_s3, **default_options
    )
    return dict(file_path=file_url, file_name=file_name)
