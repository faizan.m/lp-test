from rest_framework import serializers

from auditlog.models import LogEntry

MODEL_DISPLAY_NAMES = {
    "datestatussetting": "Date Status Setting",
    "timestatussetting": "Time Status Setting",
    "projectboardsetting": "Project Board Setting",
    "projectphase": "Project Phase",
    "projecttype": "Project Type Setting",
    "overallstatussetting": "Overall Status Setting",
    "activitystatussetting": "Activity Status Setting",
    "customercontactrole": "Customer Contact Role Setting",
    "projectboard": "Project Board Setting",
    "engineerrolemapping": "Engineer Role Mapping Setting",
    "customertouchmeetingtemplate": "Customer Touch Meeting Template",
    "document": "Project Documents",
    "project": "Project",
    "additionalsetting": "Project Additional Settings",
    "projectactivephase": "Project Active Phase",
    "projectnote": "Project Note",
    "actionitem": "Action Item",
    "criticalpathitem": "Critical Path Item",
    "riskitem": "Risk Item",
    "customertouchmeeting": "Customer touch Meeting",
    "kickoffmeeting": "Kickoff Meeting",
    "meeting": "Meeting",
    "statusmeeting": "Status Meeting",
    "milestonestatussetting": "Milestone status Settings",
    "projectcontactrole": "Project Contact Role",
    "meetingattendee": "Meeting attendee",
    "meetingtemplate": "Meeting template Setting",
    "closeoutmeeting": "Closeout Meeting",
    "meetingdocument": "Meeting Document",
    "closeoutmeetingtemplate": "Closeout meeting template",
    "meetingmailtemplate": "Meeting Email template Setting",
    "changerequest": "Change Request",
    "changerequestattachment": "Change Request Attachment",
    "changerequestemailtemplate": "Change Request Email Template",
    "agreement": "Agreement",
    "agreementtemplate": "AgreementTemplate",
}


class LogEntrySerializer(serializers.ModelSerializer):
    action = serializers.CharField(source="get_action_display")
    actor = serializers.StringRelatedField()
    model_display_name = serializers.SerializerMethodField()

    @staticmethod
    def get_model_display_name(obj):
        display_name = MODEL_DISPLAY_NAMES.get(
            obj.content_type.model, obj.content_type.model
        )
        return display_name

    class Meta:
        model = LogEntry
        fields = (
            "action",
            "actor",
            "remote_addr",
            "timestamp",
            "additional_data",
            "changes_dict",
            "changes_str",
            "changes_display_dict",
            "object_repr",
            "model_display_name",
        )
