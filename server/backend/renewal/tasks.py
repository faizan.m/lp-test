import csv
import os
import tempfile
from datetime import datetime

from celery import shared_task, chain
from celery.utils.log import get_task_logger

from accounts.email import RenewalRequestEmail
from accounts.models import Customer, User, Provider
from core.utils import (
    generate_random_id,
    convert_date_string_format,
    convert_date_string_to_date_object,
)
from renewal.models import RenewalRequest, RenewalHaltedDevice
from utils import convert_local_files_to_remote_attachments

# get logger instance
logger = get_task_logger(__name__)


@shared_task
def prepare_renewal_data(customer_id, data):
    customer = Customer.objects.get(id=customer_id)
    customer_crm_id = customer.crm_id
    erp_client = customer.provider.erp_client
    site_ids = list(map(lambda x: x.get("device_info").get("site_id"), data))
    customer_sites = erp_client.get_customer_complete_site_addresses(
        customer_crm_id, site_ids=site_ids
    )
    for request_data in data:
        device_info = request_data.get("device_info")
        address = customer_sites.get(device_info.get("site_id"))
        device_info.update({"site_address": address})
    return data


@shared_task
def prepare_renewal_csv(data):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    headers = [
        "Serial Number",
        "Product Id",
        "Instance Number",
        "Type",
        "LDOS",
        "Site",
        "Site Address",
        "Contract #",
        "Expiring On",
        "Target Service Level",
        "Renewal Type",
        "Notes",
    ]
    new_data = []
    for record in data:
        prepared_data = dict()
        prepared_data["Serial Number"] = record.get("device_serial_number")
        if record.get("device_info"):
            prepared_data["Product Id"] = record.get("device_info").get(
                "product_id"
            )
            prepared_data["Instance Number"] = record.get("device_info").get(
                "instance_id"
            )
            prepared_data["Type"] = record.get("device_info").get(
                "device_type"
            )
            prepared_data["LDOS"] = record.get("device_info").get("LDOS_date")
            prepared_data["Site"] = record.get("device_info").get("site")
            prepared_data["Site Address"] = record.get("device_info").get(
                "site_address"
            )
            prepared_data["Contract #"] = record.get("device_info").get(
                "service_contract_number"
            )
            try:
                prepared_data["Expiring On"] = convert_date_string_format(
                    record.get("device_info").get("expiration_date"),
                    "%Y-%m-%dT%H:%M:%SZ",
                    "%m/%d/%Y",
                )
            except:
                prepared_data["Expiring On"] = ""
        if record.get("target_service_level"):
            target_service_level = RenewalRequest.SERVICE_LEVELS[
                record.get("target_service_level")
            ]
        else:
            target_service_level = ""
        prepared_data["Target Service Level"] = target_service_level

        prepared_data["Renewal Type"] = record.get("renewal_type")
        prepared_data["Renewal Type"] = RenewalRequest.RENEWAL_TYPES[
            record.get("renewal_type")
        ]
        prepared_data["Notes"] = record.get("notes")
        new_data.append(prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_path = os.path.join(TEMP_DIR, f"{generate_random_id(15)}.csv")
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info("Renewal csv file generated at path {0}".format(file_path))
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None
    return file_path


@shared_task
def send_renewal_request_email(file_path, to, customer_id, user_id):
    """
    Send an email to Provider about the new renewal requests received.
    Include the csv of renewal requests as an attachment.
    """
    customer = Customer.objects.get(id=customer_id)
    user = User.objects.get(id=user_id)
    try:
        provider = user.provider
    except AttributeError:
        provider = None
    context = {"customer": customer, "user": user, "provider": provider}
    files = [("RenewalRequests.csv", file_path)]
    files, remote_attachments = convert_local_files_to_remote_attachments(
        files
    )
    logger.info("Sending Renewal request email to: {0}".format(to))
    RenewalRequestEmail(
        context=context, files=files, remote_attachments=remote_attachments
    ).send([to])
    os.remove(file_path)


@shared_task
def deactivate_device_status_in_CW(provider_id, device_ids):
    """
    Set Device status to Inactive in CW.
    """
    provider = Provider.objects.get(id=provider_id)
    client = provider.erp_client
    data = [
        {"device_id": device_id, "status_id": client.device_inactive_status_id}
        for device_id in device_ids
    ]
    client.batch_update_devices(data)


@shared_task
def remove_device_from_renewal_request(provider_id, device_ids):
    logger.info(f"Removing {device_ids} from Renewal Requests")
    RenewalRequest.objects.filter(device_crm_id__in=list(device_ids)).delete()


def prepare_and_send_renewal_csv_mail(to, customer_id, user_id, data):
    """
    Calls async task to prepare csv file from data and send email to the provider.
    """
    return chain(
        prepare_renewal_data.s(customer_id, data)
        | prepare_renewal_csv.s()
        | send_renewal_request_email.s(to, customer_id, user_id)
    ).delay()


@shared_task
def delete_devices_on_renewal_hold_status():
    """
    Delete devices from RenewalHatedDevice table where created_on date is before 90 days from current date.
    """
    for provider in Provider.objects.all():
        if provider.is_active and provider.is_configured:
            device_crm_ids = RenewalHaltedDevice.objects.filter(
                status=RenewalHaltedDevice.HOLD, customer__provider=provider
            ).values_list("device_crm_id", flat=True)

            # Get expiration dates for devices
            if device_crm_ids:
                device_expiration_dates = (
                    provider.erp_client.get_device_expiration_date_by_crm_ids(
                        device_crm_ids
                    )
                )
                devices_to_be_removed = []

                # Check if device is expiring
                for device in device_expiration_dates:
                    expiration_date = device.get("expiration_date")
                    if expiration_date:
                        expiration_date = convert_date_string_to_date_object(
                            expiration_date, "%Y-%m-%dT%H:%M:%SZ"
                        )
                        current_date = datetime.today().date()
                        difference_in_days = (
                            expiration_date - current_date
                        ).days
                        if difference_in_days <= 90:
                            devices_to_be_removed.append(device.get("id"))

                # Remove device from HOLD state
                RenewalHaltedDevice.objects.filter(
                    status=RenewalHaltedDevice.HOLD,
                    customer__provider=provider,
                    device_crm_id__in=devices_to_be_removed,
                ).delete()
