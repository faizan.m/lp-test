from enum import Enum

from rest_framework import serializers


class ChoiceEnum(Enum):
    """
    Enum class with extra classmethod choices which returns tuple of choices.
    """

    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class ChoicesField(serializers.Field):
    def __init__(self, choices, **kwargs):
        self._choices = choices
        self._invert_display_map = dict(
            (v, k) for k, v in self._choices._display_map.items()
        )
        super(ChoicesField, self).__init__(**kwargs)

    def to_representation(self, obj):
        return self._choices[obj]

    def to_internal_value(self, data):
        value = self._invert_display_map.get(data)
        if not value:
            raise serializers.ValidationError("Invalid Choice.")
        return value
