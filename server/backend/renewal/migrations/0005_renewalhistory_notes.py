# Generated by Django 2.0.5 on 2018-11-29 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('renewal', '0004_auto_20181031_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='renewalhistory',
            name='notes',
            field=models.TextField(blank=True, null=True, verbose_name='Notes'),
        ),
    ]
