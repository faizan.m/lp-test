# Create your views here.
from rest_framework import status, generics, filters
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_bulk import BulkCreateAPIView

from accounts.custom_permissions import IsProviderUser
from accounts.models import UserType, Customer
from core.pagination import CustomPagination
from core.renderers import CSVFileRenderer
from renewal.models import RenewalRequest
from renewal.serializers import (
    CompletedRenewalSerializer,
    RenewalRequestSerializer,
    RenewalHistoryListSerializer,
    RenewalRequestsSummarySerializer,
    RenewFileUploadSerializer,
    RenewalRejectSerializer,
)
from renewal.services import RenewalService


class GetCustomerMixin(object):
    def _get_customer(self):
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
            customer = get_object_or_404(Customer, id=customer_id)
        else:
            customer = self.request.customer
        return customer


class CustomerNewRenewalsListAPIView(APIView, GetCustomerMixin):
    """
    API view to list new renewals for customers.
    """

    def get(self, request, **kwargs):
        customer = self._get_customer()
        show_uncovered_only = request.query_params.get(
            "show_uncovered_only", "false"
        )
        show_uncovered_only = True if show_uncovered_only == "true" else False
        category_id = request.provider.get_all_device_categories()
        devices = RenewalService.get_devices_up_for_renewal(
            request.provider.erp_client,
            customer,
            category_id,
            request.provider.id,
            show_uncovered_only,
        )
        return Response(data=devices, status=status.HTTP_200_OK)


class RenewalTypesListAPIView(APIView):
    """
    API View to list renewal types.
    """

    def get(self, request):
        renewal_types = [item[1] for item in RenewalRequest.RENEWAL_TYPES]
        return Response(data=renewal_types, status=status.HTTP_200_OK)


class ServiceLevelTypesListAPIView(APIView):
    """
    API View to list Service Level types.
    """

    def get(self, request):
        service_level_types = [
            item[1] for item in RenewalRequest.SERVICE_LEVELS
        ]
        data = [
            {
                "label": service_level,
                "description": RenewalRequest.SERVICE_LEVEL_DESCRIPTION.get(
                    service_level
                ),
            }
            for service_level in service_level_types
        ]
        return Response(data=data, status=status.HTTP_200_OK)


class RenewalRequestListAPIView(generics.ListAPIView):
    """
    API View to list Renewal Requests for Provider.
    """

    serializer_class = RenewalRequestSerializer
    filter_backends = (
        filters.OrderingFilter,
        filters.SearchFilter,
    )
    ordering_fields = [
        "device_serial_number",
        "status",
        "renewal_type",
        "target_service_level",
    ]
    search_fields = ["device_serial_number", "target_service_level"]
    pagination_class = CustomPagination

    def get_queryset(self):
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        else:
            customer_id = self.request.user.customer.id
        provider = self.request.provider
        queryset = RenewalService.get_pending_renewals(provider, customer_id)
        return queryset


class RenewalRequestReportAPIView(APIView):
    renderer_classes = (CSVFileRenderer,)
    permission_classes = (IsProviderUser,)

    def get(self, request, **kwargs):
        customer_id = self.kwargs.get("customer_id")
        provider = request.provider
        file_path = RenewalService.prepare_pending_renewal_request_report(
            provider, customer_id
        )
        if file_path is not None:
            data = dict(
                file_path=file_path, file_name="Pending-Renewal-Report.csv"
            )
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            return Response(
                {"message": "No data found for this request"},
                status=status.HTTP_404_NOT_FOUND,
            )


class RenewalRequestCreateAPIView(BulkCreateAPIView, GetCustomerMixin):
    """
    API View to create new Renewal Request.
    """

    serializer_class = RenewalRequestSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        customer = self._get_customer()
        if not customer.renewal_service_allowed:
            raise PermissionDenied()
        RenewalService.handle_create_renewal_request(
            self.request.user,
            self.request.provider,
            customer,
            serializer.validated_data,
        )


class RenewalRequestSummaryListAPIView(APIView):
    """
    API View to return Renewal Request Summary List
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request):
        data = RenewalService.get_renewal_requests_summary(request.provider)
        serializer = RenewalRequestsSummarySerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RenewAPIView(generics.CreateAPIView):
    """
    API View to upload renewal file and renew devices.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = RenewFileUploadSerializer

    def perform_create(self, serializer):
        customer_id = self.kwargs.get("customer_id")
        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise NotFound("Invalid Customer Id.")
        RenewalService.renew_devices(
            self.request.user,
            self.request.provider,
            customer,
            serializer.validated_data["renewal_file"],
        )


class RenewalRejectAPIView(APIView):
    """
    API view to apply Reject action on renewal request
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = RenewalRejectSerializer

    def post(self, request, **kwargs):
        serializer = RenewalRejectSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = self.kwargs.get("customer_id")
        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise NotFound("Invalid Customer Id.")

        renewal_request_id = kwargs.get("id")
        try:
            renewal_request = RenewalRequest.objects.get(
                id=renewal_request_id,
                provider=request.provider,
                customer=customer,
            )
        except RenewalRequest.DoesNotExist:
            raise NotFound("Invalid Renewal Request ID.")

        RenewalService.reject_device_renewal(
            request.user,
            request.provider,
            customer,
            renewal_request,
            serializer.validated_data["notes"],
        )
        return Response(status=status.HTTP_200_OK)


class RenewalHaltedDevicesUpdateView(APIView, GetCustomerMixin):
    """
    API View to remove Device from renewal halted state.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer = self._get_customer()
        device_id = kwargs.get("device_id")
        RenewalService.remove_device_from_renewal_halted_state(
            customer, device_id, request.user
        )
        return Response(status=status.HTTP_200_OK)


class RenewalHistoryListAPIView(ListAPIView):
    """
    API View to list Renewal history of a device.

    """

    permission_classes = (IsAuthenticated,)
    serializer_class = RenewalHistoryListSerializer
    pagination_class = None

    def get_queryset(self):
        customer_id = (
            self.kwargs.get("customer_id") or self.request.customer.id
        )
        device_id = self.kwargs.get("device_id")
        if not Customer.objects.filter(
            provider=self.request.provider, id=customer_id
        ).exists():
            raise NotFound("Invalid Customer Id.")

        queryset = RenewalService.get_device_renewal_history(
            customer_id, device_id
        )
        return queryset

    def list(self, request, *args, **kwargs):
        customer_id = (
            self.kwargs.get("customer_id") or self.request.customer.id
        )
        device_id = self.kwargs.get("device_id")
        response = super().list(request, *args, **kwargs)
        renewal_status = RenewalService.get_device_renewal_status(
            customer_id, device_id
        )
        data = dict(history=response.data, renewal_status=renewal_status)
        response.data = data
        return response


class CompletedRenewalListAPIView(ListAPIView):
    """
    API View to list completed renewals
    """

    serializer_class = CompletedRenewalSerializer
    filter_backends = (
        filters.OrderingFilter,
        filters.SearchFilter,
    )
    ordering_fields = ["device_serial_number", "status"]
    search_fields = ["device_serial_number"]
    pagination_class = CustomPagination

    def get_queryset(self):
        complete = self.kwargs.get("complete", False)
        customer_id = self.kwargs.get("customer_id")

        if self.request.user.type == UserType.PROVIDER:
            queryset = RenewalService.get_completed_renewals_for_provider(
                self.request.provider, customer_id, complete
            )
        elif self.request.user.type == UserType.CUSTOMER:
            queryset = RenewalService.get_completed_renewals_for_customer(
                self.request.customer, complete
            )
        return queryset
