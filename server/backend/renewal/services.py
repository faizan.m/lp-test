from datetime import datetime

import pandas as pd
from django.db import transaction, models
from django.db.models import Count, F, OuterRef, Subquery, Q
from django.db.models.functions import Coalesce
from rest_framework.exceptions import ValidationError

from accounts.models import Customer
from core.services import DeviceService
from core.tasks.connectwise.connectwise import batch_update_crm_device_data

from renewal.models import (
    RenewalRequest,
    RenewalHaltedDevice,
    CompletedRenewal,
    RenewalHistory,
)
from renewal.tasks import (
    prepare_and_send_renewal_csv_mail,
    deactivate_device_status_in_CW,
    prepare_renewal_data,
    prepare_renewal_csv,
)

from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


class RenewalService(object):
    @classmethod
    def get_devices_up_for_renewal(
        cls,
        erp_client,
        customer,
        category_id,
        provider_id,
        show_uncovered_only=False,
    ):
        """
        Returns devices which are up for renewal.
        Case 1: show_uncovered_only = False:
            - Fetch active devices from connectwise.
            - Filter devices which have an expiration date (covered)
            - Remove devices which are marked with status DO NOT RENEW or HOLD.
            - Remove devices that are in pending renewal.
        Case 2: show_uncovered_only = True:
            - Fetch active devices from connectwise.
            - Filter devices which do not have an expiration date (uncovered)
            - Remove devices that are in pending renewal.
        """
        devices_for_renewal = []
        devices_list = DeviceService.get_device_list(
            provider_id=provider_id,
            customer_id=customer.crm_id,
            erp_client=erp_client,
            category_id=category_id,
        )
        if show_uncovered_only:
            devices = list(
                filter(
                    lambda d: d.get("expiration_date") is None, devices_list
                )
            )
        else:
            devices = list(
                filter(
                    lambda d: d.get("expiration_date") is not None,
                    devices_list,
                )
            )

        if devices:
            devices_already_in_renewal_process = list(
                RenewalRequest.objects.filter(customer=customer).values_list(
                    "device_crm_id", flat=True
                )
            )
            if not show_uncovered_only:
                devices_with_halted_renewal = list(
                    RenewalHaltedDevice.objects.filter(
                        customer=customer
                    ).values_list("device_crm_id", flat=True)
                )
            else:
                devices_with_halted_renewal = []

            device_exclude_list = (
                devices_already_in_renewal_process
                + devices_with_halted_renewal
            )
            devices_for_renewal = list(
                filter(
                    lambda device: device.get("id") not in device_exclude_list,
                    devices,
                )
            )
        return devices_for_renewal

    @classmethod
    def get_renewal_requests_summary(cls, provider):
        """
        Returns Renewal Requests Summary for the provider.
        :param provider:
        :return:
        """
        logger.info(f"Preparing renewal summary for {provider}")
        # Pending Renewals Count for Customer.
        pending_renewal_requests = (
            RenewalRequest.objects.pending()
            .filter(customer_id=OuterRef("id"))
            .values("customer_id")
            .annotate(pending_renewal_requests=Count("id"))
            .values("pending_renewal_requests")[:1]
        )

        # Completed Renewals Count for Customer
        completed_renewal_requests = (
            CompletedRenewal.objects.filter(customer_id=OuterRef("id"))
            .values("customer_id")
            .annotate(completed_renewal_requests=Count("id"))
            .values("completed_renewal_requests")[:1]
        )

        # Pending and Completed renewals grouped by customer
        renewal_summary = (
            Customer.objects.filter(
                provider=provider, renewal_service_allowed=True
            )
            .values("id")
            .annotate(
                pending_renewals_count=Coalesce(
                    Subquery(
                        pending_renewal_requests,
                        output_field=models.IntegerField(),
                    ),
                    0,
                ),
                completed_renewals_count=Coalesce(
                    Subquery(
                        completed_renewal_requests,
                        output_field=models.IntegerField(),
                    ),
                    0,
                ),
                customer_id=F("id"),
                customer_name=F("name"),
            )
            .order_by("-pending_renewals_count", "-completed_renewals_count")
            .values(
                "customer_id",
                "customer_name",
                "pending_renewals_count",
                "completed_renewals_count",
            )
        )
        logger.info("Renewal Summary returned")
        return renewal_summary

    @classmethod
    def get_pending_renewals(cls, provider, customer_id=None):
        """
        Returns queryset of pending renewals for a provider.
        If customer_id is provided, queryset if filtered for that customer.
        """
        queryset = RenewalRequest.objects.pending().filter(provider=provider)
        if customer_id:
            queryset = queryset.filter(customer_id=customer_id)
        return queryset.order_by("-updated_on")

    @classmethod
    def prepare_renewal_info(cls, renewal_request_data):
        if renewal_request_data.get("target_service_level"):
            target_service_level = RenewalRequest.SERVICE_LEVELS[
                renewal_request_data.get("target_service_level")
            ]
        else:
            target_service_level = ""
        renewal_info = dict(
            renewal_type=RenewalRequest.RENEWAL_TYPES[
                renewal_request_data.get("renewal_type")
            ],
            target_service_level=target_service_level,
        )
        return renewal_info

    @classmethod
    def handle_create_renewal_request(cls, user, provider, customer, data):
        """
        Take appropriate action on the renewal request payload based on the renewal_request type.
        CASE 1: renewal request type = RENEW or RESEARCH ==> Create entry in Renewal Request.
        CASE 2: renewal request type = TERMINATE ==> Add entry to Completed Renewals and
        update is_active=False in CW.
        CASE 3: renewal request type = DO NOT RENEW or HOLD ==> Add entry to RenewalHaltedDevice and Completed.

        Call async task to prepare csv and send an to provider accounting contact email.
        """
        device_ids_for_status_update = []
        for renewal_request in data:
            device_crm_id = renewal_request["device_crm_id"]
            renewal_type = renewal_request["renewal_type"]
            notes = renewal_request.get("notes")

            # Make entry to renewal history
            action = RenewalHistory.RENEWAL_REQUEST_RAISED.format(
                RenewalRequest.RENEWAL_TYPES[renewal_type]
            )
            cls._add_renewal_history(
                device_crm_id, customer, action, user, notes=notes
            )

            if renewal_type in [RenewalRequest.RENEW, RenewalRequest.RESEARCH]:
                cls.create_new_renewal_request(
                    provider, customer, renewal_request
                )
            elif renewal_type in [
                RenewalRequest.DO_NOT_RENEW,
                RenewalRequest.HOLD,
            ]:
                cls.add_device_to_renewal_halt(
                    user, customer, device_crm_id, renewal_type
                )
            elif renewal_type == RenewalRequest.TERMINATE:
                renewal_request["renewal_info"] = cls.prepare_renewal_info(
                    renewal_request
                )
                cls.terminate_device_renewal(
                    user, customer, device_crm_id, renewal_request
                )
                device_ids_for_status_update.append(device_crm_id)

            instance_id = DeviceService.get_instance_id(
                device_crm_id, provider.id
            )
            renewal_request.update(dict(instance_id=instance_id))

        # Call async task to prepare csv and send an to provider accounting contact email.
        to = (
            provider.accounting_contact.email
        )  # send email to provider accounting contact
        prepare_and_send_renewal_csv_mail(to, customer.id, user.id, data)

        # Call async task to update device status for Terminated renewal_type.
        if device_ids_for_status_update:
            deactivate_device_status_in_CW.delay(
                provider.id, device_ids_for_status_update
            )

    @classmethod
    def create_new_renewal_request(cls, provider, customer, data):
        """
        Create new RenewalRequest object and save to DB.
        """
        new_renewal_request = RenewalRequest(
            provider=provider, customer=customer, **data
        )
        new_renewal_request.save()
        return new_renewal_request

    @classmethod
    def add_device_to_renewal_halt(
        cls, user, customer, device_crm_id, renewal_type
    ):
        """
        Add device to RenewalHaltedDevice model.
        """
        if renewal_type not in [
            RenewalRequest.DO_NOT_RENEW,
            RenewalRequest.HOLD,
        ]:
            raise ValueError("Invalid renewal halt type.")

        # Make entry to renewal history
        action = RenewalHistory.RENEWAL_HALTED.format(
            RenewalRequest.RENEWAL_TYPES[renewal_type]
        )
        cls._add_renewal_history(device_crm_id, customer, action, user)

        if renewal_type == RenewalRequest.DO_NOT_RENEW:
            status = RenewalHaltedDevice.DO_NOT_RENEW
        elif renewal_type == RenewalRequest.HOLD:
            status = RenewalHaltedDevice.HOLD
        # Make entry to Renewal Halted table
        RenewalHaltedDevice.objects.create(
            device_crm_id=device_crm_id, customer=customer, status=status
        )
        logger.debug(
            f"Device added to Renewal Halt. Device Id: {device_crm_id}"
        )

    @classmethod
    def terminate_device_renewal(
        cls, user, customer, device_crm_id, renewal_request
    ):
        """
        Handle renewal request with renewal type TERMINATE
        """
        cls.create_completed_renewal(
            customer.provider,
            customer,
            CompletedRenewal.TERMINATED,
            renewal_request,
        )
        # Make entry to renewal history
        action = RenewalHistory.RENEWAL_COMPLETED.format(
            CompletedRenewal.RENEWAL_STATUS[CompletedRenewal.TERMINATED]
        )
        cls._add_renewal_history(device_crm_id, customer, action, user)

    @classmethod
    def reject_device_renewal(
        cls, user, provider, customer, renewal_request, notes
    ):
        """
        Reject renewal request
        """
        renewal_info = dict(
            renewal_type=RenewalRequest.RENEWAL_TYPES[
                renewal_request.renewal_type
            ],
            target_service_level=RenewalRequest.SERVICE_LEVELS[
                renewal_request.target_service_level
            ],
        )
        RenewalService.move_renewal_request_to_completed_renewal(
            user,
            provider,
            customer,
            renewal_request,
            renewal_info,
            CompletedRenewal.REJECTED,
            notes,
        )

    @classmethod
    def create_completed_renewal(cls, provider, customer, status, data):
        """
        Create new CompletedRenewal and save to DB.
        """

        completed_renewal = CompletedRenewal(
            provider=provider,
            customer=customer,
            status=status,
            device_serial_number=data.get("device_serial_number"),
            device_crm_id=data.get("device_crm_id"),
            device_info=data.get("device_info"),
            renewal_info=data.get("renewal_info"),
            notes=data.get("notes"),
        )
        completed_renewal.save()
        return completed_renewal

    @classmethod
    @transaction.atomic
    def move_renewal_request_to_completed_renewal(
        cls,
        user,
        provider,
        customer,
        renewal_request_obj,
        renewal_info,
        status,
        notes=None,
    ):
        """
        Creates CompletedRenewal object from the renewal_request_object, and saves it to db.
        Deletes renewal_request_obj object.
        """
        # create CompletedRenewal object
        data = dict(
            device_serial_number=renewal_request_obj.device_serial_number,
            device_crm_id=renewal_request_obj.device_crm_id,
            device_info=renewal_request_obj.device_info,
            renewal_info=renewal_info,
            notes=notes,
        )
        cls.create_completed_renewal(
            provider=provider, customer=customer, status=status, data=data
        )
        # Add device renewal history
        action = RenewalHistory.RENEWAL_COMPLETED.format(
            CompletedRenewal.RENEWAL_STATUS[status]
        )
        cls._add_renewal_history(
            renewal_request_obj.device_crm_id,
            customer,
            action,
            user,
            notes=notes,
        )
        # Delete renewal request
        RenewalRequest.objects.filter(id=renewal_request_obj.id).delete()
        return

    @classmethod
    def _read_cisco_renewal_file(cls, renewal_file):
        # TODO: Replace with a better dynamic way to read data.
        xl = pd.ExcelFile(renewal_file)
        df = xl.parse(xl.sheet_names[0])
        contract_number = df["Unnamed: 2"][3]
        df = df[22::].copy()
        df.columns = df.iloc[0]
        df.reindex(df.index.drop(22))
        df.reset_index(inplace=True)
        filtered_df = df.loc[df["PAK/Serial Number"].notnull()]
        filtered_df = filtered_df.where((pd.notnull(filtered_df)), None)
        data = filtered_df.to_dict("records")[1:]
        return contract_number, data

    @classmethod
    def renew_devices(cls, user, provider, customer, renewal_file):
        """
        Reads renewal file and renews devices based on the data from renewal file.
        """
        try:
            contract_number, data = cls._read_cisco_renewal_file(renewal_file)
            logger.info(
                "Renewal file parsed. New contract number: {0}".format(
                    contract_number
                )
            )
        except Exception as e:
            logger.error(
                "Error in reading uploaded renewal file: {0}".format(e)
            )
            raise ValidationError({"renewal_file": ["Invalid file format."]})

        bulk_update_payload = []
        renewed_device_serial_numbers = []
        for device in data:
            serial_number = device.get("PAK/Serial Number")
            if not (contract_number and device.get("End Date")):
                raise ValidationError(
                    {"renewal_file": ["Invalid file format."]}
                )
            renewal_request = (
                RenewalRequest.objects.pending()
                .filter(
                    device_serial_number=serial_number,
                    provider=provider,
                    customer=customer,
                )
                .first()
            )
            if renewal_request:
                try:
                    if isinstance(device["End Date"], datetime):
                        end_date = datetime.strftime(
                            device["End Date"], "%Y-%m-%dT%H:%M:%SZ"
                        )
                    else:
                        end_date = datetime.strftime(
                            datetime.strptime(device["End Date"], "%d-%b-%Y"),
                            "%Y-%m-%dT%H:%M:%SZ",
                        )
                except Exception:
                    logger.info(
                        "Invalid format End Date in renewal file: {0}".format(
                            device["End Date"]
                        )
                    )
                    raise ValidationError(
                        {"renewal_file": ["Invalid date format."]}
                    )

                renewal_info = dict(
                    device_id=renewal_request.device_crm_id,
                    service_contract_number=contract_number,
                    expiration_date=end_date,
                    renewal_type=RenewalRequest.RENEWAL_TYPES[
                        renewal_request.renewal_type
                    ],
                    target_service_level=RenewalRequest.SERVICE_LEVELS[
                        renewal_request.target_service_level
                    ],
                )
                logger.debug(
                    "Moving renewal request {0} to completed.".format(
                        renewal_request
                    )
                )
                cls.move_renewal_request_to_completed_renewal(
                    user,
                    provider,
                    customer,
                    renewal_request,
                    renewal_info,
                    CompletedRenewal.RENEWED,
                )
                bulk_update_payload.append(renewal_info)
                renewed_device_serial_numbers.append(serial_number)

        if renewed_device_serial_numbers:
            logger.info("Device renewal completed. Async tasks called.")
            # Update data in CW.
            logger.debug("Updating data in connectwise for renewed devices.")
            batch_update_crm_device_data.delay(
                provider.id, bulk_update_payload
            )

    @classmethod
    def remove_device_from_renewal_halted_state(
        cls, customer, device_crm_id, user
    ):
        """
        Deletes device from Renewal Halted state
        :param user: User object
        :param customer: Customer object
        :param device_crm_id: Device CRM Id
        :return: None
        """
        # Remove device from halted state
        RenewalHaltedDevice.objects.filter(
            customer_id=customer.id, device_crm_id=device_crm_id
        ).delete()

        # Add entry to device renewal history
        action = RenewalHistory.RENEWAL_CONTINUED
        cls._add_renewal_history(device_crm_id, customer, action, user)
        return None

    @classmethod
    def get_device_renewal_history(cls, customer_id, device_crm_id):
        """
        Returns device renewal history in descending order of created_on field.
        :param customer_id: Customer Id
        :param device_crm_id: Device CRM Id
        :return: Queryset
        """
        queryset = (
            RenewalHistory.objects.filter(
                customer_id=customer_id, device_crm_id=device_crm_id
            )
            .select_related("author")
            .order_by("-created_on")
        )
        return queryset

    @classmethod
    def _get_completed_renewals(cls, complete=False):
        """
        Returns completed renewals list in descending order of updated_on date
        :param complete: if True returns all completed renewals else returns
        recently completed renewals
        :return: queryset of Completed Renewals
        """
        queryset = CompletedRenewal.objects.all().order_by("-updated_on")
        return queryset

    @classmethod
    def get_completed_renewals_for_provider(
        cls, provider, customer_id, complete
    ):
        """
        Returns completed renewals for provider
        :param provider: Provider object
        :param customer_id: customer id
        :param complete: if True returns all completed renewals else returns
        recently completed renewals
        :return: queryset  of Completed Renewals
        """
        queryset = cls._get_completed_renewals(complete)
        queryset = queryset.filter(provider=provider)
        if customer_id:
            queryset = queryset.filter(customer_id=customer_id)
        return queryset

    @classmethod
    def get_completed_renewals_for_customer(cls, customer, complete):
        """
        Returns completed renewals for Customer
        :param customer: Customer object
        :param complete: if True returns all completed renewals else returns
        recently completed renewals
        :return: queryset of Completed Renewals
        """
        queryset = cls._get_completed_renewals(complete)
        queryset = queryset.filter(customer=customer)
        return queryset

    @classmethod
    def _add_renewal_history(
        cls, device_crm_id, customer, action, user, notes=None
    ):
        """
        Create RenewalHistory object
        """
        RenewalHistory.objects.create(
            device_crm_id=device_crm_id,
            customer=customer,
            action=action,
            author=user,
            notes=notes,
        )

    @classmethod
    def get_device_renewal_status(cls, customer_id, device_id):
        """
        Returns device renewal status if device is in renewal halted state.
        """
        try:
            renewal_halted_device = RenewalHaltedDevice.objects.get(
                device_crm_id=device_id, customer_id=customer_id
            )
            return renewal_halted_device.get_status_display()
        except RenewalHaltedDevice.DoesNotExist:
            return None

    @classmethod
    def prepare_pending_renewal_request_report(cls, provider, customer_id):
        data = RenewalService.get_pending_renewals(
            provider, customer_id
        ).values()
        data = prepare_renewal_data(customer_id, list(data))
        file_path = prepare_renewal_csv(data)
        return file_path

    @classmethod
    def get_renewal_history_is_present(cls, customer_id, device_id):
        queryset_count = RenewalHistory.objects.filter(
            customer_id=customer_id, device_crm_id=device_id
        ).count()
        return True if queryset_count else False
