from django.db.models import JSONField
from django.db import models
from django.utils import timezone
from model_utils import Choices

from accounts.model_utils import TimeStampedModel


class RenewalRequestQueryset(models.QuerySet):
    def pending(self):
        return self.filter(
            renewal_type__in=[RenewalRequest.RENEW, RenewalRequest.RESEARCH]
        )


class RenewalRequest(TimeStampedModel):
    RENEW = "1"
    DO_NOT_RENEW = "2"
    TERMINATE = "3"
    RESEARCH = "4"
    HOLD = "5"
    RENEWAL_TYPES = Choices(
        (RENEW, "Renew"),
        (DO_NOT_RENEW, "Do Not Renew"),
        (TERMINATE, "Terminate"),
        (RESEARCH, "Research"),
        (HOLD, "Hold"),
    )

    EIGHT_X_FIVE_X_NBD = "1"  # 8X5XNBD
    TWENTY_FOUR_X_SEVEN_X_FOUR = "2"  # 24X7X4
    TWENTY_FOUR_X_SEVEN_X_TWO = "3"  # 24X7X2
    TWENTY_FOUR_X_SEVEN_X_FOUR_OS = "4"  # 24X7X4-OS
    TWENTY_FOUR_X_SEVEN_X_TWO_OS = "5"  # 24X7X2-OS

    SERVICE_LEVELS = Choices(
        (EIGHT_X_FIVE_X_NBD, "8x5xNBD"),
        (TWENTY_FOUR_X_SEVEN_X_FOUR, "24x7x4"),
        (TWENTY_FOUR_X_SEVEN_X_TWO, "24x7x2"),
        (TWENTY_FOUR_X_SEVEN_X_FOUR_OS, "24x7x4-OS"),
        (TWENTY_FOUR_X_SEVEN_X_TWO_OS, "24x7x2-OS"),
    )
    SERVICE_LEVEL_DESCRIPTION = {
        "8x5xNBD": "8AM – 5PM Next Business Day Replacement",
        "24x7x4": "24 Hours – 4 Hour Hardware Replacement",
        "24x7x2": "24 Hours – 2 Hour Hardware Replacement",
        "24x7x4-OS": "24 Hours – 4 Hour Hardware Replacement with Technician Onsite",
        "24x7x2-OS": "24 Hours – 2 Hour Hardware Replacement with Technician Onsite",
    }

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="renewal_requests",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="renewal_requests",
        on_delete=models.CASCADE,
    )
    device_serial_number = models.CharField(
        max_length=100, verbose_name="Device Serial Number"
    )
    device_crm_id = models.PositiveIntegerField()
    device_info = JSONField(verbose_name="Device related information")
    target_service_level = models.CharField(
        max_length=100,
        choices=SERVICE_LEVELS,
        verbose_name="Target Service Level",
    )
    renewal_type = models.CharField(
        max_length=20, choices=RENEWAL_TYPES, verbose_name="Renewal Type"
    )
    notes = models.TextField(blank=True, null=True, verbose_name="Notes")

    objects = RenewalRequestQueryset.as_manager()

    class Meta:
        verbose_name = "Renewal Request"
        verbose_name_plural = "Renewal Requests"

    def __str__(self):
        return f"{self.id}"


class CompletedRenewalQueryset(models.QuerySet):
    def renewed(self):
        """
        Filter Renewal History which have status=RENEWED.
        """
        return self.filter(status=CompletedRenewal.RENEWED)

    def rejected(self):
        """
        Filter Renewal History which have status=REJECTED.
        """
        return self.filter(status=CompletedRenewal.REJECTED)

    def recently_completed(self):
        """
        Filter Renewal history which have updated date less than 90 days.
        """
        now = timezone.now()
        date_90_days_back = now - timezone.timedelta(days=90)
        return self.filter(updated_on__gte=date_90_days_back)


class CompletedRenewal(TimeStampedModel):
    RENEWED = "1"
    REJECTED = "2"
    MARKED_AS_DO_NOT_RENEW = "3"
    MARKED_FOR_RESEARCH = "4"
    TERMINATED = "5"
    RENEWAL_STATUS = Choices(
        (RENEWED, "Renewed"),
        (REJECTED, "Rejected"),
        (MARKED_AS_DO_NOT_RENEW, "Marked as Do Not Renew"),
        (TERMINATED, "Terminated"),
        (MARKED_FOR_RESEARCH, "Marked for Research"),
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="renewal_requests_history",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="renewal_requests_history",
        on_delete=models.CASCADE,
    )
    device_serial_number = models.CharField(
        max_length=100, verbose_name="Device Serial Number"
    )
    device_crm_id = models.PositiveIntegerField()
    status = models.CharField(
        max_length=2, choices=RENEWAL_STATUS, verbose_name="Renewal Status"
    )
    device_info = JSONField(blank=True, null=True)
    renewal_info = JSONField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    objects = CompletedRenewalQueryset.as_manager()

    class Meta:
        verbose_name = "Completed Renewal"
        verbose_name_plural = "Completed Renewals"

    def __str__(self):
        return f"{self.id}"


class RenewalHaltedDevice(models.Model):
    DO_NOT_RENEW = "1"
    HOLD = "2"
    STATUS_CHOICES = Choices((DO_NOT_RENEW, "Do Not Renew"), (HOLD, "Hold"))
    device_crm_id = models.PositiveIntegerField()
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="renewal_halted_devices",
        on_delete=models.CASCADE,
    )
    status = models.CharField(max_length=2, choices=STATUS_CHOICES)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("device_crm_id", "customer")

    def __str__(self):
        return f"{self.device_crm_id}: {self.status}"


class RenewalHistory(models.Model):
    # Renewal Action statements
    RENEWAL_REQUEST_RAISED = "Renewal request raised with type {0}."
    RENEWAL_COMPLETED = "Renewal completed with status {0}."
    RENEWAL_HALTED = "Device renewal flow halted with status {0}."
    RENEWAL_CONTINUED = "Device added back to renewal flow."

    device_crm_id = models.PositiveIntegerField()
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="renewal_history",
        on_delete=models.CASCADE,
    )
    action = models.CharField(max_length=500)
    author = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True
    )
    created_on = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(blank=True, null=True, verbose_name="Notes")

    def __str__(self):
        return f"{self.device_crm_id}: {self.action}"
