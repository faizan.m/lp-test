from rest_framework.permissions import BasePermission


class IsRenewalAllowed(BasePermission):
    def has_permission(self, request, view):
        return request.customer.renewal_service_allowed
