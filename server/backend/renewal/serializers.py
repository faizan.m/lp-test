from django.core.validators import FileExtensionValidator
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_bulk import BulkSerializerMixin

from renewal.models import RenewalRequest, RenewalHistory, CompletedRenewal
from renewal.utils import ChoicesField


class RenewalRequestSerializer(
    BulkSerializerMixin, serializers.ModelSerializer
):
    renewal_type = ChoicesField(
        choices=RenewalRequest.RENEWAL_TYPES, required=False, allow_null=True
    )
    target_service_level = ChoicesField(
        choices=RenewalRequest.SERVICE_LEVELS, required=False, allow_null=True
    )

    def validate(self, attrs):
        super().validate(attrs)
        renewal_type = attrs.get("renewal_type")
        if renewal_type in [RenewalRequest.RENEW, RenewalRequest.RESEARCH]:
            if not attrs.get("target_service_level"):
                raise ValidationError(
                    {"target_service_level": ["This field can't be empty."]}
                )
        return attrs

    class Meta:
        model = RenewalRequest
        fields = [
            "customer",
            "created_on",
            "device_serial_number",
            "device_crm_id",
            "device_info",
            "id",
            "notes",
            "target_service_level",
            "renewal_type",
            "updated_on",
        ]
        read_only_fields = ["customer"]


class RenewalHistoryListSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()

    class Meta:
        model = RenewalHistory
        fields = ("action", "author", "created_on", "notes")


class RenewalRequestsSummarySerializer(serializers.Serializer):
    customer_id = serializers.IntegerField()
    customer_name = serializers.CharField(max_length=100)
    pending_renewals_count = serializers.IntegerField(min_value=0, default=0)
    completed_renewals_count = serializers.IntegerField(min_value=0, default=0)


class RenewFileUploadSerializer(serializers.Serializer):
    renewal_file = serializers.FileField(
        validators=[FileExtensionValidator(["xlsx", "xls"])]
    )


class RenewalRejectSerializer(serializers.Serializer):
    notes = serializers.CharField(
        max_length=100, allow_blank=True, allow_null=True
    )


class CompletedRenewalSerializer(serializers.ModelSerializer):
    status = ChoicesField(choices=CompletedRenewal.RENEWAL_STATUS)

    class Meta:
        model = CompletedRenewal
        fields = "__all__"
