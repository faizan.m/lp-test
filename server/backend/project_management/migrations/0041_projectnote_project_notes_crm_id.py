# Generated by Django 3.2.14 on 2023-03-21 08:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("project_management", "0040_meeting_owner"),
    ]

    operations = [
        migrations.AddField(
            model_name="projectnote",
            name="project_notes_crm_id",
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
