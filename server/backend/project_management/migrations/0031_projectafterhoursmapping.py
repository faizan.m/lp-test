# Generated by Django 2.0.5 on 2022-05-31 12:07

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import project_management.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0106_auto_20220519_1208'),
        ('project_management', '0030_auto_20220519_0829'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectAfterHoursMapping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('work_type_ids', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), size=None)),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='accounts.Provider')),
            ],
            bases=(project_management.mixins.ProjectSettingAuditMixin, models.Model),
        ),
    ]
