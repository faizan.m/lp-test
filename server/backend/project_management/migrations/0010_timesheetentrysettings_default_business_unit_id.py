# Generated by Django 2.0.5 on 2021-09-28 11:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project_management', '0009_remove_timesheetentrysettings_time_period_open_status_ids'),
    ]

    operations = [
        migrations.AddField(
            model_name='timesheetentrysettings',
            name='default_business_unit_id',
            field=models.PositiveIntegerField(default=2),
            preserve_default=False,
        ),
    ]
