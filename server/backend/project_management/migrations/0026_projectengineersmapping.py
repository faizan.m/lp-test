# Generated by Django 2.0.5 on 2022-05-09 12:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0104_customer_customer_order_visibility_allowed"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("project_management", "0025_auto_20220421_1053"),
    ]

    operations = [
        migrations.CreateModel(
            name="ProjectEngineersMapping",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_on", models.DateTimeField(auto_now_add=True)),
                ("updated_on", models.DateTimeField(auto_now=True)),
                ("member_crm_id", models.IntegerField()),
                (
                    "member",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="+",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "project",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="engineers",
                        to="project_management.Project",
                    ),
                ),
                (
                    "provider",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="+",
                        to="accounts.Provider",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
