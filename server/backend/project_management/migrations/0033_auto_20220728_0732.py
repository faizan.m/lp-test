# Generated by Django 2.0.5 on 2022-07-28 07:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project_management', '0032_project_billing_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='changerequest',
            name='sow_document',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='change_request', to='document.SOWDocument'),
        ),
    ]
