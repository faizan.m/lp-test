# import django_filters
from django_filters import rest_framework as filters
from django.utils import timezone
from datetime import datetime, timedelta
from django.utils.timezone import get_current_timezone
from common.custom_filters import ListFilter


from project_management.models import Project, PMOMetricsNotes
from django.db.models import Q, F


class ProjectFilterSet(filters.FilterSet):
    type = ListFilter()
    customer = ListFilter()
    project_manager = ListFilter()
    project_engineer = filters.UUIDFilter(method="filter_project_engineer")
    project_engineer_or_manager = filters.UUIDFilter(
        method="filter_project_engineer_or_manager"
    )
    estimated_end_date_after = filters.DateFilter(
        field_name="estimated_end_date",
        lookup_expr=("date__gte"),
    )
    estimated_end_date_before = filters.DateFilter(
        field_name="estimated_end_date", lookup_expr=("date__lte")
    )
    last_action_date_after = filters.DateFilter(
        field_name="last_action_date",
        lookup_expr=("date__gte"),
    )
    last_action_date_before = filters.DateFilter(
        field_name="last_action_date", lookup_expr=("date__lte")
    )
    projects_closing_this_week = filters.BooleanFilter(
        method="filter_projects_closing_this_week"
    )
    overbudget_projects = filters.BooleanFilter(
        method="filter_overbudget_projects"
    )
    projects_with_overdue_action_items_or_overdue_critical_path_items = filters.BooleanFilter(
        method="filter_projects_with_overdue_action_items_or_overdue_critical_path_items"
    )
    overall_status = ListFilter()
    unmapped_sow = filters.BooleanFilter(
        method="filter_projects_without_sow_mapping"
    )
    unmapped_opp = filters.BooleanFilter(
        method="filter_projects_without_opportunity_mapping"
    )
    sow_type = filters.CharFilter(method="filter_projects_by_mapped_sow_type")

    class Meta:
        model = Project
        fields = [
            "type",
            "customer",
            "project_manager",
            "overbudget_projects",
            "projects_closing_this_week",
            "projects_with_overdue_action_items_or_overdue_critical_path_items",
            "project_engineer",
            "project_engineer_or_manager",
            "overall_status",
        ]

    def filter_projects_closing_this_week(self, queryset, name, value):
        if not value:
            return queryset

        filter_conditions = dict()

        estimated_end_date_after = datetime.now(tz=get_current_timezone())
        estimated_end_date_before = estimated_end_date_after + timedelta(
            days=7
        )
        filter_conditions.update(
            dict(
                estimated_end_date__gte=estimated_end_date_after,
                estimated_end_date__lte=estimated_end_date_before,
            )
        )

        return queryset.filter(**filter_conditions)

    def filter_overbudget_projects(self, queryset, name, value):
        if not value:
            return queryset

        return queryset.filter(actual_hours__gt=F("hours_budget"))

    def filter_projects_with_overdue_action_items_or_overdue_critical_path_items(
        self, queryset, name, value
    ):
        if not value:
            return queryset

        return queryset.filter(
            Q(overdue_action_items__isnull=False)
            | Q(overdue_critical_path_items__isnull=False)
        )

    def filter_project_engineer(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(engineers__member_id=value)

    def filter_project_engineer_or_manager(self, queryset, name, value):
        if not value:
            return queryset

        query = queryset.filter(
            Q(project_manager_id=value) | Q(engineers__member_id=value)
        ).distinct()
        return query

    def filter_projects_without_sow_mapping(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(sow_document__isnull=True)

    def filter_projects_without_opportunity_mapping(
        self, queryset, name, value
    ):
        if not value:
            return queryset
        return queryset.filter(opportunity_crm_ids=[])

    def filter_projects_by_mapped_sow_type(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(sow_document__doc_type=value)


class PMOMetricsNotesFilterset(filters.FilterSet):
    author = ListFilter()

    class Meta:
        model = PMOMetricsNotes
        fields = [
            "author",
        ]
