from accounts.mail import BaseEmailMessage


class GenericPMOMail(BaseEmailMessage):
    template_name = "generic_email_template.html"
