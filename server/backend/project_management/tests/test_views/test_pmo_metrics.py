import json
from typing import List, Dict
from datetime import datetime, timedelta
from django.urls import reverse
from rest_framework.request import Request
from rest_framework.response import Response
from faker import Faker
from accounts.models import Customer
from accounts.tests.factories import (
    CustomerFactory,
    ProfileFactory,
)
from accounts.tests.factories import ProviderUserFactory
from project_management.models.pmo_settings import (
    OverallStatusSetting,
    ProjectType,
)
from project_management.models.project import Project
from project_management.models.pmo_metrics import PMOMetricsNotes
from project_management.models.pmo_settings import (
    OverallStatusSetting,
    ProjectType,
)
from project_management.tests.factories import (
    ProjectFactory,
    OverallStatusSettingFactory,
    ProjectTypeFactory,
    ProjectBoardFactory,
    PMOMetricsNotesFactory,
)
from project_management.views.pmo_metrics import (
    PMOMetricsReviewTabDataAPIView,
    PMOMetricsBudgetReviewTabAPIView,
    PMOMetricsManagerProjectsCountChartAPIView,
    PMOMetricsProjectsCountByTypeChartAPIView,
    PMOMetricsOverallStatusProjectsCountChartAPIView,
    PMOProjectCountByEstimatedEndDateRangeChartAPIView,
    PMOMetricsNotesListCreateAPIView,
    PMOMetricsNotesRetrieveUpdateDestroyAPIView,
)
from sales.tests.utils import render_response
from test_utils import (
    BaseProviderTestCase,
    TestClientMixin,
)
from django.conf import settings
from sales.tests.utils import add_query_params_to_url
from project_management.tests.test_utils import (
    get_mock_actual_and_budget_hour_values,
)


class TestPMOMetricsReviewTabDataAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsReviewTabDataAPIView
    """

    pmo_metrics_table_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-review"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_pmo_metrics_table_data_listing(self):
        """
        Test listing of PMO metrics table data listing.
        """
        fake = Faker()
        project_manager: User = ProviderUserFactory(provider=self.provider)
        profile: Profile = ProfileFactory(
            user=project_manager, profile_pic=None
        )
        customer: Customer = CustomerFactory(provider=self.provider)
        overall_status: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, is_open=True
        )
        # Hardcode actual_budget and hard_budget so we know progress_percent=50
        project: Project = ProjectFactory.create(
            provider=self.provider,
            customer=customer,
            project_manager=project_manager,
            overall_status=overall_status,
            actual_hours=5,
            hours_budget=10,
            project_updates=dict(
                estimated_hours_to_complete=fake.pyint(
                    min_value=0, max_value=999
                ),
                note=fake.text(max_nb_chars=10),
                updates=fake.text(max_nb_chars=10),
            ),
        )
        project_details: Dict = dict(
            id=project.id,
            crm_id=project.crm_id,
            title=project.title,
            customer_id=project.customer_id,
            customer_name=project.customer.name,
            project_manager_id=str(project.project_manager_id),
            project_manager_name=project.project_manager.name,
            project_manager_pic=project.project_manager.profile.profile_pic,
            type_id=project.type_id,
            type_title=project.type.title,
            overall_status_id=project.overall_status_id,
            overall_status_title=project.overall_status.title,
            overall_status_color=project.overall_status.color,
            project_updates=project.project_updates,
            progress_percent=50,
            opportunity_crm_ids=project.opportunity_crm_ids,
            sow_document_id=project.sow_document_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pmo_metrics_table_url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        project_data: Dict = response.data.get("results")[0]
        actual_response: Dict = dict(
            id=project_data.get("id"),
            crm_id=project_data.get("crm_id"),
            title=project_data.get("title"),
            customer_id=project_data.get("customer_id"),
            customer_name=project_data.get("customer_name"),
            project_manager_id=project_data.get("project_manager_id"),
            project_manager_name=project_data.get("project_manager_name"),
            project_manager_pic=None,
            type_id=project_data.get("type_id"),
            type_title=project_data.get("type_title"),
            overall_status_id=project_data.get("overall_status_id"),
            overall_status_title=project_data.get("overall_status_title"),
            overall_status_color=project_data.get("overall_status_color"),
            project_updates=project_data.get("project_updates"),
            progress_percent=50,
            opportunity_crm_ids=project_data.get("opportunity_crm_ids"),
            sow_document_id=project_data.get("sow_document_id"),
        )
        self.assertEqual(project_details, actual_response)

    def test_project_type_filter_on_pmo_metric_review_tab(self):
        """
        Test project type filter on review tab projects listing.
        """
        project_type_count: int = 2
        project_type: ProjectType = ProjectTypeFactory.create(
            provider=self.provider, title="Mock project type"
        )
        ProjectFactory.create_batch(
            project_type_count, provider=self.provider, type=project_type
        )
        ProjectFactory.create(provider=self.provider)
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url, query_params=dict(type=project_type.id)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), project_type_count)

    def test_project_manager_filter_on_pmo_metric_review_tab(self):
        """
        Test project manager filter on review tab projects listing.
        """
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider
        )
        project_manager_project_count: int = 2
        ProjectFactory.create_batch(
            project_manager_project_count,
            provider=self.provider,
            project_manager=mock_project_manager,
        )
        ProjectFactory.create(
            provider=self.provider, project_manager=self.user
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(project_manager=str(mock_project_manager.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_project_count
        )

    def test_estimated_end_date_filter_on_pmo_metric_review_tab(self):
        """
        Test estimated end date filter on PMO metric review tab projects listing.
        """
        fake = Faker()
        today = datetime.now()
        past_7_day = today - timedelta(days=7)
        past_15_day = today - timedelta(days=15)

        datetime_within_last_7_days = past_7_day + timedelta(days=3)
        datetime_between_last_15_and_7_days = past_15_day + timedelta(days=3)
        date_range_project_count: int = 1
        ProjectFactory.create_batch(
            date_range_project_count,
            provider=self.provider,
            estimated_end_date=datetime_within_last_7_days,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=datetime_between_last_15_and_7_days,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(
                estimated_end_date_after=past_7_day.strftime("%Y-%m-%d"),
                estimated_end_date_before=today.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), date_range_project_count)

    def test_overall_status_filter_on_pmo_metric_review_table(self):
        """
        Test overall status filter on PMO metric review tab projects listing.
        """
        mock_overall_status: OverallStatusSetting = (
            OverallStatusSettingFactory(
                provider=self.provider, title="Mock overall satus"
            )
        )
        mock_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_status_project_count,
            provider=self.provider,
            overall_status=mock_overall_status,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(overall_status=mock_overall_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), mock_status_project_count)

    def test_unmapped_sow_document_filter_on_pmo_metric_review_table(self):
        """
        Test unmapped sow filter on PMO metric review tab projects listing.
        """
        project_without_sow_mapping_count: int = 2
        ProjectFactory.create_batch(
            project_without_sow_mapping_count,
            provider=self.provider,
            sow_document=None,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url, query_params=dict(unmapped_sow="true")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_without_sow_mapping_count
        )

    def test_unmapped_opportunity_filter_on_pmo_metric_review_table(self):
        """
        Test unmapped opportunity CRM IDS filter on PMO metric review tab projects listing.
        """
        project_without_sow_opp_mapping_count: int = 2
        ProjectFactory.create_batch(
            project_without_sow_opp_mapping_count,
            provider=self.provider,
            opportunity_crm_ids=[],
        )
        ProjectFactory.create(
            provider=self.provider, opportunity_crm_ids=[1, 2, 3]
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url, query_params=dict(unmapped_opp="true")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_without_sow_opp_mapping_count
        )

    def test_search_on_customer_name_in_pmo_metric_review_table(self):
        """
        Test search on customer name on PMO metric review tab projects listing.
        """
        mock_customer_name: str = "Mock customer"
        mock_customer_projects_count: int = 2
        mock_customer: Customer = CustomerFactory(
            provider=self.provider, name=mock_customer_name
        )
        ProjectFactory.create_batch(
            mock_customer_projects_count,
            provider=self.provider,
            customer=mock_customer,
        )
        ProjectFactory.create(provider=self.provider)
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(search=mock_customer_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), mock_customer_projects_count
        )

    def test_search_on_project_title_in_pmo_metric_review_table(self):
        """
        Test search on project title on PMO metric review tab projects listing.
        """
        mock_project_title: str = "Mock project title"
        ProjectFactory.create(
            title=mock_project_title,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(search=mock_project_title),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), 1)

    def test_search_on_project_manager_first_name_in_pmo_metric_review_table(
        self,
    ):
        """
        Test search on project manager first name on PMO metric review tab projects listing.
        """
        mock_first_name: str = "MockName"
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider, first_name=mock_first_name
        )
        project_manager_projects_count: int = 2
        ProjectFactory.create_batch(
            project_manager_projects_count,
            provider=self.provider,
            project_manager=mock_project_manager,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(search=mock_first_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_projects_count
        )

    def test_search_on_project_manager_last_name_in_pmo_metric_review_table(
        self,
    ):
        """
        Test search on project manager last name on PMO metric review tab projects listing.
        """
        mock_last_name: str = "MockName"
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider, last_name=mock_last_name
        )
        project_manager_projects_count: int = 2
        ProjectFactory.create_batch(
            project_manager_projects_count,
            provider=self.provider,
            project_manager=mock_project_manager,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(search=mock_last_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_projects_count
        )

    def test_search_on_overall_status_title_in_pmo_metric_review_table(self):
        """
        Test search on overall status title on PMO metric review tab projects listing.
        """
        mock_title: str = "Mock overall status"
        mock_overall_status: OverallStatusSetting = (
            OverallStatusSettingFactory(
                provider=self.provider, title=mock_title
            )
        )
        overall_status_projects_count: int = 2
        ProjectFactory.create_batch(
            overall_status_projects_count,
            provider=self.provider,
            overall_status=mock_overall_status,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(search=mock_title),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), overall_status_projects_count
        )

    def test_alphabetical_ordering_by_project_title_in_pmo_metric_review_table(
        self,
    ):
        """
        Test alphabetical ordering by project title on PMO metric review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, title="Mock title A"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, title="Mock title B"
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="title"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_ealphabetical_ordering_by_project_title_in_pmo_metric_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by project title on PMO metric review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, title="Mock title A"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, title="Mock title B"
        )
        expected_order: List[int] = [
            project_2.id,
            project_1.id,
        ]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-title"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_progress_percent_in_pmo_metric_review_table(
        self,
    ):
        """
        Test ascending ordering by progress percent on PMO metric review tab projects listing.
        """
        # Hardcode actual hours and hours_budget values as needed for progress_percent
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=2, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="progress_percent"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_progress_percent_in_pmo_metric_review_table(
        self,
    ):
        """
        Test descending ordering by progress percent on PMO metric review tab projects listing.
        """
        # Hardcode actual hours and hours_budget values as needed for progress_percent
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=2, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-progress_percent"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ordering_by_oldest_estimated_end_date_in_pmo_metric_review_table(
        self,
    ):
        """
        Test ascending ordering by estimated end date on PMO metric review tab projects listing.
        """
        current_datetime = datetime.now()
        past_datetime = current_datetime - timedelta(days=3)
        project_1: Project = ProjectFactory(
            provider=self.provider, estimated_end_date=current_datetime
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, estimated_end_date=past_datetime
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="estimated_end_date"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ordering_by_lastest_estimated_end_date_in_pmo_metric_review_table(
        self,
    ):
        """
        Test ascending ordering by estimated end date on PMO metric review tab projects listing.
        """
        current_datetime = datetime.now()
        past_datetime = current_datetime - timedelta(days=3)
        project_1: Project = ProjectFactory(
            provider=self.provider, estimated_end_date=current_datetime
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, estimated_end_date=past_datetime
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-estimated_end_date"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_overall_status_in_pmo_metric_review_table(
        self,
    ):
        """
        Test alphabetical ordering by overall status on PMO metric review tab projects listing.
        """
        overall_status_1: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, overall_status=overall_status_1
        )
        overall_status_2: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, overall_status=overall_status_2
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="status"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_overall_status_in_pmo_metric_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by overall status on PMO metric review tab projects listing.
        """
        overall_status_1: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, overall_status=overall_status_1
        )
        overall_status_2: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, overall_status=overall_status_2
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-status"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_project_manager_name_in_pmo_metric_review_table(
        self,
    ):
        """
        Test alphabetical ordering by project manager first name on PMO metric review tab projects listing.
        """
        project_manager_1: User = ProviderUserFactory(
            provider=self.provider, first_name="A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, project_manager=project_manager_1
        )
        project_manager_2: User = ProviderUserFactory(
            provider=self.provider, first_name="B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, project_manager=project_manager_2
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="project_manager_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_project_manager_name_in_pmo_metric_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by project manager first name on PMO metric review tab projects listing.
        """
        project_manager_1: User = ProviderUserFactory(
            provider=self.provider, first_name="A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, project_manager=project_manager_1
        )
        project_manager_2: User = ProviderUserFactory(
            provider=self.provider, first_name="B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, project_manager=project_manager_2
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-project_manager_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_estimated_hours_to_complete_in_pmo_metric_review_table(
        self,
    ):
        """
        Test ascending ordering by estimated hours to complete on PMO metric review tab projects listing.
        """
        fake = Faker()
        project_1: Project = ProjectFactory(
            provider=self.provider,
            project_updates=dict(
                note=fake.text(max_nb_chars=5),
                updates=fake.text(max_nb_chars=5),
                estimated_hours_to_complete=5,
            ),
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            project_updates=dict(
                note=fake.text(max_nb_chars=5),
                updates=fake.text(max_nb_chars=5),
                estimated_hours_to_complete=10,
            ),
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="estimated_hours_to_complete"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_estimated_hours_to_complete_in_pmo_metric_review_table(
        self,
    ):
        """
        Test descending ordering by estimated hours to complete on PMO metric review tab projects listing.
        """
        fake = Faker()
        project_1: Project = ProjectFactory(
            provider=self.provider,
            project_updates=dict(
                note=fake.text(max_nb_chars=5),
                updates=fake.text(max_nb_chars=5),
                estimated_hours_to_complete=5,
            ),
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            project_updates=dict(
                note=fake.text(max_nb_chars=5),
                updates=fake.text(max_nb_chars=5),
                estimated_hours_to_complete=10,
            ),
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-estimated_hours_to_complete"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_customer_name_on_pmo_metric_review_table(
        self,
    ):
        """
        Test alphabetical ordering by customer name on PMO metric review tab projects listing.
        """
        customer_1: Customer = CustomerFactory(
            name="Customer A", provider=self.provider
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, customer=customer_1
        )
        customer_2: Customer = CustomerFactory(
            name="Customer B", provider=self.provider
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, customer=customer_2
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="customer_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_customer_name_on_pmo_metric_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by customer name on PMO metric review tab projects listing.
        """
        customer_1: Customer = CustomerFactory(
            name="Customer A", provider=self.provider
        )
        project_1: Project = ProjectFactory(
            provider=self.provider, customer=customer_1
        )
        customer_2: Customer = CustomerFactory(
            name="Customer B", provider=self.provider
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, customer=customer_2
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_table_url,
            query_params=dict(ordering="-customer_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsReviewTabDataAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)


class TestPMOMetricsBudgetReviewTabAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsBudgetReviewTabAPIView
    """

    pmo_budget_review_tab_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-review"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_pmo_metric_budget_review_tab_listing(self):
        """
        Test PMO metrics budget review tab projects listing.
        """
        project_manager: User = ProviderUserFactory(provider=self.provider)
        profile: Profile = ProfileFactory(
            user=project_manager, profile_pic=None
        )
        customer: Customer = CustomerFactory(provider=self.provider)
        project: Project = ProjectFactory.create(
            provider=self.provider,
            customer=customer,
            project_manager=project_manager,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pmo_budget_review_tab_url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_project_type_filter_on_pmo_metric_budget_review_tab(self):
        """
        Test project type filter on budget review tab projects listing.
        """
        project_type_count: int = 2
        project_type: ProjectType = ProjectTypeFactory.create(
            provider=self.provider, title="Mock project type"
        )
        # For budget review tab listing, remaining hours must be less than 10, so
        # hard coded values of actual_hours and hours_budget
        ProjectFactory.create_batch(
            project_type_count,
            provider=self.provider,
            type=project_type,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(provider=self.provider)
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(type=project_type.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), project_type_count)

    def test_project_manager_filter_on_pmo_metric_budget_review_tab(self):
        """
        Test project manager filter on budget review tab projects listing.
        """
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider
        )
        project_manager_project_count: int = 2
        ProjectFactory.create_batch(
            project_manager_project_count,
            provider=self.provider,
            project_manager=mock_project_manager,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider, project_manager=self.user
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(project_manager=str(mock_project_manager.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_project_count
        )

    def test_estimated_end_date_filter_on_pmo_metric_budget_review_tab(self):
        """
        Test estimated end date filter on PMO metric budget review tab projects listing.
        """
        fake = Faker()
        today = datetime.now()
        past_7_day = today - timedelta(days=7)
        past_15_day = today - timedelta(days=15)
        datetime_within_past_7_days = past_7_day + timedelta(days=3)
        datetime_between_past_15_and_7_days = past_15_day + timedelta(days=3)
        date_range_project_count: int = 1
        ProjectFactory.create_batch(
            date_range_project_count,
            provider=self.provider,
            estimated_end_date=datetime_within_past_7_days,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
            estimated_end_date=datetime_between_past_15_and_7_days,
            **get_mock_actual_and_budget_hour_values()
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(
                estimated_end_date_after=past_7_day.strftime("%Y-%m-%d"),
                estimated_end_date_before=today.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), date_range_project_count)

    def test_overall_status_filter_on_pmo_metric_budget_review_table(self):
        """
        Test overall status filter on PMO metric budget review tab projects listing.
        """
        mock_overall_status: OverallStatusSetting = (
            OverallStatusSettingFactory(
                provider=self.provider, title="Mock overall satus"
            )
        )
        mock_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_status_project_count,
            provider=self.provider,
            overall_status=mock_overall_status,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(overall_status=mock_overall_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), mock_status_project_count)

    def test_unmapped_sow_document_filter_on_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test unmapped sow filter on PMO metric budget review tab projects listing.
        """
        project_without_sow_mapping_count: int = 2
        ProjectFactory.create_batch(
            project_without_sow_mapping_count,
            provider=self.provider,
            sow_document=None,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(unmapped_sow="true"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_without_sow_mapping_count
        )

    def test_unmapped_opportunity_filter_on_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test unmapped opportunity CRM IDS filter on PMO metric budget review tab projects listing.
        """
        project_without_sow_opp_mapping_count: int = 2
        ProjectFactory.create_batch(
            project_without_sow_opp_mapping_count,
            provider=self.provider,
            opportunity_crm_ids=[],
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
            opportunity_crm_ids=[1, 2, 3],
            **get_mock_actual_and_budget_hour_values()
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(unmapped_opp="true"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_without_sow_opp_mapping_count
        )

    def test_search_on_customer_name_in_pmo_metric_budget_review_table(self):
        """
        Test search on customer name on PMO metric budget review tab projects listing.
        """
        mock_customer_name: str = "Mock customer"
        mock_customer_projects_count: int = 2
        mock_customer: Customer = CustomerFactory(
            provider=self.provider, name=mock_customer_name
        )
        ProjectFactory.create_batch(
            mock_customer_projects_count,
            provider=self.provider,
            customer=mock_customer,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider, **get_mock_actual_and_budget_hour_values()
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(search=mock_customer_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), mock_customer_projects_count
        )

    def test_search_on_project_title_in_pmo_metric_budget_review_table(self):
        """
        Test search on project title on PMO metric budget review tab projects listing.
        """
        mock_project_title: str = "Mock project title"
        ProjectFactory.create(
            title=mock_project_title,
            provider=self.provider,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(search=mock_project_title),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(response.data.get("count"), 1)

    def test_search_on_project_manager_first_name_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test search on project manager first name on PMO metric budget review tab projects listing.
        """
        mock_first_name: str = "MockName"
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider, first_name=mock_first_name
        )
        project_manager_projects_count: int = 2
        ProjectFactory.create_batch(
            project_manager_projects_count,
            provider=self.provider,
            project_manager=mock_project_manager,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(search=mock_first_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_projects_count
        )

    def test_search_on_project_manager_last_name_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test search on project manager last name on PMO metric review tab projects listing.
        """
        mock_last_name: str = "MockName"
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider, last_name=mock_last_name
        )
        project_manager_projects_count: int = 2
        ProjectFactory.create_batch(
            project_manager_projects_count,
            provider=self.provider,
            project_manager=mock_project_manager,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(search=mock_last_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), project_manager_projects_count
        )

    def test_search_on_overall_status_title_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test search on overall status title on PMO metric budget review tab projects listing.
        """
        mock_title: str = "Mock overall status"
        mock_overall_status: OverallStatusSetting = (
            OverallStatusSettingFactory(
                provider=self.provider, title=mock_title
            )
        )
        overall_status_projects_count: int = 2
        ProjectFactory.create_batch(
            overall_status_projects_count,
            provider=self.provider,
            overall_status=mock_overall_status,
            **get_mock_actual_and_budget_hour_values()
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(search=mock_title),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(
            response.data.get("count"), overall_status_projects_count
        )

    def test_alphabetical_ordering_by_project_title_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test alphabetical ordering by project title on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider,
            title="Mock title A",
            **get_mock_actual_and_budget_hour_values()
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            title="Mock title B",
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="title"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_ealphabetical_ordering_by_project_title_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by project title on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider,
            title="Mock title A",
            **get_mock_actual_and_budget_hour_values()
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            title="Mock title B",
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [
            project_2.id,
            project_1.id,
        ]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-title"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_hours_budget_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test ascending ordering by hours budget on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, hours_budget=10, actual_hours=5
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, hours_budget=15, actual_hours=10
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="hours_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_hours_budget_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test descending ordering by hours budget on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, hours_budget=10, actual_hours=5
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, hours_budget=15, actual_hours=10
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-hours_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_actual_hours_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test ascending ordering by actual hours on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=10, hours_budget=15
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="hours_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_actual_hours_in_pmo_metric_budget_review_table(
        self,
    ):
        """
        Test descending ordering by actual hours on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=10, hours_budget=15
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-hours_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_remaining_hours_on_metric_budget_review_table(
        self,
    ):
        """
        Test ascending ordering by reamining hours PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=7
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="remaining_hours"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_remaining_hours_on_metric_budget_review_table(
        self,
    ):
        """
        Test descending ordering by reamining hours on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=7
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-remaining_hours"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_ascending_ordering_by_percent_of_budget_on_metric_budget_review_table(
        self,
    ):
        """
        Test ascending ordering by percent of budget on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=7, hours_budget=10
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="percent_of_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_descending_ordering_by_percent_of_budget_on_metric_budget_review_table(
        self,
    ):
        """
        Test descending ordering by percent of budget on PMO metric budget review tab projects listing.
        """
        project_1: Project = ProjectFactory(
            provider=self.provider, actual_hours=5, hours_budget=10
        )
        project_2: Project = ProjectFactory(
            provider=self.provider, actual_hours=7, hours_budget=10
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-percent_of_budget"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_customer_name_on_budget_review_table(
        self,
    ):
        """
        Test alphabetical ordering by customer name on PMO metric budget review tab projects listing.
        """
        customer_1: Customer = CustomerFactory(
            name="Customer A", provider=self.provider
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            customer=customer_1,
            **get_mock_actual_and_budget_hour_values()
        )
        customer_2: Customer = CustomerFactory(
            name="Customer B", provider=self.provider
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            customer=customer_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="customer_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_customer_name_on_budget_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by customer name on PMO metric budget review tab projects listing.
        """
        customer_1: Customer = CustomerFactory(
            name="Customer A", provider=self.provider
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            customer=customer_1,
            **get_mock_actual_and_budget_hour_values()
        )
        customer_2: Customer = CustomerFactory(
            name="Customer B", provider=self.provider
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            customer=customer_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-customer_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_overall_status_on_on_budget_review_table(
        self,
    ):
        """
        Test alphabetical ordering by overall status title on PMO metric budget review tab projects listing.
        """
        overall_status_1: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            overall_status=overall_status_1,
            **get_mock_actual_and_budget_hour_values()
        )
        overall_status_2: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            overall_status=overall_status_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="status"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_overall_status_on_budget_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by overall status title on PMO metric budget review tab projects listing.
        """
        overall_status_1: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status A"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            overall_status=overall_status_1,
            **get_mock_actual_and_budget_hour_values()
        )
        overall_status_2: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Status B"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            overall_status=overall_status_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-status"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = render_response(view(request))
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_alphabetical_ordering_by_project_manager_name_on_budget_review_table(
        self,
    ):
        """
        Test alphabetical ordering by project manager first name on PMO metric budget review tab.
        """
        project_manager_1: User = ProviderUserFactory(
            provider=self.provider, first_name="A", last_name="B"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            project_manager=project_manager_1,
            **get_mock_actual_and_budget_hour_values()
        )
        project_manager_2: User = ProviderUserFactory(
            provider=self.provider, first_name="C", last_name="D"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            project_manager=project_manager_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_1.id, project_2.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="project_manager_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)

    def test_reverse_alphabetical_ordering_by_project_manager_name_on_budget_review_table(
        self,
    ):
        """
        Test reverse alphabetical ordering by project manager first name on PMO metric budget review tab.
        """
        project_manager_1: User = ProviderUserFactory(
            provider=self.provider, first_name="A", last_name="B"
        )
        project_1: Project = ProjectFactory(
            provider=self.provider,
            project_manager=project_manager_1,
            **get_mock_actual_and_budget_hour_values()
        )
        project_manager_2: User = ProviderUserFactory(
            provider=self.provider, first_name="C", last_name="D"
        )
        project_2: Project = ProjectFactory(
            provider=self.provider,
            project_manager=project_manager_2,
            **get_mock_actual_and_budget_hour_values()
        )
        expected_order: List[int] = [project_2.id, project_1.id]
        url: str = add_query_params_to_url(
            self.pmo_budget_review_tab_url,
            query_params=dict(ordering="-project_manager_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsBudgetReviewTabAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            project.get("id") for project in response.data.get("results")
        ]
        self.assertListEqual(actual_order, expected_order)


class TestPMOMetricsManagerProjectsCountChartSerailizer(BaseProviderTestCase):
    """
    Test view: PMOMetricsManagerProjectsCountChartSerailizer
    """

    project_count_by_manager_chart_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-manager-projects-count-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_projects_count_by_manager_chart(self):
        """
        Test projects count by manager chart listing.
        """
        mock_manager_1: User = ProviderUserFactory(
            provider=self.provider,
            first_name="mock1FirstName",
            last_name="mock1LastName",
        )
        mock_manager_1_project_count: int = 1
        ProjectFactory.create_batch(
            mock_manager_1_project_count,
            project_manager=mock_manager_1,
            provider=self.provider,
        )
        mock_manager_2: User = ProviderUserFactory(
            provider=self.provider,
            first_name="moc2FirstName",
            last_name="mock2LastName",
        )
        mock_manager_2_project_count: int = 2
        ProjectFactory.create_batch(
            mock_manager_2_project_count,
            project_manager=mock_manager_2,
            provider=self.provider,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.project_count_by_manager_chart_url,
            self.provider,
            self.user,
        )
        expected_response_data: List = [
            dict(
                project_manager_id=str(mock_manager_2.id),
                project_manager_name=mock_manager_2.name,
                count=mock_manager_2_project_count,
            ),
            dict(
                project_manager_id=str(mock_manager_1.id),
                project_manager_name=mock_manager_1.name,
                count=mock_manager_1_project_count,
            ),
        ]
        view = PMOMetricsManagerProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        self.assertListEqual(expected_response_data, response.data)

    def test_project_type_filter_on_project_count_by_manager_chart(self):
        """
        Test project type filter on project count by project managers chart.
        """
        mock_project_type: ProjectType = ProjectTypeFactory(
            title="Mock project type", provider=self.provider
        )
        mock_project_type_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_type_project_count,
            provider=self.provider,
            type=mock_project_type,
            project_manager=self.user,
        )
        ProjectFactory.create(
            provider=self.provider, project_manager=self.user
        )
        url: str = add_query_params_to_url(
            self.project_count_by_manager_chart_url,
            query_params=dict(type=mock_project_type.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsManagerProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(
            mock_project_type_project_count, response.data[0].get("count")
        )

    def test_project_status_filter_on_project_count_by_manager_chart(self):
        """
        Test project status filter on project count by project managers chart.
        """
        mock_project_status: OverallStatusSetting = (
            OverallStatusSettingFactory.create(
                provider=self.provider, title="Mock project status"
            )
        )
        mock_project_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_status_project_count,
            provider=self.provider,
            overall_status=mock_project_status,
            project_manager=self.user,
        )
        ProjectFactory.create(
            provider=self.provider, project_manager=self.user
        )
        url: str = add_query_params_to_url(
            self.project_count_by_manager_chart_url,
            query_params=dict(overall_status=mock_project_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsManagerProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(
            mock_project_status_project_count, response.data[0].get("count")
        )

    def test_project_manager_filter_on_project_count_by_manager_chart(self):
        """
        Test project manager filter on project count by project managers chart.
        """
        mock_manager_1: User = ProviderUserFactory(
            provider=self.provider,
            first_name="mock1FirstName",
            last_name="mock1LastName",
        )
        mock_manager_1_project_count: int = 2
        ProjectFactory.create_batch(
            mock_manager_1_project_count,
            project_manager=mock_manager_1,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.project_count_by_manager_chart_url,
            query_params=dict(project_manager=str(mock_manager_1.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsManagerProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        # Response will contain data only for single project manager
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data[0].get("count"), mock_manager_1_project_count
        )

    def test_estimated_end_date_range_filter_on_project_count_by_manager_chart(
        self,
    ):
        """
        Test estimated end date range filter on project count by project managers chart.
        """
        today = datetime.now()
        last_7_day = today - timedelta(days=7)
        last_20_day = today - timedelta(days=20)
        # Create projects with estimated end date within last 7 days
        last_5_day = today - timedelta(days=5)
        ProjectFactory.create_batch(
            2, provider=self.provider, estimated_end_date=last_5_day
        )
        # Create projects with estimated end date prior to last 7 days
        ProjectFactory.create_batch(
            1, provider=self.provider, estimated_end_date=last_20_day
        )
        url: str = add_query_params_to_url(
            self.project_count_by_manager_chart_url,
            query_params=dict(
                estimated_end_date_after=last_7_day.strftime("%Y-%m-%d"),
                estimated_end_date_before=today.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsManagerProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        project_count: int = 0
        for item in response.data:
            project_count += item.get("count")
        self.assertEqual(project_count, 2)


class TestPMOMetricsProjectsCountByTypeChartAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsProjectsCountByTypeChartAPIView
    """

    projects_count_by_project_type_chart_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-projects-by-type-count-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_on_project_count_by_project_type_chart(self):
        """
        Test listing on projects count by project type chart.
        """
        # Create a scenario where there are projects with different types.
        mock_type_1: ProjectType = ProjectTypeFactory(
            provider=self.provider,
            title="mockType1",
        )
        mock_type_2: ProjectType = ProjectTypeFactory(
            provider=self.provider,
            title="mockType2",
        )
        mock_type_1_projects_count: int = 1
        mock_type_2_projects_count: int = 2
        ProjectFactory.create_batch(
            mock_type_1_projects_count,
            type=mock_type_1,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            mock_type_2_projects_count,
            type=mock_type_2,
            provider=self.provider,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.projects_count_by_project_type_chart_url,
            self.provider,
            self.user,
        )
        # As response is a list, order of element matters
        expected_response_data: List = [
            dict(
                project_type=mock_type_2.title,
                project_type_id=mock_type_2.id,
                count=mock_type_2_projects_count,
            ),
            dict(
                project_type=mock_type_1.title,
                project_type_id=mock_type_1.id,
                count=mock_type_1_projects_count,
            ),
        ]
        view = PMOMetricsProjectsCountByTypeChartAPIView().as_view()
        response: Response = view(request)
        self.assertListEqual(expected_response_data, response.data)

    def test_project_type_filter_on_project_count_by_project_type_chart(self):
        """
        Test project type filter on projects count by project type chart.
        """
        mock_project_type: ProjectType = ProjectTypeFactory(
            title="Mock type", provider=self.provider
        )
        mock_project_type_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_type_project_count,
            provider=self.provider,
            type=mock_project_type,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_type_chart_url,
            query_params=dict(type=mock_project_type.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsProjectsCountByTypeChartAPIView().as_view()
        response: Response = view(request)
        # Response should contain data for only 1 project type
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            mock_project_type_project_count, response.data[0].get("count")
        )

    def test_project_status_filter_on_project_count_by_project_type_chart(
        self,
    ):
        """
        Test project status filter on projects count by project type chart.
        """
        mock_project_status: OverallStatusSetting = (
            OverallStatusSettingFactory.create(
                provider=self.provider, title="Mock project status"
            )
        )
        mock_project_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_status_project_count,
            provider=self.provider,
            overall_status=mock_project_status,
        )
        ProjectFactory.create(
            provider=self.provider, project_manager=self.user
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_type_chart_url,
            query_params=dict(overall_status=mock_project_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsProjectsCountByTypeChartAPIView().as_view()
        response: Response = view(request)
        projects_count: int = 0
        for item in response.data:
            projects_count += item.get("count")
        self.assertEqual(mock_project_status_project_count, projects_count)

    def test_project_manager_filter_on_project_count_by_project_type_chart(
        self,
    ):
        """
        Test project manager filter on projects count by project type chart.
        """
        mock_manager_1: User = ProviderUserFactory(
            provider=self.provider,
        )
        mock_manager_1_project_count: int = 2
        ProjectFactory.create_batch(
            mock_manager_1_project_count,
            project_manager=mock_manager_1,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_type_chart_url,
            query_params=dict(project_manager=str(mock_manager_1.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsProjectsCountByTypeChartAPIView().as_view()
        response: Response = view(request)
        projects_count: int = 0
        for item in response.data:
            projects_count += item.get("count")
        self.assertEqual(mock_manager_1_project_count, projects_count)

    def test_estimated_end_date_range_filter_on_project_count_by_project_type_chart(
        self,
    ):
        """
        Test estimated end date range filter on projects count by project type chart.
        """
        today = datetime.now()
        last_7_day = today - timedelta(days=7)
        last_20_day = today - timedelta(days=20)
        # Create projects with estimated end date within last 7 days
        last_5_day = today - timedelta(days=5)
        date_range_projects_count: int = 2
        ProjectFactory.create_batch(
            date_range_projects_count,
            provider=self.provider,
            estimated_end_date=last_5_day,
        )
        # Create projects with estimated end date prior to last 7 days
        ProjectFactory.create_batch(
            2, provider=self.provider, estimated_end_date=last_20_day
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_type_chart_url,
            query_params=dict(
                estimated_end_date_after=last_7_day.strftime("%Y-%m-%d"),
                estimated_end_date_before=today.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsProjectsCountByTypeChartAPIView().as_view()
        response: Response = view(request)
        project_count: int = 0
        for item in response.data:
            project_count += item.get("count")
        self.assertEqual(project_count, date_range_projects_count)


class TestPMOMetricsOverallStatusProjectsCountChartAPIView(
    BaseProviderTestCase
):
    """
    Test view: PMOMetricsOverallStatusProjectsCountChartAPIView
    """

    projects_count_by_project_status_chart_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-projects-count-by-status-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_on_project_count_by_project_status_chart(self):
        """
        Test listing on projects count by project status chart.
        """
        # Create a scenario where there are projects with different statuses.
        mock_status_1: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="mock status 1", color="f8af03"
        )
        mock_status_2: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="mock status 2", color="f8af21"
        )
        mock_status_1_projects_count: int = 1
        mock_status_2_projects_count: int = 2
        ProjectFactory.create_batch(
            mock_status_1_projects_count,
            overall_status=mock_status_1,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            mock_status_2_projects_count,
            overall_status=mock_status_2,
            provider=self.provider,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.projects_count_by_project_status_chart_url,
            self.provider,
            self.user,
        )
        # As response is a list, order of element matters
        expected_response_data: List = [
            dict(
                project_status=mock_status_2.title,
                project_status_id=mock_status_2.id,
                count=mock_status_2_projects_count,
                project_status_color=mock_status_2.color,
            ),
            dict(
                project_status=mock_status_1.title,
                project_status_id=mock_status_1.id,
                count=mock_status_1_projects_count,
                project_status_color=mock_status_1.color,
            ),
        ]
        view = PMOMetricsOverallStatusProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        self.assertListEqual(expected_response_data, response.data)

    def test_project_type_filter_on_project_count_by_project_status_chart(
        self,
    ):
        """
        Test project type filter on projects count by project status chart.
        """
        mock_project_type: ProjectType = ProjectTypeFactory(
            title="Mock type", provider=self.provider
        )
        mock_project_type_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_type_project_count,
            provider=self.provider,
            type=mock_project_type,
        )
        ProjectFactory.create(
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_status_chart_url,
            query_params=dict(type=mock_project_type.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsOverallStatusProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        actual_projects_count: int = 0
        for item in response.data:
            actual_projects_count += item.get("count")
        self.assertEqual(
            mock_project_type_project_count, actual_projects_count
        )

    def test_project_status_filter_on_project_count_by_project_status_chart(
        self,
    ):
        """
        Test project status filter on projects count by project status chart.
        """
        mock_project_status: OverallStatusSetting = (
            OverallStatusSettingFactory.create(
                provider=self.provider, title="Mock project status"
            )
        )
        mock_project_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_project_status_project_count,
            provider=self.provider,
            overall_status=mock_project_status,
        )
        ProjectFactory.create(provider=self.provider)
        url: str = add_query_params_to_url(
            self.projects_count_by_project_status_chart_url,
            query_params=dict(overall_status=mock_project_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsOverallStatusProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        # There should be only 1 item in response as filtered for specific status
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            mock_project_status_project_count, response.data[0].get("count")
        )

    def test_project_manager_filter_on_project_count_by_project_status_chart(
        self,
    ):
        """
        Test project manager filter on projects count by project status chart.
        """
        mock_manager_1: User = ProviderUserFactory(
            provider=self.provider,
        )
        mock_manager_1_project_count: int = 2
        ProjectFactory.create_batch(
            mock_manager_1_project_count,
            project_manager=mock_manager_1,
            provider=self.provider,
        )
        ProjectFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_status_chart_url,
            query_params=dict(project_manager=str(mock_manager_1.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsOverallStatusProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        actual_projects_count: int = 0
        for item in response.data:
            actual_projects_count += item.get("count")
        self.assertEqual(mock_manager_1_project_count, actual_projects_count)

    def test_estimated_end_date_range_filter_on_project_count_by_project_status_chart(
        self,
    ):
        """
        Test estimated end date range filter on projects count by project status chart.
        """
        today = datetime.now()
        last_7_day = today - timedelta(days=7)
        last_20_day = today - timedelta(days=20)
        # Create projects with estimated end date within last 7 days
        last_5_day = today - timedelta(days=5)
        date_range_projects_count: int = 2
        ProjectFactory.create_batch(
            date_range_projects_count,
            provider=self.provider,
            estimated_end_date=last_5_day,
        )
        # Create projects with estimated end date prior to last 7 days
        ProjectFactory.create_batch(
            2, provider=self.provider, estimated_end_date=last_20_day
        )
        url: str = add_query_params_to_url(
            self.projects_count_by_project_status_chart_url,
            query_params=dict(
                estimated_end_date_after=last_7_day.strftime("%Y-%m-%d"),
                estimated_end_date_before=today.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsOverallStatusProjectsCountChartAPIView().as_view()
        response: Response = view(request)
        actual_project_count: int = 0
        for item in response.data:
            actual_project_count += item.get("count")
        self.assertEqual(actual_project_count, date_range_projects_count)


class TestPMOProjectCountByEstimatedEndDateRangeChartAPIView(
    BaseProviderTestCase
):
    """
    Test view: PMOProjectCountByEstimatedEndDateRangeChartAPIView
    """

    project_count_within_date_range_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-projects-count-within-days-range-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_projects_count_within_est_end_date_range_chart(
        self,
    ):
        """
        Test projects count within estimated end date range chart.
        """
        today = datetime.now()
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        last_week_monday = this_week_monday - timedelta(days=7)
        next_week_monday = this_week_monday + timedelta(days=7)
        next_week_sunday = next_week_monday + timedelta(days=7)
        this_week_closing_project_count: int = 1
        last_week_closing_projects_count: int = 1
        next_week_closing_projects_count: int = 1
        within_30_days_before_last_week: int = 1
        within_30_days_after_next_week: int = 1
        # Create projects within the date ranges and out of date range
        ProjectFactory.create_batch(
            this_week_closing_project_count,
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
        )
        ProjectFactory.create_batch(
            last_week_closing_projects_count,
            provider=self.provider,
            estimated_end_date=last_week_monday + timedelta(days=2),
        )
        ProjectFactory.create_batch(
            next_week_closing_projects_count,
            provider=self.provider,
            estimated_end_date=next_week_monday + timedelta(days=2),
        )
        ProjectFactory.create_batch(
            within_30_days_before_last_week,
            provider=self.provider,
            estimated_end_date=last_week_monday - timedelta(days=10),
        )
        ProjectFactory.create_batch(
            within_30_days_after_next_week,
            provider=self.provider,
            estimated_end_date=next_week_sunday + timedelta(days=10),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.project_count_within_date_range_url,
            self.provider,
            self.user,
        )
        view = PMOProjectCountByEstimatedEndDateRangeChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data), 5)

    def test_project_type_filter_on_projects_count_within_est_end_date_range_chart(
        self,
    ):
        """
        Test project type filter on project count within estimated end date range chart.
        """
        today = datetime.now()
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        mock_project_type_projects_count: int = 2
        mock_project_type: ProjectType = ProjectTypeFactory(
            provider=self.provider, title="Mock project type"
        )
        ProjectFactory.create_batch(
            mock_project_type_projects_count,
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
            type=mock_project_type,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
        )
        url: str = add_query_params_to_url(
            self.project_count_within_date_range_url,
            query_params=dict(type=mock_project_type.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_response_data: List[Dict] = [
            dict(
                days_range="Projects closing this week",
                count=mock_project_type_projects_count,
            ),
        ]
        view = PMOProjectCountByEstimatedEndDateRangeChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(expected_response_data, response.data)

    def test_project_status_filter_on_projects_count_within_est_end_date_range_chart(
        self,
    ):
        """
        Test project status filter on project count within estimated end date range chart.
        """
        today = datetime.now()
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        mock_status: OverallStatusSetting = OverallStatusSettingFactory(
            provider=self.provider, title="Mock overall status"
        )
        mock_status_project_count: int = 2
        ProjectFactory.create_batch(
            mock_status_project_count,
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
            overall_status=mock_status,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
            overall_status=None,
        )
        url: str = add_query_params_to_url(
            self.project_count_within_date_range_url,
            query_params=dict(status=mock_status.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_response_data: List[Dict] = [
            dict(
                days_range="Projects closing this week",
                count=mock_status_project_count,
            ),
        ]
        view = PMOProjectCountByEstimatedEndDateRangeChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(expected_response_data, response.data)

    def test_project_manager_filter_on_projects_count_within_est_end_date_range_chart(
        self,
    ):
        """
        Test project manager filter on project count within estimated end date range chart.
        """
        today = datetime.now()
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        mock_project_manager: User = ProviderUserFactory(
            provider=self.provider,
            first_name="MockFirstName",
            last_name="MockLastName",
        )
        mock_manager_projects_count: int = 2
        ProjectFactory.create_batch(
            mock_manager_projects_count,
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
            project_manager=mock_project_manager,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_monday + timedelta(days=2),
        )
        url: str = add_query_params_to_url(
            self.project_count_within_date_range_url,
            query_params=dict(project_manager=mock_project_manager.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_response_data: List[Dict] = [
            dict(
                days_range="Projects closing this week",
                count=mock_manager_projects_count,
            ),
        ]
        view = PMOProjectCountByEstimatedEndDateRangeChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(expected_response_data, response.data)

    def test_est_end_date_range_filter_on_projects_count_within_est_end_date_range_chart(
        self,
    ):
        """
        Test estimated end date range filter on project count within estimated end date range chart.
        """
        today = datetime.now()
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        this_week_tueday = today + timedelta(days=1)
        this_week_wednesday = today + timedelta(days=2)
        this_week_thursday = today + timedelta(days=3)
        # Create project with estimated end date on this week Monday, Tuesday, Wednesday, Thursday
        # Filter estimated end date range from Tuedsay to Wednesday
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_monday,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_tueday,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_wednesday,
        )
        ProjectFactory.create(
            provider=self.provider,
            estimated_end_date=this_week_thursday,
        )
        expected_date_range_project_count: int = 2
        url: str = add_query_params_to_url(
            self.project_count_within_date_range_url,
            query_params=dict(
                estimated_end_date_after=this_week_tueday.strftime("%Y-%m-%d"),
                estimated_end_date_before=this_week_wednesday.strftime(
                    "%Y-%m-%d"
                ),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_response_data: List[Dict] = [
            dict(
                days_range="Projects closing this week",
                count=expected_date_range_project_count,
            ),
        ]
        view = PMOProjectCountByEstimatedEndDateRangeChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(expected_response_data, response.data)


class TestPMOMetricsNotesListCreateAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsNotesListCreateAPIView
    """

    pmo_metrics_notes_list_create_url: str = reverse(
        "api_v1_endpoints:providers:pmo-metrics-notes-tab-create-list"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_unarchived_pmo_metric_notes(self):
        note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user
        )
        expected_data: Dict = dict(
            note=note.note,
            author_id=str(note.author_id),
            author_first_name=note.author.first_name,
            author_last_name=note.author.last_name,
            author_pic=note.author.profile.profile_pic,
            provider_id=note.provider_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pmo_metrics_notes_list_create_url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_data: Dict = dict(
            note=response.data.get("results")[0]["note"],
            author_id=response.data.get("results")[0]["author_id"],
            provider_id=response.data.get("results")[0]["provider_id"],
            author_first_name=response.data.get("results")[0][
                "author_first_name"
            ],
            author_last_name=response.data.get("results")[0][
                "author_last_name"
            ],
            author_pic=response.data.get("results")[0]["author_pic"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(expected_data, actual_data)

    def test_listing_of_archived_pmo_metric_notes(self):
        PMOMetricsNotesFactory(provider=self.provider, is_archived=True)
        PMOMetricsNotesFactory(provider=self.provider, is_archived=False)
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(archived="true"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data.get("results")[0].get("is_archived"), True
        )

    def test_creation_of_pmo_metric_dashboard_note(self):
        mock_note: str = "Mock note"
        create_payload: Dict = dict(note=mock_note)
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pmo_metrics_notes_list_create_url,
            self.provider,
            self.user,
            data=create_payload,
        )
        expected_notes_data: Dict = dict(
            note=mock_note,
            author_id=str(self.user.id),
            author_first_name=self.user.first_name,
            author_last_name=self.user.last_name,
            author_pic=self.user.profile.profile_pic,
            provider_id=self.provider.id,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_data: Dict = dict(
            note=response.data["note"],
            author_id=response.data["author_id"],
            provider_id=response.data["provider_id"],
            author_first_name=response.data["author_first_name"],
            author_last_name=response.data["author_last_name"],
            author_pic=response.data["author_pic"],
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(expected_notes_data, actual_data)

    def test_api_error_creating_note_with_invalid_request_body(self):
        create_payload: Dict = dict(note="")
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pmo_metrics_notes_list_create_url,
            self.provider,
            self.user,
            data=create_payload,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_search_on_note_content_in_pmo_metric_notes_tab(self):
        search_param: str = "A mock PMO metrics note"
        PMOMetricsNotesFactory(
            provider=self.provider, author=self.user, note=search_param
        )
        PMOMetricsNotesFactory.create(provider=self.provider, author=self.user)
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(search=search_param),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_on_author_first_name_in_pmo_metrics_notes_tab(self):
        mock_author_first_name: str = "MockFirstName"
        mock_author: User = ProviderUserFactory(
            provider=self.provider, first_name=mock_author_first_name
        )
        PMOMetricsNotesFactory(provider=self.provider, author=mock_author)
        PMOMetricsNotesFactory.create(provider=self.provider, author=self.user)
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(search=mock_author_first_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_on_author_last_name_in_pmo_metrics_notes_tab(self):
        mock_author_last_name: str = "MockLastName"
        mock_author: User = ProviderUserFactory(
            provider=self.provider, last_name=mock_author_last_name
        )
        PMOMetricsNotesFactory(provider=self.provider, author=mock_author)
        PMOMetricsNotesFactory.create(provider=self.provider, author=self.user)
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(search=mock_author_last_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_author_filter_in_pmo_metrics_notes_tab(self):
        mock_author: User = ProviderUserFactory(
            provider=self.provider,
            first_name="MockFirstName",
            last_name="MockLastName",
        )
        PMOMetricsNotesFactory(provider=self.provider, author=mock_author)
        PMOMetricsNotesFactory(provider=self.provider, author=self.user)
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(author=str(mock_author.id)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_ordering_by_oldest_updated_on_date_in_pmo_metrics_notes_tab(self):
        latest_datetime = datetime.now()
        old_datetime = latest_datetime - timedelta(days=3)
        new_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, updated_on=latest_datetime
        )
        old_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, updated_on=old_datetime
        )
        expected_order: List[int] = [old_mock_note.id, new_mock_note.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(ordering="updated_on"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_lastest_updated_on_date_in_pmo_metrics_notes_tab(
        self,
    ):
        latest_datetime = datetime.now()
        old_datetime = latest_datetime - timedelta(days=3)
        new_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, updated_on=latest_datetime
        )
        old_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, updated_on=old_datetime
        )
        expected_order: List[int] = [new_mock_note.id, old_mock_note.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(ordering="-updated_on"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_oldest_created_on_date_in_pmo_metrics_notes_tab(self):
        latest_datetime = datetime.now()
        old_datetime = latest_datetime - timedelta(days=3)
        new_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, created_on=latest_datetime
        )
        old_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, created_on=old_datetime
        )
        expected_order: List[int] = [old_mock_note.id, new_mock_note.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(ordering="created_on"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_latest_created_on_date_in_pmo_metrics_notes_tab(self):
        latest_datetime = datetime.now()
        old_datetime = latest_datetime - timedelta(days=3)
        new_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, created_on=latest_datetime
        )
        old_mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, created_on=old_datetime
        )
        expected_order: List[int] = [new_mock_note.id, old_mock_note.id]
        url: str = add_query_params_to_url(
            self.pmo_metrics_notes_list_create_url,
            query_params=dict(ordering="-created_on"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesListCreateAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)


class TestPMOMetricsNotesRetrieveUpdateDestroyAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsNotesRetrieveUpdateDestroyAPIView
    """

    pmo_metrics_notes_retrieve_update_destroy_endpoint: str = (
        "api_v1_endpoints:"
        "providers:pmo-metrics-notes-tab-"
        "retreive-update-destroy"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_retrieval_of_note_in_pmo_metrics_dashbaord_listing(self):
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user
        )
        expected_note_data: Dict = dict(
            note=mock_note.note,
            author_id=str(mock_note.author_id),
            author_first_name=mock_note.author.first_name,
            author_last_name=mock_note.author.last_name,
            provider_id=mock_note.provider_id,
            author_pic=mock_note.author.profile.profile_pic,
            is_archived=mock_note.is_archived,
        )
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        actual_note_data: Dict = dict(
            note=response.data["note"],
            author_id=response.data["author_id"],
            provider_id=response.data["provider_id"],
            author_first_name=response.data["author_first_name"],
            author_last_name=response.data["author_last_name"],
            author_pic=response.data["author_pic"],
            is_archived=response.data["is_archived"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(expected_note_data, actual_note_data)

    def test_api_error_when_retrieving_non_existing_note(self):
        invalid_note_id: int = 1000
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=invalid_note_id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=invalid_note_id)
        self.assertEqual(response.status_code, 404)

    def test_note_update_in_pmo_metrics_notes_tab(self):
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user
        )
        updated_note: str = "Updated note"
        expected_note_data: Dict = dict(
            note=updated_note,
            author_id=str(mock_note.author_id),
            author_first_name=mock_note.author.first_name,
            author_last_name=mock_note.author.last_name,
            provider_id=mock_note.provider_id,
            author_pic=mock_note.author.profile.profile_pic,
        )
        request_body: Dict = dict(note=updated_note)
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=request_body
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        actual_note_data: Dict = dict(
            note=response.data["note"],
            author_id=response.data["author_id"],
            provider_id=response.data["provider_id"],
            author_first_name=response.data["author_first_name"],
            author_last_name=response.data["author_last_name"],
            author_pic=response.data["author_pic"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(expected_note_data, actual_note_data)

    def test_api_error_when_updating_note_for_invalid_request_body(self):
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user
        )
        request_body: Dict = dict(note="")
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=request_body
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        self.assertEqual(response.status_code, 400)

    def test_api_error_updating_note_if_user_is_not_author(self):
        mock_author: User = ProviderUserFactory(
            provider=self.provider,
            first_name="MockFirstName",
            last_name="MockLastName",
        )
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=mock_author
        )
        updated_note: str = "Updated note"
        request_body: Dict = dict(note=updated_note)
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=request_body
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        self.assertEqual(response.status_code, 403)

    def test_deletion_of_note_in_pmo_metrics_notes_tab(self):
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user
        )
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        note_is_deleted: bool = not PMOMetricsNotes.objects.filter(
            id=mock_note.id, provider_id=self.provider.id
        ).exists()
        self.assertEqual(response.status_code, 204)
        self.assertEqual(note_is_deleted, True)

    def test_api_error_deleting_note_if_user_is_not_author(self):
        mock_author: User = ProviderUserFactory(
            provider=self.provider,
            first_name="MockFirstName",
            last_name="MockLastName",
        )
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=mock_author
        )
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        self.assertEqual(response.status_code, 403)

    def test_archiving_of_pmo_metrics_note(self):
        mock_note: PMOMetricsNotes = PMOMetricsNotesFactory(
            provider=self.provider, author=self.user, is_archived=False
        )
        request_body: Dict = dict(is_archived=True)
        url: str = reverse(
            self.pmo_metrics_notes_retrieve_update_destroy_endpoint,
            kwargs=dict(note_id=mock_note.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=request_body
        )
        view = PMOMetricsNotesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, note_id=mock_note.id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get("is_archived"), True)
