import json
from typing import List, Dict
from datetime import datetime, timedelta
from unittest import mock
from unittest.mock import patch

from django.urls import reverse
from rest_framework.request import Request
from rest_framework.response import Response
from faker import Faker
from accounts.models import Customer
from accounts.tests.factories import (
    CustomerFactory,
    ProfileFactory,
)
from accounts.tests.factories import ProviderUserFactory
from project_management.models.pmo_settings import (
    OverallStatusSetting,
    ProjectType,
    AdditionalSetting,
)
from project_management.models.project import Project, ProjectCWNote
from project_management.models.pmo_metrics import PMOMetricsNotes
from project_management.models.pmo_settings import (
    OverallStatusSetting,
    ProjectType,
)
from accounts.models import Provider
from project_management.tests.factories import (
    ProjectFactory,
    OverallStatusSettingFactory,
    ProjectTypeFactory,
    ProjectBoardFactory,
    PMOMetricsNotesFactory,
    AdditionalSettingFactory,
)
import pytest
from project_management.views.project_views import ProjectCWNotesListAPIVIew
from sales.tests.utils import render_response
from test_utils import (
    BaseProviderTestCase,
    TestClientMixin,
)
from django.conf import settings
from sales.tests.utils import add_query_params_to_url
from project_management.tests.test_utils import (
    get_mock_actual_and_budget_hour_values,
    get_project_note_cw_data,
    create_project_note_cw_data,
)


class TestProjectCWNoteAPIView(BaseProviderTestCase):
    """
    Test view: PMOMetricsReviewTabDataAPIView
    """

    note_url: str = "api_v1_endpoints:providers:project-cw-notes"

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @pytest.mark.first
    def test_project_note_create(self):
        """
        Test listing of Project note.
        """
        project: Project = ProjectFactory.create(
            provider=self.provider,
        )
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.create_project_notes.return_value = (
            create_project_note_cw_data()
        )
        data = dict(note="<p>Test merge Acela and CW notes</p>")
        note_text = "Test merge Acela and CW notes"
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            reverse(self.note_url, args=[project.id]),
            self.provider,
            self.user,
            data=data,
        )
        view = ProjectCWNotesListAPIVIew().as_view()
        response = view(request, project_id=project.id)
        project_note_id: int = response.data.get("note_id")
        project_note_text: str = response.data.get("text")
        self.assertEqual(note_text, project_note_text)

    @pytest.mark.second
    def test_project_note_listing(self):
        """
        Test listing of Project note.
        """
        project: Project = ProjectFactory.create(
            provider=self.provider,
        )
        additional_settings: AdditionalSetting = (
            AdditionalSettingFactory.create(
                provider=self.provider, note_type_ids=[1, 2, 4]
            )
        )
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.get_project_notes.return_value = (
            get_project_note_cw_data()
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            reverse(self.note_url, args=[project.id]),
            self.provider,
            self.user,
        )
        view = ProjectCWNotesListAPIVIew().as_view()
        response = view(request, project_id=project.id)
        project_data: Dict = response.data
        note_count = ProjectCWNote.objects.filter(
            provider=self.provider
        ).count()
        self.assertEqual(200, response.status_code)
        self.assertEqual(note_count, len(response.data))

    @pytest.mark.third
    def test_project_note_update(self):
        """
        Test update of Project note.
        """
        project: Project = ProjectFactory.create(
            provider=self.provider,
        )
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.update_project_notes.return_value = {
            "note_id": 2213,
            "project_id": 526,
            "text": "Update Test merge Acela and CW notes",
            "note_type_id": 2,
            "note_type_name": "Comment",
            "flagged": False,
            "last_updated": "2023-03-29T07:33:30Z",
            "updated_by": "velotio",
            "author": "V Five Test",
            "author_id": "5fbb5b1d-4d59-48cd-9072-10bd605a99ca",
            "author_email": "v5test@yopmail.com",
            "author_profile_pic": "/media/users/profile-pics/5fbb5b1d-4d59-48cd-9072-10bd605a99ca-LP.png",
        }
        data = dict(
            note="<p>Update Test merge Acela and CW notes</p>",
            note_crm_id=2213,
        )
        note_text = "Update Test merge Acela and CW notes"
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            reverse(self.note_url, args=[project.id]),
            self.provider,
            self.user,
            data=data,
        )
        view = ProjectCWNotesListAPIVIew().as_view()
        response = view(request, project_id=project.id)
        project_note_id: int = response.data.get("note_id")
        project_note_updated_text: str = response.data.get("text")
        self.assertEqual(note_text, project_note_updated_text)

    @pytest.mark.fourth
    def test_project_note_delete(self):
        """
        Test update of Project note.
        """
        project: Project = ProjectFactory.create(
            provider=self.provider,
        )
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.delete_project_notes.return_value = {}
        data = dict(note_crm_id=2213)
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            reverse(self.note_url, args=[project.id]),
            self.provider,
            self.user,
            data=data,
        )
        view = ProjectCWNotesListAPIVIew().as_view()
        response = view(request, project_id=project.id)
        self.assertEqual(204, response.status_code)
