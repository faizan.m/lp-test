from django.urls import reverse
from rest_framework.request import Request

from test_utils import (
    BaseProviderTestCase,
    TestClientMixin,
)
from project_management.views.change_request import (
    CancelledCRSettingCreateRetrieveUpdateView,
)
from project_management.tests.factories import (
    CancelledChangeRequestSettingFactory,
)
from typing import Dict, List


class TestCancelledCRSettingCreateRetrieveUpdateView(BaseProviderTestCase):
    """
    Test view: CancelledCRSettingCreateRetrieveUpdateView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:cancelled-change-request-setting"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_create_cancelled_CR_setting(self):
        request_body: Dict = dict(opp_status_crm_id=1, opp_stage_crm_id=2)
        view = CancelledCRSettingCreateRetrieveUpdateView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        response = view(request)
        expected_response: Dict = dict(
            provider=self.provider.id, opp_status_crm_id=1, opp_stage_crm_id=2
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_error_creating_setting_if_already_created(self):
        CancelledChangeRequestSettingFactory(provider=self.provider)
        request_body: Dict = dict(opp_status_crm_id=1, opp_stage_crm_id=2)
        view = CancelledCRSettingCreateRetrieveUpdateView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        response = view(request)
        self.assertEqual(400, response.status_code)

    def test_retrieve_cancelled_CR_setting(self):
        CancelledChangeRequestSettingFactory(
            provider=self.provider, opp_status_crm_id=1, opp_stage_crm_id=2
        )
        view = CancelledCRSettingCreateRetrieveUpdateView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        response = view(request)
        expeted_response: Dict = dict(
            provider=self.provider.id, opp_status_crm_id=1, opp_stage_crm_id=2
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expeted_response, response.data)

    def test_error_retrieving_cancelled_CR_setting_if_not_configured(self):
        view = CancelledCRSettingCreateRetrieveUpdateView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        response = view(request)
        self.assertEqual(404, response.status_code)

    def test_update_cancelled_change_request_setting(self):
        CancelledChangeRequestSettingFactory(
            provider=self.provider, opp_status_crm_id=1, opp_stage_crm_id=2
        )
        view = CancelledCRSettingCreateRetrieveUpdateView().as_view()
        request_body: Dict = dict(opp_status_crm_id=5, opp_stage_crm_id=6)
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=request_body
        )
        response = view(request)
        expeted_response: Dict = dict(
            provider=self.provider.id, opp_status_crm_id=5, opp_stage_crm_id=6
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expeted_response, response.data)
