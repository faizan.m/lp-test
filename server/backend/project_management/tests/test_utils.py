from typing import List, Dict
from faker import Faker

fake = Faker()


MOCK_COLORS_LIST: List[str] = [
    "#e5eb34",
    "#e5eb34",
    "#eb34e8",
    "#eb34e8",
    "#40eb34",
]


def get_mock_project_updates_data() -> Dict:
    return dict(
        estimated_hours_to_complete=fake.pyint(min_value=1, max_value=999),
        note=fake.text(max_nb_chars=10),
        updates=fake.text(max_nb_chars=10),
    )


def get_mock_actual_and_budget_hour_values() -> Dict:
    """
    Returns mock values of actual_hours and hours_budget for a project so that it
    satisfies the condition that remaining hours less than 10.
    """
    return dict(
        hours_budget=fake.pyint(min_value=5, max_value=10),
        actual_hours=fake.pyint(min_value=1, max_value=5),
    )


def get_project_note_cw_data() -> Dict:
    return [
        {
            "note_id": 2212,
            "project_id": 526,
            "text": " new Merge note creaed in Acela and CW",
            "note_type_id": 2,
            "note_type_name": "Comment",
            "flagged": False,
            "last_updated": "2023-03-29T07:03:55Z",
            "updated_by": "velotio",
            "updated_on": "2023-03-29T07:03:58.015770Z",
            "author_id": "5fbb5b1d-4d59-48cd-9072-10bd605a99ca",
            "author": "V Five Test",
            "author_email": "v5test@yopmail.com",
            "author_profile_pic": "/media/users/profile-pics/5fbb5b1d-4d59-48cd-9072-10bd605a99ca-LP.png",
        },
        {
            "note_id": 2210,
            "project_id": 526,
            "text": "Merge note creaed in Acela and CW",
            "note_type_id": 2,
            "note_type_name": "Comment",
            "flagged": False,
            "last_updated": "2023-03-28T12:08:52Z",
            "updated_by": "velotio",
            "updated_on": "2023-03-28T12:08:52.371399Z",
            "author_id": "5fbb5b1d-4d59-48cd-9072-10bd605a99ca",
            "author": "V Five Test",
            "author_email": "v5test@yopmail.com",
            "author_profile_pic": "/media/users/profile-pics/5fbb5b1d-4d59-48cd-9072-10bd605a99ca-LP.png",
        },
        {
            "note_id": 2209,
            "project_id": 526,
            "text": "test from the cw to acela",
            "note_type_id": 2,
            "note_type_name": "Comment",
            "flagged": False,
            "last_updated": "2023-03-28T11:28:20Z",
            "updated_by": "mnawandar",
            "updated_on": "2023-03-28T11:28:57.338762Z",
        },
    ]


def create_project_note_cw_data() -> Dict:
    return {
        "note_id": 2213,
        "project_id": 526,
        "text": "Test merge Acela and CW notes",
        "note_type_id": 2,
        "note_type_name": "Comment",
        "flagged": False,
        "last_updated": "2023-03-29T07:14:52Z",
        "updated_by": "velotio",
        "author": "V Five Test",
        "author_id": "5fbb5b1d-4d59-48cd-9072-10bd605a99ca",
        "author_email": "v5test@yopmail.com",
        "author_profile_pic": "/media/users/profile-pics/5fbb5b1d-4d59-48cd-9072-10bd605a99ca-LP.png",
    }
