from accounts.tests.factories import (
    ProviderFactory,
    CustomerFactory,
    ProviderUserFactory,
)
from document.tests.factories import SOWDocumentFactory
import factory
from project_management.models.project import (
    Project,
    ProjectCWNote,
    CancelledChangeRequestSetting,
)
from project_management.models.pmo_settings import (
    ProjectBoard,
    ProjectType,
    OverallStatusSetting,
    AdditionalSetting,
)
from project_management.models.pmo_metrics import PMOMetricsNotes
from faker import Faker
from project_management.tests.test_utils import MOCK_COLORS_LIST

fake = Faker()


class ProjectBoardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectBoard

    name = fake.name()
    connectwise_id = factory.Sequence(lambda n: n)
    provider = factory.SubFactory(ProviderFactory)


class ProjectTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectType
        django_get_or_create = ("provider", "title")

    title = fake.bs()
    description = fake.text(max_nb_chars=10)
    provider = factory.SubFactory(ProviderFactory)


class OverallStatusSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OverallStatusSetting

    title = fake.bs()
    description = fake.text(max_nb_chars=10)
    connectwise_id = factory.Sequence(lambda n: n)
    provider = factory.SubFactory(ProviderFactory)
    is_open = True
    board = factory.SubFactory(ProjectBoardFactory)
    color = fake.random_element(MOCK_COLORS_LIST)
    is_default = fake.pybool()


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Project

    provider = factory.SubFactory(ProviderFactory)
    board = factory.SubFactory(ProjectBoardFactory)
    title = fake.bs()
    description = fake.text(max_nb_chars=10)
    type = factory.SubFactory(ProjectTypeFactory)
    crm_id = factory.Sequence(lambda n: n)
    customer = factory.SubFactory(CustomerFactory)
    project_manager = factory.SubFactory(ProviderUserFactory)
    overall_status = factory.SubFactory(OverallStatusSettingFactory)
    hours_budget = fake.pyint(min_value=1, max_value=100)
    actual_hours = fake.pyint(min_value=1, max_value=100)
    estimated_end_date = fake.date_time_this_year()
    closed_date = fake.date_time_this_year()
    primary_contact = factory.SubFactory(ProviderUserFactory)
    account_manager = factory.SubFactory(ProviderUserFactory)
    last_updated_by = factory.SubFactory(ProviderUserFactory)
    sow_document = factory.SubFactory(SOWDocumentFactory)
    opportunity_crm_ids = list()
    billing_amount = fake.pyfloat()
    project_updates = dict()


class PMOMetricsNotesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PMOMetricsNotes

    provider = factory.SubFactory(ProviderFactory)
    note = fake.text(max_nb_chars=10)
    author = factory.SubFactory(ProviderUserFactory)
    created_on = None
    updated_on = None
    is_archived = False

    @classmethod
    def _create(cls, target_class, *args, **kwargs):
        updated_on = kwargs.pop("updated_on", None)
        created_on = kwargs.pop("created_on", None)
        obj = super(PMOMetricsNotesFactory, cls)._create(
            target_class, *args, **kwargs
        )
        # Django model overrides the updated_on and created_on values.
        # This updates the object with actual passed values to factory.
        if updated_on is not None:
            PMOMetricsNotes.objects.filter(
                id=obj.id, provider_id=obj.provider.id
            ).update(updated_on=updated_on)
        if created_on is not None:
            PMOMetricsNotes.objects.filter(
                id=obj.id, provider_id=obj.provider.id
            ).update(created_on=created_on)
        return obj


class ProjectCWNotesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectCWNote

    provider = factory.SubFactory(ProviderFactory)
    project = factory.SubFactory(ProjectFactory)
    note_crm_id = fake.pyint()
    author = factory.SubFactory(ProviderUserFactory)


class AdditionalSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AdditionalSetting

    provider = factory.SubFactory(ProviderFactory)
    default_project_manager = factory.SubFactory(ProviderUserFactory)
    project_service_board = fake.pyint(min_value=1, max_value=100)
    service_ticket_complete_status = fake.pyint(min_value=1, max_value=100)
    project_item_due_date_threshold = fake.pyint(min_value=1, max_value=7)
    project_completed_item_threshold = fake.pyint(min_value=1, max_value=7)
    default_work_role = fake.pyint(min_value=1, max_value=100)
    default_work_type = fake.pyint(min_value=1, max_value=100)
    default_business_unit = fake.pyint(min_value=1, max_value=100)
    default_opportunity_type = fake.pyint(min_value=1, max_value=100)
    default_opportunity_stage = fake.pyint(min_value=1, max_value=100)
    engineer_default_work_role = fake.pyint(min_value=1, max_value=100)
    engineer_default_work_type = fake.pyint(min_value=1, max_value=100)
    engineer_default_business_unit = fake.pyint(min_value=1, max_value=100)
    note_type_ids = list()


class CancelledChangeRequestSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CancelledChangeRequestSetting

    provider = factory.SubFactory(ProviderFactory)
    opp_status_crm_id = fake.pyint(min_value=1, max_value=10)
    opp_stage_crm_id = fake.pyint(min_value=1, max_value=10)
