import json
from typing import Dict, List, Set, Optional, Union, Any
from uuid import UUID

from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.template.loader import render_to_string

from accounts.models import Provider, User
from core.integrations.pdfshift import PdfShift
from document.services import ChangeRequestSOWService
from project_management.emails import GenericPMOMail
from project_management.models import (
    ChangeRequest,
    ChangeRequestEmailTemplate,
    Meeting,
    Project,
    StatusMeeting,
    KickoffMeeting,
)
from project_management.serializers import ProjectActivePhaseListSerializer
from project_management.services import ProjectService
from project_management.services.meeting_service import MeetingService
from utils import (
    convert_local_files_to_remote_attachments,
    format_phone_number,
)
from document.services.document_service import DocumentService

logger = get_task_logger(__name__)


def send_generic_email(
    provider_id,
    request_user_id,
    context,
    recipients,
    images=None,
    attachments=None,
    remote_attachments=None,
):
    request_user = User.providers.get(
        id=request_user_id, provider_id=provider_id
    )
    from_email = f"{request_user.name} <{request_user.email}>"
    provider = Provider.objects.get(id=provider_id)
    context.update({"provider": provider})
    try:
        signature = request_user.profile.email_signature.get("email_signature")
        context.update({"signature": signature})
    except Exception as e:
        logger.debug(e)

    files, remote_attachments = convert_local_files_to_remote_attachments(
        attachments, remote_attachments
    )
    GenericPMOMail(
        context=context,
        files=files,
        remote_attachments=remote_attachments,
        images=images,
        from_email=from_email,
    ).send(to=recipients, cc=[request_user.email])


@shared_task
def send_customer_touch_email(
    provider_id,
    email_subject: str,
    email_body: str,
    recipients: List[str],
    request_user_id: str,
) -> None:
    """
    Sends the Customer Touch email to the provided list of recipients.

    """
    context = {"body": email_body, "subject": email_subject}
    send_generic_email(provider_id, request_user_id, context, recipients)
    logger.info(f"Customer Touch email sent to {recipients}")


@shared_task
def send_close_out_email(
    provider_id: str,
    email_subject: str,
    email_body: str,
    recipients: List[str],
    request_user_id: str,
    remote_attachments: List[tuple],
) -> None:
    """
    Sends the Close Out email to the provided list of recipients.

    """
    context = {"body": email_body, "subject": email_subject}
    send_generic_email(
        provider_id,
        request_user_id,
        context,
        recipients,
        remote_attachments=remote_attachments,
    )
    logger.info(f"Close out email sent to {recipients}")


def update_attended_status_for_team_members(
    team_members: "Queryset", attendee_ids: Set[UUID], **kwargs
):
    """
    Set attended as True for a team member if team member is attending the meeting else False

    Args:
        team_members: Project team members
        attendee_ids: Set of attendee UUIDs
    """
    project_manager_id: int = kwargs.get("project_manager_id")
    for team_member in team_members:
        team_member_id: int = team_member.id
        if (
            team_member_id in attendee_ids
            or team_member_id == project_manager_id
        ):
            team_member.attended = True
        else:
            team_member.attended = False


def update_attended_status_for_customer_contacts(
    customer_contacts: List[Dict], attendee_ids: Set[UUID]
):
    """
    Set attended as True for a customer contact if contact is attending the meeting else False

    Args:
        customer_contacts: Customer contacts
        attendee_ids: Set of attendee UUIDs
    """
    for customer_contact in customer_contacts:
        if customer_contact.get("user_id", None) in attendee_ids:
            customer_contact["attended"] = True
        else:
            customer_contact["attended"] = False


def update_attended_status_for_additional_attendees(
    additional_attendees, present_additional_attendee_ids
):
    """
    Set attended as True for additional attendee if attendee is attending the meeting else False

    Args:
        additional_attendees: Additional attendees
        present_additional_attendee_ids: Additional attendees ids
    """
    for attendee in additional_attendees:
        if attendee.id in present_additional_attendee_ids:
            attendee.attended = True
        else:
            attendee.attended = False


def format_phone_number_of_team_members(team_members):
    for member in team_members:
        phone_number = member.profile.full_cell_phone_number
        member.formatted_phone_number = (
            format_phone_number(phone_number) if phone_number else None
        )


def format_phone_number_of_customer_contacts(customer_contacts):
    for contact in customer_contacts:
        phone_number = contact.get("office_phone", None)
        contact["formatted_phone_number"] = (
            format_phone_number(phone_number) if phone_number else None
        )


def format_phone_number_of_additional_attendees(additional_attendees):
    for attendee in additional_attendees:
        phone_number = attendee.phone
        attendee.formatted_phone_number = (
            format_phone_number(phone_number) if phone_number else None
        )


def get_meeting_pdf_context(
    meeting: Meeting, project: Project, add_customer_contacts: bool = False
) -> Dict[str, Any]:
    """
    Get render context for meeting PDF preview.

    Args:
        meeting: Meeting
        project: Project

    Returns:
        Render context
    """
    default_profile_pic: str = settings.USER_DEFAULT_PROFILE_PIC
    project_service: ProjectService = ProjectService(project.provider)
    critical_path_items: "QuerySet[CriticalPathItem]" = (
        project_service.query_service.get_project_critical_path_items(
            project.id
        )
    )
    action_items: "QuerySet[ActionItem]" = (
        project_service.query_service.get_project_action_items(project.id)
    )
    meeting_notes: "QuerySet[ProjectNote]" = (
        project_service.query_service.get_meeting_notes(meeting.id, project.id)
    )
    attendees: "QuerySet[MeetingAttendee]" = (
        project_service.query_service.get_meeting_attendees(meeting)
    )
    attendee_ids: Set[str] = set(
        attendees.values_list("attendee__id", flat=True)
    )
    additional_attendees: "QuerySet[AdditionalContacts]" = (
        project_service.query_service.get_additional_meeting_attendees(
            project.id
        )
    )
    present_additional_attendees: "QuerySet[ExternalAttendee]" = project_service.query_service.get_additional_attendees_present_for_meeting(
        meeting, project
    )
    present_additional_attendee_ids: Set[int] = set(
        present_additional_attendees.values_list(
            "external_contact_id", flat=True
        )
    )
    format_phone_number_of_additional_attendees(additional_attendees)
    update_attended_status_for_additional_attendees(
        additional_attendees, present_additional_attendee_ids
    )
    team_members = project_service.get_project_team_members(project.id, True)
    format_phone_number_of_team_members(team_members)
    update_attended_status_for_team_members(
        team_members,
        attendee_ids,
        project_manager_id=project.project_manager_id,
    )
    if add_customer_contacts:
        customer_contacts: List[
            Dict
        ] = project_service.get_project_customer_contacts(
            project.crm_id, project.id
        )
        format_phone_number_of_customer_contacts(customer_contacts)
        update_attended_status_for_customer_contacts(
            customer_contacts, attendee_ids
        )
    risk_items: "QuerySet[RiskItem]" = (
        project_service.query_service.get_risk_items(project.id)
    )
    type_meeting: Union[
        StatusMeeting, KickoffMeeting, None
    ] = project_service.query_service.get_typed_meeting_for_meeting(meeting)
    agenda_topics: List[Optional[Dict]] = (
        type_meeting.agenda_topics if type_meeting else []
    )
    context: Dict[str, Any] = dict(
        critical_path_items=critical_path_items,
        action_items=action_items,
        risk_items=risk_items,
        notes=meeting_notes,
        attendees=attendees,
        project_team=team_members,
        meeting=meeting,
        project=project,
        default_profile_pic=default_profile_pic,
        additional_attendees=additional_attendees,
        agenda_topics=agenda_topics,
    )
    if add_customer_contacts:
        context["customer_contacts"] = customer_contacts
    return context


def generate_kickoff_meeting_pdf(
    meeting: Meeting,
    project: Project,
    output_file_name: str,
    add_customer_contacts: bool = False,
) -> str:
    """
    Generate kickoff meeting PDF.

    Args:
        meeting: Meeting
        project: Project
        output_file_name: PDF file name

    Returns:
        PDF url
    """
    TEMPLATE_PATH: str = "pmo_email_attachments/kickoff-meeting.html"
    context: Dict[str, Any] = get_meeting_pdf_context(
        meeting, project, add_customer_contacts
    )
    html_str: str = render_to_string(TEMPLATE_PATH, context)
    options: Dict[str, str] = dict(format="A3", margin="10px 2px")
    meeting_attachment_url: str = PdfShift().convert(
        html_str, output_file_name, **options
    )
    return meeting_attachment_url


@shared_task
def send_internal_kickoff_meeting_email(
    provider_id: str,
    email_subject: str,
    email_body: str,
    recipients: List[str],
    request_user_id: str,
    project_id: int,
    meeting_id: int,
    add_customer_contacts: bool,
) -> None:
    """
    Send External Kickoff Meeting email
    """
    project = Project.objects.get(id=project_id)
    meeting = Meeting.objects.get(project=project, id=meeting_id)
    logger.info("Generating PDF attachment")
    attachment_name = f"{project.title}-{meeting.name}.pdf"
    attachment_url = generate_kickoff_meeting_pdf(
        meeting,
        project,
        attachment_name,
        add_customer_contacts,
    )
    logger.info(f"Pdf attachment saved at {attachment_url}")
    context = {"body": email_body, "subject": email_subject}
    send_generic_email(
        provider_id,
        request_user_id,
        context,
        recipients,
        remote_attachments=[(attachment_name, attachment_url)],
    )
    logger.info(f"Internal Kickoff email sent to {recipients}")


def generate_status_meeting_pdf(
    meeting: Meeting,
    project: Project,
    output_file_name: str,
    add_customer_contacts: bool = False,
) -> str:
    """
    Generate status meeeting PDF.

    Args:
        meeting: Meeting
        project: Project
        output_file_name: PDF file name

    Returns:
        URL of generated PDF
    """
    TEMPLATE_PATH: str = "pmo_email_attachments/status-meeting.html"
    context: Dict[str, Any] = get_meeting_pdf_context(
        meeting, project, add_customer_contacts
    )
    project_service: ProjectService = ProjectService(project.provider)
    phases: "QuerySet[ProjectActivePhase]" = (
        project_service.query_service.get_project_active_phase_queryset(
            project.id
        )
    )
    serialized_phases: List[Dict[str, Any]] = ProjectActivePhaseListSerializer(
        phases, many=True
    ).data
    context.update(phases=json.dumps(serialized_phases))
    rendered_status_meeting_html: str = render_to_string(
        TEMPLATE_PATH, context
    )
    options: Dict[str, str] = dict(format="A3", margin="50px 10px")
    meeting_attachment_url: str = PdfShift().convert(
        rendered_status_meeting_html, output_file_name, **options
    )
    return meeting_attachment_url


@shared_task
def send_kickoff_meeting_email(
    provider_id: str,
    email_subject: str,
    email_body: str,
    meeting_id,
    project_id,
    recipients: List[str],
    request_user_id: str,
    add_customer_contacts: bool,
) -> None:
    """
    Send External Kickoff Meeting email
    """
    project = Project.objects.get(id=project_id)
    meeting = Meeting.objects.get(project=project, id=meeting_id)
    logger.info("Generating PDF attachment")
    attachment_name = f"{project.title}-{meeting.name}.pdf"
    attachment_url = generate_kickoff_meeting_pdf(
        meeting, project, attachment_name, add_customer_contacts
    )
    logger.info(f"Pdf attachment saved at {attachment_url}")
    context = {"body": email_body, "subject": email_subject}
    send_generic_email(
        provider_id,
        request_user_id,
        context,
        recipients,
        remote_attachments=[(attachment_name, attachment_url)],
    )
    logger.info(f"Kickoff meeting email sent to {recipients}")


@shared_task
def send_status_email(
    provider_id: str,
    email_subject: str,
    email_body: str,
    meeting_id,
    project_id,
    recipients: List[str],
    request_user_id: str,
    add_customer_contacts: bool,
) -> None:
    """
    Send status meeting email
    """
    project = Project.objects.get(id=project_id)
    meeting = Meeting.objects.get(project=project, id=meeting_id)
    attachment_name = f"{project.title}-{meeting.name}.pdf"
    attachment_url = generate_status_meeting_pdf(
        meeting, project, attachment_name, add_customer_contacts
    )
    context = {"body": email_body, "subject": email_subject}
    send_generic_email(
        provider_id,
        request_user_id,
        context,
        recipients,
        remote_attachments=[(attachment_name, attachment_url)],
    )
    logger.info(f"Status meeting email sent to {recipients}")


def parse_change_request_variables_in_email_subject(
    subject: str, **change_request_variables
) -> str:
    """
    Replace placeholder variables in subject with with actual values.

    Args:
        subject: Email subject
        **change_request_variables: Change request email template variables

    Returns:
        Parsed subject.
    """
    for variable, value in change_request_variables.items():
        subject: str = subject.replace(variable, value)
    return subject


def parse_change_request_variables_in_email_body(
    email_body: str, **change_request_variables
) -> str:
    """
    Replace placeholder variables in email body with with actual values.

    Args:
        email_body: Email body
        **change_request_variables: Change request email template variables

    Returns:
        Parsed subject.
    """
    for variable, value in change_request_variables.items():
        email_body: str = email_body.replace(variable[1:], value)
    email_body: str = email_body.replace("#", "")
    return email_body


def get_change_request_email_template_variables_to_value_map(
    change_request: ChangeRequest, project: Project
) -> Dict[str, str]:
    """
    Get change request email template variables to values mapping.

    Args:
        change_request: Change Request
        project: Project

    Returns:
        Map
    """
    variables: Dict[str, str] = {
        "#{Requestor Name}": change_request.requested_by.first_name,
        "#{Project Name}": project.title,
        "#{Customer Name}": project.customer.name,
        "#{Project Owner}": f"{project.project_manager.first_name} {project.project_manager.last_name}",
    }
    if project.primary_contact:
        variables.update(
            {
                "#{Customer Primary Contact}": f"{project.primary_contact.first_name} {project.primary_contact.last_name}",
                "#{Customer Primary Contact First Name}": f"{project.primary_contact.first_name}",
            }
        )
    return variables


@shared_task
def send_change_request_email(
    provider_id: int,
    project_id: int,
    change_request_id: int,
    request_user_id: str,
):
    provider: Provider = Provider.objects.get(id=provider_id)
    user: User = User.providers.get(id=request_user_id)

    try:
        email_template: ChangeRequestEmailTemplate = (
            ChangeRequestEmailTemplate.objects.get(provider=provider)
        )
    except ChangeRequestEmailTemplate.DoesNotExist:
        logger.info("Email Template setting not found for change request.")
        return

    project: Project = Project.objects.get(
        id=project_id, provider_id=provider_id
    )
    change_request: ChangeRequest = ChangeRequest.objects.get(
        provider=provider, project_id=project_id, id=change_request_id
    )
    if change_request.sow_document is None:
        logger.info("No SOW Document found for the change request.")

    change_request_variables: Dict[
        str, str
    ] = get_change_request_email_template_variables_to_value_map(
        change_request, project
    )
    email_subject: str = parse_change_request_variables_in_email_subject(
        email_template.email_subject, **change_request_variables
    )
    email_body: str = parse_change_request_variables_in_email_body(
        email_template.email_body_markdown, **change_request_variables
    )
    sow_document_data: Dict = ChangeRequestSOWService.download_document(
        provider_id,
        provider.erp_client,
        user,
        change_request.sow_document_id,
        f"{DocumentService.PDF_FORMAT}",
    )
    recipients: List[str] = [change_request.requested_by.email]
    account_manager = ProjectService(provider).get_project_account_manager(
        project_id
    )
    if not account_manager:
        logger.info(
            f"Can't send email to Account manager. Account manager not found for the Project {project}."
        )
    else:
        recipients.append(account_manager.email)

    context: Dict = {
        "body": email_body,
        "subject": email_subject,
        "provider": provider,
    }
    sow_document_name: str = sow_document_data.get("file_name")
    sow_document_path: str = sow_document_data.get("file_path")
    attachments, remote_attachments = [], []
    if not settings.DEBUG:
        remote_attachments = [(sow_document_name, sow_document_path)]
    else:
        attachments = [(sow_document_name, sow_document_path)]
    send_generic_email(
        provider_id,
        request_user_id,
        context,
        recipients,
        attachments=attachments,
        remote_attachments=remote_attachments,
    )
    logger.info(f"Change request email sent to {recipients}")
