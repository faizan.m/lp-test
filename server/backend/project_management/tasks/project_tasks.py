import json
from collections import defaultdict
from typing import Generator, List, Optional
from django.db.models import QuerySet, Q
from celery import shared_task
from celery.utils.log import get_task_logger
from requests import Response
from rest_framework.renderers import JSONRenderer

from accounts.models import Provider
from core.exceptions import ERPAuthenticationNotConfigured
from project_management.models import (
    ActionItem,
    AdditionalSetting,
    CriticalPathItem,
    Meeting,
    MeetingArchive,
    ProjectItemsArchive,
    RiskItem,
    ProjectEngineersMapping,
    Project,
    ActivityStatusSetting,
)
from project_management.services import (
    ProjectPayloadHandler,
    ProjectService,
    ProjectEngineerPayloadHandler,
    ProjectQueryService,
)
from django.db.models import (
    Value,
    CharField,
)
from utils import DbOperation

logger = get_task_logger(__name__)


@shared_task
def sync_projects(provider_ids=None):
    """
    Fetch all the projects for the provider from connectwise
    and sync in the local database.
    The project are either created, updated or deleted based on
    the available project in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        try:
            settings_configured, message = ProjectService(
                provider
            ).query_service.check_project_settings()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        if not settings_configured:
            logger.info(
                f"PMO settings not configured for provider {provider}. Message: {message}. "
                f"Task skipped."
            )
            continue
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing Projects for {provider}")
        project_service: ProjectService = ProjectService(provider)
        project_pages: Generator[
            Response
        ] = project_service.get_provider_projects_from_erp()
        existing_projects: List[
            int
        ] = project_service.query_service.get_projects().values_list(
            "crm_id", flat=True
        )
        logger.debug(
            f"Found {len(existing_projects)} existing projects in local DB."
        )
        projects_present_in_erp = set()
        for response in project_pages:
            projects = response.json()
            logger.debug(f"Found {len(projects)} in current page.")
            for project in projects:
                result["found"] += 1
                project_id = project["id"]
                if project_id in existing_projects:
                    res, errors = ProjectPayloadHandler(
                        provider, DbOperation.UPDATE, project
                    ).process()
                    if res:
                        logger.debug(f"Updated project: {project_id}")
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for project: {project_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[project_id].append(errors)
                else:
                    res, errors = ProjectPayloadHandler(
                        provider, DbOperation.CREATE, project
                    ).process()
                    if res:
                        logger.debug(f"Created project: {project_id}")
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Create failed for project: {project_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[project_id].append(errors)
                projects_present_in_erp.add(project_id)

        # We are only fetching the projects from connectwise with open status
        # So any project which is marked closed in connectwise will not be fetched.
        # But deleting such project from Acela we will loose all it's work. So
        # to avoid this projects once synced into Acela will not be deleted.

        # deleted_projects = set(existing_projects) - projects_present_in_erp
        # logger.info(f"Found {len(deleted_projects)} to be deleted.")
        # res, errors = ProjectPayloadHandler(
        #     provider, DbOperation.DELETE, payload={"id": deleted_projects}
        # ).process()
        # if res:
        #     result["deleted"] += len(deleted_projects)
        #     logger.info(f"Deleted {len(deleted_projects)} projects.")
        # else:
        #     result["delete_failed"] += len(deleted_projects)
        #     logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(f"Project Sync finished for {provider}. Result: {result}")
        provider_results[provider.id] = result
    return provider_results


@shared_task
def archive_project_items(provider_ids=None):
    """
    This method moves completed project critical path, action and risk Items
    to the ProjectItemsArchive table and removes from their individual tables.
    It uses the project_completed_item_threshold setting to calculate if the
    item is completed and has reached the aging period. Once the aging period is
    completed for the Item, a record gets created in the ProjectItemsArchive table and
    the original record is deleted.

    """
    if provider_ids:
        providers = Provider.objects.filter(id__in=provider_ids)
    else:
        providers = Provider.objects.filter(is_active=True)

    for provider in providers:
        try:
            settings_configured, message = ProjectService(
                provider
            ).query_service.check_project_settings()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        if not settings_configured:
            logger.info(
                f"PMO settings not configured for provider {provider}. Message: {message}. "
                f"Task skipped."
            )
            continue

        logger.debug(f"Archiving Project Items for {provider}")
        try:
            project_completed_item_threshold = (
                AdditionalSetting.objects.only(
                    "project_completed_item_threshold"
                )
                .get(provider=provider)
                .project_completed_item_threshold
            )
            logger.debug(
                f"Configured project_completed_item_threshold value: {project_completed_item_threshold}"
            )
        except AdditionalSetting.DoesNotExist:
            project_completed_item_threshold = 7
            logger.debug(
                f"No configured value found for project_completed_item_threshold. "
                f"Using default value {project_completed_item_threshold}"
            )

        project_service = ProjectService(provider)
        critical_path_items = (
            project_service.query_service.get_critical_path_items_for_archive(
                project_completed_item_threshold
            )
        )
        logger.debug(
            f"Critical Path Items for archive {critical_path_items.count()}"
        )
        action_items = (
            project_service.query_service.get_action_items_for_archive(
                project_completed_item_threshold
            )
        )
        logger.debug(f"Action Items for archive {action_items.count()}")
        risk_items = project_service.query_service.get_risk_items_for_archive(
            project_completed_item_threshold
        )
        logger.debug(f"Risk Items for archive {risk_items.count()}")

        archived_meetings = (
            project_service.query_service.get_meetings_for_archive(
                project_completed_item_threshold
            )
        )
        logger.debug(f"Meetings for archive {archived_meetings.count()}")

        archive_items: List[ProjectItemsArchive] = []
        archived_critical_path_items_ids: List[int] = []
        archived_risk_items_ids: List[int] = []
        archived_action_items_ids: List[int] = []
        archived_meeting_ids: List[int] = []
        meeting_ids: List[int] = []

        for critical_path_item in critical_path_items:
            serialized_critical_path_item = (
                project_service.serialize_critical_path_item_object(
                    critical_path_item, False
                )
            )
            archive_item = ProjectItemsArchive(
                provider=provider,
                project=critical_path_item.project,
                item_type=ProjectItemsArchive.CRITICAL_PATH_ITEM,
                blob=json.loads(
                    JSONRenderer().render(serialized_critical_path_item)
                ),
                updated_on=critical_path_item.completed_on,
            )
            archive_items.append(archive_item)
            archived_critical_path_items_ids.append(critical_path_item.id)

        for action_item in action_items:
            serialized_action_item = (
                project_service.serialize_action_item_object(
                    action_item, False
                )
            )
            archive_item = ProjectItemsArchive(
                provider=provider,
                project=action_item.project,
                item_type=ProjectItemsArchive.ACTION_ITEM,
                blob=json.loads(JSONRenderer().render(serialized_action_item)),
                updated_on=action_item.completed_on,
            )
            archive_items.append(archive_item)
            archived_action_items_ids.append(action_item.id)

        for risk_item in risk_items:
            serialized_risk_item = project_service.serialize_risk_item_object(
                risk_item, False
            )
            archive_item = ProjectItemsArchive(
                provider=provider,
                project=risk_item.project,
                item_type=ProjectItemsArchive.RISK_ITEM,
                blob=json.loads(JSONRenderer().render(serialized_risk_item)),
                updated_on=risk_item.completed_on,
            )
            archive_items.append(archive_item)
            archived_risk_items_ids.append(risk_item.id)

        for meeting in archived_meetings:
            archive_item = ProjectItemsArchive(
                provider=provider,
                project=meeting.project,
                item_type=ProjectItemsArchive.MEETING,
                blob=meeting.payload,
                updated_on=meeting.meeting.conducted_on,
            )
            archive_items.append(archive_item)
            archived_meeting_ids.append(meeting.id)
            meeting_ids.append(meeting.meeting.id)

        # Bulk create records in ProjectItemsArchive table
        ProjectItemsArchive.objects.bulk_create(archive_items)

        # Delete Archived Items
        CriticalPathItem.objects.filter(
            provider=provider, id__in=archived_critical_path_items_ids
        ).delete()
        ActionItem.objects.filter(
            provider=provider, id__in=archived_action_items_ids
        ).delete()
        RiskItem.objects.filter(
            provider=provider, id__in=archived_risk_items_ids
        ).delete()

        # Delete Meeting and MeetingArchive record
        MeetingArchive.objects.filter(
            provider=provider, id__in=archived_meeting_ids
        ).delete()
        Meeting.objects.filter(provider=provider, id__in=meeting_ids).delete()

        logger.debug(f"Finished archiving project Items for {provider}")


@shared_task
def sync_projects_engineers(provider_ids=None):
    """
    Fetch all the projects Engineers for the provider from connectwise
    and sync in the local database.
    The project engineers mapping are either created or updated based on
    the available project in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = []
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        try:
            settings_configured, message = ProjectService(
                provider
            ).query_service.check_project_settings()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        if not settings_configured:
            logger.info(
                f"PMO settings not configured for provider {provider}. Message: {message}. "
                f"Task skipped."
            )
            continue
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing Projects for {provider}")
        project_service: ProjectService = ProjectService(provider)
        project_query_service: ProjectQueryService = ProjectQueryService(
            provider
        )
        existing_projects: List[
            int
        ] = project_service.query_service.get_projects().values_list(
            "crm_id", flat=True
        )
        logger.debug(
            f"Found {len(existing_projects)} existing projects in local DB."
        )
        project_engineers = ProjectService(provider).fetch_project_engineer(
            existing_projects
        )
        engineering_role_ids = (
            project_service.query_service.get_engineering_role_ids()
        )
        for _project_engineers in project_engineers:
            if _project_engineers:
                engineers = filter(
                    lambda member: member.get("projectRole", {}).get("id")
                    in engineering_role_ids,
                    _project_engineers,
                )
                member_ids = [
                    user.get("member", {}).get("id") for user in engineers
                ]
                for project_engineer in _project_engineers:
                    if project_engineer and engineering_role_ids:
                        if (
                            project_engineer.get("member", {}).get("id")
                            in member_ids
                        ):
                            member_id = project_engineer.get("member", {}).get(
                                "id"
                            )
                            queryset = project_service.query_service.get_provider_user_by_member_id(
                                [member_id]
                            )
                            if queryset:
                                project_engineer[
                                    "member_id"
                                ] = queryset.values_list("id")[0]
                                projects_present_in_erp = set()
                                result["found"] += 1
                                project_id = project_engineer["projectId"]
                                project_id = project_query_service.get_project_by_crm_id(
                                    project_id
                                )
                                existing_project_engineer = (
                                    ProjectEngineersMapping.objects.filter(
                                        provider=provider,
                                        project_id=project_id,
                                        member_id=project_engineer.get(
                                            "member_id"
                                        ),
                                    )
                                )
                                if existing_project_engineer.exists():
                                    (
                                        res,
                                        errors,
                                    ) = ProjectEngineerPayloadHandler(
                                        provider,
                                        DbOperation.UPDATE,
                                        project_engineer,
                                    ).process()
                                    if res:
                                        logger.debug(
                                            f"Updated project Engineer for project: {project_id}"
                                        )
                                        result["updated"] += 1
                                    else:
                                        logger.debug(
                                            f"Updated project Engineer failed for project: {project_id}"
                                        )
                                        logger.debug(f"Errors: {errors}")
                                        result["update_failed"] += 1
                                        update_errors[project_id].append(
                                            errors
                                        )
                                else:
                                    (
                                        res,
                                        errors,
                                    ) = ProjectEngineerPayloadHandler(
                                        provider,
                                        DbOperation.CREATE,
                                        project_engineer,
                                    ).process()
                                    if res:
                                        logger.debug(
                                            f"Created project Engineer for project: {project_id}"
                                        )
                                        result["created"] += 1
                                    else:
                                        logger.debug(
                                            f"Create project Engineer failed for project: {project_id}"
                                        )
                                        logger.debug(f"Errors: {errors}")
                                        result["create_failed"] += 1
                                        create_errors[project_id].append(
                                            errors
                                        )
                                projects_present_in_erp.add(project_id)
                    # We are only fetching the projects from connectwise with open status
                    # So any project which is marked closed in connectwise will not be fetched.
                    # But deleting such project from Acela we will loose all it's work. So
                    # to avoid this projects once synced into Acela will not be deleted.

                    # deleted_projects = set(existing_projects) - projects_present_in_erp
                    # logger.info(f"Found {len(deleted_projects)} to be deleted.")
                    # res, errors = ProjectPayloadHandler(
                    #     provider, DbOperation.DELETE, payload={"id": deleted_projects}
                    # ).process()
                    # if res:
                    #     result["deleted"] += len(deleted_projects)
                    #     logger.info(f"Deleted {len(deleted_projects)} projects.")
                    # else:
                    #     result["delete_failed"] += len(deleted_projects)
                    #     logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(f"Project Sync finished for {provider}. Result: {result}")
        provider_results.append({provider.id: result})
    return provider_results


@shared_task
def sync_status_of_critical_path_and_action_items(
    provider_ids: Optional[List[int]] = None,
):
    """
    Task to sync status of critical path items and action items if due date is overdue.
    Run every 6 hours to compensate the offset for various timezones.

    Args:
        provider_ids: List of provider IDs
    """
    providers = (
        Provider.objects.filter(id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        try:
            project_service: ProjectService = ProjectService(provider)
            (
                settings_configured,
                message,
            ) = project_service.query_service.check_project_settings()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        if not settings_configured:
            logger.info(
                f"PMO settings not configured for the provider: {provider}. Message: {message}. "
                f"Sync skipped for the provider."
            )
            continue
        query_service: ProjectQueryService = project_service.query_service
        projects: "QuerySet[Project]" = Project.objects.filter(
            provider_id=provider.id
        )
        logger.info(
            f"Found {projects.count()} projects for the provider: {provider}, ID: {provider.id}"
        )
        try:
            OVERDUE_STATUS: ActivityStatusSetting = (
                ActivityStatusSetting.objects.get(
                    provider_id=provider.id,
                    title=ActivityStatusSetting.OVERDUE,
                )
            )
        except ActivityStatusSetting.DoesNotExist:
            logger.debug(
                f"Overdue activity status not configured for the provider: {provider}, ID: {provider.id}. "
                f"Skipping sync."
            )
            continue
        completed_status = query_service.get_action_status_done_status()
        for project in projects:
            # Ignore the items with overdue and complete status
            critical_path_items: "QuerySet[CriticalPathItem]" = (
                project.critical_path_items.exclude(
                    Q(status=OVERDUE_STATUS)
                    | Q(status=completed_status)
                    | Q(due_date__isnull=True)
                )
            )
            action_items: "QuerySet[ActionItem]" = (
                project.action_items.exclude(
                    Q(status=OVERDUE_STATUS)
                    | Q(status=completed_status)
                    | Q(due_date__isnull=True)
                )
            )
            if critical_path_items.count() > 0:
                overdue_critical_path_items: "QuerySet[CriticalPathItem]" = (
                    query_service.filter_overdue_items(
                        provider, critical_path_items
                    )
                )
                if overdue_critical_path_items.count() > 0:
                    logger.info(
                        f"Found {overdue_critical_path_items.count()} overdue critical path items "
                        f"for the project: {project.title}, ID: {project.id}"
                    )
                    overdue_critical_path_items.update(status=OVERDUE_STATUS)
            if action_items.count() > 0:
                overdue_action_items: "QuerySet[ActionItem]" = (
                    query_service.filter_overdue_items(provider, action_items)
                )
                if overdue_action_items.count() > 0:
                    logger.info(
                        f"Found {overdue_action_items.count()} overdue action items for the project: "
                        f"{project.title}, ID: {project.id}"
                    )
                    overdue_action_items.update(status=OVERDUE_STATUS)
            logger.info(
                f"Synced status of critical path items and action items for the project: {project.title},"
                f" ID: {project.id}"
            )
        logger.info(
            f"Synced critical path items and action items for the provider: {provider}, ID: {provider.id}"
        )
    logger.info(
        f"Sync completed for overdue critical path items and action items!"
    )
