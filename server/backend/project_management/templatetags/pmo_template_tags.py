from django import template

from accounts.utils import get_formatted_phone_number
from dateutil.parser import parse
from datetime import datetime

register = template.Library()


@register.filter(name="lighter_color")
def get_lighter_rgb_value_for_hex(hex_code, percent=0.2):
    if not hex_code:
        return ""
    hex_code = hex_code.lstrip("#")
    rgb = tuple(int(hex_code[i : i + 2], 16) for i in (0, 2, 4))
    return f"rgb({rgb[0]}, {rgb[1]}, {rgb[2]}, {percent})"


@register.filter(name="phone_number")
def phone_number(number):
    return get_formatted_phone_number(number, None)


@register.filter(name="iso_string_to_date")
def extract_date_from_iso_datetime_string(iso_datetime_string: str) -> str:
    """
    Extracts date from ISO datetime string.
    E.g. 2021-08-05T14:53:56+0000 -> Aug 05, 2021

    Args:
        iso_datetime_string: ISO datetime string

    Returns:
        (str) Date
    """
    date: str = parse(iso_datetime_string).strftime("%b %d %Y")
    return date
