class ProjectManagementAuditLogMixin:
    def get_additional_data(self):
        if hasattr(self, "project"):
            return {"project_id": self.project.id}
        else:
            return {}


class SpecificTypeMeetingAuditMixin(ProjectManagementAuditLogMixin):
    def get_additional_data(self):
        return {"project_id": self.meeting.project.id}


class ProjectSettingAuditMixin(ProjectManagementAuditLogMixin):
    def get_additional_data(self):
        return {"pmo_setting": True}
