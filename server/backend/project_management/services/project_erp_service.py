import datetime
from typing import Dict, Generator, List

from requests import Response

from accounts.models import Provider
from core.integrations import Connectwise
from utils import get_app_logger

logger = get_app_logger(__name__)


class ProjectERPService:
    def __init__(self, provider: Provider):
        self.provider: Provider = provider
        self.client: Connectwise = provider.erp_client

    def list_project_roles(self) -> List[Dict]:
        """
        Returns list of project roles
        """
        roles = self.client.list_project_roles()
        return roles

    def list_project_boards(self) -> List[Dict]:
        """
        Returns Project boards list from connectwise
        """
        boards = self.client.get_project_boards()
        return boards

    def list_project_statuses(self) -> List[Dict]:
        """
        Returns Project status list from connectwise
        """
        boards = self.client.get_project_statuses()
        return boards

    def get_all_projects(self, **kwargs) -> Generator[Response, None, None]:
        """
        Can be used to get the list for projects. Following parameters can be passed to
        apply filters on the projects.
        customer_ids: List of customer ids
        board_ids: List of board ids
        status_ids: List of status Ids

        Returns: This function returns a generator to the result which can be iterated to
        get all the pages of the result.

        """
        customer_ids: List[int] = kwargs.get("customer_ids", [])
        board_ids: List[int] = kwargs.get("board_ids", [])
        status_ids: List[int] = kwargs.get("status_ids", [])
        fields: List[str] = kwargs.get("fields", [])
        return self.client.list_projects(
            customer_ids, board_ids, status_ids, fields
        )

    def get_project_team_members(self, project_id: int) -> List[Dict]:
        """
        Returns the list of project team members.
        Args:
            project_id: Project CRM Id

        """
        try:
            team_members = self.client.get_project_team_members(project_id)
            return team_members
        except Exception as e:
            logger.info(f"Exception while fetching project team members: {e}")
            pass

    def get_project_customer_contacts(self, project_id: int) -> List[Dict]:
        """
        Returns the list of project contacts.
        Args:
            project_id: Project CRM Id

        """
        project_contacts = self.client.get_project_customer_contacts(
            project_id
        )
        return project_contacts

    def get_project_tickets(
        self, project_id: int
    ) -> Generator[Response, None, None]:
        """
        Get project tickets list

        """
        return self.client.get_project_tickets(project_id)

    def mark_project_ticket_complete(self, ticket_id, status_id):
        return self.client.update_project_ticket_status(ticket_id, status_id)

    def get_project_documents(self, project_id):
        """
        Returns the list of project documents.
        Args:
            project_id: Project CRM Id

        """
        return self.client.get_project_documents(project_id)

    def upload_project_documents(self, payload):
        """
        Returns the list of project documents.
        Args:
            project_id: Project CRM Id

        """
        return self.client.upload_system_attachments(payload)

    def create_project_callback(self, board_id):
        from core.webhook_service import ConnectwiseCallbackService

        ConnectwiseCallbackService.add_callback(
            self.provider,
            "Project",
            board_id,
            "Board",
            description="All Project callback",
        )

    def update_project(self, project_crm_id, **payload):
        return self.client.update_project(project_crm_id, **payload)

    def get_territory_manager(self, company_crm_id):
        company = self.client.get_customer(company_crm_id)
        return company.get("territory_manager_id")

    def create_project_customer_contacts(
        self, project_id: int, contatct_crm_id: int
    ):
        """
        Returns the list of project contacts.
        Args:
            project_id: Project CRM Id

        """
        project_contacts = self.client.create_project_customer_contact(
            project_id, contatct_crm_id
        )
        return project_contacts

    def delete_project_customer_contacts(
        self, project_id: int, contact_record_id: int
    ) -> List[Dict]:
        """
        Deletes project contacts with contact_record_id
        Args:
            project_id: Project CRM Id
            contact_record_id: Customer Contact CRM Id

        """
        project_contacts = self.client.delete_project_customer_contact(
            project_id, contact_record_id
        )
        return project_contacts

    def create_ticket_time_entry(self, payload):
        return self.provider.erp_client.create_time_entry(payload)

    def delete_project_documents(self, doc_crm_id: int):
        """
        Delete project documents on CW

        Args:
            doc_crm_id: Document crm ID
        """
        return self.provider.erp_client.get_delete_documents(doc_crm_id)
