import json
from typing import Dict, List, Tuple, Union, Optional

from django.conf import settings
from rest_framework.renderers import JSONRenderer

from accounts.models import User
from exceptions.exceptions import InvalidRequestError
from project_management.exceptions import ProjectSettingException
from project_management.models import (
    CloseOutMeeting,
    CustomerTouchMeeting,
    KickoffMeeting,
    Meeting,
    MeetingArchive,
    MeetingDocument,
    MeetingMailTemplate,
    Project,
    ProjectActivePhase,
    ProjectNote,
    ProjectPhase,
    StatusMeeting,
    AdditionalContacts,
)
from project_management.serializers import ProjectNoteSerializer
from project_management.serializers.meeting_serilalizer import (
    CloseOutMeetingReadSerializer,
    CustomerTouchMeetingReadSerializer,
    KickoffMeetingReadSerializer,
    MeetingAttendeeArchiveSerializer,
    MeetingDocumentSerializer,
    StatusMeetingReadSerializer,
)
from project_management.serializers.project_serilalizer import (
    ProjectAdditionalContactsSerializer,
)
from project_management.services import ProjectService
from utils import get_app_logger

logger = get_app_logger(__name__)


class MeetingService(ProjectService):
    def _perform_meeting_complete_common_actions(
        self,
        meeting: Meeting,
        ticket_id: int,
        phase: ProjectActivePhase,
        time_entry_payload: Dict,
    ) -> None:
        """
        This method performs the common steps in meeting `complete` action..
        It performs the following steps in order:
        1. Add time entry for the ticket
        2. Update the Project ticket status to complete.
        3. Set the meeting status to Done.
        4. Set the Active Phase status to Done.
        """
        # Step 1. Add time entry to the ticket
        self.create_project_time_entries(time_entry_payload)
        status_id = self.query_service.get_project_ticket_complete_status()
        if not status_id:
            raise ProjectSettingException(
                "Ticket complete status not configured under Project settings."
            )
        # Step 2. Update the Project ticket status to complete.
        if not (settings.ENV_NAME == settings.TEST_ENV_NAME or settings.DEBUG):
            try:
                self.erp_service.mark_project_ticket_complete(
                    ticket_id, status_id
                )
                self.event_log.append(
                    {
                        "label": "Project Ticket Complete Status",
                        "error": [],
                        "result": True,
                    }
                )
            except InvalidRequestError as e:
                self.event_log.append(
                    {
                        "label": "Project Ticket Complete Status",
                        "error": [e.detail],
                        "result": False,
                    }
                )
            logger.debug("Marked project ticket complete.")
        # Step 3. Set the meeting status to Done.
        meeting_status = self.query_service.mark_meeting_done(meeting)
        if meeting_status:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [],
                    "result": True,
                }
            )
        else:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [meeting_status],
                    "result": False,
                }
            )
        logger.debug("Marked meeting complete.")
        if phase:
            # Step 4. Set the Active Phase status to Done.
            phase_status = self.query_service.mark_project_phase_complete(
                phase
            )
            if phase_status:
                self.event_log.append(
                    {
                        "label": "Project Phase Complete Status",
                        "error": [],
                        "result": True,
                    }
                )
            else:
                self.event_log.append(
                    {
                        "label": "Project Phase Complete Status",
                        "error": [phase_status],
                        "result": False,
                    }
                )
            logger.debug("Marked project phase complete.")
        else:
            logger.info("Can't mark phase complete. Phase not found.")
            self.event_log.append(
                {
                    "label": "Project Phase Complete Status",
                    "error": ["Can't mark phase complete. Phase not found."],
                    "result": False,
                }
            )

    @staticmethod
    def get_meeting_email_variables(project: Project) -> Dict:
        email_variables = {
            "project_name": project.title,
            "customer_name": project.customer.name,
            "project_owner": f"{project.project_manager.first_name} "
            f"{project.project_manager.last_name}",
        }
        if project.primary_contact:
            email_variables.update(
                {
                    "customer_primary_contact": f"{project.primary_contact.first_name} "
                    f"{project.primary_contact.last_name}",
                    "customer_primary_contact_first_name": f"{project.primary_contact.first_name}",
                }
            )
        return email_variables

    @staticmethod
    def parse_special_variables_in_email(
        content: str,
        project_name: str,
        customer_name: str,
        customer_primary_contact: str = None,
        customer_primary_contact_first_name: str = None,
        project_owner: str = None,
    ) -> str:
        """
        Customer Touch email contains place-holders or variables which needs to be replaced with the actual
        values. This method builds a mapping of supported variables and their values and replaces the content
        with the actual values.

        Args:
            content: Content containing variables, which needs to be parsed.
            project_name: Project Name to be used for `#{Project Name}` variable.
            customer_name: Customer Name to be used for `#{Customer Name}` variable.
            customer_primary_contact: Customer Primary contact name to be used for
            `#{Customer Primary Contact}` variable.
            customer_primary_contact_first_name: Customer Primary contact first name to be used for
            `#{Customer Primary Contact First Name}` variable.
            project_owner: Project owner name to be used for `#{Project Owner}` variable.
        """
        RULE_STR = "#RULE#"

        supported_variables = {
            "#{Project Name}": project_name,
            "#{Customer Name}": customer_name,
            "#{Customer Primary Contact}": customer_primary_contact,
            "#{Customer Primary Contact First Name}": customer_primary_contact_first_name,
            "#{Project Owner}": project_owner,
        }
        for variable in supported_variables.keys():
            value = supported_variables.get(variable, "")
            if value:
                content = content.replace(variable, value)

        if RULE_STR in content:
            content = content.replace("#RULE#", "")

        return content

    @staticmethod
    def parse_special_variables_in_email_body(
        content: str,
        project_name: str,
        customer_name: str,
        customer_primary_contact: str = None,
        customer_primary_contact_first_name: str = None,
        project_owner: str = None,
    ) -> str:
        """
        Customer Touch email contains place-holders or variables which needs to be replaced with the actual
        values. This method builds a mapping of supported variables and their values and replaces the content
        with the actual values.

        Args:
            content: Content containing variables, which needs to be parsed.
            project_name: Project Name to be used for `#{Project Name}` variable.
            customer_name: Customer Name to be used for `#{Customer Name}` variable.
            customer_primary_contact: Customer Primary contact name to be used for
            `#{Customer Primary Contact}` variable.
            customer_primary_contact_first_name: Customer Primary contact first name to be used for
            `#{Customer Primary Contact First Name}` variable.
            project_owner: Project owner name to be used for `#{Project Owner}` variable.
        """
        supported_variables = {
            "{Project Name}": project_name,
            "{Customer Name}": customer_name,
            "{Customer Primary Contact}": customer_primary_contact,
            "{Customer Primary Contact First Name}": customer_primary_contact_first_name,
            "{Project Owner}": project_owner,
        }
        for variable in supported_variables.keys():
            value = supported_variables.get(variable, "")
            if value:
                content = content.replace(variable, value)

        content = content.replace("#", "")
        return content

    def get_project_data(self, project_id):
        project = (
            Project.objects.select_related("customer", "project_manager")
            .only(
                "title",
                "customer__name",
                "project_manager__first_name",
                "project_manager__last_name",
                "project_manager__email",
                "primary_contact__first_name",
                "primary_contact__last_name",
                "primary_contact__email",
            )
            .get(provider=self.provider, id=project_id)
        )

        return project

    def prepare_meeting_email_contents(
        self, project, email_subject, email_body
    ):
        """
        Returns variable replaced meeting mail subject and body.
        """

        parsed_email_subject = self.parse_special_variables_in_email(
            email_subject, **self.get_meeting_email_variables(project)
        )

        parsed_email_body = self.parse_special_variables_in_email_body(
            email_body, **self.get_meeting_email_variables(project)
        )

        return parsed_email_subject, parsed_email_body

    def get_parsed_meeting_mail_contents(self, meeting, project_id):
        """
        Returns variable replaced meeting contents for respective meeting type
        from the meeting template settings.
        """
        parsed_email_subject = ""
        parsed_email_body = ""

        project = self.get_project_data(project_id)
        try:
            meeting_mail_setting = MeetingMailTemplate.objects.get(
                provider=self.provider, meeting_type=meeting.meeting_type
            )
        except MeetingMailTemplate.DoesNotExist:
            raise ProjectSettingException(
                f"Email Template not configured for meeting type {meeting.meeting_type}"
            )

        (
            parsed_email_subject,
            parsed_email_body,
        ) = self.prepare_meeting_email_contents(
            project,
            meeting_mail_setting.email_subject,
            meeting_mail_setting.email_body_markdown,
        )

        return parsed_email_subject, parsed_email_body

    def prepare_meeting_email(
        self,
        meeting_type: str,
        meeting,
        project_id: int,
        add_customer_contacts: bool,
    ) -> Tuple[str, str, List[str]]:
        """
        This methods fetches the project details from DB using project_id along with the required
        related fields. It then calls method to replace the special variables available in
        meeting email subject and and body.
        Returns the parsed email subject, email body and the list of email recipients (Primary contact and
        Account Manager)
        """

        project = self.get_project_data(project_id)

        if not project.primary_contact:
            raise ValueError("Primary Contact not found for the project.")
        if not project.project_manager:
            raise ValueError("Project Manager not found for the project.")
        account_manager = self.get_project_account_manager(project_id)
        if not account_manager:
            raise ValueError("Account Manager not found for the project.")

        if meeting_type == Meeting.CUSTOMER_TOUCH:
            if not project.project_manager:
                raise ValueError("Project Manager not found for the project.")

            email_subject = meeting.email_subject
            email_body = meeting.email_body
            # if meeting.email_body_text is not None:
            #     email_body = meeting.email_body

        if meeting_type == Meeting.CLOSE_OUT:
            email_subject = f"Close Out Meeting - {project.title}"
            email_body = meeting.project_acceptance_markdown

        (
            parsed_email_subject,
            parsed_email_body,
        ) = self.prepare_meeting_email_contents(
            project, email_subject, email_body
        )
        customer_contacts_mails = self.get_project_customer_contact_emails(
            project_id
        )
        additional_attendees_emails = self.get_additional_contact_emails(
            project_id
        )
        if add_customer_contacts:
            recipients = (
                [
                    project.primary_contact.email,
                    account_manager.email,
                ]
                + customer_contacts_mails
                + additional_attendees_emails
            )
        else:
            recipients = [
                project.primary_contact.email,
                account_manager.email,
            ] + additional_attendees_emails
        return parsed_email_body, parsed_email_subject, recipients

    def complete_customer_touch_meeting(
        self,
        project_id: int,
        meeting: Meeting,
        customer_touch_meeting: CustomerTouchMeeting,
        ticket_id: int,
        time_entry_payload: dict,
        request_user: User,
        add_customer_contacts: bool,
    ):
        """
        This method performs the steps of a Customer touch meeting complete action.
        It first calls method to perform common steps and then prepares and sends the customer touch email.
        """
        customer_touch_phase = self.query_service.get_project_phase(
            project_id, ProjectPhase.CUSTOMER_TOUCH
        )
        self._perform_meeting_complete_common_actions(
            meeting, ticket_id, customer_touch_phase, time_entry_payload
        )

        # Prepare and send customer touch email.
        email_body, email_subject, recipients = self.prepare_meeting_email(
            Meeting.CUSTOMER_TOUCH,
            customer_touch_meeting,
            project_id,
            add_customer_contacts,
        )
        from project_management.tasks.meeting_emails import (
            send_customer_touch_email,
        )

        logger.debug(f"Customer Touch Email sent to {recipients}.")
        self.event_log.append(
            {"label": "Email Triggered", "error": [], "result": True}
        )
        send_customer_touch_email.apply_async(
            (
                self.provider.id,
                email_subject,
                email_body,
                recipients,
                request_user.id,
            ),
            queue="high",
        )
        return self.event_log

    def complete_close_out_meeting(
        self,
        project_id: int,
        meeting: Meeting,
        close_out_meeting: CloseOutMeeting,
        ticket_id: int,
        time_entry_payload: Dict,
        request_user: User,
        add_customer_contacts: bool,
    ):
        """
        This method performs the steps of a Close out meeting complete action.
        It first calls method to perform common steps and then prepares and sends the close out meeting email.
        """
        project_close_phase = self.query_service.get_project_phase(
            project_id, ProjectPhase.PROJECT_CLOSE
        )

        self._perform_meeting_complete_common_actions(
            meeting, ticket_id, project_close_phase, time_entry_payload
        )

        # Prepare and send customer touch email.
        email_body, email_subject, recipients = self.prepare_meeting_email(
            Meeting.CLOSE_OUT,
            close_out_meeting,
            project_id,
            add_customer_contacts,
        )

        from project_management.tasks.meeting_emails import (
            send_close_out_email,
        )

        meeting_documents = MeetingDocument.objects.filter(meeting=meeting)

        document_files = []
        for document in meeting_documents:
            logger.debug(
                f"Close out meeting documents {document.file_name, document.file_path.url}."
            )
            document_files.append((document.file_name, document.file_path.url))

        logger.debug(f"Close out meeting email sent to {recipients}.")
        self.event_log.append(
            {"label": "Email Triggered", "error": [], "result": True}
        )
        send_close_out_email.apply_async(
            (
                self.provider.id,
                email_subject,
                email_body,
                recipients,
                request_user.id,
                document_files,
            ),
            queue="high",
        )
        return self.event_log

    def get_project_team_members_email(self, project_id: int) -> List[str]:
        """
        Returns a list of emails of project team members. Project team members include
        the Project Manager, the Primary Contact and the Account manager.
        """
        team_members = self.get_project_team_members(project_id)
        return list(team_members.values_list("email", flat=True))

    def get_project_customer_contact_emails(
        self, project_id: int
    ) -> List[str]:
        project_crm_id = Project.objects.get(
            provider=self.provider, id=project_id
        ).crm_id
        customer_contacts = self.get_project_customer_contacts(
            project_crm_id, project_id
        )
        customer_contacts_mails = [
            contact.get("email")
            for contact in customer_contacts
            if contact.get("email")
        ]
        return customer_contacts_mails

    def get_kickoff_meeting_email_recipients(
        self, project_id: int, add_customer_contacts: bool
    ) -> List[str]:
        """
        Create a list of recipients for Kickoff meeting emails. This includes the entire project team
        and the customer contacts for the project.

        """
        project_team_members = self.get_project_team_members_email(project_id)
        if add_customer_contacts:
            customer_contacts_mails = self.get_project_customer_contact_emails(
                project_id
            )
            return project_team_members + customer_contacts_mails
        return project_team_members

    def complete_kickoff_meeting(
        self,
        project: Project,
        meeting: Meeting,
        ticket_id: int,
        time_entry_payload: Dict,
        request_user: User,
        add_customer_contacts: bool,
    ):
        """
        This method performs the steps of a Kickoff meeting complete action.
        It first calls method to perform common steps and then prepares and sends the email.
        """
        kickoff_phase = self.query_service.get_project_phase(
            project.id, ProjectPhase.KICKOFF
        )
        self._perform_meeting_complete_common_actions(
            meeting, ticket_id, kickoff_phase, time_entry_payload
        )
        email_recipients = self.get_kickoff_meeting_email_recipients(
            project.id, add_customer_contacts
        )
        additional_attendee_contact_emails = (
            self.get_additional_contact_emails(project.id)
        )
        email_recipients.extend(additional_attendee_contact_emails)
        (
            parsed_email_subject,
            parsed_email_body,
        ) = self.get_parsed_meeting_mail_contents(meeting, project.id)

        from project_management.tasks.meeting_emails import (
            send_kickoff_meeting_email,
        )

        logger.debug(
            f"(Project: {project} Meeting: {meeting})Sending Kickoff meeting email to {email_recipients}."
        )
        self.event_log.append(
            {"label": "Email Triggered", "error": [], "result": True}
        )
        send_kickoff_meeting_email.apply_async(
            (
                self.provider.id,
                parsed_email_subject,
                parsed_email_body,
                meeting.id,
                project.id,
                email_recipients,
                request_user.id,
                add_customer_contacts,
            ),
            queue="high",
        )
        return self.event_log

    def complete_internal_kickoff_meeting(
        self,
        project: Project,
        meeting: Meeting,
        ticket_id: int,
        time_entry_payload: Dict,
        request_user: User,
        add_customer_contacts: bool,
    ):
        """
        Mark internal kickoff meeting complete, update ticket status and send email
        """
        # Step 1. Add time entry to the ticket
        self.create_project_time_entries(time_entry_payload)

        status_id = self.query_service.get_project_ticket_complete_status()
        if not status_id:
            raise ProjectSettingException(
                "Ticket complete status not configured under Project settings."
            )
        if not (settings.ENV_NAME == settings.TEST_ENV_NAME or settings.DEBUG):
            try:
                self.erp_service.mark_project_ticket_complete(
                    ticket_id, status_id
                )
                self.event_log.append(
                    {
                        "label": "Project Ticket Complete Status",
                        "error": [],
                        "result": True,
                    }
                )
            except InvalidRequestError as e:
                self.event_log.append(
                    {
                        "label": "Project Ticket Complete Status",
                        "error": [e.detail],
                        "result": False,
                    }
                )
            logger.debug("Marked project ticket complete.")
        meeting_status = self.query_service.mark_meeting_done(meeting)
        if meeting_status:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [],
                    "result": True,
                }
            )
        else:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [meeting_status],
                    "result": False,
                }
            )
        logger.debug("Marked meeting complete.")

        (
            parsed_email_subject,
            parsed_email_body,
        ) = self.get_parsed_meeting_mail_contents(meeting, project.id)

        from project_management.tasks.meeting_emails import (
            send_internal_kickoff_meeting_email,
        )

        project_team_members = self.get_project_team_members_email(project.id)
        # Remove customer contact from the project managers list
        if project.primary_contact:
            project_primary_contact_email = project.primary_contact.email
            try:
                project_team_members.remove(project_primary_contact_email)
            except ValueError:
                pass
        logger.debug(
            f"(Project: {project} Meeting: {meeting})Sending Kickoff meeting email to {project_team_members}."
        )
        self.event_log.append(
            {"label": "Email Triggered", "error": [], "result": True}
        )
        send_internal_kickoff_meeting_email.apply_async(
            (
                self.provider.id,
                parsed_email_subject,
                parsed_email_body,
                project_team_members,
                request_user.id,
                project.id,
                meeting.id,
                add_customer_contacts,
            ),
            queue="high",
        )
        return self.event_log

    def get_additional_contact_emails(self, project_id):
        try:
            project: Project = Project.objects.get(id=project_id)
            provider_id = project.provider_id
        except Project.DoesNotExist:
            return []
        additional_attendees = AdditionalContacts.objects.filter(
            project_id=project_id, provider_id=provider_id
        )
        return list(additional_attendees.values_list("email", flat=True))

    def complete_status_meeting(
        self,
        project_id: int,
        meeting: Meeting,
        time_entry_payload: Dict,
        request_user: User,
        add_customer_contacts: bool,
    ):
        """
        Mark status meeting complete and send email
        """
        # Step 1. Add time entry to the ticket
        self.create_project_time_entries(time_entry_payload)
        meeting_status = self.query_service.mark_meeting_done(meeting)
        if meeting_status:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [],
                    "result": True,
                }
            )
        else:
            self.event_log.append(
                {
                    "label": "Meeting Complete Status",
                    "error": [meeting_status],
                    "result": False,
                }
            )
        logger.debug("Marked meeting complete.")
        project_team_members = self.get_project_team_members_email(project_id)
        customer_contacts_mails = self.get_project_customer_contact_emails(
            project_id
        )
        additional_attendee_contact_emails = (
            self.get_additional_contact_emails(project_id)
        )
        if add_customer_contacts:
            recipients = (
                project_team_members
                + customer_contacts_mails
                + additional_attendee_contact_emails
            )
        else:
            recipients = (
                project_team_members + additional_attendee_contact_emails
            )

        (
            parsed_email_subject,
            parsed_email_body,
        ) = self.get_parsed_meeting_mail_contents(meeting, project_id)

        from project_management.tasks.meeting_emails import send_status_email

        logger.debug(
            f"(Project: {project_id} Meeting: {meeting})Sending Status meeting email to {project_team_members}."
        )
        self.event_log.append(
            {"label": "Email Triggered", "error": [], "result": True}
        )
        send_status_email.apply_async(
            (
                self.provider.id,
                parsed_email_subject,
                parsed_email_body,
                meeting.id,
                project_id,
                recipients,
                request_user.id,
                add_customer_contacts,
            ),
            queue="high",
        )
        return self.event_log

    @staticmethod
    def get_meeting_notes_serialized_object(notes: ProjectNote) -> List[Dict]:
        """
        Gives back serialized meeting notes
        """
        serializer = ProjectNoteSerializer(notes, many=True)
        return serializer.data

    @staticmethod
    def get_customer_touch_meeting_serialized_object(
        customer_touch_meeting: CustomerTouchMeeting,
    ) -> Dict:
        """
        Creates a serialized object for a Customer Touch Meeting

        """
        serializer = CustomerTouchMeetingReadSerializer(customer_touch_meeting)
        serialized_meeting = {"meeting": serializer.data}
        return serialized_meeting

    @staticmethod
    def get_close_out_meeting_serialized_object(
        close_out_meeting: CloseOutMeeting,
    ) -> Dict:
        """
        Creates a serialized object for a Closeout Meeting

        """
        serializer = CloseOutMeetingReadSerializer(close_out_meeting)
        meeting_documents = MeetingDocument.objects.filter(
            meeting=close_out_meeting.meeting
        )
        serialized_documents = MeetingDocumentSerializer(
            meeting_documents, many=True
        )
        serialized_meeting = {
            "meeting": serializer.data,
            "documents": serialized_documents.data,
        }
        return serialized_meeting

    def get_meeting_items(
        self, project_id: int, meeting: Union[KickoffMeeting, StatusMeeting]
    ) -> Dict:
        """
        Fetches critical path items, action items, risk items and notes for a meeting.
        Serializes these objects and returns a dict of these serialized objects.
        """
        risk_items = self.query_service.get_risk_items(project_id)
        critical_path_items = (
            self.query_service.get_project_critical_path_items(project_id)
        )
        action_items = self.query_service.get_project_action_items(project_id)
        meeting_notes = self.query_service.get_meeting_notes(
            meeting.meeting.id, project_id
        )
        meeting_attendees = self.query_service.get_meeting_attendees(
            meeting.meeting
        )
        meeting_attendees_serialized = MeetingAttendeeArchiveSerializer(
            meeting_attendees, many=True
        )
        external_meeting_attendees = (
            self.query_service.get_additional_meeting_attendees(project_id)
        )
        external_meeting_attendees_serialized = (
            ProjectAdditionalContactsSerializer(
                external_meeting_attendees, many=True
            )
        )
        serialized_meeting_items = {
            "risk_items": self.serialize_risk_item_object(risk_items),
            "critical_path_items": self.serialize_critical_path_item_object(
                critical_path_items
            ),
            "action_items": self.serialize_action_item_object(action_items),
            "meeting_notes": self.get_meeting_notes_serialized_object(
                meeting_notes
            ),
            "meeting_attendee": meeting_attendees_serialized.data,
            "external_meeting_attendees": external_meeting_attendees_serialized.data,
        }
        return serialized_meeting_items

    def get_status_meeting_serialized_object(
        self, status_meeting: StatusMeeting, project_id: int
    ) -> Dict:
        """
        Returns a dict with the serialized data of the status meeting and it's related objects.

        """
        serializer = StatusMeetingReadSerializer(status_meeting)
        serialized_meeting = {"meeting": serializer.data}
        meeting_items = self.get_meeting_items(project_id, status_meeting)
        serialized_meeting.update(meeting_items)
        return serialized_meeting

    def get_kickoff_meeting_archive_json(self, kickoff_meeting, project_id):
        """
        Returns a dict with the serialized data of the kickoff/internal kickoff meeting and it's related
        objects.

        """
        serializer = KickoffMeetingReadSerializer(kickoff_meeting)
        serialized_meeting = {"meeting": serializer.data}
        meeting_items = self.get_meeting_items(project_id, kickoff_meeting)
        serialized_meeting.update(meeting_items)
        return serialized_meeting

    def archive_meeting(
        self,
        meeting: Union[CustomerTouchMeeting, KickoffMeeting, StatusMeeting],
        project_id: int,
    ) -> MeetingArchive:
        """
        Calls method to get a serialized object for a meeting, based on the type.
        Stores this serialized object to MeetingArchive table.

        """
        serialized_meeting = None
        if meeting.meeting.meeting_type == Meeting.CUSTOMER_TOUCH:
            serialized_meeting = (
                self.get_customer_touch_meeting_serialized_object(meeting)
            )
        elif meeting.meeting.meeting_type == Meeting.STATUS:
            serialized_meeting = self.get_status_meeting_serialized_object(
                meeting, project_id
            )
        elif meeting.meeting.meeting_type in [
            Meeting.KICKOFF,
            Meeting.INTERNAL_KICKOFF,
        ]:
            serialized_meeting = self.get_kickoff_meeting_archive_json(
                meeting, project_id
            )
        elif meeting.meeting.meeting_type == Meeting.CLOSE_OUT:
            serialized_meeting = self.get_close_out_meeting_serialized_object(
                meeting
            )
        assert serialized_meeting

        meeting_json = JSONRenderer().render(serialized_meeting)
        meeting_archive = MeetingArchive.objects.create(
            provider=self.provider,
            project_id=project_id,
            meeting_id=meeting.meeting.id,
            payload=json.loads(meeting_json),
        )
        return meeting_archive
