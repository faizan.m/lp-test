import asyncio
import datetime
from collections import OrderedDict
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from typing import Dict, Generator, List, Optional, Tuple, Union
from pytz import timezone as pytz_timezone
from celery import chain
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.db.models import (
    BooleanField,
    Case,
    CharField,
    Count,
    DateField,
    DurationField,
    Exists,
    ExpressionWrapper,
    F,
    IntegerField,
    OuterRef,
    Prefetch,
    Q,
    QuerySet,
    Subquery,
    Value,
    When,
    FloatField,
)
from django.db.models.functions import Cast, Greatest
from django.utils import timezone
from requests import Response

from accounts.models import Customer, Provider, User
from core.integrations.erp.connectwise import field_mappings
from core.integrations.utils import prepare_response
from document.services import DocumentService, QuoteService
from exceptions.exceptions import InvalidRequestError, ValidationError
from project_management.models import (
    ActionItem,
    ActivityStatusSetting,
    AdditionalSetting,
    CloseOutMeeting,
    CriticalPathItem,
    CustomerContactRole,
    CustomerTouchMeeting,
    DateStatusSetting,
    EngineerRoleMapping,
    KickoffMeeting,
    Meeting,
    MeetingArchive,
    MeetingAttendee,
    MilestoneStatusSetting,
    OverallStatusSetting,
    Project,
    ProjectActivePhase,
    ProjectBoard,
    ProjectContactRole,
    ProjectNote,
    ProjectPhase,
    ProjectType,
    RiskItem,
    StatusMeeting,
    TimeStatusSetting,
    ProjectItemsArchive,
    PreSalesEngineerRoleMapping,
    ExternalAttendee,
    AdditionalContacts,
    ProjectEngineersMapping,
    ProjectAfterHoursMapping,
)
from project_management.serializers.project_serilalizer import (
    ActionItemSerializer,
    CriticalPathItemSerializer,
    ProjectCreateSerializer,
    ProjectUpdateSerializer,
    RiskItemSerializer,
    ProjectEngineerCreateSerializer,
    ProjectEngineerUpdateSerializer,
)
from project_management.services import ProjectERPService
from utils import DbOperation, get_app_logger
from django.contrib.postgres.fields.jsonb import KeyTransform

logger = get_app_logger(__name__)


class ProjectQueryService:
    def __init__(self, provider):
        self.provider = provider

    def get_project_board_ids(self) -> List[int]:
        """
        Get the list of mapped board id's for the provider
        Returns: List of connectwise board Id's.

        """
        boards = ProjectBoard.objects.filter(
            provider=self.provider
        ).values_list("connectwise_id", flat=True)
        return boards

    def get_board_with_open_statuses(self) -> Dict[int, List[int]]:
        """
        Get the list of unique boards along with all the open
        statuses mapped to them.
        Returns: A dict with board's connectwise Id as the key and
        the list of connectwise Id's of the open statuses mapped to the
        board.

        """
        statuses = OverallStatusSetting.objects.filter(
            provider=self.provider, is_open=True
        ).values_list("board_id__connectwise_id", "connectwise_id")
        statuses = list(statuses)
        board_mapping = defaultdict(list)
        for board_id, status_id in statuses:
            board_mapping[board_id].append(status_id)
        return board_mapping

    def get_projects(self):
        projects = Project.objects.filter(provider_id=self.provider.id)
        return projects

    def get_customer_by_crm_id(self, crm_id: int) -> Optional[int]:
        """
        Searches the customer on the crm_id field and returns the DB Id if found, else
        returns None.
        Args:
            crm_id: CRM Id of the customer

        """
        try:
            customer = Customer.objects.get(
                provider=self.provider, crm_id=crm_id
            ).id
        except Customer.DoesNotExist:
            return None
        return customer

    def get_provider_user_by_crm_id(self, crm_id: int) -> Optional[str]:
        """
        Searches the Provider users on the customer_user_instance_id field and returns the
        UUID of the found user. If no User is found returns None.
        Args:
            crm_id: User CRM Id
        """
        try:
            user = User.providers.get(
                provider=self.provider,
                profile__customer_user_instance_id=crm_id,
            ).id
        except User.DoesNotExist:
            return None
        return user

    def get_provider_user_by_member_id(
        self, member_ids: List[int]
    ) -> "QuerySet[User]":
        """
        Searches the Provider users on the profile__system_member_crm_id field and returns the
        first_nme, last_name and profile_pic of the found user. If no User is found returns None.
        Args:
            member_ids: List of User System Member Id
        """
        fields = [
            "email",
            "first_name",
            "last_name",
            "profile__profile_pic",
            "profile__system_member_crm_id",
            "profile__office_phone",
        ]
        users = self.get_user_profile_info(
            fields, profile__system_member_crm_id__in=member_ids
        )
        return users

    def get_customer_user_by_crm_id(self, crm_id: int) -> Optional[str]:
        """
        Searches the Customer users on the crm_id field and returns the
        UUID of the found user. If no User is found returns None.
        Args:
            crm_id: User CRM Id
        """
        try:
            user = User.customers.get(provider=self.provider, crm_id=crm_id).id
        except User.DoesNotExist:
            return None
        return user

    def get_project_by_crm_id(self, crm_id: int) -> Optional[int]:
        try:
            project = Project.objects.get(
                provider=self.provider, crm_id=crm_id
            ).id
        except Project.DoesNotExist:
            return None
        return project

    def get_board_by_crm_id(self, crm_id: int) -> Optional[int]:
        """
        Searches the ProjectBoard on the connectwise_id field and returns the
        Id of the found Board. If no Board is found returns None.
        Args:
            crm_id: Board CRM Id
        """
        try:
            board = ProjectBoard.objects.get(
                provider=self.provider, connectwise_id=crm_id
            ).id
        except ProjectBoard.DoesNotExist:
            return None
        return board

    def get_overall_status_by_crm_id(
        self, crm_id: int
    ) -> Optional[OverallStatusSetting]:
        """
        Searches the OverallStatusSetting on the connectwise_id field and returns the
        Id of the found OverallStatus. If no OverallStatus is found returns None.
        Args:
            crm_id: Board CRM Id
        """
        try:
            overall_status = OverallStatusSetting.objects.get(
                provider=self.provider, connectwise_id=crm_id, is_default=True
            )
        except OverallStatusSetting.DoesNotExist:
            return None
        except OverallStatusSetting.MultipleObjectsReturned:
            overall_status = OverallStatusSetting.objects.filter(
                provider=self.provider, connectwise_id=crm_id, is_default=True
            ).first()
        return overall_status

    def get_time_status_setting_queryset(self, value=None):
        """
        Query on the TimeStatusSetting model to return configured settings.
        By default all the records for the provider are returned.
        If the value parameter is given, the records are filtered
        where the given value falls in the slot_range.

        """
        time_status_setting = TimeStatusSetting.objects.filter(
            provider=self.provider
        )
        if value:
            time_status_setting = time_status_setting.filter(
                slot_range__contains=value
            )
        time_status_setting = time_status_setting.order_by("slot_range")
        return time_status_setting

    def get_date_status_setting(self):
        date_status_setting = DateStatusSetting.objects.filter(
            provider=self.provider
        ).order_by("slot_range")
        return date_status_setting

    def get_default_project_manager(self) -> User:
        """
        returns the user set as default project manager in the
        additional project settings

        """
        return (
            self.provider.additional_project_settings.default_project_manager
        )

    @staticmethod
    def create_project(payload: Dict) -> Project:
        """
        Creates a new Project in db.
        Args:
            payload: Project data

        """
        project = Project.objects.create(**payload)
        return project

    def update_project_type(
        self,
        project: Project,
        project_type: ProjectType,
        previous_project_type: Optional[ProjectType],
    ) -> None:
        """
        Update project type for a project. This step includes, updating all the phases.
        Delete all existing records from ProjectActivePhase table for this project
        Create entry of all phases for the new project type in the ProjectActivePhase table
        Args:
            project: Project instance to be updated
            project_type: New project type instance
        """

        # Delete all existing records from ProjectActivePhase table for this project
        if previous_project_type:
            ProjectActivePhase.objects.filter(
                provider=self.provider,
                project_id=project.id,
                phase__project_type=previous_project_type,
            ).delete()

        # Create entry of all phases for the new project type in the ProjectActivePhase table
        new_project_phases = ProjectPhase.objects.filter(
            project_type=project_type
        ).order_by("order")
        len_new_project_phases = new_project_phases.count()

        for i, phase in enumerate(new_project_phases):
            estimated_completion_date = None
            # For the last phase i.e. Project Close phase the estimated_completion_date
            # should be equal to the project estimated_completion_date

            if i == len_new_project_phases - 1:
                estimated_completion_date = project.estimated_end_date

            ProjectActivePhase.objects.create(
                provider=self.provider,
                project=project,
                phase=phase,
                estimated_completion_date=estimated_completion_date,
            )

    def update_project_close_phase(self, project: Project, estimated_end_date):
        """
        Updates the estimated end date for the project last phase. The last phase
        estimated end date is the estimated end date of the project and need to be kept
        in sync with connectwise data.
        """
        last_phase: ProjectActivePhase = (
            ProjectActivePhase.objects.filter(
                provider=self.provider, project=project
            )
            .order_by("-phase__order")
            .first()
        )
        if last_phase:
            last_phase.estimated_end_date = estimated_end_date
            last_phase.save()

    def update_project(
        self, project: Project, payload: Dict, updated_by: User
    ) -> Project:
        """
        Update a project instance in DB. Handles type update explicitly.
        Args:
            project: project instance to be updated
            payload: new payload for project
            updated_by: User instance

        """
        project_type = payload.get("type", None)
        overall_status = payload.get("overall_status", None)
        description = payload.get("description", None)
        estimated_end_date = payload.get("estimated_end_date", None)
        payload_for_cw = {}

        if project_type is not None and project_type != project.type:
            self.update_project_type(project, project_type, project.type)
        if overall_status and overall_status != project.overall_status:
            payload_for_cw.update(
                dict(overall_status_id=overall_status.connectwise_id)
            )
        if description and description != project.description:
            payload_for_cw.update(dict(description=description))
        if (
            estimated_end_date
            and estimated_end_date != project.estimated_end_date
        ):
            payload_for_cw.update(dict(estimated_end_date=estimated_end_date))
            self.update_project_close_phase(project, estimated_end_date)
        if payload_for_cw:
            ProjectERPService(self.provider).update_project(
                project.crm_id, **payload_for_cw
            )
        for attr, value in payload.items():
            setattr(project, attr, value)
        project.last_updated_by = updated_by
        project.save()
        return project

    def update_project_by_crm_id(
        self, project_crm_id: int, payload: Dict
    ) -> object:
        """
        Updates the project instance where crm_id is project_crm_id.
        Sets the new attributes using payload.
        Args:
            project_crm_id: CRM id of the project
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        project_query = Project.objects.filter(
            provider=self.provider, crm_id=project_crm_id
        )
        project_query.update(**payload)
        project = project_query.first()
        if project:
            # estimated end date need to updated in the project phases last phase as well.
            estimated_end_date = payload.get("estimated_end_date")
            if estimated_end_date:
                self.update_project_close_phase(project, estimated_end_date)
        return project_query

    @staticmethod
    def delete_project(filter_conditions: Dict) -> object:
        """
        Delete projects filtered using the filter conditions.
        Args:
            filter_conditions: A dict of Project model fields as key and
            value of the key.

        """
        res = Project.objects.filter(**filter_conditions).delete()
        return res

    def update_engineers(self, member_crm_id: int, payload: Dict) -> object:
        """
        Updates the engineers instance where crm_id is member_crm_id.
        Sets the new attributes using payload.
        Args:
            member_crm_id: CRM id of the member
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        member_query = ProjectEngineersMapping.objects.filter(
            provider=self.provider,
            member_crm_id=member_crm_id,
            member_id=payload.get("member_id"),
            project_id=payload.get("project.id"),
        )
        member_query.update(**payload)
        return member_query

    def create_engineer(self, payload: Dict):
        try:
            project_engineer = ProjectEngineersMapping.objects.create(
                **payload
            )
        except IntegrityError as e:
            logger.info(e)
            return None
        return project_engineer

    @staticmethod
    def get_time_status_value_expression():
        """
        Expression to calculate remaining time percent using
        ((hours_budget - actual_budget)/hours_budget)*100
        In case the above expression gives negative, limit it to 0

        """

        time_status_value_expression = Greatest(
            Cast(
                (
                    (
                        (F("hours_budget") - F("actual_hours"))
                        / Greatest(F("hours_budget"), Value(1))
                    )
                    * Value(100)
                ),
                IntegerField(),
            ),
            Value(0),
        )
        return time_status_value_expression

    @staticmethod
    def get_project_progress_expression():
        """
        Expression to calculate project progress  using
        (actual_budget/hours_budget)*100

        """

        progress_expression = Cast(
            (
                (F("actual_hours") / Greatest(F("hours_budget"), Value(1)))
                * Value(100)
            ),
            IntegerField(),
        )
        return progress_expression

    @staticmethod
    def get_percent_of_budget_expression():
        percent_of_budget = Case(
            When(
                Q(hours_budget__in=[0]) | Q(actual_hours__in=[0]),
                then=Value(0),
            ),
            default=Cast(
                ((F("actual_hours") / F("hours_budget")) * Value(100)),
                FloatField(),
            ),
            output_field=FloatField(),
        )
        return percent_of_budget

    @staticmethod
    def get_remaining_hours_expression():
        remaining_hours = Cast(
            (F("hours_budget") - F("actual_hours")),
            FloatField(),
        )
        return remaining_hours

    @staticmethod
    def get_estimated_hours_to_complete_query():
        estimated_hours_to_complete = KeyTransform(
            "estimated_hours_to_complete", "project_updates"
        )
        return estimated_hours_to_complete

    @staticmethod
    def get_date_status_value_expression():
        """
        Expression to calculate difference between estimated_end_date and current date

        """
        current_datetime = timezone.now()
        date_status_expr = ExpressionWrapper(
            (F("estimated_end_date") - current_datetime),
            output_field=DurationField(),
        )
        return date_status_expr

    def get_date_status_color_expression(self):
        """
        Returns query expression to calculate date status color based on
        date status settings.

        """
        current_datetime = timezone.now()
        date_status_setting = list(self.get_date_status_setting())
        if not len(date_status_setting) == 2:
            return Value(None, output_field=CharField())
        first_slot: DateStatusSetting = date_status_setting[0]
        second_slot: DateStatusSetting = date_status_setting[1]
        threshold: int = first_slot.slot_range.upper
        first_slot_color = first_slot.color
        second_slot_color = second_slot.color
        threshold_date = current_datetime + datetime.timedelta(days=threshold)
        date_status_color_expr = Case(
            When(
                estimated_end_date__lt=current_datetime, then=Value("#db1643")
            ),
            When(
                estimated_end_date__lt=threshold_date,
                then=Value(first_slot_color),
            ),
            When(
                estimated_end_date__gte=threshold_date,
                then=Value(second_slot_color),
            ),
            output_field=CharField(),
        )
        return date_status_color_expr

    def get_critical_time_status_expression(self):
        """
        returns query expression to calculate the is_time_status_critical
        flag. This flag is true if the time status value lies in the first (min)
        slot range.

        """
        time_status_setting_query = (
            self.get_time_status_setting_queryset().only("slot_range")
        )
        first_slot = time_status_setting_query.first()
        if not first_slot:
            return Value(None, output_field=CharField())
        lower_bound = first_slot.slot_range.lower
        upper_bound = first_slot.slot_range.upper
        time_status_critical_expr = Case(
            When(
                time_status_value__range=(lower_bound, upper_bound), then=True
            ),
            default=False,
            output_field=BooleanField(),
        )
        return time_status_critical_expr

    @staticmethod
    def get_time_status_color_expression():
        """
        Subquery Expression to find the color for time status
        using time status settings

        """
        time_status_color = Subquery(
            (
                TimeStatusSetting.objects.filter(
                    provider=OuterRef("provider"),
                    slot_range__contains=OuterRef("time_status_value"),
                )
                .order_by("slot_range")
                .values("color")
            )[:1]
        )
        return time_status_color

    @staticmethod
    def get_action_status_done_status():
        """
        Returns Subquery to find the done status id for ActivityStatusSetting
        """
        action_status_done_status = Subquery(
            ActivityStatusSetting.objects.filter(
                provider=OuterRef("provider"), title=ActivityStatusSetting.DONE
            ).values("id")[:1]
        )
        return action_status_done_status

    def get_overdue_critical_path_items_expression(self) -> "Subquery":
        """
        Returns the queryset of overdue (due_date in the past) critical path items
        for the project.
        """
        action_status_done_status = self.get_action_status_done_status()
        current_date = timezone.now().date()
        overdue_critical_path_items = Subquery(
            CriticalPathItem.objects.values("project_id")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                due_date__date__lt=current_date,
            )
            .exclude(status=action_status_done_status)
            .annotate(cnt=Count("*"))
            .values("cnt")[:1],
            output_field=IntegerField(),
        )
        return overdue_critical_path_items

    def get_due_date_approaching_critical_path_items_expression(
        self, threshold_days
    ) -> "Subquery":
        """
        Returns the queryset of overdue (due_date in the past) critical path items
        for the project.
        """
        action_status_done_status = self.get_action_status_done_status()
        current_date = timezone.now().date()
        expiring_in_days = ExpressionWrapper(
            F("due_date") - current_date, output_field=DurationField()
        )
        overdue_critical_path_items = Subquery(
            CriticalPathItem.objects.filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                due_date__date__gte=current_date,
            )
            .annotate(expiring_in_days=expiring_in_days)
            .filter(
                expiring_in_days__lte=datetime.timedelta(
                    threshold_days, seconds=0
                )
            )
            .exclude(status=action_status_done_status)
            .values("project_id")
            .annotate(cnt=Count("*"))
            .values("cnt")[:1],
            output_field=IntegerField(),
        )
        return overdue_critical_path_items

    def get_overdue_action_items_expression(self) -> "Subquery":
        """
        Returns the queryset of overdue (due_date in the past) action items
        for the project.
        """
        action_status_done_status = self.get_action_status_done_status()
        current_date = timezone.now().date()
        overdue_action_items = Subquery(
            ActionItem.objects.values("project_id")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                due_date__date__lt=current_date,
            )
            .exclude(status=action_status_done_status)
            .annotate(cnt=Count("*"))
            .values("cnt")[:1],
            output_field=IntegerField(default=0),
        )
        return overdue_action_items

    def get_due_date_approaching_action_items_expression(
        self, threshold_days
    ) -> "Subquery":
        """
        Returns the queryset of overdue (due_date in the past) action items
        for the project.
        """
        action_status_done_status = self.get_action_status_done_status()
        current_date = timezone.now().date()
        expiring_in_days = ExpressionWrapper(
            F("due_date") - current_date, output_field=DurationField()
        )
        overdue_action_items = Subquery(
            ActionItem.objects.filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                due_date__date__gte=current_date,
            )
            .annotate(expiring_in_days=expiring_in_days)
            .filter(
                expiring_in_days__lte=datetime.timedelta(
                    threshold_days, seconds=0
                )
            )
            .exclude(status=action_status_done_status)
            .values("project_id")
            .annotate(cnt=Count("*"))
            .values("cnt")[:1],
            output_field=IntegerField(default=0),
        )
        return overdue_action_items

    @staticmethod
    def filter_overdue_items(
        provider: Provider,
        items: Union["QuerySet[CriticalPathItem]", "QuerySet[ActionItem]"],
    ) -> Union["QuerySet[CriticalPathItem]", "QuerySet[ActionItem]"]:
        """
        Filter out overdue critical path items and action items by taking into account the
        provider's local timezone.
        Convert due date from utc timezone to provider's timezone and then compare dates.

        Args:
            items: Could be critical path items or action items

        Returns:
            Overdue critical path items
        """
        provider_timezone = pytz_timezone(provider.timezone)
        provider_current_date = datetime.datetime.now(provider_timezone).date()
        items_data = items.values_list("id", "due_date")
        overdue_item_ids: List[int] = []
        for id, utc_due_date in items_data:
            due_date = utc_due_date.astimezone(provider_timezone).date()
            if due_date < provider_current_date:
                overdue_item_ids.append(id)
        return items.filter(id__in=overdue_item_ids)

    def get_last_action_date_expression(self):
        """
        Returns query expression to calculate the last action date value.
        last action value is the most recent updated time of any Action Item,
        Critical Path Item or Meeting that is marked done.

        """
        done_status = self.get_action_status_done_status()
        meeting_done_status = Meeting.DONE
        most_recently_completed_action_item = Subquery(
            ActionItem.objects.values("provider")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                status=done_status,
            )
            .order_by("-updated_on")
            .values("updated_on")[:1]
        )
        most_recently_completed_critical_path_item = Subquery(
            CriticalPathItem.objects.values("provider")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                status=done_status,
            )
            .order_by("-updated_on")
            .values("updated_on")[:1]
        )
        most_recent_completed_meeting = Subquery(
            Meeting.objects.values("provider")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
                status=meeting_done_status,
            )
            .order_by("-updated_on")
            .values("updated_on")[:1]
        )
        most_recently_archive_item = Subquery(
            ProjectItemsArchive.objects.values("provider")
            .filter(
                provider=OuterRef("provider"),
                project_id=OuterRef("id"),
            )
            .order_by(F("updated_on").desc(nulls_last=True))
            .values("updated_on")[:1]
        )
        last_action_date_expression = Greatest(
            most_recently_completed_action_item,
            most_recently_completed_critical_path_item,
            most_recent_completed_meeting,
            most_recently_archive_item,
        )
        return last_action_date_expression

    def get_listing_api_queryset(
        self,
        status_filter=None,
        filter_conditions=None,
        selected_fields=None,
        select_related_fields=True,
    ):
        filter_conditions = filter_conditions or {}
        GREEN_COLOR = "#176F07"
        RED_COLOR = "#DB1643"
        YELLOW_COLOR = "#DBA629"
        time_status_value_expression = self.get_time_status_value_expression()
        time_status_color = self.get_time_status_color_expression()
        is_time_status_critical_expression = (
            self.get_critical_time_status_expression()
        )
        date_status_expr = self.get_date_status_value_expression()
        date_status_color_expr = self.get_date_status_color_expression()
        progress_calculation_expression = (
            self.get_project_progress_expression()
        )
        overdue_critical_path_items_expression = (
            self.get_overdue_critical_path_items_expression()
        )
        overdue_action_items_expression = (
            self.get_overdue_action_items_expression()
        )
        last_action_date_expression = self.get_last_action_date_expression()
        try:
            project_item_due_date_threshold = (
                AdditionalSetting.objects.only(
                    "project_item_due_date_threshold"
                )
                .get(provider=self.provider)
                .project_item_due_date_threshold
            )
        except AdditionalSetting.DoesNotExist:
            project_item_due_date_threshold = 7
        expiring_action_items = (
            self.get_due_date_approaching_action_items_expression(
                project_item_due_date_threshold
            )
        )
        expiring_critical_path_items = (
            self.get_due_date_approaching_critical_path_items_expression(
                project_item_due_date_threshold
            )
        )

        # select only the required fields helps to reduce query time

        selected_fields = selected_fields or [
            "id",
            "title",
            "crm_id",
            "hours_budget",
            "actual_hours",
            "estimated_end_date",
            "closed_date",
            "created_on",
            "updated_on",
            "customer__name",
            "type__title",
            "overall_status__title",
            "overall_status__color",
            "project_manager__first_name",
            "project_manager__last_name",
            "account_manager__first_name",
            "account_manager__last_name",
            "primary_contact__first_name",
            "primary_contact__last_name",
            "last_updated_by__first_name",
            "last_updated_by__last_name",
            "project_manager__profile__profile_pic",
            "description",
            "sow_document",
            "opportunity_crm_ids",
        ]

        # list of related models which needs to be joined in the query
        if select_related_fields:
            select_related_fields = [
                "customer",
                "type",
                "project_manager",
                "project_manager__profile",
                "overall_status",
                "last_updated_by",
                "account_manager",
                "primary_contact",
                "sow_document",
            ]
        else:
            select_related_fields = []
        if status_filter == "closed":
            overall_status_filtered_projects = Project.objects.closed()
        elif status_filter == "all":
            overall_status_filtered_projects = Project.objects
        else:
            overall_status_filtered_projects = Project.objects.open()

        action_status_color_expr = Case(
            When(overdue_action_items__gt=Value(0), then=Value(RED_COLOR)),
            When(
                overdue_critical_path_items__gt=Value(0), then=Value(RED_COLOR)
            ),
            When(
                expiring_critical_path_items__gt=Value(0),
                then=Value(YELLOW_COLOR),
            ),
            When(expiring_action_items__gt=Value(0), then=Value(YELLOW_COLOR)),
            default=Value(GREEN_COLOR),
            output_field=CharField(),
        )
        queryset = overall_status_filtered_projects.select_related(
            *select_related_fields
        ).annotate(
            time_status_value=time_status_value_expression,
            time_status_color=time_status_color,
            is_time_status_critical=is_time_status_critical_expression,
            date_status_value_diff=date_status_expr,
            date_status_color=date_status_color_expr,
            progress_percent=progress_calculation_expression,
            overdue_action_items=overdue_action_items_expression,
            overdue_critical_path_items=overdue_critical_path_items_expression,
            last_action_date=last_action_date_expression,
            expiring_action_items=expiring_action_items,
            expiring_critical_path_items=expiring_critical_path_items,
            action_status_color=action_status_color_expr,
        )
        if filter_conditions:
            queryset = queryset.filter(
                provider=self.provider,
            ).filter(filter_conditions)
        else:
            queryset = queryset.filter(
                provider=self.provider, **filter_conditions
            )
        queryset = queryset.only(*selected_fields).order_by("-updated_on")
        return queryset

    def get_project_active_phase_queryset(
        self, project_id: int
    ) -> "QuerySet[ProjectActivePhase]":
        """
        Return query to fetch list of project active phases.

        """
        queryset = (
            ProjectActivePhase.objects.select_related("phase", "status")
            .filter(provider=self.provider, project_id=project_id)
            .only(
                "phase__title",
                "phase__order",
                "status__title",
                "status__color",
                "estimated_completion_date",
                "updated_on",
            )
            .order_by("phase__order")
        )
        return queryset

    def get_project_activity_statuses(self, project_id: int):
        """
        Returns queryset to fetch the project activity statuses. Project activity statuses contains
        all the statuses in the activity status setting and the new statuses added specific to the project.

        """

        queryset = ActivityStatusSetting.objects.filter(
            Q(provider=self.provider)
            & (Q(project__isnull=True) | Q(project_id=project_id))
        )
        return queryset

    def get_project_dashboard_query(
        self, status_filter, filter_conditions=None
    ):
        """
        Returns Dashboard API queryset. List of projects along with phases.

        """

        filter_conditions = filter_conditions or {}

        progress_calculation_expression = (
            self.get_project_progress_expression()
        )

        # select only the required fields helps to reduce query time

        selected_fields = [
            "id",
            "title",
            "crm_id",
            "updated_on",
            "customer__name",
            "type__title",
            "overall_status__title",
            "overall_status__color",
            "project_manager__first_name",
            "project_manager__last_name",
            "project_manager__profile__profile_pic",
            "project_updates",
        ]

        # list of related models which needs to be joined in the query

        select_related_fields = [
            "customer",
            "type",
            "project_manager",
            "project_manager__profile",
            "overall_status",
        ]

        if status_filter == "all":
            overall_status_filtered_projects = Project.objects.open()
        else:
            overall_status_filtered_projects = Project.objects.closed()

        prefetch_query = Prefetch(
            "phases",
            queryset=ProjectActivePhase.objects.select_related(
                "phase", "status"
            )
            .filter(provider=self.provider)
            .only(
                "project_id",
                "phase__title",
                "phase__order",
                "status__title",
                "status__color",
                "estimated_completion_date",
                "updated_on",
            )
            .order_by("phase__order"),
        )
        queryset = (
            overall_status_filtered_projects.select_related(
                *select_related_fields
            )
            .prefetch_related(prefetch_query)
            .filter(provider=self.provider, **filter_conditions)
            .annotate(progress_percent=progress_calculation_expression)
            .only(*selected_fields)
            .order_by("type__title")
        )
        return queryset

    def get_meeting_list_query(
        self, project_id: int, filter_conditions: Dict = None
    ) -> "QuerySet[Meeting]":
        """
        Gives a query to list all meetings for a project. This queries on the Meeting model and
        gives fields common to all meetings types. Doesn't returns the type specific fields.
        Args:
            project_id: Project Id
            filter_conditions: Additional filter conditions for the query.
        """
        archived_meeting = MeetingArchive.objects.filter(
            provider=OuterRef("provider"),
            project_id=OuterRef("project_id"),
            meeting_id=OuterRef("id"),
        )
        filter_conditions = filter_conditions or {}
        selected_fields = [
            "name",
            "schedule_start_datetime",
            "schedule_end_datetime",
            "conducted_on",
            "provider_id",
            "project_id",
            "meeting_type",
            "status",
            "updated_on",
        ]
        queryset = (
            Meeting.objects.filter(
                provider=self.provider,
                project_id=project_id,
                **filter_conditions,
            )
            .annotate(is_archived=Exists(archived_meeting))
            .only(*selected_fields)
            .order_by("schedule_start_datetime")
        )
        return queryset

    def create_meeting(self, project_id: int, payload: Dict) -> Meeting:
        """
        Creates a Meeting model instance for a project.
        Args:
            project_id: Project Id
            payload: Meeting data

        """
        meeting = Meeting.objects.create(
            provider=self.provider, project_id=project_id, **payload
        )
        return meeting

    @staticmethod
    def update_meeting(meeting: Meeting, payload: Dict) -> None:
        """
        Updates a Meeting instance.
        Args:
            meeting: Meeting Instance to be updated
            payload: Updated data for the meeting

        """
        for attr, value in payload.items():
            setattr(meeting, attr, value)
        meeting.save()

    def create_status_meeting(
        self, project_id: int, payload: Dict
    ) -> StatusMeeting:
        """
        Creates a new Status Meeting. First creates a Meeting instance using the Meeting model fields and
        then StatusMeeting instance using the type specific fields. The payload should contain fields of both
        Meeting and StatusMeeting model.
        Args:
            project_id: Project Id
            payload: Payload for StatusMeeting + Meeting

        """
        payload.update(dict(meeting_type=Meeting.STATUS))
        agenda_topics = payload.pop("agenda_topics", [])
        meeting = self.create_meeting(project_id, payload)
        status_meeting = StatusMeeting.objects.create(
            meeting=meeting, agenda_topics=agenda_topics
        )
        return status_meeting

    def update_status_meeting(
        self, status_meeting: StatusMeeting, payload: Dict
    ) -> None:
        """
        Updates a StatusMeeting instance. Updates data for the related Meeting instance also.
        Args:
            status_meeting: StatusMeeting instance to be updated.
            payload: Updated payload.
        """
        agenda_topics = payload.pop("agenda_topics", [])
        self.update_meeting(status_meeting.meeting, payload)
        status_meeting.agenda_topics = agenda_topics
        status_meeting.save()

    def create_kickoff_meeting(
        self, project_id: int, payload: Dict
    ) -> KickoffMeeting:
        """
        Creates a new Kickoff Meeting. First creates a Meeting instance using the Meeting model fields and
        then Kickoff Meeting instance using the type specific fields. The payload should contain fields of
        both Meeting and kickoffMeeting model.
        Args:
            project_id: Project Id
            payload: Payload for KickoffMeeting + Meeting

        """
        is_internal = payload.pop("is_internal", False)
        meeting_type = (
            Meeting.INTERNAL_KICKOFF if is_internal else Meeting.KICKOFF
        )
        payload.update(dict(meeting_type=meeting_type))
        agenda_topics = payload.pop("agenda_topics", [])
        meeting = self.create_meeting(project_id, payload)
        kickoff_meeting = KickoffMeeting.objects.create(
            meeting=meeting,
            agenda_topics=agenda_topics,
            is_internal=is_internal,
        )
        return kickoff_meeting

    def update_kickoff_meeting(
        self, kickoff_meeting: KickoffMeeting, payload: Dict
    ) -> None:
        """
        Updates a KickoffMeeting instance. Updates data for the related Meeting instance also.
        Args:
            kickoff_meeting: KickoffMeeting instance to be updated.
            payload: Updated payload.
        """
        agenda_topics = payload.pop("agenda_topics", [])
        self.update_meeting(kickoff_meeting.meeting, payload)
        kickoff_meeting.agenda_topics = agenda_topics
        kickoff_meeting.save()

    def create_customer_touch_meeting(
        self, project_id: int, payload: Dict
    ) -> CustomerTouchMeeting:
        """
        Creates a new customerTouch Meeting. First creates a Meeting instance using the Meeting model fields and
        then customerTouch Meeting instance using the type specific fields. The payload should contain fields of
        both Meeting and customer_touchMeeting model.
        Args:
            project_id: Project Id
            payload: Payload for customerTouchMeeting + Meeting

        """
        email_subject = payload.pop("email_subject")
        email_body = payload.pop("email_body")
        email_body_text = payload.pop("email_body_text")
        payload.pop("agenda_topics", [])
        meeting_type = Meeting.CUSTOMER_TOUCH
        payload.update(dict(meeting_type=meeting_type))
        meeting = self.create_meeting(project_id, payload)
        customer_touch_meeting = CustomerTouchMeeting.objects.create(
            meeting=meeting,
            email_subject=email_subject,
            email_body=email_body,
            email_body_text=email_body_text,
        )
        return customer_touch_meeting

    def update_customer_touch_meeting(
        self, customer_touch_meeting: CustomerTouchMeeting, payload: Dict
    ) -> None:
        """
        Updates a customerTouchMeeting instance. Updates data for the related Meeting instance also.
        Args:
            customer_touch_meeting: customerTouchMeeting instance to be updated.
            payload: Updated payload.
        """
        email_subject = payload.pop("email_subject")
        email_body = payload.pop("email_body")
        email_body_text = payload.pop("email_body_text")
        self.update_meeting(customer_touch_meeting.meeting, payload)
        customer_touch_meeting.email_subject = email_subject
        customer_touch_meeting.email_body = email_body
        customer_touch_meeting.email_body_text = email_body_text
        customer_touch_meeting.save()

    def create_close_out_meeting(
        self, project_id: int, payload: Dict
    ) -> CloseOutMeeting:
        """
        Creates a new Close Out Meeting. First creates a Meeting instance using the Meeting model fields and
        then close out Meeting instance using the type specific fields. The payload should contain fields of
        both Meeting and Close out meeting model.
        Args:
            project_id: Project Id
            payload: Payload for customerTouchMeeting + Meeting

        """
        project_acceptance_markdown = payload.pop(
            "project_acceptance_markdown"
        )
        project_acceptance_text = payload.pop("project_acceptance_text")
        email_subject = payload.pop("email_subject")
        payload.pop("agenda_topics", [])
        meeting_type = Meeting.CLOSE_OUT
        payload.update(dict(meeting_type=meeting_type))
        meeting = self.create_meeting(project_id, payload)
        close_out_meeting = CloseOutMeeting.objects.create(
            meeting=meeting,
            project_acceptance_markdown=project_acceptance_markdown,
            project_acceptance_text=project_acceptance_text,
            email_subject=email_subject,
        )
        return close_out_meeting

    def update_close_out_meeting(
        self, close_out_meeting: CloseOutMeeting, payload: Dict
    ) -> None:
        """
        Updates a CloseOutMeeting instance. Updates data for the related Meeting instance also.
        Args:
            close_out_meeting: CloseOutMeeting instance to be updated.
            payload: Updated payload.
        """
        project_acceptance_markdown = payload.pop(
            "project_acceptance_markdown"
        )
        project_acceptance_text = payload.pop("project_acceptance_text")
        email_subject = payload.pop("email_subject")
        self.update_meeting(close_out_meeting.meeting, payload)
        close_out_meeting.project_acceptance_markdown = (
            project_acceptance_markdown
        )
        close_out_meeting.project_acceptance_text = project_acceptance_text
        close_out_meeting.email_subject = email_subject
        close_out_meeting.save()

    @staticmethod
    def create_default_project_phases(project_type: ProjectType) -> None:
        """
        Add default phases for the project Type
        Args:
            project_type: Project Type object

        Returns: None

        """
        default_phases = ProjectPhase.DEFAULT_PROJECT_PHASES
        for order, phase in enumerate(default_phases):
            ProjectPhase.objects.create(
                title=phase, project_type=project_type, order=order + 1
            )

    def get_engineering_role_ids(self) -> List[int]:
        """
        Get the list of role_ids configured in EngineerRoleMapping

        """
        engineering_role_setting = EngineerRoleMapping.objects.get(
            provider=self.provider
        )
        return engineering_role_setting.role_ids

    def get_pre_sales_engineering_role_ids(self):
        """
        Get the list of role_ids configured in PreSalesEngineerRoleMapping

        """
        try:
            pre_sales_engineering_role_setting = (
                PreSalesEngineerRoleMapping.objects.get(provider=self.provider)
            )
        except PreSalesEngineerRoleMapping.DoesNotExist:
            return None
        return pre_sales_engineering_role_setting.role_ids

    def get_customer_contact_role_role_ids(self) -> List[int]:
        """
        Get the list of role_ids configured in CustomerContactRole

        """
        customer_contact_role_setting = CustomerContactRole.objects.filter(
            provider=self.provider
        )
        return customer_contact_role_setting

    def get_user_profile_info(
        self, fields: Optional[List[str]] = None, **filter_kwargs
    ) -> "QuerySet[User]":
        """
        Get User Profile details.

        Args:
            filter_kwargs: Query conditions
            fields: Set of fields to be selected from DB

        Returns:
            User queryset
        """
        DEFAULT_FIELDS: List[str] = [
            "first_name",
            "last_name",
            "profile__profile_pic",
        ]
        fields: List[str] = fields or DEFAULT_FIELDS
        users: "QuerySet[User]" = (
            User.default_manager.select_related("profile")
            .filter(provider=self.provider, **filter_kwargs)
            .only(*fields)
        ).order_by()
        return users

    def get_project_ticket_complete_status(self):

        ticket_complete_status = AdditionalSetting.objects.values_list(
            "service_ticket_complete_status", flat=True
        ).get(provider=self.provider)
        return ticket_complete_status

    def create_milestone_done_status(self) -> MilestoneStatusSetting:
        """
        Create the DONE Milestone Status in the database.
        """
        done_status = MilestoneStatusSetting.objects.create(
            provider=self.provider,
            title=MilestoneStatusSetting.DONE,
            color=settings.MILESTONE_DONE_STATUS_COLOR,
        )
        return done_status

    def create_activity_status_done_status(self) -> ActivityStatusSetting:
        """
        Create the DONE Activity Status in the database.
        """
        done_status = ActivityStatusSetting.objects.create(
            provider=self.provider,
            title=ActivityStatusSetting.DONE,
            color=settings.ACTIVITY_STATUS_DONE_STATUS_COLOR,
        )
        return done_status

    def get_milestone_done_status(self) -> MilestoneStatusSetting:
        """
        Returns the MilestoneStatus Model object where the title is MilestoneStatusSetting.DONE
        By default this status should be present. If this status is not present, it is first
        created and then returned.

        """
        done_status = MilestoneStatusSetting.objects.filter(
            provider=self.provider, title=MilestoneStatusSetting.DONE
        ).first()
        if not done_status:
            done_status = self.create_milestone_done_status()
        return done_status

    def get_activity_status_done_status(self) -> ActivityStatusSetting:
        """
        Returns the ActivityStatusSetting Model object where the title is ActivityStatusSetting.DONE
        By default this status should be present. If this status is not present, it is first
        created and then returned.

        """
        done_status = ActivityStatusSetting.objects.filter(
            provider=self.provider, title=ActivityStatusSetting.DONE
        ).first()
        if not done_status:
            done_status = self.create_activity_status_done_status()
        return done_status

    def get_project_phase(
        self, project_id: int, phase_title: str
    ) -> ProjectActivePhase:
        """
        Returns the projects phase object.
        """
        project_phase = ProjectActivePhase.objects.filter(
            provider=self.provider,
            project_id=project_id,
            phase__title=phase_title,
        ).first()
        return project_phase

    def mark_project_phase_complete(self, phase: ProjectActivePhase):
        """
        Set a project active phase status to MileStoneStatus Done status.

        """
        try:
            phase.status = self.get_milestone_done_status()
            phase.estimated_completion_date = timezone.now()
            phase.save()
            return True
        except Exception as e:
            return False

    def mark_customer_touch_phase_complete(self, project_id: int) -> None:
        """
        Method to mark the customer touch phase of a project Done.

        """
        customer_touch_phase = self.get_project_phase(
            project_id, ProjectPhase.CUSTOMER_TOUCH
        )
        if not customer_touch_phase:
            raise ValueError("Customer Touch Phase not found for the project.")
        self.mark_project_phase_complete(customer_touch_phase)

    def mark_kickoff_phase_complete(self, project_id: int) -> None:
        """
        Method to mark the kickoff phase of a project Done.

        """
        kickoff_phase = self.get_project_phase(
            project_id, ProjectPhase.KICKOFF
        )
        if not kickoff_phase:
            return
        self.mark_project_phase_complete(kickoff_phase)

    @staticmethod
    def mark_meeting_done(meeting: Meeting):
        """
        Marks a CustomerTouch meeting Done. Updates the status to Done,
        and stores the current time as the meeting conducted_on time.

        """
        try:
            meeting.status = Meeting.DONE
            meeting.conducted_on = timezone.now()
            meeting.save()
            return True
        except Exception as e:
            return False

    def get_project_critical_path_items(
        self, project_id
    ) -> "QuerySet[CriticalPathItem]":
        queryset = (
            CriticalPathItem.objects.select_related(
                "owner", "owner__profile", "status", "resource"
            )
            .filter(provider=self.provider, project_id=project_id)
            .only(
                "description",
                "due_date",
                "status__title",
                "status__color",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "notes",
                "resource__name",
                "resource__email",
                "resource__phone",
                "resource__role",
            )
            .order_by("due_date")
        )
        return queryset

    def get_project_action_items(self, project_id) -> "QuerySet[ActionItem]":
        queryset = (
            ActionItem.objects.select_related(
                "owner", "owner__profile", "status", "resource"
            )
            .filter(provider=self.provider, project_id=project_id)
            .only(
                "description",
                "due_date",
                "status__title",
                "status__color",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "notes",
                "resource__name",
                "resource__email",
                "resource__phone",
                "resource__role",
            )
            .order_by("due_date")
        )
        return queryset

    def get_risk_items(self, project_id: int) -> "QuerySet[RiskItem]":
        queryset = (
            RiskItem.objects.select_related(
                "owner", "owner__profile", "resource"
            )
            .filter(provider=self.provider, project_id=project_id)
            .only(
                "description",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "impact",
                "notes",
                "resource__name",
                "resource__email",
                "resource__phone",
                "resource__role",
            )
            .order_by("-impact")
        )
        return queryset

    def get_meeting_notes(
        self, meeting_id, project_id
    ) -> "QuerySet[ProjectNote]":
        queryset = (
            ProjectNote.objects.meeting_notes()
            .select_related("author", "author__profile")
            .filter(
                provider=self.provider,
                project_id=project_id,
                meeting_id=meeting_id,
            )
            .only(
                "note",
                "updated_on",
                "author__first_name",
                "author__last_name",
                "author__profile__profile_pic",
            )
            .order_by("-updated_on")
        )
        return queryset

    @staticmethod
    def get_typed_meeting_for_meeting(meeting: Meeting):
        """
        Returns the Typed meeting related to the meeting.

        """

        typed_meeting = None
        try:
            if meeting.meeting_type == Meeting.CUSTOMER_TOUCH:
                typed_meeting = CustomerTouchMeeting.objects.get(
                    meeting_id=meeting.id
                )
            elif meeting.meeting_type == Meeting.STATUS:
                typed_meeting = StatusMeeting.objects.get(
                    meeting_id=meeting.id
                )
            elif meeting.meeting_type == Meeting.CLOSE_OUT:
                typed_meeting = CloseOutMeeting.objects.get(
                    meeting_id=meeting.id
                )
            elif meeting.meeting_type in [
                Meeting.KICKOFF,
                Meeting.INTERNAL_KICKOFF,
            ]:
                typed_meeting = KickoffMeeting.objects.get(
                    meeting_id=meeting.id
                )
        except (
            CustomerTouchMeeting.DoesNotExist,
            StatusMeeting.DoesNotExist,
            KickoffMeeting.DoesNotExist,
            CloseOutMeeting.DoesNotExist,
        ):
            return None
        return typed_meeting

    def check_project_settings(self):
        message = "Provider required PMO settings not configured."
        configured = True
        try:
            additional_settings: AdditionalSetting = (
                self.provider.additional_project_settings
            )
        except ObjectDoesNotExist as e:
            logger.info(e)
            configured = False
            return configured, message
        try:
            additional_settings.default_project_manager
        except AdditionalSetting.DoesNotExist:
            configured = False
            message = "Default Project Manager not set."
        try:
            additional_settings.default_work_type
        except AdditionalSetting.DoesNotExist:
            configured = False
            message = "Default Work Type not set."
        try:
            additional_settings.default_work_role
        except AdditionalSetting.DoesNotExist:
            configured = False
            message = "Default Work Role not set."
        try:
            additional_settings.default_business_unit
        except AdditionalSetting.DoesNotExist:
            configured = False
            message = "Default Business Unit not set."
        time_status_settings = self.get_time_status_setting_queryset()
        if not time_status_settings.count() > 0:
            configured = False
            message = "Time status setting not configured"
        date_status_settings = self.get_date_status_setting()
        if not date_status_settings.count() > 1:
            configured = False
            message = "Date Status setting not configured."
        boards = self.get_board_with_open_statuses()
        if not boards:
            configured = False
            message = "Board and status mapping not configured."
        return configured, message

    @staticmethod
    def get_meeting_attendees(meeting: Meeting) -> "QuerySet[MeetingAttendee]":
        attendees = (
            MeetingAttendee.objects.select_related(
                "attendee", "attendee__profile"
            )
            .filter(meeting_id=meeting.id)
            .only(
                "attendee__id",
                "attendee__first_name",
                "attendee__last_name",
                "attendee__profile__profile_pic",
            )
        )
        return attendees

    @staticmethod
    def get_additional_meeting_attendees(
        project_id: int,
    ) -> "QuerySet[AdditionalContacts]":
        """
        Get additional meeting attendees.

        Args:
            project_id: Project ID

        Returns:
            Additional meeting attendees
        """
        project: Project = Project.objects.get(id=project_id)
        provider_id: int = project.provider.id
        additional_attendees: "QuerySet[AdditionalContacts]" = (
            AdditionalContacts.objects.filter(
                provider_id=provider_id, project_id=project_id
            )
        )
        return additional_attendees

    @staticmethod
    def get_additional_attendees_present_for_meeting(
        meeting: Meeting, project: Project
    ) -> "QuerySet[ExternalAttendee]":
        additional_attendees_present: "QuerySet[ExternalAttendee]" = (
            ExternalAttendee.objects.filter(
                provider_id=project.provider.id,
                meeting_id=meeting.id,
                project_id=project.id,
            )
        )
        return additional_attendees_present

    @staticmethod
    def _get_completed_period_expression():
        """
        Gives back the expression to calculate number of days since the task was completed.

        """

        current_date = timezone.now().date()
        completed_period_in_days = ExpressionWrapper(
            current_date - F("completed_on_date"), output_field=DurationField()
        )
        return completed_period_in_days

    @staticmethod
    def _get_completed_on_date_expression():
        expression = Case(
            When(completed_on__isnull=False, then=F("completed_on")),
            default=F("updated_on"),
        )
        return expression

    def get_critical_path_items_for_archive(
        self, aging_period: int, project_id: int = None
    ) -> "QuerySet[CriticalPathItem]":
        """
        Gives the queryset of CriticalPathItems which have been marked completed
        and have also completed the aging period(defined in the settings).
        So If the aging period is 5, this will filter the CriticalPath Items which have the
        status completed and were completed 5 days ago or before that.
        Args:
            aging_period: Number of days
            project_id: Project unique identifier

        """
        completed_on_date_expression = self._get_completed_on_date_expression()
        completed_period_in_days = self._get_completed_period_expression()
        completed_status = self.get_action_status_done_status()
        query_params = dict(provider=self.provider, status=completed_status)
        if project_id:
            query_params.update(dict(project_id=project_id))

        queryset = (
            CriticalPathItem.objects.select_related(
                "owner", "owner__profile", "status"
            )
            .filter(**query_params)
            .only(
                "description",
                "due_date",
                "completed_on",
                "status__title",
                "status__color",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "updated_on",
            )
            .annotate(
                completed_on_date=completed_on_date_expression,
                completed_period_in_days=completed_period_in_days,
            )
            .filter(
                completed_period_in_days__gte=datetime.timedelta(aging_period)
            )
        )
        return queryset

    def get_action_items_for_archive(
        self, aging_period: int, project_id: int = None
    ) -> "QuerySet[ActionItem]":
        """
        Gives the queryset of ActionItems which have been marked completed
        and have also completed the aging period(defined in the settings).
        So If the aging period is 5, this will filter the Action Items which have the
        status completed and were completed 5 days ago or before that.
        Args:
            aging_period: Number of days
            project_id: Project unique identifier

        """
        completed_on_date_expression = self._get_completed_on_date_expression()
        completed_period_in_days = self._get_completed_period_expression()
        completed_status = self.get_action_status_done_status()
        query_params = dict(provider=self.provider, status=completed_status)
        if project_id:
            query_params.update(dict(project_id=project_id))

        queryset = (
            ActionItem.objects.select_related(
                "owner", "owner__profile", "status"
            )
            .filter(**query_params)
            .only(
                "description",
                "due_date",
                "completed_on",
                "status__title",
                "status__color",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "updated_on",
            )
            .annotate(
                completed_on_date=completed_on_date_expression,
                completed_period_in_days=completed_period_in_days,
            )
            .filter(
                completed_period_in_days__gte=datetime.timedelta(aging_period)
            )
        )
        return queryset

    def get_risk_items_for_archive(
        self, aging_period: int, project_id: int = None
    ) -> "QuerySet[RiskItem]":
        """
        Gives the queryset of RiskItems which have been marked completed
        and have also completed the aging period(defined in the settings).
        So If the aging period is 5, this will filter the Risk Items which have the
        status completed and were completed 5 days ago or before that.
        Args:
            aging_period: Number of days
            project_id: Project unique identifier

        """
        completed_on_date_expression = self._get_completed_on_date_expression()
        completed_period_in_days = self._get_completed_period_expression()
        completed_status = RiskItem.MITIGATED.numeric_value
        query_params = dict(provider=self.provider, impact=completed_status)
        if project_id:
            query_params.update(dict(project_id=project_id))

        queryset = (
            RiskItem.objects.select_related("owner", "owner__profile")
            .filter(**query_params)
            .only(
                "description",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "impact",
                "updated_on",
                "completed_on",
            )
            .annotate(
                completed_on_date=completed_on_date_expression,
                completed_period_in_days=completed_period_in_days,
            )
            .filter(
                completed_period_in_days__gte=datetime.timedelta(aging_period)
            )
        )
        return queryset

    def get_meetings_for_archive(
        self, aging_period: int, project_id: int = None
    ) -> "QuerySet[MeetingArchive]":
        current_date = timezone.now().date()
        completed_period_in_days = ExpressionWrapper(
            current_date - F("meeting__updated_on"),
            output_field=DurationField(),
        )
        query_params = dict(provider=self.provider)
        if project_id:
            query_params.update(dict(project_id=project_id))
        queryset = (
            MeetingArchive.objects.filter(**query_params)
            .annotate(completed_period_in_days=completed_period_in_days)
            .filter(
                completed_period_in_days__gte=datetime.timedelta(aging_period)
            )
        )
        return queryset


class ProjectService:
    def __init__(self, provider):
        self.provider = provider
        self.query_service = ProjectQueryService(provider)
        self.erp_service = ProjectERPService(provider)
        self.event_log = []

    def get_provider_projects_from_erp(
        self, fields: List[str] = None
    ) -> Generator[Response, None, None]:
        """
        Fetch all projects from ERP service for the provider.
        The projects will be filtered for the boards configured
        and status mapped in the Board Mapping and Overall Status Mapping
        settings.

        Returns: A generator to all the pages of the result. Each page contains multiple
        results. Following code can be used to get all the results:
        for response in pages:
            page = response.json()
        """
        board_mapping: Dict[
            int, List[int]
        ] = self.query_service.get_board_with_open_statuses()
        status_ids = [
            status_id
            for status_list in board_mapping.values()
            for status_id in status_list
        ]
        return self.erp_service.get_all_projects(
            board_ids=list(board_mapping.keys()),
            status_ids=status_ids,
            fields=fields,
        )

    def run_project_sync_task(self):
        """
        Calls the project sync task and project engineer sync task.

        """
        from project_management.tasks.project_tasks import (
            sync_projects,
            sync_projects_engineers,
        )

        tasks = [
            sync_projects.si(
                [self.provider.id],
            ),
            sync_projects_engineers.si(
                [self.provider.id],
            ),
        ]
        task = chain(*tasks).apply_async()
        # task = sync_projects.apply_async(([self.provider.id],), queue="high")
        return task

    def get_project_engineers(
        self, project_crm_id: int, pre_sales_engineer=False
    ) -> "QuerySet[User]":
        """
        Get project engineers or pre sales engineers
        Args:
            project_crm_id: Project CRM Id

        """
        PRE_SALES_ENGINEER = "Pre Sales Engineer"
        ENGINEER = "Engineer"
        if pre_sales_engineer:
            engineering_role_ids = (
                self.query_service.get_pre_sales_engineering_role_ids()
            )
            project_role = PRE_SALES_ENGINEER
        else:
            engineering_role_ids = (
                self.query_service.get_engineering_role_ids()
            )
            project_role = ENGINEER
        project_team_members = self.erp_service.get_project_team_members(
            project_crm_id
        )
        if engineering_role_ids:
            engineers = filter(
                lambda member: member.get("projectRole", {}).get("id")
                in engineering_role_ids,
                project_team_members,
            )
            member_ids = [
                user.get("member", {}).get("id") for user in engineers
            ]
            queryset = self.query_service.get_provider_user_by_member_id(
                member_ids
            )
            queryset = queryset.annotate(
                project_role=Value(project_role, output_field=CharField())
            )
            return queryset

    async def get_project_engineers_async(self, project_crm_ids):
        """
        Async method to fetch the project engineers from project ids.
        Args:
            project_crm_ids: List of project ids

        Returns: Dictionary containing Engineers attached with the project
        """
        with ThreadPoolExecutor(max_workers=10) as executor:
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    partial(
                        self.erp_service.get_project_team_members,
                        project_crm_id,
                    ),
                )
                for project_crm_id in project_crm_ids
            ]
            project_engineers = await asyncio.gather(*tasks)
        return project_engineers

    def fetch_project_engineer(self, project_crm_ids):
        """
        Method to fetch project engineers from project ids.
        Args:
            project_crm_ids: List of project ids

        Returns:
            Dictionary containing Engineers attached with the project
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            self.get_project_engineers_async(project_crm_ids)
        )
        project_engineers = loop.run_until_complete(future)
        return project_engineers

    def get_project_customer_contacts(
        self, project_crm_id: int, project_id: int
    ) -> List[Dict]:
        """
        Get project customer contacts.

        Args:
            project_id: Project ID
            project_crm_id: Project CRM Id

        Returns:
            Project customer contacts.
        """
        project_customer_contacts: List[
            Dict
        ] = self.erp_service.get_project_customer_contacts(project_crm_id)
        # Map project contact record ID to contact CRM ID
        project_contact_id_to_contact_crm_id_mapping: Dict[int, int] = {
            record["id"]: record.get("contact", {}).get("id")
            for record in project_customer_contacts
        }

        whens: List = [
            When(crm_id=v, then=k)
            for k, v in project_contact_id_to_contact_crm_id_mapping.items()
        ]
        fields: List[str] = [
            "email",
            "first_name",
            "last_name",
            "profile__profile_pic",
        ]
        role_subquery = Subquery(
            ProjectContactRole.objects.filter(
                contact_crm_id=OuterRef("crm_id"), project_id=Value(project_id)
            ).values("role__role")[:1]
        )
        users_in_db: "QuerySet[User]" = (
            self.query_service.get_user_profile_info(
                fields,
                crm_id__in=list(
                    project_contact_id_to_contact_crm_id_mapping.values()
                ),
            ).annotate(
                project_role=role_subquery,
                contact_record_id=Case(*whens, output_field=IntegerField()),
            )
        )

        project_contacts: List[Dict] = list()
        crm_ids_of_users_found_in_db: List[int] = list()
        for user in users_in_db:
            contact_data: Dict = {
                "email": user.email,
                "user_id": user.id,
                "contact_crm_id": user.crm_id,
                "name": user.name,
                "profile_url": user.profile.profile_pic,
                "contact_record_id": user.contact_record_id,
                "project_role": user.project_role,
                "office_phone": user.profile.full_office_phone_number,
            }
            project_contacts.append(contact_data)
            crm_ids_of_users_found_in_db.append(user.crm_id)

        # This will run for the contacts which were not found in acela DB using CRM id
        # This is the case when the customer contact exists in CW but is not yet synced in DB
        if len(crm_ids_of_users_found_in_db) < len(project_customer_contacts):
            roles: "QuerySet[ProjectContactRole]" = (
                ProjectContactRole.objects.filter(
                    project_id=project_id
                ).exclude(contact_crm_id__in=crm_ids_of_users_found_in_db)
            )
            roles_mapping: Dict[int, str] = {
                role.contact_crm_id: role.role.role for role in roles
            }
            customer_contacts_not_in_db: List[Dict] = [
                user
                for user in project_customer_contacts
                if user.get("contact", {}).get("id")
                not in set(crm_ids_of_users_found_in_db)
            ]
            if customer_contacts_not_in_db:
                contact_crm_ids_not_in_db: List[int] = [
                    user.get("contact", {}).get("id")
                    for user in customer_contacts_not_in_db
                ]
                # Fetch contact details from CW for those users not in DB
                customer_contacts: List[
                    Dict
                ] = self.provider.erp_client.get_customer_contacts_details(
                    contact_crm_ids_not_in_db
                )
                contact_crm_id_to_communication_details_mapping: Dict[
                    int, Dict
                ] = self.get_contact_crm_id_to_communication_details_mapping(
                    customer_contacts
                )
                for user in customer_contacts_not_in_db:
                    contact_crm_id: int = user.get("contact", {}).get("id")
                    contact_role: str = roles_mapping.get(contact_crm_id)
                    communication_details: Dict = (
                        contact_crm_id_to_communication_details_mapping.get(
                            contact_crm_id
                        )
                    )
                    contact_data: Dict = {
                        "contact_crm_id": contact_crm_id,
                        "project_role": contact_role,
                        "name": user.get("contact", {}).get("name"),
                        "contact_record_id": user.get("id"),
                        "office_phone": communication_details.get(
                            "phone_number"
                        ),
                        "email": communication_details.get("email"),
                    }
                    project_contacts.append(contact_data)
        return project_contacts

    @staticmethod
    def get_contact_crm_id_to_communication_details_mapping(
        customer_contacts: List[Dict],
    ) -> Dict[int, Dict]:
        """
        Get mapping of customer contact CRM ID to communication details.

        Args:
            customer_contacts: List of customer contacts

        Returns:
            Customer contact CRM ID to communication item details.
        """
        contact_crm_id_to_communication_details_mapping: Dict[int, Dict] = {
            contact.get("id"): dict(
                phone_number=contact.get("default_phone_number", ""),
                email=contact.get("email", ""),
            )
            for contact in customer_contacts
        }
        return contact_crm_id_to_communication_details_mapping

    def get_project_account_manager(self, project_id):
        """
        Fetches the account manager / territory manager for the project company.
        Searches the user in DB using the system member id mapping.

        """
        customer_crm_id = (
            Project.objects.only("customer__crm_id")
            .select_related("customer")
            .get(id=project_id)
        ).customer.crm_id
        account_manager_member_id = self.erp_service.get_territory_manager(
            customer_crm_id
        )
        account_manager = self.query_service.get_provider_user_by_member_id(
            [account_manager_member_id]
        )
        return account_manager.first()

    def get_project_team_members(
        self, project_id: int, skip_primary_contact=False
    ):
        project_members = Project.objects.only(
            "project_manager_id", "primary_contact_id", "crm_id"
        ).get(provider=self.provider, id=project_id)
        fields = [
            "email",
            "first_name",
            "last_name",
            "profile__profile_pic",
            "profile__system_member_crm_id",
            "profile__office_phone",
        ]
        account_manager = self.get_project_account_manager(project_id)
        member_ids = [
            project_members.project_manager_id,
        ]
        if not skip_primary_contact and project_members.primary_contact_id:
            member_ids.append(project_members.primary_contact_id)
        if account_manager:
            member_ids.append(account_manager.id)
        members = self.query_service.get_user_profile_info(
            fields, id__in=member_ids
        )

        # Add member roles

        cases = []

        if project_members.project_manager_id:
            cases.append(
                When(
                    id=Value(project_members.project_manager_id),
                    then=Value("Project Manager"),
                )
            )
        if not skip_primary_contact and project_members.primary_contact_id:
            cases.append(
                When(
                    id=Value(project_members.primary_contact_id),
                    then=Value("Primary Contact"),
                )
            )
        if account_manager:
            cases.append(
                When(
                    id=Value(account_manager.id),
                    then=Value("Account Manager"),
                )
            )
        if cases:
            member_roles = Case(
                *cases,
                output_field=CharField(),
            )
            members = members.annotate(project_role=member_roles)
        else:
            members = members.annotate(
                project_role=Value("", output_field=CharField())
            )
        engineers = self.get_project_engineers(project_members.crm_id)
        pre_sales_engineers = self.get_project_engineers(
            project_members.crm_id, True
        )
        if engineers:
            members = members.union(engineers)
        if pre_sales_engineers:
            members = members.union(pre_sales_engineers)
        return members

    @staticmethod
    def serialize_critical_path_item_object(
        critical_path_items: Union[CriticalPathItem, List[CriticalPathItem]],
        many=True,
    ) -> Union[Dict, List[Dict]]:
        """
        Gives back serialized critical path items
        """
        serializer = CriticalPathItemSerializer(critical_path_items, many=many)
        return serializer.data

    @staticmethod
    def serialize_action_item_object(
        action_items: Union[ActionItem, List[ActionItem]], many=True
    ) -> Union[Dict, List[Dict]]:
        """
        Gives back serialized action items
        """
        serializer = ActionItemSerializer(action_items, many=many)
        return serializer.data

    @staticmethod
    def serialize_risk_item_object(
        risk_items: Union[RiskItem, List[RiskItem]], many=True
    ) -> Union[Dict, List[Dict]]:
        """
        Gives back serialized risk items
        """
        serializer = RiskItemSerializer(risk_items, many=many)
        return serializer.data

    def create_project_time_entries(self, time_entry_payload: Dict):
        """
        Creates time entries for project managers and engineers

        """
        pm_time_entry_payload = time_entry_payload.get("pm_time_entry")
        try:
            if pm_time_entry_payload:
                # create pm time entry
                self.create_project_manager_time_entry(**pm_time_entry_payload)
            engineer_time_entries = time_entry_payload.get(
                "engineer_time_entries", []
            )
            for engineer_time_entry in engineer_time_entries:
                self.create_engineer_time_entry(**engineer_time_entry)
            self.event_log.append(
                {"label": "Time Entry Creation", "error": [], "result": True}
            )
        except InvalidRequestError as e:
            logger.debug(f"Error while creating the time entry: {e}")
            self.event_log.append(
                {
                    "label": "Time Entry Creation",
                    "error": [e.detail],
                    "result": False,
                }
            )

    def create_project_manager_time_entry(
        self,
        ticket_id: int,
        member_crm_id: int,
        start_time: datetime.datetime,
        end_time: datetime.datetime,
        customer_crm_id: int,
        notes: Optional[str] = None,
    ):
        """
        Create Project Manager time entry

        """
        additional_settings: AdditionalSetting = (
            self.provider.additional_project_settings
        )
        try:
            work_role = additional_settings.default_work_role
            work_type = additional_settings.default_work_type
            business_unit = additional_settings.default_business_unit
        except AdditionalSetting.DoesNotExist:
            return False

        payload = dict(
            charge_to_id=ticket_id,
            member_crm_id=member_crm_id,
            start_time=start_time,
            end_time=end_time,
            customer_crm_id=customer_crm_id,
            work_role=work_role,
            work_type=work_type,
            business_unit=business_unit,
            notes=notes,
        )
        return self.erp_service.create_ticket_time_entry(payload)
        # return True

    def create_engineer_time_entry(
        self,
        ticket_id: int,
        member_crm_id: int,
        start_time: datetime.datetime,
        end_time: datetime.datetime,
        customer_crm_id: int,
        notes: Optional[str] = None,
    ):
        """
        Creates Engineer time entries.

        """
        additional_settings: AdditionalSetting = (
            self.provider.additional_project_settings
        )
        try:
            work_role = additional_settings.engineer_default_work_role
            work_type = additional_settings.engineer_default_work_type
            business_unit = additional_settings.engineer_default_business_unit
        except AdditionalSetting.DoesNotExist:
            return False

        payload = dict(
            charge_to_id=ticket_id,
            member_crm_id=member_crm_id,
            start_time=start_time,
            end_time=end_time,
            customer_crm_id=customer_crm_id,
            work_role=work_role,
            work_type=work_type,
            business_unit=business_unit,
            notes=notes,
        )
        return self.erp_service.create_ticket_time_entry(payload)
        # return True

    @staticmethod
    def export_raw_data(provider, payload):
        project_ids = payload.get("project_ids")
        start_date = payload.get("start_date", "")
        end_date = payload.get("end_date", "")
        projects = Project.objects.filter(
            provider=provider,
            id__in=project_ids,
        )
        if start_date and end_date:
            date_range = f"{start_date.date().strftime('%m/%d/%Y')} - {end_date.date().strftime('%m/%d/%Y')}"
            projects = projects.filter(
                closed_date__range=[start_date, end_date],
            )
        else:
            date_range = None
        sow_doc_settings = Provider.objects.get(
            id=provider.id
        ).sow_doc_settings
        results = []
        for project in projects:
            sow_document = project.sow_document
            data = OrderedDict()
            if date_range:
                data["Date Range"] = date_range
            if sow_document:
                data["Project Name"] = project.title
                data[
                    "Project Start Date"
                ] = project.created_on.date().strftime("%m/%d/%Y")
                if project.closed_date:
                    data[
                        "Project End Date"
                    ] = project.closed_date.date().strftime("%m/%d/%Y")
                # data[
                #     "Actual Engineering Hours – Standard"
                # ] = project.actual_hours
                data["Customer Name"] = project.customer
                data[
                    "Total Project Revenue Billed"
                ] = f"${project.billing_amount}"
                project_service = ProjectService(provider)
                try:
                    after_hours_settings = (
                        ProjectAfterHoursMapping.objects.get(provider=provider)
                    )
                except ProjectAfterHoursMapping.DoesNotExist:
                    raise ValidationError(
                        "After Hours mapping is not configured."
                    )
                project_engineers = project_service.get_project_engineers(
                    project.crm_id
                )
                eng_actual_hours = 0
                eng_after_hours = 0
                eng_actual_cost = 0
                for project_engineer in project_engineers:
                    system_member_id = (
                        project_engineer.profile.system_member_crm_id
                    )
                    try:
                        engineer_cost = (
                            project_engineer.project_rate_role.hourly_cost
                        )
                    except Exception as e:
                        logger.info("Project Manager cost not found")
                        engineer_cost = 0
                    if system_member_id:
                        result = project_service.get_engineer_hours_and_cost(
                            project.crm_id,
                            system_member_id,
                            after_hours_settings,
                            engineer_cost,
                        )
                        eng_actual_hours += result.get("actual_hours")
                        data[
                            "Actual Engineering Hours – Standard"
                        ] = eng_actual_hours
                        eng_after_hours += result.get("after_hours")
                        data[
                            "Actual Engineering Hours – After Hours"
                        ] = eng_after_hours
                        eng_actual_cost += result.get(
                            "engineer_total_cost", "0"
                        )
                        data[
                            "Actual Engineering Cost"
                        ] = f"${round(eng_actual_cost)}"
                project_manager_member_id = (
                    project.project_manager.profile.system_member_crm_id
                )
                try:
                    pm_cost = (
                        project.project_manager.project_rate_role.hourly_cost
                    )
                except Exception as e:
                    logger.info("Project Manager cost not found")
                    pm_cost = 0
                if project_manager_member_id:
                    timesheet_entries = provider.erp_client.get_time_entries(
                        member_id=project_manager_member_id,
                        project_crm_id=project.crm_id,
                    )
                    pm_actual_hours = 0
                    pm_total_cost = 0
                    for timesheet_entry in timesheet_entries:
                        if timesheet_entry:
                            pm_actual_hours += timesheet_entry.get(
                                "actualHours", 0
                            )
                    if pm_cost:
                        # Calculating the total project manager cost from the total hours of a PM
                        pm_total_cost = pm_actual_hours * float(pm_cost)
                    data["Actual Project Management Hours"] = pm_actual_hours
                    data["Actual PM Cost"] = f"${round(pm_total_cost)}"
                query_params = {"id": sow_document.id}
                documents = DocumentService.list_document(
                    provider=provider,
                    query_params=query_params,
                    show_closed=True,
                )
                data["Opportunity name"] = (
                    documents[0].get("quote", "").get("name", "")
                )
                config_data = documents[0].get("json_config", {})
                service_cost = config_data.get("service_cost", {})
                contractors = service_cost.get("contractors", [])
                contractor_partner_cost = 0
                contractor_customer_cost = 0
                for contractor in contractors:
                    contractor_partner_cost += contractor.get(
                        "partner_cost", 0
                    )
                    contractor_customer_cost += contractor.get(
                        "customer_cost", 0
                    )
                data["Contractor Revenue"] = f"${contractor_customer_cost}"
                data["Contractor Cost"] = f"${contractor_partner_cost}"
                travels = service_cost.get("travels", [])
                travel_cost = 0
                for travel in travels:
                    travel_cost += travel.get("cost", 0)
                data["Travel Revenue"] = f"${travel_cost}"
                data["Engineering Hours – Standard"] = service_cost.get(
                    "engineering_hours", ""
                )
                data["Engineering Hours – After Hours"] = service_cost.get(
                    "after_hours", ""
                )
                data["Project Management Hours"] = service_cost.get(
                    "project_management_hours", ""
                )
                data["Engineering Rate – Standard"] = service_cost.get(
                    "engineering_hourly_rate", ""
                )
                data["Engineering Rate – After Hours"] = service_cost.get(
                    "after_hours_rate", ""
                )
                data[
                    "PM Rate"
                ] = f'${service_cost.get("project_management_hourly_rate", 0)}'
                data[
                    "Risk Revenue"
                ] = f"${DocumentService.calculate_risk_budget(service_cost, sow_doc_settings)}"
                params = {"provider_id": provider.id}
                revenue_and_cost = QuoteService.get_forecast_from_template(
                    documents[0], documents[0].get("doc_type"), **params
                )
                data[
                    "LP PS Revenue"
                ] = f'${revenue_and_cost.get("revenue", 0)}'
                data["LP PS Cost"] = f'${revenue_and_cost.get("cost", 0)}'
                actual_eng_cost = float(
                    data.get("Actual Engineering Cost", "0").replace("$", "")
                )
                actual_pm_cost = float(
                    data.get("Actual PM Cost", "0").replace("$", "")
                )
                realized_margin = round(
                    float(project.billing_amount)
                    - actual_eng_cost
                    - actual_pm_cost
                )
                data["Realized Margin"] = f"${realized_margin}"
                data["Anticipated Margin"] = f"${sow_document.margin}"
                results.append(data)
        return results

    def get_engineer_hours_and_cost(
        self,
        project_crm_id,
        system_member_id,
        after_hours_settings,
        engineer_cost,
    ):
        """
        system_member_id: system member id of the engineer
        after_hours_settings : settings for after hours
        engineer_cost : cost of the engineer maps to project engineer in Acela

        this method will calculate the total engineer hours from all the
        time entries of a project engineer, also calculate the after hours
        on the basis of after hours settings.Calculate the total engineer
        cost from the total hours of a engineer
        """
        result = {}
        timesheet_entries = self.provider.erp_client.get_time_entries(
            member_id=system_member_id, project_crm_id=project_crm_id
        )
        actual_hours = 0
        after_hours = 0
        engineer_total_cost = 0
        if timesheet_entries:
            for timesheet_entry in timesheet_entries:
                work_type = timesheet_entry.get("workType", {}).get("id", "")
                # Checking the after hours setting
                if work_type in after_hours_settings.work_type_ids:
                    after_hours += timesheet_entry.get("actualHours", 0)
                else:
                    actual_hours += timesheet_entry.get("actualHours", 0)
                    if engineer_cost:
                        # Calculating the total engineer cost from the total hours of a engineer
                        engineer_total_cost += timesheet_entry.get(
                            "actualHours", 0
                        ) * float(engineer_cost)
        result["actual_hours"] = actual_hours
        result["after_hours"] = after_hours
        result["engineer_total_cost"] = engineer_total_cost
        return result


class ProjectPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on Project
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        If the actions is deleted, payload accepts a set of crm Id's of the deleted
        projects.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = ProjectQueryService(self.provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        self.payload = prepare_response(self.payload, field_mappings.Project)

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        customer_crm_id = self.payload.pop("customer_crm_id", None)
        primary_contact_crm_id = self.payload.pop(
            "primary_contact_crm_id", None
        )
        project_manager_member_id = self.payload.pop(
            "project_manager_member_id", None
        )
        board_crm_id = self.payload.pop("board_crm_id", None)
        status_crm_id = self.payload.pop("status_crm_id", None)

        # Add provider to payload
        self.payload["provider"] = self.provider.id

        (
            customer_id,
            project_manager_id,
            primary_contact_id,
            board_id,
            overall_status_id,
        ) = (None, None, None, None, None)
        # get customer ID
        if customer_crm_id:
            customer_id = self.query_service.get_customer_by_crm_id(
                customer_crm_id
            )
        self.payload["customer"] = customer_id

        # get project manager id
        if project_manager_member_id:
            # Get users based on member id
            users = self.query_service.get_provider_user_by_member_id(
                [project_manager_member_id]
            )
            if users is not None and users.first():
                project_manager_id = users.first().id
            # If no user found based on member Id use default project manager
            if not project_manager_id:
                project_manager_id = (
                    self.query_service.get_default_project_manager().id
                )
        self.payload["project_manager"] = project_manager_id

        # get primary contact id
        if primary_contact_crm_id:
            primary_contact_id = (
                self.query_service.get_customer_user_by_crm_id(
                    primary_contact_crm_id
                )
            )
        self.payload["primary_contact"] = primary_contact_id

        # get board by board_crm_id
        if board_crm_id:
            board_id = self.query_service.get_board_by_crm_id(board_crm_id)
        self.payload["board"] = board_id

        # get overall status by status_crm_id
        if status_crm_id:
            overall_status = self.query_service.get_overall_status_by_crm_id(
                status_crm_id
            )
            if overall_status:
                overall_status_id = overall_status.id
        self.payload["overall_status"] = overall_status_id

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = ProjectCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = ProjectUpdateSerializer
        project_serializer = serializer(data=self.payload)
        is_valid = project_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = project_serializer.errors
            return is_valid, errors
        self.payload = project_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        if self.action == DbOperation.UPDATE:
            assert crm_id is not None
            project_present = Project.objects.filter(
                provider=self.provider, crm_id=crm_id
            ).exists()
            if project_present:
                self.query_service.update_project_by_crm_id(
                    crm_id, self.payload
                )
            else:
                # This handles the case when the update action is called but the project is
                # present. It creates the project. Use in cases when the create action failed for the
                # object.
                self.payload.update(
                    dict(provider_id=self.provider.id, crm_id=crm_id)
                )
                self.query_service.create_project(self.payload)
        elif self.action == DbOperation.CREATE:
            self.query_service.create_project(self.payload)
        else:
            filter_conditions = dict(
                provider=self.provider, crm_id__in=self.payload.get("id", [])
            )
            self.query_service.delete_project(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("crm_id")
            self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class ProjectEngineerPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on Project
        Engineer. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        If the actions is deleted, payload accepts a set of crm Id's of the deleted
        projects.
        Args:
            action: create/update action
            payload: In case of create/update action, payload is a dict of project data
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = ProjectQueryService(self.provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        self.payload = prepare_response(
            self.payload, field_mappings.ProjectEngineer
        )

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        # Add provider to payload
        self.payload["provider"] = self.provider.id
        self.payload["member"] = self.payload.get("member_id")[0]
        try:
            # project_query = Project.objects.get(
            #     provider=self.provider, crm_id=self.payload.get("project_id")
            # )
            project_id = self.query_service.get_project_by_crm_id(
                self.payload.get("project_id")
            )
            self.payload["project"] = project_id
        except Project.DoesNotExist:
            logger.error("No Project mapping found for Project Engineers")

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = ProjectEngineerCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = ProjectEngineerUpdateSerializer
        project_serializer = serializer(data=self.payload)
        is_valid = project_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = project_serializer.errors
            return is_valid, errors
        self.payload = project_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        if self.action == DbOperation.UPDATE:
            project_engineer_present = ProjectEngineersMapping.objects.filter(
                provider=self.provider,
                member=self.payload.get("member"),
                project=self.payload.get("project"),
            ).exists()
            if project_engineer_present:
                self.query_service.update_engineers(crm_id, self.payload)
            else:
                # This handles the case when the update action is called but the project engineer is
                # present. It creates the project engineer. Use in cases when the create action failed for the
                # object.
                self.payload.update(dict(provider_id=self.provider.id))
                self.query_service.create_engineer(self.payload)
        elif self.action == DbOperation.CREATE:
            self.query_service.create_engineer(self.payload)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("member_crm_id")
            self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors
