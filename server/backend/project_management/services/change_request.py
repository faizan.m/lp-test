from accounts.models import Provider
from core.services import ERPClientIntegrationService
from document.models import SOWCategory, SOWDocument
from document.services import DocumentService, QuoteService
from project_management.exceptions import (
    ProjectSettingException,
)
from project_management.models import (
    AdditionalSetting,
    ChangeRequest,
    Project,
    CancelledChangeRequestSetting,
)
from utils import get_app_logger
from document.services.quote_service import QuoteService
from typing import Dict

logger = get_app_logger(__name__)


class ProjectChangeRequestService:
    def __init__(
        self,
        provider: Provider,
        project: Project,
        change_request: ChangeRequest,
    ):
        self.provider = provider
        self.project = project
        self.change_request = change_request

    @staticmethod
    def get_change_request_category():
        try:
            category = SOWCategory.objects.get(name=SOWCategory.CHANGE_REQUEST)
        except SOWCategory.DoesNotExist:
            category = SOWCategory.objects.create(
                name=SOWCategory.CHANGE_REQUEST
            )
        return category

    def get_change_request_opportunity_settings(self) -> AdditionalSetting:
        try:
            opportunity_settings = AdditionalSetting.objects.only(
                "provider",
                "default_opportunity_stage",
                "default_opportunity_stage",
            ).get(provider=self.provider)
        except AdditionalSetting.DoesNotExist as e:
            raise ProjectSettingException(
                "Opportunity Stage and Type not configured under Project settings."
            )
        return opportunity_settings

    def get_opportunity_default_name(self):
        name = f"CR-{self.project.title}-{self.change_request.name}"
        return name

    def create_change_request_opportunity(self):
        opportunity_settings = self.get_change_request_opportunity_settings()
        payload = {
            "name": self.get_opportunity_default_name(),
            "customer_id": self.project.customer_id,
            "user_id": self.change_request.requested_by_id,
            "stage_id": opportunity_settings.default_opportunity_stage,
            "type_id": opportunity_settings.default_opportunity_type,
        }
        opportunity = QuoteService.create_quote(self.provider.id, payload)
        return opportunity

    def prepare_sow_payload_from_change_request_object(
        self, change_request_details
    ):
        name = f"{self.change_request.name}"
        json_config = change_request_details
        user = self.change_request.requested_by
        quote_id = self.create_change_request_opportunity().get("id")
        doc_type = SOWDocument.CHANGE_REQUEST
        category = self.get_change_request_category()
        sow_payload = dict(
            name=name,
            json_config=json_config,
            user=user,
            quote_id=quote_id,
            doc_type=doc_type,
            category=category,
            customer=self.project.customer,
        )
        return sow_payload

    def create_change_request_sow(self, request_user, change_request_details):
        doc = DocumentService.create_document(
            self.provider.id,
            request_user.id,
            self.prepare_sow_payload_from_change_request_object(
                change_request_details
            ),
        )
        return doc

    @property
    def opportunity_id(self):
        try:
            return self.change_request.sow_document.quote_id
        except Exception:
            return None

    def mark_opportunity_won(self):
        """
        Marks an opportunity won in connectwise. This won stage and won status configured on the
        Change Request Mapping done under board mapping.
        """
        change_request_config = (
            QuoteService.get_provider_change_request_config(self.provider)
        )
        won_status = change_request_config.get("won_status")[0]
        won_stage = change_request_config.get("won_stage")[0]
        opportunity_payload = {"stage_id": won_stage, "status_id": won_status}
        QuoteService.update_quote_stage(
            self.provider, self.opportunity_id, opportunity_payload
        )

    def upload_attachment_to_opportunity(self, files):
        """
        Uploads attachment to opportunity in connectwise.
        """
        record_type = "Opportunity"
        payload = {
            "record_id": self.opportunity_id,
            "record_type": record_type,
            "files": files,
        }
        ERPClientIntegrationService.upload_attachments(
            self.provider.erp_client, payload
        )

    def process_change_request(self, files: object) -> None:
        """
        Runs steps to process a change request. This involves two steps:
        1. Uploading the signed SOW document to the opportunity in the connectwise.
        2. Marking the opportunity status won.
        """
        self.upload_attachment_to_opportunity(files)
        logger.info(
            f"Signed SOW uploaded to Opportunity {self.opportunity_id}"
        )
        self.mark_opportunity_won()
        logger.info(f"Opportunity {self.opportunity_id} marked won.")

        self.change_request.status = ChangeRequest.DONE
        self.change_request.save()
        logger.info(
            f"Change Request {self.change_request} status updated {ChangeRequest.DONE}"
        )

    def update_cancelled_change_request_opportunity(self) -> Dict:
        """
        Update the opportunity linked to cancelled Change Request.
        Opportunity status & stage are updated as per values configured in
        Cancelled Change Request setting.

        Returns:
            Updated opportunity
        """
        try:
            setting: CancelledChangeRequestSetting = (
                CancelledChangeRequestSetting.objects.get(
                    provider=self.provider
                )
            )
        except CancelledChangeRequestSetting.DoesNotExist:
            logger.info(
                f"Cancelled Change Request Setting not configured for the "
                f"provider: {self.provider.name}"
            )
            return {}
        update_payload: Dict = dict(
            stage_id=setting.opp_stage_crm_id,
            status_id=setting.opp_status_crm_id,
        )
        updated_quote: Dict = QuoteService.partially_update_quote(
            self.provider,
            self.change_request.sow_document.quote_id,
            update_payload,
        )
        return updated_quote
