from datetime import datetime, timedelta
from typing import List, Dict

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import Provider, User
from core.integrations import Connectwise
from project_management.models import (
    TimesheetEntryDraft,
    TimesheetEntrySettings,
)
from project_management.timesheet_entry.exceptions import (
    TimeEntrySettingException,
    TimeEntrySettingsNotConfiguredAPIException,
)
from project_management.timesheet_entry.serializer import (
    MemberTimeEntrySerializer,
    TimesheetEntryDraftSerializer,
    TimesheetEntrySettingsSerializer,
    TimeEntryUpdateSerializer,
)
from project_management.timesheet_entry.service import (
    TimeEntry,
    TimeSheetEntry,
)
from utils import get_app_logger

logger = get_app_logger(__name__)


class TimesheetPeriodsListAPIView(APIView):
    """
    API View to list time sheet periods.
    """

    @staticmethod
    def filter_timesheet_periods(
        timesheet_entries: List[Dict],
    ) -> List[Dict]:
        """
        Method to filter timesheet entries for last 30 days and for next week

        Args:
            timesheet_entries: List of timesheet entries

        Returns:
            Filtered timesheet entries
        """
        TODAY: datetime.date = datetime.today().date()
        CURRENT_DAY_OF_WEEK: int = TODAY.weekday()
        THIS_WEEK_MONDAY: datetime.date = TODAY - timedelta(
            days=CURRENT_DAY_OF_WEEK
        )
        LAST_WEEK_MONDAY: datetime.date = THIS_WEEK_MONDAY - timedelta(weeks=1)
        NEXT_WEEK_SUNDAY: datetime.date = TODAY + timedelta(
            days=6 - CURRENT_DAY_OF_WEEK, weeks=1
        )
        PAST_30_DAYS: datetime.date = TODAY - timedelta(days=30)
        required_timesheet_entries: List[Dict] = []
        ########################################################################
        # Setting for minimum hours is yet to be done, hardcoding value for now
        ########################################################################
        MIN_HOURS = 30

        for time_entry in timesheet_entries:
            try:
                start_date: datetime.date = datetime.strptime(
                    time_entry.get("dateStart"), "%Y-%m-%dT%H:%M:%SZ"
                ).date()
                end_date: datetime.date = datetime.strptime(
                    time_entry.get("dateEnd"), "%Y-%m-%dT%H:%M:%SZ"
                ).date()
                hours: float = time_entry.get("hours")
            except ValueError as err:
                logger.log(err)
                continue
            # The time entry should lie within past 30 days
            if (LAST_WEEK_MONDAY <= end_date <= NEXT_WEEK_SUNDAY) or (
                LAST_WEEK_MONDAY <= start_date <= NEXT_WEEK_SUNDAY
            ):
                required_timesheet_entries.append(time_entry)
            elif (
                (TODAY > end_date >= PAST_30_DAYS)
                or (TODAY > start_date >= PAST_30_DAYS)
            ) and hours < MIN_HOURS:
                required_timesheet_entries.append(time_entry)
        return required_timesheet_entries

    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        user: User = request.user
        try:
            system_member_id: int = user.profile.system_member_crm_id
        except AttributeError:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )
        if not system_member_id:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )

        try:
            timesheet_periods = TimeSheetEntry(provider).get_timesheet_periods(
                system_member_id
            )
        except TimeEntrySettingException as e:
            raise TimeEntrySettingsNotConfiguredAPIException(e)
        filtered_time_entries = self.filter_timesheet_periods(
            timesheet_periods
        )
        return Response(data=filtered_time_entries, status=status.HTTP_200_OK)


class TimesheetEntrySettingsCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = TimesheetEntrySettingsSerializer

    def get_queryset(self):
        return TimesheetEntrySettings.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):
        try:
            TimesheetEntrySettings.objects.get(provider=self.request.provider)
        except TimesheetEntrySettings.DoesNotExist:
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                "Time sheet Entry Setting already exists for the Provider."
            )

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class TimesheetEntryDraftListCreateView(generics.ListCreateAPIView):
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("time_period_id",)
    serializer_class = TimesheetEntryDraftSerializer

    def get_queryset(self):
        return TimesheetEntryDraft.objects.filter(
            provider=self.request.provider, user=self.request.user
        ).order_by("-id")

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider, user=self.request.user)


class TimesheetEntryDraftRetrieveUpdateDestroyView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = TimesheetEntryDraftSerializer

    def get_queryset(self):
        return TimesheetEntryDraft.objects.filter(
            provider=self.request.provider, user=self.request.user
        ).order_by("-id")

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider, user=self.request.user)


class TimesheetPeriodProjectsListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        projects = TimeSheetEntry(provider).get_project_list()
        return Response(data=projects, status=status.HTTP_200_OK)


class TimeEntryCreateAPIView(APIView):
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = MemberTimeEntrySerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            system_member_id: int = (
                self.request.user.profile.system_member_crm_id
            )
        except AttributeError:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )
        if not system_member_id:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )

        try:
            time_entry_service = TimeSheetEntry(self.request.provider)
            time_entry_settings = (
                time_entry_service.get_time_sheet_entry_settings()
            )
        except TimeEntrySettingException as e:
            raise TimeEntrySettingsNotConfiguredAPIException(e)

        ticket_validation = []
        # validate ticket status
        for time_entry_record in serializer.validated_data:
            if time_entry_record.get("charge_to_status_id"):
                project_ticket_status = (
                    time_entry_service.get_project_ticket_status(
                        status_id=time_entry_record.get("charge_to_status_id")
                    )
                )
                if project_ticket_status.get("time_entry_not_allowed"):
                    ticket_validation.append(time_entry_record.get("charge_to_id"))
        if ticket_validation:
            error = {
                "error": "chargeToId: Please update the status of this ticket before entering time, "
                         "the current status does not allow time entry.",
                "invalid_ticket_ids": ticket_validation
            }
            raise ValidationError(error)

        time_sheet_ids = []
        for time_entry_record in serializer.validated_data:
            time_sheet_ids.append(time_entry_record["time_sheet_id"])
            time_entry = TimeEntry(
                member_crm_id=system_member_id,
                charge_to_id=time_entry_record["charge_to_id"],
                start_time=time_entry_record["start_time"],
                hours=time_entry_record["hours"],
                work_type=time_entry_record["work_type_id"],
                business_unit=time_entry_settings.default_business_unit_id,
                notes=time_entry_record.get("notes"),
                charge_to_type=time_entry_record.get("charge_to_type"),
            )
            time_entry_service.create_time_entry(time_entry)
        for sheet_id in set(time_sheet_ids):
            time_entry_service.submit_time_sheet(sheet_id)


class TimeEntryListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        user: User = request.user
        try:
            system_member_id: int = user.profile.system_member_crm_id
        except AttributeError:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )
        if not system_member_id:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )

        time_sheet_id = request.query_params.get("time_sheet_id")
        if time_sheet_id:
            time_sheet_ids = [time_sheet_id]
        else:
            time_sheet_ids = None
        timesheet_entries = TimeSheetEntry(provider).get_time_entries(
            system_member_id, time_sheet_ids
        )
        return Response(data=timesheet_entries, status=status.HTTP_200_OK)


class TimeEntryUpdateDeleteAPIView(APIView):
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        serializer = TimeEntryUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        updated_time_entry = self.perform_update(serializer)
        return Response(data=updated_time_entry, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        try:
            system_member_id: int = (
                self.request.user.profile.system_member_crm_id
            )
        except AttributeError:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )
        if not system_member_id:
            raise ValidationError(
                "System Member mapping does not exist for the user."
            )

        try:
            time_entry_id = self.request.query_params.get("time_entry_id")
            time_entry_service = TimeSheetEntry(self.request.provider)
            time_entry_settings = (
                time_entry_service.get_time_sheet_entry_settings()
            )
        except TimeEntrySettingException as e:
            raise TimeEntrySettingsNotConfiguredAPIException(e)
        time_entry = TimeEntry(
            member_crm_id=system_member_id,
            charge_to_id=serializer.validated_data.get("charge_to_id"),
            start_time=serializer.validated_data.get("start_time"),
            hours=serializer.validated_data.get("hours"),
            work_type=serializer.validated_data.get("work_type_id"),
            business_unit=time_entry_settings.default_business_unit_id,
            notes=serializer.validated_data.get("notes"),
            charge_to_type=serializer.validated_data.get("charge_to_type"),
            work_role_id=serializer.validated_data.get("work_role_id"),
            billing_option=serializer.validated_data.get("billing_option"),
        )
        updated_time_entry = time_entry_service.update_time_entry(
            time_entry_id, time_entry
        )
        return updated_time_entry

    def delete(self, request, *args, **kwargs):
        time_entry_id: int = self.request.query_params.get("time_entry_id")
        time_entry_service = TimeSheetEntry(self.request.provider)
        time_entry_service.delete_time_entry(time_entry_id)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ChargeCodesListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        erp_client: Connectwise = provider.erp_client
        charge_codes = erp_client.get_charge_codes()
        return Response(data=charge_codes, status=status.HTTP_200_OK)
