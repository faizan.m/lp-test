import datetime
from dataclasses import asdict, dataclass
from typing import Dict, Generator, List, Optional

from requests import Response

import utils
from accounts.models import Provider
from project_management.models import TimesheetEntrySettings
from project_management.services import ProjectService
from project_management.timesheet_entry.exceptions import (
    TimeEntrySettingException,
)
from service_requests.services import ServiceRequestService

logger = utils.get_app_logger("__name__")

ENTRY_DATE_FORMAT = "%d/%m%/%Y"
PROJECT_CHARGE_TO_TYPE = "ProjectTicket"


@dataclass
class TimeEntry:
    """
    Class to represent a single Time Entry .
    """

    member_crm_id: int
    charge_to_id: int
    start_time: datetime.datetime
    hours: int
    work_type: int
    notes: str
    business_unit: int
    billing_option: Optional[str] = None
    work_role_id: Optional[int] = None
    charge_to_type: str = PROJECT_CHARGE_TO_TYPE

    @property
    def end_time(self):
        return self.start_time + datetime.timedelta(hours=self.hours)


class TimeSheetEntry:
    def __init__(self, provider):
        self.provider: Provider = provider

    def get_time_sheet_entry_settings(self):
        """
        Returns time sheet entry settings for the provider
        """
        try:
            settings: TimesheetEntrySettings = (
                TimesheetEntrySettings.objects.get(provider=self.provider)
            )
        except TimesheetEntrySettings.DoesNotExist:
            logger.debug(
                "TimeSheet Entry Settings not configured for the Provider"
            )
            raise TimeEntrySettingException(
                "TimeSheet Entry Settings not configured for the Provider"
            )
        return settings

    def get_timesheet_periods(self, system_member_id: int) -> List[Dict]:
        """
        Fetches the timesheet periods for the member from connectwise. Only timesheet periods with status
        ids in  settings.time_period_open_status_ids are returned.
        """
        settings: Optional[
            TimesheetEntrySettings
        ] = self.get_time_sheet_entry_settings()
        if settings.time_period_status is None:
            raise TimeEntrySettingException(
                "TimeSheet Entry Settings Open Time period statuses not configured for the Provider"
            )
        return self.provider.erp_client.get_timesheet_periods(
            system_member_id, settings.time_period_status
        )

    def get_project_list(self):
        result = []
        project_service = ProjectService(self.provider)
        required_fields = ["id", "name", "company/name"]
        project_pages: Generator[
            Response, None, None
        ] = project_service.get_provider_projects_from_erp(
            fields=required_fields
        )
        for response in project_pages:
            projects = response.json()
            result.extend(projects)
        return result

    def create_time_entry(self, time_entry: TimeEntry):
        """
        Calls method to create time entry in connectwise
        """
        time_entry_dict = asdict(time_entry) | dict(
            end_time=time_entry.end_time
        )
        self.provider.erp_client.create_time_entry(time_entry_dict)
        logger.debug("Time entry created.")

    def update_time_entry(self, time_entry_id, time_entry: TimeEntry):
        """
        Calls method to update time entry in connectwise
        """
        time_entry_dict = asdict(time_entry) | dict(
            end_time=time_entry.end_time
        )
        return self.provider.erp_client.update_time_entry(
            time_entry_id, time_entry_dict
        )

    def delete_time_entry(self, time_entry_id):
        """
        Calls method to delete time entry in connectwise
        """
        return self.provider.erp_client.delete_time_entry(time_entry_id)

    def submit_time_sheet(self, time_sheet_id: int):
        return self.provider.erp_client.submit_time_sheet(time_sheet_id)

    def get_time_entries(
        self, system_member_id: int, time_sheet_ids: Optional[List[int]] = None
    ) -> List[Dict]:
        """
        Fetches the timesheet entries for the member from connectwise.
        Supports additional filter on the time sheet ids.
        """

        return self.provider.erp_client.get_time_entries(
            system_member_id, time_sheet_ids
        )

    def get_project_ticket_status(self, status_id: int):
        """
        Fetches the project ticket status from status id
        """
        project_board_configs = (
            self.provider.erp_integration.other_config.get("board_mapping")
            .get("Service Board Mapping")
            .get("Projects")
        )
        boards = project_board_configs.get("board")
        return self.provider.erp_client.get_board_status_by_id(
            board_id=boards[0], status_id=status_id
        )
