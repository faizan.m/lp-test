from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from project_management.models import (
    TimesheetEntryDraft,
    TimesheetEntrySettings,
)


class TimesheetEntrySettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimesheetEntrySettings
        fields = (
            "default_work_type_id",
            "time_period_status",
            "default_hours",
            "default_business_unit_id",
            "weekly_hours_threshold",
            "daily_hours_threshold",
            "default_start_time",
            "default_timezone",
        )


class TimesheetEntryDraftSerializer(serializers.ModelSerializer):
    def validate_time_period_id(self, value):
        request = self.context.get("request")
        try:
            draft = TimesheetEntryDraft.objects.get(
                provider=request.provider,
                user=request.user,
                time_period_id=value,
            )
        except TimesheetEntryDraft.DoesNotExist:
            return value

        if self.instance and self.instance.id == draft.id:
            return value

        raise ValidationError(f"Duplicate time period id: {value}")

    class Meta:
        model = TimesheetEntryDraft
        fields = ("id", "draft", "time_period_id")


class MemberTimeEntrySerializer(serializers.Serializer):
    hours = serializers.FloatField()
    start_time = serializers.DateTimeField()
    charge_to_id = serializers.IntegerField()
    work_type_id = serializers.IntegerField()
    time_sheet_id = serializers.IntegerField()
    notes = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    unique_id = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    charge_to_type = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    charge_to_status_id = serializers.IntegerField(
        allow_null=True, required=False
    )


class TimeEntryUpdateSerializer(MemberTimeEntrySerializer):
    work_role_id = serializers.IntegerField()
    billing_option = serializers.CharField()
