from core.exceptions import InvalidConfigurations


class TimeEntrySettingsNotConfiguredAPIException(InvalidConfigurations):
    default_detail = "TimeSheet Entry Settings not configured for the Provider"


class TimeEntrySettingException(Exception):
    "Raise any time entry setting related exception"
