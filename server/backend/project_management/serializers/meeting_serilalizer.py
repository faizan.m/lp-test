from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, DateTimeField, IntegerField
from rest_framework.relations import StringRelatedField, PrimaryKeyRelatedField
from rest_framework.validators import UniqueTogetherValidator
from accounts.models import Profile, User
from project_management.models import (
    Meeting,
    MeetingAttendee,
    MeetingDocument,
    ExternalAttendee,
    AdditionalContacts,
)


class MeetingBaseSerializer(serializers.Serializer):
    id = IntegerField(label="ID", read_only=True)
    name = CharField(style={"base_template": "textarea.html"})
    schedule_start_datetime = DateTimeField(allow_null=True, required=False)
    schedule_end_datetime = DateTimeField(allow_null=True, required=False)
    conducted_on = DateTimeField(allow_null=True, required=False)
    status = serializers.ChoiceField(choices=Meeting.STATUS_CHOICES)
    meeting_type = CharField(read_only=True)
    is_archived = serializers.BooleanField(read_only=True, default=False)
    updated_on = DateTimeField(read_only=True)
    owner = PrimaryKeyRelatedField(
        allow_null=True, queryset=User.objects.all(), required=False
    )


class MeetingUpdateSerializer(serializers.ModelSerializer):
    is_archived = serializers.BooleanField(default=False)

    class Meta:
        model = Meeting
        fields = (
            "name",
            "schedule_start_datetime",
            "schedule_end_datetime",
            "conducted_on",
            "status",
            "is_archived",
        )


class AgendaTopicsSerializer(serializers.Serializer):
    topic = serializers.CharField(required=True)
    notes = serializers.CharField(allow_blank=True)
    id = serializers.IntegerField(allow_null=True, required=False)


class TypedMeetingWriteSerializer(MeetingBaseSerializer):
    status = serializers.ChoiceField(
        choices=Meeting.STATUS_CHOICES, required=False, allow_null=True
    )
    agenda_topics = AgendaTopicsSerializer(many=True, allow_null=True)


class TypedMeetingReadSerializer(serializers.Serializer):
    id = IntegerField(label="ID", read_only=True)
    name = CharField(
        style={"base_template": "textarea.html"}, source="meeting.name"
    )
    schedule_start_datetime = DateTimeField(
        source="meeting.schedule_start_datetime",
        allow_null=True,
        required=False,
    )
    schedule_end_datetime = DateTimeField(
        source="meeting.schedule_end_datetime", allow_null=True, required=False
    )
    conducted_on = DateTimeField(
        source="meeting.conducted_on", allow_null=True, required=False
    )
    agenda_topics = AgendaTopicsSerializer(many=True, allow_null=True)
    status = serializers.ChoiceField(
        source="meeting.status", choices=Meeting.STATUS_CHOICES
    )
    meeting_type = CharField(source="meeting.meeting_type", read_only=True)
    updated_on = DateTimeField(source="meeting.updated_on", read_only=True)
    owner = serializers.UUIDField(source="meeting.owner_id", read_only=True, allow_null=True, required=False)
    owner_name = CharField(source="meeting.owner.name", read_only=True, allow_null=True, required=False)
    owner_profile_pic = serializers.ImageField(
        source="meeting.owner.profile.profile_pic", read_only=True, allow_null=True, required=False
    )


class StatusMeetingWriteSerializer(TypedMeetingWriteSerializer):
    agenda_topics = AgendaTopicsSerializer(many=True, allow_null=True)

    def to_internal_value(self, data):
        # Agenda topics should not be null
        agenda_topics = data.pop("agenda_topics", None)
        agenda_topics = [] if not agenda_topics else agenda_topics
        data.update(agenda_topics=agenda_topics)
        return super().to_internal_value(data)


class StatusMeetingReadSerializer(TypedMeetingReadSerializer):
    pass


class KickoffMeetingWriteSerializer(TypedMeetingWriteSerializer):
    is_internal = serializers.BooleanField(default=False)
    agenda_topics = AgendaTopicsSerializer(many=True, allow_null=True)

    def to_internal_value(self, data):
        # Agenda topics should not be null
        agenda_topics = data.pop("agenda_topics", None)
        agenda_topics = [] if not agenda_topics else agenda_topics
        data.update(agenda_topics=agenda_topics)
        return super().to_internal_value(data)


class KickoffMeetingReadSerializer(TypedMeetingReadSerializer):
    is_internal = serializers.BooleanField()


class CustomerTouchMeetingReadSerializer(TypedMeetingReadSerializer):
    email_subject = serializers.SerializerMethodField()
    email_body = CharField()
    email_body_text = CharField()
    email_body_replaced_text = serializers.SerializerMethodField()

    def get_email_subject(self, obj):
        email_subject_text = obj.email_subject if obj.email_subject else ""

        from project_management.services.meeting_service import MeetingService

        email_variables = MeetingService.get_meeting_email_variables(
            obj.meeting.project
        )

        replaced_subject_text = (
            MeetingService.parse_special_variables_in_email(
                email_subject_text, **email_variables
            )
        )

        return replaced_subject_text

    def get_email_body_replaced_text(self, obj):
        email_body_markdown = obj.email_body if obj.email_body else ""

        from project_management.services.meeting_service import MeetingService

        email_variables = MeetingService.get_meeting_email_variables(
            obj.meeting.project
        )

        replaced_meeting_text = (
            MeetingService.parse_special_variables_in_email_body(
                email_body_markdown, **email_variables
            )
        )

        return replaced_meeting_text


class CustomerTouchMeetingWriteSerializer(TypedMeetingWriteSerializer):
    email_subject = CharField(max_length=1000)
    email_body = CharField()
    email_body_text = CharField(allow_null=True, allow_blank=True)


class CloseOutMeetingReadSerializer(TypedMeetingReadSerializer):
    email_subject = serializers.SerializerMethodField()
    project_acceptance_markdown = CharField()
    project_acceptance_text = CharField()
    project_acceptance_replaced_text = serializers.SerializerMethodField()

    def get_email_subject(self, obj):
        """
        Replaces the mail subject with variables.
        """
        email_subject_text = obj.email_subject if obj.email_subject else ""

        from project_management.services.meeting_service import MeetingService

        email_variables = MeetingService.get_meeting_email_variables(
            obj.meeting.project
        )

        replaced_subject_text = (
            MeetingService.parse_special_variables_in_email(
                email_subject_text, **email_variables
            )
        )

        return replaced_subject_text

    def get_project_acceptance_replaced_text(self, obj):
        from project_management.services.meeting_service import MeetingService

        project_acceptance_markdown = (
            obj.project_acceptance_markdown
            if obj.project_acceptance_markdown
            else ""
        )

        email_variables = MeetingService.get_meeting_email_variables(
            obj.meeting.project
        )

        replaced_meeting_text = (
            MeetingService.parse_special_variables_in_email_body(
                project_acceptance_markdown, **email_variables
            )
        )

        return replaced_meeting_text


class CloseOutMeetingWriteSerializer(TypedMeetingWriteSerializer):
    email_subject = CharField()
    project_acceptance_markdown = CharField()
    project_acceptance_text = CharField()


class MeetingAttendeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeetingAttendee
        fields = ("attendee", "id")


class MeetingAttendeeArchiveSerializer(serializers.Serializer):
    attendee = serializers.CharField(
        read_only=True, allow_null=True, source="attendee.id"
    )
    first_name = serializers.CharField(
        allow_null=True, source="attendee.first_name"
    )
    last_name = serializers.CharField(
        allow_null=True, source="attendee.last_name"
    )
    profile_url = serializers.ImageField(
        read_only=True, allow_null=True, source="attendee.profile.profile_pic"
    )


class MeetingDocumentSerializer(serializers.ModelSerializer):
    uploaded_by = StringRelatedField()

    class Meta:
        model = MeetingDocument
        exclude = ("meeting", "created_on", "updated_on")


class BaseTimeEntrySerializer(serializers.Serializer):
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()
    notes = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )


class PMTimeEntrySerializer(BaseTimeEntrySerializer):
    pass


class EngineerTimeEntrySerializer(BaseTimeEntrySerializer):
    user_id = serializers.UUIDField()
    ticket_id = serializers.IntegerField()

    def validate_user_id(self, user_id):
        request = self.context.get("request")
        provider = request.provider
        try:
            user_profile = Profile.objects.get(
                user__provider=provider, user_id=user_id
            )
            if not user_profile.system_member_crm_id:
                raise ValidationError(
                    f"System Member mapping not done for the user."
                )
        except Profile.DoesNotExist:
            raise ValidationError("No user profile found.")
        return user_profile.system_member_crm_id


class MeetingCompleteSerializer(serializers.Serializer):
    ticket_id = serializers.IntegerField(min_value=0)
    pm_time_entry = PMTimeEntrySerializer()
    engineer_time_entries = EngineerTimeEntrySerializer(
        many=True, required=False, allow_null=True
    )


class ExternalAttendeeSerializer(serializers.ModelSerializer):
    external_contact_name = serializers.CharField(
        source="external_contact.name", read_only=True
    )
    external_contact_email = serializers.EmailField(
        source="external_contact.email",
        read_only=True,
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    external_contact_phone = serializers.CharField(
        source="external_contact.phone",
        max_length=20,
        allow_null=True,
        allow_blank=True,
        required=False,
        read_only=True,
    )
    external_contact_role = serializers.CharField(
        source="external_contact.role",
        max_length=200,
        allow_null=True,
        allow_blank=True,
        required=False,
        read_only=True,
    )

    class Meta:
        model = ExternalAttendee
        fields = [
            "id",
            "provider",
            "meeting",
            "project",
            "external_contact",
            "external_contact_name",
            "external_contact_email",
            "external_contact_phone",
            "external_contact_role",
        ]
        read_only_fields = ["id", "provider", "meeting", "project"]

    def validate_external_contact(self, external_contact):
        """
        Verify if the attendee is added to meeting.
        """
        provider_id = self.context["view"].request.provider.id
        meeting_id = self.context["view"].kwargs["meeting_id"]
        project_id = self.context["view"].kwargs["project_id"]

        if ExternalAttendee.objects.filter(
            external_contact_id=external_contact,
            provider_id=provider_id,
            meeting_id=meeting_id,
            project_id=project_id,
        ).exists():
            raise ValidationError(
                "The attendee is already added to the meeting."
            )
        return external_contact
