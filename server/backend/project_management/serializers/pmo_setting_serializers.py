from django.db.models import QuerySet
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_bulk import BulkListSerializer, BulkSerializerMixin

from project_management.models import (
    AdditionalSetting,
    ChangeRequestEmailTemplate,
    CustomerContactRole,
    EngineerRoleMapping,
    MeetingTemplate,
    MilestoneStatusSetting,
    Project,
    ProjectBoard,
    TimeStatusSetting,
    DateStatusSetting,
    OverallStatusSetting,
    ActivityStatusSetting,
    ProjectPhase,
    ProjectType,
    MeetingMailTemplate,
    PreSalesEngineerRoleMapping,
    ProjectRoleType,
    ProjectRateMappingSetting,
    ProjectAfterHoursMapping,
)
from core.serilizer_fields import IntegerRangeField
from project_management.serializers.meeting_serilalizer import (
    AgendaTopicsSerializer,
)


class TimeStatusSerializer(serializers.ModelSerializer):
    slot_range = IntegerRangeField()

    def validate_slot_range(self, slot_range):
        """
        Validates time ranges for continous and valid slots.
        Tracks previous ranges upper bound and validated with the
        next consecutive slot range.
        """
        last_upper_bound = self.context.get("last_upper_bound", None)

        if last_upper_bound:
            # validate ranges
            if slot_range.lower > slot_range.upper:
                raise ValidationError(f"Invalid time range: {slot_range}")

            if slot_range.lower != last_upper_bound:
                raise ValidationError(f"Overalapping time range: {slot_range}")

            self.context.update({"last_upper_bound": slot_range.upper})
        else:
            # update with context
            self.context.update({"last_upper_bound": slot_range.upper})
        return slot_range

    class Meta:
        model = TimeStatusSetting
        fields = ("id", "slot_range", "color")


class DateStatusSerializer(serializers.ModelSerializer):
    slot_range = IntegerRangeField()

    def validate_slot_range(self, slot_range):
        """
        Validates date ranges for continous and valid slots.
        Tracks previous ranges upper bound and validated with the
        next consecutive slot range.
        """
        last_upper_bound = self.context.get("last_upper_bound", None)

        if last_upper_bound:
            # validate ranges
            if slot_range.lower > slot_range.upper:
                raise ValidationError(f"Invalid date range: {slot_range}")

            if slot_range.lower != last_upper_bound:
                raise ValidationError(f"Overalapping date range: {slot_range}")

            self.context.update({"last_upper_bound": slot_range.upper})
        else:
            # update with context
            self.context.update({"last_upper_bound": slot_range.upper})
        return slot_range

    class Meta:
        model = DateStatusSetting
        fields = ("id", "slot_range", "color")


class OverallStatusSerializer(serializers.ModelSerializer):
    def validate_title(self, title):
        """
        Validates for a provider, project type with the title should not exist.
        """
        request = self.context.get("request")
        board_id = self.context.get("board_id")
        try:
            overall_status_obj = OverallStatusSetting.objects.get(
                title=title, provider=request.provider, board_id=board_id
            )
        except OverallStatusSetting.DoesNotExist:
            return title

        if self.instance and self.instance.id == overall_status_obj.id:
            return title

        raise ValidationError(f"Overall status setting {title} already exist.")

    class Meta:
        model = OverallStatusSetting
        fields = (
            "id",
            "title",
            "connectwise_id",
            "is_default",
            "is_open",
            "color",
        )


class ProjectBoardSerializer(serializers.ModelSerializer):
    statuses = OverallStatusSerializer(many=True)

    def validate_connectwise_id(self, connectwise_id):
        request = self.context.get("request")
        provider_id = request.provider.id
        try:
            board = ProjectBoard.objects.get(
                connectwise_id=connectwise_id, provider_id=provider_id
            )
        except ProjectBoard.DoesNotExist:
            return connectwise_id

        if self.instance and self.instance.id == board.id:
            return connectwise_id

        raise ValidationError(f"Board already exists")

    def create(self, validated_data):
        # handle nested overall status create
        overall_statuses = validated_data.pop("statuses", [])
        provider = self.context.get("request").provider
        # create board
        board = ProjectBoard.objects.create(
            **validated_data, provider_id=provider.id
        )

        # create all overall statuses related to the board
        for status in overall_statuses:
            OverallStatusSetting.objects.create(
                **status, board_id=board.id, provider_id=provider.id
            )
        return board

    def update(self, instance, validated_data):
        # Handles bulk update of overall statuses for a board
        # New statuses are created. Older ones gets updated and
        # the one no more in the list gets deleted.
        overall_statuses = validated_data.pop("statuses", [])
        provider = self.context.get("request").provider
        board_id = instance.id
        # Create or Update
        for status in overall_statuses:
            OverallStatusSetting.objects.update_or_create(
                provider=provider,
                board_id=board_id,
                connectwise_id=status.pop("connectwise_id"),
                title=status.pop("title"),
                defaults={**status},
            )
        return instance

    class Meta:
        model = ProjectBoard
        fields = "__all__"
        read_only_fields = ("id", "provider")


class ActivityStatusSerializer(serializers.ModelSerializer):
    def validate_title(self, title):
        """
        Validates for a provider, project type with the title should not exist.
        """
        request = self.context.get("request")
        try:
            activity_status_obj = ActivityStatusSetting.objects.get(
                title=title, provider=request.provider
            )
        except ActivityStatusSetting.DoesNotExist:
            return title

        if self.instance and self.instance.id == activity_status_obj.id:
            return title

        raise ValidationError(
            f"Activity status setting {title} already exist."
        )

    class Meta:
        model = ActivityStatusSetting
        fields = ("id", "title", "color", "is_default")


class ProjectPhaseSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    def validate_title(self, title):
        """
        Validates for a provider, project type with the title should not exist.
        """
        view = self.context.get("view")
        project_type_id: int = view.kwargs.get("project_type_id")

        queryset = ProjectPhase.objects.filter(
            title=title, project_type_id=project_type_id
        )
        if self.instance is not None:
            if isinstance(self.instance, QuerySet):
                queryset = queryset.exclude(pk__in=self.instance.all())
            else:
                queryset = queryset.exclude(pk=self.instance.pk)

        if queryset.exists():
            raise ValidationError(f"Project Phase {title} already exist.")
        return title

    class Meta:
        model = ProjectPhase
        fields = ("id", "title", "order")
        list_serializer_class = BulkListSerializer


class ProjectTypeSerializer(serializers.ModelSerializer):
    phases = ProjectPhaseSerializer(many=True, read_only=True)
    in_use = serializers.BooleanField(read_only=True)

    def validate_title(self, title):
        """
        Validates for a provider, project type with the title should not exist.
        """
        request = self.context.get("request")
        try:
            project_type = ProjectType.objects.get(
                title=title, provider=request.provider
            )
        except ProjectType.DoesNotExist:
            return title
        if self.instance and self.instance.id == project_type.id:
            return title
        raise ValidationError(f"Project Type {title} already exist.")

    class Meta:
        model = ProjectType
        fields = ("id", "title", "phases", "in_use")


class CustomerContactRoleSettingSerialzier(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    role = serializers.CharField()

    class Meta:
        list_serializer_class = BulkListSerializer

    def validate_role(self, role):
        """
        Validate role is unique for each provider
        Args:
            role: role name E.g. Engineer

        Raises: Validation error if role already exists for the provider
        """

        request = self.context.get("request")
        provider = request.provider
        try:
            role = CustomerContactRole.objects.get(
                role=role, provider=provider
            )
        except CustomerContactRole.DoesNotExist:
            return role

        if self.instance and self.instance.id == role.id:
            return role
        raise ValidationError(f"Role {role} already exist.")

    def create(self, validated_data, **kwargs):
        request = self.context.get("request")
        provider = request.provider
        role = CustomerContactRole.objects.create(
            provider=provider, role=validated_data.get("role")
        )
        return role


class EngineerRoleMappingSerializer(serializers.Serializer):
    role_ids = serializers.ListField(child=serializers.IntegerField())

    def validate_role_ids(self, role_ids):
        provider = self.context.get("request").provider
        try:
            mapping = EngineerRoleMapping.objects.get(provider=provider)
        except EngineerRoleMapping.DoesNotExist:
            return role_ids
        if self.instance and self.instance.id == mapping.id:
            return role_ids
        raise ValidationError(
            "Engineer Role Mapping already exists for the Provider."
        )

    def create(self, validated_data):
        provider = self.context.get("request").provider
        obj = EngineerRoleMapping.objects.create(
            provider=provider, role_ids=validated_data.get("role_ids")
        )
        return obj

    def update(self, instance, validated_data):
        self.instance.role_ids = validated_data.get("role_ids")
        self.instance.save()
        return self.instance


class PreSalesEngineerRoleMappingSerializer(serializers.Serializer):
    role_ids = serializers.ListField(child=serializers.IntegerField())

    def validate_role_ids(self, role_ids):
        provider = self.context.get("request").provider
        try:
            mapping = PreSalesEngineerRoleMapping.objects.get(
                provider=provider
            )
        except PreSalesEngineerRoleMapping.DoesNotExist:
            return role_ids
        if self.instance and self.instance.id == mapping.id:
            return role_ids
        raise ValidationError(
            "Pre Sales Engineer Role Mapping already exists for the Provider."
        )

    def create(self, validated_data):
        provider = self.context.get("request").provider
        obj = PreSalesEngineerRoleMapping.objects.create(
            provider=provider, role_ids=validated_data.get("role_ids")
        )
        return obj

    def update(self, instance, validated_data):
        self.instance.role_ids = validated_data.get("role_ids")
        self.instance.save()
        return self.instance


class MeetingTemplateSerializer(serializers.ModelSerializer):
    agenda_topics = AgendaTopicsSerializer(many=True)

    def validate_template_name(self, template_name):
        provider = self.context.get("request").provider
        try:
            template = MeetingTemplate.objects.get(
                provider=provider, template_name=template_name
            )
        except MeetingTemplate.DoesNotExist:
            return template_name
        if self.instance and self.instance.id == template.id:
            return template_name
        raise ValidationError(
            f"Template {template_name} already exists for the Provider."
        )

    class Meta:
        model = MeetingTemplate
        fields = (
            "id",
            "template_name",
            "action_items",
            "agenda_topics",
            "meeting_type",
        )
        read_only_fields = ("id",)

    def create(self, validated_data):
        agenda_topics = validated_data.pop("agenda_topics", [])
        meeting_template = MeetingTemplate.objects.create(
            **validated_data, agenda_topics=agenda_topics
        )
        return meeting_template

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        return instance


class AdditionalSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalSetting
        fields = (
            "default_project_manager",
            "project_service_board",
            "service_ticket_complete_status",
            "project_completed_item_threshold",
            "project_item_due_date_threshold",
            "default_work_role",
            "default_work_type",
            "default_business_unit",
            "default_opportunity_stage",
            "default_opportunity_type",
            "engineer_default_business_unit",
            "engineer_default_work_type",
            "engineer_default_work_role",
            "note_type_ids",
        )


class MilestoneStatusSerializer(serializers.ModelSerializer):
    def validate_title(self, title):
        """
        Validates for a provider, Milestone status with the title should not exist.
        """
        request = self.context.get("request")
        try:
            milestone_status_obj = MilestoneStatusSetting.objects.get(
                title__iexact=title, provider=request.provider
            )
        except MilestoneStatusSetting.DoesNotExist:
            return title

        if self.instance and self.instance.id == milestone_status_obj.id:
            return title

        raise ValidationError(
            f"Milestone status setting {title} already exist."
        )

    class Meta:
        model = MilestoneStatusSetting
        fields = ("id", "title", "color")


class ProjectRateMappingSerializer(serializers.ModelSerializer):
    is_default = serializers.BooleanField(required=True)

    def validate_role_name(self, role_name):
        """
        Validates for a provider, Project rate mapping with the role_name should not exist.
        """
        request = self.context.get("request")
        try:
            project_rate_mapping_obj = ProjectRateMappingSetting.objects.get(
                role_name__iexact=role_name, provider=request.provider
            )
        except ProjectRateMappingSetting.DoesNotExist:
            return role_name

        if self.instance and self.instance.id == project_rate_mapping_obj.id:
            return role_name

        raise ValidationError(f"Project role with {role_name} already exist.")

    class Meta:
        model = ProjectRateMappingSetting
        fields = ("id", "role_name", "role_type", "is_default", "hourly_cost")


class MeetingMailTemplateSerializer(serializers.ModelSerializer):
    last_updated_by_name = serializers.CharField(read_only=True)

    class Meta:
        model = MeetingMailTemplate
        fields = (
            "id",
            "email_subject",
            "email_body_text",
            "email_body_markdown",
            "meeting_type",
            "last_updated_by_name",
        )
        read_only_fields = ("id",)


class ChangeRequestMailTemplateSerializer(serializers.ModelSerializer):
    last_updated_by_name = serializers.CharField(read_only=True)

    class Meta:
        model = ChangeRequestEmailTemplate
        fields = (
            "id",
            "email_subject",
            "email_body_text",
            "email_body_markdown",
            "last_updated_by_name",
        )
        read_only_fields = ("id",)


class ProjectRoleTypeSettingSerialzier(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    role = serializers.CharField()

    class Meta:
        list_serializer_class = BulkListSerializer

    def validate_role(self, role):
        """
        Validate role is unique for each provider
        Args:
            role: role name E.g. Engineer

        Raises: Validation error if role already exists for the provider
        """

        request = self.context.get("request")
        provider = request.provider
        try:
            role = ProjectRoleType.objects.get(role=role, provider=provider)
        except ProjectRoleType.DoesNotExist:
            return role

        if self.instance and self.instance.id == role.id:
            return role
        raise ValidationError(f"Role {role} already exist.")

    def create(self, validated_data, **kwargs):
        request = self.context.get("request")
        provider = request.provider
        role = ProjectRoleType.objects.create(
            provider=provider, role=validated_data.get("role")
        )
        return role


class AfterHoursMappingSerializer(serializers.Serializer):
    work_type_ids = serializers.ListField(child=serializers.IntegerField())

    def validate_work_type_ids(self, work_type_ids):
        provider = self.context.get("request").provider
        try:
            mapping = ProjectAfterHoursMapping.objects.get(provider=provider)
        except ProjectAfterHoursMapping.DoesNotExist:
            return work_type_ids
        if self.instance and self.instance.id == mapping.id:
            return work_type_ids
        raise ValidationError(
            "After Hours Mapping already exists for the Provider."
        )

    def create(self, validated_data):
        provider = self.context.get("request").provider
        obj = ProjectAfterHoursMapping.objects.create(
            provider=provider, work_type_ids=validated_data.get("work_type_ids")
        )
        return obj

    def update(self, instance, validated_data):
        self.instance.work_type_ids = validated_data.get("work_type_ids")
        self.instance.save()
        return self.instance