from typing import Optional

from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import (
    BooleanField,
    CharField,
    DateTimeField,
    FloatField,
    IntegerField,
    SerializerMethodField,
)
from rest_framework.relations import PrimaryKeyRelatedField, StringRelatedField
from rest_framework.request import Request
from rest_framework.validators import UniqueTogetherValidator

from accounts.models import Customer, Provider, User
from document.models import SOWDocument
from document.services import DocumentService
from project_management.models import (
    ActionItem,
    ChangeRequest,
    ChangeRequestAttachment,
    CriticalPathItem,
    Document,
    MilestoneStatusSetting,
    OverallStatusSetting,
    Project,
    ProjectActivePhase,
    ProjectBoard,
    ProjectItemsArchive,
    ProjectNote,
    ProjectType,
    RiskItem,
    ProjectContactRole,
    AdditionalContacts,
    CancelledChangeRequestSetting,
)
from project_management.serializers import (
    MilestoneStatusSerializer,
    ProjectPhaseSerializer,
)
from utils import name_contains_forbidden_characters, FORBIDDEN_CHARS_LIST


class ProjectUpdatesSchemaSerializer(serializers.Serializer):
    estimated_hours_to_complete = serializers.IntegerField(
        min_value=0, max_value=999, required=False, allow_null=True
    )
    note = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    updates = serializers.CharField(
        allow_blank=True, allow_null=True, required=False
    )


class ProjectCreateSerializer(serializers.Serializer):
    id = IntegerField(label="ID", read_only=True)
    title = CharField(max_length=500)
    description = CharField(
        allow_blank=True,
        allow_null=True,
        required=False,
        style={"base_template": "textarea.html"},
    )
    crm_id = IntegerField(
        allow_null=True,
        max_value=2147483647,
        min_value=-2147483648,
        required=True,
    )
    hours_budget = FloatField(required=False)
    actual_hours = FloatField(required=False)
    estimated_end_date = DateTimeField(allow_null=True, required=False)
    closed_date = DateTimeField(allow_null=True, required=False)
    provider = PrimaryKeyRelatedField(
        queryset=Provider.objects.all(), required=True
    )
    board = PrimaryKeyRelatedField(queryset=ProjectBoard.objects.all())
    type = PrimaryKeyRelatedField(
        allow_null=True, queryset=ProjectType.objects.all(), required=False
    )
    customer = PrimaryKeyRelatedField(
        allow_null=True, queryset=Customer.objects.all(), required=False
    )
    project_manager = PrimaryKeyRelatedField(
        allow_null=True, queryset=User.objects.all(), required=False
    )
    overall_status = PrimaryKeyRelatedField(
        queryset=OverallStatusSetting.objects.all()
    )
    primary_contact = PrimaryKeyRelatedField(
        allow_null=True, queryset=User.objects.all(), required=False
    )
    account_manager = PrimaryKeyRelatedField(
        allow_null=True, queryset=User.objects.all(), required=False
    )
    created_on = DateTimeField(read_only=True)
    updated_on = DateTimeField(read_only=True)
    sow_document = PrimaryKeyRelatedField(
        queryset=SOWDocument.objects.all(), allow_null=True, required=False
    )
    opportunity_crm_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=True, required=False
    )
    billing_amount = FloatField(required=False)
    project_updates = serializers.JSONField(allow_null=True, required=False)

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=Project.objects.all(), fields=("provider", "crm_id")
            )
        ]


class ProjectUpdateSerializer(ProjectCreateSerializer):
    crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("crm_id", "provider")


class ProjectUpdateViewSerializer(ProjectCreateSerializer):
    # crm_id = None
    # provider = None
    customer = None
    board = None
    project_updates = ProjectUpdatesSchemaSerializer(required=False)

    def validate_type(self, project_type):
        if project_type:
            provider = self.context.get("request").provider
            try:
                ProjectType.objects.get(provider=provider, id=project_type.id)
            except ProjectType.DoesNotExist:
                ValidationError("Invalid Project Type.")
        return project_type

    def validate_overall_status(self, overall_status):
        if overall_status:
            provider = self.context.get("request").provider
            try:
                OverallStatusSetting.objects.get(
                    provider=provider, id=overall_status.id
                )
            except OverallStatusSetting.DoesNotExist:
                ValidationError("Invalid Overall Status.")
        return overall_status


class ProjectListRetrieveSerializer(ProjectCreateSerializer):
    provider = None
    board = None
    type = StringRelatedField()
    type_id = IntegerField(source="type.id", read_only=True)
    customer = StringRelatedField()
    customer_id = IntegerField(source="customer.id", read_only=True)
    project_manager = StringRelatedField()
    project_manager_id = CharField(source="project_manager.id", read_only=True)
    project_manager_profile_pic = serializers.ImageField(
        source="project_manager.profile.profile_pic", read_only=True
    )
    overall_status = StringRelatedField()
    overall_status_id = IntegerField(
        source="overall_status.id", read_only=True
    )
    overall_status_color = CharField(
        source="overall_status.color", read_only=True
    )
    primary_contact = StringRelatedField()
    primary_contact_id = CharField(source="primary_contact.id", read_only=True)
    account_manager = StringRelatedField()
    account_manager_id = CharField(source="account_manager.id", read_only=True)
    last_updated_by = StringRelatedField()
    last_updated_by_id = CharField(source="last_updated_by.id", read_only=True)
    time_status_value = IntegerField(read_only=True)
    time_status_color = CharField(read_only=True)
    is_time_status_critical = BooleanField(read_only=True)
    date_status_value = SerializerMethodField()
    date_status_color = CharField(read_only=True)
    progress_percent = IntegerField(read_only=True)
    overdue_critical_path_items = IntegerField(read_only=True)
    overdue_action_items = IntegerField(read_only=True)
    last_action_date = DateTimeField(read_only=True)
    expiring_critical_path_items = IntegerField(read_only=True)
    expiring_action_items = IntegerField(read_only=True)
    action_status_color = CharField(read_only=True)
    sow_document = IntegerField(
        source="sow_document.id", allow_null=True, read_only=True
    )
    linked_sow_doc_name = CharField(
        source="sow_document.name", allow_null=True, read_only=True
    )
    opportunity_crm_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=True, read_only=True
    )
    project_updates = ProjectUpdatesSchemaSerializer(required=False)

    @staticmethod
    def get_date_status_value(obj):
        if obj.date_status_value_diff:
            return obj.date_status_value_diff.days


class SowLinkedClosedProjectsSerialzer(serializers.Serializer):
    id = IntegerField(label="ID", read_only=True)
    title = CharField(max_length=500)


class ProjectActivePhaseListSerializer(serializers.ModelSerializer):
    status = MilestoneStatusSerializer(read_only=True)
    phase = ProjectPhaseSerializer(read_only=True)

    class Meta:
        model = ProjectActivePhase
        fields = (
            "id",
            "phase",
            "status",
            "estimated_completion_date",
            "updated_on",
        )


class ProjectActivePhaseUpdateSerializer(serializers.ModelSerializer):
    def validate_status(self, status):
        """
        Validate that the status_id passed belongs to the project

        """
        provider = self.context.get("request").provider
        queryset = MilestoneStatusSetting.objects.filter(provider=provider)
        is_present = queryset.filter(id=status.id).exists()
        if not is_present:
            raise ValidationError("Invalid status Id")
        return status

    class Meta:
        model = ProjectActivePhase
        fields = ("status", "estimated_completion_date")


class ProjectNoteSerializer(serializers.ModelSerializer):
    author = StringRelatedField()
    author_id = serializers.CharField(read_only=True, source="author.id")
    author_email = serializers.CharField(read_only=True, source="author.email")
    author_profile_pic = serializers.ImageField(
        source="author.profile.profile_pic", read_only=True
    )
    meeting_id = serializers.IntegerField(read_only=True)
    meeting_name = serializers.ReadOnlyField(source="meeting.name")

    def update(self, instance, validated_data):
        if self.context.get("request").user != instance.author:
            raise ValidationError(
                "Do not have edit permissions on the requested note."
            )
        instance = super(ProjectNoteSerializer, self).update(
            instance, validated_data
        )
        return instance

    class Meta:
        model = ProjectNote
        fields = (
            "id",
            "note",
            "author",
            "updated_on",
            "author_profile_pic",
            "author_id",
            "author_email",
            "meeting_id",
            "meeting_name",
            "project_notes_crm_id",
        )


class ProjectDocumentSerializer(serializers.ModelSerializer):
    uploaded_by = StringRelatedField()

    class Meta:
        model = Document
        exclude = ("project", "created_on", "updated_on")


class BaseItemSerializer(serializers.ModelSerializer):
    status_title = CharField(read_only=True, source="status.title")
    status_color = CharField(read_only=True, source="status.color")
    owner_name = CharField(source="owner.name", read_only=True)
    owner_profile_pic = serializers.ImageField(
        source="owner.profile.profile_pic", read_only=True
    )
    resource_name = CharField(read_only=True, source="resource.name")

    class Meta:
        fields = (
            "id",
            "description",
            "owner",
            "resource",
            "due_date",
            "status",
            "status_title",
            "status_color",
            "owner_name",
            "owner_profile_pic",
            "resource_name",
            "updated_on",
            "completed_on",
            "notes",
        )


class CriticalPathItemSerializer(BaseItemSerializer):
    class Meta:
        model = CriticalPathItem
        fields = BaseItemSerializer.Meta.fields


class CriticalPathItemUpdateSerializer(BaseItemSerializer):
    class Meta:
        model = CriticalPathItem
        fields = BaseItemSerializer.Meta.fields


class ActionItemSerializer(BaseItemSerializer):
    class Meta:
        model = ActionItem
        fields = BaseItemSerializer.Meta.fields


class RiskItemSerializer(serializers.ModelSerializer):
    owner_name = CharField(source="owner.name", read_only=True)
    owner_profile_pic = serializers.ImageField(
        source="owner.profile.profile_pic", read_only=True
    )
    created_on = serializers.DateTimeField(default=timezone.now())
    resource_name = CharField(read_only=True, source="resource.name")

    class Meta:
        model = RiskItem
        fields = (
            "id",
            "description",
            "owner",
            "owner_name",
            "owner_profile_pic",
            "resource",
            "resource_name",
            "impact",
            "impact_name",
            "completed_on",
            "created_on",
            "notes",
        )


class ProjectDashboardSerializer(serializers.Serializer):
    id = IntegerField(label="ID", read_only=True)
    title = CharField(max_length=500)
    phases = ProjectActivePhaseListSerializer(many=True)
    type = StringRelatedField()
    type_id = IntegerField(source="type.id", read_only=True)
    customer = StringRelatedField()
    customer_id = IntegerField(source="customer.id", read_only=True)
    project_manager = StringRelatedField()
    project_manager_id = CharField(source="project_manager.id", read_only=True)
    project_manager_profile_pic = serializers.ImageField(
        source="project_manager.profile.profile_pic", read_only=True
    )
    overall_status = StringRelatedField()
    overall_status_id = IntegerField(
        source="overall_status.id", read_only=True
    )
    overall_status_color = CharField(
        source="overall_status.color", read_only=True
    )
    progress_percent = IntegerField(read_only=True)
    project_updates = ProjectUpdatesSchemaSerializer(required=False)


class ProjectEngineerSerializer(serializers.Serializer):
    id = serializers.CharField()
    member_id = serializers.IntegerField(source="profile.system_member_crm_id")
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    name = serializers.CharField()
    profile_url = serializers.ImageField(source="profile.profile_pic")


class ProjectCustomerContactSerializer(serializers.Serializer):
    id = serializers.CharField(allow_null=True)
    user_id = serializers.CharField(allow_null=True)
    email = serializers.CharField(read_only=True, allow_null=True)
    contact_crm_id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(allow_null=True)
    profile_url = serializers.ImageField(read_only=True, allow_null=True)
    contact_record_id = serializers.IntegerField(
        read_only=True, allow_null=True
    )
    project_role = serializers.CharField(allow_null=True)


class ProjectCustomerUpdateContactSerializer(serializers.Serializer):
    contact_crm_id = serializers.IntegerField()
    role_id = serializers.CharField()


class ProjectCustomerContactCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectContactRole
        fields = ("id", "project", "user", "contact_crm_id", "role")
        read_only_fields = ("project",)

    def validate_contact_crm_id(self, contact_crm_id):
        """
        Validate that the contact is already mapped to the project.

        """
        project_id = self.context["view"].kwargs["project_id"]
        queryset = ProjectContactRole.objects.filter(
            project=project_id, contact_crm_id=contact_crm_id
        )
        is_present = queryset.exists()
        if is_present:
            raise ValidationError("Contact already present!")
        return contact_crm_id


class ProjectItemsArchiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectItemsArchive
        fields = "__all__"


class ChangeRequestDetailsSerializer(serializers.Serializer):
    CHANGE_AFFECT_CHOICES = (
        "Resources",
        "Schedule",
        "Budget",
        "Deliverables / Requirements",
    )
    RISK_CHOICES = ("High", "Medium", "Low")
    change_description = serializers.CharField(required=False)
    change_description_markdown = serializers.CharField(required=False)
    technical_details = serializers.CharField(required=False)
    technical_details_markdown = serializers.CharField(required=False)
    estimated_engineering_hours = serializers.IntegerField(
        min_value=0, required=False
    )
    estimated_project_management_hours = serializers.IntegerField(
        min_value=0, required=False
    )
    is_zero_dollar_change_request = serializers.BooleanField(default=False)
    internal_notes = serializers.CharField(required=False)
    internal_notes_markdown = serializers.CharField(required=False)
    reason_for_change = serializers.CharField(required=False)
    reason_for_change_markdown = serializers.CharField(required=False)
    alternates_to_consider = serializers.CharField(required=False)
    alternates_to_consider_markdown = serializers.CharField(required=False)
    change_affects = serializers.ListField(
        child=serializers.ChoiceField(
            choices=CHANGE_AFFECT_CHOICES, required=False
        ),
        required=False,
    )
    risk = serializers.ChoiceField(choices=RISK_CHOICES, required=False)
    impact = serializers.CharField(required=False)
    impact_markdown = serializers.CharField(required=False)


class ChangeRequestSerializer(serializers.ModelSerializer):
    json_config = serializers.JSONField(default={})

    class Meta:
        model = ChangeRequest
        fields = (
            "id",
            "name",
            "requested_by",
            "assigned_to",
            "change_request_type",
            "status",
            "sow_document",
            "json_config",
            "change_number",
            "last_updated_by",
            "created_on",
            "updated_on",
        )
        read_only_fields = (
            "id",
            "sow_document",
            "last_updated_by",
            "change_number",
        )

    def validate_name(self, name: str):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Change Request name should not contain following characters: "
                f"{FORBIDDEN_CHARS_LIST}"
            )
        return name


class ChangeRequestAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChangeRequestAttachment
        exclude = ("change_request", "created_on", "updated_on")


class ProcessChangeRequestSerializer(serializers.Serializer):
    files = serializers.ListField(child=serializers.FileField(), min_length=1)


class ProjectAdditionalContactsSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    phone = serializers.CharField(
        max_length=20, allow_null=True, required=False
    )
    role = serializers.CharField(
        max_length=200, allow_null=True, required=False
    )

    def validate_email(self, email):
        """
        Validate that the email is already mapped to the project.

        """
        project_id = self.context["view"].kwargs["project_id"]
        queryset = AdditionalContacts.objects.filter(
            project=project_id, email=email
        )
        is_present = queryset.exists()
        if is_present:
            raise ValidationError("Email Already Exists for the project!")
        return email

    class Meta:
        model = AdditionalContacts
        fields = (
            "id",
            "name",
            "email",
            "phone",
            "role",
        )
        read_only_fields = ("id", "provider", "project")


class ProjectEngineerCreateSerializer(serializers.Serializer):
    provider = PrimaryKeyRelatedField(
        queryset=Provider.objects.all(), required=True
    )
    member = serializers.PrimaryKeyRelatedField(
        allow_null=False, queryset=User.objects.all(), required=True
    )
    project = serializers.PrimaryKeyRelatedField(
        allow_null=False, queryset=Project.objects.all(), required=True
    )
    member_crm_id = serializers.IntegerField()


class ProjectEngineerUpdateSerializer(ProjectEngineerCreateSerializer):
    provider = None


class CancelledChangeRequestSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = CancelledChangeRequestSetting
        fields = ("provider", "opp_status_crm_id", "opp_stage_crm_id")
        read_only_fields = ("provider",)
