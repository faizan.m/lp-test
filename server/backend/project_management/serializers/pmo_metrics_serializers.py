from rest_framework import serializers
from project_management.serializers.project_serilalizer import (
    ProjectUpdatesSchemaSerializer,
)
from project_management.models.pmo_metrics import PMOMetricsNotes


class PMOMetricsTableDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    crm_id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    customer_id = serializers.IntegerField(read_only=True)
    customer_name = serializers.CharField(
        source="customer.name", read_only=True
    )
    project_manager_id = serializers.UUIDField(read_only=True)
    project_manager_name = serializers.CharField(
        source="project_manager.name", read_only=True
    )
    project_manager_pic = serializers.ImageField(
        source="project_manager.profile.profile_pic", read_only=True
    )
    type_id = serializers.IntegerField(read_only=True)
    type_title = serializers.CharField(source="type.title", read_only=True)
    overall_status_id = serializers.IntegerField(
        source="overall_status.id", read_only=True
    )
    overall_status_title = serializers.CharField(
        source="overall_status.title", read_only=True
    )
    overall_status_color = serializers.CharField(
        source="overall_status.color", read_only=True
    )
    project_updates = ProjectUpdatesSchemaSerializer(read_only=True)
    progress_percent = serializers.IntegerField(read_only=True)
    closed_date = serializers.DateTimeField(read_only=True)
    estimated_end_date = serializers.DateTimeField(read_only=True)
    opportunity_crm_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=True, read_only=True
    )
    sow_document_id = serializers.IntegerField(allow_null=True, read_only=True)


class PMOMetricsBudgetReviewTabDataSerializer(PMOMetricsTableDataSerializer):
    project_manager_pic = None
    project_updates = None
    progress_percent = None
    closed_date = None
    opportunity_crm_ids = None
    sow_document_id = None
    hours_budget = serializers.IntegerField(read_only=True)
    actual_hours = serializers.IntegerField(read_only=True)
    percent_of_budget = serializers.FloatField(read_only=True)
    remaining_hours = serializers.IntegerField(read_only=True)


class PMOMetricsManagerProjectsCountChartSerailizer(serializers.Serializer):
    project_manager_id = serializers.CharField(read_only=True)
    project_manager_name = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class PMOMetricsProjectTypeProjectsCountChartSerializer(
    serializers.Serializer
):
    project_type = serializers.CharField(read_only=True)
    project_type_id = serializers.IntegerField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class PMOMetricsOverallStatusProjectsCountChartSerializer(
    serializers.Serializer
):
    project_status = serializers.CharField(read_only=True)
    project_status_id = serializers.IntegerField(read_only=True)
    count = serializers.IntegerField(read_only=True)
    project_status_color = serializers.CharField(read_only=True)


class PMOMetricsProjectsCounByEstimatedEndDateRangeChartSerializer(
    serializers.Serializer
):
    days_range = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class PMOMetricsNotesTabDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    note = serializers.CharField(read_only=True)
    provider_id = serializers.IntegerField(read_only=True)
    author_id = serializers.CharField(
        source="author.id", read_only=True, required=False
    )
    author_first_name = serializers.CharField(
        source="author.first_name", read_only=True, required=False
    )
    author_last_name = serializers.CharField(
        source="author.last_name", read_only=True, required=False
    )
    author_pic = serializers.ImageField(
        source="author.profile.profile_pic", read_only=True, required=False
    )
    created_on = serializers.DateTimeField(read_only=True)
    updated_on = serializers.DateTimeField(read_only=True)
    is_archived = serializers.BooleanField(read_only=True)


class PMOMetricsNotesCreateSerializer(serializers.Serializer):
    note = serializers.CharField(
        allow_blank=False, allow_null=False, required=True
    )


class PMOMetricsNotesUpdateSerializer(serializers.Serializer):
    note = serializers.CharField(
        required=False, allow_blank=False, allow_null=False
    )
    is_archived = serializers.BooleanField(required=False, allow_null=False)
