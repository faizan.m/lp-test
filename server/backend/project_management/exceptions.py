from core.exceptions import InvalidConfigurations


class PMOSettingsNotConfigured(InvalidConfigurations):
    default_detail = "Provider Project settings not configured."


class ProjectSettingException(Exception):
    "Raise any project setting related exception"
