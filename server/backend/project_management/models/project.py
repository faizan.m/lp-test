from collections import namedtuple

from django.contrib.postgres.fields import ArrayField
from django.db.models import JSONField
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from model_utils import Choices

from accounts.model_utils import TimeStampedModel
from auditlog.registry import auditlog, update_only_auditlog
from custom_storages import get_storage_class
from project_management.mixins import (
    ProjectManagementAuditLogMixin,
    SpecificTypeMeetingAuditMixin,
)


class ProjectQueryset(models.QuerySet):
    def open(self):
        return self.filter(overall_status__is_open=True)

    def closed(self):
        return self.filter(overall_status__is_open=False)


class Project(ProjectManagementAuditLogMixin, TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="projects"
    )
    board = models.ForeignKey(
        "project_management.ProjectBoard",
        on_delete=models.CASCADE,
        related_name="projects",
    )
    title = models.CharField(max_length=500)
    description = models.TextField(null=True, blank=True)
    type = models.ForeignKey(
        "project_management.ProjectType",
        on_delete=models.SET_NULL,
        related_name="projects",
        null=True,
    )
    crm_id = models.IntegerField(null=True)
    customer = models.ForeignKey(
        "accounts.Customer",
        on_delete=models.SET_NULL,
        related_name="projects",
        null=True,
    )
    project_manager = models.ForeignKey(
        "accounts.User",
        on_delete=models.SET_NULL,
        related_name="projects",
        null=True,
    )
    overall_status = models.ForeignKey(
        "project_management.OverallStatusSetting",
        on_delete=models.SET_NULL,
        related_name="projects",
        null=True,
    )
    hours_budget = models.FloatField(default=0)
    actual_hours = models.FloatField(default=0)
    estimated_end_date = models.DateTimeField(null=True)
    closed_date = models.DateTimeField(null=True)
    primary_contact = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    account_manager = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    sow_document = models.ForeignKey(
        "document.SOWDocument",
        related_name="+",
        on_delete=models.SET_NULL,
        null=True,
    )
    opportunity_crm_ids = ArrayField(
        base_field=models.IntegerField(), default=list, blank=True
    )
    billing_amount = models.FloatField(default=0)

    objects = ProjectQueryset.as_manager()
    project_updates = models.JSONField(default=dict)

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project_manager"]),
            models.Index(fields=["provider", "crm_id"]),
            models.Index(fields=["provider", "customer"]),
            models.Index(fields=["provider", "board"]),
            models.Index(fields=["-updated_on"]),
        ]
        unique_together = ["provider", "crm_id"]

    def __str__(self):
        return self.title

    def get_additional_data(self):
        return {"project_id": self.id}


def file_upload_url(instance, filename):
    file_path = f"providers/projects/{instance.project.id}/files/{filename}"
    return file_path


def meeting_file_upload_url(instance, filename):
    file_path = (
        f"providers/projects/{instance.meeting.project.id}/"
        f"meetings/{instance.meeting.id}/files/{filename}"
    )
    return file_path


class Document(TimeStampedModel):
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="documents",
    )
    file_name = models.CharField(max_length=200)
    file_path = models.FileField(
        upload_to=file_upload_url,
        null=True,
        blank=True,
        storage=get_storage_class(),
    )
    uploaded_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return self.file_name


class ProjectActivePhase(ProjectManagementAuditLogMixin, models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="phases",
    )
    phase = models.ForeignKey(
        "project_management.ProjectPhase",
        on_delete=models.CASCADE,
        related_name="+",
    )
    status = models.ForeignKey(
        "project_management.MilestoneStatusSetting",
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    estimated_completion_date = models.DateTimeField(null=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [models.Index(fields=["provider", "project"])]


class NoteQuerySet(models.QuerySet):
    def project_notes(self):
        return self.filter(meeting__isnull=True)

    def meeting_notes(self):
        return self.filter(meeting__isnull=False)


class ProjectNote(ProjectManagementAuditLogMixin, models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="notes",
    )
    author = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    meeting = models.ForeignKey(
        "project_management.Meeting",
        on_delete=models.SET_NULL,
        related_name="meeting_notes",
        null=True,
    )
    note = models.TextField()
    project_notes_crm_id = models.PositiveIntegerField(null=True, blank=True)
    updated_on = models.DateTimeField(auto_now=True)

    objects = NoteQuerySet.as_manager()

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["-updated_on"]),
        ]


class ProjectCWNote(ProjectManagementAuditLogMixin, models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="project_cw_notes",
    )
    note_crm_id = models.PositiveIntegerField(null=True, blank=True)
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="+",
        null=True,
        blank=True,
    )
    updated_on = models.DateTimeField(auto_now=True)

    objects = NoteQuerySet.as_manager()

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["-updated_on"]),
        ]


class ProjectItemBaseModel(ProjectManagementAuditLogMixin, TimeStampedModel):
    description = models.TextField()
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    owner = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, related_name="+", null=True
    )
    resource = models.ForeignKey(
        "project_management.AdditionalContacts",
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
    )
    due_date = models.DateTimeField(null=True)
    completed_on = models.DateTimeField(null=True)
    status = models.ForeignKey(
        "project_management.ActivityStatusSetting",
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    notes = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.description

    class Meta:
        abstract = True


class CriticalPathItem(ProjectItemBaseModel):
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="critical_path_items",
    )

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["due_date"]),
        ]


class ActionItem(ProjectItemBaseModel):
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="action_items",
    )

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["due_date"]),
        ]


class RiskItem(ProjectManagementAuditLogMixin, TimeStampedModel):
    Impact = namedtuple("Impact", "title color numeric_value")
    HIGH = Impact("High", "#db1643", 3)
    MEDIUM = Impact("Medium", "#dba629", 2)
    LOW = Impact("Low", "#808688", 1)
    MITIGATED = Impact("Mitigated", "#176f07", 0)
    RISK_IMPACTS = [HIGH, MEDIUM, LOW, MITIGATED]
    RISK_IMPACT_CHOICES = (
        (HIGH.numeric_value, HIGH.title),
        (MEDIUM.numeric_value, MEDIUM.title),
        (LOW.numeric_value, LOW.title),
        (MITIGATED.numeric_value, MITIGATED.title),
    )
    RISK_IMPACT_CHOICES_MAPPING = dict(RISK_IMPACT_CHOICES)
    IMPACT_COLORS = (
        (HIGH.numeric_value, HIGH.color),
        (MEDIUM.numeric_value, MEDIUM.color),
        (LOW.numeric_value, LOW.color),
        (MITIGATED.numeric_value, MITIGATED.color),
    )
    IMPACT_COLORS_MAPPING = dict(IMPACT_COLORS)

    description = models.TextField()
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="risk_items",
    )
    owner = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+", null=True
    )
    resource = models.ForeignKey(
        "project_management.AdditionalContacts",
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
    )
    impact = models.PositiveSmallIntegerField(
        choices=RISK_IMPACT_CHOICES, default=LOW.numeric_value
    )
    completed_on = models.DateTimeField(null=True)
    created_on = models.DateTimeField()
    notes = models.TextField(null=True, blank=True)

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["-updated_on"]),
        ]

    def __str__(self):
        return self.description

    def get_impact_color(self):
        return self.IMPACT_COLORS_MAPPING.get(self.impact)

    @property
    def impact_name(self):
        return self.RISK_IMPACT_CHOICES_MAPPING.get(self.impact)


class Meeting(TimeStampedModel):
    CLOSE_OUT = "Close Out"
    CUSTOMER_TOUCH = "Customer Touch"
    INTERNAL_KICKOFF = "Internal Kickoff"
    KICKOFF = "Kickoff"
    STATUS = "Status"
    MEETING_TYPES = (
        (STATUS, STATUS),
        (CUSTOMER_TOUCH, CUSTOMER_TOUCH),
        (INTERNAL_KICKOFF, INTERNAL_KICKOFF),
        (KICKOFF, KICKOFF),
        (CLOSE_OUT, CLOSE_OUT),
    )

    PLANNED = "Pending"
    DONE = "Completed"
    STATUS_CHOICES = ((PLANNED, PLANNED), (DONE, DONE))
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    status = models.CharField(
        choices=STATUS_CHOICES, max_length=100, null=True, default=PLANNED
    )
    name = models.TextField()
    meeting_type = models.CharField(max_length=100, choices=MEETING_TYPES)
    schedule_start_datetime = models.DateTimeField(null=True)
    schedule_end_datetime = models.DateTimeField(null=True)
    conducted_on = models.DateTimeField(null=True)
    owner = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+", null=True
    )

    class Meta:
        indexes = [
            models.Index(fields=["provider", "project"]),
            models.Index(fields=["schedule_start_datetime"]),
        ]

    def __str__(self):
        return self.name


class MeetingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related("meeting")


class StatusMeeting(SpecificTypeMeetingAuditMixin, models.Model):
    meeting = models.OneToOneField(
        "project_management.Meeting",
        related_name="status_meeting",
        on_delete=models.CASCADE,
    )
    agenda_topics = JSONField(default=list)
    objects = MeetingManager()

    def __str__(self):
        return self.meeting.name


class KickoffMeeting(SpecificTypeMeetingAuditMixin, models.Model):
    meeting = models.OneToOneField(
        "project_management.Meeting",
        related_name="kickoff_meeting",
        on_delete=models.CASCADE,
    )
    is_internal = models.BooleanField(default=False)
    agenda_topics = JSONField(default=list)
    objects = MeetingManager()

    def __str__(self):
        return self.meeting.name


class CustomerTouchMeeting(SpecificTypeMeetingAuditMixin, models.Model):
    meeting = models.OneToOneField(
        "project_management.Meeting",
        related_name="+",
        on_delete=models.CASCADE,
    )
    email_subject = models.CharField(max_length=1000)
    email_body = models.TextField()
    email_body_text = models.TextField(null=True, blank=True)

    objects = MeetingManager()

    def __str__(self):
        return self.meeting.name


class CloseOutMeeting(SpecificTypeMeetingAuditMixin, models.Model):
    meeting = models.OneToOneField(
        "project_management.Meeting",
        related_name="+",
        on_delete=models.CASCADE,
    )
    email_subject = models.CharField(max_length=500, null=True)
    project_acceptance_text = models.TextField(null=True, blank=True)
    project_acceptance_markdown = models.TextField(null=True, blank=True)

    objects = MeetingManager()

    def __str__(self):
        return self.meeting.name


@receiver(post_delete, sender=CustomerTouchMeeting)
@receiver(post_delete, sender=StatusMeeting)
@receiver(post_delete, sender=KickoffMeeting)
@receiver(post_delete, sender=CloseOutMeeting)
def post_delete_user(sender, instance, *args, **kwargs):
    """
    Signal to delete related Meeting object on Type meeting delete.
    """
    if instance.meeting:
        instance.meeting.delete()


class ProjectContactRole(ProjectManagementAuditLogMixin, models.Model):
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    contact_crm_id = models.IntegerField(null=True)
    user = models.ForeignKey(
        "accounts.User",
        on_delete=models.SET_NULL,
        related_name="project_contact_roles",
        null=True,
    )
    role = models.ForeignKey(
        "project_management.CustomerContactRole",
        related_name="+",
        on_delete=models.CASCADE,
    )

    class Meta:
        indexes = [models.Index(fields=["project", "contact_crm_id"])]
        unique_together = ["project", "contact_crm_id"]

    def __str__(self):
        return f"{self.user} {self.role.role}"


class MeetingAttendee(SpecificTypeMeetingAuditMixin, models.Model):
    meeting = models.ForeignKey(
        "project_management.Meeting",
        related_name="attendees",
        on_delete=models.CASCADE,
    )
    attendee = models.ForeignKey(
        "accounts.User", related_name="+", on_delete=models.CASCADE
    )


class MeetingArchive(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    meeting = models.OneToOneField(
        "project_management.Meeting",
        related_name="archive",
        on_delete=models.DO_NOTHING,
    )
    payload = JSONField()

    class Meta:
        indexes = [models.Index(fields=["provider", "project", "meeting"])]
        unique_together = ["provider", "project", "meeting"]

    def __str__(self):
        return self.meeting.name


class MeetingDocument(TimeStampedModel):
    meeting = models.ForeignKey(
        "project_management.meeting",
        on_delete=models.CASCADE,
        related_name="documents",
    )
    file_name = models.CharField(max_length=1000)
    file_path = models.FileField(
        upload_to=meeting_file_upload_url,
        null=True,
        blank=True,
        max_length=1000,
        storage=get_storage_class(),
    )
    uploaded_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return self.file_name


class ProjectItemsArchive(models.Model):
    CRITICAL_PATH_ITEM = "Critical Path Item"
    ACTION_ITEM = "Action Item"
    RISK_ITEM = "Risk Item"
    MEETING = "Meeting"

    ITEM_TYPE_CHOICES = (
        (CRITICAL_PATH_ITEM, CRITICAL_PATH_ITEM),
        (ACTION_ITEM, ACTION_ITEM),
        (RISK_ITEM, RISK_ITEM),
    )

    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    item_type = models.CharField(max_length=100, choices=ITEM_TYPE_CHOICES)
    blob = JSONField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(null=True)

    def __str__(self):
        return self.item_type


class ChangeRequest(ProjectManagementAuditLogMixin, TimeStampedModel):
    IN_PROGRESS = "In Progress"
    DONE = "Approved"
    REJECTED = "Cancelled/Rejected"

    CHANGE_REQUEST_STATUS = (
        (REJECTED, REJECTED),
        (IN_PROGRESS, IN_PROGRESS),
        (DONE, DONE),
    )
    FIXED_FEE = "Fixed Fee"
    T_AND_M = "T & M"
    CHANGE_REQUEST_TYPE = Choices((FIXED_FEE, FIXED_FEE), (T_AND_M, T_AND_M))
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="change_requests",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=500)
    requested_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    assigned_to = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    change_number = models.PositiveIntegerField()
    sow_document = models.ForeignKey(
        "document.SOWDocument",
        related_name="change_request",
        on_delete=models.SET_NULL,
        null=True,
    )
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    status = models.CharField(
        choices=CHANGE_REQUEST_STATUS, default=IN_PROGRESS, max_length=200
    )
    change_request_type = models.CharField(
        choices=CHANGE_REQUEST_TYPE, max_length=30, null=True
    )

    def __str__(self):
        return self.name


class ChangeRequestAttachment(TimeStampedModel):
    def change_request_attachment_file_upload_url(instance, filename):
        file_path = (
            f"providers/projects/{instance.change_request.project.id}/"
            f"change-requests/{instance.id}/files/{filename}"
        )
        return file_path

    change_request = models.ForeignKey(
        "project_management.ChangeRequest",
        on_delete=models.CASCADE,
        related_name="attachments",
    )
    file_name = models.CharField(max_length=1000)
    file_path = models.FileField(
        upload_to=change_request_attachment_file_upload_url,
        null=True,
        blank=True,
        storage=get_storage_class(),
    )

    def __str__(self):
        return self.file_name


class AdditionalContacts(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    name = models.TextField()
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    role = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        indexes = [models.Index(fields=["provider", "project"])]
        unique_together = ["project", "email"]


class ExternalAttendee(models.Model):
    """
    Model to store the details of external contacts for a project meeting.
    """

    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
    )
    meeting = models.ForeignKey(
        "project_management.Meeting",
        related_name="external_attendees",
        on_delete=models.CASCADE,
    )
    external_contact = models.ForeignKey(
        "project_management.AdditionalContacts",
        related_name="+",
        on_delete=models.CASCADE,
    )


class ProjectEngineersMapping(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    project = models.ForeignKey(
        "project_management.Project",
        on_delete=models.CASCADE,
        related_name="engineers",
    )
    member = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="+",
    )
    member_crm_id = models.IntegerField()


class CancelledChangeRequestSetting(models.Model):
    """
    Model to persist cancelled change request setting.
    """

    provider = models.OneToOneField(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    # Cancelled opportunity status
    opp_status_crm_id = models.PositiveIntegerField(null=True)
    # Cancelled opportunity stage
    opp_stage_crm_id = models.PositiveIntegerField(null=True)

    @classmethod
    def configured_for_provider(cls, provider) -> bool:
        """
        Checks if setting configured for provider.

        Args:
            provider: Provider

        Returns:
            True if configured else False
        """
        return CancelledChangeRequestSetting.objects.filter(
            provider=provider
        ).exists()


common_exclude_fields = [
    "id",
    "created_on",
    "updated_on",
    "provider",
    "project",
]
auditlog.register(ActionItem, exclude_fields=common_exclude_fields)
auditlog.register(ChangeRequest, exclude_fields=common_exclude_fields)
auditlog.register(CloseOutMeeting, exclude_fields=common_exclude_fields)
auditlog.register(CriticalPathItem, exclude_fields=common_exclude_fields)
auditlog.register(CustomerTouchMeeting, exclude_fields=common_exclude_fields)
auditlog.register(KickoffMeeting, exclude_fields=common_exclude_fields)
auditlog.register(Meeting, exclude_fields=common_exclude_fields)
auditlog.register(MeetingAttendee, exclude_fields=common_exclude_fields)
auditlog.register(ProjectNote, exclude_fields=common_exclude_fields)
auditlog.register(Project, exclude_fields=common_exclude_fields)
update_only_auditlog.register(
    ProjectActivePhase, exclude_fields=common_exclude_fields
)
auditlog.register(RiskItem, exclude_fields=common_exclude_fields)
auditlog.register(StatusMeeting, exclude_fields=common_exclude_fields)
