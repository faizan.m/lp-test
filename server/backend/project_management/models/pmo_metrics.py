from django.db import models
from accounts.model_utils import TimeStampedModel


class PMOMetricsNotes(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    note = models.TextField(null=False, blank=False)
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.SET_NULL,
        null=True,
        related_name="pmo_metrics_notes",
    )
    is_archived = models.BooleanField(default=False)
