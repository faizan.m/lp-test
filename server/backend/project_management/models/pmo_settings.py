"""
Contains models related to settings required for PMO module
"""
from django.contrib.postgres.fields import (
    ArrayField,
    IntegerRangeField,
)
from django.db.models import JSONField
from django.contrib.postgres.validators import (
    RangeMaxValueValidator,
    RangeMinValueValidator,
)
from django.core.exceptions import ValidationError
from django.db import models

from auditlog.registry import auditlog
from .project import Meeting
from accounts.model_utils import TimeStampedModel
from project_management.mixins import ProjectSettingAuditMixin


class TimeStatusSetting(ProjectSettingAuditMixin, models.Model):
    """
    This model is used to store threshold values for Time status.
    Based on the threshold values the project Time (T) status can
    be calculated.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_time_status",
        on_delete=models.CASCADE,
    )
    slot_range = IntegerRangeField(
        validators=[RangeMinValueValidator(0), RangeMaxValueValidator(101)],
        help_text="The range of values for this slot.",
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this slot.",
    )
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        text = f"{self.slot_range}"
        return text

    def validate_overlapping_slots(self, *args, **kwargs):
        """
        Validate overlapping slots for the provider
        """

        qs = self.__class__.objects.filter(
            slot_range__overlap=self.slot_range, provider=self.provider
        )
        if qs.exists():
            raise ValidationError(
                {"slot_range": ["overlapping slots for provider"]}
            )

    def clean(self, *args, **kwargs):
        super(TimeStatusSetting, self).clean(*args, **kwargs)
        self.validate_overlapping_slots()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(TimeStatusSetting, self).save(*args, **kwargs)


class DateStatusSetting(ProjectSettingAuditMixin, models.Model):
    """
    This model is used to store threshold values for Date status.
    Based on the threshold values the project Date (D) status can
    be calculated.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_date_status",
        on_delete=models.CASCADE,
    )
    slot_range = IntegerRangeField(
        help_text="The range of values for this slot."
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this slot.",
    )
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        text = f"{self.slot_range}"
        return text

    def validate_overlapping_slots(self, *args, **kwargs):
        """
        Validate overlapping slots for the provider
        """

        qs = self.__class__.objects.filter(
            slot_range__overlap=self.slot_range, provider=self.provider
        )
        if qs.exists():
            raise ValidationError(
                {"slot_range": ["overlapping slots for provider"]}
            )

    def clean(self, *args, **kwargs):
        super(DateStatusSetting, self).clean(*args, **kwargs)
        self.validate_overlapping_slots()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(DateStatusSetting, self).save(*args, **kwargs)


class ProjectBoard(ProjectSettingAuditMixin, models.Model):
    name = models.CharField(max_length=150)
    connectwise_id = models.PositiveIntegerField(
        null=True, help_text="Connectwise id for this board."
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="project_boards",
    )

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("provider", "connectwise_id")


class OverallStatusSetting(ProjectSettingAuditMixin, TimeStampedModel):
    """
    This model is used to store overall status settings.
    """

    title = models.CharField(
        max_length=100, help_text="Title for this status."
    )
    description = models.TextField(
        blank=True, null=True, help_text="Description for this status"
    )
    connectwise_id = models.PositiveIntegerField(
        null=True, help_text="Connectwise id for this status."
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_overall_status",
        on_delete=models.CASCADE,
    )
    is_open = models.BooleanField(default=True)
    board = models.ForeignKey(
        "project_management.ProjectBoard",
        on_delete=models.CASCADE,
        related_name="statuses",
    )
    color = models.CharField(max_length=10)
    is_default = models.BooleanField(default=True)

    def __str__(self):
        text = f"{self.title}"
        return text


class ActivityStatusSetting(ProjectSettingAuditMixin, TimeStampedModel):
    DONE = "Completed"
    OVERDUE = "Overdue"
    title = models.CharField(
        max_length=100, help_text="Title for this status."
    )
    description = models.TextField(
        blank=True, null=True, help_text="Description for this status"
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_activity_status",
        on_delete=models.CASCADE,
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this status.",
    )
    project = models.ForeignKey(
        "project_management.Project",
        related_name="+",
        on_delete=models.CASCADE,
        null=True,
    )
    is_default = models.BooleanField(default=False)

    class Meta:
        unique_together = ("provider", "title")

    def __str__(self):
        text = f"{self.title}"
        return text


class ProjectType(ProjectSettingAuditMixin, TimeStampedModel):
    title = models.CharField(max_length=250, help_text="Project title")
    description = models.TextField(
        null=True, help_text="A short description of the project type."
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="project_types",
        on_delete=models.CASCADE,
        help_text="Provider for the project type",
    )

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ("provider", "title")


class ProjectPhase(ProjectSettingAuditMixin, TimeStampedModel):
    CUSTOMER_TOUCH = "Customer Touch"
    KICKOFF = "Kickoff"
    PROJECT_CLOSE = "Project Close"

    DEFAULT_PROJECT_PHASES = [CUSTOMER_TOUCH, KICKOFF, PROJECT_CLOSE]

    title = models.CharField(max_length=250, help_text="Phase Title")
    description = models.TextField(
        null=True, help_text="A short description of the phase."
    )
    order = models.PositiveSmallIntegerField()
    project_type = models.ForeignKey(
        "project_management.ProjectType",
        related_name="phases",
        on_delete=models.CASCADE,
        help_text="Project Type to which the phase is related.",
    )

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ("project_type", "title")
        indexes = [models.Index(fields=["order"])]


class CustomerContactRole(ProjectSettingAuditMixin, models.Model):
    role = models.CharField(max_length=200)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)

    def __str__(self):
        return self.role

    class Meta:
        unique_together = ("provider", "role")


class EngineerRoleMapping(ProjectSettingAuditMixin, models.Model):
    role_ids = ArrayField(models.IntegerField())
    provider = models.OneToOneField(
        "accounts.Provider", on_delete=models.CASCADE
    )


class PreSalesEngineerRoleMapping(ProjectSettingAuditMixin, models.Model):
    role_ids = ArrayField(models.IntegerField())
    provider = models.OneToOneField(
        "accounts.Provider", on_delete=models.CASCADE
    )


class MeetingTemplate(ProjectSettingAuditMixin, TimeStampedModel):
    INTERNAL_KICKOFF = "Internal Kickoff"
    KICKOFF = "Kickoff"
    STATUS = "Status"

    MEETING_TYPES = (
        (INTERNAL_KICKOFF, INTERNAL_KICKOFF),
        (KICKOFF, KICKOFF),
        (STATUS, STATUS),
    )

    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="kickoff_meeting_templates",
    )
    template_name = models.CharField(max_length=150)
    action_items = ArrayField(models.TextField())
    meeting_type = models.CharField(max_length=100, choices=MEETING_TYPES)
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )
    agenda_topics = JSONField(default=list)

    def __str__(self):
        return f"{self.template_name}"

    class Meta:
        unique_together = ("provider", "template_name")


class AdditionalSetting(ProjectSettingAuditMixin, TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="additional_project_settings",
    )
    default_project_manager = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    project_service_board = models.PositiveSmallIntegerField(null=True)
    service_ticket_complete_status = models.PositiveSmallIntegerField(
        null=True
    )
    project_item_due_date_threshold = models.PositiveSmallIntegerField(
        default=7,
        help_text="Denotes the number before due_date a critical path item or "
        "action item should be counted as pending item.",
    )
    project_completed_item_threshold = models.PositiveSmallIntegerField(
        default=7,
        help_text="Denotes the number after which a completed critical path item, "
        "action item and risk item should be moved to completed item list.",
    )
    default_work_role = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the work role to be "
        "used as default value when adding time entries for project ticket",
    )
    default_work_type = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the work type to be "
        "used as default value when adding time entries for project ticket",
    )
    default_business_unit = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the business unit to be "
        "used as default value when adding time entries for project ticket",
    )
    default_opportunity_type = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the Opportunity type to be used to create opportunity for a change request",
    )
    default_opportunity_stage = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the Opportunity stage to be used to create opportunity for a change request",
    )
    engineer_default_work_role = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the work role to be "
        "used as default value when adding time entries for project ticket for engineers.",
    )
    engineer_default_work_type = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the work type to be "
        "used as default value when adding time entries for project ticket for engineers.",
    )
    engineer_default_business_unit = models.PositiveSmallIntegerField(
        null=True,
        help_text="Id of the business unit to be "
        "used as default value when adding time entries for project ticket for engineers.",
    )
    note_type_ids = ArrayField(models.PositiveIntegerField(), default=list)


class MilestoneStatusSetting(ProjectSettingAuditMixin, TimeStampedModel):
    DONE = "Completed"
    title = models.CharField(
        max_length=100, help_text="Title for this status."
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_milestone_status",
        on_delete=models.CASCADE,
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this status.",
    )

    class Meta:
        unique_together = ("provider", "title")

    def __str__(self):
        text = f"{self.title}"
        return text


class ProjectRateMappingSetting(ProjectSettingAuditMixin, TimeStampedModel):
    role_name = models.CharField(max_length=200)
    role_type = models.ForeignKey(
        "project_management.ProjectRoleType",
        related_name="project_rate_mapping_type",
        on_delete=models.CASCADE,
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="pmo_rate_mapping",
        on_delete=models.CASCADE,
    )
    # store numbers up to approximately one billion with a resolution of 2 decimal places
    hourly_cost = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True
    )
    is_default = models.BooleanField()

    class Meta:
        unique_together = ("provider", "role_name")

    def __str__(self):
        text = f"{self.role_name}"
        return text


class MeetingMailTemplate(ProjectSettingAuditMixin, TimeStampedModel):
    MEETING_TYPES = Meeting.MEETING_TYPES

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="meeting_mail_template",
        on_delete=models.CASCADE,
    )
    meeting_type = models.CharField(max_length=100, choices=MEETING_TYPES)
    email_body_text = models.TextField()
    email_body_markdown = models.TextField()
    email_subject = models.CharField(max_length=500, null=True)
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )

    class Meta:
        unique_together = ("provider", "meeting_type")

    def __str__(self):
        return f"{self.email_body_text[:20]}"


class ChangeRequestEmailTemplate(ProjectSettingAuditMixin, TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    email_body_text = models.TextField()
    email_body_markdown = models.TextField()
    email_subject = models.CharField(max_length=500, null=True)
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )


class ProjectRoleType(ProjectSettingAuditMixin, models.Model):
    role = models.CharField(max_length=200)
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )

    def __str__(self):
        return self.role

    class Meta:
        unique_together = ("provider", "role")


class ProjectAfterHoursMapping(ProjectSettingAuditMixin, models.Model):
    work_type_ids = ArrayField(models.IntegerField())
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )


# Register models for audit logging
common_exclude_fields = ["id", "created_on", "updated_on", "provider"]

auditlog.register(ActivityStatusSetting, exclude_fields=common_exclude_fields)
auditlog.register(AdditionalSetting, exclude_fields=common_exclude_fields)
auditlog.register(
    ChangeRequestEmailTemplate, exclude_fields=common_exclude_fields
)
auditlog.register(CustomerContactRole, exclude_fields=common_exclude_fields)
auditlog.register(DateStatusSetting, exclude_fields=common_exclude_fields)
auditlog.register(EngineerRoleMapping, exclude_fields=common_exclude_fields)
auditlog.register(MeetingMailTemplate, exclude_fields=common_exclude_fields)
auditlog.register(MeetingTemplate, exclude_fields=common_exclude_fields)
auditlog.register(MilestoneStatusSetting, exclude_fields=common_exclude_fields)
auditlog.register(OverallStatusSetting, exclude_fields=common_exclude_fields)
auditlog.register(ProjectBoard, exclude_fields=common_exclude_fields)
auditlog.register(ProjectPhase, exclude_fields=common_exclude_fields)
auditlog.register(ProjectType, exclude_fields=common_exclude_fields)
auditlog.register(TimeStatusSetting, exclude_fields=common_exclude_fields)
