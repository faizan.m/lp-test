import pytz
from django.contrib.postgres.fields import ArrayField
from django.db.models import JSONField
from django.db import models

from auditlog.registry import auditlog


class TimesheetEntrySettings(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    provider = models.OneToOneField(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    default_work_type_id = models.PositiveIntegerField()
    time_period_status = ArrayField(
        base_field=models.CharField(max_length=100)
    )
    default_hours = models.PositiveIntegerField()
    default_business_unit_id = models.PositiveIntegerField()
    default_start_time = models.CharField(default="09:00", max_length=10)
    daily_hours_threshold = models.PositiveIntegerField(default=8)
    weekly_hours_threshold = models.PositiveIntegerField(default=50)
    default_timezone = models.CharField(
        max_length=50, choices=TIMEZONES, default="America/Los_Angeles"
    )


class TimesheetEntryDraft(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    time_period_id = models.CharField(max_length=50)
    user = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    draft = JSONField(default=dict)

    class Meta:
        unique_together = ("provider", "user", "time_period_id")

    def __str__(self):
        return self.time_period_id


# Register models for audit logging
common_exclude_fields = ["id", "created_on", "updated_on", "provider"]

auditlog.register(TimesheetEntrySettings, exclude_fields=common_exclude_fields)
auditlog.register(TimesheetEntryDraft, exclude_fields=common_exclude_fields)
