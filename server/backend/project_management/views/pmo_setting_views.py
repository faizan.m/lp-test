from typing import Dict, List

from django.db.models import Exists, OuterRef, Prefetch, Q, QuerySet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_bulk import BulkCreateAPIView, ListCreateBulkUpdateAPIView

from accounts.models import Provider
from exceptions.exceptions import APIError
from project_management.exceptions import PMOSettingsNotConfigured
from project_management.models import (
    ActivityStatusSetting,
    AdditionalSetting,
    ChangeRequestEmailTemplate,
    CustomerContactRole,
    DateStatusSetting,
    EngineerRoleMapping,
    MeetingTemplate,
    MilestoneStatusSetting,
    OverallStatusSetting,
    Project,
    ProjectBoard,
    ProjectPhase,
    ProjectType,
    TimeStatusSetting,
    MeetingMailTemplate,
    PreSalesEngineerRoleMapping,
    ProjectRoleType,
    ProjectRateMappingSetting,
    ProjectAfterHoursMapping,
)
from project_management.serializers import (
    ActivityStatusSerializer,
    AdditionalSettingSerializer,
    ChangeRequestMailTemplateSerializer,
    CustomerContactRoleSettingSerialzier,
    MeetingMailTemplateSerializer,
    DateStatusSerializer,
    EngineerRoleMappingSerializer,
    MeetingTemplateSerializer,
    MilestoneStatusSerializer,
    OverallStatusSerializer,
    ProjectBoardSerializer,
    ProjectPhaseSerializer,
    ProjectTypeSerializer,
    TimeStatusSerializer,
    PreSalesEngineerRoleMappingSerializer,
    ProjectRoleTypeSettingSerialzier,
    ProjectRateMappingSerializer,
    AfterHoursMappingSerializer,
)
from project_management.services import (
    ProjectERPService,
    ProjectQueryService,
    ProjectService,
)


class TimeStatusListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = TimeStatusSerializer

    def get_queryset(self):
        queryset = TimeStatusSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def perform_create(self, serializer):
        self.get_queryset().delete()
        serializer.save(provider=self.request.provider)


class DateStatusListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = DateStatusSerializer

    def get_queryset(self):
        queryset = DateStatusSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def perform_create(self, serializer):
        self.get_queryset().delete()
        serializer.save(provider=self.request.provider)


class ProjectBoardListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ProjectBoardSerializer

    def get_queryset(self):
        queryset = (
            ProjectBoard.objects.filter(provider=self.request.provider)
            .prefetch_related("statuses")
            .order_by("name")
        )
        return queryset

    def perform_create(self, serializer):
        provider = self.request.provider
        board = serializer.save(provider=provider)
        project_service = ProjectService(provider)
        # add project callback in connectwise for the new board
        try:
            project_service.erp_service.create_project_callback(
                board.connectwise_id
            )
        except APIError as e:
            if e.status_code == 400:
                raise ValidationError(
                    {
                        "connectwise_id": f"Unable to create callback entry for the board {board.connectwise_id}"
                    }
                )
            raise e
        # call project sync task
        project_service.run_project_sync_task()


class ProjectBoardRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ProjectBoardSerializer

    def get_queryset(self):
        queryset = ProjectBoard.objects.filter(
            provider=self.request.provider
        ).prefetch_related("statuses")
        return queryset

    def perform_update(self, serializer):
        provider = self.request.provider
        serializer.save(provider=provider)
        # call project sync task
        ProjectService(provider).run_project_sync_task()


class OverallStatusMixin:
    def get_board_id(self) -> int:
        """
        Extract board id from Url, verify it
        """
        board_id: int = self.kwargs.get("board_id")
        if not board_id:
            raise ValidationError("Invalid board_id")
        provider: Provider = self.request.provider
        get_object_or_404(
            ProjectBoard.objects.all(), provider_id=provider.id, id=board_id
        )
        return board_id

    def get_serializer_context(self):
        context = super().get_serializer_context()
        board_id = self.get_board_id()
        context.update(dict(board_id=board_id))
        return context

    def get_queryset(self):
        board_id = self.get_board_id()
        queryset = OverallStatusSetting.objects.filter(
            provider=self.request.provider, board_id=board_id
        ).order_by("-created_on")
        return queryset


class OverallStatusListCreateAPIView(
    OverallStatusMixin, generics.ListCreateAPIView
):
    serializer_class = OverallStatusSerializer

    def perform_create(self, serializer):
        board_id = self.get_board_id()
        provider = self.request.provider
        serializer.save(provider=provider, board_id=board_id)
        # call project sync task
        ProjectService(provider).run_project_sync_task()


class OverallStatusRetrieveUpdateDestroyAPIView(
    OverallStatusMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = OverallStatusSerializer

    def perform_update(self, serializer):
        board_id = self.get_board_id()
        provider = self.request.provider
        serializer.save(provider=provider, board_id=board_id)
        # call project sync task
        ProjectService(provider).run_project_sync_task()


class ActivityStatusListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ActivityStatusSerializer

    def get_queryset(self):
        queryset = ActivityStatusSetting.objects.filter(
            provider=self.request.provider, project__isnull=True
        )
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class ActivityStatusRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ActivityStatusSerializer

    def get_queryset(self):
        queryset = ActivityStatusSetting.objects.filter(
            provider=self.request.provider, project__isnull=True
        )
        return queryset

    def perform_update(self, serializer):
        instance: ActivityStatusSetting = self.get_object()
        if serializer.validated_data.get(
            "is_default"
        ) and instance.is_default != serializer.validated_data.get(
            "is_default"
        ):
            ActivityStatusSetting.objects.filter(
                provider=self.request.provider
            ).exclude(id=instance.id).update(is_default=False)
        serializer.save(provider=self.request.provider)

    def perform_destroy(self, instance):
        # Prevents deleting of Overdue status
        if instance.title == ActivityStatusSetting.OVERDUE:
            return
        instance.delete()


class ProjectTypeListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ProjectTypeSerializer
    pagination_class = None

    def get_queryset(self):
        in_use = Exists(
            Project.objects.open().filter(
                provider=OuterRef("provider"), type_id=OuterRef("id")
            )
        )
        queryset = (
            ProjectType.objects.filter(provider=self.request.provider)
            .prefetch_related(
                Prefetch(
                    "phases",
                    queryset=ProjectPhase.objects.all().order_by("order"),
                )
            )
            .annotate(in_use=in_use)
            .order_by("-updated_on")
        )
        return queryset

    def perform_create(self, serializer):
        project_type = serializer.save(provider=self.request.provider)
        ProjectQueryService(
            self.request.provider
        ).create_default_project_phases(project_type)


class ProjectTypeRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ProjectTypeSerializer

    def validate_project_type(self):
        provider = self.request.provider
        project_type_id = self.kwargs.get("pk")
        existing_projects_with_type = Project.objects.open().filter(
            provider=provider, type_id=project_type_id
        )
        if existing_projects_with_type.exists():
            raise ValidationError("Type already in use. Can't be deleted.")

    def get_queryset(self):
        queryset = ProjectType.objects.filter(
            provider=self.request.provider
        ).prefetch_related(
            Prefetch(
                "phases", queryset=ProjectPhase.objects.all().order_by("order")
            )
        )
        return queryset

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)

    def perform_destroy(self, instance):
        self.validate_project_type()
        instance.delete()


class ProjectPhaseMixin:
    def validate_project_type(self):
        provider = self.request.provider
        project_type_id = self.kwargs.get("project_type_id")
        existing_projects_with_type = Project.objects.open().filter(
            provider=provider, type_id=project_type_id
        )
        if existing_projects_with_type.exists():
            raise ValidationError(
                "Operation can't be performed. Type is being used in an active project."
            )

    def get_queryset(self) -> QuerySet:
        project_type: int = self.kwargs.get("project_type_id")
        provider: Provider = self.request.provider
        queryset: QuerySet = ProjectPhase.objects.filter(
            project_type_id=project_type, project_type__provider=provider
        ).order_by("order")
        return queryset

    def perform_save(
        self,
        project_type_id: int,
        provider: Provider,
        serializer: ProjectPhaseSerializer,
    ) -> None:
        self.validate_project_type()
        project_type = get_object_or_404(
            ProjectType.objects.filter(provider=provider),
            **{"id": project_type_id},
        )
        serializer.save(project_type=project_type)


class ProjectPhaseListCreateAPIView(
    ProjectPhaseMixin, ListCreateBulkUpdateAPIView, generics.ListCreateAPIView
):
    serializer_class = ProjectPhaseSerializer
    pagination_class = None

    def perform_create(self, serializer):
        project_type_id = self.kwargs.get("project_type_id")
        self.perform_save(project_type_id, self.request.provider, serializer)

    def perform_update(self, serializer):
        project_type_id = self.kwargs.get("project_type_id")
        existing_phase_ids = [
            phase["id"] for phase in serializer.validated_data
        ]
        self.perform_save(project_type_id, self.request.provider, serializer)
        # Handle delete in bulk update
        ProjectPhase.objects.filter(
            Q(project_type_id=project_type_id)
            & Q(project_type__provider_id=self.request.provider)
            & ~Q(id__in=existing_phase_ids)
        ).delete()


class ProjectPhaseRetrieveUpdateDestroyAPIView(
    ProjectPhaseMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ProjectPhaseSerializer

    def perform_update(self, serializer):
        project_type_id = self.kwargs.get("project_type_id")
        self.perform_save(project_type_id, self.request.provider, serializer)

    def perform_destroy(self, instance):
        self.validate_project_type()
        instance.delete()


class CustomerContactRoleListCreateBulkDeleteView(
    BulkCreateAPIView, generics.ListCreateAPIView, generics.DestroyAPIView
):
    serializer_class = CustomerContactRoleSettingSerialzier
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        return CustomerContactRole.objects.filter(provider=provider).order_by(
            "role"
        )

    def perform_create(self, serializer):
        provider = self.request.provider
        serializer.save(provider=provider)

    def destroy(self, request, *args, **kwargs):
        deleted_ids = request.data.get("deleted")
        if not deleted_ids:
            raise ValidationError("Invalid Request")
        self.get_queryset().filter(id__in=deleted_ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProjectRoleListAPIView(APIView):
    def get(self, *args, **kwargs):
        provider = self.request.provider
        roles = ProjectERPService(provider).list_project_roles()
        return Response(roles, status=status.HTTP_200_OK)


class EngineerRoleMappingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = EngineerRoleMappingSerializer

    def get_queryset(self):
        return EngineerRoleMapping.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class PreSalesEngineerRoleMappingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = PreSalesEngineerRoleMappingSerializer

    def get_queryset(self):
        return PreSalesEngineerRoleMapping.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class MeetingTemplateListCreateView(generics.ListCreateAPIView):
    serializer_class = MeetingTemplateSerializer

    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("meeting_type",)

    def get_queryset(self):
        provider = self.request.provider
        queryset = MeetingTemplate.objects.filter(provider=provider)
        return queryset

    def perform_create(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class MeetingTemplateRetrieveUpdateDestroyView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = MeetingTemplateSerializer

    def get_queryset(self):
        provider = self.request.provider
        queryset = MeetingTemplate.objects.filter(provider=provider)
        return queryset

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class ProjectBoardsListView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        boards: List[Dict] = ProjectERPService(provider).list_project_boards()
        return Response(boards, status=status.HTTP_200_OK)


class ServiceBoardsListView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        boards: List[Dict] = provider.erp_client.get_service_boards()
        return Response(boards, status=status.HTTP_200_OK)


class ProjectStatusListView(APIView):
    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        statuses: List[Dict] = ProjectERPService(
            provider
        ).list_project_statuses()
        return Response(statuses, status=status.HTTP_200_OK)


class AdditionalSettingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = AdditionalSettingSerializer

    def get_queryset(self):
        return AdditionalSetting.objects.filter(provider=self.request.provider)

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):
        try:
            AdditionalSetting.objects.get(provider=self.request.provider)
        except AdditionalSetting.DoesNotExist:
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                "Additional Setting already exists for the Provider."
            )

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class MilestoneStatusListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = MilestoneStatusSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = MilestoneStatusSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class MilestoneStatusRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = MilestoneStatusSerializer

    def get_queryset(self):
        queryset = MilestoneStatusSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class ProjectRateMappingListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ProjectRateMappingSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = ProjectRateMappingSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class ProjectRateMappingRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ProjectRateMappingSerializer

    def get_queryset(self):
        queryset = ProjectRateMappingSetting.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class CheckProjectSettingsAPIView(APIView):
    def get(self, request, *args, **kwargs):
        configured, message = ProjectQueryService(
            request.provider
        ).check_project_settings()
        if not configured:
            raise PMOSettingsNotConfigured(message)
        return Response(status=status.HTTP_200_OK)


class MeetingMailTemplateCreateRetrieveUpdateView(
    generics.ListCreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = MeetingMailTemplateSerializer

    def get_queryset(self):
        return MeetingMailTemplate.objects.filter(
            provider=self.request.provider
        )

    def perform_create(self, serializer):

        try:
            MeetingMailTemplate.objects.get(
                provider=self.request.provider,
                meeting_type=serializer.validated_data.get("meeting_type"),
            )
        except MeetingMailTemplate.DoesNotExist:
            serializer.save(
                provider=self.request.provider,
                last_updated_by=self.request.user,
            )
        else:
            raise ValidationError(
                "Meeting Template already exists for the Provider."
            )

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class ChangeRequestMailTemplateCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = ChangeRequestMailTemplateSerializer

    def get_queryset(self):
        return ChangeRequestEmailTemplate.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):

        try:
            ChangeRequestEmailTemplate.objects.get(
                provider=self.request.provider
            )
        except ChangeRequestEmailTemplate.DoesNotExist:
            serializer.save(
                provider=self.request.provider,
                last_updated_by=self.request.user,
            )
        else:
            raise ValidationError(
                "Email Template already exists for the Provider."
            )

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )


class ProjectRoleTypeListCreateBulkDeleteView(
    BulkCreateAPIView, generics.ListCreateAPIView, generics.DestroyAPIView
):
    serializer_class = ProjectRoleTypeSettingSerialzier
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        return ProjectRoleType.objects.filter(provider=provider).order_by(
            "role"
        )

    def perform_create(self, serializer):
        provider = self.request.provider
        serializer.save(provider=provider)

    def destroy(self, request, *args, **kwargs):
        deleted_ids = request.data.get("deleted")
        if not deleted_ids:
            raise ValidationError("Invalid Request")
        self.get_queryset().filter(id__in=deleted_ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AfterHoursMappingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = AfterHoursMappingSerializer

    def get_queryset(self):
        return ProjectAfterHoursMapping.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_update(self, serializer):
        serializer.save(
            provider=self.request.provider, last_updated_by=self.request.user
        )