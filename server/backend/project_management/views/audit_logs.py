from rest_framework import generics
from rest_framework.exceptions import NotFound

from auditlog.models import LogEntry
from auditlog.serializers import LogEntrySerializer
from project_management.models import Project


class ProjectAuditLogsListAPIView(generics.ListAPIView):
    serializer_class = LogEntrySerializer

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        if not Project.objects.filter(
            provider=provider, id=project_id
        ).exists():
            raise NotFound(f"Project {project_id} not found.")
        queryset = (
            LogEntry.objects.get_for_app("project_management")
            .select_related("content_type", "actor")
            .filter(additional_data__project_id=project_id, provider=provider)
        )
        return queryset


class ProjectSettingsAuditLogsListAPIView(generics.ListAPIView):
    serializer_class = LogEntrySerializer

    def get_queryset(self):
        provider = self.request.provider
        queryset = (
            LogEntry.objects.get_for_app("project_management")
            .select_related("content_type", "actor")
            .filter(additional_data__pmo_setting=True, provider=provider)
        )
        return queryset
