from datetime import datetime, timedelta

from django.db import IntegrityError
from django.db.models import Q, F
from django.utils import timezone
from django.utils.timezone import get_current_timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework import generics
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import Provider, User
from project_management.filterset import ProjectFilterSet
from project_management.models import (
    ActionItem,
    CriticalPathItem,
    CustomerContactRole,
    Project,
    ProjectActivePhase,
    ProjectContactRole,
    ProjectItemsArchive,
    ProjectNote,
    RiskItem,
    AdditionalSetting,
    AdditionalContacts,
    ProjectEngineersMapping,
    ProjectCWNote,
)
from project_management.project_management_utils import remove_html_tags
from project_management.serializers import (
    ActionItemSerializer,
    ActivityStatusSerializer,
    CriticalPathItemSerializer,
    ProjectActivePhaseListSerializer,
    ProjectActivePhaseUpdateSerializer,
    ProjectCustomerContactCreateSerializer,
    ProjectCustomerContactSerializer,
    ProjectCustomerUpdateContactSerializer,
    ProjectDashboardSerializer,
    ProjectEngineerSerializer,
    ProjectItemsArchiveSerializer,
    ProjectListRetrieveSerializer,
    ProjectNoteSerializer,
    ProjectUpdateViewSerializer,
    RiskItemSerializer,
    ProjectAdditionalContactsSerializer,
    SowLinkedClosedProjectsSerialzer,
)
from project_management.services import (
    ProjectERPService,
    ProjectQueryService,
    ProjectService,
)
from service_requests.mixins import ServiceRequestModelMixin
from utils import get_app_logger

logger = get_app_logger(__name__)


class ProjectListAPIView(generics.ListAPIView):
    serializer_class = ProjectListRetrieveSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "title",
        "type__title",
        "customer__name",
        "project_manager__first_name",
        "project_manager__last_name",
        "overall_status__title",
    ]
    ordering_fields = [
        "title",
        "created_on",
        "estimated_end_date",
        "project_manager__first_name",
        "overall_status__title",
        "type__title",
        "customer__name",
        "last_action_date",
        "time_status_value",
        "date_status_value_diff",
        "action_status_color",
    ]

    filterset_class = ProjectFilterSet

    def get_queryset(self):
        status_filter = self.request.query_params.get("status")

        queryset = ProjectQueryService(
            self.request.provider
        ).get_listing_api_queryset(status_filter)
        return queryset


class ProjectRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = ProjectListRetrieveSerializer

    def get_serializer_class(self):
        if self.request.method in ["PUT", "PATCH"]:
            return ProjectUpdateViewSerializer
        else:
            return ProjectListRetrieveSerializer

    def get_queryset(self):
        queryset = ProjectQueryService(
            self.request.provider
        ).get_listing_api_queryset(status_filter="all")
        return queryset

    def perform_update(self, serializer):
        provider = self.request.provider
        instance = self.get_object()
        ProjectQueryService(provider).update_project(
            instance, serializer.validated_data, updated_by=self.request.user
        )


class ClosedProjectsListAPIView(generics.ListAPIView):
    serializer_class = SowLinkedClosedProjectsSerialzer
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)

    filterset_class = ProjectFilterSet

    def get_queryset(self):
        status_filter = self.request.query_params.get("status")
        selected_fields = ["id", "title"]

        queryset = ProjectQueryService(
            self.request.provider
        ).get_listing_api_queryset(
            status_filter=status_filter,
            selected_fields=selected_fields,
            select_related_fields=False,
        )
        queryset = queryset.filter(sow_document__isnull=False)
        return queryset


class ProjectActivePhaseListAPIView(generics.ListAPIView):
    serializer_class = ProjectActivePhaseListSerializer
    pagination_class = None

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = ProjectQueryService(
            provider
        ).get_project_active_phase_queryset(project_id)
        return queryset


class ProjectActiveStateUpdateView(generics.UpdateAPIView):
    serializer_class = ProjectActivePhaseUpdateSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = ProjectActivePhase.objects.filter(
            provider=provider, project_id=project_id
        )
        return queryset

    def perform_update(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        serializer.save(provider=provider, project_id=project_id)


class ProjectNoteListCreateView(generics.ListCreateAPIView):
    serializer_class = ProjectNoteSerializer
    pagination_class = None

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = (
            ProjectNote.objects.select_related(
                "author", "author__profile", "meeting"
            )
            .filter(provider=provider, project_id=project_id)
            .only(
                "note",
                "updated_on",
                "author__email",
                "author__first_name",
                "author__last_name",
                "author__profile__profile_pic",
                "meeting_id",
                "meeting__name",
                "project_notes_crm_id",
            )
            .order_by("-updated_on")
        )
        return queryset

    def perform_create(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        author = self.request.user
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        try:
            # remove html tags from the notes
            note = remove_html_tags(self.request.data.get("note"))
            cw_project_notes = provider.erp_client.create_project_notes(
                project.crm_id,
                note,
            )
        except Exception as e:
            logger.info(
                f"Error while creating notes in CW for project {project_id}"
            )
            cw_project_notes = {}
        serializer.save(
            provider=provider,
            project_id=project_id,
            author=author,
            project_notes_crm_id=cw_project_notes.get("note_id"),
        )


class ProjectNoteDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectNoteSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = ProjectNote.objects.filter(
            provider=provider, project_id=project_id, author=self.request.user
        )
        return queryset

    def perform_update(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        author = self.request.user
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        try:
            # remove html tags from the notes
            note = remove_html_tags(self.request.data.get("note"))
            project_notes_crm_id = self.request.data.get(
                "project_notes_crm_id", ""
            )
            cw_project_notes = provider.erp_client.update_project_notes(
                project.crm_id,
                project_notes_crm_id,
                note,
            )
        except Exception as e:
            logger.info(
                f"Error while creating notes in CW for project {project_id}"
            )
            cw_project_notes = {}
        serializer.save(
            provider=provider,
            project_id=project_id,
            author=author,
            project_notes_crm_id=cw_project_notes.get("note_id"),
        )

    def delete(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        author = self.request.user
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        project_notes_crm_id = self.request.data.get(
            "project_notes_crm_id", ""
        )
        cw_project_notes = provider.erp_client.delete_project_note(
            project.crm_id, project_notes_crm_id
        )
        return self.destroy(request, *args, **kwargs)


class ProjectCWNotesListAPIVIew(APIView):
    """
    API View to list connectwise project notes.
    """

    def post(self, request, **kwargs):
        project_id = self.kwargs.get("project_id")
        client = request.provider.erp_client
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=request.provider,
        )
        # remove html tags from the notes
        # note = remove_html_tags(self.request.data.get("note"))
        note = self.request.data.get("note")
        cw_project_notes = client.create_project_notes(project.crm_id, note)
        author = request.user
        # creating entry in Project cw note
        ProjectCWNote.objects.create(
            provider=request.provider,
            project_id=project_id,
            author=author,
            note_crm_id=cw_project_notes.get("note_id"),
        )
        cw_project_notes["author"] = author.name
        cw_project_notes["author_id"] = author.id
        cw_project_notes["author_email"] = author.email
        cw_project_notes["project_id"] = project_id
        cw_project_notes["author_profile_pic"] = author.profile.profile_pic_url
        return Response(data=cw_project_notes, status=status.HTTP_201_CREATED)

    def get(self, request, **kwargs):
        project_id = self.kwargs.get("project_id")
        project_settings = AdditionalSetting.objects.get(
            provider=request.provider
        )

        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            raise NotFound("Project does not exist")

        data = request.provider.erp_client.get_project_notes(
            project.crm_id, project_settings.note_type_ids
        )
        for project_note in data:
            try:
                project_note_data = ProjectCWNote.objects.get(
                    provider=request.provider,
                    note_crm_id=project_note.get("note_id"),
                )
                project_note["updated_on"] = project_note_data.updated_on
                project_note["project_id"] = project_id
                if project_note_data.author_id:
                    project_note["author_id"] = project_note_data.author_id
                    project_note["author"] = project_note_data.author.name
                    project_note[
                        "author_email"
                    ] = project_note_data.author.email
                    project_note[
                        "author_profile_pic"
                    ] = project_note_data.author.profile.profile_pic_url
            except ProjectCWNote.DoesNotExist:
                # create note entry in Acela if created in CW
                ProjectCWNote.objects.create(
                    provider=request.provider,
                    project_id=project_id,
                    note_crm_id=project_note.get("note_id"),
                )
                logger.info(
                    f"project note is not created or synced in Acela, so creating in ProjectCWNote model {project}"
                )

        return Response(data=data, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=request.provider,
        )
        # remove html tags from the notes
        # note = remove_html_tags(self.request.data.get("note"))
        note = self.request.data.get("note")
        project_notes_crm_id = self.request.data.get("note_crm_id")

        cw_project_notes_updated = (
            request.provider.erp_client.update_project_notes(
                project.crm_id,
                project_notes_crm_id,
                note,
            )
        )
        author = request.user
        try:
            # updating entry in Project cw note
            project_cw_note = ProjectCWNote.objects.get(
                provider=request.provider,
                project_id=project_id,
                note_crm_id=cw_project_notes_updated.get("note_id"),
            )
            project_cw_note.author = author
            project_cw_note.save()
        except ProjectCWNote.DoesNotExist:
            ProjectCWNote.objects.create(
                provider=request.provider,
                project_id=project_id,
                note_crm_id=cw_project_notes_updated.get("note_id"),
            )
            logger.info(
                f"project note is not created or synced in Acela, so creating in ProjectCWNote model {project}"
            )
        cw_project_notes_updated["author"] = author.name
        cw_project_notes_updated["author_id"] = author.id
        cw_project_notes_updated["author_email"] = author.email
        cw_project_notes_updated["project_id"] = project_id
        cw_project_notes_updated[
            "author_profile_pic"
        ] = author.profile.profile_pic_url
        return Response(
            data=cw_project_notes_updated, status=status.HTTP_200_OK
        )

    def delete(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        author = self.request.user
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        project_notes_crm_id = self.request.data.get("note_crm_id", "")
        cw_project_notes = provider.erp_client.delete_project_note(
            project.crm_id, project_notes_crm_id
        )
        ProjectCWNote.objects.filter(
            project_id=project_id, note_crm_id=project_notes_crm_id
        ).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProjectItemBaseView:
    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = (
            self.model.objects.select_related(
                "owner", "owner__profile", "status"
            )
            .filter(provider=provider, project_id=project_id)
            .only(
                "description",
                "due_date",
                "status__title",
                "status__color",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "notes",
            )
            .order_by("due_date")
        )
        return queryset

    def save_action(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        updated_on = timezone.now()
        serializer.save(
            project_id=project_id, provider=provider, updated_on=updated_on
        )

    def perform_create(self, serializer):
        self.save_action(serializer)

    def perform_update(self, serializer):
        self.save_action(serializer)


class CriticalPathItemListCreateView(
    ProjectItemBaseView, generics.ListCreateAPIView
):
    serializer_class = CriticalPathItemSerializer
    model = CriticalPathItem
    pagination_class = None


class CriticalPathItemUpdateDestroyView(
    ProjectItemBaseView, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = CriticalPathItemSerializer
    model = CriticalPathItem


class ActionItemListCreateView(
    ProjectItemBaseView, generics.ListCreateAPIView
):
    serializer_class = ActionItemSerializer
    model = ActionItem
    pagination_class = None


class ActionItemUpdateDestroyView(
    ProjectItemBaseView, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ActionItemSerializer
    model = ActionItem


class RiskItemListCreateView(ProjectItemBaseView, generics.ListCreateAPIView):
    serializer_class = RiskItemSerializer
    pagination_class = None

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = (
            RiskItem.objects.select_related("owner", "owner__profile")
            .filter(provider=provider, project_id=project_id)
            .only(
                "description",
                "owner__first_name",
                "owner__last_name",
                "owner__profile__profile_pic",
                "impact",
                "notes",
            )
            .order_by("-impact")
        )
        return queryset


class RiskItemUpdateDestroyView(
    ProjectItemBaseView, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = RiskItemSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = RiskItem.objects.filter(
            provider=provider, project_id=project_id
        )
        return queryset


class RiskItemImpactChoicesListView(APIView):
    def get(self, request, *args, **kwargs):
        impact_choices = []
        for impact in RiskItem.RISK_IMPACTS:
            impact_choices.append(
                {
                    "id": impact.numeric_value,
                    "name": impact.title,
                    "color": impact.color,
                }
            )

        return Response(data=impact_choices, status=status.HTTP_200_OK)


class ProjectActivityStatusListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ActivityStatusSerializer
    pagination_class = None

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = ProjectQueryService(provider).get_project_activity_statuses(
            project_id
        )
        return queryset

    def perform_create(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        serializer.save(provider=provider, project_id=project_id)


class ProjectDashboardView(generics.ListAPIView):
    serializer_class = ProjectDashboardSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "title",
        "type__title",
        "customer__name",
        "project_manager__first_name",
        "project_manager__last_name",
        "overall_status__title",
    ]

    filterset_class = ProjectFilterSet

    def get_queryset(self):
        provider = self.request.provider
        status_filter = self.request.query_params.get("status", "all")
        queryset = ProjectQueryService(provider).get_project_dashboard_query(
            status_filter
        )
        return queryset


class ProjectEngineerListView(generics.ListAPIView):
    serializer_class = ProjectEngineerSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        engineers = ProjectService(provider).get_project_engineers(
            project.crm_id
        )
        return engineers


class ProjectPreSalesEngineerListView(generics.ListAPIView):
    serializer_class = ProjectEngineerSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        engineers = ProjectService(provider).get_project_engineers(
            project.crm_id, pre_sales_engineer=True
        )
        return engineers


class ProjectTeamMemberListAPIView(generics.ListAPIView):
    serializer_class = ProjectEngineerSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        team_members = ProjectERPService(provider).get_project_team_members(
            project.crm_id
        )
        member_ids = [
            user.get("member", {}).get("id") for user in team_members
        ]
        queryset = ProjectQueryService(
            provider
        ).get_provider_user_by_member_id(member_ids)
        return queryset


class ProjectCustomerContactListView(generics.ListCreateAPIView):
    serializer_class = ProjectCustomerContactSerializer
    pagination_class = None

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ProjectCustomerContactSerializer
        return ProjectCustomerContactCreateSerializer

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all().only("crm_id"),
            id=project_id,
            provider=provider,
        )
        customer_contacts_crm_ids = ProjectService(
            provider
        ).get_project_customer_contacts(project.crm_id, project.id)
        return customer_contacts_crm_ids

    def perform_create(self, serializer):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )
        try:
            serializer.save(project_id=project_id)
        except IntegrityError as e:
            logger.info(e)
        # add contact on connectwise
        ProjectERPService(provider).create_project_customer_contacts(
            project.crm_id, serializer.validated_data.get("contact_crm_id")
        )


class ProjectCustomerContactRetrieveUpdateDestroyView(APIView):
    def put(self, request, *args, **kwargs):
        project_id = kwargs.get("project_id")
        contact_crm_id = kwargs.get("contact_crm_id")

        serializer = ProjectCustomerUpdateContactSerializer(data=request.data)

        if serializer.is_valid():
            try:
                contact_role = ProjectContactRole.objects.get(
                    project_id=project_id,
                    contact_crm_id=serializer.validated_data["contact_crm_id"],
                )

                contact_role.role = CustomerContactRole.objects.filter(
                    id=serializer.validated_data["role_id"]
                ).first()

                if contact_role.user is None:
                    user = User.objects.filter(
                        provider_id=request.provider.id,
                        crm_id=serializer.validated_data["contact_crm_id"],
                    ).first()
                    contact_role.user = user

                contact_role.save()

            except ProjectContactRole.DoesNotExist:
                # create a record.
                ProjectContactRole.objects.create(
                    project_id=project_id,
                    contact_crm_id=serializer.validated_data["contact_crm_id"],
                    role_id=serializer.validated_data["role_id"],
                    user=User.objects.filter(
                        provider_id=request.provider.id,
                        crm_id=serializer.validated_data["contact_crm_id"],
                    ).first(),
                )

            return Response(status=status.HTTP_200_OK)

        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    def post(self, request, *args, **kwargs):
        project_id = kwargs.get("project_id")
        contact_record_id = request.data.get("contact_record_id", None)
        contact_crm_id = kwargs.get("contact_crm_id")
        provider = self.request.provider

        if contact_record_id is None:
            raise ValidationError(
                {"contact_record_id": "Contact record id is missing."}
            )

        project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )

        # delete from DB if present
        contact_role = ProjectContactRole.objects.filter(
            project_id=project_id, contact_crm_id=contact_crm_id
        )
        contact_role.delete()

        # delete from connectwise.
        ProjectERPService(provider).delete_project_customer_contacts(
            project.crm_id, contact_record_id
        )

        return Response(status=status.HTTP_204_NO_CONTENT)


class ProjectTicketsListView(APIView):
    def get(self, *args, **kwargs):
        project_crm_id = self.kwargs.get("project_crm_id")
        provider = self.request.provider
        tickets_generator = ProjectERPService(provider).get_project_tickets(
            project_crm_id
        )
        tickets = []
        for res in tickets_generator:
            tickets.extend(res.json())
        return Response(data=tickets, status=status.HTTP_200_OK)


class ProjectManagersListView(generics.ListAPIView):
    serializer_class = ProjectEngineerSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        # project_id = self.kwargs.get("project_id")

        pm_ids = (
            Project.objects.filter(provider=provider)
            .only("project_manager")
            .values_list("project_manager__id", flat=True)
        )

        queryset = User.default_manager.filter(
            provider=provider, id__in=pm_ids
        ).only("id", "first_name", "last_name")

        return queryset


class ProjectEngineersListView(generics.ListAPIView):
    serializer_class = ProjectEngineerSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider

        member_ids = (
            ProjectEngineersMapping.objects.filter(provider=provider)
            .only("member")
            .values_list("member__id", flat=True)
        )
        queryset = User.default_manager.filter(
            provider=provider, id__in=member_ids
        ).only("id", "first_name", "last_name")

        return queryset


class ProjectDocumentListCreateView(APIView):
    def get(self, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )
        project_documents = ProjectERPService(provider).get_project_documents(
            project.crm_id
        )

        return Response(data=project_documents, status=status.HTTP_200_OK)

    def post(self, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )

        file_path = self.request.data.get("file_path", None)

        if file_path is None:
            raise ValidationError({"file": "File is missing."})

        payload = {
            "file": file_path,
            "record_type": "Project",
            "record_id": project.crm_id,
        }

        response = ProjectERPService(provider).upload_project_documents(
            payload
        )

        return Response(data=response, status=status.HTTP_200_OK)


class ProjectAccountManagerRetrieveView(APIView):
    def get(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        account_manager = ProjectService(provider).get_project_account_manager(
            project_id
        )
        if not account_manager:
            data = {}
        else:
            serializer = ProjectEngineerSerializer(account_manager)
            data = serializer.data
        return Response(data, status=status.HTTP_200_OK)


class ProjectItemsArchiveListView(generics.ListAPIView):
    serializer_class = ProjectItemsArchiveSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        provider = self.request.provider
        queryset = ProjectItemsArchive.objects.filter(
            provider=provider, project_id=project_id
        )
        return queryset


class WorkRoleListAPIView(APIView):
    def get(self, *args, **kwargs):
        provider: Provider = self.request.provider
        work_roles = provider.erp_client.get_work_roles()
        return Response(data=work_roles, status=status.HTTP_200_OK)


class WorkTypeListAPIView(APIView):
    def get(self, *args, **kwargs):
        provider: Provider = self.request.provider
        work_types = provider.erp_client.get_work_types()
        return Response(data=work_types, status=status.HTTP_200_OK)


class ProjectMetricsAPIView(APIView):
    """
    APIView to get the count of various graph parameters for project dashboard
    """

    def get(self, *args, **kwargs):
        filter_conditions = dict()
        status_filter = self.request.query_params.get("status")
        project_engineer_or_manager = self.request.query_params.get(
            "project_engineer_or_manager", None
        )
        estimated_end_date_after = datetime.now(tz=get_current_timezone())
        estimated_end_date_before = estimated_end_date_after + timedelta(
            days=7
        )
        if project_engineer_or_manager:
            filter_conditions = Q()
            filter_conditions_dict = {}
            project_engineer_or_manager = project_engineer_or_manager.strip(
                ","
            )
            filter_conditions_dict.update(
                {
                    "engineers__member_id": project_engineer_or_manager,
                    "project_manager": project_engineer_or_manager,
                }
            )
            # Applying the OR filter on dictionary arguments in an empty Q() object
            for _filter in filter_conditions_dict:
                filter_conditions |= Q(
                    **{_filter: filter_conditions_dict[_filter]}
                )

        # Using queryset used for Dashboard listing
        dashboard_queryset = ProjectQueryService(
            self.request.provider
        ).get_listing_api_queryset(status_filter, filter_conditions)

        projects_closing_this_week = dashboard_queryset.filter(
            estimated_end_date__gte=estimated_end_date_after,
            estimated_end_date__lte=estimated_end_date_before,
        )

        projects_with_overdue_action_items_or_overdue_critical_path_items = (
            dashboard_queryset.filter(
                Q(overdue_action_items__isnull=False)
                | Q(overdue_critical_path_items__isnull=False)
            )
        )

        overbudget_projects = dashboard_queryset.filter(
            actual_hours__gt=F("hours_budget")
        )

        return Response(
            {
                "total_projects": dashboard_queryset.count(),
                "projects_closing_this_week": projects_closing_this_week.count(),
                "projects_with_overdue_action_items_or_overdue_critical_path_items": (
                    projects_with_overdue_action_items_or_overdue_critical_path_items.count()
                ),
                "overbudget_projects": overbudget_projects.count(),
            },
            status=status.HTTP_200_OK,
        )


class ProjectAdditionalContactsListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ProjectAdditionalContactsSerializer

    def get_queryset(self, **kwargs):
        project_id = self.kwargs.get("project_id")
        queryset = AdditionalContacts.objects.filter(
            provider=self.request.provider, project_id=project_id
        )
        return queryset

    def perform_create(self, serializer):
        project_id = self.kwargs.get("project_id")
        serializer.save(provider=self.request.provider, project_id=project_id)


class ProjectAdditionalContactsRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ProjectAdditionalContactsSerializer

    def get_object(self):
        project_id = self.kwargs.get("project_id")
        additional_contact_id = self.kwargs.get("pk")
        try:
            additional_contact = AdditionalContacts.objects.filter(
                provider=self.request.provider, project_id=project_id
            ).get(id=additional_contact_id)
            return additional_contact
        except:
            raise NotFound()


class ProjectDocumentsDeleteAPIView(generics.DestroyAPIView):
    """
    API view to delete project document
    """

    serializer_class = None
    pagination_class = None

    def delete(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        project_id: int = self.kwargs.get("project_id")
        document_crm_id: int = self.kwargs.get("doc_id")
        project: Project = get_object_or_404(
            Project.objects.all(), id=project_id
        )
        response = ProjectERPService(provider).delete_project_documents(
            document_crm_id
        )
        if response.status_code == 204:
            return Response(
                data={"Successfully deleted the document."},
                status=status.HTTP_204_NO_CONTENT,
            )
        else:
            return Response(response)
