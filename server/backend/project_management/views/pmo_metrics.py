from datetime import timedelta

from django.db.models import Case, When, Q, CharField, QuerySet
from django.db.models import Count, F, Value
from django.db.models.functions import Concat, Cast
from django.utils import timezone
from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from project_management.filterset import ProjectFilterSet
from project_management.models import (
    Project,
)
from project_management.models.pmo_metrics import PMOMetricsNotes
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from accounts.custom_permissions import IsProviderUser
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework.backends import DjangoFilterBackend
from project_management.services.project_service import ProjectQueryService
from project_management.serializers.pmo_metrics_serializers import (
    PMOMetricsTableDataSerializer,
    PMOMetricsBudgetReviewTabDataSerializer,
    PMOMetricsManagerProjectsCountChartSerailizer,
    PMOMetricsProjectTypeProjectsCountChartSerializer,
    PMOMetricsOverallStatusProjectsCountChartSerializer,
    PMOMetricsProjectsCounByEstimatedEndDateRangeChartSerializer,
    PMOMetricsNotesTabDataSerializer,
    PMOMetricsNotesCreateSerializer,
    PMOMetricsNotesUpdateSerializer,
)
import json
from rest_framework.response import Response
from rest_framework import status
from project_management.filterset import (
    ProjectFilterSet,
    PMOMetricsNotesFilterset,
)
from django.db.models import Count, F, Q, Case, Value, CharField
from accounts.models import User
from django.db.models.functions import Concat
from django.utils import timezone
from datetime import timedelta
from django.db.models import Case, When, Q, CharField
from rest_framework.exceptions import NotFound, PermissionDenied


class PMOMetricsReviewTabDataAPIView(ListAPIView):
    """
    API view to list projects data under review tab.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PMOMetricsTableDataSerializer
    filter_backends = (
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "title",
        "customer__name",
        "project_manager__first_name",
        "project_manager__last_name",
        "overall_status__title",
    )
    ordering_fields = (
        "title",
        "progress_percent",
        "estimated_end_date",
        "status",
        "project_manager_name",
        "estimated_hours_to_complete",
        "customer_name",
    )
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        provider: Provider = self.request.provider
        project_query_service: ProjectQueryService = ProjectQueryService(
            provider
        )
        progress_percent_query = (
            project_query_service.get_project_progress_expression()
        )
        estimated_hours_to_complete_query = (
            project_query_service.get_estimated_hours_to_complete_query()
        )
        projects: "QuerySet[Project]" = (
            Project.objects.open()
            .filter(provider_id=provider.id)
            .select_related(
                "customer",
                "project_manager",
                "project_manager__profile",
                "type",
                "overall_status",
            )
            .only(
                "id",
                "title",
                "crm_id",
                "provider",
                "customer",
                "project_manager",
                "type",
                "estimated_end_date",
                "overall_status",
                "project_updates",
                "estimated_end_date",
                "closed_date",
                "sow_document",
                "opportunity_crm_ids",
            )
            .annotate(
                progress_percent=progress_percent_query,
                status=F("overall_status__title"),
                customer_name=F("customer__name"),
                project_manager_name=Concat(
                    F("project_manager__first_name"),
                    Value(" "),
                    F("project_manager__last_name"),
                    output_field=CharField(),
                ),
                estimated_hours_to_complete=estimated_hours_to_complete_query,
            )
            .order_by("-updated_on")
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        return filtered_projects


class PMOMetricsBudgetReviewTabAPIView(ListAPIView):
    """
    API view for Budget review tab in PMO metrics.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PMOMetricsBudgetReviewTabDataSerializer
    filter_backends = (
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "title",
        "customer__name",
        "project_manager__first_name",
        "project_manager__last_name",
        "overall_status__title",
    )
    ordering_fields = (
        "title",
        "hours_budget",
        "actual_hours",
        "remaining_hours",
        "percent_of_budget",
        "customer_name",
        "status",
        "project_manager_name",
    )
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        provider: Provider = self.request.provider
        project_query_service: ProjectQueryService = ProjectQueryService(
            provider
        )
        percent_of_budget_query = (
            project_query_service.get_percent_of_budget_expression()
        )
        remaining_hours_query = (
            project_query_service.get_remaining_hours_expression()
        )
        projects: "QuerySet[Project]" = (
            Project.objects.open()
            .filter(provider_id=provider.id)
            .select_related(
                "customer", "project_manager", "overall_status", "type"
            )
            .only(
                "id",
                "title",
                "crm_id",
                "provider",
                "customer",
                "project_manager",
                "overall_status",
                "actual_hours",
                "hours_budget",
                "type",
                "estimated_end_date",
            )
            .annotate(
                percent_of_budget=percent_of_budget_query,
                remaining_hours=remaining_hours_query,
                customer_name=F("customer__name"),
                status=F("overall_status__title"),
                project_manager_name=Concat(
                    F("project_manager__first_name"),
                    Value(" "),
                    F("project_manager__last_name"),
                    output_field=CharField(),
                ),
            )
            .filter(remaining_hours__lt=10)
            .order_by("-updated_on")
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        return filtered_projects


class PMOMetricsManagerProjectsCountChartAPIView(ListAPIView):
    """
    API view for projects count by project manager chart.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PMOMetricsManagerProjectsCountChartSerailizer
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        projects: "QuerySet[Project]" = (
            Project.objects.open()
            .select_related("project_manager")
            .filter(provider_id=self.request.provider.id)
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        queryset = (
            filtered_projects.annotate(
                project_manager_name=Concat(
                    F("project_manager__first_name"),
                    Value(" "),
                    F("project_manager__last_name"),
                    output_field=CharField(),
                )
            )
            .values("project_manager_name", "project_manager_id")
            .annotate(count=Count("*"))
            .order_by("-count")
        )
        return queryset


class PMOMetricsProjectsCountByTypeChartAPIView(ListAPIView):
    """
    API view for projects count by project type
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PMOMetricsProjectTypeProjectsCountChartSerializer
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        projects: "QuerySet[Project]" = (
            Project.objects.open()
            .select_related("type")
            .filter(provider_id=self.request.provider.id)
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        queryset = (
            filtered_projects.annotate(
                project_type=F("type__title"),
                project_type_id=F("type_id"),
            )
            .values("project_type", "project_type_id")
            .annotate(count=Count("*"))
            .order_by("-count")
        )
        return queryset


class PMOMetricsOverallStatusProjectsCountChartAPIView(ListAPIView):
    """
    API view for projects count by project status chart.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PMOMetricsOverallStatusProjectsCountChartSerializer
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        projects: "QuerySet[Project]" = (
            Project.objects.open()
            .select_related(
                "overall_status",
            )
            .filter(provider_id=self.request.provider.id)
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        queryset = (
            filtered_projects.annotate(
                project_status=F("overall_status__title"),
                project_status_id=F("overall_status_id"),
                project_status_color=F("overall_status__color"),
            )
            .values(
                "project_status", "project_status_id", "project_status_color"
            )
            .annotate(count=Count("*"))
            .order_by("-count")
        )
        return queryset


class PMOProjectCountByEstimatedEndDateRangeChartAPIView(ListAPIView):
    """
    API view for proejct count by estimated end date range chart.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = (
        PMOMetricsProjectsCounByEstimatedEndDateRangeChartSerializer
    )
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ProjectFilterSet

    def get_queryset(self):
        today = timezone.now().date()
        # ISO numbering is used for weekday, so Monday = 1 and Sunday = 7
        this_week_monday = today - timedelta(days=today.isoweekday() - 1)
        this_week_sunday = today + timedelta(days=7 - today.isoweekday())
        last_week_monday = this_week_monday - timedelta(days=7)
        last_week_sunday = this_week_sunday - timedelta(days=7)
        next_week_monday = this_week_monday + timedelta(days=7)
        next_week_sunday = this_week_sunday + timedelta(days=7)
        past_30_days_before_last_week_monday = last_week_monday - timedelta(
            days=30
        )
        next_30_days_after_next_week_sunday = next_week_sunday + timedelta(
            days=30
        )

        projects: "QuerySet[Project]" = Project.objects.open().filter(
            provider_id=self.request.provider.id
        )
        filtered_projects: "QuerySet[Project]" = super().filter_queryset(
            projects
        )
        queryset = (
            filtered_projects.annotate(
                days_range=Case(
                    When(
                        Q(
                            estimated_end_date__date__gte=past_30_days_before_last_week_monday
                        )
                        & Q(estimated_end_date__date__lt=last_week_monday),
                        then=Value(
                            "Projects closed in last 30 days before last week"
                        ),
                    ),
                    When(
                        Q(estimated_end_date__date__gte=last_week_monday)
                        & Q(estimated_end_date__date__lte=last_week_sunday),
                        then=Value("Projects closed last week"),
                    ),
                    When(
                        Q(estimated_end_date__date__gte=this_week_monday)
                        & Q(estimated_end_date__date__lte=this_week_sunday),
                        then=Value("Projects closing this week"),
                    ),
                    When(
                        Q(estimated_end_date__date__gte=next_week_monday)
                        & Q(estimated_end_date__date__lte=next_week_sunday),
                        then=Value("Projects closing next week"),
                    ),
                    When(
                        Q(estimated_end_date__date__gt=next_week_sunday)
                        & Q(
                            estimated_end_date__date__lte=next_30_days_after_next_week_sunday
                        ),
                        then=Value(
                            "Projects closing in 30 days after next week"
                        ),
                    ),
                    default=None,
                    output_field=CharField(),
                )
            )
            .filter(days_range__isnull=False)
            .values("days_range")
            .annotate(count=Count("days_range"))
            .order_by("-count")
        )
        return queryset


class PMOMetricsNotesListCreateAPIView(ListCreateAPIView):
    """
    API view to list create PMO metrics notes.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    search_fields = ("author__first_name", "author__last_name", "note")
    ordering_fields = (
        "created_on",
        "updated_on",
    )
    filterset_class = PMOMetricsNotesFilterset

    def get_queryset(self):
        filter_params: Dict = dict(
            provider_id=self.request.provider.id, is_archived=False
        )
        is_archived: bool = json.loads(
            self.request.query_params.get("archived", "false")
        )
        if is_archived:
            filter_params.update(is_archived=True)
        return (
            PMOMetricsNotes.objects.select_related("author", "author__profile")
            .filter(**filter_params)
            .order_by("-updated_on")
        )

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            note = PMOMetricsNotes.objects.create(
                provider=self.request.provider,
                author=self.request.user,
                **serializer.validated_data
            )
            return Response(
                data=PMOMetricsNotesTabDataSerializer(note).data,
                status=status.HTTP_201_CREATED,
            )

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == "POST":
            return PMOMetricsNotesCreateSerializer
        else:
            return PMOMetricsNotesTabDataSerializer


class PMOMetricsNotesRetrieveUpdateDestroyAPIView(
    RetrieveUpdateDestroyAPIView
):
    """
    API view to retrieve, updated, destroy PMO metric notes.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def user_is_author(self, author_id: str):
        return True if str(self.request.user.id) == author_id else False

    def get_object(self):
        note_id: int = self.kwargs.get("note_id")
        try:
            note: PMOMetricsNotes = PMOMetricsNotes.objects.get(
                provider=self.request.provider, id=note_id
            )
        except PMOMetricsNotes.DoesNotExist:
            raise NotFound("Note does not exist!")
        return note

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            note_instance = self.get_object()
            if self.user_is_author(str(note_instance.author_id)):
                note = serializer.validated_data.get("note", None)
                is_archived = serializer.validated_data.get(
                    "is_archived", False
                )
                if note:
                    note_instance.note = note
                if is_archived:
                    note_instance.is_archived = is_archived
                note_instance.save()
                return Response(
                    data=PMOMetricsNotesTabDataSerializer(note_instance).data,
                    status=status.HTTP_200_OK,
                )
            else:
                raise PermissionDenied(
                    "Only original author can update the note!"
                )

    def perform_destroy(self, instance):
        if self.user_is_author(str(instance.author_id)):
            instance.delete()
        else:
            raise PermissionDenied("Only original author can delete the note!")

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == "PUT":
            return PMOMetricsNotesUpdateSerializer
        else:
            return PMOMetricsNotesTabDataSerializer
