import copy
import base64
import json

from django_downloadview import HTTPDownloadView
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import Profile
from project_management.exceptions import (
    PMOSettingsNotConfigured,
    ProjectSettingException,
)
from project_management.models import (
    CustomerTouchMeeting,
    KickoffMeeting,
    Meeting,
    MeetingArchive,
    MeetingAttendee,
    Project,
    ProjectNote,
    StatusMeeting,
    CloseOutMeeting,
    MeetingDocument,
    ExternalAttendee,
    ProjectItemsArchive,
)
from project_management.serializers import ProjectNoteSerializer
from project_management.serializers.meeting_serilalizer import (
    CustomerTouchMeetingReadSerializer,
    CustomerTouchMeetingWriteSerializer,
    KickoffMeetingReadSerializer,
    KickoffMeetingWriteSerializer,
    MeetingAttendeeSerializer,
    MeetingBaseSerializer,
    MeetingCompleteSerializer,
    MeetingUpdateSerializer,
    StatusMeetingReadSerializer,
    StatusMeetingWriteSerializer,
    CloseOutMeetingReadSerializer,
    CloseOutMeetingWriteSerializer,
    MeetingDocumentSerializer,
    ExternalAttendeeSerializer,
)
from project_management.services import ProjectQueryService
from project_management.services.meeting_service import MeetingService
from project_management.tasks.meeting_emails import (
    generate_kickoff_meeting_pdf,
    generate_status_meeting_pdf,
)


class MeetingArchiveCheckMixin:
    def check_if_meeting_archived(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id") or self.kwargs.get("pk")
        is_archived = MeetingArchive.objects.filter(
            provider=provider, project_id=project_id, meeting_id=meeting_id
        ).exists()
        return is_archived

    def perform_destroy(self, instance):
        is_archived = self.check_if_meeting_archived()
        if is_archived:
            raise ValidationError("Archived Meeting can not be deleted.")
        instance.delete()


class ProjectMeetingListView(generics.ListAPIView):
    serializer_class = MeetingBaseSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        queryset = ProjectQueryService(provider).get_meeting_list_query(
            project_id
        )
        return queryset


class ProjectMeetingUpdateDeleteView(
    MeetingArchiveCheckMixin, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = MeetingUpdateSerializer

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        queryset = ProjectQueryService(provider).get_meeting_list_query(
            project_id
        )
        return queryset


class MeetingCreateAPIView(generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        meeting_object = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        result = {**serializer.data, **{"id": meeting_object.meeting.id}}
        return Response(
            result, status=status.HTTP_201_CREATED, headers=headers
        )


class StatusMeetingCreateView(MeetingCreateAPIView):
    serializer_class = StatusMeetingWriteSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        return ProjectQueryService(provider).create_status_meeting(
            project_id, copy.deepcopy(serializer.validated_data)
        )


class TypedMeetingMixin(MeetingArchiveCheckMixin):
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response()

    def get_object(self):
        """
        We will be using meeting id which is related through one-to-one mapping to get the
        specific type meeting. This logic to get_object will be same for all type of meetings.
        """

        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        queryset = self.model.objects.all()
        filter_kwargs = {
            "meeting__provider": provider,
            "meeting__project_id": project_id,
            "meeting_id": meeting_id,
        }
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj


class StatusMeetingRetrieveUpdateDestroyView(
    TypedMeetingMixin, generics.RetrieveUpdateDestroyAPIView
):
    model = StatusMeeting

    def get_serializer_class(self):
        if self.request.method == "GET":
            return StatusMeetingReadSerializer
        return StatusMeetingWriteSerializer

    def perform_update(self, serializer):
        provider = self.request.provider
        status_meeting = self.get_object()
        ProjectQueryService(provider).update_status_meeting(
            status_meeting, copy.deepcopy(serializer.validated_data)
        )


class KickoffMeetingCreateView(MeetingCreateAPIView):
    serializer_class = KickoffMeetingWriteSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        return ProjectQueryService(provider).create_kickoff_meeting(
            project_id, copy.deepcopy(serializer.validated_data)
        )


class KickoffMeetingRetrieveUpdateDestroyView(
    TypedMeetingMixin, generics.RetrieveUpdateDestroyAPIView
):
    model = KickoffMeeting

    def get_serializer_class(self):
        if self.request.method == "GET":
            return KickoffMeetingReadSerializer
        return KickoffMeetingWriteSerializer

    def perform_update(self, serializer):
        provider = self.request.provider
        status_meeting = self.get_object()
        ProjectQueryService(provider).update_kickoff_meeting(
            status_meeting, copy.deepcopy(serializer.validated_data)
        )


class CustomerTouchMeetingCreateView(MeetingCreateAPIView):
    serializer_class = CustomerTouchMeetingWriteSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        return ProjectQueryService(provider).create_customer_touch_meeting(
            project_id, copy.deepcopy(serializer.validated_data)
        )


class CustomerTouchMeetingRetrieveUpdateDestroyView(
    TypedMeetingMixin, generics.RetrieveUpdateDestroyAPIView
):
    model = CustomerTouchMeeting

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CustomerTouchMeetingReadSerializer
        return CustomerTouchMeetingWriteSerializer

    def perform_update(self, serializer):
        provider = self.request.provider
        status_meeting = self.get_object()
        ProjectQueryService(provider).update_customer_touch_meeting(
            status_meeting, serializer.validated_data
        )


class MeetingTimeEntryMixin:
    def _get_engineer_time_entry_payload(self, serializer, customer_crm_id):
        engineer_time_entries_payload = serializer.validated_data.get(
            "engineer_time_entries"
        )
        engineer_time_entries = []
        for time_entry in engineer_time_entries_payload:
            engineer_time_entry = dict(time_entry)
            engineer_time_entry["customer_crm_id"] = customer_crm_id
            engineer_time_entry["member_crm_id"] = time_entry["user_id"]
            engineer_time_entry.pop("user_id", None)
            engineer_time_entries.append(engineer_time_entry)
        return engineer_time_entries

    def _get_pm_time_entry_payload(self, serializer, customer_crm_id):
        pm_time_entry = serializer.validated_data.get("pm_time_entry")
        pm_time_entry["ticket_id"] = serializer.validated_data.get("ticket_id")
        system_member_crm_id = self.request.user.profile.system_member_crm_id
        if not system_member_crm_id:
            raise ValidationError(
                f"System Member mapping not done for the user {self.request.user}."
            )
        pm_time_entry["member_crm_id"] = system_member_crm_id
        pm_time_entry["customer_crm_id"] = customer_crm_id
        return pm_time_entry

    def get_time_entry_data(self):
        serializer = MeetingCompleteSerializer(
            data=self.request.data, context={"request": self.request}
        )
        serializer.is_valid(raise_exception=True)
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        project: Project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )
        customer_crm_id = project.customer.crm_id
        time_entry_payload = {
            "pm_time_entry": self._get_pm_time_entry_payload(
                serializer, customer_crm_id
            ),
            "engineer_time_entries": self._get_engineer_time_entry_payload(
                serializer, customer_crm_id
            ),
        }
        return time_entry_payload


class CustomerTouchMeetingCompleteActionView(MeetingTimeEntryMixin, APIView):
    def post(self, request, *args, **kwargs):
        provider = request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        ticket_id = self.request.data.get("ticket_id")
        time_entry_payload = self.get_time_entry_data()
        add_customer_contacts = request.query_params.get(
            "add_customer_contacts", False
        )
        add_customer_contacts: bool = json.loads(add_customer_contacts)
        if not ticket_id:
            raise ValidationError({"ticket_id": "This field is required."})
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(), provider=provider, id=meeting_id
        )
        customer_touch_meeting: CustomerTouchMeeting = get_object_or_404(
            CustomerTouchMeeting.objects.all(), meeting=meeting
        )
        try:
            result = MeetingService(provider).complete_customer_touch_meeting(
                project_id,
                meeting,
                customer_touch_meeting,
                ticket_id,
                time_entry_payload,
                self.request.user,
                add_customer_contacts,
            )
        except ValueError as e:
            raise ValidationError({"detail": e})
        except ProjectSettingException as e:
            raise PMOSettingsNotConfigured(e)
        return Response(data=result, status=status.HTTP_200_OK)


class MeetingAttendeeListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = MeetingAttendeeSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")

        queryset = MeetingAttendee.objects.filter(
            meeting_id=meeting_id,
            meeting__provider=provider,
            meeting__project_id=project_id,
        )
        return queryset

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(),
            provider=provider,
            project_id=project_id,
            id=meeting_id,
        )
        serializer.save(meeting=meeting)


class MeetingAttendeeDeleteView(generics.DestroyAPIView):
    def get_queryset(self):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")

        queryset = MeetingAttendee.objects.filter(
            meeting_id=meeting_id,
            meeting__provider=provider,
            meeting__project_id=project_id,
        )
        return queryset


class KickoffMeetingCompleteActionView(MeetingTimeEntryMixin, APIView):
    def post(self, request, *args, **kwargs):
        provider = request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        ticket_id = self.request.data.get("ticket_id")
        add_customer_contacts = request.query_params.get(
            "add_customer_contacts", False
        )
        add_customer_contacts: bool = json.loads(add_customer_contacts)
        time_entry_payload = self.get_time_entry_data()
        if not ticket_id:
            raise ValidationError({"ticket_id": "This field is required."})
        project: Project = get_object_or_404(
            Project.objects.all(), provider=provider, id=project_id
        )
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(), provider=provider, id=meeting_id
        )
        try:
            try:
                kickoff_meeting_is_internal = KickoffMeeting.objects.only(
                    "meeting", "is_internal"
                ).get(meeting=meeting)
            except KickoffMeeting.DoesNotExist:
                raise ValidationError("Invalid meeting.")

            if not kickoff_meeting_is_internal.is_internal:
                result = MeetingService(provider).complete_kickoff_meeting(
                    project,
                    meeting,
                    ticket_id,
                    time_entry_payload,
                    self.request.user,
                    add_customer_contacts,
                )
            else:
                result = MeetingService(
                    provider
                ).complete_internal_kickoff_meeting(
                    project,
                    meeting,
                    ticket_id,
                    time_entry_payload,
                    self.request.user,
                    add_customer_contacts,
                )
        except ValueError as e:
            raise ValidationError({"detail": e})
        except ProjectSettingException as e:
            raise PMOSettingsNotConfigured(e)
        return Response(data=result, status=status.HTTP_200_OK)


class StatusMeetingCompleteActionView(MeetingTimeEntryMixin, APIView):
    def post(self, request, *args, **kwargs):
        provider = request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        add_customer_contacts = request.query_params.get(
            "add_customer_contacts", False
        )
        add_customer_contacts: bool = json.loads(add_customer_contacts)
        time_entry_payload = self.get_time_entry_data()
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(), provider=provider, id=meeting_id
        )
        try:
            result = MeetingService(provider).complete_status_meeting(
                project_id,
                meeting,
                time_entry_payload,
                self.request.user,
                add_customer_contacts,
            )
        except ValueError as e:
            raise ValidationError({"detail": e})
        except ProjectSettingException as e:
            raise PMOSettingsNotConfigured(e)
        return Response(data=result, status=status.HTTP_200_OK)


class MeetingNoteListCreateView(generics.ListCreateAPIView):
    serializer_class = ProjectNoteSerializer
    pagination_class = None

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        queryset = ProjectQueryService(provider).get_meeting_notes(
            meeting_id, project_id
        )
        return queryset

    def perform_create(self, serializer):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        author = self.request.user
        # validate meeting_id
        get_object_or_404(
            Meeting.objects.all().only("id"),
            provider=provider,
            id=meeting_id,
            project_id=project_id,
        )

        serializer.save(
            provider=provider,
            project_id=project_id,
            author=author,
            meeting_id=meeting_id,
        )


class MeetingNoteUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectNoteSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        queryset = ProjectNote.objects.meeting_notes().filter(
            provider=provider,
            project_id=project_id,
            author=self.request.user,
            meeting_id=meeting_id,
        )
        return queryset

    def perform_update(self, serializer):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        author = self.request.user
        # validate meeting_id
        get_object_or_404(
            Meeting.objects.all().only("id"), provider=provider, id=meeting_id
        )
        serializer.save(
            provider=provider,
            project_id=project_id,
            author=author,
            meeting_id=meeting_id,
        )


class MeetingArchiveRetrieveAPI(MeetingArchiveCheckMixin, APIView):
    def get(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        meeting_archive = get_object_or_404(
            MeetingArchive.objects.all(),
            provider=provider,
            project_id=project_id,
            meeting_id=meeting_id,
        )
        return Response(meeting_archive.payload, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        provider = self.request.provider
        meeting = get_object_or_404(
            Meeting.objects.all(),
            provider=provider,
            project_id=project_id,
            id=meeting_id,
        )
        if self.check_if_meeting_archived():
            raise ValidationError("Meeting already archived.")
        if meeting.status != Meeting.DONE:
            raise ValidationError(
                "Meeting not completed yet. Can not be archived."
            )
        typed_meeting = ProjectQueryService(
            provider
        ).get_typed_meeting_for_meeting(meeting)
        if not typed_meeting:
            raise ValidationError("Invalid Meeting.")
        meeting_service = MeetingService(provider).archive_meeting(
            typed_meeting, project_id
        )
        return Response(
            data=meeting_service.payload, status=status.HTTP_200_OK
        )


class CloseOutMeetingCreateView(MeetingCreateAPIView):
    serializer_class = CloseOutMeetingWriteSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        return ProjectQueryService(provider).create_close_out_meeting(
            project_id, copy.deepcopy(serializer.validated_data)
        )


class CloseOutMeetingRetrieveUpdateDestroyView(
    TypedMeetingMixin, generics.RetrieveUpdateDestroyAPIView
):
    model = CloseOutMeeting

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CloseOutMeetingReadSerializer
        return CloseOutMeetingWriteSerializer

    def perform_update(self, serializer):
        provider = self.request.provider
        status_meeting = self.get_object()
        ProjectQueryService(provider).update_close_out_meeting(
            status_meeting, serializer.validated_data
        )


class CloseOutMeetingCompleteActionView(MeetingTimeEntryMixin, APIView):
    def post(self, request, *args, **kwargs):
        provider = request.provider
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        ticket_id = self.request.data.get("ticket_id")
        add_customer_contacts = request.query_params.get(
            "add_customer_contacts", False
        )
        add_customer_contacts: bool = json.loads(add_customer_contacts)
        time_entry_payload = self.get_time_entry_data()
        if not ticket_id:
            raise ValidationError({"ticket_id": "This field is required."})
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(), provider=provider, id=meeting_id
        )
        close_out_meeting: CloseOutMeeting = get_object_or_404(
            CloseOutMeeting.objects.all(), meeting=meeting
        )
        try:
            result = MeetingService(provider).complete_close_out_meeting(
                project_id,
                meeting,
                close_out_meeting,
                ticket_id,
                time_entry_payload,
                self.request.user,
                add_customer_contacts,
            )
        except ValueError as e:
            raise ValidationError({"detail": e})
        except ProjectSettingException as e:
            raise PMOSettingsNotConfigured(e)
        return Response(data=result, status=status.HTTP_200_OK)


class MeetingDocumentListCreateView(generics.ListCreateAPIView):
    serializer_class = MeetingDocumentSerializer

    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        queryset = (
            MeetingDocument.objects.select_related("uploaded_by")
            .filter(meeting__project_id=project_id, meeting_id=meeting_id)
            .only(
                "file_name",
                "file_path",
                "uploaded_by__first_name",
                "uploaded_by__last_name",
            )
            .order_by("-updated_on")
        )
        return queryset

    def perform_create(self, serializer):
        meeting_id = self.kwargs.get("meeting_id")
        uploaded_by = self.request.user
        serializer.save(meeting_id=meeting_id, uploaded_by=uploaded_by)


class MeetingDocumentDeleteView(generics.DestroyAPIView):
    def get_queryset(self):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        queryset = MeetingDocument.objects.filter(
            meeting_id=meeting_id, meeting__project_id=project_id
        ).order_by("-updated_on")
        return queryset


class MeetingDocumentDownloadView(HTTPDownloadView, APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        file_ = self.get_file()
        data = base64.b64encode(file_.file.read())
        response = Response(data)
        return response

    def get_url(self):
        document_id = self.kwargs.get("pk")
        meeting_id = self.kwargs.get("meeting_id")
        document = get_object_or_404(
            MeetingDocument.objects.all(),
            id=document_id,
            meeting_id=meeting_id,
        )
        return document.file_path.url


class MeetingPDFPreview(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        add_customer_contacts = request.query_params.get(
            "add_customer_contacts", False
        )
        add_customer_contacts: bool = json.loads(add_customer_contacts)
        project: Project = get_object_or_404(
            Project.objects.all(), provider=request.provider, id=project_id
        )
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(),
            provider=request.provider,
            id=meeting_id,
            project_id=project_id,
        )
        if meeting.meeting_type not in [
            Meeting.INTERNAL_KICKOFF,
            Meeting.KICKOFF,
            Meeting.STATUS,
        ]:
            raise ValidationError(
                f"No preview available for the meeting type {meeting.meeting_type}"
            )
        attachment_name = f"{project.title}-{meeting.name}.pdf"

        if meeting.meeting_type in [Meeting.INTERNAL_KICKOFF, Meeting.KICKOFF]:
            attachment_url = generate_kickoff_meeting_pdf(
                meeting, project, attachment_name, add_customer_contacts
            )
        elif meeting.meeting_type == Meeting.STATUS:
            attachment_url = generate_status_meeting_pdf(
                meeting, project, attachment_name, add_customer_contacts
            )
        return Response(
            data={"url": attachment_url}, status=status.HTTP_200_OK
        )


class CompletedMeetingPDFPreview(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        project_id: int = self.kwargs.get("project_id")
        meeting_id: int = self.kwargs.get("meeting_id")
        project: Project = get_object_or_404(
            Project.objects.all(), provider=request.provider, id=project_id
        )
        archive_item: "QuerySet[ProjectItemsArchive]" = (
            ProjectItemsArchive.objects.filter(
                item_type="Meeting", blob__meeting__id=meeting_id
            ).values_list("blob", flat=True)
        )
        if archive_item:
            meeting_data = archive_item[0].get("meeting")
            meeting: Meeting = Meeting.objects.none()
            meeting.id = meeting_data.get("id")
            meeting.meeting_type = meeting_data.get("meeting_type")
            meeting.name = meeting_data.get("name")
            meeting.conducted_on = meeting_data.get("conducted_on")
            if meeting.meeting_type not in [
                Meeting.INTERNAL_KICKOFF,
                Meeting.KICKOFF,
                Meeting.STATUS,
            ]:
                raise ValidationError(
                    f"No preview available for the meeting type {meeting.meeting_type}"
                )
            attachment_name: str = f"{project.title}-{meeting.name}.pdf"
            if meeting.meeting_type in [
                Meeting.INTERNAL_KICKOFF,
                Meeting.KICKOFF,
            ]:
                attachment_url: str = generate_kickoff_meeting_pdf(
                    meeting, project, attachment_name
                )
            elif meeting.meeting_type == Meeting.STATUS:
                attachment_url: str = generate_status_meeting_pdf(
                    meeting, project, attachment_name
                )
            return Response(
                data={"url": attachment_url}, status=status.HTTP_200_OK
            )


class MeetingExternalAttendeeListCreateAPIView(generics.ListCreateAPIView):
    """
    API view to create and list external attendees for a meeting.
    """

    permission_classes = [
        IsAuthenticated,
    ]
    serializer_class = ExternalAttendeeSerializer
    pagination_class = None

    def get_queryset(self):
        provider_id = self.request.provider.id
        meeting_id = self.kwargs.get("meeting_id")
        project_id = self.kwargs.get("project_id")
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(),
            project_id=project_id,
            provider_id=provider_id,
            id=meeting_id,
        )
        queryset = ExternalAttendee.objects.filter(
            meeting_id=meeting_id,
            provider_id=provider_id,
            project_id=project_id,
        )
        return queryset

    def perform_create(self, serializer):
        provider_id = self.request.provider.id
        project_id = self.kwargs.get("project_id")
        meeting_id = self.kwargs.get("meeting_id")
        meeting: Meeting = get_object_or_404(
            Meeting.objects.all(),
            provider_id=provider_id,
            project_id=project_id,
            id=meeting_id,
        )
        serializer.save(
            provider_id=provider_id,
            project_id=project_id,
            meeting_id=meeting_id,
        )


class MeetingExternalAttendeeDeleteAPIView(generics.DestroyAPIView):
    """
    API view to delete an external meeting attendee
    """

    permission_classes = [
        IsAuthenticated,
    ]
    pagination_class = None

    def get_queryset(self):
        pk = self.kwargs.get("pk")
        project_id = self.kwargs.get("project_id")
        project = Project.objects.get(id=project_id)
        provider_id = project.provider_id
        meeting_id = self.kwargs.get("meeting_id")
        queryset = ExternalAttendee.objects.filter(
            pk=pk,
            project_id=project_id,
            meeting_id=meeting_id,
            provider_id=provider_id,
        )
        return queryset
