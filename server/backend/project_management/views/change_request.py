from typing import Optional, Dict, List

from django.db import transaction
from django.db.models import F, QuerySet
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework import status
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView
from rest_framework.generics import GenericAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from document.models import SOWDocument
from document.tasks.document_tasks import (
    sow_document_post_create_update_processing,
)
from project_management.exceptions import (
    PMOSettingsNotConfigured,
)
from project_management.models import (
    ChangeRequest,
    ChangeRequestEmailTemplate,
    Project,
    CancelledChangeRequestSetting,
)
from project_management.serializers import (
    ChangeRequestSerializer,
    ProcessChangeRequestSerializer,
    CancelledChangeRequestSettingSerializer,
)
from project_management.services.change_request import (
    ProjectChangeRequestService,
)
from project_management.tasks.meeting_emails import send_change_request_email


class ChangeRequestViewMixin:
    def get_queryset(self: GenericAPIView):
        project_id: int = self.kwargs.get("project_id")
        provider: Provider = self.request.provider
        queryset: "QuerySet[ChangeRequest]" = (
            ChangeRequest.objects.select_related("sow_document")
            .filter(provider=provider, project_id=project_id)
            .annotate(json_config=F("sow_document__json_config"))
            .only(
                "name",
                "requested_by",
                "assigned_to",
                "change_request_type",
                "status",
                "last_updated_by",
                "sow_document__json_config",
            )
            .order_by("-updated_on")
        )
        return queryset

    def create_change_request(
        self: GenericAPIView, project: Project, serializer
    ):
        """
        Creates change request. A SoW document is also created for the Change Reqeust.
        The SoW & service detail documents are uploaded to the linked opportunity.

        Args:
            project: Project
            serializer: Serializer instance
        """
        # Add change number if the document is being created.
        # Change number is 1 + the count of existing change requests
        existing_change_request_count: int = ChangeRequest.objects.filter(
            provider=project.provider, project_id=project.id
        ).count()
        change_number: int = existing_change_request_count + 1
        serializer.validated_data["change_number"] = change_number

        json_config: Dict = serializer.validated_data.pop("json_config", {})
        change_request: ChangeRequest = serializer.save(
            project_id=project.id,
            provider=project.provider,
            last_updated_by=self.request.user,
            updated_on=timezone.now(),
        )
        change_request_service: ProjectChangeRequestService = (
            ProjectChangeRequestService(
                project.provider, project, change_request
            )
        )
        sow_document: SOWDocument = (
            change_request_service.create_change_request_sow(
                self.request.user,
                json_config,
            )
        )
        change_request.sow_document = sow_document
        change_request.save()
        transaction.on_commit(
            lambda: sow_document_post_create_update_processing.apply_async(
                (
                    project.provider_id,
                    self.request.user.id,
                    sow_document.id,
                ),
                queue="high",
            )
        )

    def update_change_request(
        self: GenericAPIView, project: Project, serializer
    ):
        """
        Updates change request. The SoW document is also updated.
        The updated SoW & service detail documents are uploaded to the linked opportunity

        Args:
            project: Project
            serializer: Serializer instance
        """
        json_config: Dict = serializer.validated_data.pop("json_config", {})
        change_request: ChangeRequest = serializer.save(
            project_id=project.id,
            provider=project.provider,
            last_updated_by=self.request.user,
            updated_on=timezone.now(),
        )
        change_request.sow_document.name = change_request.name
        change_request.sow_document.user = change_request.requested_by
        change_request.sow_document.json_config = json_config
        change_request.sow_document.save()

        if serializer.validated_data.get("status") == ChangeRequest.REJECTED:
            change_request_service: ProjectChangeRequestService = (
                ProjectChangeRequestService(
                    project.provider, project, change_request
                )
            )
            change_request_service.update_cancelled_change_request_opportunity()
        transaction.on_commit(
            lambda: sow_document_post_create_update_processing.apply_async(
                (
                    project.provider_id,
                    self.request.user.id,
                    change_request.sow_document_id,
                ),
                queue="high",
            )
        )

    def perform_save(self: GenericAPIView, serializer):
        project: Optional[Project] = get_object_or_404(
            Project.objects.all(),
            provider=self.request.provider,
            id=self.kwargs.get("project_id"),
        )
        if self.request.method == "POST":
            self.create_change_request(project, serializer)
        elif self.request.method == "PUT":
            self.update_change_request(project, serializer)
        else:
            raise ValidationError(f"HTTP Method not supported!")


class ChangeRequestListCreateAPIView(
    ChangeRequestViewMixin, generics.ListCreateAPIView
):
    serializer_class = ChangeRequestSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "requested_by__first_name",
        "requested_by__last_name",
        "assigned_to__first_name",
        "assigned_to__last_name",
    ]
    ordering_fields = ["name"]

    def perform_create(self, serializer):
        self.perform_save(serializer)


class ChangeRequestRetrieveUpdateDestroyView(
    ChangeRequestViewMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ChangeRequestSerializer

    def perform_update(self, serializer):
        self.perform_save(serializer)


class ProcessChangeRequest(generics.CreateAPIView):
    serializer_class = ProcessChangeRequestSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        project_id = self.kwargs.get("project_id")
        change_request_id = self.kwargs.get("pk")
        change_request: ChangeRequest = get_object_or_404(
            ChangeRequest.objects.all(),
            provider=provider,
            project_id=project_id,
            id=change_request_id,
        )
        if change_request.sow_document is None:
            raise ValidationError(
                "No SOW Document found for the change request."
            )
        ProjectChangeRequestService(
            provider, change_request.project, change_request
        ).process_change_request(serializer.validated_data.get("files"))


class ChangeRequestEmailActionAPIView(APIView):
    def post(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        try:
            ChangeRequestEmailTemplate.objects.get(provider=provider)
        except ChangeRequestEmailTemplate.DoesNotExist:
            raise PMOSettingsNotConfigured(
                "Change Request Email Template setting not completed!"
            )
        project_id: int = self.kwargs.get("project_id")
        change_request_id: int = self.kwargs.get("pk")
        change_request: ChangeRequest = get_object_or_404(
            ChangeRequest.objects.all(),
            provider=provider,
            project_id=project_id,
            id=change_request_id,
        )
        if change_request.sow_document is None:
            raise ValidationError(
                "No SOW Document found for the change request."
            )
        send_change_request_email.apply_async(
            (provider.id, project_id, change_request_id, request.user.id),
            queue="high",
        )
        return Response(status=status.HTTP_202_ACCEPTED)


class CancelledCRSettingCreateRetrieveUpdateView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve & update Cancelled Change Request Setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = CancelledChangeRequestSettingSerializer

    def get_object(self):
        try:
            setting: CancelledChangeRequestSetting = (
                CancelledChangeRequestSetting.objects.get(
                    provider=self.request.provider
                )
            )
            return setting
        except CancelledChangeRequestSetting.DoesNotExist:
            raise NotFound(
                "Cancelled Change Request Setting not configured for the provider!"
            )

    def perform_create(self, serializer):
        if not CancelledChangeRequestSetting.configured_for_provider(
            self.request.provider
        ):
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                "Cancelled Change Request Setting already exists for the provider!"
            )
