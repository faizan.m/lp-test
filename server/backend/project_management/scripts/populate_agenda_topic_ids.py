from project_management.models import StatusMeeting, KickoffMeeting, Meeting
from typing import List, Set, Optional, Union, Dict
from django.db.models import QuerySet
from accounts.models import Provider


def populate_ids_of_agenda_topic_items(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Populates agenda topics with ID. ID is basically the order of agenda topic.

    Args:
        provider_ids: Optional list of provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        print(
            f"Populating agenda topics with ID for provider: {provider.name}, ID: {provider.id}..."
        )
        meetings: "QuerySet[Meeting]" = Meeting.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {meetings.count()} meetings for provider: {provider.name}."
        )
        for meeting in meetings:
            status_meeting_exists: bool = StatusMeeting.objects.filter(
                meeting_id=meeting.id
            ).exists()
            kickoff_meeting_exists: bool = KickoffMeeting.objects.filter(
                meeting_id=meeting.id
            ).exists()

            if status_meeting_exists:
                status_meeting: StatusMeeting = StatusMeeting.objects.get(
                    meeting_id=meeting.id
                )
                print(
                    f"Populating agenda topic with ID for status meeting with ID: {status_meeting.id} ..."
                )
                status_meeting_agenda_topics: List[
                    Dict
                ] = status_meeting.agenda_topics
                for index, agenda_topic in enumerate(
                    status_meeting_agenda_topics
                ):
                    agenda_topic.update(id=index + 1)
                StatusMeeting.objects.filter(id=status_meeting.id).update(
                    agenda_topics=status_meeting_agenda_topics
                )
                print(
                    f"Successfully populated agenda topic ID for status meeting: {status_meeting.id}!"
                )

            if kickoff_meeting_exists:
                kickoff_meeting: KickoffMeeting = KickoffMeeting.objects.get(
                    meeting_id=meeting.id
                )
                print(
                    f"Populating agenda topic with ID for kickoff meeting with ID: {kickoff_meeting.id} ..."
                )
                kickoff_meeting_agenda_topics: List = (
                    kickoff_meeting.agenda_topics
                )
                for index, agenda_topic in enumerate(
                    kickoff_meeting_agenda_topics
                ):
                    agenda_topic.update(id=index + 1)
                KickoffMeeting.objects.filter(id=kickoff_meeting.id).update(
                    agenda_topics=kickoff_meeting_agenda_topics
                )
                print(
                    f"Successfully populated agenda topic ID for kickoff meeting with ID: {kickoff_meeting.id}!"
                )
        print(
            f"Successfully populated agenda topic IDs for provider: {provider.name}, ID: {provider.id}! \n"
        )
