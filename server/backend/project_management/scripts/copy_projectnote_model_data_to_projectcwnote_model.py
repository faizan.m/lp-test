from project_management.models import ProjectCWNote, ProjectNote, Project
from typing import List, Optional
from django.db.models import QuerySet
from rest_framework.generics import get_object_or_404
from accounts.models import Provider
from project_management.project_management_utils import remove_html_tags
from utils import get_app_logger

logger = get_app_logger(__name__)


def copy_projectnote_model_data_to_projectcwnote_model(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Populates project note data into projectcwnote table.

    Args:
        provider_ids: Optional list of provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        print(
            f"Copying project note in project cw note model for provider: {provider.name}, ID: {provider.id}..."
        )
        project_notes: "QuerySet[ProjectNote]" = ProjectNote.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {project_notes.count()} projects for provider: {provider.name}."
        )
        for project_note in project_notes:
            if not project_note.meeting:
                if not project_note.project_notes_crm_id:
                    project: Project = get_object_or_404(
                        Project.objects.all().only("crm_id"),
                        id=project_note.project.id,
                        provider=provider,
                    )
                    note = remove_html_tags(project_note.note)
                    cw_project_note = provider.erp_client.create_project_notes(
                        project.crm_id, note
                    )
                    payload = dict(
                        project=project_note.project,
                        provider=provider,
                        note_crm_id=cw_project_note.get("note_id"),
                        author=project_note.author,
                    )
                else:
                    payload = dict(
                        project=project_note.project,
                        provider=provider,
                        note_crm_id=project_note.project_notes_crm_id,
                        author=project_note.author,
                    )
                ProjectCWNote.objects.create(**payload)
                print(f"Copied data for {project_note.id}")
                logger.info(f"Copied data for {project_note.id}")

        logger.info(
            f"Successfully copied project notes for provider: {provider.name}, ID: {provider.id}! \n"
        )
