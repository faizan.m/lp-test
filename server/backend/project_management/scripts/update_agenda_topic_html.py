from accounts.models import Provider, Customer
from project_management.models import (
    Meeting,
    StatusMeeting,
    KickoffMeeting,
    MeetingTemplate,
    ActionItem,
    RiskItem,
    CriticalPathItem,
)
from typing import List, Dict, Optional


def add_html_tag_to_agenda_topics_if_contains_plain_text(
    agenda_topics: List[Dict],
) -> List[Dict]:
    """
    Add HTML tags in topic if it contains plain text.

    Args:
        agenda_topics: Agenda topics

    Returns:
        Agenda topics
    """
    for agenda_topic in agenda_topics:
        topic: str = agenda_topic.get("topic", "")
        if not topic.startswith("<"):
            topic: str = f"<p>{topic}</p>"
            agenda_topic.update(topic=topic)
    return agenda_topics


def add_html_tag_to_action_items_if_contains_plain_text(
    action_items: List[str],
) -> List[str]:
    """
    Add html tag to action item if contains plain text.

    Args:
        action_items: Action items

    Returns:
        Updated action items
    """
    for index, action_item in enumerate(action_items):
        if not action_item.startswith("<"):
            action_items[index] = f"<p>{action_item}</p>"
    return action_items


def update_agenda_topic_html_in_meetings(
    provider_ids: Optional[List[int]] = None,
):
    """
    Update agenda topic content for those topics that have text content.
    Adds <p> tags to agenda topic if does not start with angular brackets.

    Args:
        provider_ids: List of Provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    configured_providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in configured_providers:
        meetings: "QuerySet[Meeting]" = Meeting.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {meetings.count()} meetings for provider: {provider.name}, ID: {provider.id}."
        )
        for meeting in meetings:
            status_meeting_exists: bool = StatusMeeting.objects.filter(
                meeting_id=meeting.id
            ).exists()
            kickoff_meeting_exists: bool = KickoffMeeting.objects.filter(
                meeting_id=meeting.id
            ).exists()

            if status_meeting_exists:
                status_meeting: StatusMeeting = StatusMeeting.objects.get(
                    meeting_id=meeting.id
                )
                agenda_topics: List[
                    Dict
                ] = add_html_tag_to_agenda_topics_if_contains_plain_text(
                    status_meeting.agenda_topics
                )
                StatusMeeting.objects.filter(id=status_meeting.id).update(
                    agenda_topics=agenda_topics
                )
                print(
                    f"Added HTML to agenda topics of status meeting with ID: {status_meeting.id}"
                )
            if kickoff_meeting_exists:
                kickoff_meeting: KickoffMeeting = KickoffMeeting.objects.get(
                    meeting_id=meeting.id
                )
                agenda_topics: List[
                    Dict
                ] = add_html_tag_to_agenda_topics_if_contains_plain_text(
                    kickoff_meeting.agenda_topics
                )
                KickoffMeeting.objects.filter(id=kickoff_meeting.id).update(
                    agenda_topics=agenda_topics
                )
                print(
                    f"Added HTML to agenda topics of kickoff meeting with ID: {kickoff_meeting.id}"
                )
        print(
            f"Successfully added HTML to agenda topics of meetings for provider: {provider.name}, ID: {provider.id}\n\n"
        )


def update_agenda_topics_action_items_in_content_in_meeting_templates(
    provider_ids: Optional[List[int]] = None,
):
    """
    Update agenda topic and action item content of meeting templates if it contains plain text.
    Adds <p> tags to agenda topic if does not start with angular brackets.

    Args:
        provider_ids: List of Provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        meeting_templates: "QuerySet[MeetingTemplate]" = (
            MeetingTemplate.objects.filter(provider_id=provider.id)
        )
        print(
            f"Found {meeting_templates.count()} meeting templates for "
            f"provider: {provider.name}, ID: {provider.id}"
        )
        for template in meeting_templates:
            if template.agenda_topics:
                updated_agenda_topics: List[
                    Dict
                ] = add_html_tag_to_agenda_topics_if_contains_plain_text(
                    template.agenda_topics
                )
                MeetingTemplate.objects.filter(
                    id=template.id, provider_id=provider.id
                ).update(agenda_topics=updated_agenda_topics)
                print(
                    f"Updated agenda topics of meeting template with ID: {template.id}"
                )
            if template.action_items:
                updated_action_items: List[
                    str
                ] = add_html_tag_to_action_items_if_contains_plain_text(
                    template.action_items
                )
                MeetingTemplate.objects.filter(
                    id=template.id, provider_id=provider.id
                ).update(action_items=updated_action_items)
                print(
                    f"Updated action items of meeting template with ID: {template.id}"
                )
        print(
            f"Successfully updated meeting templates for "
            f"provider: {provider.name}, ID: {provider.id} \n\n"
        )
    return


def update_action_items_risk_items_critical_path_items_html(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Update action items, risk items, critical path items content if in plain text.

    Args:
        provider_ids: List of Provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        action_items: "QuerySet[ActionItem]" = ActionItem.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {action_items.count()} action items for "
            f"provider: {provider.name}, ID: {provider.id}"
        )
        for action_item in action_items:
            if not action_item.description.startswith("<"):
                updated_description: str = f"<p>{action_item.description}</p>"
                ActionItem.objects.filter(
                    id=action_item.id, provider_id=provider.id
                ).update(description=updated_description)
                print(f"Updated action item with ID: {action_item.id}")

        risk_items: "QuerySet[RiskItem]" = RiskItem.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {risk_items.count()} risk items for "
            f"provider: {provider.name}, ID: {provider.id}"
        )
        for risk_item in risk_items:
            if not risk_item.description.startswith("<"):
                updated_description: str = f"<p>{risk_item.description}</p>"
                RiskItem.objects.filter(
                    id=risk_item.id, provider_id=provider.id
                ).update(description=updated_description)
                print(f"Updated risk item with ID: {risk_item.id}")

        critical_path_items: "QuerySet[CriticalPathItem]" = (
            CriticalPathItem.objects.filter(provider_id=provider.id)
        )
        print(
            f"Found {critical_path_items.count()} critical path items for "
            f"provider: {provider.name}, ID: {provider.id}"
        )
        for critical_path_item in critical_path_items:
            if not critical_path_item.description.startswith("<"):
                updated_description: str = (
                    f"<p>{critical_path_item.description}</p>"
                )
                CriticalPathItem.objects.filter(
                    id=critical_path_item.id, provider_id=provider.id
                ).update(description=updated_description)
                print(
                    f"Updated critical path item with ID: {critical_path_item.id}"
                )
        print(
            f"Successfully updated items for provider: {provider.name}, ID: {provider.id}\n\n"
        )
    return
