import gzip
import logging.handlers as handlers
import shutil
from os import listdir, remove
from os.path import dirname, join, basename, splitext, exists


class RotatingFileHandlerWithCompression(handlers.RotatingFileHandler):
    """
    Enable log file compression for RotatingFileHandler
    """
    def doRollover(self):
        super().doRollover()
        log_dir = dirname(self.baseFilename)
        to_compress = [
            join(log_dir, f) for f in listdir(log_dir) if f.startswith(
                basename(splitext(self.baseFilename)[0])
            ) and not f.endswith((".gz", ".log"))
        ]
        for f in to_compress:
            if exists(f):
                with open(f, "rb") as _old, gzip.open(f + ".gz", "wb") as _new:
                    shutil.copyfileobj(_old, _new)
                remove(f)
