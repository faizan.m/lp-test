import custom_storages


class FileUploadService:
    @classmethod
    def _get_storage_class(cls):
        return custom_storages.get_temporary_storage_class()

    @classmethod
    def _get_permanent_storage_class(cls):
        return custom_storages.get_permanent_storage_class()

    # @classmethod
    # def _get_logs_storage_class(cls):
    #     return custom_storages.LogsS3Storage()

    @staticmethod
    def upload_file(file_path):
        storage_class = FileUploadService._get_storage_class()
        return storage_class.upload_file(file_path)

    @staticmethod
    def upload_attachment(file_path):
        storage_class = FileUploadService._get_permanent_storage_class()
        return storage_class.upload_file(file_path)

    # @staticmethod
    # def upload_logs_file(file_path):
    #     storage_class = FileUploadService._get_logs_storage_class()
    #     return storage_class.upload_file(file_path)
