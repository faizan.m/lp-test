from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from accounts.models import UserType

User = get_user_model()


class CustomAuthBackend(ModelBackend):
    """
    Custom Authentication Backend

    Allows a user to sign in using an email/password pair
    """

    def authenticate(self, request, email=None, password=None, **kwargs):
        """Authenticate a user based on email address as the user name."""
        if not request.provider and request.host == settings.ADMIN_PORTAL_URL:
            try:
                user = User.superusers.get(email__iexact=email, is_active=True)
                if user.check_password(password):
                    return user
            except User.DoesNotExist:
                return None
        elif request.provider:
            try:
                user = User.providers_and_customers.get(
                    email__iexact=email,
                    provider_id=request.provider.id,
                    is_active=True,
                    provider__is_active=True,
                )
                if user.type == UserType.CUSTOMER:
                    if (
                        not user.customer.is_active
                        or user.customer.is_deleted
                        or not user.is_active_in_crm
                        or user.is_deleted
                    ):
                        return None
                if user.check_password(password):
                    return user
            except User.DoesNotExist:
                return None
        return None

    def get_user(self, user_id):
        """Get a User object from the user_id."""
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
