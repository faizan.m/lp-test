from django.conf import settings
from django.db.models import Q

from accounts import utils
from accounts.models import Provider


def provider_middleware(get_response):
    def middleware(request):
        # Get request host
        host = utils.get_host(
            request.META.get("HTTP_CLIENT_URL"), request.META.get("HTTP_HOST")
        )

        if host == settings.ADMIN_PORTAL_URL:
            provider = None
        else:
            try:
                provider = Provider.objects.get(
                    Q(is_active=True) & (Q(url=host) | Q(url_alias=host))
                )
            except Provider.DoesNotExist:
                provider = None
            except Provider.MultipleObjectsReturned:
                provider = None
        # add provider to the request
        request.provider = provider
        # add host to request
        request.host = host
        return get_response(request)

    return middleware
