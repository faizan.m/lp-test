from django.utils.deprecation import MiddlewareMixin

from accounts.models import UserType, Customer


class CustomerMiddleware(MiddlewareMixin):
    def process_request(self, request):
        customer = None
        if request.provider and not request.user.is_anonymous:
            if request.user.user_type.name == UserType.CUSTOMER:
                try:
                    customer = Customer.objects.get(
                        name=request.user.customer.name,
                        provider__name=request.provider,
                        is_active=True,
                        is_deleted=False,
                    )
                except Customer.DoesNotExist:
                    customer = None
            # add it to the request
        request.customer = customer
