from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from django.utils.functional import SimpleLazyObject
from rest_framework.exceptions import APIException, AuthenticationFailed

from accounts.jwt_authentication import CustomJSONWebTokenAuthentication


class JWTAuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            request.user = SimpleLazyObject(
                lambda: self.__class__.get_jwt_user(request)
            )
            return self.get_response(request)
        except APIException as err:
            return JsonResponse(
                {err.default_code: err.default_detail}, status=err.status_code
            )

    @staticmethod
    def get_jwt_user(request):
        user = None
        jwt_authentication = CustomJSONWebTokenAuthentication()
        if jwt_authentication.get_jwt_value(request):
            try:
                user, jwt = jwt_authentication.authenticate(request)
            except AuthenticationFailed:
                user = None
        return user or AnonymousUser()
