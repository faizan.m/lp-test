import copy

from rest_framework import exceptions
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import DjangoObjectPermissions, BasePermission

# Default model permissions to be checked
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from accounts.models import Role, UserType, User
from accounts.token_manager import UntypedToken

DEFAULT_MODEL_PERMISSIONS_MAP = {
    "GET": [],
    "OPTIONS": [],
    "HEAD": [],
    "POST": ["%(app_label)s.add_%(model_name)s"],
    "PUT": [],
    "PATCH": [],
    "DELETE": [],
}

# Default object level permissions
DEFAULT_OBJECT_PERMISSIONS_MAP = {
    "GET": ["%(app_label)s.view_%(model_name)s"],
    "OPTIONS": [],
    "HEAD": [],
    "POST": ["%(app_label)s.add_%(model_name)s"],
    "PUT": ["%(app_label)s.change_%(model_name)s"],
    "PATCH": ["%(app_label)s.change_%(model_name)s"],
    "DELETE": ["%(app_label)s.delete_%(model_name)s"],
}


class CustomPermissions(DjangoObjectPermissions):
    SAFE_METHODS = ("HEAD", "OPTIONS")

    # Model level Permissions
    model_perm_map = DEFAULT_MODEL_PERMISSIONS_MAP
    perms_map = DEFAULT_OBJECT_PERMISSIONS_MAP

    def get_required_permissions(self, method, model_cls):
        """
        Given a model and an HTTP method, return the list of permission
        codes that the user is required to have.
        """
        kwargs = {
            "app_label": model_cls._meta.app_label,
            "model_name": model_cls._meta.model_name,
        }

        if method not in self.perms_map:
            raise exceptions.MethodNotAllowed(method)

        return [perm % kwargs for perm in self.model_perm_map[method]]


class ManageSuperuserUserPermissions(CustomPermissions):
    perms_map = copy.deepcopy(DEFAULT_OBJECT_PERMISSIONS_MAP)
    perms_map["POST"] = []

    model_perm_map = copy.deepcopy(DEFAULT_MODEL_PERMISSIONS_MAP)
    model_perm_map["POST"] = ["accounts.add_superuser_user"]


class ManageProviderUserPermission(CustomPermissions):
    perms_map = copy.deepcopy(DEFAULT_OBJECT_PERMISSIONS_MAP)
    perms_map["POST"] = []

    model_perm_map = copy.deepcopy(DEFAULT_MODEL_PERMISSIONS_MAP)
    model_perm_map["POST"] = ["accounts.add_provider_user"]


class ManageCustomerUserPermission(CustomPermissions):
    perms_map = copy.deepcopy(DEFAULT_OBJECT_PERMISSIONS_MAP)
    perms_map["POST"] = []

    model_perm_map = copy.deepcopy(DEFAULT_MODEL_PERMISSIONS_MAP)
    model_perm_map["POST"] = ["accounts.add_customer_user"]


class IsProviderOwner(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            request.provider
            and user.type == UserType.PROVIDER
            and user.role == Role.OWNER
        )


class IsProviderOwnerOrAdmin(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            request.provider
            and user.type == UserType.PROVIDER
            and user.role == Role.ADMIN
            or user.role == Role.OWNER
        )


class IsProviderAdmin(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            request.provider
            and user.type == UserType.PROVIDER
            and user.role == Role.ADMIN
        )


class IsProviderUser(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return request.provider and user.type == UserType.PROVIDER


class IsCustomerUser(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return request.customer and user.type == UserType.CUSTOMER


class IsCustomerOwner(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            request.customer
            and user.type == UserType.CUSTOMER
            and user.role == Role.OWNER
        )


class IsCustomerOwnerOrAdmin(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            request.provider
            and user.type == UserType.CUSTOMER
            and user.role == Role.ADMIN
            or user.role == Role.OWNER
        )


class IsAcelavarUser(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return user.type == UserType.ACCELAVAR


class IsProviderOrCustomerUser(BasePermission):
    """
    Check if user type is PROVIDER or CUSTOMER
    """

    def has_permission(self, request, view):
        return request.user.type in [UserType.PROVIDER, UserType.CUSTOMER]


class IsCustomerMSCustomer(BasePermission):
    """
    Check if the Customer is a Managed Service Customer
    """

    def has_permission(self, request, view):
        if request.user.type == UserType.CUSTOMER:
            return request.customer.is_ms_customer
        else:
            return True


class IsUntypedToken(BasePermission):
    def has_permission(self, request, view):
        try:
            jwt_value = request.META["HTTP_MULTI_FA_TOKEN"].split(" ")[-1]
        except KeyError:
            raise AuthenticationFailed(
                {"msg": "Please specify the 2FA token header."}
            )
        payload = UntypedToken.verify(jwt_value)
        user_id = payload["user_id"]
        user = User.objects.get(id=user_id)
        if request.user:
            request.user = user
            return True
        else:
            return False


class IsTwoFAVerified(BasePermission):
    """
    Custom permission that checks for token in request body and validates it.
    """

    def has_permission(self, request, view):
        token: str = request.data.pop("token", None)
        if not token:
            return False
        return UntypedToken.is_valid_token(token, request.user)
