# Create your views here.
import os
from smtplib import SMTPRecipientsRefused
from typing import List, Dict, Optional
from django.conf import settings
from django.contrib.auth.models import update_last_login
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.forms import model_to_dict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions, status
from rest_framework.exceptions import (
    NotFound,
    PermissionDenied,
    ValidationError,
)
from rest_framework.generics import (
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView,
    ListCreateAPIView,
)
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.views import ObtainJSONWebToken

from accounts import custom_filters, groups, utils
from accounts.custom_permissions import (
    CustomPermissions,
    IsCustomerMSCustomer,
    IsProviderOrCustomerUser,
    IsProviderUser,
    IsUntypedToken,
    ManageCustomerUserPermission,
    ManageProviderUserPermission,
    ManageSuperuserUserPermissions,
    IsProviderAdmin,
    IsProviderOwner,
    IsTwoFAVerified,
)
from accounts.email import PasswordResetEmail, send_activation_mail_to_user
from accounts.mail import BaseEmailMessage
from accounts.mixins import CustomerUserUpdateMixin, UserUpdateModelMixin
from accounts.models import (
    CircuitInfo,
    CircuitType,
    Customer,
    CustomerEscalation,
    Profile,
    Provider,
    Role,
    StoredEmailData,
    User,
    UserType,
    FeatureStatus,
)
from accounts.serializers import (
    AcelaUserListCreateSerializer,
    CircuitInfoCreateUpdateSerializer,
    CircuitInfoListRetrieveSerializer,
    CircuitInfoReportDownloadSerializer,
    CircuitInfoTypeListCreateSerializer,
    CircuitInfoTypeWithAssociationSerializer,
    CustomerEscalationCreateUpdateSerializer,
    CustomerEscalationRetrieveSerializer,
    CustomerUserActivationSerializer,
    CustomerUserListCreateSerializer,
    JWTSerializer,
    LoggedEmailDataSerializer,
    PasswordResetConfirmSerializer,
    PasswordResetSerializer,
    ProfileBaseUpdateSerializer,
    ProfilePicSerializer,
    ProviderCustomerUserUpdateSerializer,
    ProviderUserListCreateSerializer,
    RequestPhoneVerificationSerializer,
    SetPasswordSerializer,
    TwoFactorAuthenticationRequestSerializer,
    TwoFactorAuthenticationValidateSerializer,
    UidAndTokenSerializer,
    UniqueEmailSerializer,
    UserDropDownSerializer,
    UserRetrieveBaseUpdateSerializer,
    UserTypeSerializer,
    ValidatePhoneVerificationSerializer,
    FeatureStatusSerializer,
    FeatureStatusGetSerializer,
    MergingCustomerSerializer,
    AuthyOTPSerializer,
)
from accounts.services import CustomerService
from accounts.services.circuit_info_service import CircuitInfoService
from accounts.services.customer_service import (
    fetch_related_models_to_customers,
)
from accounts.services.two_factor_authentication_service import (
    TwilioAuthenticationService,
)
from accounts.tasks import (
    send_account_activation_success_email,
    send_password_set_success_email,
)
from accounts.token_manager import AccessToken, RefreshToken, UntypedToken
from accounts.utils import build_url, get_port_number_from_url
from core.connectwise_communication_item import (
    ConnectwiseCommunicationItemsService,
)
from core.models import ConnectwisePhoneNumberMappingSetting
from core.pagination import CustomPagination
from storage.file_upload import FileUploadService
import json
from django.core.serializers.json import DjangoJSONEncoder
from utils import get_app_logger
from caching.service import CacheService

# Get an instance of a logger
logger = get_app_logger(__name__)


class JWTView(ObtainJSONWebToken):
    """
    Endpoint to authenticate user using email and password
    """

    serializer_class = JWTSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.object.get("user") or request.user
            if settings.TWO_FA_ENABLED and (
                user.type == UserType.ACCELAVAR
                or request.provider.is_two_fa_enabled
            ):
                phone_number = user.profile.cell_phone_number
                data = dict(
                    error_message="User requires Two Factor Authentication",
                    is_two_fa_enabled=True if user.external_auth_id else False,
                    phone=TwilioAuthenticationService.hide_phone_number(
                        phone_number
                    )
                    if phone_number
                    else "",
                    country_code=user.profile.country_code,
                )
                data.update({"token": UntypedToken.for_user(user)})
                return Response(
                    data=data, status=status.HTTP_412_PRECONDITION_FAILED
                )
            else:
                response_data = serializer.validated_data
                return Response(
                    {
                        "token": response_data["token"],
                        "refresh": response_data["refresh"],
                    }
                )
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class ListCreateSuperuserUserAPIView(
    generics.ListCreateAPIView
):  # pylint: disable=too-many-ancestors
    """
    Endpoint to List and create superuser users
    """

    queryset = User.objects.none()
    serializer_class = AcelaUserListCreateSerializer
    permission_classes = (IsAuthenticated, ManageSuperuserUserPermissions)
    pagination_class = CustomPagination
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        custom_filters.UserOrderingFilter,
    )
    search_fields = [
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
    ]
    ordering_fields = [
        "first_name",
        "last_name",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
        "email",
        "is_active",
    ]

    def get_queryset(self):
        if self.request.user.type == UserType.ACCELAVAR:
            users = User.superusers.all()
            return users

        return User.objects.none()

    def perform_create(self, serializer):
        user_type = UserType.objects.get(name=UserType.ACCELAVAR)
        create_user(self.request, user_type, serializer)


class SuperuserUserRetrieveUpdateDeleteAPIView(
    UserUpdateModelMixin, generics.RetrieveUpdateDestroyAPIView
):  # pylint: disable=too-many-ancestors
    """
    Endpoint to get, update single superuser user instance
    """

    queryset = User.objects.all()
    serializer_class = UserRetrieveBaseUpdateSerializer
    permission_classes = (IsAuthenticated, ManageSuperuserUserPermissions)

    def perform_destroy(self, instance):
        logger.info(
            f"Hard deleting Acela user with email: {instance.email} and Id: {instance.id}"
        )
        super().perform_destroy(instance)


class ListCreateProviderUserAPIView(
    generics.ListCreateAPIView
):  # pylint: disable=too-many-ancestors
    """
    API View to list and create Provider users
    """

    queryset = User.providers.none()
    serializer_class = ProviderUserListCreateSerializer
    permission_classes = (IsAuthenticated, ManageProviderUserPermission)
    pagination_class = CustomPagination
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        custom_filters.UserOrderingFilter,
    )
    search_fields = [
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
    ]
    ordering_fields = [
        "first_name",
        "last_name",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
        "email",
        "is_active",
    ]

    def get_serializer_context(self):
        context = super(
            ListCreateProviderUserAPIView, self
        ).get_serializer_context()
        context.update({"provider_id": self.kwargs.get("provider_id")})
        return context

    def get_queryset(self):
        if self.request.user.type == UserType.ACCELAVAR:
            try:
                provider_id = self.kwargs["provider_id"]
                users = User.providers.filter(
                    provider=provider_id, user_role__role_name=Role.OWNER
                )
                return users
            except KeyError:
                raise NotFound("Invalid Provider ID.")

        elif self.request.user.type == UserType.PROVIDER:
            users = User.providers.filter(provider=self.request.provider)
            return users
        else:
            return User.objects.none()

    def perform_create(self, serializer):
        if self.request.user.type == UserType.ACCELAVAR:
            try:
                provider_id = self.kwargs["provider_id"]
                provider = Provider.objects.get(id=provider_id)
            except (KeyError, Provider.DoesNotExist):
                raise NotFound("Invalid Provider ID.")
        else:
            provider = self.request.provider
        user_type = UserType.objects.get(name=UserType.PROVIDER)
        create_user(self.request, user_type, serializer, provider=provider)


class ListProviderUserAPIView(
    generics.ListAPIView
):  # pylint: disable=too-many-ancestors
    """
    API View to list and create Provider users
    """

    queryset = User.providers.none()
    serializer_class = ProviderUserListCreateSerializer
    permission_classes = (IsAuthenticated, ManageProviderUserPermission)
    pagination_class = CustomPagination
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        custom_filters.UserOrderingFilter,
    )
    search_fields = [
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
    ]
    ordering_fields = [
        "first_name",
        "last_name",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
        "email",
        "is_active",
    ]

    def get_serializer_context(self):
        context = super(ListProviderUserAPIView, self).get_serializer_context()
        context.update({"provider_id": self.kwargs.get("provider_id")})
        return context

    def get_queryset(self):
        try:
            provider_id = self.kwargs["provider_id"]
            users = User.providers.filter(provider=provider_id)
        except KeyError:
            raise NotFound("Invalid Provider ID.")
        return users


class ProviderUserRetrieveUpdateDeleteAPIView(
    UserUpdateModelMixin, generics.RetrieveUpdateDestroyAPIView
):
    """
    Endpoint to get, update single superuser user instance
    """

    queryset = User.providers.all()
    serializer_class = UserRetrieveBaseUpdateSerializer
    permission_classes = (IsAuthenticated, ManageProviderUserPermission)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {}

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        if self.request.user.type == UserType.ACCELAVAR:
            try:
                provider_id = self.kwargs["provider_id"]
                Provider.objects.get(id=provider_id)  # Verify Provider ID
                filter_kwargs["provider_id"] = provider_id
            except (KeyError, Provider.DoesNotExist):
                raise NotFound("Invalid Provider ID.")

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def perform_destroy(self, instance):
        logger.info(
            f"Hard deleting Provider user with email: {instance.email} and Id: {instance.id}"
        )
        super().perform_destroy(instance)


class ListCreateCustomerUserAPIView(generics.ListCreateAPIView):
    """
    API View to list and create Customer users
    """

    queryset = User.customers.none()
    serializer_class = CustomerUserListCreateSerializer
    permission_classes = (IsAuthenticated, ManageCustomerUserPermission)
    pagination_class = CustomPagination
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        custom_filters.UserOrderingFilter,
    )
    search_fields = [
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
    ]
    ordering_fields = [
        "first_name",
        "last_name",
        "phone_number",
        "user_type__name",
        "user_role__role_name",
        "customer__name",
        "profile__department",
        "profile__title",
        "is_active",
        "email",
        "is_active",
    ]

    def get_serializer_class(self):
        full_info = self.request.query_params.get("full-info", "true")
        if full_info == "false":
            return UserDropDownSerializer
        return CustomerUserListCreateSerializer

    def get_queryset(self):
        if self.request.user.type == UserType.PROVIDER:
            try:
                customer_id = self.kwargs["customer_id"]
                users = User.customers.filter(
                    customer=customer_id,
                    is_active_in_crm=True,
                    is_deleted=False,
                )
                return users
            except KeyError:
                raise NotFound("Invalid Customers ID.")

        elif self.request.user.type == UserType.CUSTOMER:
            users = User.customers.filter(
                customer=self.request.customer,
                is_active_in_crm=True,
                is_deleted=False,
            )
            return users
        else:
            return User.objects.none()

    def perform_create(self, serializer):
        user_type = UserType.objects.get(name=UserType.CUSTOMER)
        # Create Customer User in ConnectWise
        if self.request.user.type == UserType.PROVIDER:
            try:
                customer_id = self.kwargs["customer_id"]
                customer = Customer.objects.get(
                    id=customer_id
                )  # Verify Provider ID
                send_mail = False
            except (KeyError, Customer.DoesNotExist):
                raise NotFound("Invalid Customer ID.")
        else:
            customer = self.request.customer
            send_mail = True
        client = self.request.provider.erp_client
        phone_mapping_setting: ConnectwisePhoneNumberMappingSetting = (
            ConnectwiseCommunicationItemsService(
                self.request.provider
            ).get_phone_number_mapping()
        )
        # create user in connectwise
        user = client.create_customer_user(
            customer.crm_id, serializer.validated_data, phone_mapping_setting
        )
        # create user in Acela DB
        create_user(
            self.request,
            user_type,
            serializer,
            self.request.provider,
            customer,
            send_mail=send_mail,
            crm_id=user["id"],
            is_active_in_crm=True,
        )


class CustomerUserRetrieveUpdateAPIView(
    UserUpdateModelMixin,
    CustomerUserUpdateMixin,
    generics.RetrieveUpdateAPIView,
):
    """
    Endpoint to get, update and delete single superuser user instance
    """

    queryset = User.customers.all()
    permission_classes = (IsAuthenticated, ManageCustomerUserPermission)

    def get_serializer_class(self):
        if (
            self.request.user.is_authenticated
            and self.request.user.type == UserType.PROVIDER
            and self.request.method
            in [
                "PUT",
                "PATCH",
            ]
        ):
            return ProviderCustomerUserUpdateSerializer
        return UserRetrieveBaseUpdateSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {}

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        if self.request.user.type == UserType.PROVIDER:
            try:
                customer_id = self.kwargs["customer_id"]
                Customer.objects.get(id=customer_id)  # Verify Provider ID
                filter_kwargs["customer_id"] = customer_id
            except (KeyError, Customer.DoesNotExist):
                raise NotFound("Invalid Customer ID.")

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj


class ProfileRetrieveUpdateAPIView(
    CustomerUserUpdateMixin, generics.RetrieveUpdateAPIView
):
    serializer_class = ProfileBaseUpdateSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class ProfilePicUpdateAPIView(generics.CreateAPIView):
    serializer_class = ProfilePicSerializer
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        profile = self.request.user.profile
        profile.profile_pic = serializer.validated_data.get("profile_pic")
        profile.save()
        return Response(status=status.HTTP_200_OK)


class ForgotPasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to send email to user with password reset link.
    """

    serializer_class = PasswordResetSerializer
    permission_classes = (permissions.AllowAny,)

    _user = None

    def _action(self, serializer):
        user = self.get_user(serializer.data["email"])
        self.send_password_reset_email(user)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_user(self, email):
        if not self.request.provider:
            try:
                user = User.superusers.get(email__iexact=email, is_active=True)
            except User.DoesNotExist:
                user = None
        else:
            try:
                user = User.providers_and_customers.get(
                    email__iexact=email,
                    provider_id=self.request.provider.id,
                    is_active=True,
                )
                if user.type == UserType.CUSTOMER:
                    if not (
                        user.is_active_in_crm
                        and user.customer.is_active
                        and not user.customer.is_deleted
                    ):
                        user = None
            except User.DoesNotExist:
                user = None
        if user and not user.has_usable_password():
            user = None
        return user

    def send_password_reset_email(self, user):
        port = get_port_number_from_url(self.request.META["HTTP_ORIGIN"])
        url = build_url(user.get_domain(), port)
        context = {"user": user, "domain": url}
        to_ = [user.email]
        images = [("logo.png", "logo")]
        logger.info(f"Reset Password Link sent to email:{user.email}")
        PasswordResetEmail(self.request, context, images=images).send(to_)


class ChangePasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to change user password.
    """

    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        return SetPasswordSerializer

    def _action(self, serializer):
        self.request.user.set_password(serializer.data["new_password"])
        self.request.user.save()
        logger.info("User password changed successfully")
        return Response(status=status.HTTP_204_NO_CONTENT)


class SetPasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to set password for user.
    """

    permission_classes = (permissions.AllowAny,)
    token_generator = default_token_generator

    def get_serializer_class(self):
        return PasswordResetConfirmSerializer

    def _action(self, serializer):
        """
        TODO: This should be a atomic operation
        :param serializer:
        :return:
        """
        # activate user if user has no password set
        if (
            not serializer.user.has_usable_password()
            and not serializer.user.is_active
        ):
            serializer.user.is_active = True
            logger.info("User account activated.")
            send_account_activation_success_email.delay(
                serializer.user.id, self.request.META["HTTP_ORIGIN"]
            )
        else:
            logger.info("User password set successfully")
            send_password_set_success_email.delay(
                serializer.user.id, self.request.META["HTTP_ORIGIN"]
            )
        # set new password for user
        serializer.user.set_password(serializer.data["new_password"])
        serializer.user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UidTokenValidationAPIView(generics.GenericAPIView):
    """
    Use this endpoint to validate the User ID and the Token
    """

    serializer_class = UidAndTokenSerializer
    permission_classes = (permissions.AllowAny,)
    token_generator = default_token_generator

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(status=status.HTTP_200_OK)


class UserTypeRoleListAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserTypeSerializer
    queryset = UserType.objects.all()


class UserActivationAPIView(APIView):
    """
    Set User Role and send activation email to the user.
    """

    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, ManageCustomerUserPermission)

    def put(self, request, **kwargs):
        serializer = CustomerUserActivationSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        try:
            if request.user.type == UserType.PROVIDER:
                user = User.customers.get(
                    id=kwargs["pk"], provider=request.provider
                )
            else:
                user = User.customers.get(
                    id=kwargs["pk"],
                    provider=request.provider,
                    customer=request.customer,
                )
            user.user_role_id = serializer.validated_data["user_role"]
            user.save()
            # Refresh Instance to get the latest version
            user.refresh_from_db()
            # Assign group to user. As the object is being updated here, the logic to assign user role
            # inside save() method will not be called. So, we need to call it explicitly.
            groups.change_user_role(user)
            if (
                user.activation_mails_sent_count
                <= settings.MAX_ACTIVATION_MAILS_COUNT
            ):
                # Send Account Activation email
                send_activation_mail_to_user(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                raise ValidationError("Maximum Retry Limit Reached.")
        except User.DoesNotExist:
            raise NotFound("Invalid User Id.")


class ResendUserActivationAPIView(generics.UpdateAPIView):
    """
    Send Activation email to user.
    """

    permission_classes = (IsAuthenticated, CustomPermissions)
    queryset = User.objects.all()

    def get_serializer(self, *args, **kwargs):
        return None

    def put(self, request, **kwargs):
        user_id = kwargs.get("pk")
        try:
            user = User.objects.get(id=user_id)
            self.check_object_permissions(self.request, user)
            if (
                user.activation_mails_sent_count
                <= settings.MAX_ACTIVATION_MAILS_COUNT
            ):
                # Send Account Activation email
                send_activation_mail_to_user(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                raise ValidationError("Maximum Retry Limit Reached.")
        except User.DoesNotExist:
            logger.debug(f"Invalid user Id {user_id}")
            raise NotFound("Invalid User Id.")


class VerifyUniqueEmailAPIView(generics.CreateAPIView):
    serializer_class = UniqueEmailSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user.type == UserType.ACCELAVAR:
            unique = utils.check_unique_email_for_acela_portal(
                serializer.validated_data["email"]
            )
        else:
            unique = utils.check_unique_email_for_provider_portal(
                serializer.validated_data["email"], request.provider
            )
        data = dict(unique=unique)
        headers = self.get_success_headers(serializer.data)
        return Response(data, status=status.HTTP_200_OK, headers=headers)


class CustomerEscalationFieldsListAPIView(APIView):
    def get(self, request, **kwargs):
        data = dict(
            escalation_priority_types=dict(
                CustomerEscalation.ESCALATION_PRIORITY_CHOICES
            ),
            escalation_types=dict(CustomerEscalation.ESCALATION_TYPE_CHOICES),
        )
        return Response(data=data, status=status.HTTP_200_OK)


class ListCreateCustomerEscalationAPIView(generics.ListCreateAPIView):
    permission_classes = (IsProviderOrCustomerUser, IsCustomerMSCustomer)
    serializer_class = CustomerEscalationRetrieveSerializer
    pagination_class = None

    def get_queryset(self):
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        queryset = CustomerEscalation.objects.filter(
            user__customer_id=customer_id
        )
        return queryset.order_by("-updated_on")

    def get_serializer_class(self):
        if self.request.method == "POST":
            return CustomerEscalationCreateUpdateSerializer
        else:
            return CustomerEscalationRetrieveSerializer


class RetrieveUpdateDestroyCustomerEscalationAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsProviderOrCustomerUser, IsCustomerMSCustomer)
    serializer_class = CustomerEscalationRetrieveSerializer

    def get_object(self):
        escalation_id = self.kwargs.get("pk")
        try:
            return CustomerEscalation.objects.get(id=escalation_id)
        except CustomerEscalation.DoesNotExist:
            raise NotFound("Not Found.")

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return CustomerEscalationCreateUpdateSerializer
        else:
            return CustomerEscalationRetrieveSerializer

    def perform_destroy(self, instance):
        if self.request.user.type == UserType.CUSTOMER:
            raise PermissionDenied()
        else:
            instance.delete()


class CircuitInfoListCreateAPIView(generics.ListCreateAPIView):
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    permission_classes = (IsProviderOrCustomerUser,)
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("cw_site_id", "circuit_type")

    def _get_customer_id(self):
        customer_id = None
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        return customer_id

    def get_queryset(self):
        customer_id = self._get_customer_id()
        queryset = CircuitInfo.objects.filter(
            customer=customer_id, is_deleted=False
        ).select_related("circuit_type")
        return queryset.order_by("circuit_type__name", "-updated_on")

    def perform_create(self, serializer):
        customer_id = self._get_customer_id()
        data = serializer.validated_data
        data.update({"customer_id": customer_id})
        CircuitInfoService.perform_create(data)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        if self.request.user.is_authenticated:
            context.update({"customer_id": self._get_customer_id()})
        return context

    def get_serializer_class(self):
        if self.request.method == "POST":
            return CircuitInfoCreateUpdateSerializer
        else:
            return CircuitInfoListRetrieveSerializer


class CircuitInfoRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    permission_classes = (IsProviderOrCustomerUser,)

    def _get_customer_id(self):
        customer_id = None
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        return customer_id

    def get_serializer_context(self):
        context = super().get_serializer_context()
        if self.request.user.is_authenticated:
            context.update({"customer_id": self._get_customer_id()})
        return context

    def get_object(self):
        customer_id = self._get_customer_id()
        circuit_info_id = self.kwargs.get("pk")
        try:
            circuit_info = CircuitInfo.objects.filter(
                customer=customer_id
            ).get(id=circuit_info_id)
            return circuit_info
        except:
            raise NotFound()

    def perform_destroy(self, instance):
        if self.request.user.type == UserType.CUSTOMER:
            raise PermissionDenied()
        else:
            CircuitInfoService.perform_delete(instance)

    def perform_update(self, instance):
        instance.save()
        CircuitInfoService.update_historical_data(instance.data, type="update")

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return CircuitInfoCreateUpdateSerializer
        else:
            return CircuitInfoListRetrieveSerializer


class CircuitInfoTypesListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    pagination_class = None

    def get_serializer_class(self):
        show_associated_circuits = self.request.query_params.get(
            "show-associated-circuits", "false"
        )
        if show_associated_circuits == "true":
            return CircuitInfoTypeWithAssociationSerializer
        else:
            return CircuitInfoTypeListCreateSerializer

    def get_queryset(self):
        queryset = CircuitType.objects.filter(
            provider_id=self.request.provider.id
        ).order_by("name")
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class CircuitInfoTypesRetrieveUpdateDeleteAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated,)
    serializer_class = CircuitInfoTypeListCreateSerializer

    def get_queryset(self):
        provider = self.request.provider
        queryset = CircuitType.objects.filter(provider=provider)
        return queryset


class CircuitReportingTypeRetrieveAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    """
    API View to return the types of Circuit report types that can be generated.
    """

    def get(self, request):
        data = CircuitInfoService.get_customer_reporting_types()
        return Response(data=data, status=status.HTTP_200_OK)


class CircuitInfoReportDownloadAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def post(self, request, *args, **kwargs):
        serializer = CircuitInfoReportDownloadSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = None
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        (
            file_path,
            file_name,
        ) = CircuitInfoService.generate_customer_circuit_info_report(
            request.provider.id, customer_id, serializer.validated_data
        )
        if file_path:
            url = FileUploadService.upload_file(file_path)
            data = dict(file_path=url, file_name=file_name)
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)


class TwoFactorAuthenticationRequestAPIView(APIView):
    permission_classes = (IsUntypedToken,)

    def post(self, request, *args, **kwargs):
        serializer = TwoFactorAuthenticationRequestSerializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)
        user = request.user
        if not user.external_auth_id:
            raise ValidationError(
                {"email": "Two Factor Authentication not done."}
            )
        response = (
            TwilioAuthenticationService.request_two_factor_authentication(
                int(user.external_auth_id),
                serializer.validated_data.get("type"),
            )
        )
        return Response(data=response, status=status.HTTP_200_OK)


class TwoFactorAuthenticationValidateAPIView(APIView):
    permission_classes = (IsUntypedToken,)

    def post(self, request, *args, **kwargs):
        user = request.user
        serializer = TwoFactorAuthenticationValidateSerializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)
        if not user.external_auth_id:
            raise ValidationError(
                {"email": "Two Factor Authentication not done."}
            )
        if TwilioAuthenticationService.validate_authentication_request(
            int(user.external_auth_id), serializer.validated_data
        ):
            refresh_token = RefreshToken.for_user(user)
            update_last_login(None, user)
            return Response(
                {"token": AccessToken.for_user(user), "refresh": refresh_token}
            )
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class TwoFactorAuthCallbackAPIView(APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = TwilioAuthenticationService.validate_authy_notify_request(
            settings.AUTHY_CALLBACK_URL, "POST", request.META, request.data
        )
        return Response(status=status.HTTP_200_OK, data=response)


class RequestPhoneVerification(APIView):
    permission_classes = (IsUntypedToken,)

    def post(self, request, *args, **kwargs):
        serializer = RequestPhoneVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        phone = serializer.validated_data.get("phone")
        country_code = serializer.validated_data.get("country_code")
        if phone == TwilioAuthenticationService.hide_phone_number(
            user.profile.cell_phone_number
        ):
            phone = user.profile.cell_phone_number
        response = TwilioAuthenticationService.start_phone_verification(
            phone, country_code
        )
        return Response(data=response, status=status.HTTP_200_OK)


class ValidatePhoneVerification(APIView):
    permission_classes = (IsUntypedToken,)

    def post(self, request, *args, **kwargs):
        serializer = ValidatePhoneVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        phone = serializer.validated_data["phone"]
        if phone == TwilioAuthenticationService.hide_phone_number(
            user.profile.cell_phone_number
        ):
            phone = user.profile.cell_phone_number
        country_code = serializer.validated_data["country_code"]
        code = serializer.validated_data["code"]
        verification = TwilioAuthenticationService.validate_phone_verification(
            phone, country_code, code
        )
        if verification:
            try:
                user.profile.cell_phone_number = phone
                user.profile.country_code = country_code
                twilio_user = TwilioAuthenticationService.create_auth_account(
                    dict(
                        email=user.email,
                        phone=phone,
                        country_code=country_code,
                    )
                )
                user.external_auth_id = twilio_user.id
                user.save()
                user.profile.save()
            except User.DoesNotExist:
                raise NotFound("User does not exist")
        return Response(
            data={"success": verification}, status=status.HTTP_200_OK
        )


class CustomerUserMetricDashboardAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        customer_stats = CustomerService.get_customers_users_metrics(
            request.provider.id
        )
        return Response(customer_stats)


@transaction.atomic
def create_user(
    request,
    user_type,
    serializer,
    provider=None,
    customer=None,
    send_mail=True,
    **kwargs,
):
    """
    helper function to create user
    """
    profile = serializer.validated_data.pop("profile", {})

    data = {**serializer.validated_data, **kwargs}
    # Create User Object
    user = User.objects.create_user(
        user_type=user_type, provider=provider, customer=customer, **data
    )

    # create Profile Object
    Profile.objects.create(user=user, **profile)

    logger.debug(f"User created: {user}")

    # # Send Account Activation email
    if send_mail:
        try:
            send_activation_mail_to_user(request, user)
        except SMTPRecipientsRefused:
            raise ValidationError("Invalid email address.")


class LoggedEmailDataListAPIView(ListAPIView):
    serializer_class = LoggedEmailDataSerializer
    permission_classes = [IsAuthenticated, IsProviderUser]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = [
        "created",
    ]
    search_fields = [
        "email_data",
        "created",
    ]

    def get_queryset(self):
        return StoredEmailData.objects.filter(
            provider=self.request.provider
        ).order_by("-created")


class LoggedEmailDataDestroyAPIView(DestroyAPIView):
    serializer_class = LoggedEmailDataSerializer
    permission_classes = [IsAuthenticated, IsProviderUser]

    def get_queryset(self):
        return StoredEmailData.objects.filter(provider=self.request.provider)


class ResendLoggedEmailAPIView(APIView):
    permission_classes = [IsAuthenticated, IsProviderUser]

    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        try:
            logged_email = StoredEmailData.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response(
                {"message": "No email to resend."},
                status=status.HTTP_404_NOT_FOUND,
            )

        if logged_email:
            email_data = logged_email.email_data
            BaseEmailMessage(
                subject=email_data.get("subject"),
                images=email_data.get("images"),
                files=email_data.get("files"),
                remote_attachments=email_data.get("remote_attachments"),
                template_name=email_data.get("template_name"),
                from_email=email_data.get("from_email"),
                reply_to=email_data.get("reply_to"),
                body=email_data.get("body"),
            ).resend(
                to=email_data.get("to"),
                cc=email_data.get("cc"),
                bcc=email_data.get("bcc"),
            )
            return Response(
                {"message": "Successfully resend the email."},
                status=status.HTTP_200_OK,
            )
        return Response(
            {"message": "No email to resend."},
            status=status.HTTP_404_NOT_FOUND,
        )


class FeatureStatusListCreateAPIView(ListCreateAPIView):
    """
    API view to create, and retrieve Feature status for the provider.
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)
    pagination_class = None
    serializer_class = FeatureStatusSerializer

    def get_queryset(self):
        queryset = FeatureStatus.objects.all()
        return queryset

    def perform_create(self, serializer):
        serializer.save()
        # create feature data in user Profile
        for serialized_data in serializer.data:
            serialized_data = json.loads(
                json.dumps(serialized_data, cls=DjangoJSONEncoder)
            )
            Profile.objects.filter(user_id=serialized_data.get("user")).update(
                feature=[serialized_data]
            )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class FeatureStatusRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    """
    API view to retrieve and update Feature status for the provider.
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)
    pagination_class = None
    serializer_class = FeatureStatusSerializer

    def get_queryset(self):
        queryset = FeatureStatus.objects.all()
        return queryset

    def perform_update(self, serializer):
        serializer.save()
        # update feature data in user Profile
        self.request.data["id"] = serializer.data.get("id")
        Profile.objects.filter(user_id=self.request.data.get("user")).update(
            feature=[self.request.data]
        )


class FeatureWiseAccessListCreateUpdateAPIView(generics.ListCreateAPIView):
    """
    API view to create, and retrieve Feature access for the provider users.
    """

    permission_classes = (IsAuthenticated,)
    pagination_class = None

    def get_queryset(self):
        feature = self.request.query_params.get("feature")
        if feature:
            queryset = FeatureStatus.objects.filter(
                feature=feature, enabled=True
            )
        else:
            queryset = FeatureStatus.objects.filter(enabled=True)
        return queryset

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        for item in serializer.validated_data:
            users = item.pop("users")
            feature = item.get("feature")
            enabled_status = item.get("enabled")

            # Update feature access for multiple users
            deleted_feature_data = FeatureStatus.objects.filter(
                feature=feature
            ).exclude(user__in=users)
            # update the profile model enabled status
            for data in deleted_feature_data:
                for _data in data.user.profile.feature:
                    if _data.get("feature") == feature:
                        _data["enabled"] = False
                # Save the updated user profile feature data
                data.user.profile.save()
            deleted_feature_data.delete()
            existing_users = FeatureStatus.objects.filter(
                feature=feature, user__in=users
            )
            existing_users_users = existing_users.values_list(
                "user", flat=True
            )
            new_users = [
                user for user in users if user.pk not in existing_users_users
            ]
            new_instances = [
                FeatureStatus(
                    user=user, feature=feature, enabled=enabled_status
                )
                for user in new_users
            ]
            feature_status_data = FeatureStatus.objects.bulk_create(
                new_instances
            )
            # update the profile model
            for data in feature_status_data:
                data = json.loads(
                    json.dumps(model_to_dict(data), cls=DjangoJSONEncoder)
                )
                profile_data = Profile.objects.filter(user_id=data.get("user"))
                if profile_data[0].feature:
                    _feature_data = profile_data[0].feature
                    _feature_data.append(data)
                    profile_data.update(feature=_feature_data)
                else:
                    profile_data.update(feature=[data])
        return Response(
            data=serializer.initial_data, status=status.HTTP_200_OK
        )

    def get_serializer_class(self):
        if self.request.method == "GET":
            return FeatureStatusGetSerializer
        else:
            return FeatureStatusSerializer


class FeaturesListAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = FeatureStatus.FEATURES_LIST
        return Response(data=data, status=status.HTTP_200_OK)


class MergingCustomerAPIView(APIView):
    permission_classes = [IsAuthenticated, IsProviderUser, IsTwoFAVerified]

    def post(self, request, *args, **kwargs):
        serializer = MergingCustomerSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        provider: Provider = self.request.provider
        customer_to_be_merge = serializer.validated_data.get(
            "source_customer_id"
        )
        customer_in_which_merge = serializer.validated_data.get(
            "target_customer_id"
        )
        customer_to_be_merge_id: int = customer_to_be_merge.id
        customer_in_which_merge_id: int = customer_in_which_merge.id
        # Fetching all models related to customer model
        customer_related_models = fetch_related_models_to_customers(
            model_name="Customer"
        )
        merge_info = []
        merge_dict = dict(
            source_customer_id=customer_to_be_merge_id,
            source_customer_name=customer_to_be_merge.name,
            target_customer_id=customer_in_which_merge_id,
            target_customer_name=customer_in_which_merge.name,
        )
        try:
            for customer_related_model in customer_related_models:
                # get related model instance and data
                customer_related_model = customer_related_model.related_model
                customer_related_model_data = (
                    customer_related_model.objects.filter(
                        customer_id=customer_to_be_merge_id
                    )
                )
                try:
                    if customer_related_model_data.exists():
                        # put all the related model data id in the merge dict
                        merge_dict[
                            customer_related_model._meta.object_name
                        ] = [
                            related_data.id
                            for related_data in customer_related_model_data
                        ]
                        # update the target customer id to the related model
                        customer_related_model_data.update(
                            customer_id=customer_in_which_merge_id
                        )
                    else:
                        # if no data found in the related model
                        merge_dict[
                            customer_related_model._meta.object_name
                        ] = "No Related Data found to merge"
                except Exception as e:
                    logger.error(
                        f"Error while merging customer :: source customer {customer_to_be_merge_id} "
                        f"and target customer { customer_in_which_merge_id} {e}"
                    )
                    break
            merge_info.append(merge_dict)
        except Exception as error:
            logger.error(
                f"Error while fetching related model to the source customer {customer_to_be_merge.name}"
            )
            return Response(
                {"message": error}, status=status.HTTP_400_BAD_REQUEST
            )

        # delete the source company
        customer_to_be_merge.delete()

        # upload log files into S3
        # base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # configs_file_dir = os.path.join(base_dir, "logs")
        # file_name = "data.json"
        # file_path = os.path.join(configs_file_dir, file_name)
        # f = open(file_path, 'w+')  # open file in write mode
        # f.write(json.dumps(merge_info, cls=DjangoJSONEncoder))
        # f.close()
        # # with open(file_path, 'w+') as file_path:
        # #     file_path.write(json.dumps(merge_info, cls=DjangoJSONEncoder))
        # FileUploadService.upload_logs_file(file_path)
        return Response(
            {"message": merge_info},
            status=status.HTTP_200_OK,
        )


class User2FAConfigStatusAPIView(generics.RetrieveAPIView):
    """
    API view to get user's 2FA config status.
    """

    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request, *args, **kwargs):
        user: User = get_object_or_404(
            User.objects.all(),
            provider_id=self.request.provider.id,
            id=self.kwargs.get("pk"),
        )
        two_fa_configured: bool = False if not user.external_auth_id else True
        # Untyped token is required for requesting phone validation which is requested if user's phone
        # is not registered for 2FA.
        untyped_token: Optional[str] = (
            None
            if two_fa_configured
            else UntypedToken.for_user(self.request.user, expiry=300)
        )
        return Response(
            data=dict(
                two_fa_configured=two_fa_configured, token=untyped_token
            ),
            status=status.HTTP_200_OK,
        )


class AuthyOTPValidatorAPIView(generics.CreateAPIView):
    """
    API view to validate 2FA using Authy OTP.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = AuthyOTPSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            is_valid: bool = (
                TwilioAuthenticationService.validate_authentication_request(
                    int(self.request.user.external_auth_id),
                    data=dict(
                        type="AUTHY_APP",
                        code=serializer.validated_data.get("otp"),
                    ),
                )
            )
            # Provide a token if 2FA is successful. The token is used to confirm that 2FA is successful.
            token: Optional[str] = (
                UntypedToken.for_user(self.request.user, expiry=300)
                if is_valid
                else None
            )
            return Response(
                data=dict(is_valid=is_valid, token=token),
                status=status.HTTP_200_OK,
            )
