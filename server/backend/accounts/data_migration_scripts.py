from django.contrib.auth.models import Group
from guardian.models import GroupObjectPermission

from accounts import groups
from accounts.models import Provider, Customer, User
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


def deprecated_get_provider_group(provider_name, role):
    group_name = "{0} {1}".format(provider_name, role)
    try:
        group = Group.objects.get(name=group_name)
    except Group.DoesNotExist:
        return None
    return group


def deprecated_get_customer_group(provider_name, customer_name, role):
    group_name = "{0} {1} {2}".format(provider_name, customer_name, role)
    try:
        group = Group.objects.get(name=group_name)
    except Group.DoesNotExist:
        return None
    return group


def update_group_names():
    providers = Provider.objects.only("name", "id")
    roles = ["owner", "admin", "staff"]
    for provider in providers:
        for role in roles:
            old_group = deprecated_get_provider_group(provider.name, role)
            if old_group:
                old_group.name = "{0} {1}".format(provider.id, role)
                old_group.save(update_fields=["name"])
        logger.debug(f"Provider: {provider} groups names updated.")

    customers = Customer.objects.only("name", "id")
    for customer in customers:
        for role in roles:
            old_group = deprecated_get_customer_group(
                customer.provider.name, customer.name, role
            )
            if old_group:
                old_group.name = "{0} {1} {2}".format(
                    customer.provider.id, customer.id, role
                )
                old_group.save(update_fields=["name"])
        logger.debug(f"Customer: {customer} group names updated.")


def update_acela_group_model_permissions():
    groups.assign_model_permissions_to_acelauser_group()


def update_provider_group_model_permissions(provider_id):
    groups.assign_model_permissions_to_provider(provider_id)


def update_customer_group_model_permissions(provider_id, customer_id):
    groups.assign_model_permissions_to_customer(provider_id, customer_id)


def update_user_model_object_permissions():
    for user in User.objects.all():
        GroupObjectPermission.objects.filter(object_pk=user.id).delete()
        groups.assign_user_model_obj_permissions(user)


def update_provider_model_object_permissions():
    for provider in Provider.objects.all():
        groups.assign_provider_model_obj_permissions(provider)


def update_customer_model_object_permissions():
    for customer in Customer.objects.all():
        groups.assign_customer_model_obj_permissions(customer)


def update_all_model_permissions():
    # Update model permissions
    update_acela_group_model_permissions()

    for provider in Provider.objects.all():
        update_provider_group_model_permissions(provider.id)

    for customer in Customer.objects.all():
        update_customer_group_model_permissions(
            customer.provider.id, customer.id
        )


def update_all_object_permissions():
    # Update all object permissions
    update_user_model_object_permissions()
    update_provider_model_object_permissions()
    update_customer_model_object_permissions()
