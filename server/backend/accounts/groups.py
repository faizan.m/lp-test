from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.db import transaction
from guardian.models import GroupObjectPermission
from guardian.shortcuts import assign_perm

from accounts import utils
from utils import get_app_logger
from .models import Role, UserType, User

# Get an instance of a logger
logger = get_app_logger(__name__)


def create_initial_user_types_and_roles():
    """
    Creates initial user types and roles in the database
    :return: none
    """
    user_types = ["accelavar", "provider", "customer"]
    roles = ["owner", "admin", "staff"]
    for type in user_types:
        user_type = UserType.objects.create(name=type)
        logger.info('Successfully created User Type "%s"' % user_type.name)
        for role in roles:
            role = Role.objects.create(role_name=role, user_type=user_type)
            logger.info('Successfully created Role "%s"' % role.role_name)


def _create_user_groups(prefix, user_type, roles=None):
    """
    Create Groups for different types of roles based on user type.
    Example: if Prefix = X , user_type = Provider, roles = ['owner', 'admin', 'staff']
     3 groups are created i.e. X owner, X admin, X staff
    :param prefix: <'provider_id'>  for Provider user,
                   <'provider_id customer_id'>  for Customer user
                   <'accelavar'> for accelavar user
    :type prefix: string
    :param user_type: Type of User for which groups are to created
    :type user_type: string
    :param roles: list of roles
    :return: none
    """
    if not roles:
        roles = Role.objects.filter(user_type__name=user_type).values_list(
            "role_name", flat=True
        )
    for role in roles:
        group_name = "{prefix} {role}".format(prefix=prefix, role=role)
        Group.objects.get_or_create(name=group_name)
        logger.info("Group {name} created.".format(name=group_name))


def create_superuser_user_groups():
    """
    Create user groups for accelavar
    :return: none
    """
    name = "{0} {1}".format(settings.SUPER_USER_PORTAL_NAME, "owner")
    if not Group.objects.filter(name=name).exists():
        _create_user_groups(
            prefix=settings.SUPER_USER_PORTAL_NAME,
            user_type=UserType.ACCELAVAR,
        )
        logger.info(
            "Groups created for {0}".format(settings.SUPER_USER_PORTAL_NAME)
        )

        # Assign Model level Permissions to accelavar Owner Group
        assign_model_permissions_to_acelauser_group()


def create_provider_user_groups(provider_id):
    """
    Creates groups for Provider
    :param provider_id: name of the provider
    :type provider_id: string
    :return: none
    """
    _create_user_groups(prefix=provider_id, user_type=UserType.PROVIDER)
    logger.info("Groups created for {0}".format(provider_id))

    # Assign Model level Permissions to Provider Owner Group
    assign_model_permissions_to_provider(provider_id)


def create_customer_user_groups(provider_id, customer_id):
    """
    Creates groups for Provider Customer
    :param provider_id: Provider name for the Customer
    :type provider_id: string
    :param customer_id: Customer name
    :type customer_id: string
    :return: none
    """
    _create_user_groups(
        prefix="{0} {1}".format(provider_id, customer_id),
        user_type=UserType.CUSTOMER,
    )
    logger.info("Groups created for {0}".format(customer_id))

    # Assign Model level Permissions to Customer Owner Group
    assign_model_permissions_to_customer(provider_id, customer_id)


def assign_group_to_user(user=None, user_id=None):
    """
    Assigns group to User based on the role.
    :param user: User Model Object
    :type user: User
    :param user_id: UUID of the user
    :type user_id: string
    :param email: email of the user
    :type email: string
    :return: none
    """
    if user_id:
        user = User.objects.get(id=user_id)
    if not user:
        raise ValueError("Both user and user_id can not be None.")

    if user.type == UserType.CUSTOMER:
        group = get_customer_group(
            user.provider.id, user.customer.id, user.role
        )
    elif user.type == UserType.PROVIDER:
        group = get_provider_group(user.provider.id, user.role)
    else:
        group = get_superuser_group(user.role)

    if group:
        user.groups.add(group)
        logger.info("{user} added to {group}".format(user=user, group=group))
    else:
        logger.warning("No group found.")


def _assign_model_level_permissions_to_group(group, permissions):
    """
    assigns list of permissions to the group
    :param group: group object
    :type group: Group Model object
    :param permissions: list of model permissions
    :type permissions: list
    :return: none
    """
    for perm in permissions:
        permission = Permission.objects.get(codename=perm)
        group.permissions.add(permission)


@transaction.atomic
def change_user_role(user):
    """
    Assign new role to user object.
    :param user: user object to be updated
    :type user: User model object
    :return: none
    """

    # Remove user from the existing group
    if user.role:
        current_group = user.groups.first()
        if current_group:
            user.groups.remove(current_group)

    # Assign new user to group
    assign_group_to_user(user=user)
    # Update group permissions on object
    update_user_object_permissions(user)


def get_superuser_group(role):
    """
    returns a superuser group for the role
    :param role: role name
    :type role: string
    :return: Group object
    """
    group_name = "{0} {1}".format(settings.SUPER_USER_PORTAL_NAME, role)
    group = Group.objects.get(name=group_name)
    return group


def get_provider_group(provider_id, role):
    """
    returns a group for provider for the role
    :param provider_id: name of the provider
    :type provider_id: string
    :param role: role name
    :type role: string
    :return: Group object
    """
    group_name = "{0} {1}".format(provider_id, role)
    group = Group.objects.get(name=group_name)
    return group


def get_customer_group(provider_id, customer_id, role):
    """
    returns a group for the customer for the role
    :param provider_id: name of the provider
    :type provider_id: string
    :param customer_id: name of the customer
    :type customer_id: string
    :param role: role name
    :type role: string
    :return: Group Object
    """
    group_name = "{0} {1} {2}".format(provider_id, customer_id, role)
    group = Group.objects.get(name=group_name)
    return group


# Model Permission functions


def assign_model_permissions_to_acelauser_group():
    """
    assigns model level permissions to superuser owner group
    :return: none
    """

    # List of applicable model permissions for accelavar Owner Group
    owner_permissions = [
        "add_superuser_user",
        "add_provider_user",
        "add_provider",
    ]
    admin_permissions = ["add_superuser_user", "add_provider_user"]
    staff_permissions = ["add_superuser_user"]
    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    for role, permissions in roles_and_permissions.items():
        group = get_superuser_group(role)
        _assign_model_level_permissions_to_group(
            permissions=permissions, group=group
        )


def assign_model_permissions_to_provider(provider_id):
    """
    assigns model level permissions to provider owner group
    :param provider_id: name of the provider
    :type provider_id: string
    :return: none
    """

    # List of applicable model permissions for Provider Owner Group
    owner_permissions = ["add_provider_user", "add_customer_user"]
    admin_permissions = ["add_provider_user", "add_customer_user"]
    staff_permissions = ["add_customer_user"]

    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    for role, permissions in roles_and_permissions.items():
        group = get_provider_group(provider_id, role)
        _assign_model_level_permissions_to_group(
            permissions=permissions, group=group
        )


def assign_model_permissions_to_customer(provider_id, customer_id):
    """
    assigns model level permissions to customer owner group
    :param provider_id: name of the provider
    :type provider_id: string
    :param customer_id: name of the customer
    :type customer_id: string
    :return: none
    """
    # List of applicable model permissions for Customer Owner Group
    owner_permissions = ["add_customer_user"]
    admin_permissions = ["add_customer_user"]
    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, []
    )
    for role, permissions in roles_and_permissions.items():
        group = get_customer_group(provider_id, customer_id, role)
        _assign_model_level_permissions_to_group(
            permissions=permissions, group=group
        )


# Model Object Permission functions


def _assign_obj_permissions(permissions, group, obj):
    """
    assign list of permissions to the group over the object
    :param permissions: list of permissions to be assigned
    :type permissions: list
    :param group: user group
    :type group: Group model object
    :param obj: object for which permissions are to assigned
    :type obj: any object
    :return: none
    """
    for perm in permissions:
        assign_perm(perm, group, obj)


# User Model
# ----------------
def _assign_object_permissions_to_groups(
    user_types, roles_and_permissions, obj, **kwargs
):
    """
    assigns obj permissions to all the groups of user_type in user_types
    :param user_types: list of UserTypes for which permissions are to assigned.
    :type user_types: list
    :param roles_and_permissions: a dict of various roles for the user type along
    with thier permissions.
    Example:
    {'owner': ['change_user', 'view_user', 'delete_user'],
    'admin': ['view_user'],
    'staff': ['view_user']}
    :type roles_and_permissions: dict
    :param obj: object for which permissions are to be assigned
    :param kwargs: if user_types contains UserType.PROVIDER type then provider_id is required.
    if user_types contains UserType.CUSTOMER type then provider_id and customer_id are required.
    :return: none
    """
    for user_type in user_types:
        if user_type == UserType.ACCELAVAR:
            for role, permissions in roles_and_permissions.items():
                group = get_superuser_group(role)
                _assign_obj_permissions(permissions, group, obj)
        elif user_type == UserType.PROVIDER:
            for role, permissions in roles_and_permissions.items():
                group = get_provider_group(kwargs.get("provider_id"), role)
                _assign_obj_permissions(permissions, group, obj)
        elif user_type == UserType.CUSTOMER:
            for role, permissions in roles_and_permissions.items():
                group = get_customer_group(
                    kwargs.get("provider_id"), kwargs.get("customer_id"), role
                )
                _assign_obj_permissions(permissions, group, obj)


def _assign_user_model_obj_permissions(
    user_types,
    user,
    owner_permissions,
    admin_permissions,
    staff_permissions,
    *args,
    **kwargs
):
    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    _assign_object_permissions_to_groups(
        user_types, roles_and_permissions, user, *args, **kwargs
    )


def assign_user_model_obj_permissions(user):
    """
    assign user object permissions to various groups based on user type
    :param user: User model object
    :return: none
    """
    view_user = "view_user"
    edit_user = "change_user"
    delete_user = "delete_user"
    owner_permissions, admin_permissions, staff_permissions = (
        [view_user, edit_user, delete_user],
        [view_user],
        [view_user],
    )
    if user.role == Role.ADMIN:
        admin_permissions.append(edit_user)
    elif user.role == Role.STAFF:
        admin_permissions.append(edit_user)
        staff_permissions.append(edit_user)

    if user.type == UserType.ACCELAVAR:
        user_types = [UserType.ACCELAVAR]
        _assign_user_model_obj_permissions(
            user_types,
            user,
            owner_permissions,
            admin_permissions,
            staff_permissions,
        )
    elif user.type == UserType.PROVIDER:
        # Assign permissions to Provider groups
        user_types = [UserType.PROVIDER]
        _assign_user_model_obj_permissions(
            user_types,
            user,
            owner_permissions,
            admin_permissions,
            staff_permissions,
            provider_id=user.provider.id,
        )
        # Assign permissions to Acela groups
        staff_permissions = [view_user]
        user_types = [UserType.ACCELAVAR]
        _assign_user_model_obj_permissions(
            user_types,
            user,
            owner_permissions,
            admin_permissions,
            staff_permissions,
            provider_id=user.provider.id,
        )

    elif user.type == UserType.CUSTOMER:
        # Assign permissions to Customer groups
        user_types = [UserType.CUSTOMER]
        _assign_user_model_obj_permissions(
            user_types,
            user,
            owner_permissions,
            admin_permissions,
            staff_permissions,
            provider_id=user.provider.id,
            customer_id=user.customer.id,
        )
        # Assign permissions to Provider Groups
        staff_permissions = [view_user]
        user_types = [UserType.PROVIDER]
        _assign_user_model_obj_permissions(
            user_types,
            user,
            owner_permissions,
            admin_permissions,
            staff_permissions,
            provider_id=user.provider.id,
            customer_id=user.customer.id,
        )


def update_user_object_permissions(user):
    """
    Deletes all previous permissions on the object and assigns new permissions.
    """
    GroupObjectPermission.objects.filter(object_pk=user.id).delete()
    assign_user_model_obj_permissions(user)


# Provider Model
# -------------------


def assign_provider_model_obj_permissions(provider):
    """
    assigns Provider model object permissions
    :param provider: Provider model object
    :return: none
    """
    owner_permissions = ["change_provider", "view_provider", "delete_provider"]
    admin_permissions = ["change_provider", "view_provider"]
    staff_permissions = ["view_provider"]
    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    user_types = [UserType.ACCELAVAR]
    _assign_object_permissions_to_groups(
        user_types, roles_and_permissions, provider
    )


# Customer Model
# -------------------


def assign_customer_model_obj_permissions(customer):
    """
    assigns Customer model object permissions
    :param customer: Customer model object
    :return: none
    """
    owner_permissions = ["change_customer", "view_customer", "delete_customer"]
    admin_permissions = ["change_customer", "view_customer"]
    staff_permissions = ["change_customer", "view_customer"]

    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    user_types = [UserType.PROVIDER]
    _assign_object_permissions_to_groups(
        user_types,
        roles_and_permissions,
        customer,
        provider_id=customer.provider.id,
    )

    owner_permissions = ["change_customer", "view_customer", "delete_customer"]
    admin_permissions = ["change_customer", "view_customer"]
    staff_permissions = ["view_customer"]

    roles_and_permissions = utils.get_roles_permissions_dict(
        owner_permissions, admin_permissions, staff_permissions
    )
    user_types = [UserType.CUSTOMER]
    _assign_object_permissions_to_groups(
        user_types,
        roles_and_permissions,
        customer,
        provider_id=customer.provider.id,
        customer_id=customer.id,
    )
