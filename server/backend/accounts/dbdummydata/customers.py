from accounts.tests.factories import CustomerFactory


def create_customer(provider):
    customer = CustomerFactory(provider=provider)
    return customer
