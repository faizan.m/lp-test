from accounts.models import Address, AccountingContact
from accounts.tests.factories import ProviderFactory


def create_providers():
    address_data = {
        "address_1": "Velotio",
        "address_2": "Websym",
        "city": "Pune",
        "state": "M.H.",
        "zip_code": "123456",
    }
    address = Address.objects.create(**address_data)

    accounting_contact = {
        "first_name": "Ram",
        "last_name": "Kumar",
        "email": "ram@example.com",
        "website": "web.com",
    }
    accounting_contact = AccountingContact.objects.create(**accounting_contact)

    # Provider One
    provider_one = ProviderFactory(
        name="velotio",
        url="velotio.acela.io",
        http_protocol="http://",
        address=address,
        accounting_contact=accounting_contact,
    )

    address_data = {
        "address_1": "Dell",
        "address_2": "Dell",
        "city": "Pune",
        "state": "M.H.",
        "zip_code": "123456",
    }
    address = Address.objects.create(**address_data)

    accounting_contact = {
        "first_name": "Henry",
        "last_name": "Code",
        "email": "henry@example.com",
        "website": "dell.com",
    }
    accounting_contact = AccountingContact.objects.create(**accounting_contact)

    # Provider Two
    provider_two = ProviderFactory(
        name="dell",
        url="dell.acela.io",
        http_protocol="http://",
        address=address,
        accounting_contact=accounting_contact,
    )

    return [provider_one, provider_two]
