from faker import Faker

from accounts.models import UserType, Role, Profile
from accounts.tests.factories import UserFactory


def create_superuser_users():
    create_superuser_owner()
    create_superuser_admin()
    create_superuser_staff()


def create_provider_users(provider):
    create_provider_owner(provider, [f"{provider.name}ownerone@example.com"])
    create_provider_admin(provider, [f"{provider.name}adminone@example.com"])
    create_provider_staff(provider, [f"{provider.name}staffone@example.com"])


def create_customer_users(provider, customer):
    create_customer_owner(
        provider, customer, [f"{provider.name}customerone@example.com"]
    )
    create_customer_admin(
        provider, customer, [f"{provider.name}customertwo@example.com"]
    )
    create_customer_staff(
        provider, customer, [f"{provider.name}customerthree@example.com"]
    )


def create_users(
    emails,
    user_type,
    role,
    provider=None,
    customer=None,
    password="password123",
):
    for email in emails:
        user = UserFactory(
            email=email,
            provider=provider,
            customer=customer,
            user_type=user_type,
            user_role=role,
        )
        user.set_password(password)
        user.save()
        # Create User Profile
        Profile.objects.create(user=user)


def create_superuser_owner():
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    emails = [
        "superownerone@example.com",
        "superownertwo@example.com",
        "madhur@velotio.com",
    ]
    create_users(emails, user_type, role)


def create_superuser_admin():
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    emails = ["superadminone@example.com", "superadmintwo@example.com"]
    create_users(emails, user_type, role)


def create_superuser_staff():
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    emails = ["superstaffone@example.com", "superstafftwo@example.com"]
    create_users(emails, user_type, role)


def create_provider_owner(provider, emails):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    create_users(emails, user_type, role, provider=provider)


def create_provider_admin(provider, emails):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    create_users(emails, user_type, role, provider=provider)


def create_provider_staff(provider, emails):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    create_users(emails, user_type, role, provider=provider)


def create_customer_owner(provider, customer, emails):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    create_users(emails, user_type, role, provider, customer)


def create_customer_admin(provider, customer, emails):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    create_users(emails, user_type, role, provider, customer)


def create_customer_staff(provider, customer, emails):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    create_users(emails, user_type, role, provider, customer)


def generate_emails(count=2):
    fake = Faker()
    emails = []
    for _ in range(count):
        first_name = fake.first_name()
        emails.append(f"{first_name.lower()}@example.com")
    return emails
