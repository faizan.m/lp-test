from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from accounts.models import User, UserType
from core.integrations import Connectwise


class UserUpdateModelMixin(object):
    """
    Apply this mixin to raise Permission Denied exception if user tries to
    set is_active=False for self or
    change role for self.
    """

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        if instance == self.request.user:
            if instance.is_active != serializer.validated_data.get(
                "is_active"
            ):
                raise PermissionDenied()
            if instance.user_role != serializer.validated_data.get(
                "user_role"
            ):
                raise PermissionDenied()

        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class CustomerUserUpdateMixin:
    """
    Mixin for updating customer user details. In case of customer user
    some information need to be synced back to connectwise.
    """

    def perform_update(self, serializer):
        user: User = serializer.instance
        if not user.has_usable_password():
            serializer.validated_data.pop("is_active", None)
        if user.type == UserType.CUSTOMER:
            # if phone number is updated
            profile = serializer.validated_data.get("profile", {})
            if (
                user.profile.cell_phone_number
                != profile.get("cell_phone_number")
            ) or (user.profile.office_phone != profile.get("office_phone")):
                profile |= {
                    "cell_phone_number_crm_id": user.profile.cell_phone_number_crm_id,
                    "office_phone_crm_id": user.profile.office_phone_crm_id,
                }
                serializer.validated_data["profile"] = profile
            erp_client: Connectwise = self.request.provider.erp_client
            erp_client.update_customer_user(
                user_id=user.crm_id,
                data=serializer.validated_data,
            )
        serializer.save()
