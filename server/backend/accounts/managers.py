from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("Email must be set.")
        email = self.normalize_email(email.lower())
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        from .models import UserType, Role

        user_type = UserType.objects.get(name=UserType.ACCELAVAR)
        role = Role.objects.get(user_type=user_type, role_name=Role.OWNER)
        extra_fields.setdefault("user_type", user_type)
        extra_fields.setdefault("user_role", role)
        return self._create_user(email, password, **extra_fields)

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("customer", "provider", "user_type", "user_role")
            .prefetch_related("profile")
        )
