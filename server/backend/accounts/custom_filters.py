"""
Provides customer filtering backends that can be used to filter the results
returned by list views.
"""
from rest_framework.filters import BaseFilterBackend


class UserOrderingFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        ordering = request.query_params.get("ordering")
        if ordering == "role":
            queryset = queryset.order_by("user_role__role_name")
        elif ordering == "-role":
            queryset = queryset.order_by("-user_role__role_name")
        elif ordering == "-name":
            queryset = queryset.order_by("first_name", "last_name")
        elif ordering == "name":
            queryset = queryset.order_by("-first_name", "-last_name")
        return queryset
