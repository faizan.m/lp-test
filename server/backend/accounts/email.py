from django.conf import settings
from django.contrib.auth.tokens import default_token_generator

from accounts import utils
from accounts.mail import BaseEmailMessage
from accounts.utils import get_port_number_from_url, build_url
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


class ActivationEmail(BaseEmailMessage):
    template_name = "emails/account_activation.html"

    def get_context_data(self):
        context = super().get_context_data()

        user = context.get("user")
        domain = context.get("domain")
        token = default_token_generator.make_token(user)
        url = settings.PASSWORD_RESET_CONFIRM_URL
        url = utils.get_security_url(user, token, url, domain)
        context["url"] = url
        return context


class PasswordResetEmail(BaseEmailMessage):
    template_name = "emails/password_reset.html"

    def get_context_data(self):
        context = super().get_context_data()

        user = context.get("user")
        domain = context.get("domain")
        token = default_token_generator.make_token(user)
        url = settings.PASSWORD_RESET_CONFIRM_URL
        url = utils.get_security_url(user, token, url, domain)
        context["url"] = url
        return context


class PasswordSetSuccessEmail(BaseEmailMessage):
    template_name = "emails/password_set_success.html"


class AccountActivationSuccess(BaseEmailMessage):
    template_name = "emails/account_activation_success.html"


class RenewalRequestEmail(BaseEmailMessage):
    template_name = "emails/renewal_request.html"


class ReportRequestEmail(BaseEmailMessage):
    template_name = "emails/report_request.html"


class DeviceReportRequestEmail(BaseEmailMessage):
    template_name = "emails/device_report_request.html"


class ScheduledDeviceReportRequestEmail(BaseEmailMessage):
    template_name = "emails/scheduled_device_report_request.html"


class DevicesReportsBundleRequestEmail(BaseEmailMessage):
    template_name = "emails/device_reports_bundle.html"


class CustomerDistributionUpdateEmail(BaseEmailMessage):
    template_name = "emails/customer_distribution_update.html"


class SoWDocumentUpdateEmail(BaseEmailMessage):
    template_name = "emails/sow_document_update.html"


def send_activation_mail_to_user(request, user):
    port = get_port_number_from_url(request.META["HTTP_ORIGIN"])
    url = build_url(user.get_domain(), port)
    context = {"user": user, "domain": url}
    to = [user.email]
    images = [("logo.png", "logo")]
    ActivationEmail(request, context, images=images).send(to)
    user.activation_mails_sent_count += 1
    user.save()
    logger.info(f"Account Activation mail sent at {user.email}")
