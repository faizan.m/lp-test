from celery import shared_task

from accounts.email import (
    CustomerDistributionUpdateEmail,
    AccountActivationSuccess,
    PasswordSetSuccessEmail,
)
from accounts.models import Provider, Customer, User
from accounts.utils import get_login_url


@shared_task
def send_customer_distribution_update_email(
    customer_id, provider_id, customer_distribution
):
    provider = Provider.objects.get(id=provider_id)
    customer = Customer.objects.get(id=customer_id)
    customer_distribution["Staffing Changes"] = customer_distribution.pop(
        "staffing_changes"
    )
    customer_distribution["Monitoring Alerts"] = customer_distribution.pop(
        "monitoring_alerts"
    )
    customer_distribution["Scheduled Reports"] = customer_distribution.pop(
        "scheduled_reports"
    )
    customer_distribution[
        "Alerts and Notifications"
    ] = customer_distribution.pop("alerts_and_notifications")

    context = dict(
        customer=customer,
        customer_distribution=customer_distribution,
        provider=provider,
    )
    CustomerDistributionUpdateEmail(context=context).send(
        [provider.support_contact]
    )


@shared_task
def send_account_activation_success_email(user_id, http_origin):
    user = User.objects.get(id=user_id)
    to_ = [user.email]
    context = dict(url=get_login_url(user, http_origin), user=user)
    AccountActivationSuccess(context=context).send(to_)


@shared_task
def send_password_set_success_email(user_id, http_origin):
    user = User.objects.get(id=user_id)
    to_ = [user.email]
    context = dict(url=get_login_url(user, http_origin), user=user)
    PasswordSetSuccessEmail(context=context).send(to_)
