from rest_framework import serializers

from accounts.models import CircuitInfo


class CircuitImportFieldMappingSerializer(serializers.Serializer):
    cw_site_id = serializers.CharField()
    circuit_type = serializers.CharField()
    provider = serializers.CharField()
    circuit_id = serializers.CharField()
    contact_phone = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    note = serializers.CharField(required=False)
    ip_address = serializers.CharField(required=False)
    bandwidth = serializers.CharField(required=False)
    bandwidth_measurement = serializers.CharField(required=False)
    account_number = serializers.CharField(required=False)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs.get("bandwidth") and not attrs.get("bandwidth_measurement"):
            raise serializers.ValidationError(
                {
                    "bandwidth_measurement": "This field is required since, Bandwidth is selected."
                }
            )
        return attrs


class CircuitImportSerializer(serializers.Serializer):
    file_path = serializers.CharField(required=True)
    field_mappings = CircuitImportFieldMappingSerializer()
    site_mapping = serializers.JSONField(required=True)
    circuit_type_mapping = serializers.JSONField(required=True)


class CircuitInfoImportSerializer(serializers.ModelSerializer):
    def validate_bandwidth(self, value):
        if value:
            return value.upper()
        return value

    def validate(self, attrs):
        # validation to handle duplicate records in import file
        super().validate(attrs)
        visited_circuit_info_pair = self.context.setdefault(
            "visited_circuit_records", []
        )
        current_record = attrs.get("circuit_id"), attrs.get("circuit_type").id
        if current_record not in visited_circuit_info_pair:
            visited_circuit_info_pair.append(current_record)
            self.context["visited_circuit_records"] = visited_circuit_info_pair
        else:
            raise serializers.ValidationError(
                {
                    "circuit_id": "Duplicate record based on circuit Id and circuit Type"
                }
            )
        return attrs

    class Meta:
        model = CircuitInfo
        exclude = ["customer"]
        validators = []
