from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from django.contrib.auth.password_validation import validate_password
from django.core.validators import FileExtensionValidator
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices
from rest_framework import serializers, exceptions
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework_jwt.compat import PasswordField
from rest_framework_jwt.serializers import JSONWebTokenSerializer

from accounts import utils, groups
from accounts.models import (
    User,
    UserType,
    Role,
    Profile,
    CustomerEscalation,
    CircuitInfo,
    CircuitType,
    StoredEmailData,
    FeatureStatus,
    Customer,
)
from accounts.services.two_factor_authentication_service import (
    TwilioAuthenticationService,
)
from accounts.token_manager import AccessToken, RefreshToken


def update_previous_login_of_user(user):
    User.objects.filter(id=user.id).update(previous_login=user.last_login)


class JWTSerializer(JSONWebTokenSerializer):
    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.EmailField()
        self.fields["password"] = PasswordField(write_only=True)

    @property
    def username_field(self):
        return "email"

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            "password": attrs.get("password"),
        }

        if all(credentials.values()):
            user = authenticate(request=self.context["request"], **credentials)

            if user:
                if not user.is_active:
                    msg = _("User account is disabled.")
                    raise serializers.ValidationError(msg)

                update_previous_login_of_user(user)
                update_last_login(None, user)
                access_token = AccessToken.for_user(user)
                refresh_token = RefreshToken.for_user(user)
                return {
                    "token": access_token,
                    "refresh": refresh_token,
                    "user": user,
                }
            else:
                raise AuthenticationFailed()
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class UserProfileSerializer(serializers.ModelSerializer):
    def validate_system_member_crm_id(self, system_member_crm_id):
        if system_member_crm_id:
            request = self.context.get("request")
            if (
                request.method == "POST"
                and Profile.objects.filter(
                    system_member_crm_id=system_member_crm_id,
                    user__provider__id=request.provider.id,
                ).exists()
            ):
                raise ValidationError("This system member is already mapped!")
            elif (
                request.method == "PUT"
                and Profile.objects.filter(
                    system_member_crm_id=system_member_crm_id,
                    user__provider__id=request.provider.id,
                )
                .exclude(id=request.user.profile.id)
                .exists()
            ):
                raise ValidationError("This system member is already mapped!")
        return system_member_crm_id

    class Meta:
        model = Profile
        fields = (
            "id",
            "department",
            "title",
            "office_phone",
            "cell_phone_number",
            "country_code",
            "office_phone_country_code",
            "twitter_profile_url",
            "linkedin_profile_url",
            "profile_pic",
            "cco_id",
            "cco_id_acknowledged",
            "customer_user_instance_id",
            "system_member_crm_id",
            "email_signature",
            "default_landing_page",
            "feature",
        )
        read_only_fields = ("profile_pic", "id")


class UserProfileUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            "department",
            "title",
            "office_phone",
            "cell_phone_number",
            "country_code",
            "office_phone_country_code",
            "twitter_profile_url",
            "linkedin_profile_url",
            "profile_pic",
            "cco_id",
            "cco_id_acknowledged",
            "customer_user_instance_id",
            "system_member_crm_id",
            "email_signature",
            "default_landing_page",
            "feature",
        )
        read_only_fields = ("profile_pic",)


class BaseUserListCreateSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(required=False)
    can_edit = serializers.SerializerMethodField()
    can_delete = serializers.SerializerMethodField()

    def get_can_edit(self, obj):
        request = self.context.get("request")
        user = request.user
        if user == obj:
            return False
        user_edit_permission = "accounts.change_user"
        return user.has_perm(user_edit_permission, obj)

    def get_can_delete(self, obj):
        request = self.context.get("request")
        user = request.user
        if user == obj:
            return False
        user_delete_permission = "accounts.delete_user"
        return user.has_perm(user_delete_permission, obj)

    def validate_user_role(self, value):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        valid_ids = utils.get_valid_user_role_ids(user.type, user.user_role_id)
        if value.id not in valid_ids:
            raise ValidationError("Invalid user_role id.")
        return value

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "phone_number",
            "type",
            "role",
            "role_display_name",
            "user_type",
            "user_role",
            "provider",
            "customer",
            "is_active",
            "is_password_set",
            "activation_mails_sent_count",
            "profile",
            "created_on",
            "updated_on",
            "last_login",
            "can_edit",
            "can_delete",
            "crm_id",
            "project_rate_role",
        )

        read_only_fields = (
            "is_active",
            "customer",
            "provider",
            "user_type",
            "last_login",
            "can_edit",
            "crm_id",
        )


class AcelaUserListCreateSerializer(BaseUserListCreateSerializer):
    def validate_email(self, value):
        unique = utils.check_unique_email_for_acela_portal(value)
        if not unique:
            raise ValidationError(
                "User with this email address already exists"
            )
        return value

    class Meta(BaseUserListCreateSerializer.Meta):
        model = User
        fields = BaseUserListCreateSerializer.Meta.fields
        read_only_fields = BaseUserListCreateSerializer.Meta.read_only_fields


class ProviderUserListCreateSerializer(BaseUserListCreateSerializer):
    def validate_email(self, value):
        request = self.context["request"]
        provider = request.provider or self.context.get("provider_id")
        unique = utils.check_unique_email_for_provider_portal(value, provider)
        if not unique:
            raise ValidationError(
                "User with this email address already exists"
            )
        return value

    class Meta(BaseUserListCreateSerializer.Meta):
        model = User
        fields = BaseUserListCreateSerializer.Meta.fields
        read_only_fields = BaseUserListCreateSerializer.Meta.read_only_fields


class CustomerUserListCreateSerializer(BaseUserListCreateSerializer):
    def validate_email(self, value):
        request = self.context["request"]
        unique = utils.check_unique_email_for_provider_portal(
            value, request.provider
        )
        if not unique:
            raise ValidationError(
                "User with this email address already exists"
            )
        return value

    class Meta(BaseUserListCreateSerializer.Meta):
        model = User
        fields = BaseUserListCreateSerializer.Meta.fields
        read_only_fields = BaseUserListCreateSerializer.Meta.read_only_fields


class UserDropDownSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("name", "id")
        read_only_fields = ("name", "id")


class CustomerUserActivationSerializer(serializers.Serializer):
    user_role = serializers.IntegerField()

    def validate_user_role(self, value):
        valid_ids = Role.objects.filter(
            user_type__name=UserType.CUSTOMER
        ).values_list("id", flat=True)
        if value not in valid_ids:
            raise ValidationError("Invalid user_role id.")
        return value


class UserBaseUpdateSerializer(object):
    """
    Base class with update method for updating user data and profile data
    """

    def validate_user_role(self, value):
        if not value:
            return
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        valid_ids = utils.get_valid_user_role_ids(user.type, user.user_role_id)
        if value.id not in valid_ids:
            raise ValidationError("You are not allowed to set this role")
        return value

    def validate_profile(self, profile):
        if (
            profile.get("system_member_crm_id")
            and profile.get("system_member_crm_id")
            != self.instance.profile.system_member_crm_id
        ):
            if Profile.objects.filter(
                system_member_crm_id=profile.get("system_member_crm_id"),
                user__provider_id=self.context.get("request").provider.id,
            ).exists():
                raise ValidationError("This system member is already mapped")
        return profile

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile", None)
        if profile_data:
            profile = instance.profile
            # On update of phone number, setting the external auth ID as empty and deleting the user's auth account.
            if (
                profile.cell_phone_number
                != profile_data.get("cell_phone_number")
                and instance.external_auth_id
            ):
                if not User.objects.exclude(id=instance.id).filter(
                    external_auth_id=instance.external_auth_id
                ):
                    TwilioAuthenticationService.delete_auth_account(
                        instance.external_auth_id
                    )
                validated_data.update({"external_auth_id": None})
            Profile.objects.filter(id=instance.profile.id).update(
                **profile_data
            )

        # Check for role update
        call_role_update = False
        if instance.user_role != validated_data.get(
            "user_role", instance.user_role
        ):
            call_role_update = True
        User.objects.filter(id=instance.id).update(**validated_data)

        # Refresh Instance to get the latest version
        instance.refresh_from_db()
        # Call for role update
        if call_role_update:
            groups.change_user_role(instance)
        return instance


class UserRetrieveBaseUpdateSerializer(
    UserBaseUpdateSerializer, serializers.ModelSerializer
):
    profile = UserProfileUpdateSerializer(required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "phone_number",
            "type",
            "role",
            "role_display_name",
            "user_type",
            "user_role",
            "provider",
            "customer",
            "is_active",
            "is_password_set",
            "profile",
            "created_on",
            "updated_on",
            "last_login",
            "project_rate_role",
        )

        read_only_fields = (
            "email",
            "user_type",
            "provider",
            "customer",
            "last_login",
        )


class ProviderCustomerUserProfileUpdateSerializer(serializers.ModelSerializer):
    cco_id = serializers.CharField(allow_null=True, allow_blank=True)
    cco_id_acknowledged = serializers.BooleanField(default=False)

    class Meta:
        model = Profile
        fields = (
            "office_phone",
            "cell_phone_number",
            "country_code",
            "office_phone_country_code",
            "cco_id",
            "cco_id_acknowledged",
        )


class ProviderCustomerUserUpdateSerializer(
    UserBaseUpdateSerializer, serializers.ModelSerializer
):
    profile = ProviderCustomerUserProfileUpdateSerializer(required=False)

    class Meta:
        model = User
        fields = ("is_active", "user_role", "profile")


class ProfileBaseUpdateSerializer(
    UserBaseUpdateSerializer, serializers.ModelSerializer
):
    profile = UserProfileSerializer(required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "phone_number",
            "type",
            "role",
            "role_display_name",
            "user_type",
            "is_password_set",
            "user_role",
            "provider",
            "customer",
            "is_active",
            "profile",
            "created_on",
            "updated_on",
            "last_login",
            "previous_login",
        )

        read_only_fields = (
            "id",
            "email",
            "user_role",
            "user_type",
            "is_active",
            "provider",
            "customer",
            "user_type",
            "last_login",
        )


class ProfilePicSerializer(serializers.Serializer):
    profile_pic = serializers.ImageField(
        validators=[FileExtensionValidator(["png", "jpeg", "jpg"])]
    )


class UidAndTokenSerializer(serializers.Serializer):
    uid = serializers.CharField()
    token = serializers.CharField()

    default_error_messages = {
        "invalid_token": "Token is expired",
        "invalid_uid": "User not found",
    }

    def validate_uid(self, value):
        try:
            uid = utils.decode_uid(value)
            self.user = User.objects.get(pk=uid)
        except (User.DoesNotExist, ValueError, TypeError, OverflowError):
            self.fail("invalid_uid")

        return value

    def validate(self, attrs):
        attrs = super(UidAndTokenSerializer, self).validate(attrs)
        is_token_valid = self.context["view"].token_generator.check_token(
            self.user, attrs["token"]
        )
        if is_token_valid:
            return attrs
        else:
            self.fail("invalid_token")


class ActivationSerializer(UidAndTokenSerializer):
    default_error_messages = {"stale_token": "Token expired"}

    def validate(self, attrs):
        attrs = super(ActivationSerializer, self).validate(attrs)
        if not self.user.is_active:
            return attrs
        raise exceptions.PermissionDenied(self.error_messages["stale_token"])


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()

    default_error_messages = {
        "email_not_found": "No User found with this email."
    }

    def validate_email(self, value):
        users = self.context["view"].get_user(value)
        if not users:
            self.fail("email_not_found")
        else:
            return value


class PasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(style={"input_type": "password"})

    def validate_new_password(self, attrs):
        user = self.context["request"].user or self.user
        assert user is not None

        try:
            validate_password(attrs, user)
        except ValidationError as e:
            raise serializers.ValidationError(
                {"new_password": list(e.messages)}
            )
        return attrs


class CurrentPasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(style={"input_type": "password"})

    default_error_messages = {
        "invalid_password": "Please enter a valid password."
    }

    def validate_current_password(self, value):
        is_password_valid = self.context["request"].user.check_password(value)
        if is_password_valid:
            return value
        else:
            self.fail("invalid_password")


class SetPasswordSerializer(PasswordSerializer, CurrentPasswordSerializer):
    pass


class PasswordResetConfirmSerializer(
    UidAndTokenSerializer, PasswordSerializer
):
    pass


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = "id", "role_name", "display_name"


class UserTypeSerializer(serializers.ModelSerializer):
    user_type = serializers.SerializerMethodField(source="get_user_type")
    roles = serializers.SerializerMethodField()

    def get_user_type(self, obj):
        return obj.name

    def get_roles(self, obj):
        request = self.context.get("request")
        user = request.user
        roles = Role.objects.filter(id__gte=user.user_role_id, user_type=obj)
        return RoleSerializer(roles, many=True).data

    class Meta:
        model = UserType
        fields = "id", "user_type", "roles"
        read_only_fields = "user_type", "roles"


class UniqueEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class CustomerEscalationRetrieveSerializer(serializers.ModelSerializer):
    user = UserRetrieveBaseUpdateSerializer()

    class Meta:
        model = CustomerEscalation
        fields = "__all__"


class CustomerEscalationCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerEscalation
        fields = "__all__"


class CircuitInfoCreateUpdateSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        loa = attrs.get("loa")
        if not loa:
            attrs["attachment"] = None

        # validate unique together constraint
        customer_id = self.context.get("customer_id")
        circuit_id = attrs.get("circuit_id")
        circuit_type = attrs.get("circuit_type")
        try:
            obj = CircuitInfo.objects.get(
                customer_id=customer_id,
                circuit_type=circuit_type,
                circuit_id=circuit_id,
            )
        except CircuitInfo.DoesNotExist:
            return attrs
        if self.instance and obj.id == self.instance.id:
            return attrs
        else:
            raise ValidationError(
                {
                    "circuit_id": "A circuit with the same Circuit ID and Circuit Type already exists."
                }
            )

    class Meta:
        model = CircuitInfo
        exclude = ["customer"]


class CircuitInfoListRetrieveSerializer(CircuitInfoCreateUpdateSerializer):
    circuit_type = serializers.CharField(
        source="circuit_type.name", read_only=True
    )
    circuit_type_id = serializers.CharField(
        source="circuit_type.id", read_only=True
    )


class CircuitInfoSerializer(serializers.ModelSerializer):
    circuit_type = serializers.CharField(
        source="circuit_type.name", read_only=True
    )
    circuit_type_id = serializers.CharField(
        source="circuit_type.id", read_only=True
    )

    class Meta:
        model = CircuitInfo
        fields = "__all__"


class CircuitInfoTypeListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CircuitType
        exclude = ("provider",)
        read_only_fields = ("updated_on", "created_on")


class CircuitInfoReportDownloadSerializer(serializers.Serializer):
    type = serializers.CharField()


class CircuitInfoTypeWithAssociationSerializer(serializers.ModelSerializer):
    circuit_infos = CircuitInfoSerializer(many=True)

    class Meta:
        model = CircuitType
        fields = ["name", "updated_on", "created_on", "circuit_infos", "id"]
        read_only_fields = ("updated_on", "created_on")


class TwoFactorAuthenticationRequestSerializer(serializers.Serializer):
    AUTH_TYPES = Choices(
        ("SMS", "SMS"), ("CALL", "CALL"), ("AUTHY_NOTIFY", "AUTHY_NOTIFY")
    )
    type = serializers.ChoiceField(choices=AUTH_TYPES)


class TwoFactorAuthenticationValidateSerializer(serializers.Serializer):
    code = serializers.CharField()
    AUTH_TYPES = Choices(
        ("SMS", "SMS"),
        ("CALL", "CALL"),
        ("AUTHY_NOTIFY", "AUTHY_NOTIFY"),
        ("AUTHY_APP", "AUTHY_APP"),
    )
    type = serializers.ChoiceField(choices=AUTH_TYPES)


class RequestPhoneVerificationSerializer(serializers.Serializer):
    phone = serializers.CharField(min_length=10, max_length=20)
    country_code = serializers.IntegerField()


class ValidatePhoneVerificationSerializer(RequestPhoneVerificationSerializer):
    code = serializers.CharField()


class LoggedEmailDataSerializer(serializers.ModelSerializer):
    email_data = serializers.JSONField()

    class Meta:
        model = StoredEmailData
        exclude = ["provider"]


class FeatureStatusSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(
        many=True, queryset=User.objects.all()
    )
    feature = serializers.CharField()

    class Meta:
        model = FeatureStatus
        fields = ["id", "users", "feature", "enabled"]


class FeatureStatusGetSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="user.name", read_only=True)
    email = serializers.EmailField(source="user.email", read_only=True)

    class Meta:
        model = FeatureStatus
        fields = ["id", "user", "feature", "enabled", "name", "email"]


class MergingCustomerSerializer(serializers.Serializer):
    source_customer_id = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all()
    )
    target_customer_id = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all()
    )

    def validate_source_customer_id(self, source_customer_id):
        request = self.context["request"]
        if source_customer_id.id == request.data.get("target_customer_id"):
            raise ValidationError(
                "source and target customer id can not be same"
            )
        else:
            return source_customer_id


class AuthyOTPSerializer(serializers.Serializer):
    otp = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
