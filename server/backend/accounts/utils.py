import uuid
import warnings
from datetime import datetime
from urllib.parse import urlparse

import phonenumbers
from django.conf import settings
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework_jwt.compat import get_username_field, get_username
from rest_framework_jwt.settings import api_settings

from accounts.models import UserType, Role, User


def encode_uid(pk):
    return urlsafe_base64_encode(force_bytes(pk))


def decode_uid(pk):
    return force_text(urlsafe_base64_decode(pk))


def get_roles_permissions_dict(
    owner_permissions, admin_permissions, staff_permissions
):
    """
    Returns dict with roles as keys and list of permissions as values
    :param owner_permissions: Owner role permissions list
    :param admin_permissions: Admin role permissions list
    :param staff_permissions: Staff role permissions list
    :return:
    """

    roles = [Role.OWNER, Role.ADMIN, Role.STAFF]
    permissions = [owner_permissions, admin_permissions, staff_permissions]

    roles_and_permissions = dict(zip(roles, permissions))
    return roles_and_permissions


def get_security_url(user, token, url, domain):
    """
    Generate secure URL for password reset and account activation
    :param user: User model object
    :param token: token
    :param url: url format string
    :return:
    """
    uid = encode_uid(user.pk)
    url = url.format(uid=uid, token=token)
    url = f"{domain}/{url}"
    return url


def get_user_base_url(user, http_origin):
    """
    Generates the Base URL for the User
    :param user: User model object
    :param http_origin: Http origin
    """
    port = get_port_number_from_url(http_origin)
    url = build_url(user.get_domain(), port)
    return url


def get_login_url(user, http_origin):
    """
    Generate login URL for the User.
    :param user: User model object
    :param http_origin: Http origin
    """
    url = f"{get_user_base_url(user, http_origin)}/{settings.UI_LOGIN_URL}"
    return url


class ActionViewMixin(object):
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return self._action(serializer)


def jwt_get_username_from_payload_handler(payload):
    """
    Override this function if username is formatted differently in payload
    """
    return payload.get("id")


def custom_jwt_payload_handler(user):
    username_field = get_username_field()
    username = get_username(user)

    warnings.warn(
        "The following fields will be removed in the future: "
        "`email` and `user_id`. ",
        DeprecationWarning,
    )

    current_time = datetime.utcnow()
    payload = {
        "token_type": "access",
        "user_id": str(user.pk),
        "email": username,
        "exp": current_time + api_settings.JWT_EXPIRATION_DELTA,
        "scopes": {"type": user.type.upper(), "role": user.role.upper()},
    }
    if hasattr(user, "email"):
        payload["email"] = user.email
    if isinstance(user.pk, uuid.UUID):
        payload["user_id"] = str(user.pk)
    if isinstance(username, uuid.UUID):
        username = username.hex
    if api_settings.JWT_AUDIENCE is not None:
        payload["aud"] = api_settings.JWT_AUDIENCE
    if api_settings.JWT_ISSUER is not None:
        payload["iss"] = api_settings.JWT_ISSUER
    if hasattr(user, "profile"):
        payload["default_landing_page"] = user.profile.default_landing_page
    payload[username_field] = username
    return payload


def get_valid_user_role_ids(user_type, user_role_id):
    """
    Return list of accessible user role ids for user_type
    :param user_type: UserType
    :return: list of valid ids
    """
    user_types_list = []
    if user_type == UserType.ACCELAVAR:
        user_types_list = [UserType.ACCELAVAR, UserType.PROVIDER]

    elif user_type == UserType.PROVIDER:
        user_types_list = [UserType.PROVIDER, UserType.CUSTOMER]

    elif user_type == UserType.CUSTOMER:
        user_types_list = [UserType.CUSTOMER]

    valid_ids = Role.objects.filter(
        user_type__name__in=user_types_list, id__gte=user_role_id
    )
    return valid_ids.values_list("id", flat=True)


def check_unique_email_for_acela_portal(email):
    """
    Check if entered email is unique for acela portal
    :param email:
    :return:
    """
    if User.superusers.filter(email=email).exists():
        return False
    return True


def check_unique_email_for_provider_portal(email, provider):
    """
    Check if entered email is unique across Provider and its Customer users
    :param email:
    :return:
    """
    if User.providers_and_customers.filter(
        provider=provider, email=email
    ).exists():
        return False
    return True


def get_port_number_from_url(url):
    """
    Returns port number from url
    """
    parsed_uri = urlparse(url)
    return parsed_uri.port or None


def build_url(domain, port=None):
    """
    Returns prepared URL
    :return
    """
    http_protocol = settings.HTTP_PROTOCOL
    if port:
        url = f"{http_protocol}://{domain}:{port}"
    else:
        url = f"{http_protocol}://{domain}"
    return url


def get_host(client_url, host_url):
    """
    Returns request host
    :param client_url: HTTP_CLIENT_URL request header
    :param host_url: HTTP_HOST request header
    :return: request host
    """
    host = None
    if client_url:
        parsed_uri = urlparse(client_url)
        host = parsed_uri.hostname
    else:
        try:
            host = host_url.split(":")[0]
        except AttributeError:
            pass
    return host


def get_formatted_phone_number(phone_number, region=settings.DEFAULT_REGION):
    """Format phone number"""
    try:
        parsed_phone_number = phonenumbers.parse(phone_number, region)
        formatted_phone_number = phonenumbers.format_number(
            parsed_phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL
        )
    except Exception:
        return phone_number
    return formatted_phone_number
