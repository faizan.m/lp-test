from box import Box
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.circuit_import_serializers import CircuitImportSerializer
from accounts.models import UserType
from accounts.services.circuit_info_service import CircuitInfoImportService


class CircuitImportAPIView(APIView):
    """
    API View to import Subscription from xlsx file.
    """

    def post(self, request, **kwargs):
        serializer = CircuitImportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_path = serializer.validated_data.get("file_path")
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        field_mapping = Box(serializer.data["field_mappings"])
        site_mapping = serializer.data["site_mapping"]
        circuit_type_mapping = serializer.data["circuit_type_mapping"]
        data = CircuitInfoImportService.import_from_file(
            request.provider,
            customer_id,
            file_path,
            field_mapping,
            site_mapping,
            circuit_type_mapping,
        )
        return Response(data=data, status=status.HTTP_200_OK)
