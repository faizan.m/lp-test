from calendar import timegm
from datetime import datetime, timedelta

import jwt
from django.conf import settings
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_jwt.authentication import jwt_decode_handler
from rest_framework_jwt.serializers import (
    jwt_encode_handler,
    jwt_payload_handler,
)
from rest_framework_jwt.settings import api_settings
from typing import Optional, Dict


class AccessToken:
    token_type = "access"
    lifetime = api_settings.JWT_EXPIRATION_DELTA

    def __init__(self):
        self.payload = {"token_type": self.token_type}

    @classmethod
    def verify(self, token):
        try:
            payload = jwt_decode_handler(token)
            if payload.get("token_type") != AccessToken.token_type:
                raise AuthenticationFailed()
        except jwt.ExpiredSignature:
            msg = "Signature has expired."
            raise AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = "Error decoding signature."
            raise AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise AuthenticationFailed()
        return payload

    @classmethod
    def for_user(cls, user):
        token = cls()
        token.payload.update(jwt_payload_handler(user))
        return jwt_encode_handler(token.payload)

    def set_exp(self, lifetime=None):
        if lifetime is None:
            lifetime = AccessToken.lifetime
        current_time = datetime.utcnow()
        self.payload["exp"] = current_time + lifetime


class RefreshToken(AccessToken):
    token_type = "refresh"
    lifetime = api_settings.JWT_REFRESH_EXPIRATION_DELTA

    def __init__(self):
        super().__init__()

    @classmethod
    def for_user(cls, user):
        current_time = datetime.utcnow()
        token = cls()
        token.payload.update(jwt_payload_handler(user))
        token.set_exp(cls.lifetime)
        token.payload.update(
            {
                "orig_iat": timegm(current_time.utctimetuple()),
                "token_type": RefreshToken.token_type,
                "inactivity": settings.USER_INACTIVITY_DELTA,
            }
        )
        return jwt_encode_handler(token.payload)


class UntypedToken(AccessToken):
    token_type = "untyped"
    lifetime = timedelta(seconds=15 * 60)

    def __init__(self):
        super().__init__()

    @classmethod
    def verify(self, token):
        try:
            payload = jwt_decode_handler(token)
            if payload.get("token_type") != UntypedToken.token_type:
                raise AuthenticationFailed()
        except jwt.ExpiredSignature:
            msg = "Signature has expired."
            raise AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = "Error decoding signature."
            raise AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise AuthenticationFailed()
        return payload

    @classmethod
    def for_user(cls, user, expiry: Optional[int] = None):
        token = cls()
        token.payload.update(jwt_payload_handler(user))
        token.payload.update({"token_type": UntypedToken.token_type})
        expiry_time = cls.lifetime if not expiry else timedelta(seconds=expiry)
        token.set_exp(expiry_time)
        return jwt_encode_handler(token.payload)

    @classmethod
    def is_valid_token(cls, token: str, user) -> bool:
        try:
            payload: Dict = jwt_decode_handler(token)
            return payload.get("user_id") == str(user.id)
        except (jwt.ExpiredSignature, jwt.DecodeError, jwt.InvalidTokenError):
            return False
