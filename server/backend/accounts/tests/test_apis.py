import pytest
from django.contrib.auth.tokens import default_token_generator
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from accounts import utils
from accounts.authentication_backend import User
from accounts.serializers import (
    BaseUserListCreateSerializer,
    UserRetrieveBaseUpdateSerializer,
)

pytestmark = pytest.mark.django_db


class TestLogin:
    url = reverse("api_v1_endpoints:accounts:login")

    @staticmethod
    def get_token(
        client, email, password="password123", server_name="testserver"
    ):
        # Authenticate User
        credentials = {"email": email, "password": password}
        response = client.post(
            reverse("api_v1_endpoints:accounts:login"),
            credentials,
            format="json",
            SERVER_NAME=server_name,
        )
        _token = response.data["token"]
        return _token

    def test_login_with_correct_credentials_for_superuser(
        self, client, superuser_owner_user
    ):
        data = {
            "email": f"{superuser_owner_user.email}",
            "password": "password123",
        }
        response = client.post(self.url, data, format="json")
        assert response.status_code == status.HTTP_200_OK

    def test_successful_login_should_return_token(
        self, client, superuser_owner_user
    ):
        data = {
            "email": f"{superuser_owner_user.email}",
            "password": "password123",
        }
        response = client.post(self.url, data, format="json")
        assert response.status_code == status.HTTP_200_OK
        assert set(response.data.keys()) == {"token"}

    def test_login_with_incorrect_email_for_superuser(
        self, client, superuser_owner_user
    ):
        data = {"email": "random@example.com", "password": "password123"}
        response = client.post(self.url, data, format="json")
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_login_with_incorrect_password_for_superuser(
        self, client, superuser_owner_user
    ):
        data = {
            "email": f"{superuser_owner_user.email}",
            "password": "password12",
        }
        response = client.post(self.url, data, format="json")
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_login_with_correct_credentials_for_provider_user(
        self, client, provider_owner_user
    ):
        data = {
            "email": f"{provider_owner_user.email}",
            "password": "password123",
        }
        response = client.post(
            self.url,
            data,
            format="json",
            SERVER_NAME=provider_owner_user.provider.url,
        )
        assert response.status_code == status.HTTP_200_OK

    def test_login_with_provider_credentials_in_superuser_portal(
        self, client, superuser_owner_user, provider_owner_user
    ):
        data = {
            "email": f"{provider_owner_user.email}",
            "password": "password123",
        }
        response = client.post(self.url, data, format="json")
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_login_with_superuser_credentials_in_provider_portal(
        self, client, superuser_owner_user, provider_owner_user
    ):
        data = {
            "email": f"{superuser_owner_user.email}",
            "password": "password123",
        }
        response = client.post(
            self.url,
            data,
            format="json",
            SERVER_NAME=provider_owner_user.provider.url,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestAccountsAPI:
    """Test following endpoints:
    /forgot-password/
    /set-password/
    /change-password/
    /profile/
    """

    def test_forgot_password_for_existing_email(
        self, client, superuser_staff_user, mailoutbox
    ):
        url = reverse("api_v1_endpoints:accounts:forgot-password")
        data = {"email": superuser_staff_user.email}
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert len(mailoutbox) == 1
        mail_ = mailoutbox[0]
        assert list(mail_.to) == [superuser_staff_user.email]

    def test_forgot_password_for_non_existing_email(self, client):
        url = reverse("api_v1_endpoints:accounts:forgot-password")
        data = {"email": "random@example.com"}
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_set_password_for_new_user(self, client, superuser_admin_user):
        superuser_admin_user.is_active = False
        superuser_admin_user.password = ""
        superuser_admin_user.save()
        url = reverse("api_v1_endpoints:accounts:set_password")
        data = {
            "uid": utils.encode_uid(superuser_admin_user.pk),
            "token": default_token_generator.make_token(superuser_admin_user),
            "new_password": "newpassword1",
        }
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_204_NO_CONTENT
        superuser_admin_user.refresh_from_db()
        assert (
            superuser_admin_user.check_password(data["new_password"]) is True
        )
        assert superuser_admin_user.is_active is True

    def test_set_password_for_existing_user(
        self, client, superuser_admin_user
    ):
        superuser_admin_user.is_active = False
        superuser_admin_user.save()
        url = reverse("api_v1_endpoints:accounts:set_password")
        data = {
            "uid": utils.encode_uid(superuser_admin_user.pk),
            "token": default_token_generator.make_token(superuser_admin_user),
            "new_password": "newpassword1",
        }
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_204_NO_CONTENT
        superuser_admin_user.refresh_from_db()
        assert (
            superuser_admin_user.check_password(data["new_password"]) is True
        )
        assert superuser_admin_user.is_active is False

    def test_post_not_set_new_password_if_broken_uid(
        self, client, superuser_admin_user
    ):
        url = reverse("api_v1_endpoints:accounts:set_password")
        data = {
            "uid": "x",
            "token": default_token_generator.make_token(superuser_admin_user),
            "new_password": "newpassword1",
        }
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_post_not_set_new_password_if_wrong_token(
        self, client, superuser_admin_user
    ):
        url = reverse("api_v1_endpoints:accounts:set_password")
        data = {
            "uid": utils.encode_uid(superuser_admin_user.pk),
            "token": "wrong-token",
            "new_password": "newpassword1",
        }
        response = client.post(url, data, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_post_change_password(self, client, superuser_admin_user):
        url = reverse("api_v1_endpoints:accounts:change-password")
        data = {
            "new_password": "newpassword1",
            "current_password": "password123",
        }
        token = TestLogin.get_token(client, superuser_admin_user.email)
        response = client.post(
            url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        superuser_admin_user.refresh_from_db()
        assert (
            superuser_admin_user.check_password(data["new_password"]) is True
        )


class TestSuperuserUserListCreateAPI:
    """Test /superusers/users/ endpoint"""

    url = reverse("api_v1_endpoints:superusers:list-create-user")

    def test_get_should_return_list_of_superuser_users_for_authenticated_user(
        self,
        client,
        superuser_owner_user,
        superuser_admin_user,
        superuser_staff_user,
        provider_admin_user,
    ):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        users = User.superusers.all()
        response = client.get(
            self.url, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_200_OK
        serializer = BaseUserListCreateSerializer(users, many=True)
        assert response.data["results"] == serializer.data

    def test_get_request_is_forbidden_without_auth(
        self,
        client,
        superuser_owner_user,
        superuser_admin_user,
        superuser_staff_user,
    ):
        response = client.get(self.url, format="json")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_get_request_returned_object_has_count(
        self,
        client,
        superuser_owner_user,
        superuser_admin_user,
        superuser_staff_user,
        provider_admin_user,
    ):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        response = client.get(
            self.url, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.data["count"] == 3

    def test_provider_user_should_not_be_able_to_see_superuser_users(
        self,
        client,
        superuser_owner_user,
        superuser_admin_user,
        superuser_staff_user,
        provider_owner_user,
    ):
        server_name = provider_owner_user.provider.url
        token = TestLogin.get_token(
            client, provider_owner_user.email, server_name=server_name
        )
        response = client.get(
            self.url, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.data["count"] == 0

    def test_post_for_superuser_owner(
        self, client, superuser_owner_user, mailoutbox
    ):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        qs = User.objects.filter(email=data["email"])
        assert response.status_code == status.HTTP_201_CREATED
        assert qs.count() == 1
        assert qs.first().is_active is False
        assert len(mailoutbox) == 1
        mail_ = mailoutbox[0]
        assert list(mail_.to) == ["newuser@example.com"]

    def test_post_without_valid_email(self, client, superuser_owner_user):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        data = {
            "email": "newuser",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert set(response.data.keys()) == {"email"}

    def test_post_with_already_existing_email(
        self, client, superuser_owner_user
    ):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        data = {
            "email": superuser_owner_user.email,
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert set(response.data.keys()) == {"email"}

    def test_post_with_invalid_user_role(self, client, superuser_owner_user):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "8",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert set(response.data.keys()) == {"user_role"}

    def test_post_for_superuser_admin(self, client, superuser_admin_user):
        token = TestLogin.get_token(client, superuser_admin_user.email)
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_post_for_superuser_staff(self, client, superuser_staff_user):
        token = TestLogin.get_token(client, superuser_staff_user.email)
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_post_for_provider_user(self, client, provider_owner_user):
        token = TestLogin.get_token(
            client,
            provider_owner_user.email,
            server_name=provider_owner_user.provider.url,
        )
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_post_for_customer_user(self, client, customer_owner_user):
        token = TestLogin.get_token(
            client,
            customer_owner_user.email,
            server_name=customer_owner_user.provider.url,
        )
        data = {
            "email": "newuser@example.com",
            "password": "password123",
            "user_role": "2",
        }
        response = client.post(
            self.url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestSuperuserUserRetrieveUpdateAPI:
    """Test /superusers/users/<:user_id>/ endpoint"""

    def test_get_for_superuser_owner(
        self, client, superuser_owner_user, superuser_admin_user
    ):
        token = TestLogin.get_token(client, superuser_owner_user.email)
        url = reverse(
            "api_v1_endpoints:superusers:retrieve-update-user",
            kwargs={"pk": superuser_admin_user.id},
        )
        response = client.get(
            url, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        serializer = UserRetrieveBaseUpdateSerializer(superuser_admin_user)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == serializer.data

    def test_put_for_superuser_owner(
        self, superuser_owner_user, superuser_admin_user
    ):
        client = APIClient()
        token = TestLogin.get_token(client, superuser_owner_user.email)
        url = reverse(
            "api_v1_endpoints:superusers:retrieve-update-user",
            kwargs={"pk": superuser_admin_user.id},
        )
        data = {
            "phone_number": "8956235689",
            "profile": {"department": "Sales"},
        }
        response = client.put(
            url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.data["phone_number"] == data["phone_number"]
        assert (
            response.data["profile"]["department"]
            == data["profile"]["department"]
        )

    def test_put_for_superuser_admin(
        self, superuser_owner_user, superuser_admin_user
    ):
        client = APIClient()
        token = TestLogin.get_token(client, superuser_admin_user.email)
        url = reverse(
            "api_v1_endpoints:superusers:retrieve-update-user",
            kwargs={"pk": superuser_owner_user.id},
        )
        data = {
            "phone_number": "8956235689",
            "profile": {"department": "Sales"},
        }
        response = client.put(
            url, data, format="json", HTTP_AUTHORIZATION=f"Bearer {token}"
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
