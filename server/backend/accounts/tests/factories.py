import datetime
import uuid

import factory
from factory import Sequence, Faker
import random
from accounts.models import (
    User,
    Provider,
    Customer,
    Profile,
    Address,
    AccountingContact,
    UserType,
    Role,
)
from faker import Faker as libFaker

fake = libFaker()


class AddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Address


class AccountingContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountingContact


class ProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Provider
        django_get_or_create = ("url",)

    name = Faker("company")
    # url = factory.LazyAttribute(
    #     lambda obj: f'{text(obj.name.split()[0].lower())}.lookingpoint.com')
    url = Faker("domain_name")
    is_active = True
    email = Faker("email")
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    integration_statuses = dict(
        crm_authentication_configured=True,
        crm_board_mapping_configured=True,
        crm_device_categories_configured=True,
        device_manufacturer_api_configured=True,
    )


class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Customer

    name = Faker("company")
    provider = factory.SubFactory(ProviderFactory)
    is_active = True
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    crm_id = factory.Sequence(lambda n: n)


class UserTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserType

    name = factory.LazyAttribute(
        lambda _: random.choice(
            [UserType.ACCELAVAR, UserType.PROVIDER, UserType.CUSTOMER]
        )
    )


class ProviderUserTypeFactory(UserTypeFactory):
    name = UserType.PROVIDER


class CustomerUserTypeFactory(UserTypeFactory):
    name = UserType.CUSTOMER


class UserRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Role

    role_name = factory.LazyAttribute(
        lambda _: random.choice([Role.OWNER, Role.ADMIN, Role.STAFF])
    )
    user_type = factory.SubFactory(UserTypeFactory)


class OwnerUserRoleFactory(UserRoleFactory):
    role_name = Role.OWNER
    user_type = factory.SubFactory(ProviderUserTypeFactory)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    id = Sequence(lambda n: uuid.uuid4())
    password = Faker(
        "password",
        length=10,
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True,
    )
    email = Faker("email")
    first_name = Faker("first_name")
    last_name = Faker("last_name")
    is_active = True
    provider = factory.SubFactory(ProviderFactory)
    customer = factory.SubFactory(CustomerFactory)
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    user_type = factory.SubFactory(UserTypeFactory)
    user_role = factory.SubFactory(UserRoleFactory)


class ProviderUserFactory(UserFactory):
    customer = None
    user_type = factory.SubFactory(ProviderUserTypeFactory)


class ProfileFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    department = fake.bs()
    title = fake.bs()
    office_phone = fake.phone_number()
    office_phone_crm_id = factory.Sequence(lambda n: n)
    cell_phone_number = fake.phone_number()
    cell_phone_number_crm_id = factory.Sequence(lambda n: n)
    country_code = fake.pyint()
    office_phone_country_code = fake.pyint()
    twitter_profile_url = fake.url()
    linkedin_profile_url = fake.url()
    profile_pic = None
    customer_user_instance_id = factory.Sequence(lambda n: n)
    system_member_crm_id = factory.Sequence(lambda n: n)
    cco_id = fake.bban()
    email_signature = dict()
    cco_id_acknowledged = False
    default_landing_page = fake.url()

    class Meta:
        model = Profile


# Remove any special chars from string
text = lambda x: "".join(e for e in x if e.isalnum())
