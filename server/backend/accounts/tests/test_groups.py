import pytest
from django.contrib.auth.models import Group
from guardian.shortcuts import get_group_perms

from accounts import groups
from accounts.models import UserType, Role

pytestmark = pytest.mark.django_db


def test_create_user_group():
    prefix = "provider"
    groups._create_user_groups(prefix, UserType.PROVIDER)
    assert Group.objects.count() == 6
    expected_groups = {
        "accelavar staff",
        "accelavar admin",
        "accelavar owner",
        "provider owner",
        "provider admin",
        "provider staff",
    }
    actual_groups = set(Group.objects.values_list("name", flat=True))
    assert expected_groups == actual_groups


def test_create_superuser_user_groups():
    groups.create_superuser_user_groups()
    assert Group.objects.count() == 3
    expected_groups = {"accelavar staff", "accelavar admin", "accelavar owner"}
    actual_groups = set(Group.objects.values_list("name", flat=True))
    assert expected_groups == actual_groups


def test_create_provider_user_group():
    groups.create_provider_user_groups(provider_id="provider")
    assert Group.objects.count() == 6
    expected_groups = {
        "accelavar staff",
        "accelavar admin",
        "accelavar owner",
        "provider owner",
        "provider admin",
        "provider staff",
    }
    actual_groups = set(Group.objects.values_list("name", flat=True))
    assert expected_groups == actual_groups


def test_create_customer_user_group():
    groups.create_customer_user_groups(
        provider_id="provider", customer_id="customer"
    )
    assert Group.objects.count() == 6
    expected_groups = {
        "accelavar staff",
        "accelavar admin",
        "accelavar owner",
        "provider customer owner",
        "provider customer admin",
        "provider customer staff",
    }
    actual_groups = set(Group.objects.values_list("name", flat=True))
    assert expected_groups == actual_groups


def test_get_superuser_group(superuser_owner_user):
    actual_group = groups.get_superuser_group(superuser_owner_user.role)
    expected_group = Group.objects.get(name="accelavar owner")
    assert expected_group == actual_group


def test_get_provider_group(provider_owner_user):
    user = provider_owner_user
    actual_group = groups.get_provider_group(
        provider_owner_user.provider.name, provider_owner_user.role
    )
    group_name = f"{user.provider.name} {user.role}"
    expected_group = Group.objects.get(name=group_name)
    assert expected_group == actual_group


def test_get_customer_group(customer_owner_user):
    user = customer_owner_user
    actual_group = groups.get_customer_group(
        user.provider.name, user.customer.name, user.role
    )
    group_name = f"{user.provider.name} {user.customer.name} {user.role}"
    expected_group = Group.objects.get(name=group_name)
    assert expected_group == actual_group


def test_assign_group_to_user_superuser_owner(superuser_owner_user):
    # Assign group to superuser owner user
    groups.assign_group_to_user(superuser_owner_user)
    assert superuser_owner_user.groups.count() == 1, (
        "Should belong to " "only one group"
    )
    assert superuser_owner_user.groups.first().name == "accelavar owner"


def test_assign_group_to_user_superuser_admin(superuser_admin_user):
    # Assign group to superuser owner user
    groups.assign_group_to_user(superuser_admin_user)
    assert superuser_admin_user.groups.count() == 1, (
        "Should belong to " "only one group"
    )
    assert superuser_admin_user.groups.first().name == "accelavar admin"


def test_assign_group_to_user_superuser_staff(superuser_staff_user):
    # Assign group to superuser owner user
    groups.assign_group_to_user(superuser_staff_user)
    assert superuser_staff_user.groups.count() == 1, (
        "Should belong to only " "one group"
    )
    assert superuser_staff_user.groups.first().name == "accelavar staff"


def test_assign_group_to_provider_owner_user(provider_owner_user):
    # Assign group to provider owner user
    groups.assign_group_to_user(provider_owner_user)
    assert provider_owner_user.groups.count() == 1, (
        "Should belong to only " "one group"
    )
    assert (
        provider_owner_user.groups.first().name
        == f"{provider_owner_user.provider.name} owner"
    )


def test_assign_group_to_provider_admin_user(provider_admin_user):
    # Assign group to provider owner user
    groups.assign_group_to_user(provider_admin_user)
    assert provider_admin_user.groups.count() == 1, (
        "Should belong to only " "one group"
    )
    assert (
        provider_admin_user.groups.first().name
        == f"{provider_admin_user.provider.name} admin"
    )


def test_assign_group_to_provider_staff_user(provider_staff_user):
    # Assign group to provider owner user
    groups.assign_group_to_user(provider_staff_user)
    assert provider_staff_user.groups.count() == 1, (
        "Should belong to only " "one group"
    )
    assert (
        provider_staff_user.groups.first().name
        == f"{provider_staff_user.provider.name} staff"
    )


def test_superuser_owner_user_model_permissions(superuser_owner_user):
    permissions = [
        "accounts.add_superuser_user",
        "accounts.add_provider_user",
        "accounts.add_provider",
    ]
    assert superuser_owner_user.has_perms(permissions)


def test_provider_owner_user_model_permissions(provider_owner_user):
    permissions = ["accounts.add_provider_user"]
    assert provider_owner_user.has_perms(permissions)


def test_customer_owner_user_model_permissions(customer_owner_user):
    permissions = ["accounts.add_customer_user"]
    assert customer_owner_user.has_perms(permissions)


def test_superuser_groups_superuser_object_permissions(superuser_admin_user):
    owner_permissions = ["change_user", "view_user", "delete_user"]
    admin_permissions = ["view_user"]
    staff_permissions = ["view_user"]
    superuser_owner_group = groups.get_superuser_group(Role.OWNER)
    superuser_admin_group = groups.get_superuser_group(Role.ADMIN)
    superuser_staff_group = groups.get_superuser_group(Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(superuser_owner_group, superuser_admin_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(superuser_admin_group, superuser_admin_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(superuser_staff_group, superuser_admin_user)
    )


def test_superuser_groups_provider_user_object_permissions(
    provider_owner_user,
):
    owner_permissions = ["change_user", "view_user", "delete_user"]
    admin_permissions = ["view_user"]
    staff_permissions = ["view_user"]
    superuser_owner_group = groups.get_superuser_group(Role.OWNER)
    superuser_admin_group = groups.get_superuser_group(Role.ADMIN)
    superuser_staff_group = groups.get_superuser_group(Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(superuser_owner_group, provider_owner_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(superuser_admin_group, provider_owner_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(superuser_staff_group, provider_owner_user)
    )


def test_superuser_groups_customer_user_object_permissions(
    customer_owner_user,
):
    owner_permissions = []
    admin_permissions = []
    staff_permissions = []
    superuser_owner_group = groups.get_superuser_group(Role.OWNER)
    superuser_admin_group = groups.get_superuser_group(Role.ADMIN)
    superuser_staff_group = groups.get_superuser_group(Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(superuser_owner_group, customer_owner_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(superuser_admin_group, customer_owner_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(superuser_staff_group, customer_owner_user)
    )


def test_provider_groups_superuser_user_object_permissions(
    provider_owner_user, superuser_owner_user
):
    owner_permissions = []
    admin_permissions = []
    staff_permissions = []
    provider = provider_owner_user.provider.name
    provider_owner_group = groups.get_provider_group(provider, Role.OWNER)
    provider_admin_group = groups.get_provider_group(provider, Role.ADMIN)
    provider_staff_group = groups.get_provider_group(provider, Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(provider_owner_group, superuser_owner_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(provider_admin_group, superuser_owner_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(provider_staff_group, superuser_owner_user)
    )


def test_provider_groups_provider_user_object_permissions(provider_admin_user):
    owner_permissions = ["change_user", "view_user", "delete_user"]
    admin_permissions = ["view_user"]
    staff_permissions = ["view_user"]
    provider = provider_admin_user.provider.name
    provider_owner_group = groups.get_provider_group(provider, Role.OWNER)
    provider_admin_group = groups.get_provider_group(provider, Role.ADMIN)
    provider_staff_group = groups.get_provider_group(provider, Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(provider_owner_group, provider_admin_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(provider_admin_group, provider_admin_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(provider_staff_group, provider_admin_user)
    )


def test_provider_groups_customer_user_object_permissions(customer_owner_user):
    owner_permissions = ["change_user", "view_user", "delete_user"]
    admin_permissions = ["view_user"]
    staff_permissions = ["view_user"]
    provider = customer_owner_user.provider.name
    provider_owner_group = groups.get_provider_group(provider, Role.OWNER)
    provider_admin_group = groups.get_provider_group(provider, Role.ADMIN)
    provider_staff_group = groups.get_provider_group(provider, Role.STAFF)
    assert set(owner_permissions) == set(
        get_group_perms(provider_owner_group, customer_owner_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(provider_admin_group, customer_owner_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(provider_staff_group, customer_owner_user)
    )


def test_customer_groups_customer_user_object_permissions(customer_staff_user):
    owner_permissions = ["change_user", "view_user", "delete_user"]
    admin_permissions = ["view_user"]
    staff_permissions = ["view_user"]
    provider = customer_staff_user.provider.name
    customer = customer_staff_user.customer.name
    customer_owner_group = groups.get_customer_group(
        provider, customer, Role.OWNER
    )
    customer_admin_group = groups.get_customer_group(
        provider, customer, Role.ADMIN
    )
    customer_staff_group = groups.get_customer_group(
        provider, customer, Role.STAFF
    )
    assert set(owner_permissions) == set(
        get_group_perms(customer_owner_group, customer_staff_user)
    )
    assert set(admin_permissions) == set(
        get_group_perms(customer_admin_group, customer_staff_user)
    )
    assert set(staff_permissions) == set(
        get_group_perms(customer_staff_group, customer_staff_user)
    )
