import pytest

from accounts.models import UserType, Role, User
from accounts.tests.factories import (
    UserFactory,
    ProviderFactory,
    CustomerFactory,
    ProfileFactory,
)


@pytest.fixture(autouse=True)
def superuser_owner_user(db):
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    user = UserFactory(
        user_type=user_type, user_role=role, provider=None, customer=None
    )
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


def teardown_users():
    User.objects.all().delete()


@pytest.fixture(autouse=True)
def superuser_admin_user(db):
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    user = UserFactory(
        user_type=user_type, user_role=role, provider=None, customer=None
    )
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def superuser_staff_user(db):
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    user = UserFactory.create(
        user_type=user_type, user_role=role, provider=None, customer=None
    )
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def provider_owner_user(db):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    provider = ProviderFactory()
    user = UserFactory(user_type=user_type, user_role=role, provider=provider)
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def provider_admin_user(db):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    provider = ProviderFactory()
    user = UserFactory(user_type=user_type, user_role=role, provider=provider)
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def provider_staff_user(db):
    user_type = UserType.objects.get(name=UserType.PROVIDER)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    provider = ProviderFactory()
    user = UserFactory(user_type=user_type, user_role=role, provider=provider)
    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def customer_owner_user(db):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
    provider = ProviderFactory()
    customer = CustomerFactory(provider=provider)
    user = UserFactory(
        user_type=user_type,
        user_role=role,
        provider=provider,
        customer=customer,
    )

    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def customer_admin_user(db):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.ADMIN, user_type=user_type)
    provider = ProviderFactory()
    customer = CustomerFactory(provider=provider)
    user = UserFactory(
        user_type=user_type,
        user_role=role,
        provider=provider,
        customer=customer,
    )

    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def customer_staff_user(db):
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    role = Role.objects.get(role_name=Role.STAFF, user_type=user_type)
    provider = ProviderFactory()
    customer = CustomerFactory(provider=provider)
    user = UserFactory(
        user_type=user_type,
        user_role=role,
        provider=provider,
        customer=customer,
    )

    user.set_password("password123")
    user.save()
    ProfileFactory(user=user)
    yield user
    teardown_users()


@pytest.fixture(autouse=True)
def provider():
    pass


@pytest.fixture(autouse=True)
def customer():
    pass
