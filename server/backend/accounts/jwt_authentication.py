from rest_framework_jwt.authentication import (
    JSONWebTokenAuthentication,
)

from accounts.token_manager import AccessToken


class CustomJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    """
    Custom class to verify user by JWT token. Checks for provider.
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        payload = AccessToken.verify(jwt_value)
        user = self.authenticate_credentials(payload)

        # check the provider for user is same as request.provider
        if user.provider != request.provider:
            user = None
        return user, jwt_value
