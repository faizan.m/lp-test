# Generated by Django 2.0.5 on 2018-08-31 06:37

# import partial_index
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0039_auto_20180824_0917_squashed_0044_auto_20180828_1130'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='user',
            unique_together=set(),
        ),
        # migrations.AddIndex(
        #     model_name='user',
        #     index=partial_index.PartialIndex(fields=['email', 'provider'], name='accounts_us_email_b19a11_partial', unique=True, where=partial_index.PQ(provider__isnull=False)),
        # ),
        # migrations.AddIndex(
        #     model_name='user',
        #     index=partial_index.PartialIndex(fields=['email'], name='accounts_us_email_8917e7_partial', unique=True, where=partial_index.PQ(provider__isnull=True)),
        # ),
    ]
