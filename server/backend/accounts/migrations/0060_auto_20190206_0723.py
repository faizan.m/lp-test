# Generated by Django 2.0.5 on 2019-02-06 07:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0059_profile_profile_pic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='circuitinfo',
            name='contract_end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
