# Generated by Django 2.0.5 on 2018-08-29 11:28

import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models

import accounts.managers
import accounts.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0013_create_usertypes_and_roles'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='role',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='usertype',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelManagers(
            name='user',
            managers=[
                ('objects', accounts.managers.UserManager()),
                ('superusers', accounts.models.SuperuserUserManager()),
                ('providers', accounts.models.ProviderUserManager()),
                ('customers', accounts.models.CustomerUserManager()),
                ('providers_and_customers', accounts.models.ProviderAndCustomerManager()),
            ],
        ),
        migrations.AddField(
            model_name='provider',
            name='accounting_contact',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='accounting_contact', to='accounts.AccountingContact'),
        ),
        migrations.AddField(
            model_name='provider',
            name='address',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='address', to='accounts.Address'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='address',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accounts.Address'),
        ),
        migrations.AlterField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_superuser',
            field=models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_role',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='members', to='accounts.Role'),
        ),
        migrations.AddField(
            model_name='provider',
            name='support_contact',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='provider',
            name='http_protocol',
            field=models.CharField(choices=[('http://', ('https://', 'https://'))], default='http://', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='accountingcontact',
            name='email',
            field=models.EmailField(default='email@example.com', max_length=254),
            preserve_default=False,
        ),
        migrations.AlterModelOptions(
            name='provider',
            options={'ordering': ('-id',), 'permissions': (('view_provider', 'can view provider'),)},
        ),
        migrations.AlterField(
            model_name='address',
            name='address_2',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='accountingcontact',
            name='website',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='provider',
            name='url',
            field=models.CharField(max_length=200, unique=True),
        ),
        migrations.AlterField(
            model_name='provider',
            name='http_protocol',
            field=models.CharField(choices=[('http://', 'http://'), ('https://', 'https://')], max_length=10),
        ),
        migrations.AddField(
            model_name='provider',
            name='crm_integration_configured',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='provider',
            name='monitoring_integration_configured',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='provider',
            name='logo',
            field=models.URLField(blank=True, max_length=3000, null=True),
        ),
        migrations.AlterField(
            model_name='provider',
            name='url',
            field=models.CharField(max_length=3000, unique=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(max_length=30),
        ),
        migrations.AddField(
            model_name='provider',
            name='integration_statuses',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default={'crm_authentication_configured': False, 'crm_board_mapping_configured': False, 'crm_device_categories_configured': False}, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='address_1',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='city',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='state',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='zip_code',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='customer',
            name='crm_id',
            field=models.PositiveIntegerField(blank=True, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='department',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='title',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='activation_mails_sent_count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='customer',
            name='renewal_service_allowed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='provider',
            name='ticket_note',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='provider',
            name='url_alias',
            field=models.CharField(blank=True, max_length=500, null=True, unique=True),
        ),
    ]
