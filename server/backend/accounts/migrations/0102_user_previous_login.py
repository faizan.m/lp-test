# Generated by Django 2.0.5 on 2021-12-23 06:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0101_auto_20211206_1046'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='previous_login',
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
    ]
