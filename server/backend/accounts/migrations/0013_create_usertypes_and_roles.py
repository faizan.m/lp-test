# Generated by Django 2.0.5 on 2018-05-28 05:51

from django.db import migrations


class Migration(migrations.Migration):

    def call_command_to_create_roles(apps, schema_editor):
        """
        Creates initial user types and roles in the database
        :return: none
        """
        user_types = ["accelavar", "provider", "customer"]
        roles = ["owner", "admin", "staff"]
        UserType = apps.get_model("accounts", "UserType")
        Role = apps.get_model("accounts", "Role")
        for type in user_types:
            user_type = UserType.objects.create(name=type)
            for role in roles:
                role = Role.objects.create(role_name=role, user_type=user_type)

    def delete_user_types_and_roles(apps, schema_editor):
        UserType = apps.get_model("accounts", "UserType")
        Role = apps.get_model("accounts", "Role")
        Role.objects.all().delete()
        UserType.objects.all().delete()

    dependencies = [
        ('accounts', '0001_initial_squashed_0012_auto_20180528_1145'),
    ]

    operations = [
        migrations.RunPython(call_command_to_create_roles, delete_user_types_and_roles),
    ]
