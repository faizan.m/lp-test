import csv
import os
import shutil
import tempfile
from datetime import datetime

import numpy as np
import pandas as pd
import requests
from box import Box
from django.conf import settings
from django.db.models import F
from django.forms import model_to_dict
from django.template.loader import get_template
from weasyprint import CSS, HTML

from accounts.circuit_import_serializers import CircuitInfoImportSerializer
from accounts.models import (
    CircuitInfo,
    Customer,
    HistoricalCircuitInfo,
    Provider,
)
from accounts.serializers import (
    CircuitInfoListRetrieveSerializer,
)
from core.utils import generate_random_id, group_list_of_dictionary_by_key
from storage.file_upload import FileUploadService
from utils import get_app_logger, get_valid_file_name

logger = get_app_logger(__name__)


class CircuitInfoService(object):
    CIRCUIT_REPORTS_TYPES = Box(
        CIRCUIT_INFO_REPORT="CIRCUIT_INFO_REPORT",
        CIRCUIT_LIFE_CYCLE_REPORT="CIRCUIT_LIFE_CYCLE_REPORT",
    )

    @classmethod
    def perform_create(cls, data):
        instance = CircuitInfo.objects.create(**data)
        cls.update_historical_data(model_to_dict(instance), type="create")

    @classmethod
    def perform_delete(cls, instance):
        instance.is_deleted = True
        instance.save()
        cls.update_historical_data(model_to_dict(instance), type="delete")

    @classmethod
    def update_historical_data(cls, instance_data, type):
        serializer = CircuitInfoListRetrieveSerializer(instance_data)
        data = serializer.data
        data.update({"customer_id": instance_data.get("customer")})
        result = dict(
            old_instance=data,
            circuit_info_id=instance_data.get("id"),
            change_type=type,
        )
        HistoricalCircuitInfo.objects.create(**result)

    @classmethod
    def get_customer_reporting_types(cls):
        circuit_info_report = dict(
            name="Circuit Info Report", display_name="Circuit Info Report"
        )
        circuit_life_cycle_report = dict(
            name="Circuit Life Cycle Management Report",
            display_name="Circuit Life Cycle Management Report",
        )
        REPORTS = {
            CircuitInfoService.CIRCUIT_REPORTS_TYPES.CIRCUIT_INFO_REPORT: circuit_info_report,
            CircuitInfoService.CIRCUIT_REPORTS_TYPES.CIRCUIT_LIFE_CYCLE_REPORT: circuit_life_cycle_report,
        }
        return REPORTS

    @classmethod
    def get_and_parse_circuit_report_data_by_type(
        cls, provider_id, customer_id, type
    ):
        values_list = [
            "type",
            "circuit_id",
            "provider",
            "cw_site_id",
            "contact_phone",
            "device_crm_id",
            "contract_end_date",
            "bandwidth",
            "account_number",
        ]
        queryset = (
            CircuitInfo.objects.filter(customer=customer_id, is_deleted=False)
            .annotate(type=F("circuit_type__name"))
            .values(*values_list)
        )
        if queryset:
            data = list(queryset)
            site_ids = []
            device_crm_ids = []
            _device_crm_data = {}
            for item in data:
                site_ids.append(item.get("cw_site_id"))
                if item.get("device_crm_id"):
                    device_crm_ids.append(item.get("device_crm_id"))
            provider = Provider.objects.get(id=provider_id)
            customer = Customer.objects.get(id=customer_id)
            _customer_sites = provider.erp_client.get_customer_complete_site_addresses_with_name(
                customer.crm_id, site_ids=tuple(site_ids)
            )
            if (
                type
                == CircuitInfoService.CIRCUIT_REPORTS_TYPES.CIRCUIT_INFO_REPORT
            ):
                required_fields = ["id", "name", "serialNumber"]
                if device_crm_ids:
                    _device_crm_data = provider.erp_client.get_device_by_ids(
                        device_crm_ids, required_fields=required_fields
                    )
                    _device_crm_data = group_list_of_dictionary_by_key(
                        _device_crm_data, "id"
                    )
                else:
                    _device_crm_data = None
            for item in data:
                _site_id = item.get("cw_site_id")
                item["site_address"] = _customer_sites.get(_site_id)
                if item.get("contract_end_date"):
                    item["contract_end_date"] = (
                        datetime.strftime(
                            item["contract_end_date"], "%m/%d/%Y"
                        )
                        if item["contract_end_date"]
                        else ""
                    )
                _device_crm_id = item.get("device_crm_id")
                if _device_crm_data and _device_crm_data.get(_device_crm_id):
                    _device_name = _device_crm_data.get(_device_crm_id)[0].get(
                        "device_name"
                    )
                    _serial_number = _device_crm_data.get(_device_crm_id)[
                        0
                    ].get("serial_number")
                    item[
                        "device_name"
                    ] = f"{_device_name} - ({_serial_number})"
            data = group_list_of_dictionary_by_key(data, "site_address")
            return data
        return None

    @classmethod
    def generate_customer_circuit_info_report(
        cls, provider_id, customer_id, data
    ):
        parsed_data = cls.get_and_parse_circuit_report_data_by_type(
            provider_id, customer_id, data.get("type")
        )
        if parsed_data:
            customer = Customer.objects.get(id=customer_id)
            report_types = cls.get_customer_reporting_types()
            report_type = data.get("type")
            report_meta = report_types.get(report_type)
            if (
                report_type
                == CircuitInfoService.CIRCUIT_REPORTS_TYPES.CIRCUIT_INFO_REPORT
            ):
                html_template_file = "reports/circuit_info_report.html"
            else:
                html_template_file = "reports/circuit_life_cycle_report.html"
            file_name = get_valid_file_name(
                f"{report_meta.get('display_name')} - {customer.name}.pdf"
            )
            template = get_template(html_template_file)
            template_vars = {
                "customer_name": customer.name,
                "table_html": parsed_data,
                "table_columns": list(parsed_data.keys()),
                "report_meta": report_meta,
            }
            html_out = template.render(template_vars)
            TEMP_DIR = tempfile.gettempdir()
            file_path = os.path.join(TEMP_DIR, file_name)
            HTML(string=html_out).write_pdf(
                target=file_path,
                stylesheets=[
                    CSS(os.path.join(settings.STATIC_ROOT, "css/cash.css"))
                ],
            )
            return file_path, file_name
        return None, None


class CircuitInfoImportService:
    """
    Service class to handle Circuit Import from CSV file
    """

    @classmethod
    def _generate_import_error_csv(cls, errors, error_indexes, field_mapping):
        """
        Create CSV file with errors
        Args:
            errors: list of errors dict
            error_indexes: list of error indexes
            field_mapping: mapping of user selected fields and internal fields

        Returns: Dict of the form
         {
            "file_name": "0NLB1AVJBDQ3544.csv",
            "file_path": "/tmp/0NLB1AVJBDQ3544.csv"
          }

        """
        headers = ["Row #", "Field", "Error Messages"]
        new_data = []
        for pos, error in enumerate(errors):
            row_number = error_indexes[pos] + 2
            for field, field_errors in error.items():
                _data = {}
                _data["Row #"] = row_number
                _data["Field"] = field_mapping.get(field, field)
                _data["Error Messages"] = ", ".join(field_errors)
                new_data.append(_data)
        TEMP_DIR = tempfile.gettempdir()
        file_name = f"{generate_random_id(15)}.csv"
        file_path = os.path.join(TEMP_DIR, file_name)
        try:
            with open(file_path, "w") as output_file:
                dict_writer = csv.DictWriter(output_file, headers)
                dict_writer.writeheader()
                dict_writer.writerows(new_data)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        url = FileUploadService.upload_file(file_path)
        file_path = url
        return dict(file_name=file_name, file_path=file_path)

    @classmethod
    def _download_file(cls, file_path):
        file_name = f"{generate_random_id(10)}.csv"
        local_file_path = os.path.join(tempfile.gettempdir(), file_name)
        if settings.DEBUG:
            shutil.copy(file_path, local_file_path)
        else:
            resp = requests.get(file_path)
            try:
                with open(local_file_path, "wb") as output:
                    output.write(resp.content)
            except (FileNotFoundError, OSError) as e:
                logger.error(e)
                return None
        return local_file_path

    @classmethod
    def _parse_file(
        cls, file_path, field_mapping_dict, site_mapping, circuit_type_mapping
    ):
        """
        Read and parse input csv file. Do the field mapping and return data
        Args:
            file_path: csv file path
            field_mapping_dict: mapping of user selected fields and internal fields
            site_mapping: mapping of site from file to site_id
            circuit_type_mapping: mapping of circuit to circuit_id

        Returns: Return list of records

        """
        local_file_path = cls._download_file(file_path)
        if local_file_path is None:
            return None
        df = pd.read_csv(local_file_path)
        df.rename(columns=lambda x: x.strip(), inplace=True)
        df.replace(np.nan, "", regex=True, inplace=True)
        df.rename(
            columns={v: k for k, v in field_mapping_dict.items()}, inplace=True
        )
        df.cw_site_id = df.cw_site_id.map(site_mapping)
        df.circuit_type = df.circuit_type.map(circuit_type_mapping)
        if "bandwidth" in df and "bandwidth_measurement" in df:
            df.bandwidth = df[["bandwidth", "bandwidth_measurement"]].apply(
                lambda x: " ".join(map(str, x)), axis=1
            )
            df.drop(["bandwidth_measurement"], axis=1, inplace=True)
        return df.to_dict("records")

    @classmethod
    def _validate_data(cls, records):
        """
        validate the input data using serializer, keep list of errors and return validated data
        Args:
            records: list of records

        Returns: tuple of the form serializer.validated_data, errors, error_indexes

        """
        serializer = CircuitInfoImportSerializer(data=records, many=True)
        serializer.is_valid()
        error_indexes = []
        errors = []
        if serializer.errors:
            errors = []
            for idx, row_errors in enumerate(list(serializer.errors)):
                if row_errors:
                    errors.append(row_errors)
                    error_indexes.append(idx)
        records = [
            r for idx, r in enumerate(records) if idx not in error_indexes
        ]
        serializer = CircuitInfoImportSerializer(data=records, many=True)
        serializer.is_valid()
        return serializer.validated_data, errors, error_indexes

    @classmethod
    def import_from_file(
        cls,
        provider,
        customer_id,
        file_path,
        field_mapping,
        site_mapping,
        circuit_type_mapping,
    ):
        """
        Takes an input csv, parse it, validate it and create the circuit
        Args:
            provider: Provider Object
            customer_id: Customer ID
            file_path: CSV file path
            field_mapping: mapping of user selected fields and internal fields
            site_mapping: mapping of site from file to site_id
            circuit_type_mapping: mapping of circuit to circuit_id

        Returns: Dict of the form
        {
          "total_requested_item_count": 2,
          "failed_item_count": 0,
          "created_item_count": 2,
          "error_file": {
            "file_name": "0NLB1AVJBDQ3544.csv",
            "file_path": "/tmp/0NLB1AVJBDQ3544.csv"
          }
        }

        """
        records = cls._parse_file(
            file_path, field_mapping, site_mapping, circuit_type_mapping
        )
        if records is None:
            return {}
        circuits, errors, error_indexes = cls._validate_data(records)
        created_count, updated_count = 0, 0
        for circuit in circuits:
            circuit.update({"customer_id": customer_id})
            try:
                instance = CircuitInfo.objects.get(
                    circuit_id=circuit.get("circuit_id"),
                    circuit_type=circuit.get("circuit_type"),
                    customer_id=customer_id,
                )
                for attr, value in circuit.items():
                    setattr(instance, attr, value)
                instance.save()
                circuit.update({"id": instance.id})
                CircuitInfoService.update_historical_data(
                    circuit, type="update"
                )
                updated_count += 1
            except CircuitInfo.DoesNotExist:
                CircuitInfoService.perform_create(circuit)
                created_count += 1

        total_requested_item_count = len(records)
        failed_item_count = len(error_indexes)
        if errors:
            error_file = cls._generate_import_error_csv(
                errors, error_indexes, field_mapping
            )
        else:
            error_file = {}
        result = dict(
            total_requested_item_count=total_requested_item_count,
            failed_item_count=failed_item_count,
            created_item_count=created_count,
            updated_count=updated_count,
            error_file=error_file,
        )
        return result
