import os
from time import sleep

from authy import AuthyException
from authy.api import AuthyApiClient, OneTouch
from rest_framework.exceptions import ValidationError

from caching.service import CacheService

AUTHY_API_KEY = os.environ.get(
    "AUTHY_API_KEY", "iRGexMJ7tlEYmQwdlSPZ7gPGxkYnwVws"
)


class TwilioAuthenticationService(object):
    @classmethod
    def _get_client(cls):
        return AuthyApiClient(AUTHY_API_KEY)

    @classmethod
    def create_auth_account(cls, data):
        email = data.get("email")
        phone = data.get("phone")
        country_code = data.get("country_code")
        client = TwilioAuthenticationService._get_client()
        user = client.users.create(email, phone, country_code)
        if user.errors():
            raise ValidationError(user.errors())
        return user

    @classmethod
    def delete_auth_account(cls, auth_id):
        client = TwilioAuthenticationService._get_client()
        user = client.users.delete(auth_id)
        if user.ok():
            return True
        else:
            raise ValidationError(user.errors())

    @classmethod
    def request_two_factor_authentication(cls, auth_id, request_type):
        client = TwilioAuthenticationService._get_client()
        if request_type == "AUTHY_NOTIFY":
            response = cls.request_one_touch_request(auth_id)
        elif request_type == "SMS":
            response = client.users.request_sms(auth_id)
        else:
            response = client.users.request_call(auth_id)
        return response.content

    @classmethod
    def request_one_touch_request(cls, auth_id):
        client = TwilioAuthenticationService._get_client()
        user_id = auth_id
        message = "Login requested for a Acela Portal."
        seconds_to_expire = 120
        response = client.one_touch.send_request(
            user_id, message, seconds_to_expire=seconds_to_expire
        )
        if response.ok():
            uuid = response.get_uuid()
            cache_key = f"One Touch Request: auth-id: {auth_id} uuid: {uuid}"
            CacheService.set_data(cache_key, "REQUESTED")
            return response
        else:
            return response.errors()

    @classmethod
    def validate_authentication_request(cls, auth_id, data):
        code = data.get("code")
        type = data.get("type")
        cache_key = f"One Touch Request: auth-id: {auth_id} uuid: {code}"
        if type == "AUTHY_NOTIFY":
            if CacheService.get_data(cache_key) == "REQUESTED":
                cache_key = (
                    f"One Touch Response auth-id: {auth_id} uuid: {code}"
                )
                pause_time = 2
                wait_time = 30
                while wait_time > 0:
                    result = CacheService.get_data(cache_key)
                    if isinstance(result, bool):
                        CacheService.invalidate_data(cache_key)
                        return result
                    else:
                        wait_time -= pause_time
                        sleep(pause_time)
                return False
            raise ValidationError(
                {"detail": ["You need to raise a One Touch Request"]}
            )
        client = TwilioAuthenticationService._get_client()
        try:
            verification = client.tokens.verify(auth_id, code)
            if verification.ok():
                return True
            else:
                return False
        except AuthyException as e:
            raise ValidationError({"msg": e})

    @classmethod
    def validate_authy_notify_request(cls, url, method, headers, params):
        one_touch = OneTouch(url, AUTHY_API_KEY)
        result = one_touch.validate_one_touch_signature(
            headers["HTTP_X_AUTHY_SIGNATURE"],
            headers["HTTP_X_AUTHY_SIGNATURE_NONCE"],
            method,
            url,
            params,
        )
        if result is False:
            return

        auth_id = int(params.get("authy_id"))
        user_response = False if params.get("status") == "denied" else True
        uuid = params.get("uuid")
        cache_key = f"One Touch Response auth-id: {auth_id} uuid: {uuid}"
        CacheService.set_data(cache_key, user_response)
        return user_response

    @classmethod
    def start_phone_verification(cls, phone, country_code, via="sms"):
        client = TwilioAuthenticationService._get_client()
        try:
            request = client.phones.verification_start(
                phone, country_code, via=via
            )
            if request.errors():
                raise ValidationError(request.errors())
            else:
                return True
        except AuthyException as e:
            raise ValidationError({"msg": e})

    @classmethod
    def validate_phone_verification(cls, phone, country_code, code):
        client = TwilioAuthenticationService._get_client()
        verification = client.phones.verification_check(
            phone, country_code, code
        )
        if verification.ok():
            return True
        else:
            return False

    @classmethod
    def hide_phone_number(cls, phone_number):
        if phone_number:
            return (
                phone_number[:2]
                + "X" * len(phone_number[2:-3])
                + phone_number[-3:]
            )
        else:
            return ""
