# contains logic related to Customer
from collections import defaultdict
from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import connection
from django.db.models import Count
from django.apps import apps
from accounts.models import UserType

User = get_user_model()


class CustomerService(object):
    @classmethod
    def get_most_recent_logged_in_user(cls, customer_id):
        """
        Returns customer user object which logged in most recently
        :param customer_id: Customer Id
        :return: User object
        """
        user = (
            User.customers.filter(
                customer_id=customer_id, last_login__isnull=False
            )
            .order_by("-last_login")
            .first()
        )
        return user

    @classmethod
    def get_customers_users_metrics(cls, provider_id):
        user_type_id = UserType.objects.get(name=UserType.CUSTOMER).id
        q = (
            f"SELECT customer_id, customer_name, user_id, user_name, last_login, count_last_90_days "
            f"FROM "
            f"(SELECT cust.id, cust.name customer_name, customer_id, au.id  user_id, au.last_login, "
            f"concat_ws(' ', au.first_name, au.last_name) user_name, "
            f"ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY au.last_login DESC) row_number, "
            f"COUNT(*) OVER (PARTITION BY customer_id ORDER BY au.last_login) count_last_90_days "
            f"FROM accounts_customer cust "
            f"JOIN accounts_user au on cust.id = au.customer_id "
            f"WHERE cust.provider_id = {provider_id} AND au.is_active=true "
            f"AND user_type_id = {user_type_id} "
            f"AND last_login NOTNULL "
            f"AND last_login > CURRENT_DATE - INTERVAL '90' DAY) t "
            f"WHERE row_number = 1;"
        )
        customer_stats = {}
        with connection.cursor() as cursor:
            cursor.execute(q)
            for item in cursor:
                customer_stats[item[0]] = {
                    "id": item[0],
                    "name": item[1],
                    "user_id": str(item[2]),
                    "user_name": item[3],
                    "user_last_login": datetime.strftime(
                        item[4], "%Y-%m-%dT%H:%M:%SZ"
                    ),
                    "count_last_90_days": item[5],
                }
        customer_user_activity_queryset = (
            User.objects.filter(
                is_active=True, user_type_id=user_type_id, provider=provider_id
            )
            .values("customer_id")
            .annotate(users_count=Count("id"))
            .values("users_count", "customer_id")
            .order_by("-users_count")
        )
        for each in customer_user_activity_queryset:
            if customer_stats.get(each["customer_id"]):
                customer_stats[each["customer_id"]].update(
                    {"total_active_users": each["users_count"]}
                )
        result = list(customer_stats.values())
        result.sort(key=lambda item: item["user_last_login"], reverse=True)
        return result


def fetch_related_models_to_customers(model_name):
    # Retrieve all models in your Django project
    all_models = apps.get_models()

    # Find the model instance
    for model in all_models:
        if model.__name__ == model_name:
            model_instance = model
            # Retrieve all related models for model
            related_models = model_instance._meta.related_objects
            return related_models
