from django.apps import AppConfig
from django.db.models.signals import post_migrate

from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


def superuser_groups(*args, **kwargs):
    from accounts.groups import create_superuser_user_groups

    create_superuser_user_groups()
    logger.debug("Superuser groups created.")


class AccountsConfig(AppConfig):
    name = "accounts"

    def ready(self):
        post_migrate.connect(superuser_groups, sender=self)
