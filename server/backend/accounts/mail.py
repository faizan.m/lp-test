import datetime
import os
from copy import deepcopy
from email.mime.image import MIMEImage
from smtplib import SMTPResponseException

import requests
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.template.context import make_context
from django.template.loader import get_template
from utils import get_app_logger

logger = get_app_logger(__name__)
from accounts.models import StoredEmailData


class BaseEmailMessage(mail.EmailMultiAlternatives):
    _node_map = {
        "subject": "subject",
        "text_body": "body",
        "html_body": "html",
    }
    template_name = None

    def __init__(
        self,
        request=None,
        context=None,
        template_name=None,
        images=None,
        files=None,
        remote_attachments=None,
        *args,
        **kwargs,
    ):
        super(BaseEmailMessage, self).__init__(*args, **kwargs)

        self.request = request
        self.context = {} if context is None else context
        self.html = None

        if template_name is not None:
            self.template_name = template_name
        self.images = images
        self.files = files
        self.remote_attachments = remote_attachments

    def get_context_data(self):
        self.images = self.images or []
        images = [("logo.png", "logo")]
        self.images.extend(images)
        context = deepcopy(self.context)
        if self.request:
            site = get_current_site(self.request)
            domain = context.get("domain") or (
                getattr(settings, "DOMAIN", "") or site.domain
            )
            protocol = context.get("protocol") or (
                "https" if self.request.is_secure() else "http"
            )
            site_name = context.get("site_name") or (
                getattr(settings, "SITE_NAME", "") or site.name
            )
            user = context.get("user") or self.request.user
        else:
            domain = context.get("domain") or getattr(settings, "DOMAIN", "")
            protocol = context.get("protocol") or "http"
            site_name = context.get("site_name") or getattr(
                settings, "SITE_NAME", ""
            )
            user = context.get("user")

        context.update(
            {
                "domain": domain,
                "protocol": protocol,
                "site_name": site_name,
                "user": user,
                "year": datetime.datetime.now().year,
            }
        )
        return context

    def render(self):
        context = make_context(self.get_context_data(), request=self.request)
        template = get_template(self.template_name)
        with context.bind_template(template.template):
            for node in template.template.nodelist:
                self._process_node(node, context)
        self._attach_body()
        # self._add_images()
        self._attach_files()
        self._attach_remote_files()

    @staticmethod
    def _filter_emails(addresses):
        """
        Avoid sending emails to actual email addresses in test environments.
        Works on the ENV_NAME setting. If the ENV_NAME is settings.TEST_ENV_NAME,
        only the specified email and email with specific domain will be sent.
        """
        if settings.ENV_NAME == settings.TEST_ENV_NAME or settings.DEBUG:
            domain = "yopmail.com"
            addresses = [
                email for email in addresses if email.endswith(domain)
            ]
        return addresses

    @staticmethod
    def _add_test_emails(addresses):
        if settings.ENV_NAME == settings.TEST_ENV_NAME or settings.DEBUG:
            # Add test emails
            test_emails = settings.TEST_EMAILS
            addresses.extend(test_emails)
        return addresses

    def store_email_data(self):
        # store image paths in a list
        images = []
        if self.images:
            for filename, file_id in self.images:
                images.append((filename, file_id))

        # store remote filename and url
        remote_attachments = []
        if self.remote_attachments:
            for file_name, url in self.remote_attachments:
                remote_attachments.append((file_name, url))

        # store file paths
        files = []
        if self.files:
            for file_name, file_path in self.files:
                files.append((file_name, file_path))

        email_data = dict()
        email_data["to"] = self.to or []
        email_data["cc"] = self.cc or []
        email_data["bcc"] = self.bcc or []
        email_data["subject"] = self.subject or ""
        email_data["images"] = images
        email_data["remote_attachments"] = remote_attachments
        email_data["files"] = files
        email_data["template_name"] = self.template_name or ""
        email_data["reply_to"] = self.reply_to or []
        email_data["from_email"] = self.from_email or ""
        email_data["body"] = self.body or ""
        email_data["html"] = self.html or ""

        StoredEmailData.objects.create(
            email_data=email_data, provider=self.context.get("provider")
        )

    def send(self, to, cc=None, bcc=None, *args, **kwargs):
        self.render()
        self.to = self._add_test_emails(self._filter_emails(to))
        if cc:
            self.cc = self._filter_emails(cc)
        if bcc:
            self.bcc = self._filter_emails(bcc)
        if self.context.get("provider"):
            self.store_email_data()
        log_error: bool = kwargs.pop("log_error", True)
        try:
            super(BaseEmailMessage, self).send(*args, **kwargs)
        except SMTPResponseException as e:
            if log_error:
                logger.error(
                    f" Error while sending email error code {e.smtp_code} & message {e.smtp_error}"
                )
            self.from_email = settings.DEFAULT_FROM_EMAIL
            BaseEmailMessage.resend(
                self, to, cc=None, bcc=None, *args, **kwargs
            )

    def resend(self, to, cc=None, bcc=None, *args, **kwargs):
        self.html = self.body
        self._attach_body()
        self._attach_files()
        self._attach_remote_files()
        self.to = self._add_test_emails(self._filter_emails(to))
        if cc:
            self.cc = self._filter_emails(cc)
        if bcc:
            self.bcc = self._filter_emails(bcc)
        super(BaseEmailMessage, self).send(*args, **kwargs)

    def _process_node(self, node, context):
        attr = self._node_map.get(getattr(node, "name", ""))
        if attr is not None:
            setattr(self, attr, node.render(context).strip())

    def _attach_body(self):
        if self.body and self.html:
            self.attach_alternative(self.html, "text/html")
        elif self.html:
            self.body = self.html
            self.content_subtype = "html"

    def _add_images(self):
        if self.images:
            for filename, file_id in self.images:
                image_path = os.path.join(
                    settings.STATIC_ROOT, settings.EMAIL_IMAGE_DIR, filename
                )
                image_file = open(image_path, "rb")
                msg_image = MIMEImage(image_file.read())
                image_file.close()
                msg_image.add_header("Content-ID", "<%s>" % file_id)
                self.attach(msg_image)

    def _attach_files(self):
        if self.files:
            for file_name, file_path in self.files:
                try:
                    with open(file_path, "rb") as attachment:
                        self.attach(file_name, content=attachment.read())
                except (FileNotFoundError, OSError) as e:
                    logger.error(e)
                    continue

    def _attach_remote_files(self):
        if self.remote_attachments:
            for file_name, url in self.remote_attachments:
                response = requests.get(url)
                if response.ok:
                    self.attach(file_name, response.content)
                else:
                    continue
