from django.core.management.base import BaseCommand

from accounts.models import User


class Command(BaseCommand):
    help = "Create Owner User for Accelavar."

    def handle(self, *args, **options):
        User.objects.create_superuser(
            first_name="first_name",
            last_name="last_name",
            email="owner@lp.com",
            password="password123",
        )
        self.stdout.write(
            self.style.SUCCESS("Successfully created Accelavar Owner User")
        )
