from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from accounts.models import Provider

        providers = Provider.objects.only("url")
        for provider in providers:
            sub_domain, domain = provider.url.split(".", 1)
            if domain != "acela.io":
                provider.url = ".".join([sub_domain, "acela.io"])
                provider.save()
                self.stdout.write(
                    self.style.SUCCESS("URLS updated successfully.")
                )
