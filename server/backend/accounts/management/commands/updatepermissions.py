from django.core.management import BaseCommand

from accounts.data_migration_scripts import (
    update_all_model_permissions,
    update_all_object_permissions,
)


class Command(BaseCommand):
    help = "Update all groups model and object permissions"

    def handle(self, *args, **options):
        self.stdout.write(
            self.style.SUCCESS("Updating group model permissions.")
        )
        update_all_model_permissions()
        self.stdout.write(self.style.SUCCESS("Model permissions Updated."))
        self.stdout.write(
            self.style.SUCCESS("Updating group object permissions.")
        )
        update_all_object_permissions()
        self.stdout.write(self.style.SUCCESS("Object permissions Updated."))
