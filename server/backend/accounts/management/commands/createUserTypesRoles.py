from django.core.management.base import BaseCommand

from accounts.groups import create_initial_user_types_and_roles
from accounts.models import UserType


class Command(BaseCommand):
    help = "Create initial objects for UserType and Roles model"

    def handle(self, *args, **options):
        if not UserType.objects.count() > 0:
            create_initial_user_types_and_roles()
            self.stdout.write(
                self.style.SUCCESS("Successfully created User Types and Roles")
            )
