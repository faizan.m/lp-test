from django.core.management import BaseCommand

from accounts.models import UserType, Role, User, Profile


def create_acela_owner(email, password):
    user_type = UserType.objects.get(name=UserType.ACCELAVAR)
    role = Role.objects.get(user_type=user_type, role_name=Role.OWNER)
    user = User.objects.create(
        email=email, user_type=user_type, user_role=role, is_active=True
    )
    user.set_password(password)
    user.save()
    Profile.objects.create(user=user)
    return user


class Command(BaseCommand):
    help = " Create Owner User for Acela"

    def add_arguments(self, parser):
        parser.add_argument(
            "--email", dest="email", required=True, help="Email Id of User"
        )
        parser.add_argument(
            "--p", dest="password", required=True, help="Password for User"
        )

    def handle(self, *args, **options):
        email = options.get("email")
        if User.superusers.filter(email=email).count() > 0:
            self.stderr.write(self.style.ERROR("Email already exists."))
            return
        password = options.get("password")
        create_acela_owner(email.lower(), password)
        self.stdout.write(self.style.SUCCESS("Acela Superuser created."))
