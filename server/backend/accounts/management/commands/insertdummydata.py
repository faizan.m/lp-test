from django.core.management.base import BaseCommand

from accounts.dbdummydata.providers import create_providers
from accounts.dbdummydata.users import (
    create_provider_users,
    create_superuser_users,
)
from accounts.models import User


class Command(BaseCommand):
    help = "Insert Dummy data to database."

    def handle(self, *args, **options):
        if not User.objects.count() > 0:
            create_superuser_users()
            self.stdout.write(self.style.SUCCESS("superuser users created"))
            providers = create_providers()
            self.stdout.write(self.style.SUCCESS("Provider created"))
            create_provider_users(providers[0])
            self.stdout.write(self.style.SUCCESS("Provider users created"))
            # customer = create_customer(providers[0])
            # self.stdout.write(self.style.SUCCESS('Customer created.'))
            # create_customer_users(providers[0], customer)
            # self.stdout.write(self.style.SUCCESS('Customer users created.'))
            self.stdout.write(
                self.style.SUCCESS("Dummy Data inserted successfully.")
            )
