from django.core.management.base import BaseCommand

from accounts.groups import create_superuser_user_groups


class Command(BaseCommand):
    help = "Create User Groups for Super User"

    def handle(self, *args, **options):
        create_superuser_user_groups()
        self.stdout.write(
            self.style.SUCCESS("Successfully created Super User Groups")
        )
