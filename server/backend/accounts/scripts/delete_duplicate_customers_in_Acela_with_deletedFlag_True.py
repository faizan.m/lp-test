from accounts.models import Customer, Provider
from utils import get_app_logger

logger = get_app_logger(__name__)


def delete_duplicate_customers_in_acela_with_deletedFlag_true(provider_id):
    provider = Provider.objects.get(id=provider_id)
    # Fetch updates for customer.
    customers = provider.erp_client.get_customers(full_info=True, filters=[])
    customers_in_db = Customer.objects.filter(
        provider_id=provider_id
    ).values_list("crm_id", flat=True)
    customers_to_be_created = list(
        filter(
            lambda customer: customer.get("id") not in customers_in_db,
            customers,
        )
    )
    customer_name_to_be_created = []
    for cus in customers_to_be_created:
        customer_name_to_be_created.append(cus.get("name"))

    customer_crm_ids_to_delete = []
    for payload in customers:
        # delete_customer(provider_id, customer, customers_to_be_created)
        # Update Customer
        customer = Customer.objects.filter(
            provider_id=provider_id, name=payload.get("name")
        )
        if customer:
            if customer[0].name in customer_name_to_be_created:
                customer_crm_ids_to_delete.append(customer[0].crm_id)
                logger.info(
                    f"Deleting Customer {customer[0].id} with name {customer[0].name}"
                )
                # customer.delete()
    logger.info(f"Deleted Customer {customer_crm_ids_to_delete}")
