import json
import requests
import tempfile
from requests.exceptions import HTTPError
import os
from datetime import datetime
from typing import Dict, List, Set, Union

# Enter your email here in quotes
EMAIL = ""
# Enter your password here in quotes
PASSWORD = ""

# Request methods
VALID_REQUEST_METHODS = {"GET", "PUT", "POST", "PATCH", "DELETE"}

BASE_URL = f"https://dev.acela.io/api/v1/"
ACELA_LOGIN_URL = BASE_URL + f"login"
REFRESH_TOKEN_URL = BASE_URL + f"token-refresh"
REQUEST_AUTH_URL = BASE_URL + f"request-authentication"
VALIDATE_AUTH_URL = BASE_URL + f"validate-authentication"
# Try changing CLIENT_URL if the script is not working.
# Get it using Developer Console -> Network -> API Call -> Headers
CLIENT_URL = f"https://lp.acela.io/dashboard"


def email_and_password_configured() -> bool:
    """
    Method to check if user email and password are added or not
    """
    if not EMAIL:
        print("Please add your email")
        return False
    if not PASSWORD:
        print("Please add you password")
        return False
    print("Email and password are added.")
    return True


def generate_timestamped_file_name(initial_file_name: str) -> str:
    """
    Method to add timestamp to file name.
    This handles the case of duplicate file name issues.

    Args:
        initial_file_name: Original file name

    Returns:
        File name with timestamp
    """
    file_name = initial_file_name.rsplit(".", 1)
    timestamp = str(int(datetime.timestamp(datetime.now())))
    return f"{file_name[0]}-{timestamp}.{file_name[1]}"


def get_headers_for_acela_request() -> Dict:
    """
    Method to get headers for ACELA API requests.

    Returns:
        Headers dictionary
    """
    headers = {"Content-Type": "application/json", "Client-Url": CLIENT_URL}
    return headers


def two_FA_enabled(response: Dict) -> bool:
    """
    Checks if 2 FA is enabled or not.

    Args:
        response: Response from Acela after login attempt.

    Returns:
        True if 2 FA enabled else False
    """
    response_body: Set[str] = set(response.keys())
    error_message_in_response: bool = (
        True if "error_message" in response_body else False
    )
    token_in_response: bool = True if "token" in response_body else False
    is_two_fa_enabled: bool = response.get("is_two_fa_enabled", False)
    if error_message_in_response and token_in_response and is_two_fa_enabled:
        return True
    else:
        return False


def login() -> Union[str, Dict, None]:
    """
    Login to Acela and get tokens for further requests.

    Returns:
        Access and refresh tokens
    """
    payload = json.dumps(dict(email=EMAIL, password=PASSWORD))
    headers = get_headers_for_acela_request()
    print("Trying to login to Acela using given email and password...")
    try:
        response = requests.request(
            "POST", url=ACELA_LOGIN_URL, data=payload, headers=headers
        )
    except (HTTPError, Exception) as err:
        print(f"An error occurred while logging in to Acela: {err}")
        return
    response_data: Dict = response.json()
    if response.status_code == 403:
        print("Error logging in to Acela!")
        print(response_data)
        return
    elif response.status_code == 200:
        print(
            "Logged in to Acela using email and password. Received access and refresh tokens."
        )
        return dict(
            access_token=response_data.get("token"),
            refresh_token=response_data.get("refresh"),
        )
    elif response.status_code == 412 and two_FA_enabled(response_data):
        print("2FA is enabled. Attempting to login using 2FA.")
        token_for_auth_request: str = response_data.get("token")
        print("Received auth token for 2FA.")
        return token_for_auth_request


def request_authentication(multi_fa_token: str) -> str:
    """
    Requests 2 FA using Authy app.

    Args:
        multi_fa_token: 2 FA token

    Returns:
        UUID of authenticated user.
    """
    payload = json.dumps({"code": "", "type": "AUTHY_NOTIFY"})
    headers: Dict = {
        "multi-fa-token": f"Auth {multi_fa_token}",
        "Content-Type": "application/json",
        "Client-Url": CLIENT_URL,
    }
    print("Requesting 2FA. Please approve 2FA from Authy app on your phone.")
    try:
        response = requests.request(
            "POST", url=REQUEST_AUTH_URL, data=payload, headers=headers
        )
    except (HTTPError, Exception) as err:
        print(f"Error while requesting 2FA: {err}")
        return
    response_data: Dict = response.json()
    uuid: str = response_data.get("approval_request").get("uuid")
    print("2FA authentication request successful!")
    print("Received user ID from Acela.")
    return uuid


def validate_authentication(multi_fa_token: str, uuid: str) -> Dict:
    """
    Validates 2FA request.

    Args:
        multi_fa_token: 2 FA token
        uuid: UUID of authenticated user.

    Returns:
        Access tokens.
    """
    payload = json.dumps({"code": uuid, "type": "AUTHY_NOTIFY"})
    headers: Dict = {
        "multi-fa-token": f"Auth {multi_fa_token}",
        "Content-Type": "application/json",
        "Client-Url": CLIENT_URL,
    }
    print("Validating 2FA request with Acela.")
    try:
        response = requests.request(
            "POST", url=VALIDATE_AUTH_URL, data=payload, headers=headers
        )
    except (HTTPError, Exception) as err:
        print(f"Error validating 2FA: {err}")
        return
    response_data: Dict = response.json()
    print("Validated 2FA with Acela. Received access and refresh tokens.")
    tokens: Dict = dict(
        access_token=response_data.get("token"),
        refresh_token=response_data.get("refresh"),
    )
    return tokens


def login_to_acela() -> Union[Dict, None]:
    """
    Login to Acela using the email and password.

    Returns:
        Tokens
    """
    token: Union[str, Dict] = login()
    # Two FA is disabled, token contains access and refresh tokens
    if isinstance(token, dict):
        return token
    # Two FA is enabled,perform 2 FA authentication using Authy app and get access and refresh tokens
    elif isinstance(token, str):
        uuid: str = request_authentication(token)
        if uuid:
            tokens: Dict = validate_authentication(token, uuid)
            return tokens
        return


def refresh_token(refresh_token: str) -> str:
    """
    Method to refresh access token

    Args:
        refresh_token: Refresh token

    Returns:
        New access token
    """
    headers = get_headers_for_acela_request()
    payload = json.dumps({"token": refresh_token})
    print("Trying to refresh acces token with Acela...")
    response = requests.post(
        url=REFRESH_TOKEN_URL, data=payload, headers=headers
    )
    # If we can't refresh token, then login again
    if response.status_code != 200:
        print("Unable to refresh token. Logging in to Acela again...")
        login_tokens = login_to_acela()
        access_token: str = login_tokens.get("access_token")
        print("Logged in to Acela. Received new access token.")
        return access_token
    token = response.json().get("token")
    print("Refresh successful! Received new access token.")
    return token


def download_files(
    download_url: str,
    login_tokens: Dict,
    request_payload: Dict,
    request_method: str,
):
    """
    Method to download files from ACELA

    Args:
        download_url: ACELA URL to download file
        login_tokens: Dictionary containing access and refresh tokens
        request_payload: Payload for the request
        request_method: Request method
    """
    if request_method not in VALID_REQUEST_METHODS:
        print("Pass valid request_method to download_files()")

    _access_token = login_tokens.get("access_token")
    _refresh_token = login_tokens.get("refresh_token")

    payload = json.dumps(request_payload)
    headers = get_headers_for_acela_request()
    headers.update({"Authorization": f"Bearer {_access_token}"})
    # Send request to Acela
    print("Fetching required file details from Acela...")
    try:
        response = requests.request(
            request_method, url=download_url, data=payload, headers=headers
        )
    except (HTTPError, Exception) as exc:
        print(f"Error getting file details from Acela: {exc}")
        return
    # Check if the token has expired
    if response.status_code != 200:
        _new_tokens = refresh_token(_refresh_token)
        headers.update({"Authorization": f"Bearer {_new_tokens}"})
        response = requests.request(
            request_method, url=download_url, data=payload, headers=headers
        )

    response_data: Dict = response.json()
    print("Received file details from Acela.")
    _file_url = response_data.get("file_url", None)
    _file_path = response_data.get("file_path", None)
    file_link = _file_url if _file_url else _file_path

    _file_name = response_data.get("file_name", None)
    file_name = _file_name if _file_name else "ACELA Report"

    # Download the file content
    print("Downloading file...")
    try:
        file_content = requests.request(url=file_link, method="GET")
    except (HTTPError, Exception) as err:
        print(f"Error while downloading file: {err}")
        return
    file_name = generate_timestamped_file_name(file_name)
    # Write file in home directory
    home_directory_path = os.path.expanduser("~")
    file_path = os.path.join(home_directory_path, file_name)
    try:
        with open(file_path, "wb") as output_file:
            output_file.write(file_content.content)
    except (OSError, Exception) as err:
        print(f"Error while writing file to system: {err}")
        return
    print(f"Downloaded file location is: {file_path}")


def main():
    if email_and_password_configured():
        acela_url = None
        # Payload could be None for some reports.
        payload = None
        request_method = None

        # Define acela_url, payload and request_method.
        # Refer to following example.
        # acela_url = "https://dev.acela.io/api/v1/providers/customer-device-management-report"
        # payload = {
        #     "customer_id": [1],
        #     "report_type": {
        #         "name": "End Of Life Devices",
        #         "category": "LIFECYCLE_MANAGEMENT_REPORT",
        #         "display_name": "Lifecycle management report",
        #         "type": "REPORT01",
        #     },
        # }
        # request_method = "POST"

        if (not acela_url) or (not request_method):
            print("Please define acela_url, payload and request method!")
            return
        login_tokens: Union[Dict, None] = login_to_acela()
        if login_tokens:
            download_files(acela_url, login_tokens, payload, request_method)


if __name__ == "__main__":
    main()
