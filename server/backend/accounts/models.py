import copy
import ipaddress
import os

import pytz
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models import Q, UniqueConstraint, JSONField
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices

from core.exceptions import (
    ERPAuthenticationNotConfigured,
    InvalidConfigurations,
)
from custom_storages import get_storage_class
from exceptions.exceptions import ServiceUnavailableError, AuthenticationError
from .managers import UserManager
from .model_utils import TimeStampedUUIDModel, TimeStampedModel


logger = get_task_logger(__name__)


def integration_statuses_default():
    INTEGRATION_STATUSES_DEFAULT = dict(
        crm_authentication_configured=False,
        crm_board_mapping_configured=False,
        crm_device_categories_configured=False,
        device_manufacturer_api_configured=False,
    )
    return INTEGRATION_STATUSES_DEFAULT


def customer_distribution_list():
    CUSTOMER_DISTRIBUTION_LIST = dict(
        scheduled_reports=[],
        monitoring_alerts=[],
        staffing_changes=[],
        alerts_and_notifications=[],
    )
    return CUSTOMER_DISTRIBUTION_LIST


class SuperuserUserManager(UserManager):
    def get_queryset(self):
        return (
            super().get_queryset().filter(user_type__name=UserType.ACCELAVAR)
        )


class ProviderUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type__name=UserType.PROVIDER)


class CustomerUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type__name=UserType.CUSTOMER)


class ProviderAndCustomerManager(UserManager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(user_type__name__in=[UserType.CUSTOMER, UserType.PROVIDER])
        )


# Custom User Model
class User(AbstractBaseUser, PermissionsMixin, TimeStampedUUIDModel):
    email = models.EmailField()
    first_name = models.TextField(null=True, blank=True)
    last_name = models.TextField(null=True, blank=True)
    phone_number = models.TextField(null=True, blank=True)
    user_type = models.ForeignKey(
        "accounts.UserType", related_name="members", on_delete=models.CASCADE
    )
    user_role = models.ForeignKey(
        "accounts.Role",
        related_name="members",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="users",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="users",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    is_active = models.BooleanField(default=False)
    activation_mails_sent_count = models.PositiveIntegerField(default=0)
    crm_id = models.PositiveIntegerField(null=True, blank=True)
    is_active_in_crm = models.BooleanField(null=True)
    is_deleted = models.BooleanField(default=False)
    previous_login = models.DateTimeField(default=None, null=True, blank=True)

    default_manager = models.Manager()
    objects = UserManager()  # default object manager
    superusers = SuperuserUserManager()  # for superuser user specific queries
    providers = ProviderUserManager()  # for provider user specific queries
    customers = CustomerUserManager()  # for customer user specific queries
    providers_and_customers = (
        ProviderAndCustomerManager()
    )  # for provider or customer user query
    external_auth_id = models.CharField(max_length=50, null=True)
    project_rate_role = models.ForeignKey(
        "project_management.ProjectRateMappingSetting",
        related_name="+",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    USERNAME_FIELD = "id"
    REQUIRED_FIELDS = ["first_name"]

    class Meta:
        permissions = (
            ("add_superuser_user", "can add super users"),
            ("add_provider_user", "can add provider users"),
            ("add_customer_user", "can add customer users"),
            # ("view_user", "can view user"),
        )
        ordering = ("-updated_on",)
        verbose_name = _("user")
        verbose_name_plural = _("users")
        constraints = [
            UniqueConstraint(
                fields=["email", "provider"],
                name="email_and_provider_index",
                condition=Q(provider__isnull=False),
            ),
            UniqueConstraint(
                fields=["email"],
                name="email_without_provider_index",
                condition=Q(provider__isnull=True),
            ),
        ]
        # indexes = (
        #         fields=["email", "provider"])
        # indexes = [
        #     PartialIndex(
        #         fields=["email", "provider"],
        #         unique=True,
        #         where=PQ(provider__isnull=False),
        #     ),
        #     PartialIndex(
        #         fields=["email"], unique=True, where=PQ(provider__isnull=True)
        #     ),
        # ]

    def __str__(self):
        return "{full_name}".format(full_name=self.get_full_name())

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def save(self, *args, **kwargs):
        self.email = self.email.strip().lower()
        created = self._state.adding
        super(User, self).save(*args, **kwargs)
        if created:
            self.refresh_from_db()
            user = self
            from .groups import (
                assign_group_to_user,
                assign_user_model_obj_permissions,
            )

            if user.role:
                # call function to assign group to the new user
                assign_group_to_user(user)
            # call function to assign object permissions
            assign_user_model_obj_permissions(user)

    @cached_property
    def name(self):
        return self.get_full_name()

    @cached_property
    def type(self):
        return self.user_type.name

    @cached_property
    def role(self):
        return self.user_role.role_name if self.user_role else None

    @cached_property
    def role_display_name(self):
        return self.user_role.display_name if self.user_role else None

    @cached_property
    def is_password_set(self):
        return self.has_usable_password()

    def get_domain(self):
        if self.type in [UserType.PROVIDER, UserType.CUSTOMER]:
            return self.provider.get_domain()
        else:
            return settings.ADMIN_PORTAL_URL

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


def profile_pic_upload_url(instance, filename):
    filename, file_extension = filename.split(".")
    image_path = (
        f"users/profile-pics/{instance.user_id}-{filename}.{file_extension}"
    )
    return image_path


# User Profile Model
class Profile(TimeStampedModel):
    user = models.OneToOneField(
        "accounts.User", related_name="profile", on_delete=models.CASCADE
    )
    department = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    office_phone = models.TextField(blank=True, null=True)
    office_phone_crm_id = models.TextField(blank=True, null=True)
    cell_phone_number = models.TextField(blank=True, null=True)
    cell_phone_number_crm_id = models.PositiveIntegerField(
        blank=True, null=True
    )
    country_code = models.IntegerField(null=True, blank=True)
    office_phone_country_code = models.IntegerField(null=True, blank=True)
    twitter_profile_url = models.TextField(blank=True, null=True)
    linkedin_profile_url = models.TextField(blank=True, null=True)
    profile_pic = models.ImageField(
        upload_to=profile_pic_upload_url,
        null=True,
        blank=True,
        storage=get_storage_class(),
        validators=[FileExtensionValidator(["png", "jpeg", "jpg"])],
    )
    customer_user_instance_id = models.PositiveIntegerField(
        null=True, blank=True
    )
    system_member_crm_id = models.PositiveIntegerField(null=True, blank=True)
    cco_id = models.CharField(max_length=100, blank=True, null=True)
    email_signature = JSONField(default=dict)
    cco_id_acknowledged = models.BooleanField(default=False)
    default_landing_page = models.CharField(
        max_length=500, blank=True, default=""
    )
    feature = JSONField(default=list)

    # class Meta:
    #     permissions = (("view_profile", "can view user profile"),)

    @property
    def full_cell_phone_number(self):
        country_code = (
            self.office_phone_country_code or settings.DEFAULT_COUNTRY_CODE
        )
        if self.office_phone:
            return f"+{country_code}{self.office_phone}"
        else:
            return None

    @property
    def full_office_phone_number(self):
        country_code = self.country_code or settings.DEFAULT_COUNTRY_CODE
        if self.cell_phone_number:
            return f"+{country_code}{self.cell_phone_number}"
        else:
            return None

    @property
    def profile_pic_url(self):
        if self.profile_pic and hasattr(self.profile_pic, "url"):
            return self.profile_pic.url


def logo_upload_url(instance, filename):
    from core.utils import generate_random_id

    file_extension = filename.split(".")[-1]
    file_name = generate_random_id(15)
    image_path = f"providers/logos/{file_name}_logo.{file_extension}"
    return image_path


def file_upload_url(instance, filename):
    file_path = f"providers/customers/{instance.customer_id}/files/{filename}"
    return file_path


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        """Returns a filename that's free on the target storage system, and
        available for new content to be written to.
        """
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


# Provider Model
class Provider(TimeStampedModel):
    HTTP = "http://"
    HTTPS = "https://"
    PROTOCOL_CHOICES = Choices((HTTP, "http://"), (HTTPS, "https://"))
    erp_objects = dict()
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=3000, unique=True)
    url_alias = models.CharField(
        max_length=500, null=True, blank=False, unique=True
    )
    company_url = models.CharField(max_length=500, null=True, blank=False)
    enable_url = models.BooleanField(default=False)
    ticket_note = models.CharField(max_length=500, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    logo = models.ImageField(
        upload_to=logo_upload_url,
        null=True,
        blank=True,
        storage=get_storage_class(),
        validators=[FileExtensionValidator(["png", "jpeg", "jpg"])],
    )
    colour_code = models.CharField(max_length=10, null=True, blank=True)
    support_contact = models.EmailField(blank=True, null=True)
    http_protocol = models.CharField(max_length=10, choices=PROTOCOL_CHOICES)
    address = models.OneToOneField(
        "accounts.Address",
        related_name="address",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    accounting_contact = models.OneToOneField(
        "accounts.AccountingContact",
        related_name="accounting_contact",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    crm_integration_configured = models.BooleanField(default=False)
    monitoring_integration_configured = models.BooleanField(default=False)
    integration_statuses = JSONField(
        null=True, blank=True, default=integration_statuses_default
    )
    customer_instance_id = models.PositiveIntegerField(blank=True, null=True)
    is_two_fa_enabled = models.BooleanField(default=False)
    timezone = models.CharField(
        max_length=50, choices=TIMEZONES, default="America/Los_Angeles"
    )

    class Meta:
        # permissions = (("view_provider", "can view provider"),)
        ordering = ("-id",)

    @cached_property
    def sub_domain(self):
        return self.url.split(".")[0]

    @cached_property
    def is_configured(self):
        return self.integration_statuses.get(
            "crm_authentication_configured"
        ) and self.integration_statuses.get(
            "device_manufacturer_api_configured"
        )

    @cached_property
    def erp_integration(self):
        if self.integration_statuses["crm_authentication_configured"]:
            integration = self.integrations.get(
                integration_type__category__name="CRM"
            )
            return integration
        else:
            raise ERPAuthenticationNotConfigured()

    @cached_property
    def erp_client(self):
        """
        Get ERP client instance for the Provider
        :return:
        """

        if self.erp_objects.get(self.id):
            client = self.erp_objects[self.id]
        else:
            integration = self.erp_integration
            erp_class = integration.integration_type.client_class
            client = erp_class(integration.authentication_info)
            self.erp_objects[self.id] = client
        try:
            client.client.get_info()
        except AuthenticationError as e:
            logger.error(
                "Status Code: {0} Message: {1}".format(e.status_code, e.detail)
            )
            raise ServiceUnavailableError(message=e.detail)
        return client

    @cached_property
    def device_category_id(self):
        """
        TODO: Remove this after multi manufacturer support
        :return:
        """
        configs = self.erp_integration.other_config
        if not configs or not configs.get("device_categories"):
            return None
        return configs["device_categories"][0]

    def get_manufacturer_mapping(self):
        """
        Returns manufacturer mapping dict from erp integration other config for the provider
        :return: Dict
        Example:
        {
            "5": {
              "cw_mnf_name": "Cisco",
              "manufacturer_id": 5,
              "device_category_id": 33,
              "device_category_name": "Contract - Cisco SMARTnet",
              "acela_integration_type_id": 2
            },
            "31": {
              "cw_mnf_name": "Citrix",
              "manufacturer_id": 31,
              "device_category_id": 11,
              "device_category_name": "License",
              "acela_integration_type_id": 2
            },
            "default": {
              "device_category_id": 33
            }
        }
        """
        manufacturer_mapping = self.erp_integration.other_config.get(
            "manufacturer_mapping"
        )
        return manufacturer_mapping

    def get_cisco_manufacturer_ids(self):
        """
        Returns Manufacturer Id's which are mapped to CISCO Acela Integration Type
        Returns: List of ids

        """
        manufacturer_mapping = self.get_manufacturer_mapping()
        cisco_manufacturer_ids = []
        if not manufacturer_mapping:
            return cisco_manufacturer_ids
        from core.models import IntegrationType

        try:
            cisco_integration_type = IntegrationType.objects.get(name="CISCO")
        except IntegrationType.DoesNotExist:
            return cisco_manufacturer_ids
        for manufacturer_id, meta in manufacturer_mapping.items():
            if (
                meta.get("acela_integration_type_id")
                and int(meta.get("acela_integration_type_id"))
                == cisco_integration_type.id
            ):
                cisco_manufacturer_ids.append(int(manufacturer_id))
        return cisco_manufacturer_ids

    def get_cisco_category_ids(self):
        """
        Returns Category Id's for manufacturers which are mapped to CISCO Acela Integration Type
        Returns:

        """
        manufacturer_mapping = self.get_manufacturer_mapping()
        cisco_category_ids = []
        if not manufacturer_mapping:
            return cisco_category_ids
        from core.models import IntegrationType

        try:
            cisco_integration_type = IntegrationType.objects.get(name="CISCO")
        except IntegrationType.DoesNotExist:
            return cisco_category_ids
        for manufacturer_id, meta in manufacturer_mapping.items():
            if (
                meta.get("acela_integration_type_id")
                and int(meta.get("acela_integration_type_id"))
                == cisco_integration_type.id
            ):
                cisco_category_ids.append(int(meta.get("device_category_id")))
        return cisco_category_ids

    def get_manufacturer_map_by_id(self, manufacturer_id):
        """
        Returns manufacturer mapping for a particular manufacturer by id
        If no mapping is found with manufacturer_id then default mapping is returned
        :param manufacturer_id: ERP manufacturer ID
        :return: Dict
        Example:
        {
              "cw_mnf_name": "Cisco",
              "manufacturer_id": 5,
              "device_category_id": 33,
              "device_category_name": "Contract - Cisco SMARTnet",
              "acela_integration_type_id": 2
        }
        """
        manufacturer_id = str(manufacturer_id)
        manufacturer_mapping = self.erp_integration.other_config.get(
            "manufacturer_mapping"
        )
        manufacturer_map = manufacturer_mapping.get(
            manufacturer_id, manufacturer_mapping.get("default")
        )
        return manufacturer_map

    def get_device_category_id_by_manufacturer(self, manufacturer_id):
        """
        Returns device category id from the manufacturer mapping
        :param manufacturer_id: ERP manufacturer ID
        :return: Int
        """
        manufacturer_id = str(manufacturer_id)
        manufacturer_map = self.get_manufacturer_map_by_id(manufacturer_id)
        return manufacturer_map.get("device_category_id")

    def get_all_device_categories(self):
        """
        Returns list of all the category ids for the all the manufacturer mappings
        :return: List of Int
        Example: [33, 11.....]
        """
        manufacturer_mapping = self.erp_integration.other_config.get(
            "manufacturer_mapping"
        )
        return [
            mapping.get("device_category_id")
            for _, mapping in manufacturer_mapping.items()
        ]

    def get_subscription_mapping(self):
        """
        Returns subscription mapping dict from erp integration other config for the provider
        :return: Dict
        """
        subscription_mapping = self.erp_integration.other_config.get(
            "subscription_mapping"
        )
        if subscription_mapping:
            return subscription_mapping
        else:
            raise InvalidConfigurations("Subscription Mapping Not Configured.")

    def get_subscription_map_by_id(self, manufacturer_id):
        """
        Returns subscription mapping for a particular manufacturer by id
        If no mapping is found with manufacturer_id then default mapping is returned
        :param manufacturer_id: ERP manufacturer ID
        :return: Dict
        """
        manufacturer_id = str(manufacturer_id)
        subscription_mapping = self.get_subscription_mapping()
        subscription_map = subscription_mapping.get(
            manufacturer_id, subscription_mapping.get("default")
        )
        return subscription_map

    def get_subscription_category_id_by_manufacturer(self, manufacturer_id):
        """
        Returns subscription category id from the subscription mapping
        :param manufacturer_id: ERP manufacturer ID
        :return: Int
        """
        manufacturer_id = str(manufacturer_id)
        subscription_map = self.get_subscription_map_by_id(manufacturer_id)
        return subscription_map.get("subscription_category_id")

    def get_all_subscription_categories(self):
        """
        Returns list of all the subscription category ids for the all the subscription mappings
        :return: List of Int
        Example: [33, 11.....]
        """
        subscription_mapping = self.get_subscription_mapping()
        return [
            mapping.get("subscription_category_id")
            for _, mapping in subscription_mapping.items()
        ]

    def get_domain(self):
        """
        Get domain name for the Provider
        :return: Str
        """
        if self.url_alias:
            return self.url_alias
        return self.url

    def get_device_ui_filters(self):
        device_ui_filters = self.erp_integration.other_config.get(
            "device_filters"
        )
        return device_ui_filters

    def save(self, *args, **kwargs):
        created = self._state.adding
        super(Provider, self).save(*args, **kwargs)
        self.refresh_from_db()
        if created:
            from accounts.groups import (
                assign_provider_model_obj_permissions,
                create_provider_user_groups,
            )
            from document.utils import create_default_service_category
            from core.services import SubscriptionService
            from project_management.services import ProjectQueryService

            create_provider_user_groups(self.id)
            assign_provider_model_obj_permissions(self)
            create_default_service_category(self)
            SubscriptionService.create_subscription_product_mapping(self)
            ProjectQueryService(self).create_milestone_done_status()
            ProjectQueryService(self).create_activity_status_done_status()

    def refresh_from_db(self, *args, **kwargs):
        super().refresh_from_db(*args, **kwargs)
        cached_properties = ["erp_client"]
        if self.erp_objects.get(self.id):
            self.erp_objects.pop(self.id)

        for _property in cached_properties:
            try:
                del self.__dict__[_property]
            except KeyError as e:
                pass

    def __str__(self):
        return "{0}".format(self.name)


# Customer Model
class Customer(TimeStampedModel):
    crm_id = models.PositiveIntegerField(blank=True, null=True)
    models.PositiveIntegerField(blank=True, null=True)
    name = models.TextField()
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    address = models.OneToOneField(
        "accounts.Address", null=True, on_delete=models.SET_NULL
    )
    phone = models.TextField(null=True, blank=True)
    primary_contact = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    renewal_service_allowed = models.BooleanField(default=False)
    network_note = models.TextField(blank=True)
    customer_distribution = JSONField(
        null=True, blank=True, default=customer_distribution_list
    )
    is_ms_customer = models.BooleanField(default=False)
    collector_service_allowed = models.BooleanField(default=False)
    config_compliance_service_allowed = models.BooleanField(default=False)
    config_compliance_service_write_allowed = models.BooleanField(
        default=False
    )
    account_manager_id = models.PositiveIntegerField(blank=True, null=True)
    account_manager_name = models.CharField(
        max_length=300, blank=True, null=True
    )
    customer_order_visibility_allowed = models.BooleanField(default=False)

    class Meta:
        unique_together = (("name", "provider"), ("provider", "crm_id"))
        # permissions = (("view_customer", "can view customer"),)

    @cached_property
    def users_count(self):
        """
        Count of all  users for customer
        :return: Int
        """
        try:
            count = self.users.filter(is_active_in_crm=True).count
        except Exception as e:
            count = None
        return count

    @cached_property
    def devices_count(self):
        """
        Count of all devices for customer based on the mapping done by Provider
        :return: Int
        """
        try:
            count = self.provider.erp_client.get_devices_count(
                self.crm_id, self.provider.get_all_device_categories()
            )
        except Exception as e:
            count = None
        return count

    def get_territory_manager_details(self, erp_client):
        cw_customer = erp_client.get_customer(self.crm_id)
        if not cw_customer.get("territory_manager_id"):
            return {}
        territory_manager_details = erp_client.get_member(
            cw_customer.get("territory_manager_id")
        )
        return territory_manager_details

    def get_territory_manager_email(self, erp_client):
        territory_manager_details = self.get_territory_manager_details(
            erp_client
        )
        return territory_manager_details.get("email")

    def save(self, *args, **kwargs):
        created = self._state.adding
        super(Customer, self).save(*args, **kwargs)
        customer = Customer.objects.get(name=self.name, provider=self.provider)
        if created:
            from accounts.groups import create_customer_user_groups

            create_customer_user_groups(customer.provider.id, customer.id)
            from accounts.groups import assign_customer_model_obj_permissions

            assign_customer_model_obj_permissions(customer)

    def __str__(self):
        return "{0}".format(self.name)


# Address Model
class Address(TimeStampedModel):
    address_1 = models.TextField(null=True, blank=True)
    address_2 = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zip_code = models.TextField(null=True, blank=True)

    # class Meta:
    #     permissions = (("view_address", "can view address"),)

    def __str__(self):
        return "{0}-{1}-{2}".format(self.address_1, self.city, self.state)


# Provider Accounting Contact Model
class AccountingContact(TimeStampedModel):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()
    phone = models.CharField(max_length=20, null=True, blank=True)
    website = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        permissions = (
            ("view_accounting_contact", "can view accounting contact"),
        )

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


# User Types model
class UserType(TimeStampedModel):
    ACCELAVAR = "accelavar"
    PROVIDER = "provider"
    CUSTOMER = "customer"
    name = models.CharField(max_length=30)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.name


# User Roles model
class Role(TimeStampedModel):
    OWNER = "owner"
    ADMIN = "admin"
    STAFF = "staff"
    role_name = models.CharField(max_length=30)
    user_type = models.ForeignKey(
        "accounts.UserType", related_name="roles", on_delete=models.CASCADE
    )
    display_name = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return "{0}-{1}".format(self.role_name, self.user_type.name)


class ModelSyncMeta(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    last_update_datetime = models.DateTimeField(auto_now=True)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)


class CustomerEscalation(TimeStampedModel):
    PRIMARY = "PRIMARY"
    SECONDARY = "SECONDARY"
    TERTIARY = "TERTIARY"
    DURING_BUSINESS_HRS = "During business hours"
    OUTSIDE_OF_BUSINESS_HRS = "Outside of business hours"
    ESCALATION_PRIORITY_CHOICES = Choices(
        (PRIMARY, PRIMARY), (SECONDARY, SECONDARY), (TERTIARY, TERTIARY)
    )
    ESCALATION_TYPE_CHOICES = Choices(
        (DURING_BUSINESS_HRS, DURING_BUSINESS_HRS),
        (OUTSIDE_OF_BUSINESS_HRS, OUTSIDE_OF_BUSINESS_HRS),
    )
    escalation_priority = models.CharField(
        choices=ESCALATION_PRIORITY_CHOICES, max_length=30
    )
    escalation_type = models.CharField(
        choices=ESCALATION_TYPE_CHOICES, max_length=30
    )
    user = models.ForeignKey("accounts.User", on_delete=models.CASCADE)
    authorize_charges = models.BooleanField(null=True)
    authorize_changes = models.BooleanField(null=True)
    cw_site_id = models.IntegerField(null=True, default=None)


def validate_ip(ip_address):
    if ip_address is not None:
        ip_address = str(ip_address)
        if "/" in ip_address:
            try:
                ipaddress.ip_network(ip_address)
            except ValueError:
                raise ValidationError(
                    f"{ip_address} does not appear to be an IPv4 or IPv6 network",
                    params={"ip_address": ip_address},
                )
        else:
            try:
                ipaddress.ip_address(ip_address)
            except ValueError:
                raise ValidationError(
                    f"{ip_address} does not appear to be an IPv4 or IPv6 address",
                    params={"ip_address": ip_address},
                )
    return ip_address


def validate_bandwith(bandwidth):
    if bandwidth is not None:
        bandwidth = str(bandwidth)
        band_split = bandwidth.split(" ")
        if len(band_split) == 2 and str(band_split[1]).upper() in ["MB", "GB"]:
            try:
                band_split[0] = int(float(band_split[0]))
                bandwidth = f"{band_split[0]} {band_split[1].upper()}"
            except:
                raise ValidationError(
                    f"{band_split[0]} does not appear to be a valid integer",
                    params={"bandwidth": bandwidth},
                )
        else:
            raise ValidationError(
                f"{bandwidth} does not appear to be a valid format. Example: 10 MB or 10 GB",
                params={"bandwidth": bandwidth},
            )

    return bandwidth


class CircuitInfo(TimeStampedModel):
    customer = models.ForeignKey("accounts.Customer", on_delete=models.CASCADE)
    cw_site_id = models.IntegerField()
    circuit_type = models.ForeignKey(
        "accounts.CircuitType",
        on_delete=models.CASCADE,
        related_name="circuit_infos",
    )
    provider = models.CharField(max_length=100)
    circuit_id = models.CharField(max_length=100)
    contact_phone = models.CharField(max_length=17, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    loa = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    device_crm_id = models.PositiveIntegerField(null=True, blank=True)
    attachment = models.FileField(
        upload_to=file_upload_url,
        null=True,
        blank=True,
        storage=get_storage_class(),
    )
    contract_end_date = models.DateTimeField(null=True, blank=True)
    ip_address = models.CharField(
        max_length=100, null=True, blank=True, validators=[validate_ip]
    )
    bandwidth = models.CharField(
        max_length=100, null=True, blank=True, validators=[validate_bandwith]
    )
    account_number = models.CharField(max_length=17, null=True, blank=True)

    class Meta:
        unique_together = ("customer", "circuit_type", "circuit_id")


class CircuitType(TimeStampedModel):
    name = models.CharField(max_length=100, unique=True)
    provider = models.ForeignKey(
        "accounts.Provider",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="circuit_types",
    )

    class Meta:
        ordering = ("-updated_on",)


class HistoricalCircuitInfo(TimeStampedModel):
    old_instance = JSONField(default=dict)
    circuit_info_id = models.IntegerField()
    change_type = models.CharField(max_length=30)
    note = models.TextField()


class StoredEmailData(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    email_data = JSONField()
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )


class FeatureStatus(models.Model):
    FINANCE_REPORT = "FinanceReport"
    FIRE_REPORT = "Fire Report"
    MERGE_CUSTOMER = "Merge Customer"
    FEATURES_LIST = [FINANCE_REPORT, FIRE_REPORT, MERGE_CUSTOMER]

    user = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    feature = models.CharField(max_length=255)
    enabled = models.BooleanField(default=False)

    class Meta:
        unique_together = ("user", "feature")
