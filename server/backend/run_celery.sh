#!/bin/sh

# wait for RabbitMQ server to start
sleep 10

celery worker -A backend --app=celery_app:app -l info --logfile="/var/log/django/celery.log" -c 8 -Q high
