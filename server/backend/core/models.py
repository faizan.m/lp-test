"""
Models file
"""
from datetime import datetime
from datetime import timedelta
import json
import os
from typing import List, Tuple, Optional, Dict
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db.models import JSONField
from django.db import models
from django.utils.functional import cached_property
from model_utils import Choices

from accounts.model_utils import TimeStampedModel
from core import integrations
from custom_storages import get_storage_class
from utils import get_app_logger
from document.exceptions import SOWQuoteStatusSettingsNotConfigured

logger = get_app_logger(__name__)


class IntegrationCategory(TimeStampedModel):
    """
    Model for service category
    """

    name = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.name}"


class IntegrationType(TimeStampedModel):
    """
    Model for Service Types like Connectwise
    """

    CLASS_MAPPING = {
        "CONNECTWISE": integrations.Connectwise,
        "CISCO": integrations.Cisco,
        "LOGIC_MONITOR": integrations.LogicMonitor,
        "DROPBOX": integrations.DropBox,
    }

    name = models.CharField(max_length=50)
    category = models.ForeignKey(
        "core.IntegrationCategory",
        related_name="types",
        on_delete=models.CASCADE,
    )

    @property
    def client_class(self):
        return self.CLASS_MAPPING[self.name]

    @cached_property
    def authentication_config(self):
        configs_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "integration_configs",
            self.name.lower(),
            "authentication_config.json",
        )
        try:
            with open(configs_file) as config:
                json_config = json.load(config)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        return json_config

    @cached_property
    def extra_config(self):
        configs_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "integration_configs",
            self.name.lower(),
            "extra_config.json",
        )
        try:
            with open(configs_file) as config:
                config = json.load(config)
                json_config = {}
                if self.name == "CONNECTWISE":
                    json_config["board_mapping"] = config
                else:
                    json_config = config
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        return json_config

    def __str__(self):
        return f"{self.name}"


class ProviderIntegration(TimeStampedModel):
    """
    Model for different types of Service Integration for a Provider.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="integrations",
        on_delete=models.CASCADE,
    )
    integration_type = models.ForeignKey(
        "core.IntegrationType",
        related_name="providers",
        on_delete=models.CASCADE,
    )
    authentication_info = JSONField(null=True, blank=True)
    other_config = JSONField(null=True, blank=True)

    def __str__(self):
        return f"{self.provider} {self.integration_type}"

    def get_extra_config(self):
        return self.provider.erp_client.get_other_configs(
            self.integration_type.extra_config
        )

    @property
    def type(self):
        return self.integration_type.name

    @property
    def category(self):
        return self.integration_type.category.name

    @property
    def sow_document_quote_settings(self):
        return self.other_config.get("board_mapping", {}).get(
            "SOW Quote Status Mapping", None
        )

    @property
    def open_status_ids(self) -> List[int]:
        open_status_ids = (
            self.other_config.get("board_mapping", {})
            .get("SOW Quote Status Mapping", {})
            .get("open_status", [])
        )
        if not open_status_ids:
            raise SOWQuoteStatusSettingsNotConfigured(
                "SOW Quote Status Settings not configured for the Provider"
            )
        return open_status_ids

    @property
    def won_status_ids(self) -> List[int]:
        won_status_ids = (
            self.other_config.get("board_mapping", {})
            .get("SOW Quote Status Mapping", {})
            .get("won_status", [])
        )
        if not won_status_ids:
            raise SOWQuoteStatusSettingsNotConfigured(
                "SOW Quote Status Settings not configured for the Provider"
            )
        return won_status_ids

    @property
    def lost_status_ids(self) -> List[int]:
        lost_status_ids = (
            self.other_config.get("board_mapping", {})
            .get("SOW Quote Status Mapping", {})
            .get("lost_status", [])
        )
        if not lost_status_ids:
            raise SOWQuoteStatusSettingsNotConfigured(
                "SOW Quote Status Settings not configured for the Provider"
            )
        return lost_status_ids


class ContractStatus(object):
    def __init__(self, id, label):
        self.contract_status_id = id
        self.contract_status = label

    def __str__(self):
        return self.contract_status

    def to_json(self):
        return self.__dict__


class DeviceManufacturerData(TimeStampedModel):
    """
    Model to store device data from manufacturer for e.g. cisco
    """

    ACTIVE = ContractStatus(1, "Active")
    EXPIRING = ContractStatus(2, "Expiring")
    EXPIRED = ContractStatus(3, "Expired")
    NOT_AVAILABLE = ContractStatus(4, "N.A.")
    CONTRACT_STATUSES = [ACTIVE, EXPIRING, EXPIRED, NOT_AVAILABLE]

    device_serial_number = models.CharField(max_length=50)
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="device_info",
        on_delete=models.CASCADE,
    )
    data = JSONField()
    device_crm_id = models.IntegerField(blank=True, null=True)
    device_category_id = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = ["provider", "device_serial_number"]

    def __str__(self):
        return self.device_serial_number

    @classmethod
    def get_device_contract_status(cls, contract_end_date):
        """
        Returns Device Status based on the EOL Date.
        :return: Status of device
        """
        # contract_end_date = self.data.get("expiration_date")
        if contract_end_date:
            contract_end_date = datetime.strptime(
                contract_end_date, "%Y-%m-%dT%H:%M:%SZ"
            ).date()
            current_date = datetime.today().date()
            difference_in_days = (contract_end_date - current_date).days

            if difference_in_days < 0:
                status = DeviceManufacturerData.EXPIRED
            elif 0 <= difference_in_days <= 90:
                status = DeviceManufacturerData.EXPIRING
            else:
                status = DeviceManufacturerData.ACTIVE
        else:
            status = DeviceManufacturerData.NOT_AVAILABLE
        return status.to_json()


class DeviceTypes(models.Model):
    product_id = models.CharField(max_length=100, db_index=True, unique=True)
    type = models.CharField(max_length=100)
    suggested_software_update = models.CharField(
        max_length=300, null=True, blank=True
    )
    data = JSONField()

    def __str__(self):
        return f"{self.product_id}--{self.type}"


class ManualDeviceTypeMapping(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="manual_device_types",
        on_delete=models.CASCADE,
    )
    type = models.CharField(max_length=100)
    model_ids = ArrayField(models.CharField(max_length=100))
    non_cisco = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.type}"

    class Meta:
        unique_together = ["provider", "type"]


class SubscriptionServiceProviderMapping(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="manual_service_provider",
        on_delete=models.CASCADE,
    )
    service_provider_name = models.CharField(max_length=100)
    provider_prefix = ArrayField(models.CharField(max_length=300))

    def __str__(self):
        return f"{self.service_provider_name}"

    class Meta:
        unique_together = ["provider", "service_provider_name"]


class DeviceManufacturerMissingData(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="cisco_missing_data",
        on_delete=models.CASCADE,
    )
    serial_number = models.CharField(max_length=100)
    retry_count = models.IntegerField(default=0)
    is_invalid = models.BooleanField(default=False)
    device_crm_id = models.IntegerField(blank=True, null=True)
    device_category_id = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = ["provider", "serial_number"]

    def __str__(self):
        return f"{self.serial_number}"


# Model to store extra information related to a device
class DeviceInfo(TimeStampedModel):
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    device_crm_id = models.PositiveIntegerField(blank=True, null=True)
    serial_number = models.CharField(max_length=100, blank=True, null=True)
    instance_id = models.CharField(max_length=100, blank=True, null=True)
    # True if instance_id is used as the serial number for this device.
    is_serial_instance_id = models.BooleanField(default=False)
    monitoring_meta_data = JSONField(default=dict)
    monitoring_data = JSONField(null=True, blank=True)
    category_id = models.IntegerField(blank=True, null=True)
    customer_notes = models.CharField(max_length=1000, blank=True, null=True)
    asset_tag = models.CharField(max_length=25, blank=True, null=True)
    config_meta_data = JSONField(default=dict)
    configuration = JSONField(default=dict)

    @cached_property
    def merged_config(self):
        solarwinds_config = self.configuration.get(settings.SOLARWINDS)
        logicmonitor_config = self.configuration.get(settings.LOGICMONITOR)
        if solarwinds_config:
            return solarwinds_config
        else:
            return logicmonitor_config

    @cached_property
    def config_id(self):
        merged_config_metadata = {
            **self.config_meta_data.get(settings.LOGICMONITOR, {}),
            **self.config_meta_data.get(settings.SOLARWINDS, {}),
        }
        return merged_config_metadata.get("config_id")

    class Meta:
        unique_together = ("provider", "device_crm_id")
        index_together = ("provider", "device_crm_id")

    def __str__(self):
        return f"{self.device_crm_id}"


class ScheduledReportTypes(models.Model):
    DEVICES = "DEVICES"
    DEVICE_SUPPORT_30_DAYS = "DEVICE_SUPPORT_30_DAYS"
    DEVICE_SUPPORT_60_DAYS = "DEVICE_SUPPORT_60_DAYS"
    DEVICE_SUPPORT_90_DAYS = "DEVICE_SUPPORT_90_DAYS"
    DEVICE_SUPPORT_365_DAYS = "DEVICE_SUPPORT_365_DAYS"

    CATEGORY_CHOICES = Choices((DEVICES, "DEVICES"))

    DEVICE_REPORT_TYPES = Choices(
        (DEVICE_SUPPORT_30_DAYS, "DEVICE_SUPPORT_30_DAYS"),
        (DEVICE_SUPPORT_60_DAYS, "DEVICE_SUPPORT_60_DAYS"),
        (DEVICE_SUPPORT_90_DAYS, "DEVICE_SUPPORT_90_DAYS"),
        (DEVICE_SUPPORT_365_DAYS, "DEVICE_SUPPORT_365_DAYS"),
    )
    display_name = models.CharField(max_length=100)
    type = models.CharField(max_length=100, unique=True)
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES)

    def __str__(self):
        return f"{self.type}"


class UserScheduledReport(TimeStampedModel):
    DAILY = "DAILY"
    WEEKLY = "WEEKLY"
    MONTHLY = "MONTHLY"
    YEARLY = "YEARLY"
    NEVER = "NEVER"
    FREQUENCY_CHOICES = Choices(
        (DAILY, "DAILY"),
        (WEEKLY, "WEEKLY"),
        (MONTHLY, "MONTHLY"),
        (YEARLY, "YEARLY"),
        (NEVER, "NEVER"),
    )
    user = models.ForeignKey(
        "accounts.User",
        related_name="scheduled_reports",
        on_delete=models.CASCADE,
    )
    frequency = models.CharField(max_length=10, choices=FREQUENCY_CHOICES)
    scheduled_report_type = models.ForeignKey(
        "core.ScheduledReportTypes",
        related_name="scheduled_for_users",
        on_delete=models.CASCADE,
    )
    next_run = models.DateTimeField(null=True, blank=True)

    @classmethod
    def get_next_run(cls, frequency):
        next_run = None
        current_date = datetime.now()
        current_date = current_date.replace(hour=0, minute=0, second=0)
        if frequency == UserScheduledReport.FREQUENCY_CHOICES.YEARLY:
            next_run = current_date.replace(
                year=current_date.year + 1, month=1, day=1
            )
        if frequency == UserScheduledReport.FREQUENCY_CHOICES.MONTHLY:
            if current_date.month == 12:
                next_run = current_date.replace(
                    year=current_date.year + 1, month=1, day=1
                )
            else:
                next_run = current_date.replace(
                    year=current_date.year, month=current_date.month + 1, day=1
                )
        if frequency == UserScheduledReport.FREQUENCY_CHOICES.WEEKLY:
            days_ahead = 0 - current_date.weekday()
            if days_ahead <= 0:  # Target day already happened this week
                days_ahead += 7
            next_run = current_date + timedelta(days_ahead)
        if frequency == UserScheduledReport.FREQUENCY_CHOICES.DAILY:
            next_run = current_date + timedelta(days=1)
        return next_run

    class Meta:
        unique_together = ("user", "scheduled_report_type")


class SubscriptionData(TimeStampedModel):
    """
    Model to store the subscription data from manufacturer
    """

    ACTIVE = ContractStatus(1, "Active")
    EXPIRING = ContractStatus(2, "Expiring")
    EXPIRED = ContractStatus(3, "Expired")
    NOT_AVAILABLE = ContractStatus(4, "N.A.")
    CONTRACT_STATUSES = [ACTIVE, EXPIRING, EXPIRED, NOT_AVAILABLE]

    subscription_id = models.CharField(max_length=50)
    subscription_crm_id = models.PositiveIntegerField()
    subscription_category_id = models.IntegerField(blank=True, null=True)
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="subscriptions",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="subscriptions",
        on_delete=models.CASCADE,
    )
    data = JSONField(default=dict)

    @classmethod
    def get_subscription_contract_status(cls, subscription_end_date):
        """
        Returns Subscription Status based on the Subscription End Date.
        :return: Status of Software Subscription
        """
        if subscription_end_date:
            subscription_end_date = datetime.strptime(
                subscription_end_date, "%Y-%m-%dT%H:%M:%SZ"
            ).date()
            current_date = datetime.today().date()
            difference_in_days = (subscription_end_date - current_date).days

            if difference_in_days < 0:
                status = SubscriptionData.EXPIRED
            elif 0 <= difference_in_days <= 90:
                status = SubscriptionData.EXPIRING
            else:
                status = SubscriptionData.ACTIVE
        else:
            status = SubscriptionData.NOT_AVAILABLE
        return status.to_json()

    class Meta:
        unique_together = ["provider", "subscription_crm_id"]

    def __str__(self):
        return self.subscription_id


class SubscriptionProductType(models.Model):
    name = models.CharField(max_length=150)
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="subscription_product_types",
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ["provider", "name"]

    def __str__(self):
        return self.name


class SubscriptionProductMapping(models.Model):
    name = models.CharField(max_length=150)
    type = models.ForeignKey(
        "core.SubscriptionProductType",
        related_name="products",
        on_delete=models.CASCADE,
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="subscription_products",
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ["provider", "name"]


class ReportDownloadStats(models.Model):
    user = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="downloaded_reports",
    )
    downloaded_on = models.DateTimeField(auto_now_add=True)
    report_type = models.CharField(max_length=300)
    report_sub_type = models.CharField(max_length=300)
    all_customer_report = models.BooleanField(default=False)

    def __str__(self):
        return self.report_type


class SmartnetReceivingSetting(TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="snt_receiving_settings",
    )
    order_status_alias_email = models.EmailField()
    email_subject = models.TextField()
    email_body = models.TextField()
    email_body_markdown = models.TextField(null=True)
    smartnet_receiving_ticket_closing_note = models.TextField()
    tac_email = models.EmailField()
    tac_email_subject = models.TextField()
    tac_email_body = models.TextField()
    shipment_method_id = models.PositiveIntegerField(null=True)
    purchase_order_open_statuses = ArrayField(
        base_field=models.PositiveIntegerField(), default=list
    )
    email_notifications_from_email = models.EmailField(
        default=settings.DEFAULT_FROM_EMAIL
    )
    opportunity_default_inside_sales_rep = models.IntegerField()
    opportunity_business_unit_id = models.IntegerField(null=True)
    opportunity_type_id = models.IntegerField(null=True)
    opportunity_status_id = models.IntegerField(null=True)
    partial_receive_status = models.PositiveIntegerField(null=True)

    @property
    def attachments(self):
        active_attachments = self.snt_receiving_email_attachments.filter(
            is_active=True
        )
        return active_attachments


def file_upload_url(instance, filename):
    provider_id = instance.smartnet_receiving_setting.provider.id
    file_path = (
        f"providers/{provider_id}/snt-receiving-email-attachments/{filename}"
    )
    return file_path


class SmartnetReceivingEmailAttachment(TimeStampedModel):
    smartnet_receiving_setting = models.ForeignKey(
        SmartnetReceivingSetting,
        on_delete=models.CASCADE,
        related_name="snt_receiving_email_attachments",
    )
    attachment = models.FileField(
        upload_to=file_upload_url, storage=get_storage_class()
    )
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ["-updated_on"]

    def __str__(self):
        return self.name


class MSAgentProviderAssociation(models.Model):
    user = ArrayField(
        models.CharField(max_length=100), null=True, blank=True, default=list
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )


class ConnectwisePhoneNumberMappingSetting(models.Model):
    """
    Stores Mapping of connectwise communication Types
    https://cw.lookingpoint.com/v4_6_release/apis/3.0/company/communicationTypes
    to Acela communication fields
    """

    provider = models.OneToOneField(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    cell_phone = models.PositiveSmallIntegerField()
    office_phone = models.PositiveIntegerField()
    last_updated_on = models.DateTimeField(auto_now=True)


class AutomatedSubscriptionNotificationSetting(models.Model):
    """
    Model to store automated subscription notification settings for the provider.
    """

    WEEKLY_SCHEDULE_CHOICES = [
        ("MONDAY", "MONDAY"),
        ("TUESDAY", "TUESDAY"),
        ("WEDNESDAY", "WEDNESDAY"),
        ("THURSDAY", "THURSDAY"),
        ("FRIDAY", "FRIDAY"),
        ("SATURDAY", "SATURDAY"),
        ("SUNDAY", "SUNDAY"),
    ]
    EXPIRED = "EXPIRED"
    NEXT_15_DAYS = "NEXT_15_DAYS"
    NEXT_16_TO_89_DAYS = "NEXT_16_TO_89_DAYS"
    NEXT_90_TO_120_DAYS = "NEXT_90_TO_120_DAYS"
    EXPIRY_DATE_RANGE_CHOICES = [
        (EXPIRED, EXPIRED),
        (NEXT_15_DAYS, NEXT_15_DAYS),
        (NEXT_16_TO_89_DAYS, NEXT_16_TO_89_DAYS),
        (NEXT_90_TO_120_DAYS, NEXT_90_TO_120_DAYS),
    ]
    provider = models.OneToOneField(
        "accounts.Provider",
        related_name="subscription_notification_setting",
        on_delete=models.CASCADE,
    )
    receiver_email_ids = ArrayField(
        base_field=models.EmailField(), null=False, blank=False
    )
    sender = models.ForeignKey(
        "accounts.User", related_name="+", on_delete=models.SET_NULL, null=True
    )
    email_subject = models.CharField(max_length=500, null=False)
    email_body = models.TextField(null=False)
    renewal_type_contact_crm_id = models.PositiveIntegerField(null=False)
    weekly_send_schedule = ArrayField(
        base_field=models.CharField(
            choices=WEEKLY_SCHEDULE_CHOICES,
            max_length=15,
        ),
        blank=False,
        null=False,
    )
    approaching_expiry_date_range = ArrayField(
        base_field=models.CharField(
            max_length=30, choices=EXPIRY_DATE_RANGE_CHOICES
        ),
        null=False,
    )
    config_type_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, blank=True
    )
    config_status_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, blank=True
    )
    manufacturer_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, blank=True
    )

    @classmethod
    def get_approaching_expiry_dates(
        cls, date_range: str
    ) -> Tuple["date", "date"]:
        """
        Get approaching expiry dates for given date range.

        Args:
            date_range: Date range

        Returns:
            Start date, end date
        """
        start_date, end_date = None, None

        if date_range == cls.EXPIRED:
            start_date: date = datetime(day=1, month=1, year=2000).date()
            end_date: date = datetime.now().date()
        elif date_range == cls.NEXT_15_DAYS:
            start_date: date = datetime.now().date()
            end_date: date = start_date + timedelta(days=15)
        elif date_range == cls.NEXT_16_TO_89_DAYS:
            start_date: date = (datetime.now() + timedelta(days=16)).date()
            end_date: date = (datetime.now() + timedelta(days=89)).date()
        elif date_range == cls.NEXT_90_TO_120_DAYS:
            start_date: date = (datetime.now() + timedelta(days=90)).date()
            end_date: date = (datetime.now() + timedelta(days=120)).date()
        return start_date, end_date

    def get_sender_details(self) -> Dict:
        """
        Get sender details.

        Returns:
            Sender details.
        """
        if not self.sender:
            return {}
        else:
            sender_details: Dict = dict(
                name=self.sender.name,
                email=self.sender.email,
                signature=self.sender.profile.email_signature.get(
                    "email_signature", None
                ),
            )
            return sender_details

    @classmethod
    def get_email_template_directory_path(cls) -> str:
        template_directory: str = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            os.path.join("emails"),
        )
        return template_directory


class ConfigurationMapping(models.Model):
    """
    Model to store configuration mapping.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="configuration_mapping",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=150, null=False, blank=False)
    config_type_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(null=False),
        null=False,
        blank=False,
    )
