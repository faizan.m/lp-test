from django.forms import model_to_dict
from marshmallow import Schema
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from marshmallow.exceptions import (
    ValidationError as marshmallowValidationError,
)

from caching.service import CacheService
from core.models import IntegrationType, ProviderIntegration
from core.serializers import (
    ProviderIntegrationSerializer,
    ERPIntegrationSerializer,
    LogicMonitorIntegrationSerializer,
    DropBoxIntegrationSerializer,
)
from core.services import IntegrationService
from core.tasks.connectwise.connectwise import (
    fetch_and_create_customer_and_users_for_provider,
)
from core.tasks.logic_monitor.logic_monitor import update_logic_monitor_devices
from core.tasks.dropbox.dropbox import sync_file_storage_for_providers


def _get_integration_type_serializer_mapping():
    result = {}
    integration_types = IntegrationType.objects.all()
    for integration_type in integration_types:
        if (
            integration_type
            == IntegrationService.get_connectwise_integration_type_object()
        ):
            result[integration_type.id] = ERPIntegrationSerializer
        elif (
            integration_type
            == IntegrationService.get_logic_monitor_integration_type_object()
        ):
            result[integration_type.id] = LogicMonitorIntegrationSerializer
        elif (
            integration_type
            == IntegrationService.get_dropbox_storage_integration_type_object()
        ):
            result[integration_type.id] = DropBoxIntegrationSerializer
        else:
            result[integration_type.id] = ProviderIntegrationSerializer
    return result


class ProviderIntegrationSerializerMixin(object):
    def get_provider_integration_serializer_class(self, data):
        integration_type_serializer_mapping = (
            _get_integration_type_serializer_mapping()
        )
        integration_type_id = data.get("integration_type")
        return integration_type_serializer_mapping.get(
            integration_type_id, ProviderIntegrationSerializer
        )

    def update_provider_integration(self, provider, old_object, serializer):
        serializer.save()
        if isinstance(serializer, ERPIntegrationSerializer):
            if serializer.data.get("other_config").get("board_mapping"):
                provider.integration_statuses[
                    "crm_board_mapping_configured"
                ] = True
            if serializer.data.get("other_config").get("manufacturer_mapping"):
                if not provider.integration_statuses[
                    "crm_device_categories_configured"
                ]:
                    fetch_and_create_customer_and_users_for_provider(
                        provider.id
                    )
                provider.integration_statuses[
                    "crm_device_categories_configured"
                ] = True
            provider.save()
            # Invalidating cache on provider ERP integration update
            CacheService.invalidate_cache()
        elif isinstance(serializer, DropBoxIntegrationSerializer):
            sync_file_storage_for_providers.delay()
        elif isinstance(serializer, LogicMonitorIntegrationSerializer):
            # Validating which mapping has been changed from the old object model and
            # then performing sync of the changed manufacturers
            old_object = model_to_dict(old_object)
            old_config = old_object.get("other_config")
            new_config = serializer.data.get("other_config")
            if old_config != new_config:
                mnf_ids_to_update = []
                for key, value in new_config.items():
                    if key in old_config:
                        old_mnf_id_value = old_config.get(key)
                        new_mnf_id_value = new_config.get(key)
                        if old_mnf_id_value != new_mnf_id_value:
                            if key == "device_mappings":
                                mnf_ids_to_update = list(value.keys())
                update_logic_monitor_devices.delay(
                    provider.id, mnf_ids_to_update
                )
