"""Contains helper methods to be run during migration"""
import csv
import json
import os

from accounts.models import Customer, User, Provider
from core.integrations import Connectwise
from core.models import (
    IntegrationCategory,
    IntegrationType,
    DeviceTypes,
    SubscriptionProductType,
    SubscriptionProductMapping,
)
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


def create_integration_categories():
    """
    Creates Integration Categories
    :return: None
    """
    categories = {
        "CRM": add_crm_integration_types,
        "DEVICE_INFO": add_device_info_integration_types,
        "DEVICE_MONITOR": add_device_monitoring_integration_types,
        "STORAGE_SERVICE": add_file_storage_integration_types,
    }
    for category, type_creation_func in categories.items():
        if not IntegrationCategory.objects.filter(name=category).count():
            category_obj = IntegrationCategory.objects.create(name=category)
            logger.debug("New Integration category created %s", category)
            type_creation_func(category_obj)


def add_crm_integration_types(category):
    """
    Creates Integration types for CRM category
    :param category : CRM category object
    :return: None
    """
    types = ["CONNECTWISE"]
    for type_ in types:
        # authentication_config, extra_config = get_crm_integration_configs(type_)
        integration_type = IntegrationType.objects.create(
            name=type_,
            category=category,
            # authentication_config=authentication_config,
            # extra_config=extra_config,
        )
        logger.debug("New Integration Type Added %s", integration_type)


def get_crm_integration_configs(integration_type_name):
    """
    Returns integration config templates for the `integration_type_name` (:class `str`) type.
    :param integration_type_name: (:class `str`)
    :returns:
            authentication_config: (:class `str`)
            extra_config: (:class 'str')
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    configs_file_dir = os.path.join(base_dir, "integration_configs")
    if integration_type_name == "CONNECTWISE":
        configs_file_path = os.path.join(configs_file_dir, "connectwise")
        authentication_config_path = os.path.join(
            configs_file_path, "authentication_config.json"
        )
        extra_config_path = os.path.join(
            configs_file_path, "board_mapping_config.json"
        )

    try:
        with open(authentication_config_path) as config:
            authentication_config = json.load(config)
            logger.debug(
                "Authentication Configs found for %s", integration_type_name
            )

        with open(extra_config_path) as config:
            extra_config = dict(board_mapping="")
            extra_config["board_mapping"] = json.load(config)
            logger.debug("Extra Configs found for %s", integration_type_name)
        return authentication_config, extra_config
    except FileNotFoundError as error:
        logger.warning("Configurations files not found: %s", error)


def get_device_info_integration_configs(integration_type_name):
    """
    Returns integration config templates for the `integration_type_name` (:class `str`) type.
    :param integration_type_name: (:class `str`)
    :returns:
            authentication_config: (:class `str`)
            extra_config: (:class 'str')
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    configs_file_dir = os.path.join(base_dir, "integration_configs")
    if integration_type_name == "CISCO":
        configs_file_path = os.path.join(configs_file_dir, "cisco")
        authentication_config_path = os.path.join(
            configs_file_path, "authentication_config.json"
        )
    try:
        with open(authentication_config_path) as config:
            authentication_config = json.load(config)
            logger.debug(
                "Authentication Configs found for %s", integration_type_name
            )
        extra_config = {}
        return authentication_config, extra_config
    except FileNotFoundError as error:
        logger.warning("Configurations files not found: %s", error)


def add_device_info_integration_types(device_info_category):
    """
    Creates Integration types for DEVICE_INFO category
    :param device_info_category: (:class: `IntegrationCategory`): with name as DEVICE_INFO.
    :return:
    """
    types = ["CISCO"]
    for type_ in types:
        # authentication_config, extra_config = get_device_info_integration_configs(type_)
        integration_type = IntegrationType.objects.create(
            name=type_,
            category=device_info_category,
            # authentication_config=authentication_config,
            # extra_config=extra_config,
        )
        logger.debug("New Integration Type Added %s", integration_type)


def get_device_monitor_integration_configs(integration_type_name):
    """
    Returns integration config templates for the `integration_type_name` (:class `str`) type.
    :param integration_type_name: (:class `str`)
    :returns:
            authentication_config: (:class `str`)
            extra_config: (:class 'str')
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    configs_file_dir = os.path.join(base_dir, "integration_configs")
    if integration_type_name == "LOGIC_MONITOR":
        configs_file_path = os.path.join(configs_file_dir, "logic_monitor")
        authentication_config_path = os.path.join(
            configs_file_path, "authentication_config.json"
        )
        extra_config_path = os.path.join(
            configs_file_path, "extra_config.json"
        )
    try:
        with open(authentication_config_path) as config:
            authentication_config = json.load(config)
            logger.debug(
                "Authentication Configs found for %s", integration_type_name
            )
        with open(extra_config_path) as config:
            extra_config = json.load(config)
            logger.debug("Extra Configs found for %s", integration_type_name)
        return authentication_config, extra_config
    except FileNotFoundError as error:
        logger.warning("Configurations files not found: %s", error)


def add_device_monitoring_integration_types(device_monitor_category):
    """
    Creates Integration types for LOGIC_MONITOR category
    :param device_info_category: (:class: `IntegrationCategory`): with name as LOGIC_MONITOR.
    :return:
    """
    types = ["LOGIC_MONITOR"]
    for type_ in types:
        # authentication_config, extra_config = get_device_monitor_integration_configs(
        #     type_
        # )
        integration_type = IntegrationType.objects.create(
            name=type_,
            category=device_monitor_category,
            # authentication_config=authentication_config,
            # extra_config=extra_config,
        )
        logger.debug("New Integration Type Added %s", integration_type)


def add_file_storage_integration_types(file_storage_category):
    """
    Creates Integration types for DROPBOX category
    :param device_info_category: (:class: `IntegrationCategory`): with name as DROPBOX.
    :return:
    """
    types = ["DROPBOX"]
    for type_ in types:
        # authentication_config, extra_config = get_file_storage_integration_configs(
        #     type_
        # )
        integration_type = IntegrationType.objects.create(
            name=type_,
            category=file_storage_category,
            # authentication_config=authentication_config,
            # extra_config=extra_config,
        )
        logger.debug(
            "File Storage Integration Type Added %s", integration_type
        )


def get_file_storage_integration_configs(integration_type_name):
    """
    Returns integration config templates for the `integration_type_name` (:class `str`) type.
    :param integration_type_name: (:class `str`)
    :returns:
            authentication_config: (:class `str`)
            extra_config: (:class 'str')
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    configs_file_dir = os.path.join(base_dir, "integration_configs")
    if integration_type_name == "DROPBOX":
        configs_file_path = os.path.join(configs_file_dir, "dropbox")
        authentication_config_path = os.path.join(
            configs_file_path, "authentication_config.json"
        )
    try:
        with open(authentication_config_path) as config:
            authentication_config = json.load(config)
            logger.debug(
                "Authentication Configs found for %s", integration_type_name
            )
        extra_config = {}
        return authentication_config, extra_config
    except FileNotFoundError as error:
        logger.warning("Configurations files not found: %s", error)


def load_device_types_mapping():
    """
    Creates Device Types and Model name mapping in DB.
    :return:
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(base_dir, "device_types_csv", "device-type.csv")
    if not os.path.exists(file_path):
        logger.info("File not found %s", file_path)
        return False
    with open(file_path) as device_type_csv:
        reader = csv.reader(device_type_csv)
        next(reader, None)  # skip headers
        device_type_objects = [
            DeviceTypes(product_instance_id=row[0], type=row[1])
            for row in reader
        ]
        DeviceTypes.objects.bulk_create(device_type_objects)
        logger.info("Device Categories Mapping Added to DB.")
    return True


def update_integration_type_configs(integration_type):
    authentication_config, extra_config = get_crm_integration_configs(
        integration_type.name
    )
    # integration_type.authentication_config = authentication_config
    # integration_type.extra_config = extra_config
    integration_type.save()


def add_crm_id_to_existing_customer_users():
    all_customer_crm_ids = Customer.objects.all().values_list(
        "crm_id", flat=True
    )
    erp_client = Connectwise({})
    for customer_id in all_customer_crm_ids:
        contacts = erp_client.get_customer_users(customer_id)
        for contact in contacts:
            email = contact.get("email")
            if email:
                user = User.objects.filter(email=email).first()
                if not user:
                    user = User.objects.filter(email=email.lower()).first()
                if user:
                    user.crm_id = contact["id"]
                    user.save(update_fields=["crm_id"])


def update_device_category_mappings():
    for provider in Provider.objects.all():
        if provider.is_configured and provider.is_active:
            other_config = provider.erp_integration.other_config
            device_categories = other_config.get("device_categories")
            manufacturer_map = other_config.get("manufacturer_map")
            manufacturer_mapping = {}
            manufacturers = {
                x.get("id"): x.get("label")
                for x in provider.erp_client.get_manufacturers()
            }
            categories = {
                x.get("category_id"): x.get("category_name")
                for x in provider.erp_client.get_device_categories()
            }
            if manufacturer_map:
                for (
                    acela_integration_type_id,
                    cw_mnf_id,
                ) in manufacturer_map.items():
                    manufacturer_mapping[cw_mnf_id] = dict(
                        cw_mnf_name=manufacturers.get(cw_mnf_id),
                        device_category_id=device_categories[0],
                        device_category_name=categories.get(
                            device_categories[0]
                        ),
                        acela_integration_type_id=acela_integration_type_id,
                    )

            default = {"device_category_id": device_categories[0]}
            manufacturer_mapping["default"] = default
            if provider.erp_integration.other_config.get(
                "manufacturer_mapping"
            ):
                provider.erp_integration.other_config.get(
                    "manufacturer_mapping"
                ).update(manufacturer_mapping)
            else:
                provider.erp_integration.other_config[
                    "manufacturer_mapping"
                ] = manufacturer_mapping
            provider.erp_integration.save()
            print(f"Mapping updated for Provider {provider.name}")
            print(f"Update mapping is: {manufacturer_mapping}")


def create_subscription_product_mapping():
    pass
