import functools
import re
import secrets
import string
from datetime import datetime, timedelta, time

import numpy as np
import pandas as pd
import pytz

from accounts.models import UserType
from dateutil.parser import parse


def get_customer_cw_id(request, **kwargs):
    """
    Returns crm_id for the customer based on the request user.
    :param request: Request object
    :param kwargs:
    :return:
    """
    from accounts.models import UserType, Customer
    from rest_framework.exceptions import NotFound

    customer_id = None
    if request.user.type == UserType.PROVIDER:
        try:
            customer = Customer.objects.get(
                id=kwargs.get("customer_id"), provider=request.provider
            )
            customer_id = customer.crm_id
        except Customer.DoesNotExist:
            raise NotFound("Invalid Customer ID.")

    elif request.user.type == UserType.CUSTOMER:
        customer_id = request.customer.crm_id
    if not customer_id:
        raise NotFound("No customer found.")
    return customer_id


def get_customer_id(request, **kwargs):
    """
    Returns customer id for the customer based on the request user.
    :param request: Request object
    :param kwargs:
    :return: customer_id: (:class 'int')
    """
    from rest_framework.exceptions import NotFound

    if not request.user.is_authenticated:
        return None
    customer_id = None
    if request.user.type == UserType.PROVIDER:
        customer_id = kwargs.get("customer_id")
    elif UserType.CUSTOMER == request.user.type:
        customer_id = request.customer.id
    if not customer_id:
        raise NotFound("No customer found.")
    return customer_id


def group_contacts_by_company_id(contacts):
    """
    Groups all contacts by the customer id. Example:
    {
    "2":[{contact_1}, {contact_2}],
    "5":[{contact_23}, {contact_26}]
    }
    :param contacts: List of all contacts.
    :return: dict
    """

    return group_list_of_dictionary_by_key(contacts, "customer_id")


def group_list_of_dictionary_by_key(data_list, group_by_key):
    """
    Group a list of dictionary by particular key in the dictionary
    :param data_list: list of dictionaries
    :param group_by_key: key bby which data is to be grouped
    :return:
    """
    results = {}
    for item in data_list:
        if item.get(group_by_key):
            group = results.get(item[group_by_key])
            if group:
                group.append(item)
            else:
                results[item[group_by_key]] = [item]
    return results


def get_list_of_values_for_key(data, key):
    """
    Returns list of non-none values for a particular key in a list of dictionary.
    :param data: list of dictionary
    :param key: Key whose values are to be added.
    :return: list of values.
    """
    result = []
    for item in data:
        if item.get(key):
            result.append(item[key])
    return result


def chunks_of_list_with_max_item_count(input_list, max_items_per_list):
    """Yield successive n-sized chunks from input_list."""
    for i in range(0, len(input_list), max_items_per_list):
        yield input_list[i : i + max_items_per_list]


def chunks_of_list_with_max_items_length(input_list, max_length_per_list):
    """Yield successive chunks from input_list, each chunks combined length < max_length_per_list"""
    tmpslice = []  # tmp list where we append slice numbers.
    for i, item in enumerate(input_list):
        running_count = len(",".join(tmpslice))
        if running_count + len(item) <= int(max_length_per_list):
            tmpslice.append(item)
        else:
            yield tmpslice
            tmpslice = [item]
            running_count = len(",".join(tmpslice))
    yield (tmpslice)


def generate_random_id(N):
    # Generate random string of length N.
    random_id = "".join(
        secrets.choice(string.ascii_uppercase + string.digits)
        for _ in range(N)
    )
    return random_id


def remove_keys_with_none_and_empty_value(dict_):
    """
    Utility function to remove keys which have None or empty value from a dict.
    :param dict_: Input Dictionary
    :return: dict
    """
    return {k: v for k, v in dict_.items() if v is not None and v != ""}


def remove_keys_with_certain_values(dict_, value_list):
    """
    Utility function to remove keys which have value in value_list from the dict.
    :param value_list: List of values eg: value_list=['', '-']
    :param dict_: Input Dictionary
    :return: dict
    """
    return {
        k: v for k, v in dict_.items() if v is not None and v not in value_list
    }


def device_date_formats():
    date_format_conversion = {
        "NUMBER": "NUMBER",
        # "DATE": "DATE",
        "DD/MM/YY": "%d/%m/%y",
        "DD/MM/YYYY": "%d/%m/%Y",
        "DDMMYY": "%d%m%y",
        "DDMMYYYY": "%d%m%Y",
        "DD-MM-YY": "%d-%m-%y",
        "DD-MM-YYYY": "%d-%m-%Y",
        "DD MMM YYYY": "%d %b %Y",
        "MMDDYY": "%m%d%y",
        "MMDDYYYY": "%m%d%Y",
        "MM/DD/YY": "%m/%d/%y",
        "MM/DD/YYYY": "%m/%d/%Y",
        "MM-DD-YY": "%m-%d-%y",
        "MM-DD-YYYY": "%m-%d-%Y",
        "YY/MM/DD": "%y/%m/%d",
        "YYYY/MM/DD": "%Y/%m/%d",
        "YY/DD/MM": "%y/%d/%m",
        "YYYY/DD/MM": "%Y/%d/%m",
        "YY-MM-DD": "%y-%m-%d",
        "YYYY-MM-DD": "%Y-%m-%d",
        "YY-DD-MM": "%y-%d-%m",
        "YYYY-DD-MM": "%Y-%d-%m",
        "YYMMDD": "%y%m%d",
        "YYYYMMDD": "%Y%m%d",
        "YYDDMM": "%y%d%m",
        "YYYYDDMM": "%Y%d%m",
    }
    return date_format_conversion


def convert_date_string_to_date_object(date, date_format):
    """
    Utility function to convert a date string to a python date object
    :param date: Date in String format eg: '22/04/2016'
    :param date_format: Format of the input date string'%d/%m/%Y'
    :return: datetime.date(2016, 4, 22)
    """
    if date and date_format:
        date = datetime.strptime(date, date_format).date()
    return date


def convert_date_object_to_date_string(date, output_format):
    """
    Utility function to convert date object to date string
    :param date: input date
    :type date: datetime object
    :param output_format: output string date format
    :type output_format: string
    :return: string date
    """
    return datetime.strftime(date, output_format)


def convert_date_string_format(date_string, current_format, required_format):
    """
    Utility function to convert one date string format
    :param date_string: Date string
    :type date_string: string
    :param current_format: Current date string format
    :type current_format: String
    :param required_format: required format
    :type required_format: String
    :return: New date string
    """
    date_obj = convert_date_string_to_date_object(date_string, current_format)
    new_date_str = convert_date_object_to_date_string(
        date_obj, required_format
    )
    return new_date_str


def convert_to_date_from_date_format(date_value, date_format):
    date = np.nan
    try:
        if date_format == "NUMBER":
            date = convert_ordinal_date_to_date_object(date_value)
        elif isinstance(date_value, pd.Timestamp) or isinstance(
            date_value, datetime
        ):
            date = date_value.date()
        else:
            date = convert_date_string_to_date_object(date_value, date_format)
    except:
        date = parse(date_value).date()
    finally:
        return date


def convert_ordinal_date_to_date_object(date):
    """
    Utility function to convert a ordinal value to a python date object
    :param date: Ordinal in Integer format eg: 42014
    :return: datetime.date(2015, 1, 10)
    """
    try:
        date = int(date)
        if isinstance(date, int):
            date = datetime.fromordinal(
                datetime(1900, 1, 1).toordinal() + date - 2
            ).date()
        else:
            return np.nan
    except:
        return np.nan
    return date


def batch_call(max_count=None):
    def decorator(func):
        """
        Decorator to handle batch call. Takes a parameter :max_count which is the max
        items allowed in one api call.
        """

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            data = args[0]
            result = []
            if isinstance(data, list):
                if max_count:
                    for data in chunks_of_list_with_max_item_count(
                        data, max_count
                    ):
                        input = data
                        result.extend(func(self, input, **kwargs))
                else:
                    for data in chunks_of_list_with_max_items_length(
                        data, 1500
                    ):
                        input = data
                        result.extend(func(self, input, **kwargs))
            else:
                result = func(self, *args, **kwargs)
            return result

        return wrapper

    return decorator


def url_validation_regex(url):
    """
    Util function to validate if the url is valid.
    """
    regex = re.compile(
        r"^(?!:\/\/)([a-zA-Z0-9-_]+\.)([a-zA-Z0-9-_]+\.)([a-zA-Z0-9-_]+)$",
        re.IGNORECASE,
    )
    return re.match(regex, url)
