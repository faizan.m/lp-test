import csv
import os
import tempfile
from datetime import datetime, timedelta
from dateutil import parser

import numpy as np
import pandas as pd
from django.conf import settings
from weasyprint import HTML, CSS

from django.template.loader import get_template
from django.db import connection

from accounts.models import Provider, UserType
from core.models import (
    DeviceManufacturerData,
    ScheduledReportTypes,
    ReportDownloadStats,
)
from core.services import DeviceService
from core.utils import (
    convert_date_string_to_date_object,
    generate_random_id,
    convert_date_string_format,
    convert_date_object_to_date_string,
)
from utils import get_app_logger, get_valid_file_name

# Get an instance of a logger
logger = get_app_logger(__name__)


def generate_customer_device_management_report(
    provider_id, customer_id, filter_conditions, user_type, user
):
    """
    This method generates the Lifecycle Management Report for the specific filters `filter_conditions`.
    Returns:
        file_path (:class: `str`): File path of the generated report.
        file_name (:class: `str`): Name of the generated report.
    """
    provider = Provider.objects.get(id=provider_id)
    category_id = provider.get_all_device_categories()
    file_path, file_name = None, None
    devices = DeviceService.get_device_list(
        provider_id=provider.id,
        customer_id=customer_id,
        erp_client=provider.erp_client,
        category_id=category_id,
    )
    if devices:
        report_type = filter_conditions.pop("report_type")
        report_format = filter_conditions.pop("report_format", None)
        devices = DeviceService.filter_devices_data(devices, filter_conditions)
        if not devices:
            return None, None
        devices = parse_device_data_by_report_type(
            devices, report_type, provider_id
        )
        if devices is None:
            return None, None
        file_path, file_name = generate_customer_device_report(
            devices, report_type, user_type, report_format
        )
        # log reports downloaded for all customers
        if not customer_id:
            all_customer_report = True
            log_report_download(
                user,
                report_type["display_name"],
                report_type["name"],
                all_customer_report,
            )

    return file_path, file_name


def parse_device_data_by_report_type(devices, report_type, provider_id):
    if report_type.get("category") == "LIFECYCLE_MANAGEMENT_REPORT":
        return parse_lifecycle_management_device_data(
            devices, provider_id, report_type.get("type")
        )
    if report_type.get("category") == "DEVICE_COVERAGE_REPORT":
        return parse_device_coverage_data(
            devices, provider_id, report_type.get("type")
        )
    if report_type.get("category") == "DEVICE_REPORT":
        return parse_device_report_data(devices, provider_id)
    if report_type.get("category") == "SOFTWARE_MANAGEMENT_REPORT":
        return parse_software_management_device_report_data(
            devices, provider_id, report_type.get("type")
        )


def parse_device_report_data(devices, provider_id):
    devices = DeviceService.add_site_address_to_device_data(
        provider_id, devices
    )
    for device in devices:
        try:
            expiration_date = parser.parse(
                device.get("expiration_date")
            ).date()
            expiration_date = convert_date_object_to_date_string(
                expiration_date, "%m/%d/%Y"
            )
            device["expiration_date"] = expiration_date
        except:
            device["expiration_date"] = ""
        if device.get("EOL_date"):
            eol_date = parser.parse(device["EOL_date"])
            device["EOL_date"] = convert_date_object_to_date_string(
                eol_date, "%m/%d/%Y"
            )
        else:
            device["EOL_date"] = ""
    return devices


def parse_device_coverage_data(devices, provider_id, report_type):
    if report_type == "REPORT04":
        # Expired devices
        status = dict(contract_status=[str(DeviceManufacturerData.EXPIRED)])
        devices = DeviceService.filter_devices_data(devices, status)
    elif report_type == "REPORT05":
        # Expiring device support (Next 90 Days)
        status = dict(contract_status=[str(DeviceManufacturerData.EXPIRING)])
        devices = DeviceService.filter_devices_data(devices, status)
    elif report_type == "REPORT06":
        # Expiring device support (Next 12 Months)
        status = dict(contract_status=[str(DeviceManufacturerData.ACTIVE)])
        devices = DeviceService.filter_devices_data(devices, status)
        one_year_ahead_date = (datetime.now() + timedelta(days=365)).date()
        result = []
        for device in devices:
            expiration_date = device.get("expiration_date")
            if expiration_date:
                expiration_date = parser.parse(expiration_date).date()
                difference_in_days = (
                    expiration_date - one_year_ahead_date
                ).days
                if 0 <= difference_in_days <= 365:
                    result.append(device)
        devices = result
    else:
        return None
    devices = parse_dates_for_devices(devices)
    devices = DeviceService.add_site_address_to_device_data(
        provider_id, devices
    )
    return devices


def parse_dates_for_devices(devices):
    for device in devices:
        expiration_date = device.get("expiration_date")
        if expiration_date:
            expiration_date = parser.parse(expiration_date).date()
            expiration_date = convert_date_object_to_date_string(
                expiration_date, "%m/%d/%Y"
            )
            device["expiration_date"] = str(expiration_date)
        else:
            device["expiration_date"] = ""
        if device.get("EOL_date"):
            current_date = datetime.now().date()
            eol_date = parser.parse(device["EOL_date"]).date()
            device["eol_status"] = "YES" if eol_date < current_date else "NO"
            device["EOL_date"] = convert_date_object_to_date_string(
                eol_date, "%m/%d/%Y"
            )
        else:
            device["eol_status"] = ""
            device["EOL_date"] = ""
    return devices


def parse_lifecycle_management_device_data(devices, provider_id, report_type):
    serials = {
        device.get("serial_number")
        for device in devices
        if device.get("serial_number") is not None
    }
    if report_type == "REPORT01":
        # End of life devices condition.
        date_query = "AND (TO_DATE(((data::json #> '{eox_data}')::jsonb) ->> 'EOL_date', 'YYYY-MM-DD') < current_date)"
    elif report_type == "REPORT02":
        # Coming End of life devices condition (Next 12 months).
        one_year_ahead_date = (datetime.now() + timedelta(days=365)).strftime(
            "%Y-%m-%d"
        )
        date_query = (
            f"AND (TO_DATE(((data::json #> '{{eox_data}}')::jsonb) ->> 'EOL_date', 'YYYY-MM-DD') > current_date) "
            f"AND (TO_DATE(((data::json #> '{{eox_data}}')::jsonb) ->> 'EOL_date', 'YYYY-MM-DD') < '{one_year_ahead_date}') "
        )
    elif report_type == "REPORT03":
        # Coming End of life devices condition (Greater than One Year)
        one_year_ahead_date = (datetime.now() + timedelta(days=365)).strftime(
            "%Y-%m-%d"
        )
        date_query = f"AND (TO_DATE(((data::json #> '{{eox_data}}')::jsonb) ->> 'EOL_date', 'YYYY-MM-DD') > '{one_year_ahead_date}') "
    else:
        return None
    q = (
        f"SELECT device_serial_number as serial_number  "
        f"FROM core_devicemanufacturerdata "
        f"where provider_id = {provider_id} "
        f"AND device_serial_number IN {tuple(serials)} "
        f"{date_query}"
    )
    device_manufacturer_list = []
    with connection.cursor() as cursor:
        cursor.execute(q)
        for item in cursor:
            device_manufacturer_list.append(item[0])
    # devices_to_remove = set(serials) - set(device_manufacturer_list)
    if not device_manufacturer_list:
        return None
    devices = DeviceService.filter_devices_data(
        devices, dict(serial_number=device_manufacturer_list)
    )
    for device in devices:
        if device.get("EOL_date"):
            device["EOL_date"] = convert_date_string_format(
                device["EOL_date"], "%Y-%m-%d", "%m/%d/%Y"
            )
        else:
            device["EOL_date"] = ""
        if device.get("EOSA_date"):
            device["EOSA_date"] = convert_date_string_format(
                device["EOSA_date"], "%Y-%m-%d", "%m/%d/%Y"
            )
        else:
            device["EOSA_date"] = ""
        if device.get("EOSU_date"):
            device["EOSU_date"] = convert_date_string_format(
                device["EOSU_date"], "%Y-%m-%d", "%m/%d/%Y"
            )
        else:
            device["EOSU_date"] = ""
        if device.get("LDOS_date"):
            device["LDOS_date"] = convert_date_string_format(
                device["LDOS_date"], "%Y-%m-%d", "%m/%d/%Y"
            )
        else:
            device["LDOS_date"] = ""
    devices = DeviceService.add_site_address_to_device_data(
        provider_id, devices
    )
    return devices


def parse_software_management_device_report_data(
    devices, provider_id, report_type
):
    filtered_devices = []
    if report_type == "REPORT08":
        filtered_devices = []
        for device in devices:
            monitoring_data = DeviceService.get_combined_monitoring_data(
                device.get("monitoring_data")
            )
            if (
                monitoring_data
                and monitoring_data.get("is_managed")
                and device.get("suggested_software_update")
            ):
                device.update(monitoring_data)
                filtered_devices.append(device)
    if report_type == "REPORT09":
        for device in devices:
            if device.get("suggested_software_update"):
                filtered_devices.append(device)
    filtered_devices = sorted(
        filtered_devices,
        key=lambda x: (
            x.get("model_number") is not None,
            x.get("model_number"),
        ),
    )
    filtered_devices = sorted(
        filtered_devices,
        key=lambda x: (x.get("device_type") is not None, x.get("device_type")),
    )
    return filtered_devices


def generate_customer_device_report(
    devices, report_type, user_type, report_format
):
    if devices:
        if user_type == UserType.CUSTOMER:
            # prepare pdf report
            return generate_customer_reporting_for_customer(
                devices, report_type
            )
        elif report_format and "pdf" in report_format:
            # prepare pdf report
            return generate_customer_reporting_for_customer(
                devices, report_type
            )
        else:
            # prepare csv report
            return generate_customer_reporting_for_provider(
                devices, report_type
            )
    else:
        return None, None


def generate_customer_reporting_for_customer(devices, report_type):
    customer_reporting_types = DeviceService.get_customer_reporting_types()
    category = report_type.get("category")
    sub_category = report_type.get("type")
    customer_reporting_type = customer_reporting_types.get(category).get(
        sub_category
    )
    sub_title = customer_reporting_type.get("name")
    if category == "LIFECYCLE_MANAGEMENT_REPORT":
        html_template_file = (
            "reports/lifecycle_management_device_customer_reporting.html"
        )
        pdf_title = "Lifecycle Management Device Report"
        return create_device_report_pdf(
            devices, html_template_file, f"{pdf_title} - {sub_title}"
        )
    elif category == "DEVICE_COVERAGE_REPORT":
        html_template_file = "reports/device_coverage_customer_reporting.html"
        pdf_title = "Device Coverage Customer Reporting"
        return create_device_report_pdf(
            devices, html_template_file, f"{pdf_title} - {sub_title}"
        )
    elif category == "DEVICE_REPORT":
        html_template_file = "reports/report_template.html"
        pdf_title = "Device Report"
        return prepare_device_report_pdf(
            devices, html_template_file, f"{pdf_title} - {sub_title}"
        )
    elif category == "SOFTWARE_MANAGEMENT_REPORT":
        pdf_title = "Software Management Customer Reporting"
        if sub_category == "REPORT08":
            html_template_file = "reports/soft_management_managed_devices.html"
        else:
            html_template_file = "reports/soft_management_all_devices.html"
        return create_device_report_pdf(
            devices, html_template_file, f"{pdf_title} - {sub_title}"
        )
    else:
        return None, None


def generate_customer_reporting_for_provider(devices, report_type):
    if report_type.get("category") == "LIFECYCLE_MANAGEMENT_REPORT":
        return prepare_lifecycle_management_report_csv(devices)
    if report_type.get("category") == "DEVICE_COVERAGE_REPORT":
        return prepare_device_coverage_report_csv(devices)
    if report_type.get("category") == "DEVICE_REPORT":
        return prepare_device_report_csv(devices)
    if report_type.get("category") == "SOFTWARE_MANAGEMENT_REPORT":
        return prepare_software_management_device_report_csv(
            devices, report_type.get("type")
        )


def create_device_report_pdf(devices, html_template_file, pdf_title):
    file_name = f"{pdf_title.replace(' ' ,'_')}.pdf"
    file_name = get_valid_file_name(file_name)
    template = get_template(html_template_file)
    template_vars = {
        "title": pdf_title,
        "table_html": devices,
        "device_count": len(devices),
    }
    html_out = template.render(template_vars)
    TEMP_DIR = tempfile.gettempdir()
    file_path = os.path.join(TEMP_DIR, file_name)
    HTML(string=html_out).write_pdf(
        target=file_path,
        stylesheets=[CSS(os.path.join(settings.STATIC_ROOT, "css/cash.css"))],
    )
    return file_path, file_name


def prepare_device_report_pdf(devices, html_template_file, pdf_title):
    df = pd.DataFrame.from_dict(devices, dtype=str)
    df = df.replace("\.0", "", regex=True)
    reorder_list = [
        str(DeviceManufacturerData.EXPIRED),
        str(DeviceManufacturerData.EXPIRING),
        str(DeviceManufacturerData.ACTIVE),
        str(DeviceManufacturerData.NOT_AVAILABLE),
    ]
    df.contract_status = pd.Categorical(
        df.contract_status, categories=reorder_list
    )
    if "EOL_date" in df.columns:
        df["EOL_date"] = pd.to_datetime(
            df["EOL_date"], errors="coerce"
        ).dt.strftime("%m/%d/%Y")
        df["EOL_date"].replace({"NaT": "-"}, inplace=True)
    if "expiration_date" in df.columns:
        df["expiration_date"] = pd.to_datetime(
            df["expiration_date"], errors="coerce"
        ).dt.strftime("%m/%d/%Y")
        df["expiration_date"].replace({"NaT": "-"}, inplace=True)
    df.sort_values(
        by=["site_id", "contract_status"],
        ascending=[False, True],
        inplace=True,
    )
    df = df.replace((np.nan, "nan"), "-", regex=True)
    df = df.reset_index()
    return create_device_report_pdf(
        df.to_dict("result"), html_template_file, pdf_title
    )


def prepare_lifecycle_management_report_csv(devices):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    headers = [
        "Device Name",
        "Model Number",
        "Serial Number",
        "Customer",
        "Site",
        "Site Address",
        "EOL Date",
        "Replacement Device Info",
        "Replacement Device ID",
        "EOSA Date",
        "EOSU Date",
        "LDOS Date",
        "System Name",
        "Monitored IP",
    ]
    new_data = []
    for record in devices:
        prepared_data = dict()
        prepared_data["Device Name"] = record.get("device_name")
        prepared_data["Model Number"] = record.get("model_number")
        prepared_data["Serial Number"] = record.get("serial_number")
        prepared_data["Customer"] = record.get("customer_name")
        prepared_data["Site"] = record.get("name")
        prepared_data["Site Address"] = record.get("site_address")
        prepared_data["EOL Date"] = record.get("EOL_date")
        prepared_data["Replacement Device Info"] = record.get("migration_info")
        prepared_data["Replacement Device ID"] = record.get(
            "migration_product_id"
        )
        prepared_data["EOSA Date"] = record.get("EOSA_date")
        prepared_data["EOSU Date"] = record.get("EOSU_date")
        prepared_data["LDOS Date"] = record.get("LDOS_date")
        prepared_data["System Name"] = (
            record.get("monitoring_data", {})
            .get("logicmonitor", {})
            .get("sys_name", "")
            if record.get("monitoring_data") is not None
            else ""
        )
        prepared_data["Monitored IP"] = (
            record.get("monitoring_data", {})
            .get("logicmonitor", {})
            .get("host_name", "")
            if record.get("monitoring_data") is not None
            else ""
        )
        new_data.append(prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info(
            "Lifecycle management csv file generated at path {0}".format(
                file_path
            )
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None, None
    return file_path, file_name


def prepare_device_coverage_report_csv(devices):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    headers = [
        "Device Name",
        "Model Number",
        "Serial Number",
        "Customer",
        "Site",
        "Site Address",
        "Contract End Date",
        "EOL (Yes/No)",
    ]
    new_data = []
    for record in devices:
        prepared_data = dict()
        prepared_data["Device Name"] = record.get("device_name")
        prepared_data["Model Number"] = record.get("model_number")
        prepared_data["Serial Number"] = record.get("serial_number")
        prepared_data["Customer"] = record.get("customer_name")
        prepared_data["Site"] = record.get("name")
        prepared_data["Site Address"] = record.get("site_address")
        prepared_data["Contract End Date"] = record.get("expiration_date")
        prepared_data["EOL (Yes/No)"] = record.get("eol_status")
        new_data.append(prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info(
            "Device coverage csv file generated at path {0}".format(file_path)
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None, None
    return file_path, file_name


def prepare_device_report_csv(devices):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    headers = [
        "Device Name",
        "Model Number",
        "Serial Number",
        "Customer",
        "Site",
        "Site Address",
        "EOL Date",
        "Service Contract Number",
        "Status",
        "Manufacturer Name",
        "Category Name",
        "Device Type",
        "Expiration Date",
        "System Name",
        "Monitored IP",
    ]
    new_data = []
    for record in devices:
        prepared_data = dict()
        prepared_data["Serial Number"] = record.get("serial_number")
        prepared_data["Service Contract Number"] = record.get(
            "service_contract_number"
        )
        prepared_data["Status"] = record.get("status")
        prepared_data["Model Number"] = record.get("model_number")
        prepared_data["Device Name"] = record.get("device_name")
        prepared_data["Manufacturer Name"] = record.get("manufacturer_name")
        prepared_data["Category Name"] = record.get("category_name")
        prepared_data["Device Type"] = record.get("device_type")
        prepared_data["Customer"] = record.get("customer_name")
        prepared_data["Site"] = record.get("name")
        prepared_data["Site Address"] = record.get("site_address")
        prepared_data["Expiration Date"] = record.get("expiration_date")
        prepared_data["EOL Date"] = record.get("EOL_date")
        prepared_data["System Name"] = (
            record.get("monitoring_data", {})
            .get("logicmonitor", {})
            .get("sys_name", "")
            if record.get("monitoring_data") is not None
            else ""
        )
        prepared_data["Monitored IP"] = (
            record.get("monitoring_data", {})
            .get("logicmonitor", {})
            .get("host_name", "")
            if record.get("monitoring_data") is not None
            else ""
        )
        new_data.append(prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info(
            "Device coverage csv file generated at path {0}".format(file_path)
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None, None
    return file_path, file_name


def prepare_software_management_device_report_csv(devices, report_type):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    if report_type == "REPORT08":
        headers = [
            "Device Type",
            "Device Name",
            "Model Number",
            "IP Address",
            "Current IOS Version",
            "Rec IOS Version",
            "Release Date",
        ]
        new_data = []
        for record in devices:
            prepared_data = dict()
            prepared_data["Device Type"] = record.get("device_type")
            prepared_data["Device Name"] = record.get("device_name")
            prepared_data["Model Number"] = record.get("model_number")
            monitoring_data = DeviceService.get_combined_monitoring_data(
                record.get("monitoring_data")
            )
            prepared_data["IP Address"] = monitoring_data.get("host_name", "-")
            prepared_data["Current IOS Version"] = monitoring_data.get(
                "os_version", "-"
            )
            prepared_data["Rec IOS Version"] = record.get(
                "release_version", "-"
            )
            prepared_data["Release Date"] = record.get("release_date", "-")
            new_data.append(prepared_data)
    elif report_type == "REPORT09":
        headers = [
            "Device Type",
            "Device Name",
            "Model Number",
            "Rec IOS Version",
            "Release Date",
            "EOL Date",
            "Support Status",
        ]
        new_data = []
        for record in devices:
            prepared_data = dict()
            prepared_data["Device Type"] = record.get("device_type")
            prepared_data["Device Name"] = record.get("device_name")
            prepared_data["Model Number"] = record.get("model_number")
            prepared_data["Rec IOS Version"] = record.get(
                "release_version", "-"
            )
            prepared_data["Release Date"] = record.get("release_date", "-")
            prepared_data["EOL Date"] = record.get("EOL_date")
            prepared_data["Support Status"] = record.get("status")
            new_data.append(prepared_data)
    else:
        return None, None
    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info(
            "Software Management Report (Managed Devices) csv file generated at path {0}".format(
                file_path
            )
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None, None
    return file_path, file_name


def prepare_scheduled_report_devices_data(devices, report_type):
    difference_date = None
    if (
        report_type
        == ScheduledReportTypes.DEVICE_REPORT_TYPES.DEVICE_SUPPORT_90_DAYS
    ):
        # Expiring device support (Next 90 Days)
        status = dict(contract_status=[str(DeviceManufacturerData.EXPIRING)])
        devices = DeviceService.filter_devices_data(devices, status)
        difference_date = (datetime.now() + timedelta(days=90)).date()
    elif (
        report_type
        == ScheduledReportTypes.DEVICE_REPORT_TYPES.DEVICE_SUPPORT_30_DAYS
    ):
        # Expiring device support (Next 30 Days)
        status = dict(contract_status=[str(DeviceManufacturerData.ACTIVE)])
        devices = DeviceService.filter_devices_data(devices, status)
        difference_date = (datetime.now() + timedelta(days=30)).date()
    elif (
        report_type
        == ScheduledReportTypes.DEVICE_REPORT_TYPES.DEVICE_SUPPORT_60_DAYS
    ):
        # Expiring device support (Next 60 Days)
        status = dict(contract_status=[str(DeviceManufacturerData.ACTIVE)])
        devices = DeviceService.filter_devices_data(devices, status)
        difference_date = (datetime.now() + timedelta(days=60)).date()
    elif (
        report_type
        == ScheduledReportTypes.DEVICE_REPORT_TYPES.DEVICE_SUPPORT_365_DAYS
    ):
        # Expiring device support (Next 12 Months)
        status = dict(contract_status=[str(DeviceManufacturerData.ACTIVE)])
        devices = DeviceService.filter_devices_data(devices, status)
        difference_date = (datetime.now() + timedelta(days=365)).date()
    result = []
    for device in devices:
        expiration_date = device.get("expiration_date")
        if expiration_date:
            expiration_date = parser.parse(expiration_date).date()
            # difference_in_days = (expiration_date - difference_date).days
            if difference_date and expiration_date <= difference_date:
                result.append(device)
            device["expiration_date"] = str(expiration_date)
        else:
            device["expiration_date"] = "-"
    devices = result
    return devices


def prepare_scheduled_report_device_report_pdf(devices, pdf_title):
    """
    Prepares a CSV file from data. data is a list of dictionaries.
    """
    html_template_file = "reports/scheduled_device_report.html"
    file_path, file_name = create_device_report_pdf(
        devices, html_template_file, pdf_title
    )
    logger.info(
        "Device Scheduled report csv file generated at path {0}".format(
            file_path
        )
    )
    return file_path, file_name


def log_report_download(
    user, report_type, report_sub_type, all_customer_report=False
):
    """
    Log report download
    Args:
        user: User model object
        report_type: report type (str)
        report_sub_type: report sub type (str)
        all_customer_report: Is the report downloaded for all customers (bool)
    """
    ReportDownloadStats.objects.create(
        user=user,
        report_type=report_type,
        report_sub_type=report_sub_type,
        all_customer_report=all_customer_report,
    )
