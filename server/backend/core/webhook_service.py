import hashlib
import json
from typing import Optional

import requests
from box import Box
from django.conf import settings
from requests.exceptions import HTTPError

from accounts.models import Provider
from core.integrations import Connectwise
from core.integrations.erp.connectwise.connectwise import ConnectwiseEntity
from project_management.services import DbOperation, ProjectPayloadHandler
from utils import get_app_logger
from core.models import DeviceInfo
from django.contrib.postgres.fields.jsonb import KeyTransform
from core.tasks.logic_monitor import logic_monitor
from exceptions import exceptions
from project_management.services import ProjectPayloadHandler
from utils import DbOperation, get_app_logger

logger = get_app_logger(__name__)


class ConnectwiseCallbackServiceException(Exception):
    def __init__(
        self, message="Connectwise Callback Service Exception occurred"
    ):
        self.message = message
        super().__init__(self.message)


class MissingConnectwiseEntityHandler(ConnectwiseCallbackServiceException):
    def __init__(self, entity, message=""):
        _msg = f"No callback handler found for the entity {entity}"
        self.message = message or _msg
        super().__init__(self.message)


class ConnectwiseCallbackService:
    @classmethod
    def _generate_provider_callback_url(cls, provider: Provider) -> str:
        """
        Generate a connectwise callback url using Provider Id and Base url
        Args:
            provider: Provider Object

        Returns: URL

        """
        base_url = settings.CONNECTWISE_CALLBACK_BASE_URL
        callback_url = f"{base_url}{provider.id}"
        return callback_url

    @classmethod
    def add_callback(
        cls,
        provider: Provider,
        entity: str,
        object_id: int,
        level: str,
        url=None,
        description: str = None,
    ) -> object:
        """
        Create a new callback entry in connectwise for the Provider
        Args:
            description: Description for the callback
            provider: Provider object
            entity: Connectwise entity E.g. Projects
            url: Callback url
            object_id: The ObjectId should be the Id of whatever
            record you are subscribing to. E.g. if level is Board,
            the selected board is Project board, the object_id will be
            id of Project Board
            level: E.g. Owner, Status, Board, Project
        """
        if not url:
            url = cls._generate_provider_callback_url(provider)
        erp_client: Connectwise = provider.erp_client
        response = erp_client.create_callback(
            description, url, object_id, entity, level
        )

        return response

    @classmethod
    def _get_signing_key(cls, url: str) -> Optional[str]:
        """
        Make GET request to the specified url and return signing_key from the payload

        """
        try:
            response = requests.get(url)
            response.raise_for_status()
        except HTTPError as http_err:
            logger.error(f"HTTP error occurred: {http_err}")
            return None
        except Exception as err:
            logger.error(f"Other error occurred: {err}")
            return None
        else:
            return response.json().get("signing_key")

    @classmethod
    def verify_callback(
        cls, callback_body: dict, content_signature: str
    ) -> bool:
        """
        Verify the incoming callback using the verification method
        https://github.com/covenanttechnologysolutions/connectwise-rest/blob/f3fb8b5825e6d7470d1b6761c7b7b0ff000482a7/src/utils/Callback.js#L64

        """
        if not any([callback_body, content_signature]):
            raise ValueError(
                "callback_body and content_signature must be defined."
            )

        key_url: str = callback_body.get("Metadata", {}).get("key_url")
        if not key_url:
            raise ValueError("key_url not found in callback_body.")
        signing_key = cls._get_signing_key(key_url)
        if not signing_key:
            raise ValueError("Couldn't get the signing_key.")
        is_valid = cls._verify_message(
            callback_body, content_signature, signing_key
        )
        if is_valid:
            logger.debug("Callback verified.")
        else:
            logger.error("Callback verification failed.")
        return is_valid

    @classmethod
    def _verify_message(cls, callback_body, content_signature, signing_key):
        hash = hashlib.sha256()
        hash.update(signing_key.encode("utf-8"))
        hash = hash.digest()
        return True
        # TODO: Complete verification logic

    @classmethod
    def parse_callback(cls, callback_payload: dict) -> object:
        """
        Parse the callback payload and return
        Args:
            callback_payload: body of the callback request from connectwise
        """
        if not callback_payload:
            raise ValueError("callback_payload is empty")
        entity = ConnectwiseEntity(callback_payload.get("entity"))
        action = DbOperation(callback_payload.get("Action"))
        entity_payload = json.loads(callback_payload.get("Entity"))
        result = Box(
            entity=entity, action=action, entity_payload=entity_payload
        )
        return result

    @classmethod
    def call_entity_callback_handler(
        cls,
        entity: ConnectwiseEntity,
        action: DbOperation,
        entity_payload: dict,
        provider: Provider,
    ) -> bool:
        """
        Call specific handler classes for the entity.
        Args:
            entity: ConnectwiseEntity object to which the callback belongs. E.g. ConnectwiseEntity.PROJECT
            action: Action which triggered the callback. Can be Create/Update/Delete
            entity_payload: Body of the callback request
            provider: Provider object
        """
        entity_callback_handlers = {
            ConnectwiseEntity.PROJECT: ProjectPayloadHandler
        }
        if entity not in entity_callback_handlers:
            raise MissingConnectwiseEntityHandler(entity)
        res, errors = entity_callback_handlers[entity](
            provider, action, entity_payload
        ).process()
        if res:
            logger.debug(
                f"{action} action performed successfully for {entity} entity with "
                f"CRM ID {entity_payload['id']}"
            )
        else:
            logger.debug(
                f"{action} action failed for {entity} entity with "
                f"CRM ID {entity_payload['id']}"
            )
            logger.debug(
                f"{action} the Project status for Company name {entity_payload.get('company').get('name')} "
                f"coming status from connectWise is {entity_payload.get('status')} and project closed status is {entity_payload.get('closedFlag')}"
            )
            logger.debug(f"Errors: {errors}")
        return res

    @classmethod
    def process(cls, provider, payload):
        actions = {
            "updated": DbOperation.UPDATE,
            "added": DbOperation.CREATE,
            "deleted": DbOperation.DELETE,
        }
        action = actions.get(payload.get("Action"))
        entity_str = payload.get("Entity")
        if entity_str:
            entity_payload = json.loads(entity_str)
        else:
            entity_payload = {"id": [payload["ID"]]}
        entity = ConnectwiseEntity(payload.get("Type"))
        res = cls.call_entity_callback_handler(
            entity, action, entity_payload, provider
        )
        return res


class LogicMonitorCallbackService:
    @classmethod
    def process(cls, provider, payload):
        """
        provider: provider
        payload: payload will be in string
        parse the payload and take the device id from it
        """
        # payload = 'agentid=4 deviceurl=https:\/\/lookingpoint.logicmonitor.com\/santaba\/uiv3\/device\/index.jsp#tree\/-d-900 host=LP-PH-BLUESTEEL-SW1'
        res = dict(
            map(str.strip, sub_str.split("=", 1))
            for sub_str in payload.split(" ")
            if "=" in sub_str
        )

        if res.get("deviceurl"):
            device_id = int(res.get("deviceurl").split("d-")[1])
            response = False
            try:
                device_info = (
                    DeviceInfo.objects.filter(provider=provider)
                    .annotate(
                        lm_device_id=KeyTransform(
                            "device_id",
                            KeyTransform("logicmonitor", "config_meta_data"),
                        )
                    )
                    .filter(lm_device_id=device_id)
                )
                # ).get(lm_device_id=device_id)
                try:
                    for device_id in device_info:
                        logic_monitor.sync_config_for_logic_monitor_device_id(
                            provider.id, device_id.id
                        )
                    response = True
                except exceptions.NotFoundError as e:
                    logger.info(e)
            except DeviceInfo.DoesNotExist:
                logger.debug(
                    f"Device Info data does not exist for Device ID {device_id}"
                )
                pass
        return response
