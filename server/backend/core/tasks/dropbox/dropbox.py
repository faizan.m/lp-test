import os.path
import os
import dropbox
import requests
from celery import shared_task
import urllib.parse
from celery.utils.log import get_task_logger
from accounts.models import Provider, Customer
from core.services import DropboxIntegrationService
from core.integrations.file_storage.dropbox.DropBoxClient import DropBoxClient
from django.conf import settings
from accounts.models import Customer
from core.integrations.utils import generate_timestamped_file_name

logger = get_task_logger(__name__)


@shared_task
def sync_file_storage_for_providers():
    """
    Call task to sync file storage directory structure for customer from connectwise for each Provider.
    """
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            batch_create_folders_for_customers_file_storage.delay(provider.id)


@shared_task
def batch_create_folders_for_customers_file_storage(provider_id):
    logger.info(
        "Batch creating folders for customers of provider with id: {}".format(
            provider_id
        )
    )
    provider = Provider.objects.get(id=provider_id)
    customer_id_name_tuples = Customer.objects.filter(
        provider_id=provider_id
    ).values_list("crm_id", "name")
    DropboxIntegrationService.create_folders(
        provider, list(customer_id_name_tuples)
    )


@shared_task
def upload_device_import_file_to_dropbox(
    provider_id: int, customer_crm_id: int, file_path: str
) -> None:
    """
    A task to upload the device import file of a customer to dropbox.
    :param provider_id: Primary Key of Provider.
    :param customer_crm_id: Crm id of the customer
    :param file_path: Url of uploaded file on S3 on production OR path of file on local setup.
    :return:
    """
    if not settings.DEBUG:
        response = requests.get(file_path)
        file_content = response.content
        parsed_url = urllib.parse.urlparse(file_path)
        file_name = urllib.parse.unquote(os.path.basename(parsed_url.path))
    else:
        file_name = urllib.parse.unquote(os.path.basename(file_path))
        with open(file_path, "rb") as f:
            file_content = f.read()

    file_name = generate_timestamped_file_name(file_name)
    try:
        provider = Provider.objects.get(id=provider_id)
        customer = Customer.objects.get(
            provider=provider, crm_id=customer_crm_id
        )
    except Customer.DoesNotExist:
        logger.debug(f"No customer exist with {customer_crm_id=}")
        return
    except Provider.DoesNotExist:
        logger.debug(f"No Provider exist with {provider_id=}")
        return
    dropbox_client = DropboxIntegrationService.get_storage_api_client(provider)
    dropbox_path = dropbox_client.get_customer_general_folder_path(
        customer, file_name
    )
    try:
        dropbox_client.upload_file(file_content, dropbox_path)
        logger.info(
            f"Uploading device import file for customer: {customer.name} to Dropbox"
        )
    except Exception as err:
        logger.error(err)


@shared_task
def upload_file_to_dropbox(
    provider_id: int,
    customer_id: int,
    local_file_path: str,
    customer_sub_folder: str,
):
    """
    Function to upload a file to a specified customer sub-folder in Dropbox.

    Args:
        provider_id: ID of provider
        customer_id: ID of customer
        local_file_path: Local file path of file to be uploaded
        customer_sub_folder: Sub-folder to which file needs to be uploaded

    Returns: None

    """
    try:
        provider = Provider.objects.get(id=provider_id)
    except Provider.DoesNotExist:
        logger.debug(f"Provider with given {provider_id=} does not exist!")
        return

    try:
        customer = Customer.objects.get(id=customer_id)
    except Customer.DoesNotExist:
        logger.debug(f"Customer with given {customer_id=} does not exist!")
        return

    file_name = os.path.basename(local_file_path)

    try:
        with open(local_file_path, "rb") as file:
            file_content = file.read()
    except (FileNotFoundError, OSError) as err:
        logger.error(err)
        return

    dropbox_client = DropboxIntegrationService.get_storage_api_client(provider)
    customer_folder_name = dropbox_client.get_dropbox_customer_folder_name(
        customer
    )
    dropbox_path = f"/{dropbox_client.APP_FOLDER_NAME}/{customer_folder_name}/{customer_sub_folder}/{file_name}"

    try:
        dropbox_client.upload_file(file_content, dropbox_path)
        logger.info(f"Uploading {file_name=} to Dropbox")
    except Exception as err:
        logger.error(err)
        return
