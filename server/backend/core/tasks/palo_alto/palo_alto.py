from core.tasks.base import ManufacturerTaskBase


class PaloAltoManufacturer(ManufacturerTaskBase):
    def sync_device_data_with_priority(
        self,
        provider_id,
        manufacturer_id,
        serial_numbers,
        update_missing_data_table,
        priority_task,
    ):
        pass

    def sync_manufacturer_data_for_serial_numbers(
        self, provider_id, manufacturer_id, serial_numbers
    ):
        pass

    def fetch_all_manufacturer_data(self, provider_id):
        pass
