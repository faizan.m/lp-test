from accounts.models import Provider
from celery.utils.log import get_task_logger
from core.models import IntegrationType
from core.tasks.cisco.cisco import CiscoManufacturer
from core.tasks.palo_alto.palo_alto import PaloAltoManufacturer

logger = get_task_logger(__name__)

CLASS_MAPPING = {"CISCO": CiscoManufacturer, "PALO_ALTO": PaloAltoManufacturer}


class DeviceManufacturerIntegrations(object):
    @classmethod
    def get_cisco_integration_type_object(cls):
        return IntegrationType.objects.get(name="CISCO")

    @classmethod
    def get_palo_alto_integration_type_object(cls):
        return IntegrationType.objects.get(name="PALO_ALTO")

    @classmethod
    def get_integration_type_object_by_id(cls, id):
        return IntegrationType.objects.get(id=id)


class TaskService(object):
    DEVICE_MANUFACTURERS = ["CISCO", "PALO_ALTO"]

    @classmethod
    def sync_device_data_on_create(
        cls,
        provider_id,
        manufacturer_id,
        serial_numbers,
        update_missing_data_table=False,
        priority_task=False,
    ):
        integration_type = (
            cls._get_integration_type_for_provider_by_manufacturer(
                provider_id, manufacturer_id
            )
        )
        if integration_type:
            mnf_client = cls._get_manufacturer_client(integration_type.name)
            mnf_client.sync_device_data_with_priority(
                provider_id,
                manufacturer_id,
                serial_numbers,
                update_missing_data_table,
                priority_task,
            )

    @classmethod
    def sync_device_data(cls, provider_id, manufacturer_id, serial_numbers):
        integration_type = (
            cls._get_integration_type_for_provider_by_manufacturer(
                provider_id, manufacturer_id
            )
        )
        if integration_type:
            mnf_client = cls._get_manufacturer_client(integration_type.name)
            mnf_client.sync_manufacturer_data_for_serial_numbers(
                provider_id, manufacturer_id, serial_numbers
            )

    @classmethod
    def sync_all_manufacturer_data(cls, provider_id):
        for _type in cls.DEVICE_MANUFACTURERS:
            mnf_client = cls._get_manufacturer_client(_type)
            mnf_client.fetch_all_manufacturer_data(provider_id)

    @classmethod
    def _get_device_category_id_by_integration_type(cls, provider_id, type):
        provider = Provider.objects.get(id=provider_id)
        manufacturer_mapping = provider.get_manufacturer_mapping()
        if type == "CISCO":
            integration_type = (
                DeviceManufacturerIntegrations.get_cisco_integration_type_object
            )
        elif type == "PALO_ALTO":
            integration_type = (
                DeviceManufacturerIntegrations.get_palo_alto_integration_type_object
            )
        else:
            return None
        try:
            integration_type = integration_type()
            for key, value in manufacturer_mapping.items():
                if (
                    value.get("acela_integration_type_id")
                    == integration_type.id
                ):
                    return value.get("device_category_id")
        except IntegrationType.DoesNotExist:
            return None

    @classmethod
    def _get_manufacturer_client(self, type):
        manufacturer_class = CLASS_MAPPING.get(type)
        mnf_client = manufacturer_class()
        return mnf_client

    @classmethod
    def _get_integration_type_for_provider_by_manufacturer(
        cls, provider_id, manufacturer_id
    ):
        provider = Provider.objects.get(id=provider_id)
        manufacturer_map = provider.get_manufacturer_map_by_id(manufacturer_id)
        acela_integration_type_id = manufacturer_map.get(
            "acela_integration_type_id", None
        )
        if acela_integration_type_id:
            integration_type = DeviceManufacturerIntegrations.get_integration_type_object_by_id(
                acela_integration_type_id
            )
            return integration_type
        return None
