"""
This module contains Base class for Manufacturer
"""
from abc import ABC, abstractmethod


class ManufacturerTaskBase(ABC):
    def __init__(self):
        super(ManufacturerTaskBase, self).__init__()

    @abstractmethod
    def sync_device_data_with_priority(
        self,
        provider_id,
        manufacturer_id,
        serial_numbers,
        update_missing_data_table,
        priority_task,
    ):
        pass

    @abstractmethod
    def sync_manufacturer_data_for_serial_numbers(
        self, provider_id, manufacturer_id, serial_numbers
    ):
        pass

    @abstractmethod
    def fetch_all_manufacturer_data(self, provider_id):
        """
        This method fetches all the manufacturer data
        """
        pass
