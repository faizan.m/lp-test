from datetime import datetime
from typing import Any, List, Dict, Set, Tuple

from celery import chain, shared_task
from celery.utils.log import get_task_logger
from django.contrib.contenttypes.models import ContentType
from django.db import transaction, IntegrityError
from collections import namedtuple
from accounts.groups import update_user_object_permissions
from accounts.models import (
    Provider,
    Customer,
    Address,
    Profile,
    User,
    UserType,
    ModelSyncMeta,
)
from core.connectwise_communication_item import (
    ConnectwiseCommunicationItemsService,
)
from document.models import FireReportPartnerSite
from core.exceptions import InvalidConfigurations
from core.integrations.erp.connectwise.connectwise import (
    CWConditions,
    Condition,
    Operators,
)
from core.models import DeviceManufacturerData
from core.services import DropboxIntegrationService
from core.utils import (
    group_contacts_by_company_id,
    group_list_of_dictionary_by_key,
)
from exceptions.exceptions import APIError
from document.tasks.fire_report import create_partner_site_for_new_customer
logger = get_task_logger(__name__)


@shared_task
def sync_customers(provider_id: int, full_sync: bool = False):
    """
    Task to fetch updates for customers from connectwise based on the last update date.
    If customer object is already present in local db, it will be updated else a new customer object is
    created.
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    customer_model_content_type = ContentType.objects.get_for_model(Customer)
    filters: List = []
    if not full_sync:
        last_updated = get_and_update_model_sync_meta_info(
            customer_model_content_type, provider_id
        )
        if last_updated:
            last_updated = datetime.strftime(
                last_updated, "%Y-%m-%dT%H:%M:%SZ"
            )
            last_updated_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "_info/lastUpdated",
                        Operators.GREATER_THEN_EQUALS,
                        last_updated,
                    )
                ]
            )
            filters = [last_updated_filter]
    # Fetch updates for customer.
    customers: List[Dict] = provider.erp_client.get_customers(
        full_info=True, filters=filters
    )
    customers_in_db = Customer.objects.filter(
        provider_id=provider_id
    ).values_list("crm_id", flat=True)
    customers_to_be_updated: List[Dict] = list(
        filter(
            lambda customer: customer.get("id") in customers_in_db, customers
        )
    )
    update_customers.delay(provider_id, customers_to_be_updated)
    customers_to_be_created: List[Dict] = list(
        filter(
            lambda customer: customer.get("id") not in customers_in_db,
            customers,
        )
    )
    create_customers.delay((provider_id, customers_to_be_created))


@shared_task
def update_customers(
    provider_id: int, customers: List[Dict[str, Any]]
) -> None:
    # call update method for all customers.
    for customer in customers:
        update_customer(provider_id, customer.get("id"), customer)


def update_customer(provider_id, customer_crm_id, payload):
    # Update Customer
    try:
        customer = (
            Customer.objects.filter(provider_id=provider_id)
            .select_related("address")
            .get(crm_id=customer_crm_id)
        )
    except Customer.DoesNotExist:
        logger.info(
            f"Customer with crm_id {customer_crm_id} already updated because of duplicate customer name"
        )
        return
    old_customer_name = customer.name
    customer.name = payload.get("name")
    customer.phone = payload.get("phone")
    customer.primary_contact = payload.get("primary_contact")
    customer.account_manager_id = payload.get("territory_manager_id", None)
    customer.account_manager_name = payload.get("territory_manager_name", None)
    customer.is_deleted = payload.get("is_deleted")
    ms_customer_field = get_provider_ms_customer_field_mapping(provider_id)
    if ms_customer_field and payload.get("custom_fields"):
        for custom_field in payload.get("custom_fields"):
            if ms_customer_field == custom_field.get("caption"):
                if custom_field.get("value"):
                    customer.is_ms_customer = custom_field.get("value")
                    break
    updated = save_object(customer)

    if updated:
        # Update Customer Address
        address = customer.address
        address.address_1 = payload.get("address_line_1", "N.A.")
        address.address_2 = payload.get("address_line_2")
        address.city = payload.get("city")
        address.state = payload.get("state")
        address.zip_code = payload.get("zip")
        address.save()

        # If customer name is updated then rename the folder structure in file storage
        if old_customer_name != customer.name:
            file_storage_rename_customer_name_folder(
                customer.id, old_customer_name
            )

        logger.debug(f"Customer {customer} updated.")


@shared_task
def cw_customer_user_full_sync(provider_id):
    """
    Runs sync operation for complete data from connectwise.
    Hard deletes the user objects deleted from connectwise.
    :param provider_id: Provider Id
    :return: None
    """
    provider = Provider.objects.get(id=provider_id)

    # Update Model sync Meta entry for User model
    user_model_content_type = ContentType.objects.get_for_model(User)
    get_and_update_model_sync_meta_info(user_model_content_type, provider_id)

    # Get all customer users from connectwise
    customer_users_from_cw = provider.erp_client.get_customer_users(
        active_users_only=True
    )
    customers_users_in_db = User.customers.filter(
        provider_id=provider_id
    ).values_list("crm_id", flat=True)
    grouped_users_by_customer_id = group_contacts_by_company_id(
        customer_users_from_cw
    )

    # For each customer delete Users with crm ids not in the list of crm ids coming from
    # connectwise.
    for (
        customer_crm_id,
        customer_users,
    ) in grouped_users_by_customer_id.items():
        currently_available_users = [user.get("id") for user in customer_users]
        User.customers.filter(customer__crm_id=customer_crm_id).exclude(
            crm_id__in=currently_available_users
        ).delete()

    # Update existing Users
    users_to_be_updated = list(
        filter(
            lambda user: user.get("id") in customers_users_in_db,
            customer_users_from_cw,
        )
    )
    update_customer_users.delay(users_to_be_updated, provider_id)

    # Create new Users
    users_to_be_created = list(
        filter(
            lambda user: user.get("id") not in customers_users_in_db,
            customer_users_from_cw,
        )
    )
    grouped_users_by_customer_id = group_contacts_by_company_id(
        users_to_be_created
    )
    args = [
        (provider_id, crm_id, grouped_users_by_customer_id.get(crm_id, []))
        for crm_id in list(grouped_users_by_customer_id.keys())
    ]
    create_customer_user.chunks(args, 10).delay()


def update_customer_user(provider_id, user_crm_id, payload):
    user = User.objects.filter(
        crm_id=user_crm_id, provider_id=provider_id
    ).first()
    try:
        provider = Provider.objects.get(id=provider_id)
    except Provider.DoesNotExist:
        logger.info(
            f"No provider found with { provider_id= }.Aborting update_customer_user."
        )
        return
    # Update User Object
    if payload.get("email"):
        user.email = payload.get("email")
        user.first_name = payload.get("first_name")
        user.last_name = payload.get("last_name")
        user.phone_number = payload.get("phone")
        user.is_active_in_crm = not payload.get("is_inactive_in_crm")
        # This logic is for when the customer is updated on CW. Updating the user's customer and
        # then updating the permissions
        is_customer_updated = False
        if user.customer.crm_id != payload.get("customer_id"):
            customer = Customer.objects.get(
                provider_id=provider_id, crm_id=payload.get("customer_id")
            )
            user.customer = customer
            is_customer_updated = True
        updated = save_object(user)
        if is_customer_updated:
            logger.info(f"Customer updated for User {user.email}")
            update_user_object_permissions(user)
        if updated:
            (
                cell_phone_number,
                cell_phone_number_crm_id,
                office_phone,
                office_phone_crm_id,
            ) = (None, None, None, None)
            user_phones = ConnectwiseCommunicationItemsService(
                provider=provider
            ).get_user_phones(payload.get("phone_numbers"))
            if user_phones:
                cell_phone_number = user_phones.cell_phone_number
                cell_phone_number_crm_id = user_phones.cell_phone_number_id
                office_phone = user_phones.office_phone
                office_phone_crm_id = user_phones.office_phone_id
            # Update User Profile

            try:
                profile = user.profile
                profile.title = payload.get("title")
                profile.department = payload.get("department")
                profile.cell_phone_number = cell_phone_number
                profile.cell_phone_number_crm_id = cell_phone_number_crm_id
                profile.office_phone = office_phone
                profile.office_phone_crm_id = office_phone_crm_id
                profile.linkedin_profile_url = payload.get(
                    "linkedin_profile_url"
                )
                profile.twitter_profile_url = payload.get(
                    "twitter_profile_url"
                )
                profile.save()
                logger.debug(f"User {user} updated.")
            except User.DoesNotExist:
                logger.debug("User {user.email} has no profile.")


@shared_task
def update_customer_users(users, provider_id):
    for user in users:
        update_customer_user(provider_id, user.get("id"), user)


@shared_task
def sync_customer_users(provider_id, full_sync=False):
    """
    Task to fetch updates for customer users from connectwise based on the last update date.
    If customer user object is already present in local db, it will be updated else a new customer object is
    created.
    """
    # delete inactive Users
    User.customers.filter(is_active_in_crm=False).delete()
    if full_sync:
        cw_customer_user_full_sync.delay(provider_id)
        return
    provider = Provider.objects.get(id=provider_id)
    user_model_content_type = ContentType.objects.get_for_model(User)
    filters = []
    last_updated = get_and_update_model_sync_meta_info(
        user_model_content_type, provider_id
    )
    if last_updated:
        last_updated = datetime.strftime(last_updated, "%Y-%m-%dT%H:%M:%SZ")
        last_updated_filter = CWConditions(
            datetime_conditions=[
                Condition(
                    "_info/lastUpdated",
                    Operators.GREATER_THEN_EQUALS,
                    last_updated,
                )
            ]
        )
        filters = [last_updated_filter]

    customer_users_from_cw = provider.erp_client.get_customer_users(
        filters=filters, active_users_only=True
    )
    customers_users_in_db = User.customers.filter(
        provider_id=provider_id
    ).values_list("crm_id", flat=True)
    # Update existing Users
    users_to_be_updated = list(
        filter(
            lambda user: user.get("id") in customers_users_in_db,
            customer_users_from_cw,
        )
    )
    update_customer_users.delay(users_to_be_updated, provider_id)

    # Create new Users
    users_to_be_created = list(
        filter(
            lambda user: user.get("id") not in customers_users_in_db,
            customer_users_from_cw,
        )
    )
    grouped_users_by_customer_id = group_contacts_by_company_id(
        users_to_be_created
    )
    args = [
        (provider_id, crm_id, grouped_users_by_customer_id.get(crm_id, []))
        for crm_id in list(grouped_users_by_customer_id.keys())
    ]
    create_customer_user.chunks(args, 10).delay()

    # Delete Users
    all_customer_user_ids = provider.erp_client.get_customer_users_crm_ids()
    users_to_be_deleted = list(
        filter(
            lambda crm_id: crm_id not in all_customer_user_ids,
            customers_users_in_db,
        )
    )
    User.customers.filter(
        provider_id=provider_id, crm_id__in=users_to_be_deleted
    ).update(is_deleted=True, is_active_in_crm=False)


@shared_task
def cw_data_synchronization_task(*args):
    """
    Call task to sync customer and users from connectwise for each Provider
    """
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            sync_customers.delay(provider.id, *args)
            sync_customer_users.delay(provider.id, *args)


@shared_task
def cw_customer_data_full_sync():
    """
    Task to trigger full sync of customers and customer users.
    """
    providers: "QuerySet[Provider]" = Provider.objects.filter(is_active=True)
    for provider in providers:
        if provider.is_configured:
            sync_customers.delay(provider.id, full_sync=True)
            sync_customer_users.delay(provider.id, full_sync=True)


@shared_task
def fetch_provider_customers(provider_id):
    """
    fetches all Customers for provider from erp.
    :param provider_id: Id of Provider object
    :return:
    """
    try:
        provider = Provider.objects.get(id=provider_id)
        logger.info(f"Fetching {Provider} Customers.")
        deleted_company_filter = CWConditions(
            boolean_conditions=[
                Condition("deletedFlag", Operators.EQUALS, "false")
            ]
        )
        customers = provider.erp_client.get_customers(
            full_info=True, filters=[deleted_company_filter]
        )
        logger.info(f"{len(customers)} Customers fetched for Provider.")
        args = (provider_id, customers)
        return args
    except Provider.DoesNotExist:
        raise ValueError("Invalid provider_id")


@shared_task
def create_folders_for_customer_file_storage(
    provider_id, customer_crm_id, customer_name
):
    logger.info("Creating folders for customer {}".format(customer_name))
    provider = Provider.objects.get(id=provider_id)
    customer_crm_id_name_tuple = [(customer_crm_id, customer_name)]
    DropboxIntegrationService.create_folders(
        provider, customer_crm_id_name_tuple
    )


@shared_task
def create_customers(args):
    """
    Creates customer address and customer object and saves it to db.
    :return:
    """
    provider_id, customers = args
    provider = Provider.objects.get(id=provider_id)
    customer_crm_ids = []
    for customer in customers:
        address = Address(
            address_1=customer.get("address_line_1", "N.A."),
            address_2=customer.get("address_line_2"),
            city=customer.get("city"),
            state=customer.get("state"),
            zip_code=customer.get("zip"),
        )
        address.save()
        logger.debug(f"Address: {address} created.")
        ms_customer_field = get_provider_ms_customer_field_mapping(provider_id)
        is_ms_customer = False  # Default set is_ms_customer to False
        if ms_customer_field and customer.get("custom_fields"):
            for custom_field in customer.get("custom_fields"):
                if ms_customer_field == custom_field.get("caption"):
                    if custom_field.get("value"):
                        is_ms_customer = custom_field.get("value")
        customer_object = Customer(
            crm_id=customer.get("id"),
            name=customer.get("name"),
            address=address,
            phone=customer.get("phone"),
            provider=provider,
            primary_contact=customer.get("primary_contact"),
            is_ms_customer=is_ms_customer,
            account_manager_id=customer.get("territory_manager_id", None),
            account_manager_name=customer.get("territory_manager_name", None),
            is_deleted=customer.get("is_deleted", False),
        )
        saved = create_object(customer_object)
        if saved:
            logger.debug(f"Customer: {customer} created.")
            customer_crm_ids.append(customer_object.crm_id)
            create_folders_for_customer_file_storage.delay(
                provider_id, customer_object.crm_id, customer_object.name
            )
            create_partner_site_for_new_customer.apply_async(
                (provider_id, customer_object.crm_id), queue="high"
            )
        else:
            address.delete()
    logger.info("Finished adding customers to Database.")
    args = (provider_id, customer_crm_ids)
    return args


@shared_task
def get_customer_users(args):
    """
    Fetch Customer Contacts and call Create DB Object.
    :param args:
    :return:
    """
    provider_id, customer_crm_ids = args
    provider = Provider.objects.get(id=provider_id)
    inactive_filter = CWConditions(
        boolean_conditions=[Condition("inactiveFlag", Operators.EQUALS, False)]
    )
    all_customer_contacts = provider.erp_client.get_customer_users(
        filters=[inactive_filter]
    )
    all_contacts_grouped = group_contacts_by_company_id(all_customer_contacts)
    args = [
        (provider_id, crm_id, all_contacts_grouped.get(crm_id, []))
        for crm_id in customer_crm_ids
    ]
    create_customer_user.chunks(args, 10).delay()


@shared_task
def create_customer_user(provider_id, customer_crm_id, contacts):
    """
    Creates Customer User object and saves it to database.
    :param provider_id:
    :param customer_crm_id:
    :param contacts:
    :return:
    """
    provider = Provider.objects.get(id=provider_id)
    user_type = UserType.objects.get(name=UserType.CUSTOMER)
    try:
        customer = Customer.objects.get(
            provider_id=provider_id, crm_id=customer_crm_id
        )
    except Customer.DoesNotExist:
        return None
    logger.info(f"{customer_crm_id} : {contacts}")
    if contacts:
        logger.info(f"Fetched {len(contacts)} contacts.")
        logger.info(
            f"Adding Contacts for Company with CRM ID: {customer_crm_id}"
        )
    for contact in contacts:
        if contact.get("email"):
            # Create User object
            contact_obj = User(
                email=contact.get("email"),
                first_name=contact.get("first_name"),
                last_name=contact.get("last_name"),
                crm_id=contact.get("id"),
                provider=provider,
                customer=customer,
                user_type=user_type,
                is_active_in_crm=not contact.get("is_inactive_in_crm"),
            )
            created = save_object(contact_obj)
            # create Profile Object
            if created:
                (
                    cell_phone_number,
                    cell_phone_number_crm_id,
                    office_phone,
                    office_phone_crm_id,
                ) = (None, None, None, None)
                user_phones = ConnectwiseCommunicationItemsService(
                    provider=provider
                ).get_user_phones(contact.get("phone_numbers"))
                if user_phones:
                    cell_phone_number = user_phones.cell_phone_number
                    cell_phone_number_crm_id = user_phones.cell_phone_number_id
                    office_phone = user_phones.office_phone
                    office_phone_crm_id = user_phones.office_phone_id
                profile = Profile(
                    user=contact_obj,
                    title=contact.get("title"),
                    department=contact.get("department"),
                    cell_phone_number=cell_phone_number,
                    cell_phone_number_crm_id=cell_phone_number_crm_id,
                    office_phone=office_phone,
                    office_phone_crm_id=office_phone_crm_id,
                    linkedin_profile_url=contact.get("linkedin_profile_url"),
                    twitter_profile_url=contact.get("twitter_profile_url"),
                )
                profile.save()
                logger.debug(f"Contact {contact_obj} created.")
    logger.info("Contacts added to database.")


@shared_task()
def batch_update_crm_device_data(provider_id, payload):
    """
    Calls method to bulk update device data
    :param provider_id:
    :param payload:
    :return:
    """
    provider = Provider.objects.get(id=provider_id)
    provider.erp_client.batch_update_devices(payload)


@shared_task
def sync_connectwise_device_data(provider_id):
    """
    Task to sync Device data from Device manufacturer table to connectwise.
    """
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        device_manufacturer_data = {
            sn.device_serial_number: sn.data
            for sn in DeviceManufacturerData.objects.filter(
                provider_id=provider_id
            )
        }
        available_data_serial_numbers = list(device_manufacturer_data.keys())
        required_fields = ["id", "serialNumber"]
        device_crm_ids = provider.erp_client.get_devices(
            customer_id=None,
            category_id=provider.get_all_device_categories(),
            required_fields=required_fields,
        )
        device_crm_ids = list(
            filter(
                lambda item: item.get("serial_number")
                in available_data_serial_numbers,
                device_crm_ids,
            )
        )
        device_crm_ids_by_serial_number = group_list_of_dictionary_by_key(
            device_crm_ids, "serial_number"
        )
        logger.debug(
            "Device manufacturer data available for {0} devices.".format(
                len(device_crm_ids_by_serial_number)
            )
        )
        for (
            serial_number,
            device_crm_ids_list,
        ) in device_crm_ids_by_serial_number.items():
            data = device_manufacturer_data.get(serial_number)

            payload = dict(
                expiration_date=data.get("search_data", {}).get(
                    "expiration_date"
                ),
                service_contract_number=data.get("search_data", {}).get(
                    "service_contract_number"
                ),
                product_id=data.get("search_data", {}).get("product_id"),
            )

            for entry in device_crm_ids_list:
                logger.debug(
                    "Updating device with serial number {0}. Updated payload {1}".format(
                        serial_number, payload
                    )
                )
                provider.erp_client.update_device(entry.get("id"), payload)


@shared_task
def sync_cw_device_data():
    """
    Periodic Task to trigger CW device data sync task.
    :return:
    """
    for provider in Provider.objects.filter(is_active=True):
        if provider.is_configured:
            logger.info(
                "Periodic CW device data sync task started for provider {0}".format(
                    provider
                )
            )
            sync_connectwise_device_data.delay(provider.id)


@shared_task
def file_storage_rename_customer_name_folder(customer_id, old_customer_name):
    DropboxIntegrationService.rename_customer_folder_name(
        customer_id, old_customer_name
    )


def fetch_and_create_customer_and_users_for_provider(*args):
    """
    Call Async tasks to get customer and customer user data and store in db.
    :param args:
    :return:
    """
    fetch_and_create_customers_and_users = chain(
        fetch_provider_customers.s(*args)
        | create_customers.s()
        | get_customer_users.s()
    )
    return fetch_and_create_customers_and_users.delay()


def save_object(model_object):
    try:
        with transaction.atomic():
            model_object.save()
            return True
    except IntegrityError as e:
        logger.info(
            {"customerSync": f"Customer Sync created/update Failed:: {e}"}
        )
        logger.debug(f"Duplicate object {model_object}")
        return False


def create_object(model_object):
    """
    object to create customer and handle case when duplicate name customer found
    """
    try:
        with transaction.atomic():
            model_object.save()
            return True
    except IntegrityError as e:
        Customer.objects.filter(
            provider=model_object.provider, name=model_object.name
        ).update(crm_id=model_object.crm_id)
        logger.info(
            {"customerSync": f"Customer Sync created/update Failed:: {e}"}
        )
        logger.debug(f"Duplicate object {model_object}")
        return False


def get_and_update_model_sync_meta_info(content_type, provider_id):
    try:
        last_update_meta = ModelSyncMeta.objects.get(
            content_type=content_type, provider_id=provider_id
        )
        last_updated = last_update_meta.last_update_datetime
    except ModelSyncMeta.DoesNotExist:
        last_updated = None

    # Update Model Sync Meta
    ModelSyncMeta.objects.update_or_create(
        content_type=content_type, provider_id=provider_id, defaults={}
    )
    return last_updated


def get_provider_ms_customer_field_mapping(provider_id):
    provider = Provider.objects.get(id=provider_id)
    if not provider.integration_statuses["crm_board_mapping_configured"]:
        raise InvalidConfigurations("Board Mapping Not Configured.")
    ms_customer_mapping = (
        provider.erp_integration.other_config.get("board_mapping")
        .get("Custom Fields")
        .get("ms_customer")
    )
    if ms_customer_mapping is None:
        logger.error(f"MS Customer Field mapping Not Configured")
    return ms_customer_mapping
