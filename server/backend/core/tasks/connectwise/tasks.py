import csv
import os
import shutil
import tempfile
import datetime
import traceback
from typing import Dict

import numpy as np
import pandas as pd
import requests
from django.conf import settings

from celery import shared_task
from django.core.exceptions import ValidationError
from django.db.models.functions import Lower, Upper

from accounts.models import Provider, Customer
from caching.service import CacheService
from core.models import DeviceInfo, SubscriptionData
from core.tasks.services import TaskService
from core.utils import (
    group_list_of_dictionary_by_key,
    convert_to_date_from_date_format,
    generate_random_id,
)

# Get an instance of a logger
from storage.file_upload import FileUploadService
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task
def bulk_update_devices_by_value(
    provider_id, customer_id, device_ids, value, field_name
):
    provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    data = erp_client.update_device_by_value(
        device_ids=device_ids, value=value, field_name=field_name
    )
    CacheService.invalidate_data(
        CacheService.CACHE_KEYS.DEVICE_LIST.format(provider_id, customer_id)
    )
    return data


@shared_task
def bulk_update_devices_categories(
    provider_id, customer_id, device_ids, category_id
):
    provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    old_devices = erp_client.get_device_by_ids(device_ids)
    data = erp_client.batch_add_devices(customer_id, category_id, old_devices)
    CacheService.invalidate_data(
        CacheService.CACHE_KEYS.DEVICE_LIST.format(provider_id, customer_id)
    )
    return data


@shared_task
def batch_create_or_update_devices(
    provider_id: int, customer_id: int, data: Dict, file_name: str
) -> Dict:
    provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    try:
        devices_data, invalid_index_errors = parse_device_import_file(
            data, file_name, provider
        )
        # device_name_to_row_mapping is at last index of devices_data
        device_name_to_row_mapping: Dict[str, int] = devices_data.pop()
        if devices_data is None:
            return {}
        na_serial_numbers = set(
            group_list_of_dictionary_by_key(
                invalid_index_errors, "serial_number"
            )
        )
    except Exception as e:
        logger.error("Error in device import file: {0}".format(e))
        raise ValidationError({"import_file": ["Invalid file format."]})
    result, _, cw_err_response = bulk_create_devices(
        provider_id,
        erp_client,
        customer_id,
        data.get("manufacturer_id"),
        devices_data,
        na_serial_numbers,
        mapping=device_name_to_row_mapping,
    )
    from core.tasks.dropbox.dropbox import upload_device_import_file_to_dropbox

    upload_device_import_file_to_dropbox.apply_async(
        (provider_id, customer_id, file_name),
        queue="high",
    )
    result = prepare_device_import_response(
        devices_data, invalid_index_errors, result, cw_err_response
    )
    CacheService.invalidate_data(
        CacheService.CACHE_KEYS.DEVICE_LIST.format(provider_id, customer_id)
    )
    return result


def prepare_device_import_response(
    devices_data, invalid_index_errors, result, cw_err_response
):
    failed_created_items_count = result.get("failed_created_items_count", 0)
    result["failed_created_items_count"] = failed_created_items_count + len(
        invalid_index_errors
    )
    result["total_request_items_count"] = len(devices_data) + len(
        invalid_index_errors
    )
    if invalid_index_errors or cw_err_response:
        import_warnings_file_info = generate_file_import_error_csv(
            invalid_index_errors, cw_err_response
        )
    else:
        import_warnings_file_info = {}
    if import_warnings_file_info.get(
        "file_path"
    ) and import_warnings_file_info.get("file_name"):
        import_warnings_file_path = import_warnings_file_info.get("file_path")
        import_warnings_file_name = import_warnings_file_info.get("file_name")
        url = FileUploadService.upload_file(import_warnings_file_path)
        result["import_warnings_file_info"] = dict(
            file_path=url, file_name=import_warnings_file_name
        )
    return result


def bulk_create_devices(
    provider_id,
    erp_client,
    customer_id,
    manufacturer_id,
    devices_data,
    na_serial_numbers=None,
    category_id=None,
    **kwargs,
):
    """
    Takes list of devices to be created. Creates devices in connectwise, updates DeviceInfo table,
    call APIs to update device data
    Returns:
        Dict of the form:
        result = dict(
            successfully_updated_items_count=20,
            successfully_created_items_count=10,
            failed_updated_items_count=2,
            failed_created_items_count=3,
        )
    """
    na_serial_numbers = na_serial_numbers or []
    serial_number_instance_number_mapping = {}

    # Add manufacturer_id, customer_id, category_id to the device_data
    for device in devices_data:
        _dict = dict(manufacturer_id=manufacturer_id, customer_id=customer_id)
        serial_number_instance_number_mapping[
            device.get("serial_number")
        ] = device.get("instance_id")
        if category_id:
            _dict["category_id"] = category_id
        device.update(_dict)

    # call connectwise API to create devices in connectwise
    (
        result,
        serial_numbers_crm_id_mapping,
        cw_errors,
    ) = erp_client.batch_add_devices(
        customer_id, category_id, devices_data, **kwargs
    )
    serial_numbers = list(serial_numbers_crm_id_mapping.keys())

    if serial_numbers and devices_data and provider_id:
        device_manufacturer_id = manufacturer_id
        if device_manufacturer_id:
            # call all API's related to devices to update data for the newly created devices
            TaskService.sync_device_data_on_create(
                provider_id, device_manufacturer_id, serial_numbers, True, True
            )
        _device_info_data_list = []
        # Update Device Info Table
        for (
            serial_number,
            device_crm_id,
        ) in serial_numbers_crm_id_mapping.items():
            if serial_number_instance_number_mapping.get(serial_number):
                _device_info_data = dict(
                    provider=Provider.objects.get(id=provider_id),
                    serial_number=serial_number,
                    instance_id=serial_number_instance_number_mapping.get(
                        serial_number
                    ),
                    is_serial_instance_id=serial_number in na_serial_numbers,
                    device_crm_id=serial_numbers_crm_id_mapping.get(
                        serial_number
                    ),
                    category_id=category_id,
                )
                _device_info_data_list.append(_device_info_data)
        device_info_bulk_create_or_update(_device_info_data_list, provider_id)
    if cw_errors:
        return result, serial_numbers_crm_id_mapping, cw_errors
    return result, serial_numbers_crm_id_mapping, []


def parse_device_import_file(data, file_url, provider):
    # Parsing xlsx file
    if not settings.DEBUG:
        file_path = f"{generate_random_id(10)}.xlsx"
        resp = requests.get(file_url)
        try:
            with open(file_path, "wb") as output:
                output.write(resp.content)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return None, None
    else:
        file_path = file_url
    field_mappings = data.get("field_mappings", {})
    # Serial number can be empty for some configuration types
    serial_number_column = field_mappings.get("serial_number", "")
    instance_number_column = field_mappings.get("instance_number")
    device_name_column = field_mappings.get("device_name")
    address_column = field_mappings.get("address")
    service_contract_number_column = field_mappings.get(
        "service_contract_number", ""
    )
    product_type_column = field_mappings.get("product_type", "")
    purchase_date_column = field_mappings.get("purchase_date", "")
    expiration_date_column = field_mappings.get("expiration_date", "")
    installation_date_column = field_mappings.get("installation_date", "")
    device_price_column = field_mappings.get("device_price", "")
    unique_identifier = field_mappings.get("unique_identifier", "")

    xl = pd.ExcelFile(file_path)
    df = xl.parse(xl.sheet_names[0])

    df.rename(columns=lambda x: x.strip(), inplace=True)
    columns = df.columns

    # set empty product type as "NA"
    if product_type_column in columns:
        df[product_type_column].replace(np.nan, "", inplace=True)
        df[product_type_column] = df[product_type_column].apply(
            lambda x: "N.A." if x in ("", "-") else x
        )
    # set empty address as "NA"
    if address_column in columns:
        df[address_column].replace(np.nan, "", inplace=True)
        df[address_column] = df[address_column].apply(
            lambda x: "N.A." if x in ("", "-") else x
        )

    invalid_index_errors = []
    devices = {}
    configuration_types = data.get("configuration_types", {})
    # Map device name to row number
    device_name_to_row_mapping: Dict[str, int] = {}
    for index, row in df.iterrows():
        _device_name = row.get(device_name_column)
        device_name_to_row_mapping.update({_device_name: index + 2})
        error_msgs = []
        warning_msgs = []
        _device_data = {}

        # Validate device name

        i_serial_number = row.get(serial_number_column, "")
        i_device_name = row[device_name_column]
        if i_device_name in ("-", "", np.nan):
            error_msg = "Invalid Device Name"
            error_msgs.append(error_msg)
        _device_data["device_name"] = i_device_name

        # Validate Instance Number
        if instance_number_column:
            i_instance_number = row[instance_number_column]
            if i_instance_number in ("-", "", np.nan):
                error_msg = "Invalid Instance Number"
                error_msgs.append(error_msg)
            _device_data["instance_id"] = i_instance_number

        # Validate Contract Number
        if service_contract_number_column in columns:
            i_service_contract_number = row[service_contract_number_column]
            if i_service_contract_number in ("-", "", np.nan):
                error_msg = "Invalid Contract Number"
                error_msgs.append(error_msg)
            _device_data["service_contract_number"] = i_service_contract_number

        # Validate purchase date
        if purchase_date_column and purchase_date_column in columns:
            i_purchase_date = row[purchase_date_column]
            i_purchase_date_format = data.get("field_mappings").get(
                "purchase_date_format"
            )
            _purchase_date_value = convert_to_date_from_date_format(
                i_purchase_date, i_purchase_date_format
            )
            if _purchase_date_value is np.nan:
                error_msg = "Invalid Purchase Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_purchase_date_value, datetime.date)
                and _purchase_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _device_data["purchase_date"] = _purchase_date_value

        # Validate expiration date
        if expiration_date_column and expiration_date_column in columns:
            i_expiration_date = row[expiration_date_column]
            i_expiration_date_format = data.get("field_mappings").get(
                "expiration_date_format"
            )
            _expiration_date_value = convert_to_date_from_date_format(
                i_expiration_date, i_expiration_date_format
            )
            if _expiration_date_value is np.nan:
                error_msg = "Invalid Expiration Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_expiration_date_value, datetime.date)
                and _expiration_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _device_data["expiration_date"] = _expiration_date_value

        # Validate Installation Date
        if installation_date_column and installation_date_column in columns:
            i_installation_date = row[installation_date_column]
            i_installation_date_format = data.get("field_mappings").get(
                "installation_date_format"
            )
            _installation_date_value = convert_to_date_from_date_format(
                i_installation_date, i_installation_date_format
            )
            if _installation_date_value is np.nan:
                error_msg = "Invalid Installation Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_installation_date_value, datetime.date)
                and _installation_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _device_data["installation_date"] = _installation_date_value

        # Apply filters
        if not data.get("filter_by_product_type") and data.get(
            "ignore_zero_dollar_items"
        ):
            if device_price_column in columns:
                try:
                    i_device_price = float(row[device_price_column])
                except ValueError:
                    i_device_price = 0.0
                if i_device_price == 0.0:
                    warning_msg = "Device Price is Zero"
                    warning_msgs.append(warning_msg)
        elif data.get("device_type_filter_meta"):
            filters = data.get("device_type_filter_meta")
            filters_dict = {}
            if product_type_column in columns:
                for _i in filters:
                    filters_dict[_i.get("product_type")] = _i
                i_product_type = row[product_type_column]
                if i_product_type in filters_dict:
                    i_product_type_filter = filters_dict.get(i_product_type)
                    if not i_product_type_filter.get("include"):
                        warning_msg = "Product Type not to be included"
                        warning_msgs.append(warning_msg)
                    if i_product_type_filter.get("ignore_zero_dollar_items"):
                        if device_price_column in columns:
                            try:
                                i_device_price = float(
                                    row[device_price_column]
                                )
                            except ValueError:
                                i_device_price = 0.0
                            if i_device_price == 0.0:
                                warning_msg = (
                                    "Product Type with Device Price as Zero"
                                )
                                warning_msgs.append(warning_msg)
                    if (
                        i_product_type_filter.get("include_non_serials")
                        and serial_number_column in row
                    ):
                        i_serial_number = row.get(serial_number_column)
                        if i_serial_number in ("-", ""):
                            i_serial_number = row.get(instance_number_column)
                        else:
                            i_serial_number = row.get(serial_number_column)
                    if (
                        not i_product_type_filter.get("include_non_serials")
                        and serial_number_column in row
                    ):
                        i_serial_number = row.get(serial_number_column)
        #                 if i_serial_number in ("-", ""):
        #                     warning_msg = "Product Type having Serial Number as Invalid"
        #                     warning_msgs.append(warning_msg)
        # if i_serial_number in ("-", ""):
        #     error_msg = "Serial Number is Invalid"
        #     error_msgs.append(error_msg)
        if i_serial_number and i_serial_number is not (np.nan):
            _device_data["serial_number"] = i_serial_number
        # Checking the serial numbers can not be duplicate and can be duplicate in case of empty
        if (
            devices.get(i_serial_number)
            and devices.get(i_serial_number).get("serial_number") != ""
        ):
            error_msg = "Duplicate Serial Number"
            error_msgs.append(error_msg)
        # Checking condition if serial num is empty
        if not serial_number_column or i_serial_number is (np.nan):
            unique_identifier = row.get(
                field_mappings.get("unique_identifier")
            )
            configuration_type = configuration_types.get(unique_identifier)
            device_category_ids = provider.erp_integration.other_config.get(
                "empty_serial_mapping", {}
            ).get("device_category_ids", [])
            # validating the empty serial mapping settings for configuration type
            if configuration_type not in device_category_ids:
                error_msg = "Empty Serial number is not mapped for the selected configuration type"
                error_msgs.append(error_msg)
        else:
            configuration_type = configuration_types.get(i_serial_number)
        if not configuration_type:
            error_msg = "Configuration Type not found."
            error_msgs.append(error_msg)
        _device_data["category_id"] = configuration_type
        if error_msgs or warning_msgs:
            _error = dict(
                row=index + 2,
                serial_number=row.get(serial_number_column),
                error=error_msgs,
                warning=warning_msgs,
                device_name=row.get(device_name_column),
            )
            invalid_index_errors.append(_error)
            continue
        if address_column in columns:
            add_dict = {}
            for item in data.get("address_field_meta"):
                add_dict[item.get("address")] = item.get("site_id")
            i_address = row[address_column]
            _device_data["site_id"] = str(add_dict.get(i_address))
        if service_contract_number_column in columns:
            i_service_contract_number = row[service_contract_number_column]
            try:
                i_service_contract_number = int(i_service_contract_number)
            except ValueError:
                i_service_contract_number = row[service_contract_number_column]
            _device_data["service_contract_number"] = i_service_contract_number
        # For empty serial number, updating the devices dict with unique_identifier key
        if not i_serial_number:
            devices[unique_identifier] = _device_data
        else:
            devices[i_serial_number] = _device_data
    devices.update(device_name_to_row_mapping=device_name_to_row_mapping)
    # Return type is List, List
    # So device_name_to_row_mapping is at end of list.
    return list(devices.values()), invalid_index_errors


def device_info_bulk_create_or_update(_device_info_data_list, provider_id):
    _device_info_data_dict = group_list_of_dictionary_by_key(
        _device_info_data_list, "device_crm_id"
    )
    _device_info_data_dict_device_crm_ids = _device_info_data_dict.keys()
    _device_info_device_crm_ids_for_update = DeviceInfo.objects.filter(
        provider_id=provider_id,
        device_crm_id__in=_device_info_data_dict_device_crm_ids,
    ).values_list("device_crm_id", flat=True)
    _device_info_device_crm_ids_for_create = list(
        set(_device_info_data_dict_device_crm_ids)
        - set(_device_info_device_crm_ids_for_update)
    )
    _device_info_dict_for_create = {
        key: _device_info_data_dict[key][0]
        for key in _device_info_device_crm_ids_for_create
    }

    _device_info_dict_for_update = {
        key: _device_info_data_dict[key][0]
        for key in _device_info_device_crm_ids_for_update
    }
    if _device_info_dict_for_create:
        _device_info_objects = [
            DeviceInfo(**value)
            for key, value in _device_info_dict_for_create.items()
        ]
        DeviceInfo.objects.bulk_create(_device_info_objects)

    if _device_info_dict_for_update:
        lambda data: DeviceInfo.objects.update(
            **data
        ), _device_info_dict_for_update.values()


def generate_file_import_error_csv(invalid_index_errors, cw_errors):
    headers = [
        "Row Number",
        "Serial Number",
        "Device Name",
        "Error Message",
        "Warnings",
        "CW Error Message",
    ]
    new_data = []
    if invalid_index_errors:
        for invalid_index_error in invalid_index_errors:
            prepared_data = dict()
            prepared_data["Row Number"] = invalid_index_error.get("row")
            prepared_data["Serial Number"] = invalid_index_error.get(
                "serial_number"
            )
            prepared_data["Device Name"] = invalid_index_error.get(
                "device_name"
            )
            prepared_data["Error Message"] = (
                ", ".join(invalid_index_error.get("error"))
                if invalid_index_error.get("error")
                else ""
            )
            prepared_data["Warnings"] = (
                ", ".join(invalid_index_error.get("warning"))
                if invalid_index_error.get("warning")
                else ""
            )
            prepared_data["CW Error Message"] = ""
            new_data.append(prepared_data)
    cw_data = []
    if cw_errors:
        for cw_error in cw_errors:
            cw_prepared_data = dict()
            cw_prepared_data["Row Number"] = cw_error.get("row_number", "")
            cw_prepared_data["Serial Number"] = cw_error.get(
                "serial_number", ""
            )
            cw_prepared_data["Device Name"] = cw_error.get("device_name", "")
            cw_prepared_data["Error Message"] = "Connectwise Error"
            cw_prepared_data["Warnings"] = ""
            cw_prepared_data["CW Error Message"] = cw_error.get("message", "")
            cw_data.append(cw_prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
            if cw_errors:
                dict_writer.writerows(cw_data)
        logger.info(
            "Device coverage csv file generated at path {0}".format(file_path)
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return {}
    return dict(file_name=file_name, file_path=file_path)


@shared_task
def batch_create_or_update_subscriptions(
    provider_id, category_id, data, file_name
):
    provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    try:
        (
            subscriptions_data,
            invalid_index_errors,
        ) = parse_subscriptions_import_file(data, file_name)
        if subscriptions_data is None:
            return None
    except Exception as e:
        traceback.print_exc()
        logger.error("Error in device import file: {0}".format(e))
        raise ValidationError({"import_file": ["Invalid file format."]})
    for subscription in subscriptions_data:
        _dict = dict(
            manufacturer_id=data.get("manufacturer_id"),
            category_id=category_id,
        )
        subscription.update(_dict)
    # group subscription data by the customer
    subscriptions_per_customer = group_list_of_dictionary_by_key(
        subscriptions_data, "customer_id"
    )

    result = dict(
        successfully_updated_items_count=0,
        successfully_created_items_count=0,
        failed_updated_items_count=0,
        failed_created_items_count=0,
    )
    for customer_id, subscriptions in subscriptions_per_customer.items():
        (
            _result,
            serial_numbers_crm_id_mapping,
            _,
        ) = erp_client.batch_add_devices(
            customer_id, category_id, subscriptions
        )
        if serial_numbers_crm_id_mapping:
            for subscription in subscriptions:
                subscription.update(
                    {
                        "subscription_crm_id": serial_numbers_crm_id_mapping.get(
                            subscription["serial_number"]
                        )
                    }
                )
        add_or_update_data_to_subscription_model(
            subscriptions, category_id, provider_id
        )
        result["successfully_updated_items_count"] += _result[
            "successfully_updated_items_count"
        ]
        result["successfully_created_items_count"] += _result[
            "successfully_created_items_count"
        ]
        result["failed_updated_items_count"] += _result[
            "failed_updated_items_count"
        ]
        result["failed_created_items_count"] += _result[
            "failed_created_items_count"
        ]

    failed_created_items_count = result.get("failed_created_items_count", 0)
    result["failed_created_items_count"] = failed_created_items_count + len(
        invalid_index_errors
    )
    result["total_request_items_count"] = len(subscriptions_data) + len(
        invalid_index_errors
    )
    import_warnings_file_info = (
        generate_subscription_file_import_error_csv(invalid_index_errors)
        if invalid_index_errors
        else {}
    )

    if import_warnings_file_info.get(
        "file_path"
    ) and import_warnings_file_info.get("file_name"):
        import_warnings_file_path = import_warnings_file_info.get("file_path")
        import_warnings_file_name = import_warnings_file_info.get("file_name")
        url = FileUploadService.upload_file(import_warnings_file_path)
        result["import_warnings_file_info"] = dict(
            file_path=url, file_name=import_warnings_file_name
        )
    # CacheService.invalidate_data(
    #     CacheService.CACHE_KEYS.DEVICE_LIST.format(provider_id, customer_id)
    # )
    return result


class SubscriptionFieldMappings:
    def __init__(self, mappings):
        self.subscription_name = mappings.get("subscription_name")
        self.subscription_id = mappings.get("subscription_id")
        self.contract_number = mappings.get("contract_number")
        self.customer = mappings.get("end_customer")
        self.start_date = mappings.get("start_date")
        self.start_date_format = mappings.get("start_date_format")
        self.expiration_date = mappings.get("expiration_date")
        self.expiration_date_format = mappings.get("expiration_date_format")
        self.status = mappings.get("status")
        self.renewal_date = mappings.get("renewal_date")
        self.auto_renewal_term = mappings.get("auto_renewal_term")
        self.billing_model = mappings.get("billing_model")
        self.model_number = mappings.get("subscription_sku")
        self.licence_qty = mappings.get("licence_qty")
        self.term_length = mappings.get("term_length")
        self.status = mappings.get("status")
        self.true_forward_date = mappings.get("true_forward_date")
        self.true_forward_date_format = mappings.get(
            "true_forward_date_format"
        )


def parse_subscriptions_import_file(data, file_url):
    """
    This will parse the subscription excel file.
    It validates each column data and store the errors.
    :param data:
    :param file_url:
    :return:
    """
    # Parsing xlsx file
    file_path = f"{generate_random_id(10)}.xlsx"
    if settings.DEBUG:
        shutil.copy(file_url, file_path)
    else:
        resp = requests.get(file_url)

        try:
            with open(file_path, "wb") as output:
                output.write(resp.content)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return None, None

    field_mappings = SubscriptionFieldMappings(data.get("field_mappings"))

    xl = pd.ExcelFile(file_path)
    df = xl.parse(xl.sheet_names[0])

    df.rename(columns=lambda x: x.strip(), inplace=True)

    for column in df.columns:
        column_dtype = df.dtypes[column]
        if column_dtype == np.dtype("datetime64[ns]"):
            df[column] = df[column].astype("object")

    df.replace({pd.NaT: ""}, inplace=True)
    df.replace(np.nan, "", regex=True, inplace=True)
    columns = df.columns

    invalid_index_errors = []
    devices = {}
    customer_name_mapping = data.get("customer_name_mapping")
    status_mapping = data.get("status_mapping")
    if status_mapping:
        status_to_be_used = set(status_mapping.keys())
    # fetch customers name and crm id mapping
    customer_crm_ids = Customer.objects.filter(
        id__in=set(customer_name_mapping.values())
    ).values("crm_id", "id")
    customer_id_crm_id_mapping = {
        customer["id"]: customer["crm_id"] for customer in customer_crm_ids
    }

    for index, row in df.iterrows():
        error_msgs = []
        warning_msgs = []
        _subscription_data = {}
        ###############################
        # status validation and filtering
        ################################
        if field_mappings.status:
            if row[field_mappings.status] not in status_to_be_used:
                warning_msg = "Status not marked as include in import."
                warning_msgs.append(warning_msg)
            _subscription_data["status_id"] = status_mapping.get(
                row[field_mappings.status], row[field_mappings.status]
            )

        i_subscription_id = row[field_mappings.subscription_id]
        i_subscription_name = row[field_mappings.subscription_name]
        ###############################
        # Customer Name validation
        ################################
        # verify and set customer id
        customer_id = customer_name_mapping.get(row[field_mappings.customer])
        existing_customer_crm_ids = set(customer_id_crm_id_mapping.keys())
        if not customer_id in existing_customer_crm_ids:
            err_msg = f"Customer not found {row[field_mappings.customer]}"
            error_msgs.append(err_msg)
        _subscription_data.update(
            {"customer_id": customer_id_crm_id_mapping.get(customer_id)}
        )

        ###############################
        # Subscription Name validation
        ################################
        if i_subscription_name in ("-", "", np.nan):
            error_msg = "Invalid Subscription Name"
            error_msgs.append(error_msg)
        _subscription_data["device_name"] = i_subscription_name

        ###############################
        # Contract Number validation
        ################################
        if field_mappings.contract_number in columns:
            i_service_contract_number = row[field_mappings.contract_number]
            if i_service_contract_number in ("-", "", np.nan):
                error_msg = "Invalid Contract Number"
                error_msgs.append(error_msg)
            try:
                i_service_contract_number = int(i_service_contract_number)
            except ValueError:
                i_service_contract_number = row[field_mappings.contract_number]
            _subscription_data[
                "service_contract_number"
            ] = i_service_contract_number
        ###############################
        # Expiration Date validation
        ################################
        if (
            field_mappings.expiration_date
            and field_mappings.expiration_date in columns
        ):
            i_expiration_date = row[field_mappings.expiration_date]
            i_expiration_date_format = field_mappings.expiration_date_format
            _expiration_date_value = convert_to_date_from_date_format(
                i_expiration_date, i_expiration_date_format
            )
            if _expiration_date_value is np.nan:
                error_msg = "Invalid Expiration Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_expiration_date_value, datetime.date)
                and _expiration_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _subscription_data["expiration_date"] = _expiration_date_value

        ###############################
        # Start Date validation
        ################################
        if field_mappings.start_date and field_mappings.start_date in columns:
            i_installation_date = row[field_mappings.start_date]
            i_installation_date_format = field_mappings.start_date_format
            _installation_date_value = convert_to_date_from_date_format(
                i_installation_date, i_installation_date_format
            )
            if _installation_date_value is np.nan:
                error_msg = "Invalid Start Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_installation_date_value, datetime.date)
                and _installation_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _subscription_data["installation_date"] = _installation_date_value

        ###############################
        # True Forward Date validation
        ################################
        if (
            field_mappings.true_forward_date
            and field_mappings.true_forward_date in columns
        ):
            i_true_forward_date = row[field_mappings.true_forward_date]
            i_true_forward_date_format = (
                field_mappings.true_forward_date_format
            )
            _true_forward_date_value = convert_to_date_from_date_format(
                i_true_forward_date, i_true_forward_date_format
            )
            if _true_forward_date_value is np.nan:
                error_msg = "Invalid True forward Date"
                error_msgs.append(error_msg)
            if (
                isinstance(_true_forward_date_value, datetime.date)
                and _true_forward_date_value.year <= 1900
            ):
                error_msg = "Date value before the Year 1900"
                error_msgs.append(error_msg)
            _subscription_data["true_forward_date"] = _true_forward_date_value

        ###############################
        # Subscription Id validation
        ################################
        if i_subscription_id in ("-", "") or i_subscription_id is np.nan:
            error_msg = "Subscription Id is Invalid"
            error_msgs.append(error_msg)
        _subscription_data["serial_number"] = i_subscription_id
        if devices.get(i_subscription_id):
            error_msg = "Duplicate Subscription Id"
            error_msgs.append(error_msg)

        ###############################
        # Auto renewal term validation
        ################################
        auto_renewal_term = row.get(field_mappings.auto_renewal_term)
        if auto_renewal_term:
            warning_msg = "Invalid Value for auto renewal term. The value should be between 1 to 60."
            try:
                auto_renewal_term = int(auto_renewal_term)
            except ValueError:
                warning_msgs.append(warning_msg)
                auto_renewal_term = ""
            if auto_renewal_term and not (1 <= auto_renewal_term <= 60):
                warning_msgs.append(warning_msg)
                auto_renewal_term = ""
            _subscription_data["auto_renewal_term"] = auto_renewal_term

        ###############################
        # term length validation
        ################################
        term_length = row.get(field_mappings.term_length)
        if term_length:
            warning_msg = "Invalid Value for term length. The value should be between 1 to 90."
            try:
                term_length = int(term_length)
            except ValueError:
                warning_msgs.append(warning_msg)
                term_length = ""
            if term_length and not (1 <= term_length <= 90):
                warning_msgs.append(warning_msg)
                term_length = ""
            _subscription_data["term_length"] = term_length

        ###############################
        # Set extra columns
        ################################
        licence_qty = row.get(field_mappings.licence_qty)
        if licence_qty:
            _subscription_data["licence_qty"] = licence_qty
        product_id = row.get(field_mappings.model_number)
        if product_id:
            _subscription_data["product_id"] = product_id
        billing_model = row.get(field_mappings.billing_model)
        if billing_model:
            _subscription_data["billing_model"] = billing_model

        if error_msgs or warning_msgs:
            _error = dict(
                row=index + 2,
                serial_number=row[field_mappings.subscription_id],
                error=error_msgs,
                warning=warning_msgs,
            )
            invalid_index_errors.append(_error)
            continue

        devices[i_subscription_id] = _subscription_data
    os.remove(file_path)
    return list(devices.values()), invalid_index_errors


def add_or_update_data_to_subscription_model(
    subscription_data, category_id, provider_id
):
    """
    Create or update Subscription data model object.
    :param subscription_data: Subscription data list
    :param category_id: Category ID
    :param provider_id: Provider ID
    :return: None
    """
    provider = Provider.objects.get(id=provider_id)
    for subscription in subscription_data:
        customer = Customer.objects.get(
            crm_id=subscription.get("customer_id"), provider=provider
        )
        data = dict(
            auto_renewal_term=subscription.get("auto_renewal_term"),
            licence_qty=subscription.get("licence_qty"),
            term_length=subscription.get("term_length"),
            billing_model=subscription.get("billing_model"),
            true_forward_date=subscription.get("true_forward_date").isoformat()
            if subscription.get("true_forward_date")
            else None,
        )
        # remove none keys
        data = {k: v for k, v in data.items() if v is not None}
        subscription_crm_id = subscription.get("subscription_crm_id")

        params = {
            "subscription_id": subscription.get("serial_number"),
            "subscription_category_id": category_id,
        }
        if data:
            params.update({"data": data})

        SubscriptionData.objects.update_or_create(
            provider=provider,
            customer=customer,
            subscription_crm_id=subscription_crm_id,
            defaults=params,
        )


def generate_subscription_file_import_error_csv(invalid_index_errors):
    headers = ["Row Number", "Subscription Id", "Error Message", "Warnings"]
    new_data = []
    for invalid_index_error in invalid_index_errors:
        prepared_data = dict()
        prepared_data["Row Number"] = invalid_index_error.get("row")
        prepared_data["Subscription Id"] = invalid_index_error.get(
            "serial_number"
        )
        if prepared_data["Subscription Id"] is np.nan:
            prepared_data["Subscription Id"] = ""
        prepared_data["Error Message"] = (
            ", ".join(invalid_index_error.get("error"))
            if invalid_index_error.get("error")
            else ""
        )
        prepared_data["Warnings"] = (
            ", ".join(invalid_index_error.get("warning"))
            if invalid_index_error.get("warning")
            else ""
        )
        new_data.append(prepared_data)

    TEMP_DIR = tempfile.gettempdir()
    file_name = f"{generate_random_id(15)}.csv"
    file_path = os.path.join(TEMP_DIR, file_name)
    try:
        with open(file_path, "w") as output_file:
            dict_writer = csv.DictWriter(output_file, headers)
            dict_writer.writeheader()
            dict_writer.writerows(new_data)
        logger.info(
            "Device coverage csv file generated at path {0}".format(file_path)
        )
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return {}
    return dict(file_name=file_name, file_path=file_path)
