from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.postgres.fields.jsonb import KeyTransform
from django.db.models import Q
from django.db.models.expressions import RawSQL

from accounts.models import Provider
from celery import chain, shared_task
from celery.utils.log import get_task_logger
from core.models import DeviceInfo
from core.services import LogicMonitorIntegrationService
from core.tasks.services import TaskService
from config_compliance.tasks.compliance import store_config_compliance_score
from core.utils import (
    convert_date_object_to_date_string,
    group_list_of_dictionary_by_key,
)
from exceptions import exceptions
from copy import deepcopy

logger = get_task_logger(__name__)

DEVICE_DATA_SOURCE_TYPE_FILTER = 'dataSourceType:"CS"'
DATA_SOURCE_INSTANCE_DISPLAY_NAME_FILTER = 'displayName:"startup-config"'


@shared_task
def logic_monitor_sync_device_name(provider_id=None, manufacturer_ids=None):
    if provider_id:
        providers = [Provider.objects.get(id=provider_id)]
    else:
        providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            client = LogicMonitorIntegrationService.get_monitoring_api_client(
                provider=provider
            )
            if client:
                if (
                    client.extra_config.get("config_mappings").get(
                        "config_names"
                    )
                    is None
                ):
                    logger.error(
                        "Device Monitoring Mapping not complete. Please set the config names."
                    )
                device_mappings = client.extra_config.get("device_mappings")
                if manufacturer_ids:
                    for _manufacturer_id in manufacturer_ids:
                        _lm_configs = device_mappings.get(_manufacturer_id)
                        if _lm_configs:
                            for _lm_config in _lm_configs:
                                logic_monitor_sync_device_name_for_each_config.delay(
                                    provider.id, _manufacturer_id, _lm_config
                                )
                else:
                    for (
                        _manufacturer_id,
                        _lm_configs,
                    ) in device_mappings.items():
                        logger.info(
                            f"Syncing LM data for Provider ID: {provider.id} and Manufacturer ID: {_manufacturer_id}"
                        )
                        for _lm_config in _lm_configs:
                            logic_monitor_sync_device_name_for_each_config.delay(
                                provider.id, _manufacturer_id, _lm_config
                            )


@shared_task
def logic_monitor_sync_device_name_for_each_config(
    provider_id, manufacturer_id, lm_config
):
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        client = LogicMonitorIntegrationService.get_monitoring_api_client(
            provider=provider
        )
        if client is None:
            return
        lm_config_single_group = deepcopy(lm_config)
        lm_config_single_group.pop("group_ids", "")
        for group_id in lm_config.get("group_ids", []):
            lm_config_single_group["group_id"] = group_id
            # List of devices from LM.
            lm_devices = client.get_logic_monitor_device_list(
                lm_config_single_group
            )
            category_id = provider.get_device_category_id_by_manufacturer(
                manufacturer_id
            )
            if not lm_devices:
                logger.info(f"No LogicMonitor Devices for {manufacturer_id}")
                return
            logger.info("Parsing LogicMonitor Devices")
            lm_device_dict = group_list_of_dictionary_by_key(
                lm_devices, "company_id"
            )
            if lm_device_dict:
                company_ids = list(lm_device_dict.keys())
                logger.info(f"Devices found for {company_ids} Companies")
                serial_numbers = []
                for company_id, _item in lm_device_dict.items():
                    serial_numbers.extend(
                        [x.get("serial_number") for x in _item]
                    )
                # List of devices from CW by company ids.
                cw_devices = provider.erp_client.get_devices(
                    customer_id=company_ids, category_id=category_id
                )
                cw_devices_dict = group_list_of_dictionary_by_key(
                    cw_devices, "customer_id"
                )

                (
                    devices_to_create_dict,
                    devices_to_update_list,
                ) = parse_logic_monitor_and_cw_device_data(
                    category_id, cw_devices_dict, lm_device_dict
                )

                devices_managed = {}
                if devices_to_update_list:
                    result = provider.erp_client.update_device_names(
                        devices_to_update_list
                    )
                    for _cw_res in result:
                        if _cw_res.status_code == 200 and _cw_res.json():
                            message = _cw_res.json()
                            _device_id = message.get("id")
                            _lm_serial_number = message.get("serialNumber")
                            devices_managed[_device_id] = dict(
                                serial_number=_lm_serial_number
                            )
                if devices_to_create_dict:
                    batch_requests_items = (
                        provider.erp_client.batch_create_device_data(
                            category_id, devices_to_create_dict
                        )
                    )
                    result = provider.erp_client.batch_create_devices(
                        batch_requests_items
                    )
                    for _cw_res in result:
                        if _cw_res.status_code == 201 and _cw_res.json():
                            message = _cw_res.json()
                            _device_id = message.get("id")
                            _lm_serial_number = message.get("serialNumber")
                            devices_managed[_device_id] = dict(
                                serial_number=_lm_serial_number,
                                is_created_by_lm=True,
                            )
                TaskService.sync_device_data(
                    provider_id, manufacturer_id, serial_numbers
                )
                update_logic_monitor_monitoring_data(
                    devices_managed, lm_devices, provider, category_id
                )


def parse_logic_monitor_and_cw_device_data(
    category_id, cw_devices_dict, lm_device_dict
):
    devices_to_update_list = []
    devices_to_create_dict = {}
    # Iterate over list of devices by company id from LM.
    # Check for device if exists in CW then update else create in CW
    for _company_id, _list_of_devices_from_lm in lm_device_dict.items():
        _devices_to_create = []
        _cw_devices_by_customer = cw_devices_dict.get(int(_company_id))
        if _cw_devices_by_customer:
            _cw_to_update_devices_list = _cw_devices_by_customer
            _cw_to_update_devices_dict = group_list_of_dictionary_by_key(
                _cw_to_update_devices_list, "serial_number"
            )
            for _item in _list_of_devices_from_lm:
                _lm_serial_number = _item.get("serial_number")
                _lm_device_name = _item.get("device_name")
                _cw_list_of_devices = _cw_to_update_devices_dict.get(
                    _lm_serial_number
                )
                if _cw_list_of_devices:
                    for _cw_res in _cw_list_of_devices:
                        _cw_res.update({"device_name": _lm_device_name})
                        devices_to_update_list.append(_cw_res)
                else:
                    _devices_to_create.append(
                        dict(
                            customer_id=_company_id,
                            category_id=category_id,
                            serial_number=_lm_serial_number,
                            device_name=_lm_device_name,
                        )
                    )
        else:
            for _item in _list_of_devices_from_lm:
                _lm_serial_number = _item.get("serial_number")
                _lm_device_name = _item.get("device_name")
                _devices_to_create.append(
                    dict(
                        customer_id=_company_id,
                        category_id=category_id,
                        serial_number=_lm_serial_number,
                        device_name=_lm_device_name,
                    )
                )

        if _devices_to_create:
            devices_to_create_dict[_company_id] = _devices_to_create
    return devices_to_create_dict, devices_to_update_list


def update_logic_monitor_monitoring_data(
    devices_managed, lm_devices, provider, category_id
):
    """
    This function updates `DeviceInfo` with the corresponding monitoring_data.
    """
    lm_devices_dict_by_serial_number = group_list_of_dictionary_by_key(
        lm_devices, "serial_number"
    )
    if devices_managed:
        logger.info(f"Updating Monitoring Data")
        _device_ids_managed = list(devices_managed.keys())
        device_infos_to_update = DeviceInfo.objects.filter(
            device_crm_id__in=list(_device_ids_managed),
            provider=provider,
            category_id=category_id,
        ).values_list("device_crm_id", flat=True)
        device_infos_to_create = list(
            set(_device_ids_managed) - set(device_infos_to_update)
        )
        device_infos = DeviceInfo.objects.filter(
            device_crm_id__in=device_infos_to_update,
            provider=provider,
            category_id=category_id,
        )
        for _device_info in device_infos:

            _lm_data = lm_devices_dict_by_serial_number.get(
                _device_info.serial_number
            )
            _item_dict = devices_managed.get(_device_info.device_crm_id)
            if _lm_data:
                monitoring_data = _device_info.monitoring_data or {}
                monitoring_data_from_meta_data = (
                    _get_monitoring_data_from_meta_data(_lm_data[0])
                )
                monitoring_data_updates = {
                    f"{settings.LOGICMONITOR}": monitoring_data_from_meta_data,
                    "last_updated_by": settings.LOGICMONITOR,
                    "is_managed": True,
                }
                monitoring_data.update(monitoring_data_updates)
                _device_info.monitoring_data = monitoring_data
                monitoring_meta_data = {
                    f"{settings.LOGICMONITOR}": _get_config_data_from_meta_data(
                        _lm_data[0]
                    )
                }
                _device_info.monitoring_meta_data.update(monitoring_meta_data)
                _device_info.save()
                logger.info(
                    f"Updated Logic Monitor Data for Serial Number: {_device_info.serial_number}"
                )
                sync_config_for_logic_monitor_device_id(
                    provider.id, _device_info.id
                ),
                logger.info(
                    f"Syncing Logic Monitor Config for: {_device_info.id}"
                )
        if device_infos:
            store_config_compliance_score(provider.id)

        bulk_create_devices = []
        for (_device_id, item_dict) in devices_managed.items():
            _device_serial_number = item_dict.get("serial_number")
            _is_created_by_lm = item_dict.get("is_created_by_lm")
            if _device_id in device_infos_to_create:
                _lm_data = lm_devices_dict_by_serial_number.get(
                    _device_serial_number
                )
                if _lm_data:
                    monitoring_meta_data = {
                        f"{settings.LOGICMONITOR}": _get_config_data_from_meta_data(
                            _lm_data[0]
                        )
                    }
                    monitoring_data_from_meta_data = (
                        _get_monitoring_data_from_meta_data(_lm_data[0])
                    )
                    monitoring_data_from_meta_data.update(
                        dict(is_created_by_lm=_is_created_by_lm)
                    )
                    monitoring_data = {
                        f"{settings.LOGICMONITOR}": monitoring_data_from_meta_data,
                        "created_by": settings.LOGICMONITOR,
                        "last_updated_by": settings.LOGICMONITOR,
                        "is_managed": True,
                    }
                    bulk_create_devices.append(
                        DeviceInfo(
                            provider=provider,
                            device_crm_id=_device_id,
                            monitoring_data=monitoring_data,
                            serial_number=_device_serial_number,
                            category_id=category_id,
                            monitoring_meta_data=monitoring_meta_data,
                        )
                    )
                    logger.info(
                        f"Creating Device Info object for Serial Number: {_device_serial_number}"
                    )
        if bulk_create_devices:
            DeviceInfo.objects.bulk_create(bulk_create_devices)
            sync_logic_monitor_config_data_for_provider.delay(provider.id)
            logger.info(
                f"Syncing Logic Monitor Config for Provider: {provider.id}"
            )

        devices_not_managed = DeviceInfo.objects.filter(
            category_id=category_id
        ).exclude(device_crm_id__in=list(_device_ids_managed))
        for _managed_device in devices_not_managed:
            if _managed_device.monitoring_data:
                solarwinds_managed_flag = _managed_device.monitoring_data.get(
                    settings.SOLARWINDS, {}
                ).get("is_managed", False)

                # set LM individual flag to false
                _managed_device.monitoring_data.get(settings.LOGICMONITOR, {})[
                    "is_managed"
                ] = False
                # set combined flag based on the combined value of
                # logicmonitor and solarwinds flag
                _managed_device.monitoring_data["is_managed"] = (
                    False or solarwinds_managed_flag
                )
                _managed_device.save()


def _get_monitoring_data_from_meta_data(lm_data):
    monitoring_data = dict(
        is_managed=True,
        os_version=lm_data.get("os_version"),
        host_name=lm_data.get("host_name"),
        device_name=lm_data.get("device_name"),
        up_time=lm_data.get("up_time"),
        sys_name=lm_data.get("sys_name"),
        updated_on=convert_date_object_to_date_string(
            datetime.utcnow(), settings.ISO_FORMAT
        ),
        model=lm_data.get("model"),
        manufacturer=lm_data.get("manufacturer"),
    )
    return monitoring_data


def _get_config_data_from_meta_data(lm_data):
    device_id = lm_data.get("lm_device_id")
    device_datasource_id = lm_data.get("lm_device_datasource_id")
    device_datasource_instance_id = lm_data.get(
        "lm_device_datasource_instance_id"
    )
    config_data = {"device_id": device_id}
    if device_datasource_id:
        config_data["device_datasource_id"] = device_datasource_id
    if device_datasource_instance_id:
        config_data[
            "device_datasource_instance_id"
        ] = device_datasource_instance_id
    return config_data


@shared_task
def sync_logic_monitor_config_data(provider_id=None):
    if provider_id:
        chain(
            sync_logic_monitor_config_data_for_provider.si(provider_id),
            store_config_compliance_score.si(provider_id),
        ).delay()

    else:
        for provider in Provider.objects.all():
            chain(
                sync_logic_monitor_config_data_for_provider.si(provider.id),
                store_config_compliance_score.si(provider.id),
            ).delay()


@shared_task
def sync_logic_monitor_config_data_for_provider(provider_id):
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        client = LogicMonitorIntegrationService.get_monitoring_api_client(
            provider
        )
        if client:
            prev_day = datetime.now() - timedelta(days=1)
            extracted_device_infos = list(
                DeviceInfo.objects.annotate(
                    meta_data=KeyTransform(
                        f"{settings.LOGICMONITOR}", "config_meta_data"
                    ),
                    lm_monitoring_data=KeyTransform(
                        f"{settings.LOGICMONITOR}", "monitoring_data"
                    ),
                )
                .annotate(
                    date=RawSQL(
                        f"(configuration -> '{settings.LOGICMONITOR}' ->> 'config_last_synced_date')::date",
                        [],
                    ),
                    is_managed=RawSQL(
                        f"(monitoring_data -> '{settings.LOGICMONITOR}' ->> 'is_managed')::boolean",
                        [],
                    ),
                )
                .filter(
                    meta_data__has_key="device_id",
                    lm_monitoring_data__has_key="is_managed",
                )
                .filter(Q(date__lte=prev_day) | Q(date=None), is_managed=True)
                .values_list("id", flat=True)
            )

            for device_info_id in extracted_device_infos:
                sync_config_for_logic_monitor_device_id(
                    provider_id, device_info_id
                )


@shared_task
def sync_config_for_logic_monitor_device_id(provider_id, device_info_id):
    device_info = DeviceInfo.objects.annotate(
        lm_device_id=KeyTransform(
            "device_id", KeyTransform("logicmonitor", "config_meta_data")
        )
    ).get(id=device_info_id)
    provider = Provider.objects.get(id=provider_id)
    if (
        device_info.monitoring_data.get(settings.LOGICMONITOR, {}).get(
            "is_managed", False
        )
        and provider.is_active
        and provider.is_configured
    ):
        client = LogicMonitorIntegrationService.get_monitoring_api_client(
            provider
        )
        if client:
            extra_config = client.extra_config
            config_names = extra_config.get("config_mappings").get(
                "config_names"
            )
            try:
                if not device_info.lm_device_id:
                    return
                datasources = client.get_device_datasource_list(
                    device_info.lm_device_id, DEVICE_DATA_SOURCE_TYPE_FILTER
                )
                for datasource in datasources:
                    if datasource.data_source_name in config_names:
                        datasource_instances = (
                            client.get_device_datasource_instance_list(
                                device_info.lm_device_id,
                                datasource.id,
                                DATA_SOURCE_INSTANCE_DISPLAY_NAME_FILTER,
                            )
                        )
                        if datasource_instances:
                            for (
                                datasource_instance
                            ) in datasource_instances.items:
                                device_configs = client.get_device_config_source_config_list(
                                    device_info.lm_device_id,
                                    datasource.id,
                                    datasource_instance.id,
                                )
                                if device_configs:
                                    device_config = max(
                                        device_configs,
                                        key=lambda x: x.poll_timestamp,
                                    )
                                    config_last_updated_on = (
                                        datetime.fromtimestamp(
                                            device_config.poll_timestamp / 1e3
                                        ).strftime("%Y-%m-%dT%H:%M:%SZ")
                                    )
                                    device_info.config_meta_data.get(
                                        settings.LOGICMONITOR, {}
                                    ).update(
                                        dict(
                                            config_datasource_id=datasource.id,
                                            config_datasource_name=datasource.data_source_name,
                                            config_datasource_instance_id=datasource_instance.id,
                                            config_datasource_instance_name=datasource_instance.name,
                                            config_id=device_config.id,
                                            config_display_name=device_config.device_display_name,
                                            config_instance_name=device_config.instance_name,
                                        )
                                    )
                                    lm_config = {
                                        "config_last_synced_date": datetime.now().strftime(
                                            "%Y-%m-%dT%H:%M:%SZ"
                                        ),
                                        "config": device_config.config,
                                        "config_last_updated_on": config_last_updated_on,
                                    }
                                    device_info.configuration.get(
                                        settings.LOGICMONITOR, {}
                                    ).update(lm_config)
                                    device_info.save()
                                else:
                                    logger.info(
                                        f"No Config found for LM Device: {device_info.lm_device_id}, "
                                        f"LM DataSource: {datasource.id} and LM DataSource Instance: "
                                        f"{datasource_instance.id}"
                                    )
            except exceptions.NotFoundError as e:
                logger.info(e)


@shared_task
def update_logic_monitor_devices(provider_id, manufacturer_ids=None):
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        logic_monitor_sync_device_name.delay(provider_id, manufacturer_ids)
