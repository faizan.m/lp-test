from core.exceptions import NotConfigured
from core.integrations import Cisco
from core.models import ProviderIntegration, IntegrationType
from exceptions import exceptions


class CiscoIntegrationService(object):
    @classmethod
    def _get_client(
        cls,
        provider,
        priority_task=False,
        eox=False,
        product_info=False,
        search_line=False,
        subscription_details=False,
    ):
        """
        returns primary cisco account for provider to be used for high priority tasks.
        :param priority_task: True if client required for critical task
        :param provider: Provider Object
        :param eox: True for EOX API client
        :return: Cisco Object
        """
        if not provider.is_configured:
            raise NotConfigured()
        cisco_integration_type = IntegrationType.objects.get(name="CISCO")
        cisco_integrations = ProviderIntegration.objects.filter(
            provider=provider, integration_type=cisco_integration_type
        ).order_by("id")
        if not priority_task and cisco_integrations.count() <= 1:
            return None
        usable_accounts_count = 0
        clients = []
        for integration in cisco_integrations:
            client = Cisco(integration.authentication_info)
            try:
                data = client.check_connection(
                    eox,
                    product_info,
                    search_line=search_line,
                    subscription_details=subscription_details,
                )
                if data:
                    usable_accounts_count += 1
                    clients.append(client)
            except exceptions.RateLimitExceededError:
                pass
            except exceptions.APIError:
                pass
        if usable_accounts_count == 0:
            return None
        if not priority_task and usable_accounts_count == 1:
            return None
        elif not priority_task:
            return clients[-1]
        return clients[0]

    @classmethod
    def get_eox_api_client(cls, provider, priority_task=False):
        """
        Returns Cisco EOX API Client
        :param priority_task: True if client required for critical task
        :param provider: Provider Object
        :return: Cisco Object
        """
        return CiscoIntegrationService._get_client(
            provider, priority_task, eox=True
        )

    @classmethod
    def get_product_info_api_client(cls, provider, priority_task=False):
        """
        Returns Cisco Product Info API Client
        :param priority_task: True if client required for critical task
        :param provider: Provider Object
        :return: Cisco Object
        """
        return CiscoIntegrationService._get_client(
            provider, priority_task, product_info=True
        )

    @classmethod
    def get_search_lines_api_client(cls, provider, priority_task=False):
        """
        Returns Cisco CCWR Search Info API Client
        :param priority_task: True if client required for critical task
        :param provider: Provider Object
        :return: Cisco Object
        """
        return CiscoIntegrationService._get_client(
            provider, priority_task, search_line=True
        )

    @classmethod
    def get_subscription_details_api_client(
        cls, provider, priority_task=False
    ):
        """
        Returns Cisco Subscription Details API Client
        :param priority_task: True if client required for critical task
        :param provider: Provider Object
        :return: Cisco Object
        """
        return CiscoIntegrationService._get_client(
            provider, priority_task, subscription_details=True
        )
