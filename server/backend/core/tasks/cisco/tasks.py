import inspect
from datetime import datetime, timedelta

import dateutil
from celery import shared_task, chain
from celery.utils.log import get_task_logger
from django.conf import settings
from django.db import IntegrityError
from django.db.models import F, Q
from django.db.models.expressions import RawSQL

from accounts.models import Provider, Customer
from core.integrations.erp.connectwise.connectwise import (
    Operators,
    CWConditions,
    Condition,
)
from core.models import (
    DeviceManufacturerData,
    DeviceManufacturerMissingData,
    DeviceTypes,
    IntegrationType,
    DeviceInfo,
    SubscriptionData,
)
from core.tasks.cisco.services import CiscoIntegrationService
from core.utils import (
    chunks_of_list_with_max_item_count,
    group_list_of_dictionary_by_key,
    remove_keys_with_none_and_empty_value,
)
from rest_framework.generics import get_object_or_404
from exceptions import exceptions

logger = get_task_logger(__name__)


@shared_task
def call_get_cisco_device_manufacturer_data_on_all_serial_numbers(provider_id):
    """
    Fetch all serial numbers and call method to fetch cisco data.
    """
    provider = Provider.objects.get(id=provider_id)
    device_category_ids = _get_device_categories_with_cisco_integrations(
        provider_id
    )
    for device_category_id in device_category_ids:
        all_serial_numbers = provider.erp_client.get_device_serial_numbers(
            category_id=device_category_id
        )
        available_data_serial_numbers = DeviceManufacturerData.objects.filter(
            provider_id=provider_id, device_category_id=device_category_id
        ).values_list("device_serial_number", flat=True)
        all_serial_numbers = list(set(all_serial_numbers))
        all_serial_numbers = list(
            filter(
                lambda sn: sn not in available_data_serial_numbers,
                all_serial_numbers,
            )
        )
        args = list(chunks_of_list_with_max_item_count(all_serial_numbers, 50))
        args_for_eox = [
            (provider_id, device_category_id, item, False, True)
            for item in args
        ]
        get_cisco_eox_data_and_create_or_update_object.chunks(
            args_for_eox, 10
        ).delay()


@shared_task
def get_cisco_eox_and_coverage_for_serial_numbers(
    provider_id, device_category_id, serial_numbers
):
    available_data_serial_numbers = DeviceManufacturerData.objects.filter(
        provider_id=provider_id, device_category_id=device_category_id
    ).values_list("device_serial_number", flat=True)
    serial_numbers = list(set(serial_numbers))
    serial_numbers = list(
        filter(
            lambda sn: sn not in available_data_serial_numbers, serial_numbers
        )
    )
    args = list(chunks_of_list_with_max_item_count(serial_numbers, 50))
    args_for_eox = [
        (provider_id, device_category_id, item, False, True) for item in args
    ]
    get_cisco_eox_data_and_create_or_update_object.chunks(
        args_for_eox, 10
    ).delay()


@shared_task
def get_cisco_eox_data_and_create_or_update_object(
    provider_id,
    device_category_id,
    serial_numbers,
    update_missing_data_table=False,
    priority_task=False,
):
    """
    Fetch Cisco Data and create DeviceManufacturerData object if not exist else update.
    :return:
    """
    provider = Provider.objects.get(id=provider_id)
    cisco_client = CiscoIntegrationService.get_eox_api_client(
        provider, priority_task
    )
    if not cisco_client:
        return None
    if not isinstance(serial_numbers, list):
        serial_numbers = [serial_numbers]
    args = list(chunks_of_list_with_max_item_count(serial_numbers, 500))
    for arg in args:
        arg = list(filter(lambda x: x.isalnum(), arg))
        try:
            eox_data_list = cisco_client.get_device_eox_info(arg)
        except exceptions.RateLimitExceededError:
            logger.info("Exiting cisco EOX api task. API Limit Reached.")
            return
        except exceptions.APIError as e:
            logger.error(
                "Status Code: {0} Message: {1}".format(e.status_code, e.detail)
            )
            return
        eox_create_or_update.delay(
            device_category_id,
            arg,
            eox_data_list,
            provider_id,
            update_missing_data_table,
        )


@shared_task()
def eox_create_or_update(
    device_category_id,
    serial_numbers,
    eox_data_list,
    provider_id,
    update_missing_data_table,
):
    serial_numbers_with_data = []
    for eox_data in eox_data_list:
        try:
            if eox_data:
                serial_number = eox_data.get("serial_number")
                serial_numbers_with_data.append(serial_number)
                _create_or_update_device_manufacturer_data(
                    serial_number,
                    provider_id,
                    device_category_id,
                    eox_data,
                    cisco_update_type="eox",
                )
        except Exception as e:
            logger.info("Message: {0}".format(e))

    serial_numbers_without_data = list(
        set(serial_numbers) - set(serial_numbers_with_data)
    )
    # If this call is made for missing data table. We need to update that table.
    # if update_missing_data_table:
    # Remove the entries for which we found data.
    DeviceManufacturerMissingData.objects.filter(
        serial_number__in=serial_numbers_with_data, provider_id=provider_id
    ).delete()
    # Increase retry count for serial numbers for which we didn't find any data.
    DeviceManufacturerMissingData.objects.filter(
        serial_number__in=serial_numbers_without_data,
        provider_id=provider_id,
        device_category_id=device_category_id,
    ).update(retry_count=F("retry_count") + 1)


@shared_task
def fetch_data_for_cisco_missing_data_table(provider_id):
    """
    Fetch Cisco Data for serial numbers in DeviceManufacturerMissingData Table.
    :return:
    """
    device_category_ids = _get_device_categories_with_cisco_integrations(
        provider_id
    )
    for device_category_id in device_category_ids:
        serial_numbers = DeviceManufacturerMissingData.objects.filter(
            retry_count__lt=settings.CISCO_MISSING_DATA_MAX_RETRY,
            provider_id=provider_id,
            is_invalid=False,
            device_category_id=device_category_id,
        ).values_list("serial_number", flat=True)
        chain(
            get_cisco_eox_data_and_create_or_update_object.si(
                provider_id,
                device_category_id,
                list(serial_numbers),
                True,
                True,
            ),
            get_cisco_device_search_data_and_update_object.si(
                provider_id,
                device_category_id,
                list(serial_numbers),
                priority_task=True,
            ),
        ).delay()


@shared_task()
def cisco_missing_data_for_non_alnum_serial_numbers():
    """
    Fetch data fro entries in Cisco Missing Data for Non alphanumeric serial numbers
    :return:
    """
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            device_category_ids = (
                _get_device_categories_with_cisco_integrations(provider.id)
            )
            for device_category_id in device_category_ids:
                serial_numbers = DeviceManufacturerMissingData.objects.filter(
                    provider_id=provider.id,
                    is_invalid=False,
                    device_category_id=device_category_id,
                ).values_list("serial_number", flat=True)
                serial_numbers = list(
                    filter(lambda x: not x.isalnum(), serial_numbers)
                )
                try:
                    cisco_client = CiscoIntegrationService.get_eox_api_client(
                        provider, priority_task=True
                    )
                except exceptions.RateLimitExceededError:
                    logger.info(
                        "Exiting cisco EOX api task. API Limit Reached."
                    )
                    return
                if cisco_client:
                    for serial_number in serial_numbers:
                        try:
                            eox_data = cisco_client.get_device_eox_info(
                                serial_number
                            )
                            if eox_data and len(eox_data) > 0:
                                eox_data = eox_data[0]
                                serial_number = eox_data.get("serial_number")
                                _create_or_update_device_manufacturer_data(
                                    serial_number,
                                    provider.id,
                                    device_category_id,
                                    eox_data,
                                    cisco_update_type="eox",
                                )

                                # Remove the entries for which we found data.
                                DeviceManufacturerMissingData.objects.filter(
                                    serial_number=serial_number,
                                    provider_id=provider.id,
                                ).delete()
                            else:
                                # Increase retry count for serial number for which we didn't find any data.
                                DeviceManufacturerMissingData.objects.filter(
                                    serial_number=serial_number,
                                    provider_id=provider.id,
                                    device_category_id=device_category_id,
                                ).update(retry_count=F("retry_count") + 1)

                        except exceptions.APIError as e:
                            # 'SSA_ERR_010' is for incorrect serial_number
                            if e.detail in ["SSA_ERR_010", "SSA_ERR_016"]:
                                DeviceManufacturerMissingData.objects.filter(
                                    provider_id=provider.id,
                                    serial_number=serial_number,
                                    device_category_id=device_category_id,
                                ).update(is_invalid=True)

                        except Exception as e:
                            logger.info("Message: {0}".format(e))


@shared_task
def update_manufacturer_data_for_cisco(provider_id):
    """
    Fetch and Update Cisco data for serial numbers in DeviceManufacturer table.
    :return:
    """
    device_category_ids = _get_device_categories_with_cisco_integrations(
        provider_id
    )
    for device_category_id in device_category_ids:
        prev_day = datetime.now() - timedelta(days=1)
        serial_numbers_eox = (
            DeviceManufacturerData.objects.filter(
                provider_id=provider_id, device_category_id=device_category_id
            )
            .annotate(
                date=RawSQL("(data->>'eox_last_updated_date')::date", [])
            )
            .filter(Q(date__lte=prev_day))
            .values_list("device_serial_number", flat=True)
            .order_by("updated_on")
        )

        chain(
            get_cisco_eox_data_and_create_or_update_object.si(
                provider_id,
                device_category_id,
                list(serial_numbers_eox),
                True,
                True,
            ),
        ).delay()


@shared_task
def device_manufacturer_data_sync_task():
    """
    Task to sync device manufacturer data.
    :return:
    """
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            update_manufacturer_data_for_cisco.delay(provider.id)


@shared_task
def cisco_missing_data_task():
    """
    Call task to fetch missing data for cisco for each provider
    """
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            fetch_data_for_cisco_missing_data_table.delay(provider.id)


@shared_task()
def sync_cisco_device_type_task():
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            device_category_ids = (
                _get_device_categories_with_cisco_integrations(provider.id)
            )
            for device_category_id in device_category_ids:
                cisco_client = (
                    CiscoIntegrationService.get_product_info_api_client(
                        provider, priority_task=True
                    )
                )
                if not cisco_client:
                    continue
                product_ids = list(
                    DeviceManufacturerData.objects.annotate(
                        product_id=RawSQL(
                            "data::json#>'{eox_data, product_id}'", []
                        )
                    )
                    .filter(device_category_id=device_category_id)
                    .exclude(Q(product_id=None))
                    .values_list("product_id", flat=True)
                )
                product_ids = set(product_ids)
                ids = set(
                    DeviceTypes.objects.filter().values_list(
                        "product_id", flat=True
                    )
                )
                create_device_type_object_if_not_exists(
                    provider.id, product_ids=product_ids.difference(ids)
                )
                update_device_software_suggestion.delay(provider.id)


@shared_task()
def create_device_type_object_if_not_exists(provider_id, product_ids):
    provider = Provider.objects.get(id=provider_id)
    cisco_client = CiscoIntegrationService.get_product_info_api_client(
        provider, priority_task=True
    )
    if cisco_client:
        for product_id in product_ids:
            try:
                if (
                    product_id
                    and not DeviceTypes.objects.filter(
                        product_id=product_id
                    ).count()
                    > 0
                ):
                    data = cisco_client.get_device_product_info(product_id)

                    if data:
                        data = data[0]
                        data["product_id"] = product_id
                        try:
                            DeviceTypes.objects.create(**data)
                        except IntegrityError:
                            logger.debug(
                                "Product Id: {0} already exists".format(
                                    product_id
                                )
                            )
            except exceptions.RateLimitExceededError:
                logger.warn(
                    "Exiting cisco device type api task. API Limit Reached."
                )
                return
            except exceptions.APIError as e:
                logger.error(
                    "Status Code: {0} Message: {1}".format(
                        e.status_code, e.detail
                    )
                )


@shared_task()
def update_device_software_suggestion(provider_id):
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        prev_day = datetime.now() - timedelta(days=1)
        product_ids = set(
            DeviceTypes.objects.all()
            .values_list("product_id", flat=True)
            .annotate(
                date=RawSQL(
                    "(data->>'suggestion_last_updated_date')::date", []
                )
            )
            .filter(Q(date__lte=prev_day) | Q(date=None))
        )
        cisco_client = CiscoIntegrationService.get_product_info_api_client(
            provider, priority_task=True
        )
        if cisco_client:
            try:
                data = cisco_client.get_product_software_suggestion(
                    list(product_ids)
                )
                data = group_list_of_dictionary_by_key(data, "product_id")
                device_types = DeviceTypes.objects.filter(
                    product_id__in=product_ids
                )
                for device_type in device_types:
                    product_id = device_type.product_id
                    cisco_soft_data = data.get(product_id)
                    if cisco_soft_data:
                        suggested_software_update = cisco_soft_data[0].get(
                            "suggested_software_update"
                        )
                        device_type.suggested_software_update = (
                            suggested_software_update
                        )
                        device_type.data.update(cisco_soft_data[0].get("data"))
                        device_type.data.update(
                            {
                                "suggestion_last_updated_date": datetime.now().strftime(
                                    "%Y-%m-%dT%H:%M:%SZ"
                                )
                            }
                        )
                        device_type.save()
                    else:
                        print(
                            f"No software suggestion found for, Product Id {product_id}"
                        )
            except exceptions.RateLimitExceededError:
                logger.warn(
                    "Exiting cisco software suggestion api task. API Limit Reached."
                )
                return
            except exceptions.APIError as e:
                logger.error(
                    "Status Code: {0} Message: {1}".format(
                        e.status_code, e.detail
                    )
                )


@shared_task
def sync_cisco_device_search_lines():
    for provider in Provider.objects.all():
        sync_device_search_lines_for_provider.delay(provider.id)


@shared_task
def sync_device_search_lines_for_provider(provider_id):
    """
    Task to retrieve the cisco search data, using Cisco Search API, then to update device data on CW and Acela Side.
    """
    provider = Provider.objects.get(id=provider_id)
    if provider.is_active and provider.is_configured:
        try:
            cisco_client = CiscoIntegrationService.get_search_lines_api_client(
                provider, priority_task=True
            )
            if cisco_client:
                device_category_ids = (
                    _get_device_categories_with_cisco_integrations(provider_id)
                )
                prev_day = datetime.now() - timedelta(days=1)
                serial_numbers = (
                    DeviceManufacturerData.objects.filter(
                        provider=provider,
                        device_category_id__in=device_category_ids,
                    )
                    .annotate(
                        date=RawSQL(
                            "(data->>'search_last_updated_date')::date", []
                        )
                    )
                    .filter(Q(date__lte=prev_day) | Q(date=None))
                    .values_list("device_serial_number", flat=True)
                )
                serial_numbers = list(set(serial_numbers))
                args = list(
                    chunks_of_list_with_max_item_count(serial_numbers, 30)
                )
                for arg in args:
                    get_cisco_device_search_data_and_update_object.delay(
                        provider_id, device_category_ids, arg
                    )
        except exceptions.RateLimitExceededError:
            logger.info("Exiting cisco EOX api task. API Limit Reached.")
            return
        except exceptions.APIError as e:
            logger.error(
                "Status Code: {0} Message: {1}".format(e.status_code, e.detail)
            )
            return


@shared_task
def get_cisco_device_search_data_and_update_object(
    provider_id, device_category_ids, serial_numbers, priority_task=True
):
    if not serial_numbers:
        return
    provider = Provider.objects.get(id=provider_id)
    try:
        cisco_client = CiscoIntegrationService.get_search_lines_api_client(
            provider, priority_task
        )
        if cisco_client:
            search_data = cisco_client.get_device_search_lines(serial_numbers)
            parse_and_update_device_search_lines.delay(
                provider_id, serial_numbers, search_data, device_category_ids
            )
    except exceptions.RateLimitExceededError:
        logger.info("Exiting cisco search api task. API Limit Reached.")
        return
    except exceptions.APIError as e:
        logger.error(
            "Status Code: {0} Message: {1}".format(e.status_code, e.detail)
        )
        return


@shared_task
def parse_and_update_device_search_lines(
    provider_id, serial_numbers, search_data, device_category_ids
):
    """
    Update cisco search device data on CW and Acela Side.
    """
    provider = Provider.objects.get(id=provider_id)
    if device_category_ids:
        if not isinstance(device_category_ids, list):
            device_category_ids = [device_category_ids]
        mnf_id_filter = CWConditions(
            int_conditions=[
                Condition("type/id", Operators.IN, tuple(device_category_ids))
            ]
        )
        devices = provider.erp_client.get_device_by_serial_numbers(
            serial_numbers, filters=[mnf_id_filter]
        )
        if devices:
            devices_to_update = []
            _to_updates = []
            search_data_by_sr = group_list_of_dictionary_by_key(
                search_data, "serialNumber"
            )
            for device in devices:
                _serial_number = device.get("serial_number")
                _category_id = device.get("category_id")
                if search_data_by_sr.get(_serial_number):
                    (
                        installation_date_str,
                        expiration_date_str,
                        _latest_data_by_ship_date,
                    ) = (
                        None,
                        None,
                        None,
                    )
                    _latest_purc_date_dt = None
                    _search_data_for_sr_list = search_data_by_sr.get(
                        _serial_number
                    )
                    if len(_search_data_for_sr_list) > 1:
                        installation_date_dt, expiration_date_dt = None, None
                        for _search_data_for_sr in _search_data_for_sr_list:
                            _temp_start_date_str = _search_data_for_sr.get(
                                "startDate"
                            )
                            if _temp_start_date_str:
                                _temp_start_date_dt = datetime.strptime(
                                    _temp_start_date_str, "%Y-%m-%dT%H:%M:%SZ"
                                )
                                if installation_date_dt is None:
                                    (
                                        installation_date_dt,
                                        installation_date_str,
                                    ) = (
                                        _temp_start_date_dt,
                                        _temp_start_date_str,
                                    )
                                elif (
                                    _temp_start_date_dt < installation_date_dt
                                ):
                                    (
                                        installation_date_dt,
                                        installation_date_str,
                                    ) = (
                                        _temp_start_date_dt,
                                        _temp_start_date_str,
                                    )
                            _temp_end_date_str = _search_data_for_sr.get(
                                "endDate"
                            )
                            if _temp_end_date_str:
                                _temp_end_date_dt = datetime.strptime(
                                    _temp_end_date_str, "%Y-%m-%dT%H:%M:%SZ"
                                )
                                if expiration_date_dt is None:
                                    expiration_date_dt, expiration_date_str = (
                                        _temp_end_date_dt,
                                        _temp_end_date_str,
                                    )
                                elif _temp_end_date_dt > expiration_date_dt:
                                    expiration_date_dt, expiration_date_str = (
                                        _temp_end_date_dt,
                                        _temp_end_date_str,
                                    )
                            _temp_purc_date_str = _search_data_for_sr.get(
                                "shipDate"
                            )
                            if _temp_purc_date_str:
                                _temp_purc_date_dt = datetime.strptime(
                                    _temp_purc_date_str, "%Y-%m-%dT%H:%M:%SZ"
                                )
                                if _latest_purc_date_dt is None:
                                    (
                                        _latest_purc_date_dt,
                                        _latest_data_by_ship_date,
                                    ) = (
                                        _temp_purc_date_dt,
                                        _search_data_for_sr,
                                    )
                                elif _latest_purc_date_dt < _temp_purc_date_dt:
                                    (
                                        _latest_purc_date_dt,
                                        _latest_data_by_ship_date,
                                    ) = (
                                        _temp_purc_date_dt,
                                        _search_data_for_sr,
                                    )
                            elif _latest_purc_date_dt is None:
                                _latest_data_by_ship_date = _search_data_for_sr
                        logger.debug(
                            f"Found multiple entries for serial number {_serial_number} in Cisco Search API"
                        )
                    else:
                        _latest_data_by_ship_date = _search_data_for_sr_list[0]
                        installation_date_str = _latest_data_by_ship_date.get(
                            "startDate"
                        )
                        expiration_date_str = _latest_data_by_ship_date.get(
                            "endDate"
                        )
                    try:
                        # Store Service SKU, Service Level and Service Description on Acela
                        service_sku = _latest_data_by_ship_date.get(
                            "serviceSKU"
                        )
                        service_level = _latest_data_by_ship_date.get(
                            "serviceLevel"
                        )

                        service_description = _latest_data_by_ship_date.get(
                            "serviceDescription"
                        )
                        purchase_date_str = _latest_data_by_ship_date.get(
                            "shipDate"
                        )
                        product_id = _latest_data_by_ship_date.get(
                            "product", {}
                        ).get("number")
                        service_contract_number = (
                            _latest_data_by_ship_date.get("contract", {}).get(
                                "number"
                            )
                        )
                        item_description = _latest_data_by_ship_date.get(
                            "product", {}
                        ).get("description")
                        _to_update = {
                            "service_sku": service_sku,
                            "item_description": item_description,
                            "service_contract_number": service_contract_number,
                            "product_id": product_id,
                            "purchase_date": purchase_date_str,
                            "expiration_date": expiration_date_str,
                            "installation_date": installation_date_str,
                            "service_description": service_description,
                            "service_level": service_level,
                        }

                        _to_update = remove_keys_with_none_and_empty_value(
                            _to_update
                        )
                        # Update Instance ID Acela
                        instance_id = _latest_data_by_ship_date.get(
                            "instanceNumber"
                        )
                        if instance_id:
                            DeviceInfo.objects.filter(
                                device_crm_id=device.get("id"),
                                serial_number=_serial_number,
                                provider=provider,
                            ).update(instance_id=instance_id)

                        _create_or_update_device_manufacturer_data(
                            _serial_number,
                            provider_id,
                            _category_id,
                            _to_update,
                            "search",
                        )

                    except DeviceManufacturerData.DoesNotExist:
                        logger.debug(
                            f"Device Manufacturer data does not exist for serial number {_serial_number}"
                        )
                    except DeviceInfo.DoesNotExist:
                        logger.debug(
                            f"Device Info data does not exist for Device ID {device.get('id')}"
                        )
                    except Exception as e:
                        logger.error(e)


@shared_task
def sync_cisco_subscription_data():
    providers = Provider.objects.all()
    for provider in providers:
        if provider.is_active and provider.is_configured:
            sync_cisco_subscription_data_for_provider.delay(provider.id)


@shared_task
def sync_cisco_subscription_data_for_provider(provider_id):
    provider = Provider.objects.get(id=provider_id)
    try:
        cisco_client = (
            CiscoIntegrationService.get_subscription_details_api_client(
                provider, priority_task=True
            )
        )
        if cisco_client:
            category_id = provider.get_all_subscription_categories()
            subscription_ids = provider.erp_client.get_devices(
                category_id=category_id,
                required_fields=["serialNumber", "id", "company/id"],
            )
            _subscription_ids = [
                subscription_id.get("serial_number")
                for subscription_id in subscription_ids
                if subscription_id.get("serial_number")
            ]
            data = cisco_client.get_subscription_details(_subscription_ids)
            # logger.info(
            #     "Response from Sync CISCO Subscription Details API {0}".format(data)
            # )
            if data:
                devices_to_sync = list()
                for _subscription_data in data:
                    device_data = dict()
                    serial_number = _subscription_data.get("serial_number")
                    device_data["installation_date"] = _subscription_data.pop(
                        "installation_date", None
                    )
                    device_data["expiration_date"] = _subscription_data.pop(
                        "expiration_date", None
                    )
                    device_data["serial_number"] = _subscription_data.pop(
                        "serial_number", None
                    )
                    _data = {
                        "subscription_last_updated_date": datetime.now().strftime(
                            "%Y-%m-%dT%H:%M:%SZ"
                        )
                    }
                    _subscription_data.update(_data)
                    subscriptions_data = SubscriptionData.objects.filter(
                        subscription_id=serial_number
                    )
                    if subscriptions_data.exists():
                        for subscription_data in subscriptions_data:
                            subscription_data.data.update(_subscription_data)
                            subscription_data.save()
                        devices_to_sync.append(device_data)
                    else:
                        for subscription_crm_id in subscription_ids:
                            if (
                                subscription_crm_id.get("serial_number")
                                == serial_number
                            ):
                                _subscription_crm_id = subscription_crm_id.get(
                                    "id"
                                )
                                customer = get_object_or_404(
                                    Customer,
                                    provider=provider_id,
                                    crm_id=subscription_crm_id.get(
                                        "customer_id"
                                    ),
                                )
                                try:
                                    SubscriptionData.objects.create(
                                        subscription_id=serial_number,
                                        provider_id=provider_id,
                                        data=_subscription_data,
                                        subscription_crm_id=_subscription_crm_id,
                                        customer_id=customer.id,
                                    )
                                    devices_to_sync.append(device_data)
                                except IntegrityError:
                                    logger.error(
                                        "ProviderID {0} with crm id {1} already exists for Subscription Id: {2}".format(
                                            provider_id,
                                            _subscription_crm_id,
                                            serial_number,
                                        )
                                    )
                                break
                logger.info(
                    "Successfully Sync CISCO Subscriptions Data with Acela"
                )
                # Updating devices in CW
                erp_client = provider.erp_client
                erp_client.batch_add_devices(
                    customer_id=None, category_id=None, data=devices_to_sync
                )
            else:
                logger.info("No Subscription Data to Sync")
    except exceptions.RateLimitExceededError:
        logger.info(
            "Exiting cisco Subscription details sync api task. API Limit Reached."
        )
        return
    except exceptions.APIError as e:
        logger.error(
            "Status Code: {0} Message: {1}".format(e.status_code, e.detail)
        )
        return


def _get_subscription_categories_with_cisco_integrations(provider_id):
    provider = Provider.objects.get(id=provider_id)
    subscription_mapping = provider.get_subscription_mapping()
    subscription_category_ids = []
    for cw_mnf_id, subscription_map in subscription_mapping.items():
        acela_integration_type_id = subscription_map.get(
            "acela_integration_type_id"
        )
        if acela_integration_type_id:
            if IntegrationType.objects.get(
                id=acela_integration_type_id
            ) == IntegrationType.objects.get(name="CISCO"):
                subscription_category_ids.append(
                    subscription_map.get("subscription_category_id")
                )
    return subscription_category_ids


def _get_manufacturers_with_cisco_integrations(provider_id):
    provider = Provider.objects.get(id=provider_id)
    manufacturer_mapping = provider.get_manufacturer_mapping()
    manufacturer_ids = []
    for cw_mnf_id, manufacturer_map in manufacturer_mapping.items():
        acela_integration_type_id = manufacturer_map.get(
            "acela_integration_type_id"
        )
        if acela_integration_type_id:
            if IntegrationType.objects.get(
                id=acela_integration_type_id
            ) == IntegrationType.objects.get(name="CISCO"):
                manufacturer_ids.append(cw_mnf_id)
    return manufacturer_ids


def _get_device_categories_with_cisco_integrations(provider_id):
    provider = Provider.objects.get(id=provider_id)
    manufacturer_mapping = provider.get_manufacturer_mapping()
    device_category_ids = []
    for cw_mnf_id, manufacturer_map in manufacturer_mapping.items():
        acela_integration_type_id = manufacturer_map.get(
            "acela_integration_type_id"
        )
        if acela_integration_type_id:
            if IntegrationType.objects.get(
                id=acela_integration_type_id
            ) == IntegrationType.objects.get(name="CISCO"):
                device_category_ids.append(
                    manufacturer_map.get("device_category_id")
                )
    return device_category_ids


def _create_or_update_device_manufacturer_data(
    serial_number, provider_id, device_category_id, data, cisco_update_type
):
    """
    This utils updates the DeviceManufacturerData data with the DateTime of last synced based on the `type`
    """
    mnf_data = {
        f"{cisco_update_type}_last_updated_date": datetime.now().strftime(
            "%Y-%m-%dT%H:%M:%SZ"
        ),
        f"{cisco_update_type}_data": data,
    }
    try:
        if cisco_update_type == "search":
            logger.info(
                f"serial number {serial_number} having product id {data.get('product_id','')} "
                f"installation date {data.get('installation_date', '')} & expiration date "
                f"{data.get('expiration_date', '')} updated from the caller method name: "
                f"{inspect.stack()[1][3]} "
            )
    except IndexError as error:
        logger.error(f"Index error while logging caller method name {error}")
    try:
        device_data = DeviceManufacturerData.objects.get(
            device_serial_number=serial_number, provider_id=provider_id
        )
        device_data.data.update(mnf_data)
        device_data.save()

    except DeviceManufacturerData.DoesNotExist:
        DeviceManufacturerData.objects.create(
            device_serial_number=serial_number,
            provider_id=provider_id,
            data=mnf_data,
            device_category_id=device_category_id,
        )
