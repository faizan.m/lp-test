from celery import chain
from celery.utils.log import get_task_logger

from accounts.models import Provider
from core.tasks.base import ManufacturerTaskBase
from core.tasks.cisco.tasks import (
    call_get_cisco_device_manufacturer_data_on_all_serial_numbers,
    get_cisco_eox_data_and_create_or_update_object,
    get_cisco_eox_and_coverage_for_serial_numbers,
    get_cisco_device_search_data_and_update_object,
)

logger = get_task_logger(__name__)


class CiscoManufacturer(ManufacturerTaskBase):
    def sync_device_data_with_priority(
        self,
        provider_id,
        manufacturer_id,
        serial_numbers,
        update_missing_data_table,
        priority_task,
    ):
        if not isinstance(serial_numbers, list):
            serial_numbers = [serial_numbers]
        logger.info(f"Syncing device data for {serial_numbers}")
        provider = Provider.objects.get(id=provider_id)
        device_category_id = provider.get_device_category_id_by_manufacturer(
            manufacturer_id
        )
        chain(
            get_cisco_eox_data_and_create_or_update_object.si(
                provider_id,
                device_category_id,
                serial_numbers,
                update_missing_data_table=True,
                priority_task=priority_task,
            ),
            get_cisco_device_search_data_and_update_object.si(
                provider_id,
                device_category_id,
                list(serial_numbers),
                priority_task=priority_task,
            ),
        ).delay()

    def sync_manufacturer_data_for_serial_numbers(
        self, provider_id, manufacturer_id, serial_numbers
    ):
        provider = Provider.objects.get(id=provider_id)
        device_category_id = provider.get_device_category_id_by_manufacturer(
            manufacturer_id
        )
        get_cisco_eox_and_coverage_for_serial_numbers.delay(
            provider_id, device_category_id, list(serial_numbers)
        )

    def fetch_all_manufacturer_data(self, provider_id):
        call_get_cisco_device_manufacturer_data_on_all_serial_numbers.delay(
            provider_id
        )
