import copy
import os
import tempfile
from datetime import datetime
from typing import List, Dict, Optional, Tuple
import pandas as pd
from celery import shared_task
from celery.utils.log import get_task_logger
from django.db.models import Q
from django.db.models.expressions import RawSQL
import asyncio
from accounts.email import (
    ReportRequestEmail,
    DeviceReportRequestEmail,
    CustomerDistributionUpdateEmail,
    ScheduledDeviceReportRequestEmail,
    DevicesReportsBundleRequestEmail,
)
from accounts.models import Provider, Customer, User, CircuitInfo, UserType
from core.models import (
    DeviceManufacturerData,
    DeviceManufacturerMissingData,
    DeviceTypes,
    UserScheduledReport,
    ScheduledReportTypes,
    AutomatedSubscriptionNotificationSetting,
)
from core.reports import (
    prepare_scheduled_report_devices_data,
    prepare_scheduled_report_device_report_pdf,
    generate_customer_device_management_report,
)
from core.services import (
    DeviceService,
    SubscriptionNotificationService,
)
from core.utils import generate_random_id, group_list_of_dictionary_by_key
from core.workflow.smartnet_receiving.services import SmartnetReceivingService
from reporting.services import ExcelCreationService
from utils import convert_local_files_to_remote_attachments
from django.utils import timezone

logger = get_task_logger(__name__)


@shared_task
def prepare_and_send_data_anomaly_report(provider_id, user_id):
    """
    Prepares Data anomaly reports and send email to user.
    :param provider_id: Provider Id
    :param user_id: User Id
    :return: None
    """
    provider = Provider.objects.get(id=provider_id)
    devices = DeviceService.get_device_list(
        provider_id=provider.id,
        customer_id=None,
        erp_client=provider.erp_client,
        category_id=provider.device_category_id,
    )

    df = pd.DataFrame(devices)
    df_duplicates = (
        df.groupby(["customer_name", "serial_number"])
        .size()
        .reset_index(name="counts")
    )
    df_duplicates = df_duplicates[df_duplicates.counts > 1]
    df_duplicates = df_duplicates.sort_values(by=["counts"], ascending=False)

    def func(df, customer_name, serial_number):
        data = set(
            df[
                (df.customer_name == customer_name)
                & (df.serial_number == serial_number)
            ].device_name
        )
        return ",".join(data)

    df_duplicates["device_names"] = df_duplicates.apply(
        lambda x: func(df, x.customer_name, x.serial_number), axis=1
    )

    invalid_sr_no = list(
        DeviceManufacturerMissingData.objects.filter(
            serial_number__in=set(df.serial_number), is_invalid=True
        ).values_list("serial_number", flat=True)
    )
    df_invalid_sr_no = df[df.serial_number.isin(invalid_sr_no)][
        ["customer_name", "serial_number", "device_name"]
    ]
    product_ids = list(
        DeviceManufacturerData.objects.annotate(
            product_id=RawSQL("data::json#>'{eox_data, product_id}'", [])
        )
        .exclude(Q(product_id=None))
        .values_list("product_id", flat=True)
    )
    product_ids = set(product_ids)
    ids = set(
        DeviceTypes.objects.filter().values_list("product_id", flat=True)
    )
    df_product_ids = pd.DataFrame()
    df_product_ids["Product Id"] = list(product_ids.difference(ids))

    TEMP_DIR = tempfile.gettempdir()

    file_path_for_duplicate_sr_no = os.path.join(
        TEMP_DIR, f"{generate_random_id(15)}.csv"
    )
    df_duplicates.columns = [
        "Customer Name",
        "Serial Number",
        "Duplicate Count",
        "Configuration Names",
    ]
    df_duplicates.to_csv(
        file_path_for_duplicate_sr_no, encoding="utf-8", index=False
    )

    file_path_for_invalid_sr_no = os.path.join(
        TEMP_DIR, f"{generate_random_id(15)}.csv"
    )
    df_invalid_sr_no.columns = [
        "Customer Name",
        "Serial Number",
        "Configuration Name",
    ]
    df_invalid_sr_no.to_csv(
        file_path_for_invalid_sr_no, encoding="utf-8", index=False
    )

    file_path_for_product_ids = os.path.join(
        TEMP_DIR, f"{generate_random_id(15)}.csv"
    )
    df_product_ids.to_csv(
        file_path_for_product_ids, encoding="utf-8", index=False
    )

    files = [
        ("Duplicate_Serial_Number_Report.csv", file_path_for_duplicate_sr_no),
        ("Invalid_Serial_Number_Report.csv", file_path_for_invalid_sr_no),
        ("Product_ID_Report.csv", file_path_for_product_ids),
    ]

    user = User.objects.get(id=user_id)
    context = {
        "user": user,
        "files": [file[0] for file in files],
        "provider": provider,
    }
    files, remote_attachments = convert_local_files_to_remote_attachments(
        files
    )

    ReportRequestEmail(
        context=context, files=files, remote_attachments=remote_attachments
    ).send([user.email])
    logger.debug("Anomaly data report sent to {0}", format(user.email))


@shared_task
def prepare_and_send_device_pdf_report(devices, user_id):
    file_path = DeviceService.generate_device_report(devices)
    user = User.objects.get(id=user_id)
    file = [("Device-Report.pdf", file_path)]
    try:
        provider = user.provider
    except AttributeError:
        provider = None
    context = {"user": user, "file": file[0], "provider": provider}
    files, remote_attachments = convert_local_files_to_remote_attachments(file)
    DeviceReportRequestEmail(
        context=context, files=files, remote_attachments=remote_attachments
    ).send([user.email])


@shared_task
def prepare_and_send_devices_reports_bundle(
    provider_id, user_id, user_type, customer_id, report_types_bundle
):
    customer_name = Customer.objects.get(
        crm_id=customer_id[0], provider_id=provider_id
    ).name
    user = User.objects.get(id=user_id)
    csv_paths = {}
    for report_data in report_types_bundle:
        label = report_data.get("report_type").get("name")
        file_path, file_name = generate_customer_device_management_report(
            provider_id,
            customer_id,
            report_data,
            user_type,
            user,
        )
        if file_path:
            csv_paths.update({label: file_path})

    if csv_paths:
        (
            report_path,
            file_name,
        ) = ExcelCreationService.create_bundle_reports_excel(
            csv_paths, customer_name
        )
    else:
        report_path = None
        file_name = None

    if report_path and file_name:
        context = {"user": user, "provider_id": provider_id}
        report_files = [(file_name, report_path)]

        files, remote_attachments = convert_local_files_to_remote_attachments(
            report_files
        )

        DevicesReportsBundleRequestEmail(
            context=context, files=files, remote_attachments=remote_attachments
        ).send([user.email])
        logger.info(f"Device reports bundle sent to {user.email}")
    else:
        logger.info(f"No Device reports found to sent for {user.email}")


@shared_task
def send_customer_distribution_update_email(
    customer_id, provider_id, customer_distribution
):
    provider = Provider.objects.get(id=provider_id)
    customer = Customer.objects.get(id=customer_id)
    customer_distribution["Staffing Changes"] = customer_distribution.pop(
        "staffing_changes"
    )
    customer_distribution["Monitoring Alerts"] = customer_distribution.pop(
        "monitoring_alerts"
    )
    customer_distribution["Scheduled Reports"] = customer_distribution.pop(
        "scheduled_reports"
    )
    customer_distribution[
        "Alerts and Notifications"
    ] = customer_distribution.pop("alerts_and_notifications")

    context = dict(
        customer=customer,
        customer_distribution=customer_distribution,
        provider=provider,
    )
    CustomerDistributionUpdateEmail(context=context).send(
        [provider.support_contact]
    )


@shared_task()
def bulk_device_delete(provider_id, device_ids):
    provider = Provider.objects.get(id=provider_id)
    logger.info(
        f"For Provider {provider.name}: {len(device_ids)} Devices are being deleted."
    )
    deleted_device_ids = provider.erp_client.batch_delete_devices(device_ids)
    logger.info(
        f"Total Devices deleted {len(deleted_device_ids)} of {len(device_ids)}"
    )


@shared_task()
def remove_device_circuit_association(provider_id, device_ids):
    logger.info("Remove Device and Circuits association")
    CircuitInfo.objects.filter(device_crm_id__in=list(device_ids)).update(
        device_crm_id=None
    )


@shared_task
def send_scheduled_user_device_reports():
    current_date = datetime.now().date()
    user_scheduled_device_reports = list(
        UserScheduledReport.objects.filter(
            scheduled_report_type__category=ScheduledReportTypes.CATEGORY_CHOICES.DEVICES,
            next_run__lte=current_date,
        ).values("user", "id")
    )
    user_reports_for_user = group_list_of_dictionary_by_key(
        user_scheduled_device_reports, "user"
    )
    for user_id, user_reports in user_reports_for_user.items():
        send_scheduled_user_device_reports_for_user.delay(
            str(user_id), user_reports
        )


@shared_task
def send_scheduled_user_device_reports_for_user(user_id, user_reports):
    user = User.objects.get(id=user_id)
    provider = Provider.objects.get(id=user.provider.id)
    category_id = provider.get_all_device_categories()
    if user.type == UserType.PROVIDER:
        devices = DeviceService.get_device_list(
            provider_id=provider.id,
            customer_id=None,
            erp_client=provider.erp_client,
            category_id=category_id,
        )
    else:
        devices = DeviceService.get_device_list(
            provider_id=provider.id,
            customer_id=user.customer.crm_id,
            erp_client=provider.erp_client,
            category_id=category_id,
        )
    report_files = []
    file_desc = []
    no_data = []
    for user_report_dict in user_reports:
        user_scheduled_report_id = user_report_dict.get("id")
        user_scheduled_report = UserScheduledReport.objects.get(
            id=user_scheduled_report_id
        )
        pdf_title = ScheduledReportTypes.objects.get(
            type=user_scheduled_report.scheduled_report_type
        ).display_name
        _devices = prepare_scheduled_report_devices_data(
            copy.deepcopy(devices),
            user_scheduled_report.scheduled_report_type.type,
        )
        if _devices:
            file_path, file_name = prepare_scheduled_report_device_report_pdf(
                _devices, pdf_title
            )
            file_desc.append(f"Device count: {len(_devices)} for {pdf_title}")
            report_files.append((file_name, file_path))
        else:
            no_data.append(pdf_title)

    context = {
        "user": user,
        "files": file_desc,
        "no_data": no_data,
        "provider": provider,
    }
    files, remote_attachments = convert_local_files_to_remote_attachments(
        report_files
    )

    ScheduledDeviceReportRequestEmail(
        context=context, files=files, remote_attachments=remote_attachments
    ).send([user.email])
    logger.info(f"Scheduled data report sent to {user.email}")

    for file in report_files:
        os.remove(file[1])

    for user_report_dict in user_reports:
        user_scheduled_report_id = user_report_dict.get("id")
        user_scheduled_report = UserScheduledReport.objects.get(
            id=user_scheduled_report_id
        )
        next_run = user_scheduled_report.get_next_run(
            user_scheduled_report.frequency
        )
        user_scheduled_report.next_run = next_run
        user_scheduled_report.save()


@shared_task
def smartnet_receiving_task(
    provider_id, customer_id, user_id, file_name, payload
):
    provider = Provider.objects.get(id=provider_id)
    customer = provider.customer_set.get(id=customer_id)
    user = User.providers.get(id=user_id)
    result = SmartnetReceivingService.process(
        provider, customer, user, payload, file_name
    )
    return result


@shared_task
def create_devices_task(
    provider_id, category_id, customer_id, manufacturer_id, validated_data
):
    provider = Provider.objects.get(id=provider_id)
    SmartnetReceivingService.create_devices(
        provider, category_id, customer_id, manufacturer_id, validated_data
    )


@shared_task
def create_opportunity_task(provider_id, customer_id, opportunity_payload):
    from document.services import QuoteService

    for payload in opportunity_payload:
        payload_data = {
            "name": payload.get("opportunity_name"),
            "customer_id": customer_id,
            "user_id": payload.get("user_id"),
            "type_id": payload.get("opportunity_type"),
            "stage_id": payload.get("opportunity_stage"),
            "inside_rep_id": payload.get("inside_rep"),
        }
        QuoteService.create_quote(provider_id, payload_data)


@shared_task
def send_automated_subscription_notification_emails(
    provider_ids: List[int] = None,
):
    """
    Task to send automated subscription notification emails for customers.

    Args:
        provider_ids: Provider IDs

    Returns:
        None
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in providers:
        try:
            setting: AutomatedSubscriptionNotificationSetting = (
                provider.subscription_notification_setting
            )
        except AutomatedSubscriptionNotificationSetting.DoesNotExist:
            logger.info(
                f"Automated Subscription Notification Setting not configured for the provider: {provider.name}. "
                f"Skipped sending subscription notification emails."
            )
            continue
        scheduled_days: List[str] = setting.weekly_send_schedule
        current_datetime = timezone.now().date()
        today: str = current_datetime.strftime("%A").upper()
        if today in scheduled_days:
            notification_service: SubscriptionNotificationService = (
                SubscriptionNotificationService(provider)
            )
            customers: "QuerySet[Customer]" = Customer.objects.filter(
                provider_id=provider.id, is_active=True, is_deleted=False
            )
            notification_service.send_expiring_subscription_notification_to_customers(
                customers
            )
            logger.info(
                f"Finished sending expiring subscription notification emails to customers "
                f"of provider: {provider.name}"
            )
    return


@shared_task
def send_subscription_notification_email(
    provider_id: int,
    all_customers: bool,
    customer_crm_ids: List[int],
) -> Dict:
    """
    Send subscription notification email to customers.

    Args:
        provider_id: Provider ID
        all_customers: True send notice to all customers else False
        customer_crm_ids: Customer CRM IDs.

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    try:
        provider.subscription_notification_setting
    except AutomatedSubscriptionNotificationSetting.DoesNotExist:
        reason: str = (
            f"Automated Subscription Notification Setting not configured for the provider: {provider.name}. "
            f"Skipped sending subscription notification emails."
        )
        logger.info(reason)
        return dict(operation_status="FAILURE", reason=reason, payload={})
    notification_service: SubscriptionNotificationService = (
        SubscriptionNotificationService(provider)
    )
    if all_customers:
        customers: "QuerySet[Customer]" = Customer.objects.filter(
            provider_id=provider.id, is_active=True, is_deleted=False
        )
    else:
        customers: "QuerySet[Customer]" = Customer.objects.filter(
            provider_id=provider.id,
            is_active=True,
            crm_id__in=customer_crm_ids,
        )
    customer_mail_statuses: List[
        Dict
    ] = notification_service.send_expiring_subscription_notification_to_customers(
        customers
    )
    skipped_customers: List[Dict] = [
        status
        for status in customer_mail_statuses
        if status.get("email_sent") is False
    ]
    return dict(
        operation_status="SUCCESS",
        reason=None,
        payload=dict(skipped_customers=skipped_customers),
    )
