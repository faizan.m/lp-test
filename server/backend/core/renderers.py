import unicodecsv as csv
from io import BytesIO

from django.http import HttpResponse
from rest_framework import status
from rest_framework.renderers import BaseRenderer
from rest_framework_csv.renderers import CSVRenderer
from six import BytesIO, text_type
from core.services import DeviceService
import os
from utils import get_app_logger

logger = get_app_logger(__name__)


class DictToCSVRenderer(CSVRenderer):
    """
    Renderer which serializes to CSV
    """

    media_type = "text/csv"
    format = "csv"
    level_sep = "."
    charset = "utf-8"

    def render(
        self, data, media_type=None, renderer_context={}, writer_opts=None
    ):
        """
        Renders serialized *data* into CSV.
        """
        if data is None:
            return ""

        if not isinstance(data, list):
            data = [data]

        writer_opts = renderer_context.get(
            "writer_opts", writer_opts or self.writer_opts or {}
        )
        header = renderer_context.get("header", self.header)
        labels = renderer_context.get("labels", self.labels)
        encoding = renderer_context.get("encoding", self.charset)

        table = self.tablize(data, header=header, labels=labels)
        csv_buffer = BytesIO()
        csv_writer = csv.writer(csv_buffer, encoding=encoding, **writer_opts)
        for row in table:
            csv_writer.writerow(row)

        return csv_buffer.getvalue()


class DeviceReportCSVRenderer(CSVRenderer):
    """
    Custom CSV Renderer to generate device report csv
    """

    header = [
        "EOL_date",
        "EOSA_date",
        "EOSU_date",
        "EOSWS_date",
        "EOS_date",
        "LDOS_date",
        "category_name",
        "customer_name",
        "contract_status",
        "device_name",
        "device_type",
        "expiration_date",
        "item_description",
        "manufacturer_name",
        "model_number",
        "notes",
        "product_bulletin_url",
        "product_id",
        "serial_number",
        "instance_id",
        "service_contract_number",
        "site",
        "status",
    ]
    labels = {
        "EOL_date": "EOL Date",
        "EOSA_date": "EOSA Date",
        "EOSU_date": "EOSU Date",
        "EOSWS_date": "EOSWS Date",
        "EOS_date": "EOS Date",
        "LDOS_date": "LDOS Date",
        "category_name": "Category Name",
        "customer_name": "Customer Name",
        "contract_status": "Contract Status",
        "device_name": "Device Name",
        "device_type": "Device Type",
        "expiration_date": "Expiration Date",
        "item_description": "Item Description",
        "manufacturer_name": "Manufacturer Name",
        "model_number": "Model Number",
        "notes": "Notes",
        "product_bulletin_url": "Product Bulletin Url",
        "product_id": "Product Id",
        "serial_number": "Serial Number",
        "instance_id": "Instance Id",
        "service_contract_number": "Service Contract Number",
        "site": "Site",
        "status": "Status",
    }


class FinanceReportCSVRenderer(CSVRenderer):
    """
    Custom CSV Renderer to generate finance report csv
    """

    header = [
        "purchase_order_status",
        "po_number",
        "received_status",
        "product_id",
        "description",
        "received_quantity",
        "unit_cost_in_receiving",
        "ext_cost_in_receiving",
        "vendor_company_name",
        "vendor_order_number",
        "customer_company_name",
        "ordered_quantity",
        "serial_numbers",
        "shipping_method",
        "tracking_numbers",
        "location",
        "expected_ship_date",
        "unit_cost_in_so",
        "unit_price",
        "ext_cost_in_so",
        "ext_price",
        "opportunity_name",
        "sales_order_crm_id",
        "so_linked_pos",
        "customer_po_number",
        "quantity",
        "line_number",
        "unit_of_measure",
        "internal_notes_po",
        "shipment_date",
        "total",
        "entered_by",
        "received_date",
        "opportunity_crm_id",
        "status_name",
        "vendor_invoice_number",
        "shipping_instruction",
        "order_date",
        "due_date",
        "total_in_so",
        "sub_total",
        "tax_total",
        "billing_status_name",
        "subtotal_in_invoice",
        "sales_tax_invoice",
        "total_in_invoice",
        "balance",
        "date",
        "due_date_invoice",
        "payment_date",
        "reference",
        "customer_po",
        "billing_term_name",
        "tax_code",
        "territory_name",
        "invoice_type",
        "invoice_shipping_price",
    ]
    labels = {
        "purchase_order_status": "Open/Received",
        "po_number": "PO #",
        "received_status": "Status",
        "product_id": "Product ID",
        "description": "Description",
        "received_quantity": "Received Qty",
        "unit_cost_in_receiving": "Unit Cost in receiving",
        "ext_cost_in_receiving": "Ext Cost in receiving",
        "vendor_company_name": "Vendor",
        "vendor_order_number": "Vendor Order #",
        "customer_company_name": "Ship To",
        "ordered_quantity": "Purchase Qty",
        "serial_numbers": "Serial #",
        "shipping_method": "Ship Method",
        "tracking_numbers": "Tracking Number",
        "location": "Location",
        "expected_ship_date": "Expected Ship Date",
        "unit_price": "Unit Price",
        "unit_cost_in_so": "Unit Cost in SO",
        "ext_price": "Ext Price",
        "ext_cost_in_so": "Ext Cost",
        "opportunity_name": "Opportunity Summary",
        "sales_order_crm_id": "SO#",
        "po_date": "PO Date",
        "so_linked_pos": "LPPO#",
        "customer_po_number": "Customer PO",
        "quantity": "Quantity",
        "line_number": "Line #",
        "unit_of_measure": "UOM",
        "internal_notes_po": "Internal Notes PO",
        "shipment_date": "Ship Date",
        "total": "Total in PO",
        "entered_by": "Entered By",
        "received_date": "Received Date",
        "opportunity_crm_id": "Opportunity #",
        "status_name": "SO Status",
        "vendor_invoice_number": "Vendor Invoice #",
        "shipping_instruction": "Shipping Instructions",
        "order_date": "Order Date",
        "due_date": "Due Date",
        "total_in_so": "SO Total",
        "sub_total": "SO Subtotal",
        "tax_total": "SO Sales Tax",
        "billing_status_name": "Invoice Status",
        "subtotal_in_invoice": "Invoice SubTotal",
        "sales_tax_invoice": "Invoice Sales Tax",
        "total_in_invoice": "Invoice Total",
        "balance": "balance",
        "date": "Invoice Date",
        "due_date_invoice": "Invoice Due Date",
        "payment_date": "Invoice Paid Date",
        "reference": "Invoice Reference",
        "customer_po": "Customer PO",
        "billing_term_name": "Billing Terms",
        "tax_code": "Tax Code",
        "territory_name": "Territory",
        "invoice_type": "Invoice Type",
        "invoice_shipping_price": "Shipping Price on Invoice",
    }


class ProjectAuditCSVRenderer(CSVRenderer):
    header = [
        "Customer Name",
        "Opportunity name",
        "LP PS Revenue",
        "Risk Revenue",
        "LP PS Cost",
        "Travel Revenue",
        "Contractor Revenue",
        "Contractor Cost",
        "Engineering Hours – Standard",
        "Engineering Hours – After Hours",
        "Project Management Hours",
        "Engineering Rate – Standard",
        "Engineering Rate – After Hours",
        "PM Rate",
        "Project Name",
        "Project Start Date",
        "Project End Date",
        "Actual Engineering Hours – Standard",
        "Actual Engineering Hours – After Hours",
        "Actual Project Management Hours",
        "Actual Engineering Cost",
        "Actual PM Cost",
        "Total Project Revenue Billed",
        "Realized Margin",
        "Anticipated Margin",
        "Date Range",
    ]


class DeviceReportPDFRenderer(BaseRenderer):
    """
    Custom PDF Renderer to generate device report pdf
    """

    media_type = "application/pdf"
    format = "pdf"
    charset = None
    render_style = "binary"

    def render(self, data, media_type=None, renderer_context=None):
        """
        Return the PDF as response
        """
        if data:
            file_path = DeviceService.generate_device_report(data)
            if file_path:
                try:
                    with open(file_path, "rb") as pdf:
                        response = HttpResponse(
                            pdf, content_type="application/pdf"
                        )
                        response[
                            "Content-Disposition"
                        ] = 'inline; filename="Device-Report.pdf"'
                    return response
                except (FileNotFoundError, OSError) as e:
                    logger.error(e)
                    return data
            return data
        else:
            return data


class CSVFileRenderer(BaseRenderer):
    """
    CSV file Renderer.

        Usage:
                renderer_classes = (CSVFileRenderer,)
                data = dict(file_path=`file_path`: class(:`str`), file_name=`file_name`: class(:`str`))
                return Response(data=data, status=status.HTTP_200_OK)
    """

    media_type = "application/csv"
    format = "csv"
    charset = "utf-8"
    render_style = "binary"

    def render(self, data, media_type=None, renderer_context=None):
        file_path = data.get("file_path")
        file_name = data.get("file_name")
        if file_path:
            try:
                with open(file_path, "rb") as csv:
                    response = HttpResponse(
                        csv, content_type="application/csv"
                    )
                    response[
                        "Content-Disposition"
                    ] = f'inline; filename="{file_name}"'
                return response
            except (FileNotFoundError, OSError) as e:
                logger.error(e)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class PDFFileRenderer(BaseRenderer):
    """
    PDF file Renderer.
        Usage:
            renderer_classes = (PDFFileRenderer,)
            data = dict(file_path=`file_path`: class(:`str`), file_name=`file_name`: class(:`str`))
            return Response(data=data, status=status.HTTP_200_OK)
    """

    media_type = "application/pdf"
    format = "pdf"
    charset = None
    render_style = "binary"

    def render(self, data, media_type=None, renderer_context=None):
        file_path = data.get("file_path")
        file_name = data.get("file_name")
        if file_path:
            try:
                with open(file_path, "rb") as pdf:
                    response = HttpResponse(
                        pdf, content_type="application/pdf"
                    )
                    response[
                        "Content-Disposition"
                    ] = f'inline; filename="{file_name}"'
                return response
            except (FileNotFoundError, OSError) as e:
                logger.error(e)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)
