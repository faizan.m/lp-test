import os

from dropbox import dropbox

from core.integrations.file_storage.dropbox.utils import (
    handle_error_response,
    parse_files_folders_list,
)


APP_FOLDER_NAME = os.environ.get("DROPBOX_APP_FOLDER_NAME")
ACCESS_TOKEN = os.environ.get("DROPBOX_APP_ACCESS_TOKEN")

from utils import get_app_logger
from core.integrations.file_storage.dropbox.utils import get_valid_folder_name

# Get an instance of a logger
logger = get_app_logger(__name__)


class DropBoxClient(object):
    def __init__(self, access_token=None, app_folder_name=None):
        self.APP_FOLDER_NAME = app_folder_name or APP_FOLDER_NAME
        self.ACCESS_TOKEN = access_token or ACCESS_TOKEN
        self._client = dropbox.Dropbox(self.ACCESS_TOKEN)

    def get_users_get_current_account(self):
        """
        Get information about the current user's account.

        :rtype: :class:`dropbox.users.FullAccount`
        """
        try:
            return self._client.users_get_current_account()
        except Exception as err:
            logger.error(f"User current account listing failed --Error {err}")
            raise err

    @handle_error_response()
    @parse_files_folders_list()
    def get_files_folder_list(self, path="", limit=None, is_root=False):
        """
        Gets the contents of the folder.
        :param str path: A unique identifier for the file.
        :param Nullable limit: The maximum number of results to return per
            request.
        :param bool is_root: Show only Folders when `is_root` is `True`. Default = `False`
        :return: `dict`
        """
        result = []
        try:
            res = self._client.files_list_folder(path, limit=limit)
            result.extend(res.entries)
            if res.has_more:
                res_ = self._client.files_list_folder_continue(res.cursor)
                result.append(res_.entries)
        except Exception as err:
            logger.error(
                f"Folder listing failed for {path} -- assumed empty: {err}"
            )
            raise err
        return result

    @handle_error_response()
    def get_file(self, path):
        """
        Download a file from DropBox.

        :param str path: The path of the file to download.
        :return: `dict`
            data:
             - ``file_path``: Path of the file (`str`).
             - ``size``: Size of the file in kb (`int`).
             - ``file_name``: Name of the file (`str`).
        """
        try:
            res = self._client.files_get_temporary_link(path)
            metadata = res.metadata
            link = res.link
            data = dict(
                file_path=link, size=metadata.size, file_name=metadata.name
            )
            return data
        except Exception as err:
            logger.error(
                f"File download failed for {path} -- assumed empty: {err}"
            )
            raise err

    @handle_error_response()
    def move_files_or_folders(self, from_path, to_path):
        """
        Move files or folders from one directory to new directory
        :param str from_path: The path of the file or folder you want to move.
        :param str to_path: The path where you want to move the file or folder
        """
        try:
            self._client.files_move_v2(from_path, to_path)
        except Exception as err:
            logger.error(
                f"Error while moving folders from {from_path} to {to_path}"
            )
            raise err

    @handle_error_response()
    def create_folders_batch(self, paths):
        try:
            res = self._client.files_create_folder_batch(paths)
            return res
        except Exception as err:
            logger.error(
                f"Error while batch creating Dropbox folders. Folders count: {len(paths)}: Error: {err}"
            )
            raise err

    @handle_error_response()
    def upload_file(self, data, path, mode=None):
        try:
            if mode:
                res = self._client.files_upload(data, path, mode)
            else:
                res = self._client.files_upload(data, path)
            return res
        except Exception as err:
            print(err)
            logger.error(
                f"Error while uploading file in folder for {path} -- assumed empty: {err}"
            )
            raise err

    @staticmethod
    def get_dropbox_customer_folder_name(customer):
        customer_name = get_valid_folder_name(customer.name)
        return f"{customer_name}-{customer.crm_id}"

    def get_customer_general_folder_path(self, customer, file_name):
        customer_folder_name = self.get_dropbox_customer_folder_name(customer)
        return f"/{self.APP_FOLDER_NAME}/{customer_folder_name}/General/{file_name}"

    def get_customer_audit_folder_path(
        self, ROOT_FOLDER, AUDIT_FOLDER, customer
    ):
        customer_folder_name = self.get_dropbox_customer_folder_name(customer)
        return f"/{self.APP_FOLDER_NAME}/{ROOT_FOLDER}/{AUDIT_FOLDER}/{customer_folder_name}"
