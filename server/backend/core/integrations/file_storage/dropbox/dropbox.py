from core.integrations.file_storage.dropbox.DropBoxClient import DropBoxClient


class DropBox:
    def __init__(self, authentication_config=None):
        authentication_config = authentication_config or {}
        app_folder_name = authentication_config.get("app_folder_name")
        access_token = authentication_config.get("access_token")
        self.client = self._get_client(access_token, app_folder_name)

    @staticmethod
    def _get_client(access_token, app_folder_name):
        client = DropBoxClient(access_token, app_folder_name)
        return client

    def validate_api_credentials(self):
        """
        This functions checks if the DropBox API credentials entered are valid or not.
        Returns
        :rtype: :class:`bool`

        Returns ``True`` if the API credentials are valid else ``False``
        """
        try:
            self.check_connection()
            return True
        except:
            return False

    def check_connection(self):
        return self.client.get_users_get_current_account()
