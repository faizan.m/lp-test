import functools

from dropbox.exceptions import BadInputError, ApiError, HttpError
from dropbox.stone_validators import ValidationError

from core.utils import chunks_of_list_with_max_item_count
from exceptions.exceptions import build_error


def handle_error_response():
    def decorator(func):
        """
        Decorator to handle DropBox error responses and raise `rest_framework.exceptions.APIException`.
        """

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            source = "DROPBOX"
            try:
                return func(self, *args, **kwargs)
            except BadInputError as err:
                raise build_error(
                    err.status_code, source, message=err.message, errors=None
                )
            except ValidationError as err:
                raise build_error(
                    400, source, message=err.message, errors=None
                )
            except ApiError as err:
                return []
            except HttpError as err:
                raise build_error(err.status_code, message=None, errors=None)
            except Exception as err:
                raise build_error(err, source, message="", errors=None)

        return wrapper

    return decorator


def get_valid_folder_name(name):
    if name:
        new_char = "-"
        name = (
            name.replace("\\", new_char)
            .replace("/", new_char)
            .replace(":", new_char)
            .replace("?", new_char)
            .replace("*", new_char)
            .replace('"', new_char)
            .replace("|", new_char)
        )
    return name


def parse_files_folders_list():
    def decorator(func):
        """
        Decorator to parse DropBox files and folders responses. Show only Folders when `is_root` is `True`
        """

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            data = func(self, *args, **kwargs)
            root = kwargs.get("is_root")
            result_dict = {}
            for _item in data:
                if "FolderMetadata" in str(type(_item)):
                    _path = (
                        _item.path_lower
                        if root
                        else _item.path_lower.split("/", 3)[3:][0]
                    )
                    _value = dict(value=_path, type="Folder")
                    result_dict[_item.name] = _value
                elif not root and "FileMetadata" in str(type(_item)):
                    _path = _item.path_lower.split("/", 3)[3:][0]
                    _value = dict(value=_path, type="File", size=_item.size)
                    result_dict[_item.name] = _value
            return result_dict

        return wrapper

    return decorator
