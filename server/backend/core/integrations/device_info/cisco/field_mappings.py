MigrationInformation = {
    "MigrationInformation": "migration_info",
    "MigrationProductId": "migration_product_id",
    "MigrationProductName": "migration_product_name",
}

DeviceEOXRecord = {
    "EndOfServiceContractRenewal": {"value": "EOL_date"},
    "EndOfSvcAttachDate": {"value": "EOSA_date"},
    "EndOfSaleDate": {"value": "EOS_date"},
    "EndOfSWMaintenanceReleases": {"value": "EOSWS_date"},
    "EndOfSecurityVulSupportDate": {"value": "EOSU_date"},
    "LastDateOfSupport": {"value": "LDOS_date"},
    "EOLProductID": "product_id",
    "EOXInputValue": "serial_number",
    "LinkToProductBulletinURL": "product_bulletin_url",
    "EOXMigrationDetails": MigrationInformation,
}


DeviceProductInfo = {"product_type": "type"}

SubscriptionData = {
    "initialTerm": "term_length",
    "autoRenTerm": "auto_renewal_term",
    "billingModel": "billing_model",
    "subscriptionReferenceID": "serial_number",
    "startDate": "installation_date",
    "endDate": "expiration_date",
}

ProductId = {
    "identifier": "ProductId",
}

ShipmentName = {
    "name": "Shipment Method",
}

PurchaseOrderHistoryData = {
    "description": "Description",
    "product": ProductId,
    "quantity": "Quantity",
    "purchaseOrderNumber": "Purchase order number",
    "purchaseOrderDate": "Purchase order date",
    "received": "Received",
    "dateReceived": "Date Received",
    "shipDate": "Ship Date",
    "receivedQuantity": "Received Quantity",
    "serialNumbers": "Serial Numbers",
    "trackingNumber": "Tracking Number",
    "shipmentMethod": ShipmentName,
    "vendorInvoiceNumber": "Vendor Invoice Number",
    "customer_po_number": "Customer PO Number",
}
