import functools

import requests

from core.integrations.device_info.cisco.exceptions import (
    APILimitExceeded,
    ResourceNotFound,
    CiscoAPIError,
    AuthorizationError,
)
from core.utils import (
    chunks_of_list_with_max_item_count,
    chunks_of_list_with_max_items_length,
)


def handle_cisco_api_exception(func):
    """
    Decorator to handle cisco api error
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            data = func(*args, **kwargs)
        except AuthorizationError:
            data = {}
        except APILimitExceeded as e:
            raise e
        except ResourceNotFound as e:
            raise e
        except CiscoAPIError as e:
            raise e
        except requests.exceptions.ConnectionError:
            data = {}
        return data

    return wrapper


def batch_call(max_count=None):
    def decorator(func):
        """
        Decorator to handle batch call. Takes a :paramater max_count which is the max
        items allowed in one api call.
        """

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            serial_numbers = args[0]
            data = []
            if isinstance(serial_numbers, list):
                if max_count:
                    for serial_numbers in chunks_of_list_with_max_item_count(
                        serial_numbers, max_count
                    ):
                        input = ",".join(serial_numbers)
                        data.extend(func(self, input, *args, **kwargs))
                else:
                    for serial_numbers in chunks_of_list_with_max_items_length(
                        serial_numbers, 1500
                    ):
                        input = ",".join(serial_numbers)
                        data.extend(func(self, input, *args, **kwargs))
            else:
                data = func(self, *args, **kwargs)
            return data

        return wrapper

    return decorator
