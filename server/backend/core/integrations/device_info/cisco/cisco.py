import copy

import requests

from core.integrations.device_info.base import DeviceManufacturerBase
from core.integrations.device_info.cisco.client import CiscoClient
from core.integrations.device_info.cisco.field_mappings import (
    DeviceEOXRecord,
    DeviceProductInfo,
    SubscriptionData,
)
from core.integrations.device_info.cisco.utils import batch_call
from core.integrations.utils import (
    prepare_response,
    prepare_response_for_software_suggestions,
)
from exceptions.exceptions import (
    APIError,
    AuthenticationError,
    AuthorizationError,
)
from utils import get_app_logger
from datetime import datetime
from core.utils import chunks_of_list_with_max_item_count

# Get an instance of a logger
logger = get_app_logger(__name__)


class Cisco(DeviceManufacturerBase):
    def __init__(self, authentication_config=None):
        super().__init__(authentication_config)
        authentication_config = authentication_config or {}
        client_id = authentication_config.get("client_id")
        client_secret = authentication_config.get("client_secret")
        self.client = self._get_client(client_id, client_secret)

    @staticmethod
    def _get_client(client_id, client_secret):
        try:
            return CiscoClient(client_id, client_secret)
        except AuthenticationError:
            return None
        except (
            requests.exceptions.ConnectionError,
            TimeoutError,
            requests.exceptions.Timeout,
            requests.exceptions.ConnectTimeout,
            requests.exceptions.ReadTimeout,
        ) as e:
            return None

    @batch_call(20)
    def get_device_eox_info(self, serial_numbers, *args, **kwargs):
        """
        Returns Device EOX data from Cisco API using Serial Numbers.

        :param serial_numbers: Serial Number of the device.
        :return:
        """
        logger.debug(
            "Getting eox data for serial numbers {0}".format(serial_numbers)
        )
        response = self.client.get_device_eox_info(serial_numbers)
        eox_records = response.json()["EOXRecord"]
        eox_records = list(
            filter(
                lambda eox_record: not eox_record.get("EOXError"), eox_records
            )
        )
        data = prepare_response(eox_records, DeviceEOXRecord)
        result = []
        for item in data:
            serial_numbers = item.get("serial_number").split(",")
            if len(serial_numbers) > 1:
                for serial_number in serial_numbers:
                    item_copy = copy.deepcopy(item)
                    item_copy["serial_number"] = serial_number
                    result.append(item_copy)
            else:
                result.append(item)
        logger.debug(
            "Returned EOX data for serial numbers: {0}".format(serial_numbers)
        )
        return result

    def get_device_search_lines(self, serial_numbers, *args, **kwargs):
        """
        Returns the Device CCWR Search Lines from Cisco API using Serial Numbers.
        :param serial_numbers: Serial Numbers of the devices.
        :return:
        """
        if self.client:
            logger.debug(
                "Getting CCWR Search data for serial numbers {0}".format(
                    serial_numbers
                )
            )
            result = self.client.get_device_search_lines(serial_numbers)
            return result
        else:
            return

    def _get_product_info(self, product_id):
        response = self.client.get_device_product_info(product_id)
        data = response.json()["product_list"]
        return data

    def get_device_product_info(self, product_id):
        """
        Returns Device Product Info data using product id
        """
        data = self._get_product_info(product_id)
        data = list(filter(lambda item: not item.get("ErrorResponse"), data))
        data = prepare_response(data, DeviceProductInfo)
        return data

    def _get_product_software_suggestion(self, product_id):
        response = self.client.get_product_software_suggestions(product_id)
        data = response.json()["productList"]
        return data

    @batch_call(6)
    def get_product_software_suggestion(self, product_id, *args, **kwargs):
        """
        Returns Device Product Info data using product id
        """
        data = self._get_product_software_suggestion(product_id)
        data = list(filter(lambda item: not item.get("ErrorResponse"), data))
        data = prepare_response_for_software_suggestions(data)
        return data

    def get_subscription_details(self, subscription_ids, *args, **kwargs):
        """
        Returns the Subscription details for subscription reference ids.
        :param subscription_ids: Subscription id .
        :return:
        """
        if self.client:
            logger.debug(
                "Getting Subscription details for subscription Ids {0}".format(
                    subscription_ids
                )
            )
            if len(subscription_ids) <= 500:
                result = self.client.get_subscription_details(subscription_ids)
                if not result.get("subscriptions"):
                    logger.debug(
                        "No Subscriptions found in CISCO and the API response is {0}".format(
                            result
                        )
                    )
                result = result.get("subscriptions")
            else:
                result = []
                _subscription_ids = list(
                    chunks_of_list_with_max_item_count(subscription_ids, 500)
                )
                for _subscription_id in _subscription_ids:
                    result_dict = {}
                    _result = self.client.get_subscription_details(
                        _subscription_id
                    )
                    if _result and _result.get("subscriptions"):
                        result_dict["header"] = _result.get("subscriptions")[
                            0
                        ].get("header")
                        result.append(result_dict)
                    else:
                        logger.debug(
                            "No Subscriptions found in CISCO and the API response is {0}".format(
                                _result
                            )
                        )
            if result:
                data = []
                for subscription_data in result:
                    result = prepare_response(
                        subscription_data.get("header"), SubscriptionData
                    )
                    if result.get("installation_date"):
                        result["installation_date"] = datetime.strptime(
                            result.get("installation_date"), "%Y-%m-%d"
                        )
                    if result.get("expiration_date"):
                        result["expiration_date"] = datetime.strptime(
                            result.get("expiration_date"), "%Y-%m-%d"
                        )
                    data.append(result)
                return data
        else:
            return

    def validate_api_credentials(self):
        if (
            self.client
            and self.check_connection(eox=True)
            and self.check_connection(product_info=True)
            and self.check_connection(soft_suggestion=True)
            and self.check_connection(search_line=True)
            and self.check_connection(subscription_details=True)
        ):
            return True
        return False

    def check_connection(
        self,
        eox=False,
        product_info=False,
        soft_suggestion=False,
        search_line=False,
        subscription_details=False,
    ):
        """
        Checks connection status for API.
        :param product_info: : Set True to check product info api connection
        :param eox: Set True to check EOX API connection
        :param soft_suggestion:  Set True to Check Software Suggestion API connection
        :param search_line:  Set True to Check CCWR API connection
        :param subscription_details: Set True to check Subscription details API connection
        :return:
        """
        if (
            not [
                eox,
                product_info,
                soft_suggestion,
                search_line,
                subscription_details,
            ].count(True)
            == 1
        ):
            raise ValueError(
                "Only one of the Eox, product_info, soft_suggestion, search_line and subscription_details value can be True."
            )
        test_serial_number = "PSZ18241L91"
        product_id = "CISCO2901-SEC/K9"
        test_subscription_id = "Sub745078"
        data = {}
        error_message = ""
        try:
            if eox:
                data = self.get_device_eox_info(test_serial_number)
                error_message = "EOX permissions not set."
            elif product_info:
                data = self.get_device_product_info(product_id)
                error_message = "Product Info permissions not set."
            elif soft_suggestion:
                data = self.get_product_software_suggestion(product_id)
                error_message = "Software Suggestion permissions not set."
            elif search_line:
                data = self.get_device_search_lines([test_serial_number])
                error_message = "CCWR Contract permissions not set."
            elif subscription_details:
                data = self.get_subscription_details([test_subscription_id])
                error_message = "Subscription details permissions not set."
            return True if data else False
        except AuthorizationError as e:
            e.detail = error_message or e.detail
            raise APIError(code=401, source="CISCO", message=e.detail)
