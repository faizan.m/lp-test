class CiscoAPIException(Exception):
    pass


class CiscoAPIError(CiscoAPIException):
    def __init__(self, http_status, error_code=None, message=None):
        self.status = http_status
        self.error_code = error_code
        self.message = message

    def __str__(self):
        return f"{self.status}:{self.error_code}:{self.message}"


class AuthenticationError(CiscoAPIError):
    def __init__(self):
        super(AuthenticationError, self).__init__(
            401, "invalid_credentials", "API Credentials are Invalid."
        )


class AuthorizationError(CiscoAPIError):
    def __init__(self):
        super(AuthorizationError, self).__init__(
            403,
            "unauthorized_access",
            "You do not have permission to perform this action.",
        )


class APILimitExceeded(CiscoAPIError):
    def __init__(self):
        super(APILimitExceeded, self).__init__(
            429, "api_limit_exceeded", "API call Limit Exceeded"
        )


class ResourceNotFound(CiscoAPIError):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
