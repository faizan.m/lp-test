# Client to interact with Cisco EOX API.
import datetime
import json
import os
import time

from backoff import on_exception, expo
import requests
from ratelimit import limits, sleep_and_retry
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from exceptions.exceptions import (
    build_error,
    AuthenticationError,
    ServiceUnavailableError,
)
from rate_limiter.utils import rate_limiter, cisco_rate_limiter
from utils import get_app_logger

BASE_URL = os.environ.get("CISCO_BASE_URL", "https://apix.cisco.com")
CLIENT_ID = os.environ.get("CISCO_CLIENT_ID")
CISCO_SECRET = os.environ.get("CISCO_CLIENT_SECRET")
AUTH_URL = "https://id.cisco.com/oauth2/default/v1/token"
# Get an instance of a logger
logger = get_app_logger(__name__)


class CiscoClient(object):
    """
    Performs requests to the Cisco EOX API's.
    """

    EOX_BY_SERIAL_BASE_URL = "{api_base_url}/supporttools/eox/rest/5/EOXBySerialNumber/{page_index}/{serial_number}"
    DEVICE_PRODUCT_INFO_BY_PRODUCT_IDS_BASE_URL = (
        "{api_base_url}/product/v1/information/product_ids/{input_product_ids}"
    )
    DEVICE_SOFTWARE_SUGGESTION_BY_PRODUCT_IDS = "{api_base_url}/software/suggestion/v2/suggestions/software/productIds/{input_product_ids}"
    DEVICE_CCWR_SEARCH_LINE = (
        "{api_base_url}/ccw/renewals/api/v1.0/search/lines"
    )
    SUBSCRIPTION_DETAILS_URL = "{api_base_url}/ccw/subscriptionmanagement/api/v1.0/sub/subscriptionDetails"

    def __init__(self, client_id=None, client_secret=None):
        """
        :param client_id: CISCO API application client Id
        :param client_secret: CISCO API application client Secret
        """

        self.client_id = client_id or CLIENT_ID
        self.client_secret = client_secret or CISCO_SECRET

        if not (self.client_id and self.client_secret):
            raise ValueError("Must provide client_id and client_secret.")
        self.auth_token = None
        self.session = None
        self.token_expiry = None
        self.create_session()

    def create_session(self):
        self.auth_token, expiry = self._generate_auth_token(
            self.client_id, self.client_secret
        )
        self.token_expiry = datetime.datetime.now() + datetime.timedelta(
            seconds=expiry
        )
        self.session = self._build_session(self.auth_token)

    @staticmethod
    def _generate_auth_token(client_id, client_secret):
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        data = {
            "client_id": client_id,
            "client_secret": client_secret,
            "grant_type": "client_credentials",
        }

        response = requests.post(AUTH_URL, data=data, headers=headers)
        if response.status_code == 401:
            raise AuthenticationError(source="CISCO")
        #  Response format
        # {"access_token": "BTHN3cAzbRUitXVI1sBPBGVSn9ol", "token_type": "Bearer", "expires_in": 3599}
        token = response.json().get("access_token")
        expiry = response.json().get("expires_in")
        return token, expiry

    def _build_session(self, auth_token):
        """
        Build session to make request to CW api's
        :param auth_token: authentication token for CW Developer account
        :type auth_token: string
        :return: session object
        """
        _default_headers = {
            "Authorization": "Bearer %s" % auth_token,
            "Content-Type": "application/json",
        }
        self.requests_kwargs = {}
        self.requests_kwargs.update(
            {
                "headers": _default_headers,
                "verify": False,  # NOTE(cbro): verify SSL certs.
                "timeout": 30,
            }
        )

        session = requests.Session()
        retry = Retry(total=3, backoff_factor=0.1)

        adapter = HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        session.headers = _default_headers
        return session

    def _request(self, url, method=None, headers=None, data=None):
        if self.token_expiry <= datetime.datetime.now():
            self.create_session()
        if method == "POST":
            self.session.headers.update(headers)
            response = self.session.post(url, data=data)
            return self._handle_response(response)
        response = self.session.get(url)
        logger.debug("Cisco client request url: {0}".format(response.url))
        return self._handle_response(response)

    def _handle_response(self, response):
        """Internal helper for handling API responses from the Cisco server.

        Raises the appropriate exceptions when necessary; otherwise, returns the
        response.
        """
        if not str(response.status_code).startswith("2"):
            raise self._build_api_error(response)
        elif response.json().get("EOXError"):
            raise self._build_api_error(
                response,
                response.json()["EOXError"]["ErrorID"],
                response.json()["EOXError"]["ErrorDescription"],
            )
        return response

    def _build_api_error(self, response, message=None, error_list=None):
        """
        Helper method for creating errors and attaching HTTP response/request
        details to them.
        """
        if response.status_code == 403:
            if response.headers.get("X-Mashery-Error-Code") in (
                "ERR_403_DEVELOPER_OVER_RATE",
                "ERR_403_DEVELOPER_OVER_QPS",
            ):
                response.status_code = 429
        return build_error(
            response.status_code,
            source="CISCO",
            message=message,
            errors=error_list,
        )

    @sleep_and_retry
    @limits(calls=5, period=1)
    @cisco_rate_limiter(
        key="EOX_SUMMARY",
        day=4500,
        month=30 * 4500,
        raise_exception=True,
    )
    def get_device_eox_info(self, serial_number, page_index=1):
        url = self.EOX_BY_SERIAL_BASE_URL.format(
            api_base_url=BASE_URL,
            page_index=page_index,
            serial_number=serial_number,
        )

        response = self._request(url)
        return response

    @sleep_and_retry
    @limits(calls=2, period=1)
    @cisco_rate_limiter(
        key="PRODUCT_INFO",
        day=90000,
        month=30 * 90000,
        raise_exception=False,
    )
    def get_device_product_info(self, product_ids, *args, **kwargs):
        url = self.DEVICE_PRODUCT_INFO_BY_PRODUCT_IDS_BASE_URL.format(
            api_base_url=BASE_URL, input_product_ids=product_ids
        )
        response = self._request(url)
        return response

    @sleep_and_retry
    @limits(calls=10, period=1)
    @cisco_rate_limiter(
        key="SOFTWARE_SUGGESTION",
        day=4500,
        month=30 * 4500,
        raise_exception=True,
    )
    def get_product_software_suggestions(self, product_ids, *args, **kwargs):
        url = self.DEVICE_SOFTWARE_SUGGESTION_BY_PRODUCT_IDS.format(
            api_base_url=BASE_URL, input_product_ids=product_ids
        )
        response = self._request(url)
        return response

    @sleep_and_retry
    @limits(calls=60, period=60)
    @cisco_rate_limiter(
        key="SEARCH_LINES",
        hour=27 * 10,
        day=24 * 27 * 10,
        month=30 * 24 * 27 * 10,
        raise_exception=True,
    )
    def get_device_search_lines(self, serial_numbers, *args, **kwargs):
        if not isinstance(serial_numbers, list):
            serial_numbers = [serial_numbers]
        url = self.DEVICE_CCWR_SEARCH_LINE.format(api_base_url=BASE_URL)
        headers = {
            "Authorization": "Bearer %s" % self.auth_token,
            "Content-Type": "application/json",
            "Request-ID": "20199345",
        }
        data = {"serialNumbers": serial_numbers}
        response = self._request(
            url, method="POST", headers=headers, data=json.dumps(data)
        )
        response = response.json()
        return response.get("instances")

    @sleep_and_retry
    @limits(calls=10, period=1)
    @cisco_rate_limiter(
        key="SUBSCRIPTION_DETAILS",
        day=100,
        month=30 * 100,
        raise_exception=True,
    )
    def get_subscription_details(self, subscription_ids, *args, **kwargs):
        url = self.SUBSCRIPTION_DETAILS_URL.format(api_base_url=BASE_URL)
        request_id = f"{self.client_id}-{int(time.time())}"
        headers = {
            "Request-ID": request_id,
        }
        data = {"subscriptionReferenceID": subscription_ids}
        response = self._request(
            url, method="POST", headers=headers, data=json.dumps(data)
        )
        response = response.json()
        return response
