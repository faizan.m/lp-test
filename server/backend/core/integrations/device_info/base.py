"""
This module contains Base class for Device Info Service Integration.
"""
from abc import ABC, abstractmethod


class DeviceManufacturerBase(ABC):
    def __init__(self, authentication_config):
        pass

    @abstractmethod
    def get_device_eox_info(self, serial_number):
        """
        Returns device information for the serial number.
        :param serial_number: serial number of the device.
        :return:
        """

        pass
