import functools

# get logger instance
import time

from celery.utils.log import get_task_logger
from logicmonitor_sdk.rest import ApiException

from exceptions.exceptions import build_error

logger = get_task_logger(__name__)


def handle_logic_monitor_api_exception(wait_window=60):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            while True:
                try:
                    return func(*args, **kwargs)
                except ApiException as e:
                    if e.status == 429:
                        logger.info("API Limit Exceeded")
                        time.sleep(float(wait_window))
                    else:
                        raise build_error(e.status, "Logic Monitor", e.body)
                except Exception as e:
                    logger.error("Logic Monitor Error: {}".format(e))
                    return {}
                return {}

        return wrapper

    return decorator


def enable_pagination(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            response = []
            count, total = 0, 0
            while True:
                devices = func(*args, **kwargs, offset=count)
                total = total if total else devices.total
                count = (
                    len(devices.items)
                    if count == 0
                    else (count + len(devices.items))
                )
                response.extend(devices.items)
                if count >= total:
                    break
            return response
        except Exception as e:
            raise e

    return wrapper


def map_logic_monitor_name_value(properties):
    return dict(map(lambda x: (x.name, x.value), properties))
