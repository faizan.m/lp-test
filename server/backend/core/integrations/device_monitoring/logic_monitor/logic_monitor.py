import os

# Get an instance of a logger
import re

import logicmonitor_sdk

from core.integrations.device_monitoring.logic_monitor.utils import (
    handle_logic_monitor_api_exception,
    enable_pagination,
    map_logic_monitor_name_value,
)
from core.utils import group_list_of_dictionary_by_key
from utils import get_app_logger

logger = get_app_logger(__name__)

COMPANY_NAME = os.environ.get("COMPANY", "LookingPoint")
ACCESS_ID = os.environ.get("LOGIC_MONITOR_ACCESS_ID", "47s5sI3UStMTd76C87ia")
ACCESS_KEY = os.environ.get(
    "LOGIC_MONITOR_ACCESS_KEY", "^xsz$)93[4F9^!(}8+w25J(]b5^uJ9Y}]58Q~849"
)


class LogicMonitor(object):
    def __init__(self, authentication_config=None, extra_config=None):
        authentication_config = authentication_config or {}
        name = authentication_config.get("name")
        access_id = authentication_config.get("access_id")
        access_key = authentication_config.get("access_key")
        self.access_id = access_id or ACCESS_ID
        self.access_key = access_key or ACCESS_KEY
        self.company_name = name or COMPANY_NAME
        self.extra_config = extra_config or {}
        if not (self.access_id and self.access_key and self.company_name):
            raise ValueError(
                "Must provide access_id, access_key and company_name."
            )
        self.client = self._get_client()

    def _get_client(self):
        configuration = logicmonitor_sdk.Configuration()
        configuration.company = self.company_name
        configuration.access_id = self.access_id
        configuration.access_key = self.access_key
        return logicmonitor_sdk.LMApi(
            logicmonitor_sdk.ApiClient(configuration)
        )

    def validate_api_credentials(self):
        try:
            self.client.get_api_token_list()
            return True
        except:
            return False

    @classmethod
    def default_key_mappings(cls):
        cisco_devices_key_mappings = {
            "host_name": {"name": "Host Name", "value": ["system.hostname"]},
            "os_version": {
                "name": "OS Version",
                "value": ["auto.short_sysinfo"],
            },
            "display_name": {
                "name": "Device Name",
                "value": ["system.displayname"],
            },
            "sys_name": {"name": "System Name", "value": ["system.sysname"]},
            "regex_field": {
                "name": "Regex Field",
                "value": ["auto.cisco.description"],
            },
            "categories": {
                "name": "Categories",
                "value": ["system.categories"],
            },
            "serial_number": {
                "name": "Serial Number",
                "value": ["auto.cisco.serial_number"],
            },
        }
        non_cisco_devices_key_mappings = {
            "host_name": {"name": "Host Name", "value": ["system.hostname"]},
            "categories": {
                "name": "Categories",
                "value": ["system.categories"],
            },
            "model": {"name": "Model", "value": ["auto.endpoint.model"]},
            "display_name": {
                "name": "Device Name",
                "value": ["system.sysname"],
            },
            "sys_name": {"name": "System Name", "value": ["system.sysname"]},
            "os_version": {
                "name": "OS Version",
                "value": ["auto.entphysical.softwarerev"],
            },
            "regex_field": {
                "name": "Regex Field",
                "value": ["auto.description"],
            },
            "serial_number": {
                "name": "Serial Number",
                "value": ["auto.endpoint.serial_number"],
            },
        }
        return dict(
            cisco_devices_key_mappings=cisco_devices_key_mappings,
            non_cisco_devices_key_mappings=non_cisco_devices_key_mappings,
        )

    @enable_pagination
    def _get_groups(self, offset=0):
        groups = self.client.get_device_group_list(offset=offset)
        return groups

    @enable_pagination
    def _get_devices(self, offset=0):
        devices = self.client.get_device_list(offset=offset)
        return devices

    @enable_pagination
    def _get_devices_by_group(self, group_id, offset=0):
        devices = self.client.get_immediate_device_list_by_device_group_id(
            group_id, offset=offset
        )
        return devices

    @enable_pagination
    def _get_device_datasource_list(
        self, device_id, device_datasource_filter, offset=0
    ):
        if device_datasource_filter:
            result = self.client.get_device_datasource_list(
                device_id, offset=offset, filter=device_datasource_filter
            )
        else:
            result = self.client.get_device_datasource_list(
                device_id, offset=offset
            )
        return result

    @enable_pagination
    def _get_device_group_list(self, offset=0):
        return self.client.get_device_group_list(offset=offset)

    @enable_pagination
    def _get_device_config_source_config_list(
        self, device_id, data_source_id, data_source_instance_id, offset=0
    ):
        return self.client.get_device_config_source_config_list(
            device_id, data_source_id, data_source_instance_id, offset=offset
        )

    def _get_device_datasource_instance_list(
        self, device_id, data_source_id, device_datasource_instance_filter
    ):
        if device_datasource_instance_filter:
            return self.client.get_device_datasource_instance_list(
                device_id,
                data_source_id,
                filter=device_datasource_instance_filter,
            )
        else:
            return self.client.get_device_datasource_instance_list(
                device_id, data_source_id
            )

    @enable_pagination
    def _get_alerts_by_group_list(self, group_id, offset=0):
        return self.client.get_alert_list_by_device_group_id(
            group_id, offset=offset
        )

    @handle_logic_monitor_api_exception()
    def get_device_groups(self):
        groups_dict = {
            group.name: group.id for group in self._get_device_group_list()
        }
        return groups_dict

    @handle_logic_monitor_api_exception()
    def devices_by_group_id(self, group_id):
        return self._get_devices_by_group(group_id)

    @handle_logic_monitor_api_exception()
    def get_system_categories_for_group(self, group_id):
        group_devices = self.devices_by_group_id(group_id)
        return self._parse_system_categories_from_devices(group_devices)

    @handle_logic_monitor_api_exception()
    def get_device_config_source_config_list(
        self, device_id, data_source_id, data_source_instance_id
    ):
        return self._get_device_config_source_config_list(
            device_id, data_source_id, data_source_instance_id
        )

    @handle_logic_monitor_api_exception()
    def get_customer_group_mappings(self):
        group_customer_mapping = {}
        groups = self._get_device_group_list()
        for i, group in enumerate(groups):
            if group.custom_properties:
                custom_properties = map_logic_monitor_name_value(
                    group.custom_properties
                )
                if custom_properties.get("connectwisev2.companyid"):
                    group_customer_mapping[
                        custom_properties.get("connectwisev2.companyid")
                    ] = group.id
        return group_customer_mapping

    @handle_logic_monitor_api_exception()
    def get_alerts_by_group_id(self, group_id):
        alerts = self._get_alerts_by_group_list(group_id)
        return alerts

    def get_logic_monitor_device_list(self, lm_config):
        """
        Steps
        1. Get all devices by group id
        2. Filter the devices based on the system categories
        3. Get all the device instances and match the regex with the instance description
        """
        group_id = lm_config.get("group_id")
        system_categories = lm_config.get("system_categories")
        key_mappings = lm_config.get("key_mappings")
        lm_devices = []
        if all([group_id, system_categories, key_mappings]):
            response = self.devices_by_group_id(group_id)
            lm_devices = self._filter_devices_by_system_categories(
                response, system_categories, key_mappings
            )

        result = []
        for _i, _devices_by_company_id in group_list_of_dictionary_by_key(
            lm_devices, "company_id"
        ).items():
            for (
                _j,
                _devices_by_serial_number,
            ) in group_list_of_dictionary_by_key(
                _devices_by_company_id, "serial_number"
            ).items():
                result.append(_devices_by_serial_number[0])
        return result

    @handle_logic_monitor_api_exception()
    def get_device_datasource_list(
        self, device_id, device_datasource_filter=None
    ):
        result = self._get_device_datasource_list(
            device_id, device_datasource_filter
        )
        return result

    @handle_logic_monitor_api_exception()
    def get_device_datasource_instance_list(
        self, device_id, data_source_id, device_datasource_instance_filter=None
    ):
        return self._get_device_datasource_instance_list(
            device_id, data_source_id, device_datasource_instance_filter
        )

    def _filter_devices_by_system_categories(
        self, devices, system_categories, key_mappings
    ):
        """
        Here based on each of the category we perform the necessary extraction of the
        serial number and other data from devices
        """
        categories = group_list_of_dictionary_by_key(system_categories, "name")
        lm_devices = []
        version_regex = r"Version.*"
        system_categories = list(categories.keys())
        for device in devices:
            system_properties = map_logic_monitor_name_value(
                device.system_properties
            )
            auto_properties = map_logic_monitor_name_value(
                device.auto_properties
            )
            inherited_properties = map_logic_monitor_name_value(
                device.inherited_properties
            )
            _system_category_value = self._get_key_value_from_properties(
                key_mappings.get("categories", {}).get("value"),
                system_properties,
                auto_properties,
                inherited_properties,
            )
            if not _system_category_value:
                continue
            _device_system_categories = _system_category_value
            _device_system_categories = (
                _device_system_categories.split(",")
                if _device_system_categories
                else []
            )
            common_categories = set(_device_system_categories).intersection(
                set(system_categories)
            )
            if common_categories:
                _regex_filter = []
                _non_regex_filter = []
                for common_category in list(common_categories):
                    _value = categories.get(common_category)[0].get("value")
                    _not_value = categories.get(common_category)[0].get(
                        "not_value"
                    )
                    _value = _value or []
                    _not_value = _not_value or []
                    _regex_filter.extend(_value)
                    _non_regex_filter.extend(_not_value)
                _regex_filter = [x.strip() for x in _regex_filter]
                _non_regex_filter = [x.strip() for x in _non_regex_filter]
                os_version = self._get_key_value_from_properties(
                    key_mappings.get("os_version", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                if os_version:
                    match = re.search(version_regex, os_version)
                    os_version = match[0] if match else os_version
                host_name = self._get_key_value_from_properties(
                    key_mappings.get("host_name", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                display_name = self._get_key_value_from_properties(
                    key_mappings.get("display_name", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                manufacturer = self._get_key_value_from_properties(
                    key_mappings.get("manufacturer", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                model = self._get_key_value_from_properties(
                    key_mappings.get("model", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                description = self._get_key_value_from_properties(
                    key_mappings.get("description", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )
                sys_name = self._get_key_value_from_properties(
                    key_mappings.get("sys_name", {}).get("value"),
                    system_properties,
                    auto_properties,
                    inherited_properties,
                )

                up_time = device.up_time_in_seconds
                company_id = inherited_properties.get(
                    "connectwisev2.companyid"
                )
                if all([display_name, company_id]):
                    serial_number = None
                    if any([_regex_filter, _non_regex_filter]):
                        regex_field = self._get_key_value_from_properties(
                            key_mappings.get("regex_field", {}).get("value"),
                            system_properties,
                            auto_properties,
                            inherited_properties,
                        )
                        if regex_field:
                            _regex_match_condition = re.search(
                                "|".join(_regex_filter), regex_field
                            )
                            _regex_no_match_condition = (
                                re.search(
                                    "|".join(_non_regex_filter), regex_field
                                )
                                if _non_regex_filter
                                else False
                            )
                            if (
                                _regex_match_condition
                                and not _regex_no_match_condition
                            ):
                                serial_number = (
                                    self._get_key_value_from_properties(
                                        key_mappings.get(
                                            "serial_number", {}
                                        ).get("value"),
                                        system_properties,
                                        auto_properties,
                                        inherited_properties,
                                    )
                                )
                    else:
                        serial_number = self._get_key_value_from_properties(
                            key_mappings.get("serial_number", {}).get("value"),
                            system_properties,
                            auto_properties,
                            inherited_properties,
                        )
                    logger.info(
                        f"Found device with Device Id: {device.id}, Serial Number: {serial_number}, Device Name: {display_name}"
                    )
                    data = dict(
                        company_id=int(company_id),
                        serial_number=serial_number,
                        device_name=display_name,
                        os_version=os_version,
                        up_time=up_time,
                        sys_name=sys_name,
                        host_name=host_name,
                        manufacturer=manufacturer,
                        model=model,
                        description=description,
                        regex_filter=_regex_filter,
                        non_regex_filter=_non_regex_filter,
                        lm_device_id=device.id,
                    )
                    lm_devices.append(data)
                    lm_devices.extend(
                        self.get_instance_devices(device.id, data)
                    )

        return lm_devices

    def _get_device_instances_from_data_source(
        self, device_data, data_source_id, lm_device_id
    ):
        lm_devices = []
        result = self.get_device_datasource_instance_list(
            lm_device_id, data_source_id
        )
        if result and result.items:
            device_instances = result.items
        else:
            return lm_devices
        _regex_filter = device_data.get("regex_filter")
        _non_regex_filter = device_data.get("non_regex_filter")
        _regex_filter = [x.strip() for x in _regex_filter]
        _non_regex_filter = [x.strip() for x in _non_regex_filter]
        for device_instance in device_instances:
            name = device_instance.name
            if "device_component_inventory" not in name.lower():
                continue
            device_description = device_instance.description
            _regex_match_condition = re.search(
                "|".join(_regex_filter), device_description
            )
            _regex_no_match_condition = (
                re.search("|".join(_non_regex_filter), device_description)
                if _non_regex_filter
                else False
            )
            if _regex_match_condition and not _regex_no_match_condition:
                auto_properties = map_logic_monitor_name_value(
                    device_instance.auto_properties
                )
                if not auto_properties:
                    continue
                display_name = device_instance.display_name
                serial_number = auto_properties.get("auto.serialnumber")
                if all([display_name, name, serial_number]):
                    display_name, name, serial_number = [
                        x.strip() for x in [display_name, name, serial_number]
                    ]
                    if device_data.get("serial_number") != serial_number:
                        company_id = device_data.get("company_id")
                        device_name = device_data.get("device_name")
                        os_version = device_data.get("os_version")
                        host_name = device_data.get("host_name")
                        sys_name = device_data.get("sys_name")
                        up_time = device_data.get("up_time")
                        manufacturer = device_data.get("manufacturer")
                        model = device_data.get("model")
                        description = device_data.get("description")
                        _lm_device = dict(
                            company_id=company_id,
                            device_name=device_name,
                            os_version=os_version,
                            host_name=host_name,
                            serial_number=serial_number,
                            up_time=up_time,
                            sys_name=sys_name,
                            manufacturer=manufacturer,
                            model=model,
                            description=description,
                            lm_device_datasource_id=data_source_id,
                            lm_device_datasource_instance_id=device_instance.id,
                            lm_device_id=lm_device_id,
                        )
                        logger.info(
                            "Found serial number: {} under Device {} DataSource {} Instance {} Customer {}".format(
                                serial_number,
                                lm_device_id,
                                data_source_id,
                                device_instance.id,
                                company_id,
                            )
                        )
                        lm_devices.append(_lm_device)
        return lm_devices

    def get_instance_devices(self, device_id, device_data):
        devices = []
        if not device_id:
            return devices
        device_ds_list_ids = self.get_device_datasource_list(device_id)
        device_ds_list_ids = [x.id for x in device_ds_list_ids]
        for device_data_source_id in device_ds_list_ids:
            result = self._get_device_instances_from_data_source(
                device_data, device_data_source_id, device_id
            )
            devices.extend(result)
        return devices

    def _parse_system_categories_from_devices(self, devices):
        system_categories = []
        for _device in devices:
            system_properties = map_logic_monitor_name_value(
                _device.system_properties
            )
            if system_properties.get("system.categories"):
                system_categories.extend(
                    system_properties.get("system.categories").split(",")
                )
        return set(system_categories)

    def _get_key_value_from_properties(
        self, keys, system_properties, auto_properties, inherited_properties
    ):
        keys = keys or []
        for key in keys:
            if "system" in key and system_properties.get(key):
                return system_properties.get(key)
            elif "auto" in key and auto_properties.get(key):
                return auto_properties.get(key)
            elif inherited_properties.get(key):
                return inherited_properties.get(key)
        return None

    @handle_logic_monitor_api_exception()
    def check_connection(self):
        return self.client.get_api_token_list()
