from core.integrations.erp.connectwise.utils import (
    get_data_from_communication_items,
)

DeviceCategory = {"id": "category_id", "name": "category_name"}

Status = {"id": "id", "name": "label"}

Board = {
    "id": "id",
    "name": "label",
    "timeEntryNotAllowed": "time_entry_not_allowed",
}

OpportunityStatus = {"id": "id", "name": "label"}

OpportunityStages = {"id": "id", "name": "label"}

OpportunityTypes = {"id": "id", "description": "label"}

Site = {
    "id": "site_id",
    "name": "name",
    "phoneNumber": "phone_number",
    "addressLine1": "address_line_1",
    "addressLine2": "address_line_2",
    "city": "city",
    "stateReference": {"name": "state", "id": "state_id"},
    "zip": "zip",
    "country": {"name": "country", "id": "country_id"},
}

Manufacturer = {"id": "manufacturer_id", "name": "manufacturer_name"}

Country = {"id": "country_id", "name": "country"}

State = {
    "id": "state_id",
    "name": "state",
    "country": Country,
    "identifier": "state_identifier",
}

Document = {
    "id": "id",
    "title": "title",
    "fileName": "file_name",
    "serverFileName": "server_file_name",
    "owner": "owner",
    "_info": {"lastUpdated": "last_updated_on"},
}

TerritoryManager = {
    "id": "territory_manager_id",
    "name": "territory_manager_name",
}

Company = {
    "id": "id",
    "name": "name",
    "identifier": "identifier",
    "addressLine1": "address_line_1",
    "addressLine2": "address_line_2",
    "city": "city",
    "state": "state",
    "zip": "zip",
    "country": {"name": "country"},
    "phoneNumber": "phone",
    "faxNumber": "fax_number",
    "website": "website",
    "status": {"id": "status_id", "name": "status"},
    "defaultContact": {"id": "primary_contact_id", "name": "primary_contact"},
    "deletedFlag": "is_deleted",
    "customFields": "custom_fields",
    "territoryManager": TerritoryManager,
    "isVendorFlag": "is_vendor",
}

CompanyContactType = {
    "id": "id",
    "description": "description",
}

CompanyContact = {
    "id": "id",
    "firstName": "first_name",
    "lastName": "last_name",
    "department": {"name": "department"},
    "company": {"id": "customer_id"},
    "title": "title",
    "inactiveFlag": "is_inactive_in_crm",
    "communicationItems": get_data_from_communication_items,
    "linkedInUrl": "linkedin_profile_url",
    "twitterUrl": "twitter_profile_url",
    "types": "types",
    "defaultFlag": "is_default",
    "defaultPhoneType": "default_phone_type",
    "defaultPhoneNbr": "default_phone_number",
}

NoteType = {"id": "note_type_id", "name": "note_type_name"}

Note = {
    "id": "note_id",
    "text": "text",
    "type": NoteType,
    "flagged": "flagged",
    "enteredBy": "entered_by",
    "company": Company,
    "_info": {"lastUpdated": "last_updated", "updatedBy": "updated_by"},
}

ProjectNote = {
    "id": "note_id",
    "text": "text",
    "projectId": "project_id",
    "type": NoteType,
    "flagged": "flagged",
    "_info": {"lastUpdated": "last_updated", "updatedBy": "updated_by"},
}

Device = {
    "id": "id",
    "site": Site,
    "locationId": "location_id",
    "serialNumber": "serial_number",
    "tagNumber": "service_contract_number",
    "notes": "notes",
    "name": "device_name",
    "modelNumber": "model_number",
    "type": DeviceCategory,
    "status": {"id": "status_id", "name": "status"},
    "manufacturer": Manufacturer,
    "company": {"id": "customer_id", "name": "customer_name"},
    "warrantyExpirationDate": "expiration_date",
    "purchaseDate": "purchase_date",
    "installationDate": "installation_date",
    "needsRenewalFlag": "needs_renewal",
}

ServiceTicket = {
    "id": "id",
    "summary": "summary",
    "recordType": "record_type",
    "board": {"id": "board_id", "name": "board"},
    "status": {"id": "status_id", "name": "status"},
    "company": {"id": "company_id", "name": "company_name"},
    "priority": {"id": "priority_id", "name": "priority"},
    "severity": "severity",
    "impact": "impact",
    "owner": {
        "id": "owner_id",
        "identifier": "owner_identifier",
        "name": "owner",
    },
    "_info": {"lastUpdated": "last_updated_on", "dateEntered": "created_on"},
    "contactName": "resource",
    "contactPhoneNumber": "resource_phone_number",
    "contactEmailAddress": "contact_email",
    "closedFlag": "is_closed",
    "closedDate": "closed_on",
    "type": {"name": "service_type"},
    "subType": {"name": "service_subtype"},
    "contact": {"id": "contact_id"},
    "opportunity": {"name": "opportunity_name", "id": "opportunity_id"},
    "ticket_note": "ticket_note",
}
ServiceTicketNote = {
    "id": "id",
    "text": "text",
    "contact": {"id": "contact_id", "name": "created_by"},
    "member": {"id": "member_id", "name": "member_name"},
    "dateCreated": "created_on",
    "createdBy": "alternate_created_by",
    "_info": {"lastUpdated": "last_updated_on"},
    "ticketId": "ticketId",
    "internalAnalysisFlag": "is_internal_note",
}

Opportunity = {
    "id": "id",
    "name": "name",
    "shipToContact": {
        "id": "shipping_contact_id",
        "name": "shipping_contact_name",
    },
    "status": {"id": "status_id", "name": "status_name"},
    "company": {"id": "company_id", "name": "company_name"},
    "stage": {"id": "stage_id", "name": "stage_name"},
    "type": {"id": "type_id", "name": "type_name"},
    "rating": {"id": "rating_id", "name": "rating_name"},
    "locationId": "location_id",
    "dateBecameLead": "created_on",
    "expectedCloseDate": "expected_close_date",
    "closedDate": "closed_date",
    "contact": {"name": "primary_contact_name"},
    "customerPO": "customer_po",
    "_info": {"lastUpdated": "last_updated"},
}


ForecastItems = {
    "id": "id",
    "name": "name",
    "revenue": "revenue",
    "cost": "cost",
    "type": "type",
    "status": {"id": "status_id"},
    "includedFlag": "includedFlag",
    "opportunityId": "opportunityId",
    "percent": "percent",
    "margin": "margin",
}

Forecast = {
    "id": "id",
    "name": "name",
    "revenue": "revenue",
    "cost": "cost",
    "type": "type",
    "status": {"id": "status_id"},
    "includedFlag": "includedFlag",
    "opportunityId": "opportunityId",
    "percent": "percent",
    "margin": "margin",
}

Location = {
    "id": "id",
    "structureLevel": {"id": "structure_id", "name": "structure_name"},
    "name": "name",
    "manager": {"id": "manager_id", "name": "manager_name"},
    "reportsTo": {"id": "reports_to_id", "name": "reports_to_name"},
}

SystemMember = {
    "id": "id",
    "firstName": "first_name",
    "lastName": "last_name",
    "defaultEmail": "email",
}

Department = {"id": "id", "name": "label"}

ProjectEngineer = {
    "projectId": "project_id",
    "member": {"id": "member_crm_id"},
    "member_id": "member_id",
}

Project = {
    "id": "crm_id",
    "description": "description",
    "name": "title",
    "board": {"id": "board_crm_id"},
    "company": {"id": "customer_crm_id"},
    "manager": {"id": "project_manager_member_id"},
    "budgetHours": "hours_budget",
    "actualHours": "actual_hours",
    "estimatedEnd": "estimated_end_date",
    "contact": {"id": "primary_contact_crm_id"},
    "status": {"id": "status_crm_id"},
    "actualEnd": "closed_date",
    "billingAmount": "billing_amount",
}

PurchaseOrder = {
    "id": "crm_id",
    "poNumber": "po_number",
    "customerCompany": {
        "id": "customer_company_id",
        "name": "customer_company_name",
    },
    "vendorCompany": {
        "id": "vendor_company_id",
        "name": "vendor_company_name",
    },
    "status": {
        "id": "purchase_order_status_id",
        "name": "purchase_order_status_name",
    },
    "internalNotes": "internal_notes",
    "customerSite": {"id": "site_id", "name": "site_name"},
    "poDate": "po_date",
    "locationId": "location_id",
    "businessUnitId": "business_unit",
    "shipmentDate": "shipment_date",
    "total": "total",
    "enteredBy": "entered_by",
    "vendorInvoiceNumber": "vendor_invoice_number",
    "shippingInstructions": "shipping_instruction",
    "salesTax": "sales_tax",
    "customFields": {"id": "custom_field_id", "value": "sales_order_id"},
}


PurchaseOrderLineItem = {
    "id": "crm_id",
    "description": "description",
    "lineNumber": "line_number",
    "packingSlip": "po_number",
    "product": {"id": "product_crm_id", "identifier": "product_id"},
    "purchaseOrderId": "purchase_order_crm_id",
    "quantity": "ordered_quantity",
    "receivedQuantity": "received_quantity",
    "serialNumbers": "serial_numbers",
    "shipDate": "ship_date",
    "poNumber": "po_number",
    "shipmentMethod": {"id": "shipping_id", "name": "shipping_method"},
    "dateReceived": "received_date",
    "receivedStatus": "received_status",
    "_info": {"lastUpdated": "last_updated_in_cw"},
    "trackingNumber": "tracking_numbers",
    "closedFlag": "is_closed",
    "canceledFlag": "is_cancelled",
    "unitCost": "unit_cost",
    "expectedShipDate": "expected_ship_date",
    "vendorOrderNumber": "vendor_order_number",
    "unitOfMeasure": {"name": "unit_of_measure"},
    "internalNotes": "internal_notes",
    "warehouse": {"name": "warehouse_bin_name"},
}

SalesOrder = {
    "id": "crm_id",
    "poNumber": "customer_po_number",
    "orderDate": "order_date",
    "opportunity": {"id": "opportunity_crm_id", "name": "opportunity_name"},
    "company": {"id": "company_crm_id", "name": "company_name"},
    "site": {"id": "site_crm_id", "name": "site_name"},
    "status": {"name": "status_name", "id": "status_id"},
    "productIds": "product_ids",
    "billClosedFlag": "bill_closed_flag",
    "invoiceIds": "invoice_ids",
    "shipToContact": {
        "id": "ship_to_contact_crm_id",
        "name": "ship_to_contact_name",
    },
    "shipToSite": {"id": "shipping_site_crm_id"},
    "dueDate": "due_date",
    "total": "total",
    "subTotal": "sub_total",
    "taxTotal": "tax_total",
    "location": {"name": "location"},
    "customFields": {"id": "custom_field_id", "value": "so_linked_pos"},
    "_info": {"lastUpdated": "last_crm_update"},
}

ProductItem = {
    "id": "crm_id",
    "catalogItem": {"identifier": "product_id"},
    "description": "description",
    "quantity": "quantity",
    "salesOrder": {"id": "sales_order_crm_id"},
    "invoice": {"id": "invoice_id", "identifier": "invoice_number"},
    "quantityCancelled": "cancelled_quantity",
    "cost": "cost",
    "price": "price",
    "sequenceNumber": "sequence_number",
}

BillingStatus = {"id": "billing_status_crm_id", "name": "billing_status_name"}

BillingInvoice = {
    "id": "invoice_crm_id",
    "invoiceNumber": "invoice_number",
    "status": {"id": "billing_status_crm_id", "name": "billing_status_name"},
    "company": {"id": "customer_crm_id", "name": "customer_name"},
    "customerPO": "customer_po",
    "billingTerms": {"name": "billing_term_name"},
    "dueDate": "due_date",
    "balance": "balance",
}

InvoiceById = {
    "id": "invoice_crm_id",
    "invoiceNumber": "invoice_number",
    "status": {"name": "billing_status_name"},
    "company": {"id": "customer_crm_id", "name": "customer_name"},
    "customerPO": "customer_po",
    "dueDate": "due_date_invoice",
    "balance": "balance",
    "subtotal": "subtotal_in_invoice",
    "total": "total_in_invoice",
    "date": "date",
    "reference": "reference",
    "addToBatchEmailList": "add_to_batch_email_list",
    "salesOrder": {"id": "sales_order_id"},
    "phase": {"name": "project_phase"},
    "ticket": {"id": "ticket_id"},
    "billingTerms": {"name": "billing_term_name"},
    "credits": "credits",
    "billToCompany": {"name": "bill_to_company"},
    "billingSite": {"name": "billing_site"},
    "taxCode": {"name": "tax_code"},
    "serviceTotal": "service_total",
    "expenseTotal": "expense_total",
    "productTotal": "product_total",
    "salesTax": "sales_tax_invoice",
    "internalNotes": "internal_notes_invoice",
    "territoryId": "territory_id",
    "type": "invoice_type",
}

InvoicePaymentDetails = {
    "id": "payment_id",
    "invoice": {"id": "invoice_crm_id", "identifier": "invoice_number"},
    "paymentDate": "payment_date",
}

CompanyTypes = {
    "id": "customer_type_id",
    "name": "customer_type_name",
}

CompanyStatus = {
    "id": "customer_status_crm_id",
    "name": "customer_status_name",
    "inactiveFlag": "is_inactive",
}

TeamRole = {
    "id": "role_crm_id",
    "name": "role_name",
    "accountManagerFlag": "account_manager_flag",
    "techFlag": "tech_flag",
    "salesFlag": "sales_flag",
}

TeamMember = {
    "id": "team_member_crm_id",
    "company": {"id": "customer_crm_id", "name": "customer_name"},
    "teamRole": {"id": "role_crm_id", "name": "role_name"},
    "member": {"id": "system_member_crm_id", "name": "team_member_name"},
}


CatalogItem = {
    "id": "catalog_crm_id",
    "identifier": "product_id",
    "manufacturer": {"id": "manufacturer_crm_id", "name": "manufacturer_name"},
    "description": "description",
    "manufacturerPartNumber": "part_number",
}


OpportunityTeam = {
    "id": "team_crm_id",
    "type": "team_type",
    "member": {"id": "member_crm_id", "name": "member_name"},
    "commissionPercent": "commission_percent",
    "referralFlag": "referral_flag",
    "responsibleFlag": "responsible_flag",
    "opportunityId": "opportunity_crm_id",
}
