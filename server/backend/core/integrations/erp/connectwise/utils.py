import functools
import json
from enum import Enum

from core.utils import group_list_of_dictionary_by_key


def clean_params(params, drop_nones=True, recursive=True):
    """Clean up a dict of API parameters to be sent to the Connectwise API.

    Some endpoints require boolean options to be represented as integers. By
    default, will remove all keys whose value is None, so that they will not be
    sent to the API endpoint at all.
    """
    cleaned = {}
    for key, value in params.items():
        if drop_nones and value is None:
            continue
    if recursive and isinstance(value, dict):
        value = clean_params(value, drop_nones, recursive)
    cleaned[key] = value
    return cleaned


def encode_params(params, **kwargs):
    """Clean and JSON-encode a dict of parameters."""
    cleaned = clean_params(params, **kwargs)
    return json.dumps(cleaned)


def get_all(f):
    """
    Decorator to get all items of the object
    :param f:
    :return:
    """

    def decorated_func(self, *args, **kwargs):
        if kwargs.get("params") and kwargs.get("params").get("all"):
            kwargs.get("params").pop("all")
            kwargs["params"].update({"pagesize": 1000, "page": 1})
            response = f(self, *args, **kwargs)
            result = response.json()
            while response.links.get("next"):
                kwargs["params"]["page"] += 1
                response = f(self, *args, **kwargs)
                result.extend(response.json())
            return result
        else:
            response = f(self, *args, **kwargs)
            return response

    return decorated_func


def combine_filters(filters):
    """
    Combines multiple filter objects of the same class to one object per class.
    :param filters:
    :return:
    """
    remove = []
    for index, filter_obj in enumerate(filters):
        for other in filters[index + 1 :]:
            if type(filter_obj) == type(other):
                filter_obj.extend(other)
                remove.append(other)
    filters = [item for item in filters if item not in remove]
    return filters


def batch_to_single(func):
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        requests = kwargs.get("json", {}).get("requests")
        requests = group_list_of_dictionary_by_key(requests, "method")
        responses = []
        for method, batch_request in requests.items():
            for _request in batch_request:
                _url = _request.get("relativeUrl")
                _json = {"json": _request.get("body")}
                kwargs = {"url": _url, "body": _json, "method": method}
                response = func(self, *args, **kwargs)
                responses.append(response)
        return responses

    return wrapper


class ConnectwiseCommunicationType(Enum):
    PHONE = "Phone"
    EMAIL = "Email"


def get_data_from_communication_items(result, communication_data):
    """
    Parses data of communicationItems key
    """
    phone_numbers = []
    for communication_item in communication_data:
        if (
            communication_item.get("communicationType")
            == ConnectwiseCommunicationType.PHONE.value
        ):
            phone_numbers.append(communication_item)
        elif (
            communication_item.get("communicationType")
            == ConnectwiseCommunicationType.EMAIL.value
        ):
            result.update({"email": communication_item.get("value")})
    result.update({"phone_numbers": phone_numbers})
