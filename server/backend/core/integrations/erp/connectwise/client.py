"""
Core client functionality, common across all API requests
"""

import base64
import os
from collections import defaultdict
from typing import Generator
from urllib.parse import urlencode

import requests
from requests import Response
from requests.adapters import HTTPAdapter
from retrying import retry
from urllib3 import Retry

from core.integrations.utils import prepare_response
from utils import get_app_logger
from exceptions.exceptions import build_error
from .field_mappings import (
    Board,
    Company,
    CompanyContact,
    CompanyContactType,
    Country,
    Device,
    DeviceCategory,
    Document,
    ForecastItems,
    Location,
    Note,
    NoteType,
    ProjectNote,
    Opportunity,
    OpportunityStages,
    OpportunityStatus,
    OpportunityTypes,
    ServiceTicket,
    ServiceTicketNote,
    Site,
    Status,
    State,
    SystemMember,
    Forecast,
    Department,
    PurchaseOrder,
    BillingStatus,
    BillingInvoice,
    CompanyTypes,
    InvoiceById,
    CompanyStatus,
    TeamRole,
    TeamMember,
    CatalogItem,
    OpportunityTeam,
    InvoicePaymentDetails,
)
from .utils import get_all, combine_filters, batch_to_single

# Get an instance of a logger
logger = get_app_logger(__name__)


# TODO: Remove Defualt Values from below variables.
# CW_BASE_API_URI = os.environ.get("CW_BASE_URL", "cw.lookingpoint.com")
# COMPANY_ID = os.environ.get("COMPANY_ID", "Training")
# PUBLIC_KEY = os.environ.get("PUBLIC_KEY", "DbA938sPevmqQl4j")
# PRIVATE_KEY = os.environ.get("PRIVATE_KEY", "NQ2vlb2iQs9SpCtL")
# CLIENT_ID = os.environ.get("CLIENT_ID", "796a6fe5-b1b5-4153-9e75-86999993d28f")
RELEASE_VERSION = os.environ.get("RELEASE_VERSION", "v4_6_release")


def retry_if_connection_error(exception):
    return isinstance(exception, requests.exceptions.ConnectionError)


class Client(object):
    """Performs requests to the Connectwise API web services"""

    COMPANIES_BASE_URL = "company/companies/"
    CONFIGURATIONS_BASE_URL = "company/configurations/"
    COMPANY_CONTACTS_BASE_URL = "company/contacts/"
    COMPANY_NOTE_TYPES_BASE_URL = "company/noteTypes/"
    COMPANY_TYPES_BASE_URL = "company/companies/types"
    BOARDS_BASE_URL = "service/boards/"
    SYSTEM_BASE_URL = "system/"
    ORDERS_BASE_URL = "sales/orders/"
    MANUFACTURER_BASE_URL = "procurement/manufacturers/"
    COUNTRIES_BASE_URL = "company/countries/"
    STATES_BASE_URL = "company/states/"
    SERVICE_TICKETS_BASE_URL = "service/tickets/"
    DOCUMENT_BASE_URL = "/system/documents/"
    DEPARTMENTS_BASE_URL = "/system/departments/"
    PROJECT_BASE_URL = "/project/projects/"
    SALES_OPPORTUNITIES_BASE_URL = "sales/opportunities/"
    SALES_BASE_URL = "sales/"
    LOCATIONS_BASE_URL = "/system/locations/"
    MEMBERS_BASE_URL = "/system/info/members/"
    PURCHASE_ORDERS_BASE_URL = "procurement/purchaseOrders/"
    SHIPMENT_METHODS_BASE_URL = "procurement/shipmentmethods"
    PURCHASE_ORDERS_STATUS_URL = "procurement/purchaseorderstatuses/"
    PURCHASE_ORDERS_PRODUCTS = "procurement/products/"
    WEBHOOKS_BASE_URL = "system/callbacks"
    COMMUNICATION_TYPES = "company/communicationTypes/"
    FINANCE_BASE_URL = "finance/"

    def __init__(
        self,
        auth_token=None,
        company_identifier=None,
        public_key=None,
        private_key=None,
        base_url=None,
        client_id=None,
    ):
        """
        :param auth_token: Required, unless "company identifer", "public_key" and "private_key" are set,
            A base 64 hash of your connectwise Company ID, public key and private key in
            format: companyID+public_key:private_key
        :type auth_token: string

        :param company_identifier: Required, unless "auth_token" is set. Your company ID as entered in the
        "Company" field when logging in to the connectwise Webclient.
        :type company_identifier: string

        :param public_key: Required, unless "auth_token" is set. Your public_key from your Connectwise User
        API
        :type public_key: string

        :param private_key: Required, unless "auth_token" is set. Your private_key from your Connectwise User
        API
        :type private_key: string
        """
        company_identifier = company_identifier
        public_key = public_key
        private_key = private_key

        if not auth_token and not (
            company_identifier and public_key and private_key
        ):
            raise ValueError(
                "Must provide Auth Token or company identifier, public key and private key"
            )

        if auth_token is None:
            self.auth_token = self._generate_auth(
                company_identifier, public_key, private_key
            )
        else:
            self.auth_token = auth_token
        self.base_url = base_url
        self.client_id = client_id
        self.api_url = f"https://{self.base_url}/{RELEASE_VERSION}/apis/3.0/"
        self.session = self._build_session(self.auth_token)

    def _build_session(self, auth_token):
        """
        Build session to make request to CW api's
        :param auth_token: authentication token for CW Developer account
        :type auth_token: string
        :return: session object
        """
        _default_headers = {"Authorization": "Basic %s" % auth_token}
        self.requests_kwargs = {}
        self.requests_kwargs.update(
            {
                "headers": {"Authorization": "Basic %s" % auth_token},
                "verify": False,  # NOTE(cbro): verify SSL certs.
                "timeout": 30,
            }
        )

        session = requests.Session()
        retry = Retry(total=3, backoff_factor=0.1)

        adapter = HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        session.headers = _default_headers
        return session

    def _generate_auth(self, company_id, public_key, private_key):
        token = "{}+{}:{}".format(company_id, public_key, private_key)
        token = base64.b64encode(bytes(token, "utf-8"))
        token = token.decode("utf-8")
        return token

    def _generate_auth_url(self, path, params):
        """Returns the path and a query string portion of the request URL, first adding any necessary
        parameters.

        """

        # Deterministic ordering through sorting by key
        # Useful for tests, and in the furture, any caching.
        if type(params) is dict:
            params = sorted(params.items())
        else:
            params = params[:]  # Take a copy

        return path + "?" + urlencode(params)

    # "Wait 2^x * 1000 milliseconds between each retry, up to 10 seconds, then 10 seconds afterwards"
    @retry(
        retry_on_exception=retry_if_connection_error,
        wait_exponential_multiplier=1000,
        wait_exponential_max=10000,
    )
    def _request(
        self, method, url, filters=None, raise_exception=True, *args, **kwargs
    ):
        """Internal helper for creating HTTP requests to the Connectwise API.

        Raises an APIError if the response is not 20X. Otherwise, returns the
        response object. Not intended for direct use by API consumers.
        """
        kwargs = kwargs or {}
        headers = {
            "Authorization": "Basic %s" % self.auth_token,
            "clientId": self.client_id,
        }
        kwargs.update({"headers": headers})
        filters = filters or []
        filter_data = self.parse_filters(filters)
        if kwargs.get("params"):
            kwargs["params"].update(filter_data)
        else:
            kwargs["params"] = filter_data
        response = getattr(requests, method)(url, **kwargs)
        return self._handle_response(response, raise_exception)

    def parse_filters(self, filters):
        filters = combine_filters(filters)
        filter_data = defaultdict(list)
        for filter_obj in filters:
            _filter_dict = filter_obj.to_dict()
            for k, v in _filter_dict.items():
                filter_data[k].append(v)
        for k, v in filter_data.items():
            filter_data[k] = " and ".join(v)
        return filter_data

    def _handle_response(self, response, raise_exception):
        """Internal helper for handling API responses from the Connectwise server.

        Raises the appropriate exceptions when necessary; otherwise, returns the
        response.
        """
        if not str(response.status_code).startswith("2") and raise_exception:
            raise self._build_api_error(response)
        return response

    def _build_api_error(self, response, blob=None):
        """
        Helper method for creating errors and attaching HTTP response/request
        details to them.
        For response code 500, we wrap the error to code 400.
        """
        blob = blob or response.json()
        logger.debug(f"API Error Blob: {blob}")
        message = blob.get("message", "")
        error_list = blob.get("errors", None)
        if error_list:
            error_list = [
                {error.get("field"): error.get("message")}
                for error in error_list
            ]
        if response.status_code == 500:
            response.status_code = 400
        return build_error(
            response.status_code,
            source="CONNECTWISE",
            message=message,
            errors=error_list,
        )

    @get_all
    def _get(self, url, filters=None, *args, **kwargs):
        if not kwargs.get("params"):
            kwargs["params"] = {"pagesize": 20, "page": 1}
        elif kwargs.get("params").get("pagesize", 0) > 1000:
            raise ValueError("pagesize max. value is 1000")
        return self._request("get", url, filters, *args, **kwargs)

    def _get_all_results_generator(
        self, url, filters=None, *args, **kwargs
    ) -> Generator[Response, None, None]:
        """
        Makes a get request to the specified url and returns a generator.
        The generator can be iterated to get all the pages of the result.
        """
        page = 1
        kwargs["params"].update({"pagesize": 1000, "page": page})
        while page:
            response = self._get(url, filters, *args, **kwargs)
            if response.links.get("next"):
                kwargs["params"]["page"] += 1
            else:
                page = 0
            yield response

    def _post(self, url, filters=None, *args, **kwargs):
        return self._request("post", url, filters, *args, **kwargs)

    def _put(self, url, filters=None, *args, **kwargs):
        return self._request("put", url, filters, *args, **kwargs)

    def _patch(self, url, filters=None, *args, **kwargs):
        return self._request("patch", url, filters, *args, **kwargs)

    def _delete(self, url, filters=None, *args, **kwargs):
        return self._request("delete", url, filters, *args, **kwargs)

    # -------------------------------------------------------------------- #
    # ----------------------------System API----------------------------- #
    # -------------------------------------------------------------------- #
    def get_info(self, filters=None, **params):
        url = f"{self.api_url}{self.SYSTEM_BASE_URL}info"
        response = self._get(url, filters, params=params)
        return response.json()

    @batch_to_single
    def batch_request(self, **params):
        method = params.get("method")
        _url = f"{self.api_url}{params.get('url')}"
        _json = params.get("body")
        response = self._request(
            method.lower(), _url, None, raise_exception=False, **_json
        )
        return response

    def get_document_download_url(self, document_id):
        url = f"{self.api_url}{self.DOCUMENT_BASE_URL}{document_id}/download"
        return url

    # -------------------------------------------------------------------- #
    # ----------------------------Company API----------------------------- #
    # -------------------------------------------------------------------- #

    def get_companies(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Company)

    def get_companies_count(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()

    def get_company(self, company_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Company)

    def update_company(self, company_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}"
        response = self._patch(url, filters, **params)
        return prepare_response(response, Company)

    def get_company_sites(self, company_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/sites"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Site)

    def get_company_site(self, company_id, site_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/sites/{site_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Site)

    def update_customer_site(self, company_id, site_id, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/sites/{site_id}"
        response = self._put(url, filters=None, **params)
        return prepare_response(response, Site)

    def create_company_site(self, company_id, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/sites"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, Site)

    def delete_company_site(self, company_id: int, site_id: int):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/sites/{site_id}"
        response = self._delete(url)
        return response

    def get_company_notes(self, company_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/notes"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Note)

    def get_company_note(self, company_id, note_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/notes/{note_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Note)

    def create_company_note(self, company_id, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/notes"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, Note)

    def update_company_note(self, company_id, note_id, **params):
        url = f"{self.api_url}{self.COMPANIES_BASE_URL}{company_id}/notes/{note_id}"
        response = self._put(url, filters=None, **params)
        return prepare_response(response, Note)

    def get_system_departments(self, filters=None, **params):
        url = f"{self.api_url}{self.DEPARTMENTS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Department)

    def get_opportunity_statuses(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}statuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, OpportunityStatus)

    def get_opportunity_stages(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_BASE_URL}stages"
        response = self._get(url, filters, params=params)
        return prepare_response(response, OpportunityStages)

    def get_opportunity_types(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}types"
        response = self._get(url, filters, params=params)
        return prepare_response(response, OpportunityTypes)

    def create_company_opportunity(self, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, Opportunity)

    def get_company_opportunities(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Opportunity)

    def get_opportunity_teams(
        self, opportunity_crm_id: int, filters=None, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_crm_id}/team"
        response = self._get(url, filters, params=params)
        return prepare_response(response, OpportunityTeam)

    def get_opportunity_team(
        self, opportunity_crm_id: int, team_crm_id: int, filters=None, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_crm_id}/team/{team_crm_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, OpportunityTeam)

    def create_opportunity_team(self, opportunity_crm_id: int, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_crm_id}/team"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, OpportunityTeam)

    def delete_opportunity_team(
        self, opportunity_crm_id: int, team_crm_id: int
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_crm_id}/team/{team_crm_id}"
        response = self._delete(url)
        return response

    def patch_opportunity_team(
        self, opportunity_crm_id: int, team_crm_id: int, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_crm_id}/team/{team_crm_id}"
        response = self._patch(url, filters=None, **params)
        return prepare_response(response, OpportunityTeam)

    def get_company_opportunity(self, opportunity_id, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Opportunity)

    def get_all_opportunities_using_generator(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}"
        opportunities: Generator[
            Response, None, None
        ] = self._get_all_results_generator(url, filters, params=params)
        return opportunities

    def update_company_opportunity(self, opportunity_id, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}"
        response = self._put(url, filters=None, **params)
        return prepare_response(response, Opportunity)

    def patch_opportunity(self, opportunity_id, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}"
        response = self._patch(url, filters=None, **params)
        return prepare_response(response, Opportunity)

    def get_opportunity_forecasts(
        self, opportunity_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Forecast)

    def get_opportunity_forecast_items(
        self, opportunity_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast"
        response = self._get(url, filters, params=params)
        return prepare_response(
            response.json().get("forecastItems", []), ForecastItems
        )

    def get_opportunity_forecast(
        self, opportunity_id, forecast_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast/{forecast_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Forecast)

    def delete_forecast(self, opportunity_id, forecast_id):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast/{forecast_id}"
        response = self._delete(url)
        return response

    def create_opportunity_forecast(self, opportunity_id, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, Forecast)

    def update_opportunity_forecast(
        self, opportunity_id, forecast_id, **params
    ):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}{opportunity_id}/forecast/{forecast_id}"
        response = self._patch(url, filters=None, **params)
        return prepare_response(response, Forecast)

    def get_company_opportunities_count(self, filters=None, **params):
        url = f"{self.api_url}{self.SALES_OPPORTUNITIES_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()["count"]

    def get_company_note_types(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANY_NOTE_TYPES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, NoteType)

    def get_note_type(self, note_type_id, filters=None, **params):
        url = (
            f"{self.api_url}{self.COMPANY_NOTE_TYPES_BASE_URL}/{note_type_id}"
        )
        response = self._get(url, filters, params=params)
        return prepare_response(response, NoteType)

    def get_company_communication_types(self, filters=None, **params):
        url = f"{self.api_url}{self.COMMUNICATION_TYPES}"
        response = self._get(url, filters, params=params)
        return response

    def get_company_types(self, filters=None, **params):
        url: str = f"{self.api_url}{self.COMPANY_TYPES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CompanyTypes)

    def create_company(self, **params):
        url: str = f"{self.api_url}company/companies"
        response = self._post(url, filters=None, **params)
        return prepare_response(response, Company)

    def get_company_statuses(self, filters=None, **params):
        url: str = f"{self.api_url}{self.COMPANIES_BASE_URL}statuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CompanyStatus)

    # Configurations API
    # --------------------------------------------------------------------------- #
    # --------------------------------------------------------------------------- #
    def get_configurations_types(self, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}types"
        response = self._get(url, filters, params=params)
        return prepare_response(response, DeviceCategory)

    def get_configuration_type(self, type_id, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}types/{type_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, DeviceCategory)

    def get_configurations(self, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Device)

    def get_configuration(self, configuration_id, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}{configuration_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Device)

    def delete_configuration(self, configuration_id, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}{configuration_id}"
        response = self._delete(url, filters, params=params)
        return response

    def update_configuration(self, configuration_id, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}{configuration_id}"
        response = self._patch(url, filters=None, **params)
        return prepare_response(response, Device)

    def create_configuration(self, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}"
        response = self._post(url, filters, **params)
        return prepare_response(response, Device)

    def get_configurations_count(self, filters=None, **params):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()

    def get_configuration_statuses(self):
        url = f"{self.api_url}{self.CONFIGURATIONS_BASE_URL}statuses"
        response = self._get(url)
        return response.json()

    # Company Contacts API
    # ---------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------- #
    def get_company_contact_types(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}types"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CompanyContactType)

    def get_company_contacts(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CompanyContact)

    def get_contact(self, contact_id, filters=None, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}{contact_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CompanyContact)

    def get_contacts_count(self, filters=None, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()

    def create_contact(self, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}"
        response = self._post(url, None, **params)
        return prepare_response(response, CompanyContact)

    def update_customer_contact(self, contact_id, **params):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}{contact_id}"
        response = self._patch(url, None, **params)
        return prepare_response(response, CompanyContact)

    def update_customer_contact_communication_item(
        self, contact_id: int, communication_item_id: int, **params
    ):
        url = f"{self.api_url}{self.COMPANY_CONTACTS_BASE_URL}{contact_id}/communications/{communication_item_id}"
        response = self._patch(url, None, **params)
        return prepare_response(response, CompanyContact)

    # -------------------------------------------------------------------- #
    # ----------------------------Boards API----------------------------- #
    # -------------------------------------------------------------------- #
    def get_boards(self, filters=None, **params):
        url = f"{self.api_url}{self.BOARDS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Board)

    def get_board(self, board_id, filters=None, **params):
        url = f"{self.api_url}{self.BOARDS_BASE_URL}{board_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Board)

    def get_boards_count(self, filters=None, **params):
        url = f"{self.api_url}{self.BOARDS_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()

    # Statuses API's
    def get_order_statuses(self, filters=None, **params):
        url = f"{self.api_url}{self.ORDERS_BASE_URL}statuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Status)

    def get_project_statuses(self, filters=None, **params):
        url = f"{self.api_url}project/statuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Status)

    # -------------------------------------------------------------------- #
    # ----------------------------Manufacturer API----------------------------- #
    # -------------------------------------------------------------------- #
    def get_manufacturers(self, filters=None, **params):
        url = f"{self.api_url}{self.MANUFACTURER_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Status)

    # -------------------------------------------------------------------- #
    # ----------------------------Countries and states API----------------------------- #
    # -------------------------------------------------------------------- #
    def get_countries(self, filters=None, **params):
        """ "
        Returns list of countries
        """
        url = f"{self.api_url}{self.COUNTRIES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Country)

    def get_states(self, filters=None, **params):
        """
        Returns list of states
        :param filters:
        :param params:
        :return:
        """
        url = f"{self.api_url}{self.STATES_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, State)

    def get_board_statuses(self, board_id, filters=None, **params):
        """
        Returns list of statuses for board
        """
        url = f"{self.api_url}{self.BOARDS_BASE_URL}{board_id}/statuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Board)

    def get_board_statuses_by_id(
        self, board_id, status_id, filters=None, **params
    ):
        """
        Returns list of statuses for board
        """
        url = f"{self.api_url}{self.BOARDS_BASE_URL}{board_id}/statuses/{status_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Board)

    # -------------------------------------------------------------------- #
    # ----------------------------Service Tickets API----------------------------- #
    # -------------------------------------------------------------------- #

    def get_service_tickets(self, filters=None, **params):
        """
        Returns List of service tickets
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, ServiceTicket)

    def get_service_tickets_using_generator(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_service_tickets_count(self, filters=None, **params):
        """
        Returns count of service tickets
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()["count"]

    def get_service_ticket(self, ticket_id, filters=None, **params):
        """
        Returns List of service tickets
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}{ticket_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, ServiceTicket)

    def create_service_ticket(self, **params):
        """
        Create service ticket
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}"
        response = self._post(url, **params)
        return prepare_response(response, ServiceTicket)

    def update_service_ticket(self, ticket_id, **params):
        """
        Update service ticket
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}{ticket_id}"
        response = self._patch(url, **params)
        return prepare_response(response, ServiceTicket)

    def get_service_ticket_notes(self, ticket_id, filters=None, **params):
        """
        Returns list of service ticket notes
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}{ticket_id}/notes"
        response = self._get(url, filters, params=params)
        return prepare_response(response, ServiceTicketNote)

    def create_service_ticket_note(self, ticket_id, **params):
        """
        Creates service ticket note.
        """
        url = f"{self.api_url}{self.SERVICE_TICKETS_BASE_URL}{ticket_id}/notes"
        response = self._post(url, **params)
        return prepare_response(response, ServiceTicketNote)

    def get_documents(self, filters=None, **params):
        url = f"{self.api_url}{self.DOCUMENT_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Document)

    def delete_document(self, document_id, filters=None, **params):
        url = f"{self.api_url}{self.DOCUMENT_BASE_URL}{document_id}"
        response = self._delete(url, filters, params=params)
        return response

    def create_service_ticket_document(self, **params):
        url = f"{self.api_url}{self.DOCUMENT_BASE_URL}"
        response = self._post(url, **params)
        return prepare_response(response, Document)

    def create_system_document(self, **params):
        url = f"{self.api_url}{self.DOCUMENT_BASE_URL}"
        response = self._post(url, **params)
        return prepare_response(response, Document)

    # Projects API

    def get_projects_count(self, filters, **params):
        """
        Returns count of projects
        """
        url = f"{self.api_url}{self.PROJECT_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()["count"]

    # Orders API

    def get_orders_count(self, filters, **params):
        """
        Returns count of orders
        """
        url = f"{self.api_url}{self.ORDERS_BASE_URL}count"
        response = self._get(url, filters, params=params)
        return response.json()["count"]

    # Sales Orders APIs
    def get_sales_orders(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.ORDERS_BASE_URL}"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_sales_order_by_id(
        self, sales_order_crm_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.ORDERS_BASE_URL}/{sales_order_crm_id}"
        response = self._get(url, filters, params=params)
        return response.json()

    # update custom field value for a sales sales order
    def update_sales_order_custom_field_values(
        self, sales_order_crm_id, **params
    ):
        url = f"{self.api_url}{self.ORDERS_BASE_URL}{sales_order_crm_id}"
        response = self._patch(url, **params)
        return response.json()

    # System API

    def get_system_members(self, filters=None, **params):
        """
        Returns System Members
        """
        url = f"{self.api_url}{self.MEMBERS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, SystemMember)

    def get_system_member(self, member_id, filters=None, **params):
        """
        Returns System Members
        """
        url = f"{self.api_url}{self.MEMBERS_BASE_URL}{member_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, SystemMember)

    def get_locations(self, filters=None, **params):
        """
        Returns System Locations
        """
        url = f"{self.api_url}{self.LOCATIONS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Location)

    def get_location(self, location_id, filters=None, **params):
        """
        Returns System Location by ID
        """
        url = f"{self.api_url}{self.LOCATIONS_BASE_URL}{location_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, Location)

    def update_location(self, location_id, **params):
        """
        Update System Location
        """
        url = f"{self.api_url}{self.LOCATIONS_BASE_URL}{location_id}"
        response = self._put(url, filters=None, **params)
        return prepare_response(response, Location)

    # -------------------------------------------------------------------- #
    # ----------------------------Purchase Orders API----------------------------- #
    # -------------------------------------------------------------------- #

    def get_purchase_orders_list(self, filters=None, **params):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return response

    def get_purchase_orders_details(
        self, purchase_order_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}/{purchase_order_id}"
        response = self._get(url, filters, params=params)
        return response

    def get_purchase_orders_list_using_generator(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_purchase_order_line_items(
        self, purchase_order_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}{purchase_order_id}/lineitems"
        response = self._get(url, filters, params=params)
        return response

    def get_purchase_order_line_item(
        self, purchase_order_id, line_item_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}{purchase_order_id}/lineitems/{line_item_id}"
        response = self._get(url, filters, params=params)
        return response.json()

    def update_purchase_order_line_item(
        self, purchase_order_id, line_item_id, **params
    ):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}{purchase_order_id}/lineitems/{line_item_id}"
        response = self._patch(url, **params)
        return response

    def list_shipment_methods(self, filters=None, **params):
        url = f"{self.api_url}{self.SHIPMENT_METHODS_BASE_URL}"
        response = self._get(url, filters, params=params)
        return response

    def list_purchase_order_statuses(self, filters=None, **params):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_STATUS_URL}"
        response = self._get(url, filters, params=params)
        return response

    def list_products(self, filters=None, **params):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_PRODUCTS}"
        response = self._get(url, filters, params=params)
        return response

    # Get product item details
    def get_product_item_details(self, product_crm_id, filters=None, **params):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_PRODUCTS}{product_crm_id}"
        response = self._get(url, filters, params=params)
        return response.json()

    def get_product_item_details_using_generator(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.PURCHASE_ORDERS_PRODUCTS}"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def update_purchase_order_custom_field_values(
        self, purchase_order_crm_id, **params
    ):
        url = f"{self.api_url}{self.PURCHASE_ORDERS_BASE_URL}{purchase_order_crm_id}"
        response = self._patch(url, **params)
        return response.json()

    # -------------------------------------------------------------------- #
    # ----------------------------Webhooks API---------------------------- #
    # -------------------------------------------------------------------- #

    def create_webhook_callback(self, **params):
        url = f"{self.api_url}{self.WEBHOOKS_BASE_URL}"
        response = self._post(url, **params)
        return prepare_response(response, Document)

    # ********************** Project Methods **********************
    def get_project_security_roles_list(self, filters=None, **params):
        url = f"{self.api_url}/project/securityRoles"
        response = self._get(url, filters, params=params)
        return response

    def list_projects(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.PROJECT_BASE_URL}"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_project(self, project_id):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}"
        response = self._get(url)
        return response.json()

    def get_project_team_members(self, project_id):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/teamMembers"
        response = self._get(url)
        return response.json()

    def get_project_customer_contacts(self, project_id):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/contacts"
        response = self._get(url)
        return response.json()

    def create_project_customer_contact(self, project_id, **params):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/contacts"
        response = self._post(url, **params)
        return response.json()

    def delete_project_customer_contact(self, project_id, contact_record_id):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/contacts/{contact_record_id}"
        response = self._delete(url)
        return response

    def get_project_tickets(self, filters=None, **params):
        url = f"{self.api_url}/project/tickets"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_project_notes(self, project_id, filters=None, **params):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/notes"
        response = self._get(url, filters, params=params)
        return prepare_response(response, ProjectNote)

    def create_project_notes(self, project_id, filters=None, **params):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/notes"
        response = self._post(url, **params)
        return prepare_response(response, ProjectNote)

    def update_project_notes(
        self, project_id, note_id, filters=None, **params
    ):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/notes/{note_id}"
        response = self._patch(url, **params)
        return prepare_response(response, ProjectNote)

    def delete_project_note(self, project_id, note_id, **params):
        url = f"{self.api_url}{self.PROJECT_BASE_URL}{project_id}/notes/{note_id}"
        response = self._delete(url, **params)
        return response

    def update_project_ticket(self, ticket_id, **params):
        """
        Update project ticket
        """
        url = f"{self.api_url}/project/tickets/{ticket_id}"
        response = self._patch(url, **params)
        return response.json()

    def update_project(self, project_id, **params):
        url = f"{self.api_url}/{self.PROJECT_BASE_URL}{project_id}"
        response = self._patch(url, **params)
        return response.json()

    def get_time_work_roles(self, filters=None, **params):
        """
        Get Work roles for time entries
        """
        url = f"{self.api_url}time/workRoles"
        response = self._get(url)
        return response.json()

    def get_time_work_types(self, filters=None, **params):
        """
        Get Work types for time entries
        """
        url = f"{self.api_url}time/workTypes"
        response = self._get(url)
        return response.json()

    def create_project_ticket_time_entry(self, **params):
        url = f"{self.api_url}time/entries"
        response = self._post(url, **params)
        return response.json()

    def update_project_ticket_time_entry(self, time_entry_id, **params):
        url = f"{self.api_url}time/entries/{time_entry_id}"
        response = self._put(url, **params)
        return response.json()

    def delete_project_ticket_time_entry(self, time_entry_id, **params):
        url = f"{self.api_url}time/entries/{time_entry_id}"
        response = self._delete(url, **params)
        return response

    def get_timesheet_periods(self, filters=None, **params):
        url = f"{self.api_url}time/sheets"
        response = self._get(url, filters, params=params)
        return response

    def submit_time_sheet(self, time_sheet_id: int):
        url = f"{self.api_url}time/sheets/{time_sheet_id}/submit"
        response = self._post(url)
        return response

    def get_time_entries(self, filters=None, **params):
        url = f"{self.api_url}time/entries"
        response = self._get(url, filters, params=params)
        return response.json()

    def get_charge_codes(self, filters=None, **params):
        url = f"{self.api_url}time/chargeCodes"
        response = self._get(url, filters, params=params)
        return response

    def get_custom_fields(self, filters=None, **params):
        url = f"{self.api_url}{self.SYSTEM_BASE_URL}userDefinedFields"
        response = self._get(url, filters, params=params)
        return response.json()

    def get_billing_statuses(self, filters=None, **params):
        url = f"{self.api_url}{self.FINANCE_BASE_URL}billingStatuses"
        response = self._get(url, filters, params=params)
        return prepare_response(response, BillingStatus)

    def get_billing_invoices(self, filters=None, **params):
        url = f"{self.api_url}{self.FINANCE_BASE_URL}invoices"
        response = self._get(url, filters, params=params)
        return prepare_response(response, BillingInvoice)

    def get_billing_invoices_using_generator(
        self, filters=None, **params
    ) -> Generator[Response, None, None]:
        url = f"{self.api_url}{self.FINANCE_BASE_URL}invoices"
        response_generator = self._get_all_results_generator(
            url, filters, params=params
        )
        return response_generator

    def get_invoice_by_id(self, invoice_id, filters=None, **params):
        url = f"{self.api_url}{self.FINANCE_BASE_URL}invoices/{invoice_id}"
        response = self._get(url, filters, params=params)
        return prepare_response(response, InvoiceById)

    def get_invoice_payment_details(self, invoice_id, filters=None, **params):
        url = f"{self.api_url}{self.FINANCE_BASE_URL}invoices/{invoice_id}/payments"
        response = self._get(url, filters, params=params)
        return prepare_response(response, InvoicePaymentDetails)

    def get_company_team_roles(self, filters=None, **params):
        url: str = f"{self.api_url}company/teamRoles"
        response = self._get(url, filters, params=params)
        return prepare_response(response, TeamRole)

    def create_company_team_role(self, **params):
        url: str = f"{self.api_url}company/teamRoles"
        response = self._post(url, **params)
        return prepare_response(response, TeamRole)

    def add_member_to_company_team(self, customer_crm_id: int, **params):
        url: str = f"{self.api_url}company/companies/{customer_crm_id}/teams"
        response = self._post(url, **params)
        return prepare_response(response, TeamMember)

    def delete_company_team_member(
        self, customer_crm_id: int, team_member_crm_id: int
    ):
        url: str = f"{self.api_url}company/companies/{customer_crm_id}/teams/{team_member_crm_id}"
        response = self._delete(url)
        return response

    def get_company_team_members(
        self, customer_crm_id: int, filters=None, **params
    ):
        url: str = f"{self.api_url}company/companies/{customer_crm_id}/teams"
        response = self._get(url, filters, params=params)
        return prepare_response(response, TeamMember)

    def get_product_catalog_details(self, filters=None, **params):
        url: str = f"{self.api_url}procurement/catalog"
        response = self._get(url, filters, params=params)
        return prepare_response(response, CatalogItem)
