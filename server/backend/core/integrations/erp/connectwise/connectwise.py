"""
Connectwise ERP Integration Implementation
"""
import copy
from collections import namedtuple
from datetime import date, datetime, timedelta
from enum import Enum
from typing import Any, Dict, Generator, List, Optional, Set, Union

from box import Box
from cached_property import cached_property_with_ttl
from django.conf import settings
from requests import Response, exceptions
from django.utils import timezone

from core.integrations.erp.base import ERPBase
from core.integrations.erp.connectwise.client import Client
from core.integrations.erp.connectwise.utils import (
    ConnectwiseCommunicationType,
)
from core.utils import (
    batch_call,
    chunks_of_list_with_max_item_count,
    chunks_of_list_with_max_items_length,
    generate_random_id,
    group_list_of_dictionary_by_key,
)
from exceptions.exceptions import (
    AuthenticationError,
    InvalidRequestError,
    RateLimitExceededError,
)
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)

Condition = namedtuple("Condition", "field operator value")
Operators = Box(
    EQUALS="=",
    GREATER_THEN=">",
    LESS_THEN="<",
    GREATER_THEN_EQUALS=">=",
    LESS_THEN_EQUALS="<=",
    CONTAINS=" contains ",
    LIKE=" like ",
    IN=" in ",
    NOT=" != ",
)

Order = Box(ASC="asc", DESC="desc")
RequestMethods = Box(GET="GET", POST="POST", PATCH="PATCH", DELETE="DELETE")
partial_received_status = [
    {"id": 1, "status": "Waiting"},
    {"id": 2, "status": "FullyReceived"},
    {"id": 3, "status": "PartiallyReceiveCancelRest"},
    {"id": 4, "status": "PartiallyReceiveCloneRest"},
]
FULLY_RECEIVED = "FullyReceived"
WAITING = "Waiting"
PARTIALLY_RECEIVED_CANCEL_REST = "PartiallyReceiveCancelRest"
PARTIALLY_RECEIVED_CLONE_REST = "PartiallyReceiveCloneRest"

line_items_status = [
    {"id": 1, "status": "All"},
    {"id": 2, "status": "Open", "is_closed": False, "is_cancelled": False},
    {"id": 3, "status": "Received", "is_closed": True, "is_cancelled": False},
    {"id": 4, "status": "Cancelled", "is_cancelled": True},
]


def prepare_device_post_data(customer_id, category_id, data):
    """
    TODO: Move to Connectwise Class
    :param customer_id:
    :param category_id:
    :param data:
    :return:
    """
    category_id = category_id or data.get("category_id")
    device_data = dict()
    device_data.update({"name": data.get("device_name")})
    device_data.update({"company": {"id": customer_id}})
    device_data.update({"type": {"id": category_id}})
    device_data.update({"site": {"id": data.get("site_id")}})
    device_data.update({"manufacturer": {"id": data.get("manufacturer_id")}})
    device_data.update({"serialNumber": data.get("serial_number", "")})
    device_data.update({"modelNumber": data.get("product_id")})
    device_data.update({"notes": data.get("notes", "")})
    device_data.update({"tagNumber": data.get("service_contract_number")})
    device_data.update({"purchaseDate": data.get("purchase_date")})
    device_data.update({"warrantyExpirationDate": data.get("expiration_date")})
    device_data.update({"installationDate": data.get("installation_date")})
    device_data.update({"status": {"id": data.get("status_id")}})
    return device_data


def prepare_device_patch_data(data):
    """
    TODO: Move to Connectwise Class
    :param data:
    :return:
    """

    patch_group = PatchGroup()
    if data.get("device_name"):
        patch_group.add("name", data.get("device_name"))
    if data.get("site_id"):
        patch_group.add("site", {"id": data.get("site_id")})
    if data.get("status_id"):
        patch_group.add("status", {"id": data.get("status_id")})
    if data.get("notes") or data.get("notes") == "":
        patch_group.add("notes", data.get("notes"))
    if data.get("expiration_date"):
        patch_group.add("warrantyExpirationDate", data.get("expiration_date"))
    if data.get("purchase_date"):
        patch_group.add("purchaseDate", data.get("purchase_date"))
    if data.get("installation_date"):
        patch_group.add("installationDate", data.get("installation_date"))
    if data.get("product_id"):
        patch_group.add("modelNumber", data.get("product_id"))
    if (
        data.get("service_contract_number")
        or data.get("service_contract_number") == ""
    ):
        patch_group.add("tagNumber", data.get("service_contract_number"))
    if data.get("manufacturer_id"):
        patch_group.add("manufacturer", {"id": data.get("manufacturer_id")})
    if data.get("category_id"):
        patch_group.add("type", {"id": data.get("category_id")})
    return patch_group.to_dict()


class Connectwise(ERPBase):
    """
    Connectwise API's Wrapper Implementation
    TODO: Handle API Error's
    """

    client = None

    def __init__(self, auth_configs, field_mappings=None, extra_configs=None):
        super(Connectwise, self).__init__(
            auth_configs, field_mappings, extra_configs
        )
        self.authenticate()

    def __str__(self):
        return "CONNECTWISE"

    def __hash__(self):
        return hash(self.__str__())

    def authenticate(self):
        company_identifier = self.auth_configs.get("company_identifier")
        public_key = self.auth_configs.get("public_key")
        private_key = self.auth_configs.get("private_key")
        base_url = self.auth_configs.get("base_url")
        client_id = self.auth_configs.get("client_id")
        self.client: Client = Client(
            company_identifier=company_identifier,
            public_key=public_key,
            private_key=private_key,
            base_url=base_url,
            client_id=client_id,
        )

    def validate_api_credentials(self):
        try:
            self.client.get_info()
            return True
        except (
            AuthenticationError,
            InvalidRequestError,
            RateLimitExceededError,
        ):
            return False
        except (
            exceptions.ConnectionError,
            TimeoutError,
            exceptions.Timeout,
            exceptions.ConnectTimeout,
            exceptions.ReadTimeout,
        ) as e:
            return False

    @cached_property_with_ttl(ttl=60 * 60)  # Cache expires after 3600 seconds.
    def device_active_status_id(self):
        return self.get_device_active_status_id()

    @cached_property_with_ttl(ttl=60 * 60)
    def device_inactive_status_id(self):
        return self.get_device_inactive_status_id()

    def get_project_boards(self):
        project_board_filter = CWConditions(
            boolean_conditions=[
                Condition("projectFlag", Operators.EQUALS, True),
                Condition("inactiveFlag", Operators.EQUALS, False),
            ]
        )
        return self.client.get_boards(filters=[project_board_filter])

    def get_service_boards(self):
        service_board_filter = CWConditions(
            boolean_conditions=[
                Condition("projectFlag", Operators.EQUALS, False),
                Condition("inactiveFlag", Operators.EQUALS, False),
            ]
        )
        return self.client.get_boards(filters=[service_board_filter])

    def get_other_configs(self, extra_config):
        """
        Method to get board mapping data
        We get the local json config template and place appropriate
        option values in placeholders.
        Args:
            extra_config: Config dictionary

        Returns:
            Board mapping dictionary
        """
        board_mapping = extra_config["board_mapping"]
        boards = self.client.get_boards()
        board_mapping["Service Board Mapping"]["Professional Services"][
            "board"
        ]["options"] = boards
        board_mapping["Service Board Mapping"]["Managed Services"]["board"][
            "options"
        ] = boards
        board_mapping["Service Board Mapping"]["Smartnet Services"]["board"][
            "options"
        ] = boards
        board_mapping["Service Board Mapping"]["Projects"]["board"][
            "options"
        ] = self.get_project_boards()
        order_statuses = self.client.get_order_statuses()
        project_statuses = self.client.get_project_statuses()

        # Fill statuses data
        board_mapping["Orders"]["open_status"]["options"] = order_statuses
        board_mapping["Orders"]["closed_status"]["options"] = order_statuses
        board_mapping["Service Board Mapping"]["Projects"]["open_status"][
            "options"
        ] = project_statuses
        board_mapping["Service Board Mapping"]["Projects"]["closed_status"][
            "options"
        ] = project_statuses

        # Fill opportunities statuses data
        opportunities_statuses = self.client.get_opportunity_statuses(all=True)
        board_mapping["Opportunities"]["status"][
            "options"
        ] = opportunities_statuses
        board_mapping["SOW Opportunity Mapping"]["open_status"][
            "options"
        ] = opportunities_statuses

        system_departments = self.client.get_system_departments()
        board_mapping["SOW Opportunity Mapping"]["business_unit"][
            "options"
        ] = system_departments

        # Fill opportunities stages data
        opportunities_stages = self.client.get_opportunity_stages(all=True)
        board_mapping["Opportunities"]["stages"][
            "options"
        ] = opportunities_stages

        # Fill change request mapping for opportunity status and stages
        board_mapping["Change Request Mapping"]["won_status"][
            "options"
        ] = opportunities_statuses
        board_mapping["Change Request Mapping"]["won_stage"][
            "options"
        ] = opportunities_stages

        # Fill SOW document quotes status mapping
        board_mapping["SOW Quote Status Mapping"]["won_status"][
            "options"
        ] = opportunities_statuses
        board_mapping["SOW Quote Status Mapping"]["open_status"][
            "options"
        ] = opportunities_statuses
        board_mapping["SOW Quote Status Mapping"]["lost_status"][
            "options"
        ] = opportunities_statuses

        extra_config["board_mapping"] = board_mapping
        extra_config["device_categories"] = self.get_device_categories()
        return extra_config

    def get_device_categories(self, filters=None):
        filters = filters or []
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        categories = self.client.get_configurations_types(
            filters=filters, all=True
        )
        return categories

    def get_devices(
        self,
        customer_id=None,
        category_id=None,
        filters=None,
        required_fields=None,
    ):
        filters = filters or []
        required_fields = required_fields or [
            "id",
            "site/id",
            "site/name",
            "locationID",
            "serialNumber",
            "tagNumber",
            "notes",
            "name",
            "modelNumber",
            "type/name",
            "type/id",
            "status/id",
            "status/name",
            "manufacturer/id",
            "manufacturer/name",
            "company/id",
            "company/name",
            "warrantyExpirationDate",
            "purchaseDate",
            "installationDate",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        if isinstance(category_id, int):
            category_id = [category_id]
        if customer_id:
            if isinstance(customer_id, list):
                customer_id_filter = CWConditions(
                    int_conditions=[
                        Condition(
                            "company/id", Operators.IN, tuple(customer_id)
                        )
                    ]
                )
                filters.append(customer_id_filter)
            if isinstance(customer_id, int):
                customer_id_filter = CWConditions(
                    int_conditions=[
                        Condition("company/id", Operators.EQUALS, customer_id)
                    ]
                )
                filters.append(customer_id_filter)
        if category_id:
            category_id_filter = CWConditions(
                int_conditions=[
                    Condition("type/id", Operators.IN, tuple(category_id))
                ]
            )
            filters.append(category_id_filter)
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.append(order_by_filter)
        devices = self.client.get_configurations(filters, all=True)
        return devices

    def get_device_by_serial_numbers(
        self, serial_numbers, filters=None, *args, **kwargs
    ):
        result = []
        filters = filters or []
        serial_numbers = list(
            chunks_of_list_with_max_items_length(serial_numbers, 500)
        )
        for _sr_nos in serial_numbers:
            serial_number_filter = CWConditions(
                int_conditions=[
                    Condition("serialNumber", Operators.IN, tuple(_sr_nos))
                ]
            )
            new_filters = copy.deepcopy(filters)
            new_filters.append(serial_number_filter)
            result.extend(
                self.get_devices(filters=new_filters, *args, **kwargs)
            )
        return result

    def get_active_devices(self, customer_id, category_id, fields=None):
        """
        Get only active devices from Connectwise.
        """
        active_devices_filter = CWConditions(
            int_conditions=[
                Condition(
                    "status/id", Operators.EQUALS, self.device_active_status_id
                )
            ]
        )
        return self.get_devices(
            customer_id,
            category_id,
            [active_devices_filter],
            required_fields=fields,
        )

    def get_device_serial_numbers(
        self,
        customer_id=None,
        category_id=None,
        show_active_only=False,
        filters=None,
    ):
        """
        Returns list of already available serial numbers for devices.
        :param customer_id: Customer Id.
        :return: list of serial numbers
        """
        required_fields = ["serialNumber"]
        filters = filters or []
        if customer_id:
            filters = [
                CWConditions(
                    int_conditions=[
                        Condition("company/id", Operators.EQUALS, customer_id)
                    ]
                )
            ]
        if show_active_only:
            filters.append(
                CWConditions(
                    int_conditions=[
                        Condition(
                            "status/id",
                            Operators.EQUALS,
                            self.device_active_status_id,
                        )
                    ]
                )
            )

        serial_numbers = self.get_devices(
            customer_id,
            category_id,
            required_fields=required_fields,
            filters=filters,
        )
        result = [
            item["serial_number"]
            for item in serial_numbers
            if item.get("serial_number")
        ]
        return result

    def get_device(self, device_id):
        """
        Returns single device data
        :param device_id: CW device ID
        :return: device data
        """

        required_fields = [
            "id",
            "site/id",
            "site/name",
            "locationID",
            "serialNumber",
            "tagNumber",
            "notes",
            "name",
            "modelNumber",
            "type/name",
            "type/id",
            "status/id",
            "status/name",
            "manufacturer/id",
            "manufacturer/name",
            "company/id",
            "company/name",
            "warrantyExpirationDate",
            "purchaseDate",
            "installationDate",
        ]
        fields_filter = [CWFieldsFilter(required_fields)]
        device = self.client.get_configuration(device_id, fields_filter)
        return device

    def get_devices_count(self, customer_id, category_id=None, filters=None):
        """
        Returns count of total number of devices for the company
        :param category_id:
        :param customer_id:
        :param filters:
        :return:
        """
        filters = filters or []
        if isinstance(category_id, int):
            category_id = [category_id]
        customer_id_filter = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.EQUALS, customer_id),
                Condition("type/id", Operators.IN, tuple(category_id)),
            ]
        )
        filters.append(customer_id_filter)
        data = self.client.get_configurations_count(filters)
        return data["count"]

    def get_device_by_ids(
        self, device_crm_ids: List, required_fields: bool = None
    ):
        if isinstance(device_crm_ids, int):
            device_crm_ids = [device_crm_ids]
        devices = []
        for crm_ids in chunks_of_list_with_max_item_count(device_crm_ids, 100):
            filters = [
                CWConditions(
                    int_conditions=[
                        Condition("id", Operators.IN, tuple(crm_ids))
                    ]
                )
            ]
            _devices = self.get_devices(
                filters=filters, required_fields=required_fields
            )
            devices.extend(_devices)
        return devices

    def add_device(self, customer_id, category_id, data):
        """
        should Create a new device
        :param category_id: CW type id
        :param customer_id: CW company id
        :param data: Device data
        :return: created device data
        """
        device_data = prepare_device_post_data(customer_id, category_id, data)
        device = self.client.create_configuration(
            filters=None, json=device_data
        )
        return device

    def batch_add_devices(self, customer_id, category_id, data, **kwargs):
        """
        Add devices in batch
        :param category_id:
        :param customer_id:
        :param data:
        :return:
        """
        # mapping in keyword argument stores device name to row number mapping.
        # which is used to import devices using sheet.
        if kwargs.get("mapping", ""):
            device_name_to_row_mapping: Dict[str, int] = kwargs.get("mapping")
        else:
            device_name_to_row_mapping: Dict = dict()
        create_failed_requests_count: int = 0
        update_failed_requests_count: int = 0
        serial_numbers_crm_id_mapping: Dict = dict()
        required_fields: List[str] = ["id", "serialNumber"]
        device_crm_ids: List[Dict] = self.get_devices(
            customer_id, category_id, required_fields=required_fields
        )
        device_crm_ids_by_serial_number: Dict[
            str, List[Dict]
        ] = group_list_of_dictionary_by_key(device_crm_ids, "serial_number")

        for item in data:
            if (
                item
                and item.get("purchase_date")
                and isinstance(item["purchase_date"], date)
            ):
                item["purchase_date"] = datetime.strftime(
                    item["purchase_date"], settings.ISO_FORMAT
                )
            if (
                item
                and item.get("expiration_date")
                and isinstance(item["expiration_date"], date)
            ):
                item["expiration_date"] = datetime.strftime(
                    item["expiration_date"], settings.ISO_FORMAT
                )
            if (
                item
                and item.get("installation_date")
                and isinstance(item["installation_date"], date)
            ):
                item["installation_date"] = datetime.strftime(
                    item["installation_date"], settings.ISO_FORMAT
                )
        create_devices: List[Dict] = list(
            filter(
                lambda x: str(x.get("serial_number"))
                not in device_crm_ids_by_serial_number,
                data,
            )
        )
        update_devices: List[Dict] = list(
            filter(
                lambda x: str(x.get("serial_number"))
                in device_crm_ids_by_serial_number,
                data,
            )
        )

        batch_update_devices: List = list()
        cw_error_msgs: List[Dict] = list()
        if create_devices:
            batch_requests_items = self.create_batch_create_device_data(
                customer_id, category_id, create_devices
            )
            response = self.batch_create_devices(batch_requests_items)
            for i, res in enumerate(response):
                if res.status_code == 201 and res.json():
                    message = res.json()
                    serial_numbers_crm_id_mapping[
                        message.get("serialNumber")
                    ] = message.get("id")
                else:
                    create_failed_requests_count += 1
                    if device_name_to_row_mapping:
                        cw_response: Dict = res.json()
                        cw_response_msg: Dict = dict()
                        device_name: str = create_devices[i].get(
                            "device_name", ""
                        )
                        cw_response_msg["device_name"] = device_name
                        cw_response_msg["serial_number"] = create_devices[
                            i
                        ].get("serial_number", "")
                        cw_response_msg["message"] = cw_response.get(
                            "message", ""
                        )
                        cw_response_msg["errors"] = cw_response.get(
                            "errors", []
                        )
                        if device_name_to_row_mapping:
                            cw_response_msg[
                                "row_number"
                            ] = device_name_to_row_mapping.get(
                                device_name, None
                            )
                        cw_error_msgs.append(cw_response_msg)

        if update_devices:
            device_names: List[str] = list()
            for item in update_devices:
                # Device name should not be updated when the device is being updated on CW side for bulk import.
                device_names.append(item.pop("device_name", ""))
                device_crm_ids_list = device_crm_ids_by_serial_number.get(
                    str(item.get("serial_number"))
                )
                for entry in device_crm_ids_list:
                    item_copy = copy.deepcopy(item)
                    item_copy["device_id"] = entry.get("id")
                    batch_update_devices.append(item_copy)
            logger.debug(
                "Batch call for updating devices. Device count: {0}".format(
                    len(batch_update_devices)
                )
            )
            response = self.batch_update_devices(batch_update_devices)
            for i, res in enumerate(response):
                if res.status_code == 200 and res.json():
                    message = res.json()
                    serial_numbers_crm_id_mapping[
                        message.get("serialNumber")
                    ] = message.get("id")
                else:
                    update_failed_requests_count += 1
                    if device_name_to_row_mapping:
                        cw_response: Dict = res.json()
                        cw_response_msg: Dict = dict()
                        _device_name: str = device_names[i]
                        cw_response_msg["device_name"] = _device_name
                        cw_response_msg["serial_number"] = update_devices[
                            i
                        ].get("serial_number", "")
                        cw_response_msg["message"] = cw_response.get(
                            "message", ""
                        )
                        cw_response_msg["errors"] = cw_response.get(
                            "errors", []
                        )
                        if device_name_to_row_mapping:
                            cw_response_msg[
                                "row_number"
                            ] = device_name_to_row_mapping.get(
                                _device_name, None
                            )
                        cw_error_msgs.append(cw_response_msg)
        result: Dict = dict(
            successfully_updated_items_count=len(batch_update_devices)
            - update_failed_requests_count,
            successfully_created_items_count=len(create_devices)
            - create_failed_requests_count,
            failed_updated_items_count=update_failed_requests_count,
            failed_created_items_count=create_failed_requests_count,
        )
        if cw_error_msgs:
            return result, serial_numbers_crm_id_mapping, cw_error_msgs
        return result, serial_numbers_crm_id_mapping, []

    def update_device(self, device_id, data):
        """
        Update Device Info to connectwise
        :param device_id: CW device ID
        :param data: Full updated payload of device
        :return: Updated Device data
        """
        patch_data = prepare_device_patch_data(data)
        if not patch_data:
            return
        updated_device = self.client.update_configuration(
            device_id, json=patch_data
        )
        return updated_device

    def create_batch_create_device_data(self, customer_id, category_id, data):
        batch_request_items = []
        for item in data:
            body = prepare_device_post_data(customer_id, category_id, item)
            batch_request_item = BatchRequestItem(
                RequestMethods.POST, "/company/configurations/", body
            )
            batch_request_items.append(batch_request_item)
        return batch_request_items

    def batch_create_device_data(self, category_id, data_dict):
        """
        data_dict = {
                    'customer_id_1': [device_data],
                    'customer_id_2': [device_data]
                }
        """
        batch_requests_items = []
        for _company_id, device_data in data_dict.items():
            _batch_requests_items = self.create_batch_create_device_data(
                _company_id, category_id, device_data
            )
            batch_requests_items.extend(_batch_requests_items)
        return batch_requests_items

    @batch_call(300)
    def batch_create_devices(self, data):
        """
        Create Devices in Batch
        :param data:
        :return:
        """
        batch_request_data = BatchRequest(data).to_dict()
        response_ = self.client.batch_request(json=batch_request_data)
        return response_

    @batch_call(300)
    def batch_update_devices(self, data):
        """
        Update Devices in Batch
        :param company_id:
        :param data:
        :return:
        """
        batch_request_items = []
        for device in data:
            payload = prepare_device_patch_data(device)
            device_id = device.get("device_id")
            batch_request_item = BatchRequestItem(
                RequestMethods.PATCH,
                f"/company/configurations/{device_id}",
                payload,
            )
            batch_request_items.append(batch_request_item)
        batch_request = BatchRequest(batch_request_items)
        response_ = self.client.batch_request(json=batch_request.to_dict())
        return response_

    def batch_delete_devices(self, device_ids):
        """
        Delete Devices in Batch
        :param device_ids:
        :return:
        """
        deleted_device_ids = []
        for device_id in device_ids:
            response_ = self.client.delete_configuration(device_id)
            if response_.status_code != 204:
                logger.info(f"Unable to delete device {device_id}")
                continue
            deleted_device_ids.append(device_id)
        return deleted_device_ids

    def update_device_by_value(self, device_ids, value, field_name):
        """
        Update contract for multiple devices
        :param device_ids: list of CW ids of devices
        :param value: New contract number
        :param field_name: Name of the field to update
        :return: None
        """
        batch_request_items = []
        for device_id in device_ids:
            patch_group = PatchGroup()
            patch_group.add(field_name, value)
            batch_request_item = BatchRequestItem(
                RequestMethods.PATCH,
                f"/company/configurations/{device_id}",
                patch_group.to_dict(),
            )
            batch_request_items.append(batch_request_item)
        batch_request = BatchRequest(batch_request_items)
        response = self.client.batch_request(json=batch_request.to_dict())
        result = []
        for res in response:
            result.append({"code": res.status_code, "response": res.json()})
        return result

    def update_device_names(self, devices):
        """
        Update device names for devices.
        """
        batch_request_items = []
        for _device in devices:
            patch_group = PatchGroup()
            patch_group.add("name", _device.get("device_name"))
            batch_request_item = BatchRequestItem(
                RequestMethods.PATCH,
                f"/company/configurations/{_device.get('id')}",
                patch_group.to_dict(),
            )
            batch_request_items.append(batch_request_item)
        batch_request = BatchRequest(batch_request_items)
        response_ = self.client.batch_request(json=batch_request.to_dict())
        return response_

    def get_customers(self, full_info=False, filters=None):
        filters = filters or []
        if full_info:
            required_fields = [
                "id",
                "name",
                "identifier",
                "addressLine1",
                "addressLine2",
                "city",
                "state",
                "zip",
                "country",
                "phoneNumber",
                "faxNumber",
                "website",
                "status/id",
                "status/name",
                "defaultContact/id",
                "defaultContact/name",
                "deletedFlag",
                "_info/lastUpdated",
                "customFields",
                "territoryManager",
            ]
        else:
            required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        lead_flag_filter = CWConditions(
            boolean_conditions=[
                Condition("leadFlag", Operators.EQUALS, "false")
            ]
        )
        filters.append(lead_flag_filter)
        is_deleted_filter = CWConditions(
            boolean_conditions=[
                Condition("deletedFlag", Operators.EQUALS, "false")
            ]
        )
        filters.append(is_deleted_filter)
        data = self.client.get_companies(filters, all=True)
        return data

    def get_customer(self, customer_id, full_info=False, filters=None):
        filters = filters or []
        if not full_info:
            required_fields = [
                "id",
                "name",
                "identifier",
                "addressLine1",
                "addressLine2",
                "city",
                "state",
                "zip",
                "country",
                "phoneNumber",
                "faxNumber",
                "website",
                "status/id",
                "status/name",
                "defaultContact/id",
                "defaultContact/name",
                "deletedFlag",
                "_info/lastUpdated",
                "customFields",
                "territoryManager",
            ]
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)
        data = self.client.get_company(customer_id, filters)
        return data

    def update_customer_ms_customer_status(
        self, customer_id, ms_customer_field, value
    ):
        """
        Update ms customer field for customer
        Args:
            customer_id: Customer CRM ID
            ms_customer_field: ms customer field name
            value: value to be set

        Returns: None

        """
        custom_fields = self.get_customer(customer_id).get("custom_fields", [])
        for custom_field in custom_fields:
            if ms_customer_field == custom_field.get("caption"):
                custom_field["value"] = value
        patch_group = PatchGroup()
        patch_group.add("customFields", custom_fields)
        self.client.update_company(customer_id, json=patch_group.to_dict())

    def get_customer_users_crm_ids(self, customer_id=None, filters=None):
        """
        Returns list of ids for all contacts of a company.
        """
        required_fields = ["id"]
        crm_ids = self.get_customer_users(
            customer_id, filters, required_fields
        )
        result = [item.get("id") for item in crm_ids]
        return result

    def get_customer_users(
        self,
        customer_id=None,
        filters=None,
        required_fields=None,
        active_users_only=False,
        type_filter=None,
    ):
        filters = filters or []
        if not required_fields:
            required_fields = [
                "id",
                "firstName",
                "lastName",
                "department/name",
                "communicationItems",
                "title",
                "inactiveFlag",
                "company/id",
                "linkedInUrl",
                "twitterUrl",
            ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        if customer_id:
            customer_id_filter = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customer_id)
                ]
            )
            filters.append(customer_id_filter)
        if active_users_only:
            inactive_filter = CWConditions(
                boolean_conditions=[
                    Condition("inactiveFlag", Operators.EQUALS, False)
                ]
            )
            filters.append(inactive_filter)

        if type_filter:
            contact_type_filter = CWChildConditions(
                int_conditions=[
                    Condition("types/id", Operators.EQUALS, type_filter)
                ]
            )
            filters.append(contact_type_filter)
        data = self.client.get_company_contacts(filters, all=True)
        return data

    def get_contact_types(self) -> List[Dict]:
        """
        List CW customer contact types.
        """
        contact_types: List[Dict] = self.client.get_company_contact_types()
        return contact_types

    def get_customer_contacts_details(
        self, contact_crm_ids: List[int]
    ) -> List[Dict]:
        """
        Get contact information of given contact CRM IDs.

        Args:
            contact_crm_ids: List of contact CRM IDs.

        Returns:
            Contact information
        """
        filters: List = list()

        required_fields: List[str] = [
            "id",
            "firstName",
            "lastName",
            "inactiveFlag",
            "company/id",
            "defaultPhoneType",
            "defaultPhoneNbr",
            "communicationItems",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)

        contact_crm_ids_filter = CWConditions(
            int_conditions=[
                Condition("id", Operators.IN, tuple(contact_crm_ids))
            ]
        )
        filters.append(contact_crm_ids_filter)

        customer_contacts: List[Dict] = self.client.get_company_contacts(
            filters, all=True
        )
        return customer_contacts

    def get_communication_types(self):
        """
        Gives the list of company communication Types
        """
        return self.client.get_company_communication_types(all=True)

    def get_phone_communication_types(self):
        """
        Gives the list of company phone communication Types
        """
        filters = [
            CWConditions(
                boolean_conditions=[
                    Condition("phoneFlag", Operators.EQUALS, True)
                ]
            )
        ]
        return self.client.get_company_communication_types(
            filters=filters, all=True
        )

    def get_customer_user(self, user_id, filters=None):
        filters = filters or []
        required_fields = [
            "id",
            "firstName",
            "lastName",
            "department/name",
            "communicationItems",
            "title",
            "inactiveFlag",
            "company/id",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        data = self.client.get_contact(user_id, filters, all=True)
        return data

    def create_customer_user(
        self,
        customer_id: int,
        data: Dict,
        phone_mapping_setting,
    ) -> Dict:
        payload = {
            "firstName": data.get("first_name"),
            "lastName": data.get("last_name"),
            "company": {"id": customer_id},
            "title": data.get("profile").get("title"),
            "communicationItems": [
                {
                    "communicationType": ConnectwiseCommunicationType.EMAIL.value,
                    "value": data.get("email"),
                    "type": {"id": 1},
                }
            ],
            "twitterUrl": data.get("profile", {}).get("twitter_profile_url"),
            "linkedInUrl": data.get("profile", {}).get("linkedin_profile_url"),
        }
        if phone_mapping_setting:
            cell_phone_number = data.get("profile", {}).get(
                "cell_phone_number"
            )
            office_phone = data.get("profile", {}).get("office_phone")
            if cell_phone_number:
                payload["communicationItems"].append(
                    {
                        "communicationType": ConnectwiseCommunicationType.PHONE.value,
                        "value": cell_phone_number,
                        "type": {"id": phone_mapping_setting.cell_phone},
                    }
                )
            if office_phone:
                payload["communicationItems"].append(
                    {
                        "communicationType": ConnectwiseCommunicationType.PHONE.value,
                        "value": office_phone,
                        "type": {"id": phone_mapping_setting.office_phone},
                    }
                )
        user = self.client.create_contact(json=payload)
        return user

    def update_contact_communication_item(
        self, user_id: int, communication_item_id: int, value
    ):
        payload = {"value": value}
        patch_group = PatchGroup()
        for key, value in payload.items():
            patch_group.add(key, value)
        self.client.update_customer_contact_communication_item(
            user_id, communication_item_id, json=patch_group.to_dict()
        )

    def update_customer_user(self, user_id: int, data: Dict) -> None:
        """
        Updates connectwise contact
        :param user_id: Contact CRM id
        :param data: updates to be sent
        """
        profile_data = data.get("profile", {})
        payload = {
            "firstName": data.get("first_name"),
            "lastName": data.get("last_name"),
            "title": profile_data.get("title"),
            "twitterUrl": profile_data.get("twitter_profile_url"),
            "linkedInUrl": profile_data.get("linkedin_profile_url"),
        }
        patch_group = PatchGroup()
        for key, value in payload.items():
            if value:
                patch_group.add(key, value)
        updates = patch_group.to_dict()
        if updates:
            self.client.update_customer_contact(user_id, json=updates)

        # Call method to update phone numbers
        self.update_phone_numbers(profile_data, user_id)

    def update_phone_numbers(self, payload: Dict, user_id: int):
        """
        Updates users cell phone and office phone numbers
        :param payload: payload of the form
            {
            "cell_phone_number": "8866554433"
            "cell_phone_number_crm_id": 223,
            "office_phone": "6666666666",
            "office_phone_crm_id": 332
        }
        :param user_id: User CRM id
        """

        cell_phone_number = payload.get("cell_phone_number")
        cell_phone_number_crm_id = payload.get("cell_phone_number_crm_id")
        office_phone = payload.get("office_phone")
        office_phone_crm_id = payload.get("office_phone_crm_id")
        if cell_phone_number and cell_phone_number_crm_id:
            self.update_contact_communication_item(
                user_id, cell_phone_number_crm_id, cell_phone_number
            )
        if office_phone and office_phone_crm_id:
            self.update_contact_communication_item(
                user_id, office_phone_crm_id, office_phone
            )

    def get_customer_users_count(self, customer_id, filters=None):
        filters = filters or []
        customer_id_filter = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.EQUALS, customer_id)
            ]
        )
        filters.append(customer_id_filter)
        data = self.client.get_contacts_count(filters)
        return data["count"]

    def get_customer_sites(
        self, customer_id, required_fields=None, filters=None
    ):
        filters = filters or []
        required_fields = required_fields or [
            "id",
            "name",
            "phoneNumber",
            "addressLine1",
            "addressLine2",
            "city",
            "stateReference",
            "zip",
            "country/id",
            "country/name",
        ]
        active_sites_filter = CWConditions(
            boolean_conditions=[
                Condition("inactiveFlag", Operators.EQUALS, False)
            ]
        )
        fields_filter = CWFieldsFilter(required_fields)
        order_by_name_filter = CWOrderBy("name", Order.ASC)
        order_by_updated_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        order_by_name_filter.extend(order_by_updated_filter)
        filters.extend(
            [fields_filter, order_by_name_filter, active_sites_filter]
        )
        data = self.client.get_company_sites(
            customer_id, filters=filters, all=True
        )
        return data

    def get_customer_complete_site_addresses(self, customer_id, site_ids):
        if not isinstance(site_ids, list):
            site_ids = list(site_ids)
        site_filters = CWConditions(
            int_conditions=[
                Condition("id", Operators.IN, tuple(set(site_ids)))
            ]
        )
        response = self.get_customer_sites(customer_id, filters=[site_filters])
        result = {
            x.get("site_id"): "{}, {}, {}, {}, {}, {}".format(
                x.get("address_line_1") or "",
                x.get("address_line_2") or "",
                x.get("city") or "",
                x.get("state") or "",
                x.get("zip") or "",
                x.get("country") or "",
            )
            for x in response
        }
        return result

    def get_customer_complete_site_addresses_with_name(
        self, customer_id, site_ids
    ):
        if not isinstance(site_ids, list):
            site_ids = list(site_ids)
        site_filters = CWConditions(
            int_conditions=[
                Condition("id", Operators.IN, tuple(set(site_ids)))
            ]
        )
        response = self.get_customer_sites(customer_id, filters=[site_filters])
        result = {
            x.get("site_id"): ", ".join(
                list(
                    filter(
                        None,
                        [
                            x.get("name"),
                            x.get("address_line_1"),
                            x.get("address_line_2"),
                            x.get("city"),
                            x.get("state"),
                            x.get("zip"),
                            x.get("country"),
                        ],
                    )
                )
            )
            for x in response
        }
        return result

    def create_customer_site(
        self, customer_crm_id: int, site_details: Dict[str, Any]
    ):
        site_data: Dict[str, Any] = {
            "name": site_details.get("name"),
            "company": {"id": customer_crm_id},
            "phoneNumber": site_details.get("phone_number"),
            "addressLine1": site_details.get("address_line_1"),
            "addressLine2": site_details.get("address_line_2"),
            "city": site_details.get("city"),
            "stateReference": {"id": site_details.get("state_id")},
            "country": {"id": site_details.get("country_id")},
            "zip": site_details.get("zip"),
        }
        site = self.client.create_company_site(customer_crm_id, json=site_data)
        return site

    def get_manufacturers(self, filters=None):
        filters = filters or []
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        order_by_filter = CWOrderBy("name", Order.ASC)
        filters.extend([fields_filter, order_by_filter])
        data = self.client.get_manufacturers(filters=filters, all=True)
        return data

    def get_countries_list(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        order_by_filter = CWOrderBy("name", Order.ASC)
        filters = [fields_filter, order_by_filter]
        data = self.client.get_countries(filters, all=True)
        return data

    def get_states_list(self, country_id=None):
        required_fields = ["id", "name", "country/id", "identifier"]
        fields_filter = CWFieldsFilter(required_fields)
        order_by_filter = CWOrderBy("name", Order.ASC)
        filters = [fields_filter, order_by_filter]
        if country_id:
            country_filter = CWConditions(
                int_conditions=[
                    Condition("country/id", Operators.EQUALS, country_id)
                ]
            )
            filters.append(country_filter)
        data = self.client.get_states(filters, all=True)
        return data

    def get_countries_and_state_list(self):
        countries = self.get_countries_list()
        states = self.get_states_list()
        import core.utils as core_utils

        states = core_utils.group_list_of_dictionary_by_key(
            states, "country_id"
        )
        for country in countries:
            country.update({"states": states.get(country["country_id"], [])})
        return countries

    def get_device_statuses(self):
        statuses = self.client.get_configuration_statuses()
        return statuses

    def get_device_inactive_status_id(self):
        statuses = self.get_device_statuses()
        for status in statuses:
            if status.get("description") == "Inactive":
                return status["id"]

    def get_device_active_status_id(self):
        statuses = self.get_device_statuses()
        for status in statuses:
            if status.get("description") == "Active":
                return status["id"]

    def get_board_statuses(self, board_id, statuses=None, filters=None):
        """
        Returns list of available for board.
        """
        filters = filters or []
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        if statuses:
            status_filters = CWConditions(
                int_conditions=[Condition("id", Operators.IN, tuple(statuses))]
            )
            filters.append(status_filters)
        statuses = self.client.get_board_statuses(board_id, filters)
        return statuses

    def get_board_status_by_id(self, board_id, status_id, filters=None):
        """
        Returns list of available for board.
        """
        filters = filters or []
        required_fields = ["id", "name", "timeEntryNotAllowed"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        statuses = self.client.get_board_statuses_by_id(
            board_id, status_id, filters
        )
        return statuses

    #  Service request related methods
    def get_service_tickets(
        self, customer_id, boards=None, statuses=None, filters=None
    ):
        filters = filters or []
        required_fields = [
            "id",
            "summary",
            "recordType",
            "board/id",
            "board/name",
            "status/id",
            "status/name",
            "company/id",
            "company/name",
            "priority/id",
            "priority/name",
            "severity",
            "impact",
            "closedFlag",
            "closedDate",
            "_info/lastUpdated",
            "_info/dateEntered",
            "contactName",
            "contactPhoneNumber",
            "type/name",
            "subType/name",
            "contactEmailAddress",
            "opportunity/id",
            "opportunity/name",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        int_conditions = [
            Condition("company/id", Operators.EQUALS, customer_id),
            Condition("parentTicketId", Operators.EQUALS, "null"),
        ]
        if boards:
            int_conditions.append(
                Condition("board/id", Operators.IN, tuple(boards))
            )
        if statuses:
            int_conditions.append(
                Condition("status/id", Operators.IN, tuple(statuses))
            )
        ticket_filters = CWConditions(int_conditions=int_conditions)
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([fields_filter, ticket_filters, order_by_filter])
        tickets = self.client.get_service_tickets(filters, all=True)
        return tickets

    def get_service_tickets_by_board_and_status(
        self,
        boards: List[int] = None,
        statuses: List[int] = None,
        filters: List = None,
        created_date: Optional[date] = None,
    ):
        filters = filters or []
        if not filters:
            required_fields = [
                "id",
                "summary",
                "recordType",
                "board/id",
                "board/name",
                "status/id",
                "status/name",
                "company/id",
                "company/name",
                "opportunity/id",
                "opportunity/name",
                "_info/lastUpdated",
                "owner",
            ]
            fields_filter = CWFieldsFilter(required_fields)
            int_conditions = [
                Condition("parentTicketId", Operators.EQUALS, "null"),
            ]
            if boards:
                int_conditions.append(
                    Condition("board/id", Operators.IN, tuple(boards))
                )
            if statuses:
                int_conditions.append(
                    Condition("status/id", Operators.IN, tuple(statuses))
                )
            if created_date:
                created_date_filter = CWConditions(
                    datetime_conditions=[
                        Condition(
                            "_info/dateEntered",
                            Operators.GREATER_THEN_EQUALS,
                            datetime.strftime(
                                created_date,
                                "%Y-%m-%dT%H:%M:%SZ",
                            ),
                        )
                    ]
                )
                filters.append(created_date_filter)
            ticket_filters = CWConditions(int_conditions=int_conditions)
            order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
            filters.extend([fields_filter, ticket_filters, order_by_filter])
        tickets = self.client.get_service_tickets(filters, all=True)
        return tickets

    def get_service_tickets_with_generator(
        self,
        customers: Optional[List[int]] = None,
        created_date: Optional[date] = None,
        boards: Optional[List[int]] = None,
        statuses: Optional[List[int]] = None,
        fields: Optional[List[str]] = None,
        filters: Optional[List[Any]] = None,
    ) -> Generator[Response, None, None]:
        filters = filters or []
        required_fields = fields or [
            "id",
            "summary",
            "board",
            "status",
            "company",
            "_info",
            "owner",
            "opportunity/id",
            "opportunity/name",
        ]

        if customers:
            company_filter = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customers)
                ]
            )
            filters.append(company_filter)

        if statuses:
            status_filter = CWConditions(
                int_conditions=[
                    Condition("status/id", Operators.IN, tuple(statuses))
                ]
            )
            filters.append(status_filter)

        if boards:
            boards_filter = CWConditions(
                int_conditions=[
                    Condition("board/id", Operators.IN, tuple(boards))
                ]
            )
            filters.append(boards_filter)

        if created_date:
            created_date_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "_info/dateEntered",
                        Operators.GREATER_THEN_EQUALS,
                        datetime.strftime(
                            created_date,
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                    )
                ]
            )
            filters.append(created_date_filter)
        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)
        tickets = self.client.get_service_tickets_using_generator(
            filters=filters
        )
        return tickets

    def get_all_opportunities(
        self,
        created_date,
    ) -> Generator[Response, None, None]:
        filters: List = []
        required_fields: List[str] = [
            "id",
            "name",
            "status/id",
            "status/name",
            "company/id",
            "company/name",
            "customerPO",
            "closedDate",
            "_info/lastUpdated",
            "stage/id",
            "stage/name",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        if created_date:
            created_date_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "_info/lastUpdated",
                        Operators.GREATER_THEN_EQUALS,
                        datetime.strftime(
                            created_date,
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                    )
                ]
            )
            filters.append(created_date_filter)
        opportunities: Generator[
            Response, None, None
        ] = self.client.get_all_opportunities_using_generator(filters=filters)
        return opportunities

    def get_closed_service_tickets(self, customer_id, filters=None):
        """
        Return all closed service tickets, irrespective of the board
        """
        filters = filters or []
        closed_ticket_filter = CWConditions(
            boolean_conditions=[
                Condition("closedFlag", Operators.EQUALS, True)
            ]
        )
        filters.append(closed_ticket_filter)
        tickets = self.get_service_tickets(customer_id, filters=filters)
        return tickets

    def get_service_tickets_count(
        self, boards, statuses, customer_id=None, filters=None
    ):
        filters = filters or []
        ticket_filters = CWConditions(
            int_conditions=[
                Condition("board/id", Operators.IN, tuple(boards)),
                Condition("status/id", Operators.IN, tuple(statuses)),
                Condition("parentTicketId", Operators.EQUALS, "null"),
            ]
        )
        filters.append(ticket_filters)
        if customer_id:
            company_filter = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customer_id)
                ]
            )
            filters.append(company_filter)
        count = self.client.get_service_tickets_count(filters)
        return count

    #  Service request related methods
    def get_service_ticket(self, ticket_id, filters=None):
        filters = filters or []
        required_fields = [
            "id",
            "summary",
            "recordType",
            "board/id",
            "board/name",
            "status/id",
            "status/name",
            "company/id",
            "company/name",
            "priority/id",
            "priority/name",
            "severity",
            "impact",
            "owner",
            "_info/lastUpdated",
            "_info/dateEntered",
            "contactName",
            "contactPhoneNumber",
            "contactEmailAddress",
            "contact/id",
            "opportunity",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.extend([fields_filter])
        tickets = self.client.get_service_ticket(ticket_id, filters)
        return tickets

    def create_service_ticket(self, company_id, board_id, created_by, payload):
        if payload.get("primary_contact"):
            contact = payload.get("primary_contact")
        else:
            contact = created_by
        ticket_payload: Dict = dict(
            summary=payload.get("summary"),
            company=dict(id=company_id),
            board=dict(id=board_id),
            contact=dict(id=contact),
        )
        ticket: Dict = self.client.create_service_ticket(json=ticket_payload)
        return ticket

    def update_service_ticket_status(
        self, ticket_id, status_id=None, opportunity=None
    ):
        patch_group = PatchGroup()
        if status_id:
            patch_group.add("status/id", status_id)
        if opportunity:
            patch_group.add("opportunity", opportunity)
        ticket = self.client.update_service_ticket(
            ticket_id, json=patch_group.to_dict()
        )
        return ticket

    def get_service_ticket_notes(
        self,
        ticket_id: int,
        filters: Optional[List] = None,
        external_flag_filter: bool = True,
        internal_flag_filter: bool = False,
    ) -> List[Dict]:
        """
        Returns service ticket notes which have externalFlag=true
        """
        filters = filters or []
        if external_flag_filter:
            external_flag_filter = CWConditions(
                boolean_conditions=[
                    Condition("internalAnalysisFlag", Operators.EQUALS, False)
                ]
            )
            filters.extend([external_flag_filter])
        if internal_flag_filter:
            internal_notes_flag_filter = CWConditions(
                boolean_conditions=[
                    Condition("internalAnalysisFlag", Operators.EQUALS, True)
                ]
            )
            filters.extend([internal_notes_flag_filter])
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([order_by_filter])
        notes: List[Dict] = self.client.get_service_ticket_notes(
            ticket_id, filters, all=True
        )
        return notes

    def get_service_ticket_documents(self, ticket_id):
        """
        Returns service ticket documents list
        :param ticket_id:
        :return:
        """
        params = dict(recordType="Ticket", recordId=ticket_id)
        filters = [
            CWConditions(
                boolean_conditions=[
                    Condition("linkFlag", Operators.EQUALS, False)
                ]
            )
        ]
        documents = self.client.get_documents(filters, all=True, **params)
        return documents

    def get_project_documents(self, project_id):
        """
        Returns project documents list
        :param project_id:
        :return:
        """
        params = dict(recordType="Project", recordId=project_id)
        documents = self.client.get_documents(all=True, **params)
        return documents

    def get_document_download_url(self, document_id):
        return self.client.get_document_download_url(document_id)

    def create_service_ticket_note(
        self, ticket_id, created_by, payload, internal=False
    ):
        external_flag = not internal
        note_payload = dict(
            text=payload.get("text"),
            contact=dict(id=created_by),
            externalFlag=external_flag,
            internalFlag=internal,
            detailDescriptionFlag=True,
        )
        note = self.client.create_service_ticket_note(
            ticket_id, json=note_payload
        )
        return note

    def create_ticket_discussion_note(
        self,
        ticket_crm_id: int,
        creator_crm_id: int,
        payload: Dict,
    ) -> Dict:
        note_payload: Dict = dict(
            text=payload.get("text"),
            contact=dict(id=creator_crm_id),
            internalAnalysisFlag=False,
            detailDescriptionFlag=True,
        )
        note: Dict = self.client.create_service_ticket_note(
            ticket_crm_id, json=note_payload
        )
        return note

    def create_ticket_internal_note(self, ticket_id, created_by, payload):
        note_payload = dict(
            text=payload.get("text"),
            contact=dict(id=created_by),
            internalAnalysisFlag=True,
        )
        note = self.client.create_service_ticket_note(
            ticket_id, json=note_payload
        )
        return note

    def create_service_ticket_document(
        self, ticket_id, file_name, file_object
    ):
        files = {
            "title": (None, file_name),
            "recordType": (None, "Ticket"),
            "recordId": (None, ticket_id),
            "file": (file_name, file_object),
        }
        doc = self.client.create_service_ticket_document(files=files)
        return doc

    def upload_system_attachments(self, payload):
        if (
            len(payload.get("file").name)
            > settings.CONNECTWISE_DOCUMENT_TITLE_MAX_LENGTH
        ):
            logger.info(
                f"Can not upload the file. "
                f"File title is > than the max. allowed "
                f"length {settings.CONNECTWISE_DOCUMENT_TITLE_MAX_LENGTH}."
            )
            return
        files = {
            "title": (None, payload["file"].name),
            "recordType": (None, payload.get("record_type")),
            "recordId": (None, payload.get("record_id")),
            "file": (payload["file"].name, payload["file"].file),
        }

        doc = self.client.create_system_document(files=files)
        return doc

    def get_projects_count(self, boards, statuses, customer_id, filters=None):
        filters = filters or []
        conditions = CWConditions(
            int_conditions=[
                Condition("board/id", Operators.IN, tuple(boards)),
                Condition("status/id", Operators.IN, tuple(statuses)),
            ]
        )
        filters.append(conditions)
        if customer_id:
            company_filter = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customer_id)
                ]
            )
            filters.append(company_filter)
        count = self.client.get_projects_count(filters)
        return count

    def get_project_notes(self, project_id, note_type_ids=None, filters=None):
        """
        Returns the notes for the project.
        """
        filters = filters or []
        if note_type_ids:
            notes_filter = CWConditions(
                int_conditions=[
                    Condition("type/id", Operators.IN, tuple(note_type_ids))
                ]
            )
            filters.append(notes_filter)
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([order_by_filter])
        return self.client.get_project_notes(project_id, filters)

    def create_project_notes(self, project_id, note, filters=None):
        """
        Returns the notes for the project.
        """
        project_notes_payload = dict(
            text=note,
            projectId=project_id,
        )
        project_notes = self.client.create_project_notes(
            project_id, json=project_notes_payload
        )
        return project_notes

    def update_project_notes(
        self, project_id, note_crm_id, note, filters=None
    ):
        patch_group = PatchGroup()
        if note:
            patch_group.add("text", note)
        project_notes = self.client.update_project_notes(
            project_id, note_crm_id, json=patch_group.to_dict()
        )
        return project_notes

    def delete_project_note(self, project_id, note_crm_id):
        return self.client.delete_project_note(project_id, note_crm_id)

    def get_orders_count(self, statuses, customer_id, filters=None):
        filters = filters or []
        conditions = CWConditions(
            int_conditions=[
                Condition("status/id", Operators.IN, tuple(statuses))
            ]
        )
        filters.append(conditions)
        if customer_id:
            company_filter = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customer_id)
                ]
            )
            filters.append(company_filter)

        count = self.client.get_orders_count(filters)
        return count

    def get_device_expiration_date_by_crm_ids(self, crm_ids):
        """
        Returns expiration dates for device ids in crm_ids.
        """
        required_fields = ["id", "warrantyExpirationDate"]
        filters = [
            CWConditions(
                int_conditions=[Condition("id", Operators.IN, tuple(crm_ids))]
            )
        ]
        expiration_dates = self.get_devices(
            required_fields=required_fields, filters=filters
        )
        return expiration_dates

    def get_company_note_types(self):
        """
        Get all customer note types.
        """
        return self.client.get_company_note_types()

    def get_company_notes(self, customer_id, filters=None):
        """
        Returns the notes for the customer.
        """
        filters = filters or []
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([order_by_filter])
        return self.client.get_company_notes(customer_id, filters)

    def get_company_note(self, customer_id, note_id, filters=None):
        """
        Returns the note by `note_id` for the customer.
        """
        filters = filters or []
        return self.client.get_company_note(customer_id, note_id, filters)

    def get_opportunity_types(self, filters=None):
        filters = filters or []
        required_fields = ["id", "description"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        return self.client.get_opportunity_types(filters, all=True)

    def get_opportunity_statuses(self, filters=None):
        filters = filters or []
        return self.client.get_opportunity_statuses(filters, all=True)

    def get_opportunity_stages(self, stages=None, filters=None):
        filters = filters or []
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        if stages:
            stage_filters = CWConditions(
                int_conditions=[Condition("id", Operators.IN, tuple(stages))]
            )
            filters.append(stage_filters)
        filters.append(fields_filter)
        return self.client.get_opportunity_stages(filters, all=True)

    def list_opportunity_teams(
        self, opportunity_crm_id: int, filters=None
    ) -> List[Dict]:
        """
        List opportunity teams.
        """
        filters = filters or []
        required_fields: List[str] = [
            "id",
            "type",
            "member",
            "commissionPercent",
            "referralFlag",
            "responsibleFlag",
            "opportunityId",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        return self.client.get_opportunity_teams(
            opportunity_crm_id, filters, all=True
        )

    def get_opportunity_team(
        self, opportunity_crm_id: int, team_crm_id: int, filters: List = None
    ) -> Dict:
        """
        Get opportunity team.
        """
        filters = filters or []
        required_fields: List[str] = [
            "id",
            "type",
            "member",
            "commissionPercent",
            "referralFlag",
            "responsibleFlag",
            "opportunityId",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        return self.client.get_opportunity_team(
            opportunity_crm_id, team_crm_id, filters=filters
        )

    def create_opportunity_team_of_individual_type(
        self, opportunity_crm_id: int, member_crm_id: int
    ) -> Dict[str, Any]:
        """
        Create opportunity team of Indivdual type.
        """
        team_payload: Dict = {
            "type": "Individual",
            "member": {"id": member_crm_id},
        }
        return self.client.create_opportunity_team(
            opportunity_crm_id, json=team_payload
        )

    def delete_opportunity_team(
        self, opportunity_crm_id: int, team_crm_id: int
    ):
        """
        Delete opportunity team.
        """
        return self.client.delete_opportunity_team(
            opportunity_crm_id, team_crm_id
        )

    def update_opportunity_team(
        self,
        opportunity_crm_id: int,
        team_crm_id: int,
        updated_system_member_crm_id: int,
    ) -> Dict[str, Any]:
        """
        Update opportunity team.
        """
        patch = PatchGroup()
        patch.add("member", dict(id=updated_system_member_crm_id))
        return self.client.patch_opportunity_team(
            opportunity_crm_id, team_crm_id, json=patch.to_dict()
        )

    def create_company_quote(self, customer_id, open_status, payload):
        """
        Create Opportunity by `customer_id` for the customer.
        """
        opportunity_payload = {
            "company": {"id": customer_id},
            "name": payload.get("name"),
            "primarySalesRep": {"id": payload.get("territory_manager_id")},
            "contact": {"id": payload.get("user_id")},
            "type": {"id": payload.get("type_id")},
            "expectedCloseDate": payload.get("close_date"),
            "stage": {"id": payload.get("stage_id")},
            "status": {"id": open_status},
            "businessUnitId": payload.get("business_unit_id"),
        }
        if payload.get("inside_rep_id"):
            opportunity_payload.update(
                {"secondarySalesRep": {"id": payload.get("inside_rep_id")}}
            )
        return self.client.create_company_opportunity(json=opportunity_payload)

    def patch_quote(self, quote_id: int, payload: Dict):
        """
        Update Opportunity for the customer.
        """
        patch_group = PatchGroup()
        if payload.get("stage_id"):
            patch_group.add("stage/id", payload.get("stage_id"))
        if payload.get("status_id"):
            patch_group.add("status/id", payload.get("status_id"))
        return self.client.patch_opportunity(
            quote_id, json=patch_group.to_dict()
        )

    def update_company_quote(
        self, customer_id: int, quote_id: int, payload: Dict
    ):
        """
        Update Opportunity by `customer_id` for the customer.
        """
        opportunity_payload: Dict[str, Union[int, Dict, str]] = dict(
            company=dict(id=customer_id)
        )
        if payload.get("name", None):
            opportunity_payload.update(name=payload.get("name"))
        if payload.get("territory_manager_id", None):
            opportunity_payload.update(
                primarySalesRep=dict(id=payload.get("territory_manager_id"))
            )
        if payload.get("user_id", None):
            opportunity_payload.update(contact=dict(id=payload.get("user_id")))
        if payload.get("type_id", None):
            opportunity_payload.update(type=dict(id=payload.get("type_id")))
        if payload.get("close_date", None):
            opportunity_payload.update(
                expectedCloseDate=payload.get("close_date")
            )
        if payload.get("stage_id", None):
            opportunity_payload.update(stage=dict(id=payload.get("stage_id")))
        if payload.get("status_id", None):
            opportunity_payload.update(
                status=dict(id=payload.get("status_id"))
            )
        if payload.get("rating_id", None):
            opportunity_payload.update(
                rating=dict(id=payload.get("rating_id"))
            )
        if payload.get("business_unit_id", None):
            opportunity_payload.update(
                businessUnitId=payload.get("business_unit_id")
            )
        if payload.get("location_id", None):
            opportunity_payload.update(locationId=payload.get("location_id"))
        return self.client.update_company_opportunity(
            quote_id, json=opportunity_payload
        )

    def get_company_quotes(
        self,
        customer_id,
        statuses,
        stages,
        business_unit_id,
        filters=None,
        **kwargs,
    ):
        """
        Returns the Opportunities by `customer_id` for the customer.
        """
        filters = filters or []
        required_fields = [
            "id",
            "name",
            "company",
            "shipToContact",
            "status",
            "stage",
            "type",
            "dateBecameLead",
            "closedDate",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        if stages:
            stage_filter = CWConditions(
                int_conditions=[
                    Condition("stage/id", Operators.IN, tuple(stages))
                ]
            )
            filters.append(stage_filter)
        if business_unit_id:
            business_unit_id_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "businessUnitId", Operators.IN, tuple(business_unit_id)
                    )
                ]
            )
            filters.append(business_unit_id_filter)
        quote_filters = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.EQUALS, customer_id),
                Condition("status/id", Operators.IN, tuple(statuses)),
            ]
        )
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        if kwargs.get("won_only", False):
            order_by_filter = CWOrderBy("closedDate", Order.DESC)
        filters.extend([quote_filters, order_by_filter])
        return self.client.get_company_opportunities(filters, all=True)

    def get_company_quote(self, quote_id, customer_id=None, filters=None):
        filters = filters or []
        required_fields = [
            "id",
            "name",
            "company",
            "shipToContact",
            "status",
            "stage",
            "type",
            "contact",
            "closedDate",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        if customer_id:
            quote_filters = CWConditions(
                int_conditions=[
                    Condition("company/id", Operators.EQUALS, customer_id)
                ]
            )
            filters.append(quote_filters)
        filters.append(fields_filter)
        return self.client.get_company_opportunity(quote_id, filters)

    def get_company_quote_statuses(self, quote_ids, filters=None):
        """
        Return quotes for the quote ids

        Args:
            quote_ids: list of quotes ids
            customer_id: Customer id
            filters: Filters

        """
        initial_filters = filters or []
        required_fields = [
            "id",
            "name",
            "status",
            "stage",
            "expectedCloseDate",
            "closedDate",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        initial_filters.append(fields_filter)
        statuses = []
        for _quote_ids in chunks_of_list_with_max_item_count(quote_ids, 100):
            filters = initial_filters[:]
            quotes_filter = CWConditions(
                int_conditions=[
                    Condition("id", Operators.IN, tuple(_quote_ids))
                ]
            )
            filters.append(quotes_filter)
            _statuses = self.client.get_company_opportunities(
                filters=filters, all=True
            )
            statuses.extend(_statuses)
        return statuses

    def get_opportunity_forecast_items(self, quote_id, filters=None):
        filters = filters or []
        required_fields = ["forecastItems"]
        fields_filter = CWFieldsFilter(required_fields)
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([order_by_filter, fields_filter])
        result = self.client.get_opportunity_forecast_items(quote_id, filters)
        return result

    def get_opportunity_forecasts(self, quote_id, filters=None):
        """
        Get all Forecasts for Opportunity.
        """
        filters = filters or []
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.DESC)
        filters.extend([order_by_filter])
        return self.client.get_opportunity_forecasts(quote_id, filters)

    def get_opportunity_forecast(self, quote_id, forecast_id, filters=None):
        """
        Get Forecast for Opportunity.
        """
        filters = filters or []
        return self.client.get_opportunity_forecast(
            quote_id, forecast_id, filters
        )

    def create_opportunity_forecast(self, quote_id, payload):
        """
        Create Forecast for Opportunity.
        """
        _forecast_payload = {
            "type": payload.get("type") or "Service",
            "name": payload.get("name") or "Professional Services",
            "revenue": payload.get("revenue"),
            "cost": payload.get("cost"),
            "includedFlag": True,
        }
        return self.client.create_opportunity_forecast(
            quote_id, json=_forecast_payload
        )

    def create_opportunity_forecast_item(
        self, quote_id: int, payload: Dict, open_status: int
    ):
        """
        Create Forecast Item for Opportunity.
        """
        DEFAULT_FORECAST_NAME: str = "Professional Services"
        MAXIMUM_FORECAST_NAME_LENGTH: int = 50

        forecast_description: str = (
            payload.get("name") or DEFAULT_FORECAST_NAME
        )
        if len(forecast_description) > MAXIMUM_FORECAST_NAME_LENGTH:
            forecast_description: str = DEFAULT_FORECAST_NAME

        _forecast_payload: Dict[str, Any] = {
            "forecastItems": [
                {
                    "forecastType": payload.get("type") or "Service",
                    "forecastDescription": forecast_description,
                    "revenue": payload.get("revenue"),
                    "cost": payload.get("cost"),
                    "includeFlag": True,
                    "opportunity": {"id": quote_id},
                    "status": {"id": payload.get("status") or open_status},
                }
            ]
        }
        return self.client.create_opportunity_forecast(
            quote_id, json=_forecast_payload
        )

    def create_opportunity_forecast_items(
        self, quote_id: int, forecast_items: List[Dict], status_id: int
    ):
        cw_forecast_items: List[Dict] = list()
        for forecast_item in forecast_items:
            cw_forecast_items.append(
                dict(
                    cost=forecast_item.get("cost"),
                    revenue=forecast_item.get("revenue"),
                    forecastDescription=forecast_item.get("description"),
                    forecastType=forecast_item.get("type"),
                    quantity=forecast_item.get("quantity"),
                    includeFlag=True,
                    opportunity=dict(id=quote_id),
                    status=dict(id=status_id),
                )
            )
        cw_forecast_payload: Dict = dict(forecastItems=cw_forecast_items)
        return self.client.create_opportunity_forecast(
            quote_id, json=cw_forecast_payload
        )

    def update_opportunity_forecast(self, quote_id, forecast_id, payload):
        """
        Update Forecast for Opportunity.
        """
        patch_group = PatchGroup()
        for key, value in payload.items():
            patch_group.add(key, value)
        return self.client.update_opportunity_forecast(
            quote_id, forecast_id, json=patch_group.to_dict()
        )

    def delete_forecast(self, quote_id, forecast_id):
        return self.client.delete_forecast(quote_id, forecast_id)

    def get_company_quotes_count(
        self, customer_id, statuses, stages, filters=None
    ):
        """
        Returns the Count of opportunities for a customer by `customer_id`.
        """
        filters = filters or []
        quote_filters = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.EQUALS, customer_id),
                Condition("status/id", Operators.IN, tuple(statuses)),
                Condition("stage/id", Operators.IN, tuple(stages)),
            ]
        )
        filters.append(quote_filters)
        return self.client.get_company_opportunities_count(filters)

    def get_quote_documents(self, quote_id, filters=None):
        """
        Returns quote documents list
        :param quote_id:
        :return:
        """
        filters = filters or []
        order_by_filter = CWOrderBy("_info/lastUpdated", Order.ASC)
        filters.append(order_by_filter)
        params = dict(recordType="Opportunity", recordId=quote_id)
        documents = self.client.get_documents(filters, all=True, **params)
        return documents

    def get_delete_documents(self, document_id):
        """
        Delete document by ID
        :param document_id:
        :return:
        """
        documents = self.client.delete_document(document_id)
        return documents

    def create_company_note(self, customer_id, payload):
        """
        Create customer note.
        """
        note_payload = {
            "text": payload.get("text"),
            "type": {"id": payload.get("note_type_id")},
            "flagged": payload.get("flagged"),
        }
        return self.client.create_company_note(customer_id, json=note_payload)

    def update_company_note(self, customer_id, note_id, payload):
        """
        Update customer note.
        """
        note_payload = {
            "text": payload.get("text"),
            "type": {"id": payload.get("note_type_id")},
            "flagged": payload.get("flagged"),
        }
        return self.client.update_company_note(
            customer_id, note_id, json=note_payload
        )

    def update_customer_site(self, customer_id, site_id, payload):
        site_payload = {
            "name": payload.get("name"),
            "company": {"id": customer_id},
            "phoneNumber": payload.get("phone_number"),
            "addressLine1": payload.get("address_line_1"),
            "addressLine2": payload.get("address_line_2"),
            "city": payload.get("city"),
            "stateReference": {"id": payload.get("state_id")},
            "country": {"id": payload.get("country_id")},
            "zip": payload.get("zip"),
        }
        site = self.client.update_customer_site(
            customer_id, site_id, json=site_payload
        )
        return site

    def delete_customer_site(self, customer_crm_id: int, site_crm_id: int):
        """
        Delete customer site.
        """
        return self.client.delete_company_site(customer_crm_id, site_crm_id)

    def get_members(self, filters=None):
        filters = filters or []
        inactive_filters = CWConditions(
            boolean_conditions=[
                Condition("inactiveFlag", Operators.EQUALS, False)
            ]
        )
        filters.append(inactive_filters)
        return self.client.get_system_members(filters=filters, all=True)

    def get_member(self, member_id, filters=None):
        filters = filters or []
        return self.client.get_system_member(member_id, filters=filters)

    def get_territories(self, filters=None):
        filters = filters or []
        required_fields = [
            "id",
            "structureLevel",
            "name",
            "manager",
            "reportsTo",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        return self.client.get_locations(filters=filters, all=True)

    def get_territory(self, territory_id, filters=None):
        filters = filters or []
        required_fields = [
            "id",
            "structureLevel",
            "name",
            "manager",
            "reportsTo",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        return self.client.get_location(territory_id, filters=filters)

    def update_territory(self, territory_id, payload):
        territory_payload = {
            "name": payload.get("name"),
            "structureLevel": {"id": payload.get("structure_id")},
            "manager": {"id": payload.get("manager_id")},
            "reportsTo": {"id": payload.get("reports_to_id")},
        }
        return self.client.update_location(
            territory_id, json=territory_payload
        )

    def get_company_types(self, filters=None) -> List[Dict]:
        """
        Get company types from CW.

        Args:
            filters: Filters

        Returns:
            CW company types.
        """
        filters = filters or []
        required_fields = [
            "id",
            "name",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        return self.client.get_company_types(filters=filters, all=True)

    def get_vendor_type_customers(
        self,
        vendor_status_ids: Optional[List[int]] = None,
    ) -> List[Dict]:
        """
        Get customers of type vendors from CW.

        Args:
            vendor_status_ids: List of customer status IDs.

        Returns:
            Customers of type vendors
        """
        filters: List = list()

        required_fields: List[str] = [
            "id",
            "name",
            "identifier",
            "status/id",
            "status/name",
            "isVendorFlag",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)

        if vendor_status_ids:
            vendor_status_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "status/id", Operators.IN, tuple(vendor_status_ids)
                    )
                ]
            )
            filters.append(vendor_status_filter)

        is_vendor_flag_filter = CWConditions(
            boolean_conditions=[
                Condition("isVendorFlag", Operators.EQUALS, "true")
            ]
        )
        filters.append(is_vendor_flag_filter)

        is_deleted_filter = CWConditions(
            boolean_conditions=[
                Condition("deletedFlag", Operators.EQUALS, "false")
            ]
        )
        filters.append(is_deleted_filter)

        return self.client.get_companies(filters=filters, all=True)

    def create_customer_contact(
        self,
        customer_crm_id: int,
        create_payload: Dict,
        phone_mapping_setting,
    ) -> Dict:
        cw_contact_creation_payload: Dict = dict(
            firstName=create_payload.get("first_name"),
            lastName=create_payload.get("last_name"),
            company=dict(id=customer_crm_id),
            title=create_payload.get("title"),
            types=[dict(id=create_payload.get("contact_type_crm_id"))],
            defaultFlag=True,
        )
        communication_items: List[Dict] = list()
        if create_payload.get("office_phone_number", None):
            communication_items.append(
                {
                    "communicationType": ConnectwiseCommunicationType.PHONE.value,
                    "value": create_payload.get("office_phone_number"),
                    "type": {"id": phone_mapping_setting.office_phone},
                }
            )
        if create_payload.get("email", None):
            communication_items.append(
                {
                    "communicationType": ConnectwiseCommunicationType.EMAIL.value,
                    "value": create_payload.get("email"),
                    "type": {"id": 1},
                }
            )
        cw_contact_creation_payload.update(
            communicationItems=communication_items
        )
        customer_contact: Dict = self.client.create_contact(
            json=cw_contact_creation_payload
        )
        return customer_contact

    def create_vendor_type_customer(
        self, payload: Dict, sow_vendor_onboarding_settings
    ):
        """
        Create customer of type vendor in CW.

        Args:
            payload: Customer create payload.

        Returns:
            Created customer details.
        """
        customer_status_id: int = (
            sow_vendor_onboarding_settings.get_cw_status_id_for_vendor_creation()
        )
        customer_type_id: int = (
            sow_vendor_onboarding_settings.get_cw_customer_type_id_for_vendor_creation()
        )
        cw_customer_create_payload: Dict = dict(
            identifier=payload.get("customer_identifier"),
            name=payload.get("vendor_name"),
            addressLine1=payload.get("address_line_1"),
            addressLine2=payload.get("address_line_2"),
            city=payload.get("city"),
            state=payload.get("state"),
            zip=payload.get("zip"),
            territory=dict(id=payload.get("territory_crm_id")),
            types=[dict(id=customer_type_id)],
            status=dict(id=customer_status_id),
            site=dict(name=payload.get("customer_site_name")),
            website=payload.get("website"),
        )
        return self.client.create_company(json=cw_customer_create_payload)

    def get_customer_statuses(
        self, filters: Optional[List] = None
    ) -> List[Dict]:
        """
        Get customer statuses from CW.

        Args:
            filters: Filters

        Returns:
            List of customer statuses
        """
        filters: List = filters or []
        required_fields: List[str] = ["id", "name", "inactiveFlag"]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        return self.client.get_company_statuses(filters=filters, all=True)

    def get_default_customer_contacts(
        self, customer_crm_ids: List[int]
    ) -> List[Dict]:
        filters: List = list()

        required_fields: List[str] = [
            "id",
            "firstName",
            "lastName",
            "company/id",
            "company/name",
            "defaultPhoneNbr",
            "communicationItems",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)

        customer_ids_filter = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.IN, tuple(customer_crm_ids))
            ]
        )
        filters.append(customer_ids_filter)

        default_contact_filter = CWConditions(
            boolean_conditions=[
                Condition("defaultFlag", Operators.EQUALS, True)
            ]
        )
        filters.append(default_contact_filter)
        return self.client.get_company_contacts(filters=filters, all=True)

    def get_system_departments(self, filters=None):
        filters = filters or []
        return self.client.get_system_departments(filters=filters, all=True)

    # ************************* Purchase Order APIs ************************
    def get_purchase_order_list(self, filters=None):
        filters = filters or []
        return self.client.get_purchase_orders_list(filters=filters, all=True)

    def get_purchase_order_details(self, purchase_order_id, filters=None):
        filters = filters or []
        return self.client.get_purchase_orders_details(
            purchase_order_id, filters=filters, all=True
        )

    def get_purchase_order_list_with_generator(
        self,
        po_numbers_list,
        fields: Optional[List[str]] = None,
        filters: Optional[List[Any]] = None,
    ) -> Generator[Response, None, None]:
        filters = filters or []
        required_fields = fields or [
            "id",
            "customerCompany",
            "vendorCompany",
            "poNumber",
            "_info",
            "status",
            "internalNotes",
            "customerSite",
            "poDate",
            "locationId",
            "businessUnitId",
            "shipmentDate",
            "total",
            "enteredBy",
            "vendorInvoiceNumber",
            "shippingInstructions",
            "salesTax",
            "customFields",
        ]

        if po_numbers_list:
            po_numbers_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "poNumber", Operators.IN, tuple(po_numbers_list)
                    ),
                ]
            )
            filters.append(po_numbers_filter)

        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)

        purchase_orders = self.client.get_purchase_orders_list_using_generator(
            filters=filters
        )
        return purchase_orders

    def get_open_purchase_order_list_for_customer(
        self, customer_crm_id=None, status_ids=None, filters=None
    ):
        filters = filters or []
        if customer_crm_id:
            company_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "customerCompany/id", Operators.EQUALS, customer_crm_id
                    )
                ]
            )
            filters.append(company_filter)

        if status_ids:
            status_filter = CWConditions(
                int_conditions=[
                    Condition("status/id", Operators.IN, tuple(status_ids))
                ]
            )
            filters.append(status_filter)
        return self.get_purchase_order_list(filters)

    def get_line_items(self, purchase_order_id, filters=None, **kwargs):
        filters = filters or []
        include_closed = kwargs.get("include_closed", False)
        if not include_closed:
            closed_filter = CWConditions(
                boolean_conditions=[
                    Condition("closedFlag", Operators.EQUALS, False)
                ]
            )
            filters.append(closed_filter)
        return self.client.get_purchase_order_line_items(
            purchase_order_id, filters=filters, all=True
        )

    def get_purchase_order_line_items(
        self, purchase_order_id, product_ids=None, **kwargs
    ):
        product_ids = product_ids or []
        filters = kwargs.get("fields_filter", [])
        data = self.get_line_items(
            purchase_order_id, filters=filters, **kwargs
        )
        if product_ids:
            data = [
                rec
                for rec in data
                if rec.get("product", {}).get("identifier") in product_ids
            ]
        return data

    def get_purchase_order_line_item(
        self, purchase_order_id, line_item_id, filters
    ):
        return self.client.get_purchase_order_line_item(
            purchase_order_id, line_item_id, filters
        )

    def update_purchase_order_custom_field(
        self, purchase_order_crm_id: int, custom_fields: List[Dict]
    ):
        patch_group = PatchGroup()
        patch_group.add("customFields", custom_fields)
        patch_dict = patch_group.to_dict()
        return self.client.update_purchase_order_custom_field_values(
            purchase_order_crm_id, json=patch_group.to_dict()
        )

    def _update_purchase_order_line_item(
        self, purchase_order_id, line_item_id, payload
    ):
        return self.client.update_purchase_order_line_item(
            purchase_order_id, line_item_id, json=payload
        )

    def update_purchase_order_line_item(
        self, provider, purchase_order_id, line_item_id, **kwargs
    ):
        serial_numbers = kwargs.get("serial_numbers", [])
        received_quantity = kwargs.get("received_qty")
        shipping_method_id = kwargs.get("shipping_method_id")
        received_status = kwargs.get("received_status")
        ship_date = kwargs.get("ship_date")
        notes = kwargs.get("note", "")
        serial_notes = ""
        if len(serial_numbers) > 8:
            serial_notes = (
                f'\nRemaining Serial Numbers: {",".join(serial_numbers[8:])}'
            )
        if notes or serial_notes:
            internal_notes = notes + " " + serial_notes
        else:
            internal_notes = None
        payload = {
            "serialNumbers": ",".join(serial_numbers[:8]),
            "shipDate": ship_date,
            "receivedQuantity": int(received_quantity),
            "trackingNumber": kwargs.get("tracking_number"),
            "internalNotes": internal_notes,
        }
        if shipping_method_id:
            payload.update({"shipmentMethod": {"id": shipping_method_id}})

        if received_status:
            payload.update({"receivedStatus": received_status})

        payload = {k: v for k, v in payload.items() if v}
        patch_group = PatchGroup()
        for key, value in payload.items():
            patch_group.add(key, value)

        return self._update_purchase_order_line_item(
            purchase_order_id, line_item_id, patch_group.to_dict()
        )

    def list_partial_receive_status(self):
        return partial_received_status

    # get all sales orders
    def get_sales_orders(
        self,
        created_date: Optional[date] = None,
        fields: Optional[List[Any]] = None,
        filters: Optional[List[Any]] = None,
    ) -> Generator[Response, None, None]:
        filters = filters or []
        required_fields = fields or [
            "id",
            "poNumber",
            "orderDate",
            "opportunity/name",
            "opportunity/id",
            "site/id",
            "site/name",
            "company/id",
            "company/name",
            "status/id",
            "status/name",
            "productIds",
            "billClosedFlag",
            "invoiceIds",
            "shipToContact",
            "shipToSite",
            "dueDate",
            "total",
            "subTotal",
            "taxTotal",
            "location/name",
            "customFields",
            "_info/lastUpdated",
        ]
        # create date or order date are same for sales orders
        if created_date:
            created_date_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "orderDate",
                        Operators.GREATER_THEN_EQUALS,
                        datetime.strftime(
                            created_date,
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                    )
                ]
            )
            filters.append(created_date_filter)
        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)
        sales_orders_generator_data = self.client.get_sales_orders(
            filters=filters
        )
        return sales_orders_generator_data

    def get_sales_order_by_id(
        self,
        sales_order_crm_id: int,
        fields: Optional[List[Any]] = None,
        filters: Optional[List[Any]] = None,
    ):
        filters = filters or []
        required_fields = fields or [
            "id",
            "customFields",
        ]
        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)
        sales_order_data = self.client.get_sales_order_by_id(
            sales_order_crm_id=sales_order_crm_id, filters=filters
        )
        return sales_order_data

    def update_sales_order_custom_field(
        self, sales_order_crm_id: int, custom_fields: List[Dict]
    ):
        patch_group = PatchGroup()
        patch_group.add("customFields", custom_fields)
        return self.client.update_sales_order_custom_field_values(
            sales_order_crm_id, json=patch_group.to_dict()
        )

    # cw sales orders status listing
    def list_sales_order_statuses(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        return self.client.get_order_statuses(filters=[fields_filter])

    # cw orders products details/listing
    def get_sales_order_products_list_with_generator(
        self,
        sales_order_list,
        fields: Optional[List[str]] = None,
        filters: Optional[List[Any]] = None,
    ) -> Generator[Response, None, None]:
        filters = filters or []
        required_fields = fields or [
            "id",
            "catalogItem/identifier",
            "description",
            "quantity",
            "salesOrder/id",
            "invoice/id",
            "invoice/identifier",
            "quantityCancelled",
            "cost",
            "price",
            "sequenceNumber",
        ]

        if sales_order_list:
            so_ids_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "salesOrder/id", Operators.IN, tuple(sales_order_list)
                    ),
                ]
            )
            filters.append(so_ids_filter)

        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)

        sales_orders_products = (
            self.client.get_product_item_details_using_generator(
                filters=filters
            )
        )
        return sales_orders_products

    # get product item details
    def get_product_item_details(self, product_crm_id, **kwargs):
        filters = kwargs.get("fields_filter", [])
        data = self.client.get_product_item_details(
            product_crm_id, filters=filters, **kwargs
        )
        return data

    def list_shipment_methods(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        return self.client.list_shipment_methods(
            filters=[fields_filter]
        ).json()

    def list_purchase_order_statuses(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        return self.client.list_purchase_order_statuses(
            filters=[fields_filter]
        ).json()

    def create_callback(self, description, url, object_id, entity_type, level):
        callback_payload = {
            "description": description,
            "url": url,
            "objectId": object_id,
            "type": entity_type,
            "level": level,
        }

        return self.client.create_webhook_callback(json=callback_payload)

    # ******************* PROJECT METHODS *******************************
    def list_project_roles(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        return self.client.get_project_security_roles_list(
            filters=[fields_filter]
        ).json()

    def get_project_statuses(self):
        required_fields = ["id", "name"]
        fields_filter = CWFieldsFilter(required_fields)
        return self.client.get_project_statuses(filters=[fields_filter])

    def list_projects(
        self,
        customers: Optional[List[int]] = None,
        boards: Optional[List[int]] = None,
        statuses: Optional[List[int]] = None,
        fields: Optional[List[str]] = None,
    ) -> Generator[Response, None, None]:
        """
        List projects. The projects can be filtered based on customers, boards and statuses.
        If no filter is supplied, it returns all projects.
        This function returns a generator to the result which can be iterated to
        get all the pages of the result.
        Args:
            statuses: List of status id's
            customers: List of customer id's
            boards: List of board id's
        """

        filters = []
        if customers:
            company_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "customerCompany/id", Operators.EQUALS, customers
                    )
                ]
            )
            filters.append(company_filter)

        if statuses:
            status_filter = CWConditions(
                int_conditions=[
                    Condition("status/id", Operators.IN, tuple(statuses))
                ]
            )
            filters.append(status_filter)

        if boards:
            boards_filter = CWConditions(
                int_conditions=[
                    Condition("board/id", Operators.IN, tuple(boards))
                ]
            )
            filters.append(boards_filter)
        if fields:
            fields_filter = CWFieldsFilter(fields)
            filters.append(fields_filter)
        projects = self.client.list_projects(filters=filters)
        return projects

    def get_project_team_members(self, project_id: int) -> List[Dict]:
        """
        Fetch team members list for the Project.
        Args:
            project_id: Project CRM id
        """

        return self.client.get_project_team_members(project_id)

    def get_project_customer_contacts(self, project_id: int) -> List[Dict]:
        """
        Fetch team members list for the Project.
        Args:
            project_id: Project CRM id
        """

        return self.client.get_project_customer_contacts(project_id)

    def create_project_customer_contact(
        self, project_id: int, contact_crm_id: int
    ):
        """
        Creates a contact in the specified project.
        Args:
            project_id: Project CRM id
            contact_crm_id: Contact CRM id
        """
        contact_data = {
            "projectId": project_id,
            "contact": {"id": contact_crm_id},
        }

        return self.client.create_project_customer_contact(
            project_id, json=contact_data
        )

    def delete_project_customer_contact(
        self, project_id: int, contact_record_id: int
    ):
        """
        Deletes project's contact with contact_record_id
        Args:
            project_id: Project CRM id
            contact_record_id: Contact Record id
        """

        return self.client.delete_project_customer_contact(
            project_id, contact_record_id
        )

    def get_project_tickets(
        self, project_id: int, fields: object = None
    ) -> Generator[Response, None, None]:
        """
        Returns a generator to fetch the tickets for a project

        """
        default_fields = ["id", "summary", "status"]
        fields = fields or default_fields
        fields_filter = CWFieldsFilter(fields)

        project_filter = CWConditions(
            int_conditions=[
                Condition("project/id", Operators.EQUALS, project_id)
            ]
        )
        filters = [fields_filter, project_filter]
        return self.client.get_project_tickets(filters=filters)

    def update_project_ticket_status(self, ticket_id, status_id):
        patch_group = PatchGroup()
        patch_group.add("status", {"id": status_id})
        ticket = self.client.update_project_ticket(
            ticket_id, json=patch_group.to_dict()
        )
        return ticket

    def update_project(self, project_id, **payload):
        updates = {"description": payload.get("description")}
        estimated_end_date = payload.get("estimated_end_date")
        if estimated_end_date:
            estimated_end_date = datetime.strftime(
                estimated_end_date, settings.ISO_FORMAT
            )
            updates.update({"estimatedEnd": estimated_end_date})
        status = payload.get("overall_status_id")
        if status:
            updates.update({"status": {"id": status}})
        patch_group = PatchGroup()
        for key, value in updates.items():
            if value:
                patch_group.add(key, value)
        project = self.client.update_project(
            project_id, json=patch_group.to_dict()
        )
        return project

    def get_work_roles(self):
        return self.client.get_time_work_roles()

    def get_work_types(self):
        return self.client.get_time_work_types()

    def create_time_entry(self, payload):
        member = payload.get("member_crm_id")
        charge_to_id = payload.get("charge_to_id")
        start_time = datetime.strftime(
            payload.get("start_time"), settings.ISO_FORMAT
        )
        end_time = datetime.strftime(
            payload.get("end_time"), settings.ISO_FORMAT
        )
        assert all([member, charge_to_id, start_time, end_time])
        work_role = payload.get("work_role")
        work_type = payload.get("work_type")
        business_unit = payload.get("business_unit")
        notes = payload.get("notes")
        charge_to_type = payload.get("charge_to_type")

        request_body = {
            "member": {"id": member},
            "chargeToId": charge_to_id,
            "chargeToType": charge_to_type or "ProjectTicket",
            "timeStart": start_time,
            "timeEnd": end_time,
        }
        if work_role:
            request_body["workRole"] = {"id": work_role}
        if work_type:
            request_body["workType"] = {"id": work_type}
        if business_unit:
            request_body["businessUnitId"] = business_unit
        if notes:
            request_body["notes"] = notes

        return self.client.create_project_ticket_time_entry(json=request_body)

    def update_time_entry(self, time_entry_id, payload):
        member = payload.get("member_crm_id")
        charge_to_id = payload.get("charge_to_id")
        start_time = datetime.strftime(
            payload.get("start_time"), settings.ISO_FORMAT
        )
        end_time = datetime.strftime(
            payload.get("end_time"), settings.ISO_FORMAT
        )
        assert all([member, charge_to_id, start_time, end_time])
        work_type = payload.get("work_type")
        business_unit = payload.get("business_unit")
        notes = payload.get("notes")
        charge_to_type = payload.get("charge_to_type")

        request_body = {
            "member": {"id": member},
            "chargeToId": charge_to_id,
            "chargeToType": charge_to_type or "ProjectTicket",
            "timeStart": start_time,
            "timeEnd": end_time,
            "billableOption": payload.get("billing_option"),
        }
        if payload.get("work_role_id"):
            request_body["workRole"] = {"id": payload.get("work_role_id")}
        if work_type:
            request_body["workType"] = {"id": work_type}
        if business_unit:
            request_body["businessUnitId"] = business_unit
        if notes:
            request_body["notes"] = notes

        return self.client.update_project_ticket_time_entry(
            time_entry_id=time_entry_id, json=request_body
        )

    def delete_time_entry(self, time_entry_id):
        return self.client.delete_project_ticket_time_entry(time_entry_id)

    def get_timesheet_periods(
        self, member_id: int = None, status: Optional[List[str]] = None
    ):
        """
        Returns the list of timesheet periods
        """
        default_fields = ["id", "dateStart", "dateEnd", "status", "hours"]
        fields_filter = CWFieldsFilter(default_fields)
        order_by = CWOrderBy("dateStart", Order.DESC)
        filters = [fields_filter, order_by]
        if member_id:
            members_filter = CWConditions(
                int_conditions=[
                    Condition("member/id", Operators.EQUALS, member_id)
                ]
            )
            filters.append(members_filter)
        if status:
            for _status in status:
                status_filter = CWORConditions(
                    string_conditions=[
                        Condition("status", Operators.EQUALS, _status)
                    ]
                )
                filters.append(status_filter)
        return self.client.get_timesheet_periods(filters, all=True)

    def submit_time_sheet(self, time_sheet_id: int):
        """
        Submits a time sheet for approval

        """
        return self.client.submit_time_sheet(time_sheet_id)

    def get_time_entries(
        self,
        member_id: int = None,
        time_sheet_ids: Optional[List[int]] = None,
        project_crm_id: int = None,
    ):
        """
        Get the list of time entries. Supports filter on member id and time sheet ids.
        """
        filters = []
        if member_id:
            members_filter = CWConditions(
                int_conditions=[
                    Condition("member/id", Operators.EQUALS, member_id)
                ]
            )
            filters.append(members_filter)
        if project_crm_id:
            project_filter = CWConditions(
                int_conditions=[
                    Condition("project/id", Operators.EQUALS, project_crm_id)
                ]
            )
            filters.append(project_filter)
        if time_sheet_ids:
            time_sheet_filter = CWConditions(
                int_conditions=[
                    Condition(
                        "timeSheet/id", Operators.IN, tuple(time_sheet_ids)
                    )
                ]
            )
            filters.append(time_sheet_filter)
        return self.client.get_time_entries(filters=filters)

    def get_charge_codes(self, time_entry_flag=True):
        """
        Get the list of connectwise charge codes.
        https://developer.connectwise.com/Products/Manage/REST#/ChargeCodes

        :param time_entry_flag: filter based on timeEntryFlag
        """
        filters = []
        if time_entry_flag:
            time_entry_filter = CWConditions(
                boolean_conditions=[
                    Condition(
                        "timeEntryFlag", Operators.EQUALS, time_entry_flag
                    )
                ]
            )
            filters.append(time_entry_filter)
        return self.client.get_charge_codes(filters=filters, all=True)

    def get_ticket_purchase_orders(self, product_ids=None):
        filters = []
        if product_ids:
            product_ids_filter = CWConditions(
                int_conditions=[
                    Condition("id", Operators.IN, tuple(product_ids))
                ]
            )
            filters.append(product_ids_filter)
        return self.client.list_products(filters, all=True)

    def get_custom_fields(self, **kwargs):
        filters = []
        required_fields = [
            "id",
            "caption",
            "dateCreated",
            "_info/lastUpdated",
        ]
        if required_fields:
            fields_filter = CWFieldsFilter(required_fields)
            filters.append(fields_filter)
        return self.client.get_custom_fields(filters, **kwargs)

    def get_billing_statuses_from_cw(self, filters=None, **kwargs):
        filters = filters or []
        if not filters:
            required_fields = [
                "id",
                "name",
            ]
            if required_fields:
                fields_filter = CWFieldsFilter(required_fields)
                filters.append(fields_filter)
        return self.client.get_billing_statuses(filters, **kwargs)

    def get_invoices(
        self,
        approaching_due_date_ageing: Optional[int],
        invoice_status_ids: Optional[List[int]] = None,
        customer_crm_ids: Optional[List[int]] = None,
        filters=None,
        **kwargs,
    ) -> List[Dict]:
        """
        Fetch invoices from Connectwise.

        Args:
            approaching_due_date_ageing: Denotes the number of days in future to look for due invoices
            invoice_status_ids: Optional list of invoice status IDs
            customer_crm_ids: Optional list of customer CW IDs
            filters: Filters
            **kwargs: Keyword arguments

        Returns:
            CW invoices data as per filters
        """
        filters = filters or []
        if not filters:
            required_fields = [
                "id",
                "invoiceNumber",
                "status/id",
                "status/name",
                "company/id",
                "company/name",
                "customerPO",
                "billingTerms/name",
                "dueDate",
                "balance",
            ]
            required_fields_filter = CWFieldsFilter(required_fields)
            filters.append(required_fields_filter)

            balance_filter = CWConditions(
                int_conditions=[
                    Condition("balance", Operators.GREATER_THEN, 0)
                ]
            )
            filters.append(balance_filter)

            if invoice_status_ids:
                invoice_status_filter = CWConditions(
                    int_conditions=[
                        Condition(
                            "status/id",
                            Operators.IN,
                            tuple(invoice_status_ids),
                        )
                    ]
                )
                filters.append(invoice_status_filter)

            current_datetime: datetime = kwargs.get("current_datetime", None)
            if not current_datetime:
                current_datetime: datetime = timezone.now()
            approaching_due_date_threshold: datetime = (
                current_datetime + timedelta(days=approaching_due_date_ageing)
            )
            due_date_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "dueDate",
                        Operators.LESS_THEN_EQUALS,
                        datetime.strftime(
                            approaching_due_date_threshold,
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                    )
                ]
            )
            filters.append(due_date_filter)

            if customer_crm_ids:
                customer_filter = CWConditions(
                    int_conditions=[
                        Condition(
                            "company/id",
                            Operators.IN,
                            tuple(customer_crm_ids),
                        )
                    ]
                )
                filters.append(customer_filter)
        return self.client.get_billing_invoices(filters, **kwargs)

    def get_invoices_by_crm_ids(
        self, invoice_crm_ids: Optional[List[int]] = None, filters=None
    ):
        filters = filters or []
        required_fields = [
            "id",
            "invoiceNumber",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        if invoice_crm_ids:
            invoice_crm_ids_filter = CWConditions(
                int_conditions=[
                    Condition("id", Operators.IN, tuple(invoice_crm_ids)),
                ]
            )
            filters.append(invoice_crm_ids_filter)
        return self.get_invoices(
            approaching_due_date_ageing=None, filters=filters
        )

    def get_invoices_with_generator(
        self, created_date: Optional[date] = None, filters=None
    ) -> Generator[Response, None, None]:
        filters = filters or []
        required_fields = [
            "id",
            "invoiceNumber",
            "status/name",
            "company/id",
            "company/name",
            "customerPO",
            "dueDate",
            "balance",
            "subtotal",
            "total",
            "date",
            "reference",
            "addToBatchEmailList",
            "salesOrder/id",
            "phase/name",
            "ticket/id",
            "billingTerms/name",
            "credits",
            "billToCompany/name",
            "billingSite/name",
            "taxCode/name",
            "serviceTotal",
            "expenseTotal",
            "productTotal",
            "salesTax",
            "internalNotes",
            "territoryId",
            "type",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        if created_date:
            created_date_filter = CWConditions(
                datetime_conditions=[
                    Condition(
                        "date",
                        Operators.GREATER_THEN_EQUALS,
                        datetime.strftime(
                            created_date,
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                    )
                ]
            )
            filters.append(created_date_filter)
        # if invoice_crm_ids:
        #     invoice_crm_ids_filter = CWConditions(
        #         int_conditions=[
        #             Condition("id", Operators.IN, tuple(invoice_crm_ids)),
        #         ]
        #     )
        #     filters.append(invoice_crm_ids_filter)

        return self.client.get_billing_invoices_using_generator(
            filters=filters
        )

    def get_invoice(
        self,
        invoice_id: int,
        filters=None,
        **kwargs,
    ) -> Dict:
        """
        Fetch invoice by id from Connectwise.

        Args:
            invoice_id: invoice Id
            filters: Filters
            **kwargs: Keyword arguments

        Returns:
            CW invoice data as per filters
        """
        filters = filters or []
        if not filters:
            required_fields = [
                "id",
                "invoiceNumber",
                "status/name",
                "company/id",
                "company/name",
                "customerPO",
                "dueDate",
                "balance",
                "total",
                "date",
                "reference",
                "addToBatchEmailList",
                "salesOrder/id",
                "phase/name",
                "ticket/id",
                "billingTerms/name",
                "credits",
                "billToCompany/name",
                "billingSite/name",
                "taxCode/name",
                "serviceTotal",
                "expenseTotal",
                "productTotal",
                "salesTax",
                "internalNotes",
                "territoryId",
                "type",
            ]
            required_fields_filter = CWFieldsFilter(required_fields)
            filters.append(required_fields_filter)
        return self.client.get_invoice_by_id(invoice_id, filters, **kwargs)

    def get_invoice_payment_details(
        self,
        invoice_id: int,
        filters=None,
        **kwargs,
    ) -> Dict:
        """
        Fetch invoice by id from Connectwise.

        Args:
            invoice_id: invoice Id
            filters: Filters
            **kwargs: Keyword arguments

        Returns:
            CW invoice data as per filters
        """
        filters = filters or []
        if not filters:
            required_fields = [
                "id",
                "invoice/id",
                "invoice/identifier",
                "paymentDate",
            ]
            required_fields_filter = CWFieldsFilter(required_fields)
            filters.append(required_fields_filter)
        return self.client.get_invoice_payment_details(
            invoice_id, filters, **kwargs
        )

    def line_items_status(self):
        return line_items_status

    def get_team_roles(self) -> List[Dict]:
        """
        Get team roles.
        """
        filters: List = list()
        required_fields: List[str] = [
            "id",
            "name",
            "accountManagerFlag",
            "techFlag",
            "salesFlag",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        return self.client.get_company_team_roles(filters)

    def create_team_role(self, create_payload: Dict):
        """
        Create team role.
        """
        role_creation_payload: Dict[str, Any] = dict(
            name=create_payload.get("role_name")
        )
        return self.client.create_company_team_role(json=role_creation_payload)

    def add_new_member_to_company_team(
        self, customer_crm_id: int, member_data: Dict
    ):
        """
        Adds new member to customer team.
        """
        payload: Dict = dict(
            teamRole=dict(id=member_data.get("role_crm_id")),
            member=dict(id=member_data.get("system_member_crm_id")),
        )
        updated_team: Dict = self.client.add_member_to_company_team(
            customer_crm_id, json=payload
        )
        return updated_team

    def remove_member_from_company_team(
        self, customer_crm_id: int, team_member_crm_id: int
    ):
        """
        Removes existing member from customer team.
        """
        return self.client.delete_company_team_member(
            customer_crm_id, team_member_crm_id
        )

    def get_customer_team_members(self, customer_crm_id: int) -> List[Dict]:
        """
        Get cutomer team members.
        """
        filters: List = list()
        required_fields: List[str] = [
            "id",
            "company/id",
            "company/name",
            "teamRole/id",
            "teamRole/name",
            "member/id",
            "member/name",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        team_members: List[Dict] = self.client.get_company_team_members(
            customer_crm_id, filters=filters, all=True
        )
        return team_members

    def get_product_catalogs(self, catalog_crm_ids: List[int]) -> List[Dict]:
        """
        Get product catalogs.
        """
        filters: List = list()
        required_fields: List[str] = [
            "id",
            "identifier",
            "manufacturer",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)

        catalog_ids_filter = CWConditions(
            int_conditions=[
                Condition("id", Operators.IN, tuple(catalog_crm_ids))
            ]
        )
        filters.append(catalog_ids_filter)

        catalogs: List[Dict] = self.client.get_product_catalog_details(
            filters=filters, all=True
        )
        return catalogs

    def get_product_catalogs_using_model_number(
        self, model_numbers: List[str]
    ) -> List[Dict]:
        """
        Get product catalog for using model number filter.

        Args:
            model_numbers: Model numbers

        Returns:
            Product catalog details.
        """
        filters: List = list()
        for model_number in model_numbers:
            model_number_filter = CWORConditions(
                string_conditions=[
                    Condition(
                        "manufacturerPartNumber",
                        Operators.EQUALS,
                        model_number,
                    )
                ]
            )
            filters.append(model_number_filter)
        required_fields: List[str] = [
            "id",
            "identifier",
            "manufacturer/id",
            "manufacturer/name",
            "description",
            "manufacturerPartNumber",
        ]
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        catalogs: List[Dict] = self.client.get_product_catalog_details(
            filters=filters, all=True
        )
        return catalogs


class CWConditions(object):
    """
    Class used to create different type of conditions(filters) for Connectwise API
    Operators is the dict of various supported operators by CW. To use an operator
    dot notation can be used.
    Example: for "=" use Operator.EQUALS
    Every condition should be of type Condition which can be created as
    Condition("field_name", Operator.EQUALS, "value")
    This class accept four different types of conditions based on the value of the condition.
    If the value of condition is of String type, it should be included in string_conditions_list
    If the value of condition is of Integer type, it should be included in integer_conditions_list.
    """

    __slots__ = [
        "string_conditions",
        "datetime_conditions",
        "int_conditions",
        "boolean_conditions",
    ]

    def __init__(
        self,
        string_conditions=None,
        datetime_conditions=None,
        int_conditions=None,
        boolean_conditions=None,
    ):
        self.string_conditions = string_conditions or []
        self.datetime_conditions = datetime_conditions or []
        self.int_conditions = int_conditions or []
        self.boolean_conditions = boolean_conditions or []

    def _get_conditions(self):
        """
        Creates condition strings for different types of conditions
        :return:
        """
        conditions = []
        string_conditions = [
            f'{condition.field}{condition.operator}"{condition.value}"'
            for condition in self.string_conditions
        ]
        conditions.extend(string_conditions)
        datetime_conditions = [
            f"{condition.field}{condition.operator}[{condition.value}]"
            for condition in self.datetime_conditions
        ]
        conditions.extend(datetime_conditions)
        int_conditions = [
            f"{condition.field}{condition.operator}{condition.value}"
            for condition in self.int_conditions
        ]
        conditions.extend(int_conditions)
        boolean_conditions = [
            f"{condition.field}{condition.operator}{condition.value}"
            for condition in self.boolean_conditions
        ]
        conditions.extend(boolean_conditions)
        return conditions

    def __str__(self):
        conditions = self._get_conditions()
        return " and ".join(conditions)

    def extend(self, other):
        self.string_conditions.extend(other.string_conditions)
        self.datetime_conditions.extend(other.datetime_conditions)
        self.boolean_conditions.extend(other.boolean_conditions)
        self.int_conditions.extend(other.int_conditions)

    def to_dict(self):
        """
        This method returns a dict of the form {"conditions":"All conditions
        combined with and operator in them"}
        :return: dict
        """

        return dict(conditions=str(self))


class CWORConditions(CWConditions):
    def __str__(self):
        conditions = self._get_conditions()
        return " or ".join(conditions)


class CWChildConditions(CWConditions):
    """
    Class for adding child conditions
    """

    __slots__ = []

    def to_dict(self):
        return dict(childConditions=str(self))


class CWOrderBy(object):
    """
    Class for adding ordering conditions to CW API
    """

    __slots__ = ["field_name", "order", "order_by"]

    def __init__(self, field_name, order):
        self.field_name = field_name
        self.order = order
        self.order_by = dict(orderBy=str(self))

    def __str__(self):
        return f"{self.field_name} {self.order}"

    def extend(self, other):
        self.order_by["orderBy"] = f"{self.order_by['orderBy']}, {str(other)}"

    def to_dict(self):
        """
        returns dict of the form {"orderBy":"field_name order"}
        :return:
        """
        return self.order_by


class CWFieldsFilter(object):
    """
    Class used to apply fields parameter to CW api's
    """

    __slots__ = ["fields"]

    def __init__(self, fields):
        self.fields = fields

    def __str__(self):
        return ",".join(self.fields)

    def extend(self, other):
        self.fields.append(other.fields)

    def to_dict(self):
        """
        returns dict of the form {"fields": "field_one,field_two"}
        :return:
        """
        return dict(fields=str(self))


class PatchGroup(object):
    __slots__ = ["patches"]

    def __init__(self):
        self.patches = []

    def __repr__(self):
        string = ""
        for k, v in self.__dict__.items():
            string = "".join([string, "{}: {}\n".format(k, v)])
        return string

    def add(self, path, value, operation="replace"):
        patch_op = {"op": operation, "path": path}

        if operation == "replace":
            patch_op["value"] = value

        self.patches.append(patch_op)

    def to_dict(self):
        return self.patches


class BatchRequestItem(object):
    """
    Helper class to prepare CW batch request item of the format:
    {
          "id":"Get_Contact_6",
          "method":"GET",
          "relativeUrl":"/company/contacts/6/",
          "body": None
    }
    """

    __slots__ = ["id", "method", "relative_url", "body"]

    def __init__(self, method, relative_url, body=None, request_id=None):
        """

        :param method: RequestMethods instance.
        :param relative_url: Relative url to the CW endpoint.
        :param body: body of the request.
        :param request_id: Id for request.
        """
        self.id = request_id or generate_random_id(10)
        self.method = method
        self.relative_url = relative_url
        self.body = body

    def to_dict(self):
        return dict(
            id=self.id,
            method=self.method,
            relativeUrl=self.relative_url,
            body=self.body,
        )

    def __str__(self):
        return self.id


class BatchRequest(object):
    """
     Helper class to create CW Batch request of the format:
     {
    "id":"Sample string",
    "requests":[
       {
          "id":"Get_Contact_5",
          "method":"GET",
          "relativeUrl":"/company/contacts/5/"
       },
       {
           "id":"Get_Contact_6",
           "method":"GET",
           "relativeUrl":"/company/contacts/6/"
       },
       {
           "id":"Patch_Contact_Communication",
         "method": "PATCH",
         "relativeUrl" : "company/contacts/{result=Get_Contact_5:$.id}",
         "body" : [{"op":"replace", "path":"value", "firstName": "{result=Get_Contact_6:$.firstName}"}]
       }
    ]
     }
    """

    __slots__ = ["request_id", "batch_requests_items"]

    def __init__(self, batch_request_items, request_id=None):
        """

        :param batch_request_items: list of BatchRequest class objects.
        :param request_id: Id for request.
        """
        self.request_id = request_id or generate_random_id(10)
        self.batch_requests_items = batch_request_items

    def to_dict(self):
        data = dict(
            id=self.request_id,
            requests=[
                request.to_dict() for request in self.batch_requests_items
            ],
        )
        return data

    def __str__(self):
        return self.request_id


class ConnectwiseEntity(Enum):
    PROJECT = "project"
