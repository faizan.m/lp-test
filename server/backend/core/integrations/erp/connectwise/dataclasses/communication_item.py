from dataclasses import dataclass


@dataclass
class ConnectwiseCommunicationItem:
    id: int
    type_id: int
    value: str
    communication_type: str

    @classmethod
    def from_json(cls, _json):
        """
         {
            "id": 21269,
            "type": {
                "id": 6,
                "name": "Home",
                "_info": {
                    "type_href": "https://cw.lookingpoint.com/v4_6_release/apis/3.0/company/communicationTypes/6"
                }
            },
            "value": "800642255",
            "defaultFlag": false,
            "communicationType": "Phone"
        }
        """
        return cls(
            _json.get("id"),
            _json.get("type", {}).get("id"),
            _json.get("value"),
            _json.get("communicationType"),
        )
