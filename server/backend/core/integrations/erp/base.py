"""
This module contains Base class for ERP
"""
from abc import ABC, abstractmethod


class ERPBase(ABC):
    """
    This is the Base class for all ERP Integrations.
    All ERP Integrations should use this as the Base class
    and write the implementation all the abstract methods.
    """

    def __init__(self, auth_configs, field_mappings, extra_configs):
        self.auth_configs = auth_configs
        self.field_mappings = field_mappings
        self.extra_configs = extra_configs
        super(ERPBase, self).__init__()

    @abstractmethod
    def authenticate(self):
        """
        This method should contain logic to authenticate to the ERP
        system using the provided auth_configs
        :return: Token
        """
        pass

    @abstractmethod
    def validate_api_credentials(self):
        """
        This method should verify the provided credentials valid
        If valid it should return True else False
        :return: boolean
        """
        pass

    @abstractmethod
    def get_device_categories(self, filters=None):
        """
        This method should return the list of available device categories
        :return: List of Device Categories
        """
        pass

    @abstractmethod
    def get_other_configs(self, extra_config):
        """
        Should return any other configurations
        :return:
        """
        pass

    #
    # @abstractmethod
    # def get_dashboard_metrics(self, company_id, filters=None):
    #     """
    #     Should return Acela Dashboard Metrics
    #     :return:
    #     """
    #     pass

    @abstractmethod
    def get_devices(
        self,
        company_id=None,
        category_id=None,
        filters=None,
        required_fields=None,
    ):
        """
        should return list of devices
        :param required_fields:
        :param category_id:
        :param company_id:
        :param filters:
        :return:
        """
        pass

    @abstractmethod
    def get_devices_count(self, company_id, category_id=None, filters=None):
        """
        should return the count of devices
        :param category_id:
        :param company_id:
        :param filters:
        :return:
        """
        pass

    @abstractmethod
    def add_device(self, company_id, category_id, data):
        """
        should Create a new device
        :param category_id:
        :param company_id:
        :param data:
        :return:
        """
        pass

    @abstractmethod
    def update_device(self, device_id, data):
        """
        Should Update device data
          :param data: device data
          :param device_id: CW id of the device to be updated.
          :return:
        """
        pass

    @abstractmethod
    def batch_update_devices(self, data):
        """
        Update Devices in Bulk
        :param company_id:
        :param data:
        :return:
        """
        pass

    @abstractmethod
    def batch_add_devices(self, customer_id, category_id, data):
        """
        Add devices in batch
        :param category_id:
        :param customer_id:
        :param data:
        :return:
        """
        pass

    @abstractmethod
    def get_customers(self, full_info=False, filters=None):
        """
        Should return list of customers
        :param filters:
        :param full_info: if True return all data else return minimal data
        :return:
        """
        pass

    @abstractmethod
    def get_customer_users(self, customer_id, filters=None):
        """
        should return the list of customer users
        :param filters:
        :param customer_id:
        :return:
        """
        pass

    @abstractmethod
    def get_customer_users_count(self, customer_id, filters=None):
        """
        should return the count of customer users
        :param type_id:
        :param filters:
        :param customer_id:
        :return:
        """
        pass

    #
    # @abstractmethod
    # def create_customer_user(self, customer_id):
    #     """
    #     should create a new customer user
    #     :param customer_id:
    #     :return:
    #     """
    #     pass
    #
    # @abstractmethod
    # def update_customer_user(self, customer_id, user_id, data):
    #     """
    #     should update an existing customer
    #     :param customer_id:
    #     :param user_id:
    #     :return:
    #     """
    #     pass
    #
    # @abstractmethod
    # def get_service_requests(self, customer_id, filters=None):
    #     """
    #     should return list of service requests
    #     :param filters:
    #     :param customer_id:
    #     :return:
    #     """
    #     pass
    #
    # @abstractmethod
    # def get_service_request(self, customer_id, ticket_id, filters=None):
    #     """
    #     should return details of service request
    #     :param customer_id:
    #     :param ticket_id:
    #     :return:
    #     """
    #     pass
    #
    # @abstractmethod
    # def create_service_request(self, customer_id, data):
    #     """
    #     Should create a new service request
    #     :param customer_id:
    #     :param data:
    #     :return:
    #     """
    #     pass
    #
    # @abstractmethod
    # def update_service_request(self, customer_id, ticket_id, data):
    #     """
    #     Update service request
    #     :param customer_id:
    #     :param ticket_id:
    #     :param data:
    #     :return:
    #     """
    #     pass
    #
    @abstractmethod
    def get_customer_sites(self, customer_id, filters=None):
        """

        :param filters:
        :param customer_id:
        :return:
        """
        pass

    @abstractmethod
    def create_customer_site(self, customer_id, data):
        """

        :param customer_id:
        :param data:
        :return:
        """
        pass
