from typing import Dict, List

import requests

from core.integrations.order_processing.ingram.client import IngramAPIClient
from exceptions import exceptions
from exceptions.exceptions import (
    AuthenticationError,
    AuthorizationError,
    InvalidRequestError,
)
from utils import get_app_logger
from core.integrations.order_processing.ingram.ingram_service import (
    IngramOrder,
)
from dataclasses import asdict

# Get an instance of a logger
logger = get_app_logger(__name__)


class Ingram:
    def __init__(self, client_id, client_secret, customer_number):
        self.client_id = client_id
        self.client_secret = client_secret
        self.customer_number = customer_number
        self.client = self._get_client()

    def _get_client(self):
        try:
            return IngramAPIClient(
                self.client_id, self.client_secret, self.customer_number
            )
        except AuthenticationError:
            return None
        except (
            requests.exceptions.ConnectionError,
            TimeoutError,
            requests.exceptions.Timeout,
            requests.exceptions.ConnectTimeout,
            requests.exceptions.ReadTimeout,
        ) as e:
            logger.debug(e)
            return None

    def search_order(self, po_number):
        return self.client.search_order(po_number)

    def get_order_details(self, order_number: str) -> Dict:
        try:
            return self.client.get_order_details(order_number)
        except exceptions.RateLimitExceededError:
            return self.client.get_order_details(order_number)
        except InvalidRequestError:
            return {}

    def get_all_orders_by_po_number(self, po_number: str) -> List:
        order_search_result = self.search_order(po_number)
        # orders = (
        #     order_search_result.get("serviceresponse", {})
        #     .get("ordesearchresponse", {})
        #     .get("orders", [])
        # )
        if order_search_result:
            orders = order_search_result.get("orders", [])
            order_details = []
            try:
                for order in orders:
                    try:
                        ingram_order_number = order.get("ingramOrderNumber")
                        order_detail = self.get_order_details(
                            ingram_order_number
                        )
                        if order_detail:
                            order_detail = IngramOrder.from_dict(order_detail)
                            order_details.append(asdict(order_detail))
                    except Exception as e:
                        logger.error(e)
                        if e.status_code in [404, 500, 503, 429]:
                            continue
                return order_details
            except Exception as e:
                logger.error(e)
                return []
        return []


def verify_credentials(
    client_id: str, client_secret: str, customer_number: str
) -> bool:
    ingram_client = Ingram(client_id, client_secret, customer_number)
    if not ingram_client.client:
        return False
    po_number = "LP11450-PAR"
    try:
        ingram_client.search_order(po_number)
    except (AuthenticationError, AuthorizationError):
        return False
    return True
