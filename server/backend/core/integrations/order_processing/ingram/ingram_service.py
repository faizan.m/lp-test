from dataclasses import asdict, dataclass
from typing import Dict, List
from datetime import datetime
from utils import get_app_logger

logger = get_app_logger(__name__)


@dataclass
class ShipmentDetails:
    shipmentdate: str
    carriername: str
    packagedetails: List

    @classmethod
    def from_dict(cls, data: dict) -> "ShipmentDetails":
        tracking_numbers = []
        packagedetails = {}
        carrier_details = []
        try:
            if data.get("carrierDetails") and isinstance(
                data.get("carrierDetails"), dict
            ):
                for package in data.get("carrierDetails", {}).get(
                    "trackingDetails", []
                ):
                    if package.get("trackingNumber"):
                        packagedetails["trackingnumber"] = package.get(
                            "trackingNumber", []
                        )
                    if package.get("SerialNumbers"):
                        packagedetails["serialnumberdetails"] = package.get(
                            "SerialNumbers", []
                        )
                    elif isinstance(package, list):
                        for tracking_number in package:
                            if tracking_number.get("trackingNumber"):
                                tracking_numbers.append(
                                    tracking_number.get("trackingNumber")
                                )
                        if tracking_numbers:
                            packagedetails["trackingnumber"] = tracking_numbers
                    carrier_details.append(packagedetails)
                return cls(
                    shipmentdate=data.get("shippedDate"),
                    carriername=data.get("carrierDetails").get("carrierName"),
                    packagedetails=carrier_details,
                )
            elif data.get("carrierDetails") and isinstance(
                data.get("carrierDetails"), list
            ):
                for carrier_detail in data.get("carrierDetails"):
                    for package in carrier_detail.get("trackingDetails", []):
                        if package.get("trackingNumber"):
                            packagedetails["trackingnumber"] = package.get(
                                "trackingNumber", []
                            )
                        if package.get("SerialNumbers"):
                            packagedetails[
                                "serialnumberdetails"
                            ] = package.get("SerialNumbers", [])
                        elif isinstance(package, list):
                            for tracking_number in package:
                                if tracking_number.get("trackingNumber"):
                                    tracking_numbers.append(
                                        tracking_number.get("trackingNumber")
                                    )
                            if tracking_numbers:
                                packagedetails[
                                    "trackingnumber"
                                ] = tracking_numbers
                        carrier_details.append(packagedetails)
                    return cls(
                        shipmentdate=data.get("shippedDate"),
                        carriername=carrier_detail.get("carrierName"),
                        packagedetails=carrier_details,
                    )
            else:
                return cls(
                    shipmentdate=data.get("shippedDate", ""),
                    carriername="",
                    packagedetails=[],
                )
        except Exception as e:
            logger.error(e)


@dataclass
class LineOrder:
    manufacturerpartnumber: str
    confirmedquantity: int
    shipmentdetails: List
    promiseddeliverydate: datetime
    subordernumber: str
    orderedquantity: int
    orderlinenumber: str
    orderstatus: str

    @classmethod
    def from_line_dict(cls, data: dict) -> "LineOrder":
        shipment_details = []
        for shipment_detail in data.get("shipmentDetails"):
            _shipment_detail = ShipmentDetails.from_dict(shipment_detail)
            shipment_details.append(asdict(_shipment_detail))
        return cls(
            manufacturerpartnumber=data.get("vendorPartNumber"),
            confirmedquantity=data.get("quantityConfirmed"),
            promiseddeliverydate=data.get("promisedDeliveryDate"),
            subordernumber=data.get("subOrderNumber"),
            orderlinenumber=data.get("ingramOrderLineNumber"),
            orderstatus=data.get("lineStatus"),
            orderedquantity=data.get("quantityOrdered"),
            shipmentdetails=shipment_details,
        )


@dataclass
class IngramOrder:
    shiptoaddress: Dict
    customerordernumber: str
    ingramordernnumber: str
    lines: List

    @classmethod
    def from_dict(cls, data: dict) -> "IngramOrder":
        _lines = []
        for line in data.get("lines"):
            ingram_line_item = LineOrder.from_line_dict(line)
            _lines.append(asdict(ingram_line_item))
        return cls(
            shiptoaddress=data.get("shipToInfo"),
            customerordernumber=data.get("customerOrderNumber"),
            ingramordernnumber=data.get("ingramOrderNumber"),
            lines=_lines,
        )
