# Client to interact with Ingram EOX API.
import datetime

import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from exceptions.exceptions import AuthenticationError, build_error
from utils import get_app_logger
import uuid

# Get an instance of a logger
logger = get_app_logger(__name__)


class IngramAPIClient(object):
    """
    Performs requests to the Ingram API's.
    """

    BASE_URL = "https://api.ingrammicro.com:443/resellers/v6"
    ORDERS_BASE_URL = f"{BASE_URL}/orders"
    ORDERS_SEARCH_URL = f"{ORDERS_BASE_URL}/search"

    def __init__(self, client_id, client_secret, customer_number):
        """
        :param client_id: Ingram API application client Id
        :param client_secret: Ingram API application client Secret
        """

        self.client_id = client_id
        self.client_secret = client_secret
        self.customer_number = customer_number

        if not (self.client_id and self.client_secret):
            raise ValueError("Must provide client_id and client_secret.")
        self.auth_token = None
        self.session = None
        self.token_expiry = None
        self.create_session()

    def create_session(self):
        self.auth_token, expiry = self._generate_auth_token(
            self.client_id, self.client_secret
        )
        self.token_expiry = datetime.datetime.now() + datetime.timedelta(
            seconds=expiry
        )
        self.session = self._build_session(self.auth_token)

    @staticmethod
    def _generate_auth_token(client_id, client_secret):
        url = "https://api.ingrammicro.com:443/oauth/oauth20/token"
        payload = f"grant_type=client_credentials&client_id={client_id}&client_secret={client_secret}"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}

        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == 500:
            if (
                response.json()
                .get("fault", {})
                .get("detail", {})
                .get("errorcode")
                == "oauth.v2.InvalidClientIdentifier"
            ):
                raise AuthenticationError(source="Ingram")
        #  Response format
        # {"access_token": "BTHN3cAzbRUitXVI1sBPBGVSn9ol", "token_type": "Bearer", "expires_in": 3599}
        token = response.json().get("access_token")
        expiry = int(response.json().get("expires_in"))
        return token, expiry

    def _build_session(self, auth_token):
        """
        Build session to make request to CW api's
        :param auth_token: authentication token for CW Developer account
        :type auth_token: string
        :return: session object
        """
        _default_headers = {
            "Authorization": "Bearer %s" % auth_token,
            "Content-Type": "application/json",
            "IM-CustomerNumber": self.customer_number,
            "IM-CountryCode": "US",
            "IM-CorrelationID": str(uuid.uuid4()),
        }
        self.requests_kwargs = {}
        self.requests_kwargs.update(
            {
                "headers": _default_headers,
                "verify": False,  # NOTE(cbro): verify SSL certs.
                "timeout": 30,
            }
        )

        session = requests.Session()
        retry = Retry(total=3, backoff_factor=0.1)

        adapter = HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        session.headers = _default_headers
        return session

    def _request(
        self, url, method=None, headers=None, data=None, *args, **kwargs
    ):
        headers = headers or {}
        if self.token_expiry <= datetime.datetime.now():
            self.create_session()
        if method == "POST":
            self.session.headers.update(headers)
            response = self.session.post(url, data=data)
            return self._handle_response(response)
        response = self.session.get(url, *args, **kwargs)
        logger.debug("Ingram client request url: {0}".format(response.url))
        return self._handle_response(response)

    def _handle_response(self, response):
        """Internal helper for handling API responses from the Ingram server.

        Raises the appropriate exceptions when necessary; otherwise, returns the
        response.
        """
        if (
            not str(response.status_code).startswith("2")
            and str(response.status_code) != "429"
        ):
            raise self._build_api_error(response)
        return response

    def _build_api_error(self, response, message=None, error_list=None):
        """
        Helper method for creating errors and attaching HTTP response/request
        details to them.
        """
        if response.status_code == 400:
            response_payload = response.json()
            if (
                response_payload.get("serviceresponse", {})
                .get("responsepreamble", {})
                .get("returnmessage", "")
                == "No data found"
            ):
                response.status_code = 404
        return build_error(
            response.status_code,
            source="Ingram",
            message=message,
            errors=error_list,
        )

    def search_order(self, po_number):
        """

                Success Response
                {
            "serviceresponse": {
                "responsepreamble": {
                    "requeststatus": "SUCCESS",
                    "returnmessage": "Data found"
                },
                "ordesearchresponse": {
                    "ordersfound": "2",
                    "pagesize": "2",
                    "pagenumber": "1",
                    "orders": [
                        {
                            "ordernumber": "50-15324",
                            "entrytimestamp": "2020-11-12T18:39:47Z",
                            "customerordernumber": "LP11464-PAL",
                            "suborders": [
                                {
                                    "subordernumber": "50-15324-11",
                                    "statuscode": "I",
                                    "status": "INVOICED",
                                    "links": [
                                        {
                                            "topic": "orders",
                                            "href": "/resellers/v5/orders/50-15324-11?customerNumber=50-452551&isoCountryCode=US",
                                            "type": "GET"
                                        },
                                        {
                                            "topic": "invoices",
                                            "href": "/resellers/v5/invoices/50-15324-11?customerNumber=50-452551&isoCountryCode=US",
                                            "type": "GET"
                                        }
                                    ]
                                }
                            ],
                            "links": {
                                "topic": "orders",
                                "href": "/resellers/v5/orders/50-15324?customerNumber=50-452551&isoCountryCode=US",
                                "type": "GET"
                            }
                        },
                        {
                            "ordernumber": "50-15318",
                            "entrytimestamp": "2020-11-12T18:36:16Z",
                            "customerordernumber": "LP11464-PAL",
                            "suborders": [
                                {
                                    "subordernumber": "50-15318-11",
                                    "statuscode": "I",
                                    "status": "INVOICED",
                                    "links": [
                                        {
                                            "topic": "orders",
                                            "href": "/resellers/v5/orders/50-15318-11?customerNumber=50-452551&isoCountryCode=US",
                                            "type": "GET"
                                        },
                                        {
                                            "topic": "invoices",
                                            "href": "/resellers/v5/invoices/50-15318-11?customerNumber=50-452551&isoCountryCode=US",
                                            "type": "GET"
                                        }
                                    ]
                                }
                            ],
                            "links": {
                                "topic": "orders",
                                "href": "/resellers/v5/orders/50-15318?customerNumber=50-452551&isoCountryCode=US",
                                "type": "GET"
                            }
                        }
                    ]
                }
            }
        }

                Failure Response

                {
            "serviceresponse": {
                "responsepreamble": {
                    "requeststatus": "FAILED",
                    "returnmessage": "No data found"
                },
                "ordesearchresponse": {
                    "ordersfound": "0",
                    "pagesize": "0",
                    "pagenumber": "1"
                }
            }
        }
                Args:
                    po_number: Purchase order Number
                    country_code: Country Code

                Returns:

        """
        url = self.ORDERS_SEARCH_URL
        params = {
            "customerOrderNumber": po_number,
        }
        response = self._request(url, "GET", params=params)
        if response.text:
            response_payload = response.json()
            return response_payload

    def get_order_details(self, ingram_order_id):
        url = f"{self.ORDERS_BASE_URL}/{ingram_order_id}"
        response = self._request(url, "GET", params=None)
        if response.text:
            response_payload = response.json()
            return response_payload
