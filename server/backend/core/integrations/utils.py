from urllib.parse import urlencode, quote_plus

from requests import Response
from datetime import datetime


def map_object_fields(data, mapping, result=None):
    """
    Recursive function to map UI fields to CW fields
    :param data: response data
    :param mapping: fields mapping dict
    :param result:
    :return:
    """
    result = {} if result is None else result
    for key, value in data.items():
        new_key = mapping.get(key)
        if new_key:
            if isinstance(new_key, dict):
                map_object_fields(value, new_key, result)
            elif isinstance(new_key, str):
                result[new_key] = value
            else:
                new_key(result, value)
    return result


def prepare_response(data, mapping):
    """
    Function to prepare response data
    :param data: response from api
    :param mapping: fields mapping
    :return: list of items in case of data is list else dict
    """
    if isinstance(data, Response):
        data = data.json()
    if isinstance(data, list):
        result = [map_object_fields(item, mapping) for item in data]
    else:
        result = map_object_fields(data, mapping)
    return result


def _cw_in_condition_length_checker(input_list):
    payload = {"conditions": f"company/id in {tuple(input_list)}"}
    encoded_condition = urlencode(payload, quote_via=quote_plus)
    return len(encoded_condition)


def _chunks_of_list_with_max_length(max_length, input_list):
    """
    Return a list of slices based on max_length string-length boundary.
    """
    running_count = 0  # start at 0
    tmpslice = []  # tmp list where we append slice numbers.
    for i, item in enumerate(input_list):
        running_count = _cw_in_condition_length_checker(tmpslice)
        if running_count <= int(max_length):
            tmpslice.append(item)
        else:
            yield tmpslice
            tmpslice = [item]
            running_count = _cw_in_condition_length_checker(tmpslice)
    yield (tmpslice)


def prepare_response_for_software_suggestions(data):
    result = []
    for item in data:
        suggestions = item.get("suggestions")
        suggestions = suggestions[0] if suggestions else None
        if suggestions and not suggestions.get("errorDetailsResponse"):
            product_id = item.get("product").get("basePID")
            release_version = suggestions.get("releaseFormat1")
            release_date = suggestions.get("releaseDate")
            images = suggestions.get("images")[0]
            suggested_software_update = images.get("imageName")
            result.append(
                dict(
                    product_id=product_id,
                    suggested_software_update=suggested_software_update,
                    data=dict(
                        release_version=release_version,
                        release_date=release_date,
                    ),
                )
            )
    return result


def get_timestamp():
    return int(datetime.timestamp(datetime.now()))


def generate_timestamped_file_name(initial_file_name):
    file_name = initial_file_name.rsplit(".", 1)
    timestamp = str(get_timestamp())
    return f"{file_name[0]}-{timestamp}.{file_name[1]}"
