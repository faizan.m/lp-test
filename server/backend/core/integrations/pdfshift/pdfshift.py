import re
import os
from django.conf import settings
from utils import get_app_logger
from core.integrations.pdfshift.client import PdfShiftAPIClient

logger = get_app_logger(__name__)


class PdfShift:
    def __init__(self):
        self.client = PdfShiftAPIClient(settings.PDFSHIFT_API_KEY)

    def convert(
        self, source: str, output_file_name: str, upload_to_s3=True, **kwargs
    ) -> str:
        """
        PDFShift Docs: https://docs.pdfshift.io/#convert
        Args:
            source: Original document to convert to PDF. PDFShift will automatically detect if it's an URL
            and load it, or an HTML document.
            output_file_name: Name of the destination file.

            Supported params:
            css: Will append this CSS styles to the document before saving it. Can be an URL or a
            String of CSS rules.
            javascript: Will execute the given Javascript before saving the document. Can be an URL or a
            String of JS code.
            sandbox: Will generates documents that doesn't count in the credits. The generated document will
            come with a watermark. Should not be used in production.
            format: Format of the document. You can either use the standard values
            (Letter, Legal, Tabloid, Ledger, A0, A1, A2, A3, A4, A5) or a custom {width}x{height} value.
            For {width} and {height}, you can indicate the following units: in, cm, mm.

        """
        payload = {**{"source": source}, **kwargs}
        if settings.DEBUG:
            payload.update({"sandbox": "true"})
        if upload_to_s3:
            payload.update(
                {"filename": self.get_clean_file_name(output_file_name)}
            )
        response = self.client.convert(payload)
        if upload_to_s3:
            return response.json()["url"]
        else:
            with open(output_file_name, "wb") as output:
                for chunk in response.iter_content(chunk_size=1024):
                    output.write(chunk)
            return output_file_name

    @staticmethod
    def get_clean_file_name(filename):
        name, extension = os.path.splitext(filename)
        clean_name = re.sub(r"[^a-zA-Z0-9\s\-\_]", "", name)
        clean_file_name = f"{clean_name}{extension}"
        return clean_file_name
