import requests

from exceptions.exceptions import ServiceUnavailableError, build_error
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


class PdfShiftAPIClient(object):
    """
    Performs requests to the PdfShift API's.
    """

    BASE_URL = "https://api.pdfshift.io/v3/convert/pdf"

    def __init__(self, api_key):

        self.api_key = api_key
        self.username = "api"

    def post(self, url, payload, **kwargs):
        try:
            response = requests.post(
                url, json=payload, auth=(self.username, self.api_key), **kwargs
            )
            return self._handle_response(response)
        except ConnectionError as e:
            raise ServiceUnavailableError(e)

    def _handle_response(self, response):
        """Internal helper for handling API responses from the PdfShift server.

        Raises the appropriate exceptions when necessary; otherwise, returns the
        response.
        """
        if not str(response.status_code).startswith("2"):
            raise self._build_api_error(response)
        return response

    def _build_api_error(self, response, message=None, error_list=None):
        """
        Helper method for creating errors and attaching HTTP response/request
        details to them.
        """
        return build_error(
            response.status_code,
            source="PdfShift",
            message=message,
            errors=error_list,
        )

    def convert(self, payload, **kwargs):
        return self.post(self.BASE_URL, payload, **kwargs)
