from accounts.mail import BaseEmailMessage


class AutomatedSubscriptionNotificationEmail(BaseEmailMessage):
    template_name = "emails/subscription_notice_email.html"

