import pandas as pd
from dateutil import parser
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import (
    SmartnetReceivingEmailAttachment,
    SmartnetReceivingSetting,
    MSAgentProviderAssociation,
)
from core.serializers import (
    DeviceFieldMappingSerializer,
    DeviceImportFileMetaInfo,
)
from core.integrations.erp.connectwise import connectwise
from rest_framework_bulk import (
    BulkListSerializer,
    BulkSerializerMixin,
)
from accounts.models import Profile


class SmartnetReceivingFieldMappingSerializer(DeviceFieldMappingSerializer):
    snt_sku = serializers.CharField(required=True)
    quantity = serializers.CharField(required=True)
    device_price = serializers.CharField(required=False)
    contract_type = serializers.CharField(required=False, allow_null=True)
    instance_number = serializers.CharField(required=True)


class SmartnetReceivingImportFileMetaInfo(DeviceImportFileMetaInfo):
    field_mappings = SmartnetReceivingFieldMappingSerializer()
    selected_purchase_orders = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )


class EmailNotificationMetaDataSerializer(serializers.Serializer):
    send_to_customer = serializers.BooleanField(default=True)
    extra_recipient_email = serializers.ListField(
        child=serializers.EmailField(), allow_empty=True, required=False
    )


class SmartnetReceivingImportSerializer(SmartnetReceivingImportFileMetaInfo):
    email_notification_meta = EmailNotificationMetaDataSerializer(
        required=False
    )
    purchase_order_ticket_mapping = serializers.JSONField()


class SmartnetReceivingDataSerializer(serializers.Serializer):
    serial_number = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    instance_number = serializers.CharField()
    device_name = serializers.CharField()
    address = serializers.CharField(required=False)
    service_contract_number = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    product_type = serializers.CharField(required=False)
    purchase_date = serializers.CharField(required=False)
    expiration_date = serializers.CharField(required=False)
    installation_date = serializers.CharField(required=False)
    contract_type = serializers.CharField(required=False)
    device_price = serializers.FloatField(required=False)
    snt_sku = serializers.CharField()
    quantity = serializers.IntegerField(min_value=0)
    _original_index = serializers.IntegerField(required=False)

    @staticmethod
    def _invalid_value_validation(val):
        if val in ("-", ""):
            return False
        return True

    @staticmethod
    def date_validation(val, date_format):
        _date = parser.parse(val)
        # if _date is pd.np.NaN:
        #     raise ValueError()
        return _date

    def validate_serial_number(self, val):
        if not self._invalid_value_validation(val):
            raise ValidationError("Invalid Serial Number")
        visited_serial_numbers = self.context.get("visited_serial_numbers", [])
        if val in visited_serial_numbers:
            raise ValidationError("Duplicate Serial Number")
        visited_serial_numbers.append(val)
        self.context["visited_serial_numbers"] = visited_serial_numbers
        return val

    def validate_device_name(self, val):
        if not self._invalid_value_validation(val):
            raise ValidationError("Invalid Device Name")
        return val

    def validate_instance_number(self, val):
        if not self._invalid_value_validation(val):
            raise ValidationError("Invalid Instance Number")
        return val

    def validate_service_contract_number(self, val):
        if not self._invalid_value_validation(val):
            raise ValidationError("Invalid Service Contract Number")
        try:
            val = int(val)
        except ValueError:
            pass
        finally:
            return val

    def validate_purchase_date(self, val):
        date_format = self.context.get("field_mapping", {}).get(
            "purchase_date_format"
        )
        try:
            val = self.date_validation(val, date_format)
        except ValueError:
            raise ValidationError("Invalid date/date format.")
        return val

    def validate_expiration_date(self, val):
        date_format = self.context.get("field_mapping", {}).get(
            "expiration_date_format"
        )
        try:
            val = self.date_validation(val, date_format)
        except ValueError:
            raise ValidationError("Invalid date/date format.")
        return val

    def validate_installation_date(self, val):
        date_format = self.context.get("field_mapping", {}).get(
            "installation_date_format"
        )
        try:
            val = self.date_validation(val, date_format)
        except ValueError:
            raise ValidationError("Invalid date/date format.")
        return val


class SmartnetReceivingSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmartnetReceivingSetting
        fields = "__all__"
        read_only_fields = ("provider",)

    def validate_partial_receive_status(self, attrs):
        if attrs:
            receive_status = []
            for status in connectwise.partial_received_status:
                receive_status.append(status["id"])
            if attrs in receive_status:
                return attrs
            else:
                raise ValidationError("Invalid Receive Status ID")


class MSAgentProviderAssociationSerializer(serializers.ModelSerializer):
    user = serializers.ListField()

    def validate_user(self, user, **kwargs):
        """
        Validates for a provider,duplicate user should not exist.
        """
        request = self.context.get("request")
        for user_id in request.data.get("user"):
            try:
                user_profile = Profile.objects.get(
                    user__provider=request.provider,
                    user_id=user_id,
                )
                if not user_profile.cco_id:
                    raise ValidationError(
                        f"CCO ID is not associated for the user."
                    )
            except Profile.DoesNotExist:
                raise ValidationError("No user profile found.")
            try:
                queryset = MSAgentProviderAssociation.objects.get(
                    provider=request.provider
                )
            except MSAgentProviderAssociation.DoesNotExist:
                return user
            if self.instance and self.instance.id == queryset.id:
                return user
            else:
                raise ValidationError(
                    "MS Agent is already associated for this Provider."
                )

    def create(self, validated_data):
        provider = self.context.get("request").provider
        obj = MSAgentProviderAssociation.objects.create(
            provider=provider, user=validated_data.get("user")
        )
        return obj

    def update(self, instance, validated_data):
        self.instance.user = validated_data.get("user")
        self.instance.save()
        return self.instance

    class Meta:
        model = MSAgentProviderAssociation
        fields = (
            "id",
            "provider",
            "user",
        )


class SmartnetReceivingEmailAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmartnetReceivingEmailAttachment
        fields = "__all__"
        read_only_fields = ("smartnet_receiving_setting",)


class SmartnetOpportunitySerializer(serializers.Serializer):
    name = serializers.CharField()
    type_id = serializers.CharField()
    customer_id = serializers.IntegerField()
    inside_rep_id = serializers.IntegerField()
    business_unit_id = serializers.IntegerField()
    close_date = serializers.DateTimeField()
    contact_id = serializers.IntegerField()
    status_id = serializers.IntegerField(required=False, allow_null=True)
    stage_id = serializers.IntegerField(required=False, allow_null=True)


class CreateDeviceValidateSerializer(SmartnetReceivingDataSerializer):
    product_id = serializers.CharField()
    manufacturer_id = serializers.IntegerField()
    category_id = serializers.IntegerField()
    instance_number = None
    quantity = None
    snt_sku = None
