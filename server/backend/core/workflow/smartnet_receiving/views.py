import json

from django.conf import settings
from django.http import Http404
from rest_framework import generics, status
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from accounts.models import Customer
from core import utils
from core.exceptions import (
    InvalidConfigurations,
    SmartnetReceivingSettingNotConfigured,
)
from core.integrations import Connectwise
from core.models import (
    SmartnetReceivingEmailAttachment,
    SmartnetReceivingSetting,
    MSAgentProviderAssociation,
)
from core.utils import get_customer_cw_id
from core.workflow.smartnet_receiving.mixins import (
    SmartnetReceivingSettingsMixin,
)
from core.workflow.smartnet_receiving.serializers import (
    SmartnetOpportunitySerializer,
    SmartnetReceivingEmailAttachmentSerializer,
    SmartnetReceivingImportFileMetaInfo,
    SmartnetReceivingImportSerializer,
    SmartnetReceivingSettingSerializer,
    MSAgentProviderAssociationSerializer,
)
from core.workflow.smartnet_receiving.services import (
    SmartnetReceivingDataValidationService,
    SmartnetReceivingService,
)
from core.tasks.tasks import smartnet_receiving_task
from document.services import QuoteService
from rest_framework_bulk import (
    ListBulkCreateUpdateAPIView,
)


class SmartnetReceivingImportFileValidationAPIView(
    SmartnetReceivingSettingsMixin, APIView
):
    """
    API View to create multiple devices from xlsx file
    """

    def post(self, request, **kwargs):
        smartnet_receiving_settings: SmartnetReceivingSetting = (
            self.get_smartnet_receiving_settings(request.provider)
        )
        serializer = SmartnetReceivingImportFileMetaInfo(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_name = serializer.validated_data.get("file_name")
        data = SmartnetReceivingDataValidationService.get_validation_result(
            request.provider, serializer.validated_data, file_name
        )
        return Response(data=data, status=status.HTTP_200_OK)


class CustomerPurchaseOrderListAPIView(
    SmartnetReceivingSettingsMixin, APIView
):
    def get(self, request, *args, **kwargs):
        smartnet_receiving_settings: SmartnetReceivingSetting = (
            self.get_smartnet_receiving_settings(request.provider)
        )
        customer_crm_id = get_customer_cw_id(request, **kwargs)
        client: Connectwise = request.provider.erp_client
        show_all = json.loads(
            self.request.query_params.get("show-all", "false")
        )
        status_ids = []
        if not show_all:
            status_ids = (
                smartnet_receiving_settings.purchase_order_open_statuses
            )
        purchase_orders = client.get_open_purchase_order_list_for_customer(
            customer_crm_id, status_ids
        )
        return Response(data=purchase_orders, status=status.HTTP_200_OK)


class SmartnetReceivingSettingRetrieveUpdateAPIView(
    generics.RetrieveUpdateAPIView
):
    serializer_class = SmartnetReceivingSettingSerializer
    queryset = SmartnetReceivingSetting.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except Http404:
            raise SmartnetReceivingSettingNotConfigured()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {"provider_id": self.request.provider.id}
        obj = get_object_or_404(queryset, **filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        try:
            instance = self.get_object()
        except Http404:
            instance = None
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save(provider_id=self.request.provider.id)


class SmartnetReceivingEmailAttachmentListCreateAPIView(
    generics.ListCreateAPIView
):
    serializer_class = SmartnetReceivingEmailAttachmentSerializer
    queryset = SmartnetReceivingEmailAttachment.objects.all()

    def get_queryset(self):
        queryset = SmartnetReceivingEmailAttachment.objects.filter(
            smartnet_receiving_setting__provider_id=self.request.provider.id
        )
        return queryset

    def perform_create(self, serializer):

        smartnet_receiving_setting = get_object_or_404(
            SmartnetReceivingSetting.objects.all(),
            provider_id=self.request.provider,
        )
        serializer.save(smartnet_receiving_setting=smartnet_receiving_setting)


class SmartnetReceivingEmailAttachmentDestroyAPIView(generics.DestroyAPIView):
    queryset = SmartnetReceivingEmailAttachment.objects.all()
    lookup_field = "pk"


class ShipmentMethodsListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        erp_client: Connectwise = request.provider.erp_client
        data = erp_client.list_shipment_methods()
        return Response(data=data, status=status.HTTP_200_OK)


class PartialReceiveStatusListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        erp_client: Connectwise = request.provider.erp_client
        data = erp_client.list_partial_receive_status()
        return Response(data=data, status=status.HTTP_200_OK)


class PurchaseOrderStatusesListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        erp_client: Connectwise = request.provider.erp_client
        data = erp_client.list_purchase_order_statuses()
        return Response(data=data, status=status.HTTP_200_OK)


class SalesOrderStatusesListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        erp_client: Connectwise = request.provider.erp_client
        data = erp_client.list_sales_order_statuses()
        return Response(data=data, status=status.HTTP_200_OK)


class SmartnetReceivingImportAPIView(
    SmartnetReceivingSettingsMixin, generics.CreateAPIView
):
    serializer_class = SmartnetReceivingImportSerializer

    def create(self, request, *args, **kwargs):
        self.get_smartnet_receiving_settings(request.provider)
        if not request.provider.integration_statuses[
            "crm_board_mapping_configured"
        ]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        smartnet_complete_status = (
            SmartnetReceivingService.get_smartnet_complete_status(
                request.provider
            )
        )
        if not smartnet_complete_status:
            raise InvalidConfigurations(
                "Smartnet Service Board settings not completed."
            )
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        provider_id = request.provider.id
        customer_id = self.kwargs.get("customer_id")
        file_name = serializer.validated_data.get("file_name")
        task = smartnet_receiving_task.apply_async(
            (
                provider_id,
                customer_id,
                request.user.id,
                file_name,
                serializer.validated_data,
            ),
            queue="high",
        )
        return Response(
            data={"task_id": task.task_id}, status=status.HTTP_200_OK
        )


class PurchaseOrderTicketListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        smartnet_complete_status = (
            SmartnetReceivingService.get_smartnet_complete_status(
                request.provider
            )
        )
        if not smartnet_complete_status:
            raise InvalidConfigurations(
                "Smartnet Service Board settings not completed."
            )
        customer_crm_id = get_customer_cw_id(request, **kwargs)
        service_tickets = (
            SmartnetReceivingService.get_purchase_orders_ticket_list(
                request.provider, customer_crm_id
            )
        )
        return Response(data=service_tickets, status=status.HTTP_200_OK)


class PurchaseOrderTicketNotesListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        service_ticket_id = kwargs.get("service_ticket_id")
        all_notes = json.loads(
            self.request.query_params.get("all_notes", "false")
        )
        service_ticket_notes = (
            SmartnetReceivingService.get_purchase_orders_ticket_notes(
                request.provider, service_ticket_id
            )
        )
        latest_updated_ticket_notes = []
        if service_ticket_notes:
            if all_notes:
                latest_updated_ticket_notes = service_ticket_notes
            else:
                latest_updated_ticket_notes = service_ticket_notes[0]
        return Response(
            data=latest_updated_ticket_notes, status=status.HTTP_200_OK
        )


class SNTEMailNotificationRecipientListAPIView(
    SmartnetReceivingSettingsMixin, APIView
):
    def get(self, request, *args, **kwargs):
        smartnet_receiving_settings = self.get_smartnet_receiving_settings(
            request.provider
        )
        customer_id = kwargs.get("customer_id")
        customer = get_object_or_404(Customer.objects.all(), id=customer_id)
        recipients = SmartnetReceivingService.get_snt_email_notification_recipients_list(
            request.provider,
            request.user,
            customer,
            smartnet_receiving_settings,
        )
        return Response(data=recipients, status=status.HTTP_200_OK)


class ProviderSmartnetReceivingSettingsCheck(
    SmartnetReceivingSettingsMixin, APIView
):
    def get(self, request, *args, **kwargs):
        result = True
        try:
            self.get_smartnet_receiving_settings(request.provider)
            if not request.provider.integration_statuses[
                "crm_board_mapping_configured"
            ]:
                raise InvalidConfigurations("Board Mapping Not Configured.")
            smartnet_complete_status = (
                SmartnetReceivingService.get_smartnet_complete_status(
                    request.provider
                )
            )
            if not smartnet_complete_status:
                raise InvalidConfigurations(
                    "Smartnet Service Board settings not completed."
                )
        except InvalidConfigurations:
            result = False
        data = dict(is_configured=result)
        return Response(data=data, status=status.HTTP_200_OK)


class SmartnetWorkflowOpportunityCreateAPIView(APIView):
    def post(self, request, **kwargs):
        serializer = SmartnetOpportunitySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payload = serializer.validated_data
        customer_id = payload.get("customer_id")
        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise NotFound("Customer does not exist")
        open_status_id = payload.get("status_id")
        provider = request.provider
        territory_manager_id = QuoteService.get_customer_territory_manager(
            provider, customer_id
        )
        close_date = utils.convert_date_object_to_date_string(
            payload.get("close_date"), settings.ISO_FORMAT
        )
        payload.update(
            {
                "territory_manager_id": territory_manager_id,
                "close_date": close_date,
                "user_id": payload.get("contact_id"),
            }
        )
        provider.erp_client.create_company_quote(
            customer.crm_id, open_status_id, payload
        )
        return Response(status=status.HTTP_201_CREATED)


class TestSNTNotificationEmail(APIView):
    def post(self, request, *args, **kwargs):
        to_email = request.data.get("to_email")
        email_body = request.data.get("email_body")
        if not to_email:
            raise ValidationError({"to_email": "This field is required"})
        if not email_body:
            raise ValidationError({"email_body": "This field is required"})
        try:
            provider = request.user.provider
        except AttributeError:
            provider = None
        sent = SmartnetReceivingService.test_snt_email(
            to_email, email_body, provider
        )
        return Response(data={"sent": sent}, status=status.HTTP_200_OK)


class MSAgentProviderAssociationListCreateUpdateAPIView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = MSAgentProviderAssociationSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        data = MSAgentProviderAssociation.objects.filter(
            provider_id=provider.id
        )
        return data

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)
