import csv
import datetime
import os
import shutil
import tempfile
import uuid
from collections import defaultdict, namedtuple
from operator import itemgetter
from smtplib import SMTPException
from typing import Dict, List, Tuple, Union, Optional

import pandas as pd
import pdfkit
import requests
import xlsxwriter
from django.conf import settings
from django.template.loader import get_template
from rest_framework.exceptions import ValidationError
from retrying import retry

from accounts.models import Customer, Provider, User
from core.integrations import Connectwise
from core.integrations.utils import prepare_response
from core.models import SmartnetReceivingSetting
from core.tasks.connectwise.tasks import bulk_create_devices
from core.utils import (
    convert_date_object_to_date_string,
    convert_date_string_format,
    generate_random_id,
    group_list_of_dictionary_by_key,
)
from core.workflow.smartnet_receiving.emails import (
    SNTReceivingNotificationEmail,
    TACEmail,
)
from core.workflow.smartnet_receiving.serializers import (
    SmartnetReceivingDataSerializer,
    CreateDeviceValidateSerializer,
)
from exceptions.exceptions import InvalidRequestError
from service_requests.mixins import ServiceRequestModelMixin
from service_requests.services import ServiceRequestService
from storage.file_upload import FileUploadService
from utils import get_app_logger, get_valid_file_name
from core.integrations.erp.connectwise.connectwise import (
    partial_received_status,
    FULLY_RECEIVED,
)
from utils import convert_local_files_to_remote_attachments
from typing import List

logger = get_app_logger(__name__)


class ColumnMapping:
    __slots__ = [
        "serial_number_column",
        "instance_number_column",
        "device_name_column",
        "address_column",
        "service_contract_number_column",
        "product_type_column",
        "purchase_date_column",
        "expiration_date_column",
        "installation_date_column",
        "device_price_column",
        "snt_sku_column",
        "quantity_column",
        "contract_type_column",
    ]

    def __init__(self, mapping):
        self.serial_number_column = "serial_number"
        self.instance_number_column = "instance_id"
        self.device_name_column = "device_name"
        self.address_column = "address"
        self.service_contract_number_column = "service_contract_number"
        self.product_type_column = "product_type"
        self.purchase_date_column = "purchase_date"
        self.expiration_date_column = "expiration_date"
        self.installation_date_column = "installation_date"
        self.device_price_column = "device_price"
        self.snt_sku_column = "snt_sku"
        self.quantity_column = "quantity"
        self.contract_type_column = "contract_type"


class SmartnetReceivingDataValidationService:
    __slots__ = []

    @classmethod
    def download_file(cls, file_path: str) -> Optional[str]:
        """
        Download/copy file to local path
        Args:
            file_path: file location path / url
        """
        try:
            file_name = os.path.basename(file_path)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            logger.info(f"File not found at location {file_path}")
            return None
        local_file_path = os.path.join(tempfile.gettempdir(), file_name)
        if settings.DEBUG:
            shutil.copy(file_path, local_file_path)
        else:
            resp = requests.get(file_path)
            try:
                with open(local_file_path, "wb") as output:
                    output.write(resp.content)
            except (FileNotFoundError, OSError) as e:
                logger.error(e)
                return None
        return local_file_path

    @classmethod
    def read_xlsx_file_to_dict(
        cls, file_url: str, field_mapping: Dict
    ) -> List[Dict]:
        """
        Read the 1st sheet of xlsx file and convert it to Dict
        """
        # Parsing xlsx file
        file_path = cls.download_file(file_url)
        if not file_path:
            return []
        date_columns = [
            "purchase_date",
            "expiration_date",
            "installation_date",
        ]
        date_dtypes = {
            field_mapping.get(col): str
            for col in date_columns
            if field_mapping.get(col)
        }
        df = pd.read_excel(file_path, parse_dates=False, dtype=date_dtypes)
        df.fillna("", inplace=True)
        df.rename(columns=lambda x: x.strip(), inplace=True)
        return df.to_dict("records")

    @classmethod
    def prepare_validation_result(
        cls,
        errors: List[Dict],
        error_indexes: List,
        warnings: Dict,
        field_mapping: Dict,
    ) -> List[Dict]:
        """
        Prepare validation result using the errors and warnings.
        Args:
            errors: List of Errors found
            error_indexes: Relative indexes of errors
            warnings: Dict containing index and warning message
            field_mapping: original field mapping

        """
        validation_data = []
        for pos, error in enumerate(errors):
            row_number = error_indexes[pos] + 2

            for field, field_errors in error.items():
                _data = {}
                _data["Row #"] = row_number
                _data["error_index"] = error_indexes[pos]
                _data["Field"] = field_mapping.get(field, field)
                _data["Error Messages"] = ", ".join(field_errors)
                _data["Warnings"] = ", ".join(
                    warnings.pop(error_indexes[pos], [])
                )
                validation_data.append(_data)

        for pos, _warnings in warnings.items():
            row_number = pos + 2
            _data = {}
            _data["Row #"] = row_number
            _data["error_index"] = pos
            _data["Field"] = ""
            _data["Error Messages"] = ""
            _data["Warnings"] = ", ".join(_warnings)
            validation_data.append(_data)

        validation_data = sorted(validation_data, key=itemgetter("Row #"))
        return validation_data

    @classmethod
    def rename_fields(
        cls, data: List[Dict], field_mapping: Dict
    ) -> List[Dict]:
        """
        Renames keys in data based on the field mapping

        """
        return prepare_response(data, field_mapping)

    @classmethod
    def read_file(cls, file_path: str, field_mapping: Dict) -> List[Dict]:
        """
        Read and parse Smartnet Receiving Import file

        """
        data = cls.read_xlsx_file_to_dict(file_path, field_mapping)
        data = cls.rename_fields(
            data, {v: k for k, v in field_mapping.items()}
        )
        return data

    @classmethod
    def validate_input_data(
        cls, records: List[Dict], field_mapping: Dict
    ) -> Tuple[List[Dict], List, List]:
        """
        Validates the records, and returns the validated data, errors found and error indexes

        """
        serializer = SmartnetReceivingDataSerializer(
            data=records, many=True, context={"field_mapping": field_mapping}
        )
        serializer.is_valid(raise_exception=False)
        error_indexes = []
        errors = []
        if serializer.errors:
            errors = []
            for idx, row_errors in enumerate(list(serializer.errors)):
                if row_errors:
                    errors.append(row_errors)
                    error_indexes.append(idx)
        records = [
            r for idx, r in enumerate(records) if idx not in error_indexes
        ]
        serializer = SmartnetReceivingDataSerializer(
            data=records, many=True, context={"field_mapping": field_mapping}
        )
        serializer.is_valid()
        return serializer.validated_data, errors, error_indexes

    @classmethod
    def populate_address_site_id(
        cls,
        records: List[Dict],
        payload: Dict,
        field_mapping_obj: ColumnMapping,
    ) -> List[Dict]:
        address_site_mapping = {}
        for item in payload.get("address_field_meta"):
            address_site_mapping[item.get("address")] = item.get("site_id")
        for record in records:
            record["site_id"] = str(
                address_site_mapping.get(
                    record.get(field_mapping_obj.address_column)
                )
            )
        return records

    @classmethod
    def get_purchase_order_line_items(
        cls,
        erp_client: Connectwise,
        purchase_order_ids: List,
        product_ids: Union[List, None] = None,
        **kwargs,
    ) -> List[Dict]:
        line_items = []
        product_ids = product_ids or None
        for purchase_order_id in purchase_order_ids:
            _line_items = erp_client.get_purchase_order_line_items(
                purchase_order_id, product_ids, **kwargs
            )
            line_items.extend(_line_items)
        return line_items

    @classmethod
    def validate_purchase_orders(
        cls,
        erp_client: Connectwise,
        input_data: List[Dict],
        field_mapping_obj: ColumnMapping,
        payload: Dict,
    ) -> Tuple[Dict, bool, List]:
        snt_sku_data = defaultdict(int)
        for rec in input_data:
            snt_sku_data[rec[field_mapping_obj.snt_sku_column]] += rec[
                field_mapping_obj.quantity_column
            ]
        selected_purchase_orders = payload.get("selected_purchase_orders")
        line_items = cls.get_purchase_order_line_items(
            erp_client, selected_purchase_orders
        )
        line_items_quantity_mapping = defaultdict(int)
        unmatched_line_items = []
        for line_item in line_items:
            product_id = line_item.get("product", {}).get("identifier")
            if product_id not in list(snt_sku_data.keys()):
                unmatched_line_items.append(line_item)
            else:
                line_items_quantity_mapping[product_id] += int(
                    line_item.get("quantity")
                )

        validation_result = {}
        is_valid = True
        for product_id, quantity in snt_sku_data.items():
            line_item_valid = quantity == line_items_quantity_mapping.get(
                product_id
            )
            validation_result[product_id] = {
                "input_quantity": quantity,
                "cw_quantity": line_items_quantity_mapping.get(product_id),
                "is_valid": line_item_valid,
            }
            is_valid = is_valid and line_item_valid
        return validation_result, is_valid, unmatched_line_items

    @classmethod
    def _get_record_price(cls, record, field_mapping_object):
        try:
            price = float(
                record.get(field_mapping_object.device_price_column, 0)
            )
        except ValueError:
            price = 0
        return price

    @classmethod
    def filter_zero_dollar_items(
        cls, records: List[Dict], field_mapping_object: ColumnMapping, warnings
    ) -> Dict:
        warning_message = "Device Price is Zero"
        for ind, rec in enumerate(records):
            price = cls._get_record_price(rec, field_mapping_object)
            if not price > 0:
                warnings[ind].append(warning_message)
        return warnings

    @classmethod
    def filter_by_product_type(
        cls, records, payload, field_mapping_obj, warnings
    ):
        filter_meta = payload.get("device_type_filter_meta")
        filter_meta_dict = {item["product_type"]: item for item in filter_meta}
        for pos, record in enumerate(records):
            product_type = record.get(field_mapping_obj.product_type_column)
            if product_type and product_type in filter_meta_dict:
                included = filter_meta_dict[product_type].get("include")
                ignore_zero_dollar_items = filter_meta_dict[product_type].get(
                    "ignore_zero_dollar_items"
                )
                if not included:
                    warning_msg = "Product Type not to be included"
                    warnings[pos].append(warning_msg)
                if ignore_zero_dollar_items:
                    price = cls._get_record_price(record, field_mapping_obj)
                    warning_msg = "Product Type with Device Price as Zero"
                    if not price > 0:
                        warnings[pos].append(warning_msg)
        return warnings

    @classmethod
    def apply_filters(cls, input_data, payload, field_mapping_obj):
        warnings: Dict[int, List] = defaultdict(list)
        if payload.get("ignore_zero_dollar_items"):
            # filter $0 items
            warnings = cls.filter_zero_dollar_items(
                input_data, field_mapping_obj, warnings
            )
        if payload.get("filter_by_product_type"):
            # filter by product type
            warnings = cls.filter_by_product_type(
                input_data, payload, field_mapping_obj, warnings
            )
        return warnings

    @classmethod
    def validate(
        cls, provider: Provider, payload: Dict, file_name: str
    ) -> Tuple[List[Dict], Dict]:
        field_mapping: Dict[str, str] = payload.get("field_mappings")
        field_mapping_obj = ColumnMapping(field_mapping)
        device_import_validation_result = []

        # read records from input file
        input_data = cls.read_file(file_name, field_mapping)
        for idx, record in enumerate(input_data):
            record["_original_index"] = idx

        if not input_data:
            return [], {}
        # apply device import validations
        validated_data, errors, error_indexes = cls.validate_input_data(
            input_data, field_mapping
        )

        # apply filters
        warnings = cls.apply_filters(input_data, payload, field_mapping_obj)

        # apply purchase order validations
        (
            purchase_order_validation_data,
            is_valid,
            unmatched_line_items,
        ) = cls.validate_purchase_orders(
            provider.erp_client, validated_data, field_mapping_obj, payload
        )
        # add site_id to validated data
        validated_data = cls.populate_address_site_id(
            validated_data, payload, field_mapping_obj
        )
        if errors or warnings:
            device_import_validation_result = cls.prepare_validation_result(
                errors, error_indexes, warnings, field_mapping
            )
        error_indexes: set = {
            error["error_index"]
            for error in device_import_validation_result
            if error.get("Error Messages")
        }
        device_error_product_numbers = {
            record.get(field_mapping_obj.snt_sku_column)
            for idx, record in enumerate(input_data)
            if idx in error_indexes
        }

        clean_data = []
        for record in validated_data:
            if (
                record.get(field_mapping_obj.snt_sku_column)
                not in device_error_product_numbers
            ):
                clean_data.append(record)
            else:
                row_number = record.get("_original_index") + 2
                _data = {}
                _data["Row #"] = row_number
                _data["error_index"] = record.get("_original_index")
                _data["Field"] = field_mapping.get(
                    field_mapping_obj.snt_sku_column
                )
                _data[
                    "Error Messages"
                ] = "Product Number matches the Product Number of record with device errors."
                _data["Warnings"] = ""
                device_import_validation_result.append(_data)
        device_import_validation_result = sorted(
            device_import_validation_result, key=itemgetter("Row #")
        )

        validation_result = dict(
            purchase_order_validation_result=dict(
                is_valid=is_valid,
                validation_data=purchase_order_validation_data,
                unmatched_line_items=unmatched_line_items,
            ),
            device_import_validation_result=dict(
                is_valid=not bool(errors),
                validated_data=device_import_validation_result,
                validation_data=device_import_validation_result,
            ),
            error_product_numbers=device_error_product_numbers,
            error_indexes=error_indexes,
        )
        return clean_data, validation_result

    @classmethod
    def get_validation_result(
        cls, provider: Provider, payload: Dict, file_name: str
    ):
        _, validation_result = cls.validate(provider, payload, file_name)
        return validation_result


class SmartnetReceivingService:
    @classmethod
    def get_smartnet_service_board_settings(cls, provider: Provider):
        smartnet_services_configs = (
            provider.erp_integration.other_config.get("board_mapping", {})
            .get("Service Board Mapping", {})
            .get("Smartnet Services", {})
        )
        return smartnet_services_configs

    @classmethod
    def get_purchase_orders_ticket_list(
        cls, provider: Provider, customer_crm_id: int
    ):
        smartnet_services_configs = cls.get_smartnet_service_board_settings(
            provider
        )
        boards: List = smartnet_services_configs.get("board", [])
        client: Connectwise = provider.erp_client
        service_tickets = client.get_service_tickets(customer_crm_id, boards)
        return service_tickets

    @classmethod
    def get_purchase_orders_ticket_notes(
        cls, provider: Provider, service_ticket_id: int
    ):
        service_tickets_notes = ServiceRequestService.get_ticket_notes(
            provider.erp_client,
            service_ticket_id,
            external_flag_filter=False,
            internal_flag_filter=True,
        )
        return service_tickets_notes

    @classmethod
    def get_smartnet_complete_status(cls, provider: Provider):
        smartnet_services_configs = cls.get_smartnet_service_board_settings(
            provider
        )
        if smartnet_services_configs.get("smartnet_complete_status"):
            return smartnet_services_configs.get("smartnet_complete_status")[0]
        return None

    @classmethod
    def get_smartnet_receiving_settings(cls, provider):
        try:
            smartnet_receiving_settings: SmartnetReceivingSetting = (
                provider.snt_receiving_settings
            )
        except SmartnetReceivingSetting.DoesNotExist as e:
            logger.error(
                "Smartnet Receiving Settings not configured for the Provider. "
            )
            raise e
        return smartnet_receiving_settings

    @classmethod
    def get_partial_receive_status_settings(cls, provider):
        try:
            smartnet_receiving_settings = (
                SmartnetReceivingService.get_smartnet_receiving_settings(
                    provider
                )
            )
            partial_receive_status = (
                smartnet_receiving_settings.partial_receive_status
            )
            if not partial_receive_status:
                logger.error(
                    "Smartnet Receiving Settings for partial status is not configured for the Provider. "
                )
                return
        except Exception as e:
            logger.error(
                "Smartnet Receiving Settings for partial status is not configured for the Provider. "
            )
            raise e
        return partial_receive_status

    @classmethod
    def update_purchase_order(cls, provider, validated_data, request_payload):
        Output = namedtuple("Output", ["result", "errors"])
        logger.debug("Updating Purchase order")
        try:
            smartnet_receiving_settings = cls.get_smartnet_receiving_settings(
                provider
            )
        except SmartnetReceivingSetting.DoesNotExist as e:
            logger.error(f"Unable to update purchase orders. {e}")
            return Output(False, {})

        data_grouped_by_product_number = group_list_of_dictionary_by_key(
            validated_data, "snt_sku"
        )
        line_items = SmartnetReceivingDataValidationService.get_purchase_order_line_items(
            provider.erp_client,
            request_payload.get("selected_purchase_orders", []),
            list(data_grouped_by_product_number.keys()),
        )
        receiving_errors = {}
        updated_quantity = defaultdict(int)
        for (
            product_id,
            product_group,
        ) in data_grouped_by_product_number.items():
            LineItem = namedtuple(
                "LineItem",
                ["id", "purchase_order_id", "quantity", "product_id"],
            )
            matching_line_item_ids: List[LineItem] = [
                LineItem(
                    item["id"],
                    item["purchaseOrderId"],
                    item["quantity"],
                    item["product"]["identifier"],
                )
                for item in line_items
                if item["product"]["identifier"] == product_id
            ]
            serial_numbers = [
                item.get("serial_number")
                for item in product_group
                if item.get("serial_number")
            ]
            received_quantity = [
                item.get("quantity")
                for item in product_group
                if item.get("quantity")
            ]
            shipping_method_id = smartnet_receiving_settings.shipment_method_id
            _partial_receive_status = (
                smartnet_receiving_settings.partial_receive_status
            )
            try:
                for status in partial_received_status:
                    if status.get("id") == _partial_receive_status:
                        received_status = status.get("status")
            except Exception as e:
                received_status = None
            client: Connectwise = provider.erp_client
            total_cw_quantity = [
                int(line_item.quantity) for line_item in matching_line_item_ids
            ]
            total_received_quantity = int(sum(received_quantity))
            for line_item in matching_line_item_ids:
                data = {
                    "client": client,
                    "provider": provider,
                    "line_item": line_item,
                    "serial_numbers": serial_numbers,
                    "shipping_method_id": shipping_method_id,
                    "updated_quantity": updated_quantity,
                    "receiving_errors": receiving_errors,
                }
                if sum(total_cw_quantity) == total_received_quantity:
                    # this case is to handle the total sum in the input sheet for a line identifier
                    # with the total sum in CW to fully received for all lines of a line identifier
                    data["received_quantity"] = [line_item.quantity]
                    data["received_status"] = FULLY_RECEIVED
                    receiving_errors = cls.update_order_service(data)
                else:
                    # this case is to handle partial received or fully received qty in the sheet for a line item
                    all_quantity = line_item.quantity
                    if all_quantity == total_received_quantity:
                        # this case is to handle fully received
                        data["received_quantity"] = received_quantity
                        data["received_status"] = FULLY_RECEIVED
                        receiving_errors = cls.update_order_service(data)
                    else:
                        # this case is to split the quantity, either received partially or fully
                        if (
                            all_quantity <= total_received_quantity
                            and len(total_cw_quantity) > 1
                        ):
                            # split the inputed qty if input qty is greater or equal than
                            # the CW qty and more than one lines for a line identifier
                            # then received fully in CW
                            data["received_quantity"] = [all_quantity]
                            data["received_status"] = FULLY_RECEIVED
                            receiving_errors = cls.update_order_service(data)
                            total_received_quantity = (
                                total_received_quantity - all_quantity
                            )
                        else:
                            # split the inputed qty if input qty is less than the CW qty for the line
                            # then received partially in CW
                            data["received_quantity"] = [
                                total_received_quantity
                            ]
                            data["received_status"] = received_status
                            receiving_errors = cls.update_order_service(data)
        logger.debug("Purchase orders updated.")
        return Output(True, receiving_errors)

    @classmethod
    def update_order_service(cls, data):
        try:
            data.get("client").update_purchase_order_line_item(
                data.get("provider"),
                data.get("line_item").purchase_order_id,
                data.get("line_item").id,
                serial_numbers=data.get("serial_numbers")[
                    int(
                        data.get("updated_quantity")[
                            data.get("line_item").product_id
                        ]
                    ) : int(data.get("line_item").quantity)
                ],
                received_qty=int(sum(data.get("received_quantity"))),
                shipping_method_id=data.get("shipping_method_id"),
                ship_date=datetime.datetime.strftime(
                    datetime.datetime.utcnow(), settings.ISO_FORMAT
                ),
                received_status=data.get("received_status"),
            )
            data.get("updated_quantity")[
                data.get("line_item").product_id
            ] += data.get("line_item").quantity
            return {}
        except InvalidRequestError as e:
            logger.debug(
                f"Error receiving Line item. Details:"
                f"Product Id: {data.get('line_item').product_id}"
                f"Purchase Order: {data.get('line_item').purchase_order_id}"
                f"Line Item: {data.get('line_item').id}"
                f"Error: {e.detail}"
            )
            data.get("receiving_errors")[
                data.get("line_item").product_id
            ] = e.detail
            return data.get("receiving_errors")

    @classmethod
    def create_devices(
        cls, provider, category_id, customer_id, manufacturer_id, device_data
    ):
        logger.debug("Device bulk create started.")
        result, serial_numbers_crm_id_mapping, _ = bulk_create_devices(
            provider.id,
            provider.erp_client,
            customer_id,
            manufacturer_id,
            device_data,
            category_id=category_id,
        )
        result.update(
            {"serial_numbers": list(serial_numbers_crm_id_mapping.keys())}
        )
        logger.debug("Device bulk create finished.")
        return result

    @classmethod
    def get_purchase_order_ticket_details(
        cls, erp_client: Connectwise, ticket_id
    ):
        ticket_details = erp_client.get_service_ticket(ticket_id)
        return ticket_details

    @classmethod
    def update_service_ticket(
        cls,
        erp_client: Connectwise,
        ticket_id: int,
        status_id: int,
        note: str,
        service_ticket_created_by: str,
        attachment_path: str,
    ):
        # update ticket status
        erp_client.update_service_ticket_status(ticket_id, status_id)
        # Update internal note
        erp_client.create_ticket_internal_note(
            ticket_id, service_ticket_created_by, dict(text=note)
        )
        # upload attachment
        file_local_path = SmartnetReceivingDataValidationService.download_file(
            attachment_path
        )
        if file_local_path is None:
            return False
        try:
            with open(file_local_path, "rb") as file:
                file_name = os.path.basename(file_local_path)
                if not settings.DEBUG and "?" in file_name:
                    file_name = file_name.split("?")[0]
                erp_client.create_service_ticket_document(
                    ticket_id, file_name, file
                )
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return False
        return True

    @classmethod
    def get_snt_email_notification_recipients_list(
        cls,
        provider: Provider,
        user: User,
        customer: Customer,
        smartnet_receiving_settings: SmartnetReceivingSetting,
        **kwargs,
    ) -> List[str]:

        # prepare recipients list
        recipient_emails = [
            user.email,
            smartnet_receiving_settings.order_status_alias_email,
        ]

        # read meta data for email notification
        send_customer_contact_email = kwargs.get(
            "send_customer_contact_email", False
        )
        if send_customer_contact_email:
            service_ticket_customer_contact_emails = kwargs.get(
                "service_ticket_customer_contact_emails"
            )
            assert service_ticket_customer_contact_emails
            additional_email_recipients = kwargs.get(
                "additional_email_recipients", []
            )
            recipient_emails.extend(
                list(service_ticket_customer_contact_emails)
            )
            recipient_emails.extend(additional_email_recipients)

        # fetch account manager email
        account_manager_email = customer.get_territory_manager_email(
            provider.erp_client
        )
        if account_manager_email:
            recipient_emails.append(account_manager_email)
        return recipient_emails

    @classmethod
    def _get_snt_email_notification_attachments(
        cls, smartnet_receiving_settings: SmartnetReceivingSetting
    ) -> List:
        attachments = []
        for file in smartnet_receiving_settings.attachments:
            if not settings.DEBUG:
                file_path = (
                    SmartnetReceivingDataValidationService.download_file(
                        file.attachment.url
                    )
                )
                if file_path is None:
                    continue
            else:
                file_path = file.attachment.path
            attachments.append((file.name, file_path))
        return attachments

    @classmethod
    @retry(
        stop_max_attempt_number=2,
        retry_on_exception=lambda e: isinstance(e, SMTPException),
        wait_fixed=1000,
    )
    def send_email_notification(
        cls,
        provider: Provider,
        user: User,
        customer: Customer,
        smartnet_receiving_settings: SmartnetReceivingSetting,
        opportunity_name: str,
        opportunity_id,
        **kwargs,
    ) -> Tuple[bool, List]:
        logger.debug("Sending SNT Receiving Notification email.")

        recipients_list = cls.get_snt_email_notification_recipients_list(
            provider, user, customer, smartnet_receiving_settings, **kwargs
        )
        attachments = list(
            cls._get_snt_email_notification_attachments(
                smartnet_receiving_settings
            )
        )
        customer_output_file = kwargs.get("customer_output_file")
        if customer_output_file:
            name = os.path.basename(customer_output_file)
            attachments.append((name, customer_output_file))
        opportunity_name_regex = ["HW ", "WR ", "SW "]
        for oppo_regex in opportunity_name_regex:
            opportunity_name = opportunity_name.replace(oppo_regex, "")
        company_quote = provider.erp_client.get_company_quote(
            opportunity_id, customer.id
        )
        contact = (
            company_quote.get("primary_contact_name").split(" ")[0]
            if company_quote.get("primary_contact_name")
            else customer.name
        )
        email_context = {
            "subject": f"Smartnet Delivery Notice {opportunity_name}",
            "body": smartnet_receiving_settings.email_body,
            "contact": contact,
            "provider": provider,
        }
        logger.debug(
            f"Sending Smartnet Receiving notification email to following emails:{recipients_list}"
        )
        operation_email_template_settings = (
            provider.operation_email_template_settings
        )
        contact_type_connectwise_id = (
            operation_email_template_settings.contact_type_connectwise_id
        )
        cc_users = []
        if contact_type_connectwise_id:
            cc_contacts = provider.erp_client.get_customer_users(
                customer_id=customer.crm_id,
                required_fields=[
                    "id",
                    "firstName",
                    "communicationItems",
                    "types",
                ],
                type_filter=contact_type_connectwise_id,
            )
            for contact in cc_contacts:
                if contact.get("email"):
                    cc_users.append(contact.get("email"))
        files, remote_attachments = convert_local_files_to_remote_attachments(
            attachments
        )
        try:
            SNTReceivingNotificationEmail(
                context=email_context,
                files=files,
                remote_attachments=remote_attachments,
                from_email=smartnet_receiving_settings.email_notifications_from_email,
            ).send(to=recipients_list, cc=cc_users)
        except SMTPException as e:
            logger.error(f"SNT Receiving Notification email failed: {e}")
            return False, []
        logger.debug(f"SNT Receiving Notification email sent.")
        return True, recipients_list

    @classmethod
    def get_customer_user_cco_id(cls, provider_id, customer_id, user_email):
        try:
            user = User.customers.get(
                provider_id=provider_id,
                customer_id=customer_id,
                email=user_email,
            )
            if hasattr(user.profile, "cco_id"):
                return user.profile.cco_id
        except User.DoesNotExist:
            msg = f"Customer user not found for email: {user_email}"
            logger.debug(msg)
            return None

    @classmethod
    def send_tac_email(
        cls,
        smartnet_receiving_settings: SmartnetReceivingSetting,
        user_email: str,
        cco_ids: List,
        contract_numbers: List,
        provider: Provider,
    ):
        result_meta_data = {}
        tac_email = smartnet_receiving_settings.tac_email
        subject = smartnet_receiving_settings.tac_email_subject
        body = smartnet_receiving_settings.tac_email_body
        context = dict(
            contract_numbers=contract_numbers,
            cco_ids=cco_ids,
            subject=subject,
            body=body,
            provider=provider,
        )
        to_list = [tac_email]
        cc_list = [
            user_email,
            smartnet_receiving_settings.order_status_alias_email,
        ]
        try:
            TACEmail(
                context=context,
                from_email=smartnet_receiving_settings.email_notifications_from_email,
            ).send(to=to_list, cc=cc_list)
            logger.debug(f"TAC email sent.")
        except SMTPException as e:
            logger.error(f"TAC email failed: {e}")
            return False, result_meta_data
        result_meta_data = dict(
            to_emails=to_list,
            cc_emails=cc_list,
            contract_numbers=contract_numbers,
            cco_ids=cco_ids,
        )
        return True, result_meta_data

    @classmethod
    def generate_customer_output_file(
        cls,
        customer_name: str,
        devices: List[Dict],
        field_mapping_obj: ColumnMapping,
    ):
        file_title = "Smart Net Contract Information"
        field_mappings = {
            "SmartnetSKU": field_mapping_obj.snt_sku_column,
            "Serial Number": field_mapping_obj.serial_number_column,
            "Contract #": field_mapping_obj.service_contract_number_column,
            "Contract Start Date": field_mapping_obj.purchase_date_column,
            "Contract Exp. Date": field_mapping_obj.expiration_date_column,
            "Device Location": field_mapping_obj.address_column,
        }
        data = []
        for device in devices:
            row = {
                column: device.get(key)
                for column, key in field_mappings.items()
            }
            data.append(row)
        headers = list(field_mappings.keys())
        TEMP_DIR = tempfile.gettempdir()
        file_name = f"{generate_random_id(15)}.csv"
        file_path = os.path.join(TEMP_DIR, file_name)
        try:
            with open(file_path, "w") as output_file:
                writer = csv.writer(output_file)
                writer.writerow(customer_name)
                writer.writerow(file_title)
                dict_writer = csv.DictWriter(output_file, headers)
                dict_writer.writeheader()
                dict_writer.writerows(data)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return None
        return file_path

    @classmethod
    def generate_customer_output_xlsx_file(
        cls,
        customer_name: str,
        devices: List[Dict],
        field_mapping_obj: ColumnMapping,
        opportunity_name: str,
    ):
        current_date = convert_date_object_to_date_string(
            datetime.datetime.now().date(), "%d-%b-%Y"
        )
        file_title = get_valid_file_name(
            f"Smart Net Delivery {opportunity_name} {current_date}.xlsx"
        )
        TEMP_DIR = tempfile.gettempdir()
        file_path = os.path.join(TEMP_DIR, file_title)
        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(file_path)
        # Add worksheet
        worksheet = workbook.add_worksheet(name="Smart Net Contract")

        # generate logo path
        logo_path = os.path.join(
            settings.STATIC_ROOT,
            "img",
            "lookingpoint-logo-snt-receiving-report.png",
        )
        worksheet.insert_image(
            "A1", logo_path, {"x_scale": 0.5, "y_scale": 0.6}
        )
        # worksheet.write("F2", "Educational Employees Credit Union")
        worksheet.merge_range("D2:F2", customer_name)
        worksheet.merge_range("D3:F3", "Smart Net Contract Information")
        worksheet.merge_range("D4:F4", opportunity_name)

        field_mappings = {
            "SmartnetSKU": field_mapping_obj.snt_sku_column,
            "Product ID": "product_id",
            "Serial Number": field_mapping_obj.serial_number_column,
            "Contract #": field_mapping_obj.service_contract_number_column,
            "Contract Type": field_mapping_obj.contract_type_column,
            "Contract Start Date": field_mapping_obj.installation_date_column,
            "Contract Exp. Date": field_mapping_obj.expiration_date_column,
            "Device Location": field_mapping_obj.address_column,
        }
        file_records = []
        for device in devices:
            row = {
                column: device.get(key)
                for column, key in field_mappings.items()
            }
            file_records.append(row)

        headers = [
            "SmartnetSKU",
            "Product ID",
            "Serial Number",
            "Contract #",
            "Contract Type",
            "Contract Start Date",
            "Contract Exp. Date",
            "Device Location",
        ]
        header_row = 8
        header_col = 0
        # formatting options for header
        header_format = workbook.add_format(
            {
                "bg_color": "#5b9bd5",
                "font_color": "#FFFFFF",
                "align": "center",
                "valign": "vcenter",
            }
        )
        worksheet.set_column(0, len(headers), 20)
        worksheet.set_row(header_row, 30)
        # write headers
        for heading in headers:
            worksheet.write(header_row, header_col, heading, header_format)
            header_col += 1
        rows_format = workbook.add_format(
            {"align": "center", "valign": "vcenter"}
        )
        data_row = 9
        for record in file_records:
            data_col = 0
            for header in headers:
                value = record.get(header)
                if header in ["Contract Start Date", "Contract Exp. Date"]:
                    if value:
                        value = convert_date_string_format(
                            value, settings.ISO_FORMAT, "%d-%b-%Y"
                        )
                worksheet.write(data_row, data_col, value, rows_format)
                data_col += 1
            data_row += 1
        workbook.close()
        return file_path

    @classmethod
    def generate_report(cls, context_data):
        # replace keys with space
        errors = []
        for error in context_data.get("device_import_result", {}).get(
            "errors"
        ):
            error = {k.replace(" ", "_"): v for k, v in error.items()}
            errors.append(error)
        context_data["device_import_result"]["errors"] = errors
        template_path = os.path.join(
            os.path.dirname(
                os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            ),
            "templates",
            "reports",
            "smartnet_receiving_error_report.html",
        )
        template = get_template(template_path)
        html_page = template.render(context_data)
        file_name = f"Smartnet Receiving logs {str(uuid.uuid4())}.pdf"
        TEMP_DIR = tempfile.gettempdir()
        file_path = os.path.join(TEMP_DIR, file_name)
        pdfkit.from_string(html_page, file_path)
        file_path = FileUploadService.upload_file(file_path)
        return file_path

    @classmethod
    def process(
        cls,
        provider: Provider,
        customer: Customer,
        user: User,
        payload: Dict,
        file_path: str,
    ):
        messages = []
        # load smartnet receiving settings
        try:
            smartnet_receiving_settings = cls.get_smartnet_receiving_settings(
                provider
            )
        except SmartnetReceivingSetting.DoesNotExist:
            msg = (
                f"Provider {provider} Smartnet Receiving Settings not configured. "
                f"Aborting Receiving process."
            )
            logger.debug(msg)
            messages.append(msg)
            return {}

        smartnet_receiving_partial_receive_status_settings = (
            cls.get_partial_receive_status_settings(provider)
        )
        if not smartnet_receiving_partial_receive_status_settings:
            msg = (
                f"Provider {provider} Smartnet Receiving Settings partial status not configured. "
                f"Aborting Receiving process."
            )
            logger.debug(msg)
            messages.append(msg)
            return {}

        field_mapping: Dict[str, str] = payload.get("field_mappings")
        field_mapping_obj = ColumnMapping(field_mapping)

        (
            validated_data,
            validation_result,
        ) = SmartnetReceivingDataValidationService.validate(
            provider, payload, file_path
        )
        if not validation_result.get(
            "device_import_validation_result", {}
        ).get("is_valid"):
            msg = "Device Error found in the sheet. Aborting Process."
            logger.info(msg)
            messages.append(msg)
            result = {
                "purchase_order_receiving_result": {"result": False},
                "device_import_result": {"result": False},
                "ticket_update_result": {"result": False},
                "email_notification_result": {"result": False},
                "tac_email_result": {"result": False},
                "event_logs": messages,
            }
            return result
        output = cls.update_purchase_order(provider, validated_data, payload)

        if output.result:
            messages.append("Purchase orders updated.")
            # bulk create devices
            manufacturer_id = payload.get("manufacturer_id")
            category_id = provider.get_device_category_id_by_manufacturer(
                manufacturer_id
            )
            # Device name and model number(mapped to product_id) should be the same (as per requirement)
            for device in validated_data:
                device["product_id"] = device.get(
                    field_mapping_obj.device_name_column
                )

            device_creation_output = cls.create_devices(
                provider,
                category_id,
                customer.crm_id,
                manufacturer_id,
                validated_data,
            )
            messages.append("Devices imported.")
            # Update service ticket
            ticket_update_output = False
            smartnet_complete_status = cls.get_smartnet_complete_status(
                provider
            )
            service_ticket_created_by = None
            ticket_details = []
            opportunity_name = None
            if not smartnet_complete_status:
                msg = "Smartnet Service Board settings not completed. Can not update Service Ticket"
                logger.debug(msg)
                messages.append(msg)
            else:
                try:
                    service_ticket_created_by = ServiceRequestModelMixin().get_created_by_for_service_ticket(
                        user
                    )
                except ValidationError:
                    msg = (
                        "Connectwise user not mapped for the user in the profile. "
                        "Can not update Service Ticket"
                    )
                    logger.debug(msg)
                    messages.append(msg)
            if all(
                [
                    smartnet_receiving_settings,
                    smartnet_complete_status,
                    service_ticket_created_by,
                ]
            ):
                purchase_order_ticket_mapping = payload.get(
                    "purchase_order_ticket_mapping"
                )
                for ticket_id in set(purchase_order_ticket_mapping.values()):
                    ticket_detail = cls.get_purchase_order_ticket_details(
                        provider.erp_client, ticket_id
                    )
                    # If we are selecting multiple PO’s they should all link to the same opportunity.
                    # If that is not the case take the first opportunity name from the first selected PO.
                    if not opportunity_name:
                        opportunity_name = ticket_detail.get(
                            "opportunity_name"
                        )
                    opportunity_id = ticket_detail.get("opportunity_id", "")
                    ticket_details.append(ticket_detail)
                    ticket_update_output = cls.update_service_ticket(
                        provider.erp_client,
                        ticket_detail["id"],
                        smartnet_complete_status,
                        smartnet_receiving_settings.smartnet_receiving_ticket_closing_note,
                        service_ticket_created_by,
                        file_path,
                    )
                    messages.append("Service Ticket Updated.")
            else:
                ticket_update_output = False
                msg = "Required settings missing, skipping Service Ticket update step."
                logger.info("msg")
                messages.append(msg)

            # SNT email Notification Step
            if payload.get("email_notification_meta"):
                send_customer_contact_email = payload.get(
                    "email_notification_meta"
                ).get("send_to_customer", True)
            else:
                send_customer_contact_email = False
            service_ticket_customer_contact_emails = {
                ticket_detail.get("contact_email")
                for ticket_detail in ticket_details
                if ticket_detail.get("contact_email") is not None
            }
            additional_email_recipients = payload.get(
                "email_notification_meta", {}
            ).get("extra_recipient_email", [])
            customer_output_file_path = cls.generate_customer_output_xlsx_file(
                customer.name,
                validated_data,
                field_mapping_obj,
                opportunity_name,
            )
            (
                email_notification_result,
                recipient_list,
            ) = cls.send_email_notification(
                provider,
                user,
                customer,
                smartnet_receiving_settings,
                opportunity_name,
                opportunity_id,
                send_customer_contact_email=send_customer_contact_email,
                service_ticket_customer_contact_emails=service_ticket_customer_contact_emails,
                additional_email_recipients=additional_email_recipients,
                customer_output_file=customer_output_file_path,
            )
            messages.append("Email notification sent.")

            # Send TAC email
            tac_email_result = False
            tac_email_metadata = {}
            if service_ticket_customer_contact_emails:
                for contact_email in service_ticket_customer_contact_emails:
                    cco_id = cls.get_customer_user_cco_id(
                        provider.id, customer.id, contact_email
                    )
                    # if not cco_id:
                    #     msg = (
                    #         f"Unable to send TAC Email. No CCO ID associated to customer user: "
                    #         f"{contact_email}"
                    #     )
                    #     messages.append(msg)
                    #     logger.debug(msg)
                    additional_contact_cco_ids = []
                    for email in additional_email_recipients:
                        _cco_id = cls.get_customer_user_cco_id(
                            provider.id, customer.id, email
                        )
                        if _cco_id:
                            additional_contact_cco_ids.append(_cco_id)
                    if cco_id:
                        additional_contact_cco_ids.append(cco_id)
                    all_cco_ids = additional_contact_cco_ids
                    if customer.is_ms_customer:
                        ms_contacts_cco_ids = payload.get(
                            "email_notification_meta", {}
                        ).get("ms_recipients_cco_ids", [])
                        if ms_contacts_cco_ids:
                            all_cco_ids.extend(ms_contacts_cco_ids)
                    contract_numbers = {
                        device.get(
                            field_mapping_obj.service_contract_number_column
                        )
                        for device in validated_data
                        if device.get(
                            field_mapping_obj.service_contract_number_column
                        )
                        is not None
                    }
                    contract_numbers = list(contract_numbers)
                    # msg = "TAC Email disabled temporarily."
                    # messages.append(msg)
                    # logger.debug(msg)
                    if contract_numbers and all_cco_ids:
                        (
                            tac_email_result,
                            tac_email_metadata,
                        ) = cls.send_tac_email(
                            smartnet_receiving_settings,
                            user.email,
                            all_cco_ids,
                            contract_numbers,
                            provider,
                        )
                        msg = "TAC email sent."
                        messages.append(msg)
                        logger.debug(msg)
                    else:
                        msg = "Unable to send TAC Email. Either contract numbers or CCOId not found"
                        messages.append(msg)
                        logger.debug(msg)
            else:
                msg = "Unable to send TAC Email. No customer contact found for the service ticket."
                messages.append(msg)
                logger.debug(msg)

            expiration_date = None
            if validated_data:
                expiration_date = validated_data[0].get(
                    field_mapping_obj.expiration_date_column
                )
            contact_id = None
            if ticket_details:
                contact_id = ticket_details[0].get("contact_id")

            result = {
                "purchase_order_receiving_result": {
                    "result": output.result,
                    "errors": output.errors,
                },
                "device_import_result": {
                    "result": True,
                    "meta_data": device_creation_output,
                    "errors": validation_result.get(
                        "device_import_validation_result", {}
                    ).get("validation_data"),
                },
                "ticket_update_result": {"result": ticket_update_output},
                "email_notification_result": {
                    "result": email_notification_result,
                    "recipient_list": recipient_list,
                },
                "tac_email_result": {
                    "result": tac_email_result,
                    **tac_email_metadata,
                },
                "event_logs": messages,
                "expiration_date": expiration_date,
                "contact_id": contact_id,
            }
        else:
            msg = "Purchase Order update failed"
            messages.append(msg)
            logger.info(msg)
            result = {
                "purchase_order_receiving_result": {
                    "result": output.result,
                    "errors": output.errors,
                },
                "device_import_result": {"result": False},
                "ticket_update_result": {"result": False},
                "email_notification_result": {"result": False},
                "tac_email_result": {"result": False},
                "event_logs": messages,
            }
        log_file = cls.generate_report(result)
        result.update({"log_file": log_file})
        return result

    @classmethod
    def test_snt_email(cls, to_email, email_body, provider):

        """Sends SNT Notification email to the specified to_email"""

        email_context = {
            "subject": f"Smartnet Delivery Notice",
            "body": email_body,
            "provider": provider,
        }
        try:
            SNTReceivingNotificationEmail(
                context=email_context,
            ).send(to=[to_email])
        except SMTPException as e:
            logger.error(f"SNT Receiving Notification email failed: {e}")
            return False, []
        logger.debug(f"SNT Receiving Notification email sent.")
        return True

    @classmethod
    def validate_create_device_input_data(
        cls, records: List[Dict], field_mapping: Dict
    ) -> Tuple[List[Dict], List, List]:
        """
        Validates the records, and returns the validated data, errors found

        """
        serializer = CreateDeviceValidateSerializer(
            data=records, many=True, context={"field_mapping": field_mapping}
        )
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data
