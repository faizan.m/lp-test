from core.exceptions import SmartnetReceivingSettingNotConfigured
from core.models import SmartnetReceivingSetting


class SmartnetReceivingSettingsMixin:
    def get_smartnet_receiving_settings(self, provider):
        try:
            smartnet_receiving_settings: SmartnetReceivingSetting = (
                provider.snt_receiving_settings
            )
        except SmartnetReceivingSetting.DoesNotExist:
            raise SmartnetReceivingSettingNotConfigured()
        # Check if all the properties are set
        if not all(smartnet_receiving_settings.__dict__.values()):
            raise SmartnetReceivingSettingNotConfigured()
        return smartnet_receiving_settings
