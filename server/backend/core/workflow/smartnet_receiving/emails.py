from accounts.mail import BaseEmailMessage


class SNTReceivingNotificationEmail(BaseEmailMessage):
    template_name = "emails/snt_receiving_notification.html"


class TACEmail(BaseEmailMessage):
    template_name = "emails/tac_email.html"
