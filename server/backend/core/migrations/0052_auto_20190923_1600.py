# Generated by Django 2.0.5 on 2019-09-23 16:00

from django.db import migrations


def create_scheduled_report_types(apps, schema_editor):
    ScheduledReportTypes = apps.get_model("core", "ScheduledReportTypes")
    scheduled_report_types = [
        ScheduledReportTypes(
            category="DEVICES", type="DEVICE_SUPPORT_30_DAYS", display_name="Device Support Ending in the next 30 days"
        ),
        ScheduledReportTypes(
            category="DEVICES", type="DEVICE_SUPPORT_60_DAYS", display_name="Device Support Ending in the next 60 days"
        ),
        ScheduledReportTypes(
            category="DEVICES", type="DEVICE_SUPPORT_90_DAYS", display_name="Device Support Ending in the next 90 days"
        ),
        ScheduledReportTypes(
            category="DEVICES", type="DEVICE_SUPPORT_365_DAYS",
            display_name="Devices coming End of Life in the next 12 months"
        ),
    ]
    ScheduledReportTypes.objects.bulk_create(scheduled_report_types)


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0051_auto_20190923_1600'),
    ]

    operations = [migrations.RunPython(create_scheduled_report_types)]
