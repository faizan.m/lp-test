# Generated by Django 2.0.5 on 2018-08-03 10:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_create_device_types_mapping'),
    ]

    operations = [
        migrations.CreateModel(
            name='CiscoMissingData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=100, unique=True)),
                ('retry_count', models.IntegerField(default=0)),
            ],
        ),
    ]
