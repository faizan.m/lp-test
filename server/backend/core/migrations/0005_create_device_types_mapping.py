# Generated by Django 2.0.5 on 2018-07-19 13:52

from django.db import migrations


class Migration(migrations.Migration):

    def call_command_to_create_device_types_mapping(apps, schema_editor):
        from django.core.management import call_command
        # call_command("create_device_types")
        pass

    def delete_all_device_types(apps, schema_editor):
        # DeviceTypes = apps.get_model("core", "DeviceTypes")
        # DeviceTypes.objects.all().delete()
        pass

    dependencies = [
        ('core', '0004_devicetypes'),
    ]

    operations = [
        # migrations.RunPython(call_command_to_create_device_types_mapping, delete_all_device_types),
    ]
