# Generated by Django 2.0.5 on 2019-07-30 08:50

from django.db import migrations


class Migration(migrations.Migration):
    def call_command_to_update_integrations_data(apps, schema_editor):
        from django.core.management import call_command

        call_command("update_cw_integration_configs")

    dependencies = [
        ('core', '0046_auto_20190711_1237'),
    ]

    operations = [
        # migrations.RunPython(
        #     # call_command_to_update_integrations_data
        # )
    ]

