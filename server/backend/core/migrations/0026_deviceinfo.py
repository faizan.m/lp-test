# Generated by Django 2.0.5 on 2018-10-25 10:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0045_auto_20181011_0535'),
        ('core', '0025_auto_20181025_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('device_crm_id', models.PositiveIntegerField(unique=True)),
                ('serial_number', models.CharField(blank=True, max_length=100, null=True)),
                ('instance_number', models.CharField(blank=True, max_length=100, null=True)),
                ('is_serial_instance_id', models.BooleanField(default=False)),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Provider')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
