# Generated by Django 2.0.5 on 2018-09-11 10:32

from django.db import migrations


def call_command_to_update_group_permissions(apps, schema_editor):
    from django.core.management import call_command
    call_command("updatepermissions")


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20180906_1439'),
    ]

    operations = [
        # migrations.RunPython(call_command_to_update_group_permissions)
    ]
