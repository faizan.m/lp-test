# Generated by Django 2.0.5 on 2021-11-11 07:28

from django.db import migrations


def update_device_mapping_values_as_list(apps, schema_editor):

    Provider = apps.get_model("accounts", "Provider")
    ProviderIntegration = apps.get_model("core", "ProviderIntegration")
    IntegrationType = apps.get_model("core", "IntegrationType")

    for provider in Provider.objects.all():
        try:
            logic_monitor_integration_id = IntegrationType.objects.get(
                name="LOGIC_MONITOR"
            ).category_id
            try:
                provider_integration = ProviderIntegration.objects.get(
                    provider=provider.id,
                    integration_type_id=logic_monitor_integration_id,
                )
                device_mappings = provider_integration.other_config.get("device_mappings")
                if device_mappings:
                    device_mapping = {}
                    for cw_mnf_id, mnf_mapping_values in device_mappings.items():
                        group_id_list = []
                        updated_group_dic = {}
                        group_dic = {}
                        for mnf_mapping_value in mnf_mapping_values:
                            if mnf_mapping_value.get("cw_mnf_id") == int(cw_mnf_id):
                                group_id_list.append(mnf_mapping_value.get("group_id"))
                            updated_group_dic.update(mnf_mapping_value)
                        updated_group_dic.pop("group_id")
                        group_dic["group_ids"] = group_id_list
                        group_dic.update(updated_group_dic)
                        device_mapping.update({cw_mnf_id: [group_dic]})
                    if device_mapping:
                        provider_integration.other_config.get("device_mappings").update(
                            device_mapping
                        )
                        provider_integration.save()
            except ProviderIntegration.DoesNotExist:
                pass
        except IntegrationType.DoesNotExist:
            pass


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_auto_20211102_1226'),
    ]

    operations = [
        migrations.RunPython(
            update_device_mapping_values_as_list,
        )
    ]
