from django.conf import settings
from django.core.validators import URLValidator, FileExtensionValidator
from django.db.models import Q
from model_utils import Choices
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from accounts.models import Provider, Customer, Address, AccountingContact
from accounts.services import CustomerService
from core.models import (
    ConnectwisePhoneNumberMappingSetting,
    IntegrationType,
    IntegrationCategory,
    ManualDeviceTypeMapping,
    ProviderIntegration,
    ScheduledReportTypes,
    UserScheduledReport,
    SubscriptionProductMapping,
    SubscriptionProductType,
    ReportDownloadStats,
    SubscriptionServiceProviderMapping,
    AutomatedSubscriptionNotificationSetting,
    ConfigurationMapping,
)
from core.services import IntegrationService
from core.utils import url_validation_regex, device_date_formats
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = "__all__"


class AccountingContactSerializer(serializers.ModelSerializer):
    def validate_email(self, email):
        if email:
            email = email.lower()
        return email

    class Meta:
        model = AccountingContact
        fields = "__all__"


class ProviderSerializerValidations(object):
    def validate_email(self, email):
        if email is None:
            return email
        if email:
            email = email.lower()
        return email

    def validate_url(self, url):
        return self.validate_url_alias(url)

    def validate_company_url(self, url):
        if url is None:
            return url
        try:
            validate = URLValidator()
            validate(url)
        except ValidationError as e:
            raise ValidationError("Please provide a valid URL.")
        return url

    def validate_url_alias(self, url):
        if url is None:
            return url
        url = url.lower()
        if url in settings.RESTRICTED_URLS:
            raise ValidationError("This URL is already taken.")
        if not url_validation_regex(url):
            raise ValidationError("Please provide a valid URL.")
        # This additional check is to ensure that url and url alias are unique across providers.
        queryset = Provider.objects.filter(Q(url=url) | Q(url_alias=url))
        request = self.context.get("request")
        # This check is for admin creating providers.
        if request.provider:
            provider_id = request.provider.id
            queryset = queryset.exclude(id=provider_id)
        if queryset.exists():
            raise ValidationError("Not available. Already in use.")
        return url


class ProviderSerializer(
    ProviderSerializerValidations, serializers.ModelSerializer
):
    address = AddressSerializer()
    accounting_contact = AccountingContactSerializer()

    class Meta:
        model = Provider
        fields = [
            "id",
            "address",
            "accounting_contact",
            "name",
            "url",
            "url_alias",
            "ticket_note",
            "email",
            "is_active",
            "logo",
            "colour_code",
            "support_contact",
            "http_protocol",
            "sub_domain",
            "is_configured",
            "created_on",
            "updated_on",
            "is_two_fa_enabled",
            "timezone",
        ]
        read_only_fields = ["url_alias"]

    def update(self, instance, validated_data):
        address = validated_data.pop("address", {})
        accounting_contact = validated_data.pop("accounting_contact", {})
        if address:
            Address.objects.filter(id=instance.address.id).update(**address)
        if accounting_contact:
            AccountingContact.objects.filter(
                id=instance.accounting_contact.id
            ).update(**accounting_contact)

        Provider.objects.filter(id=instance.id).update(**validated_data)
        instance = Provider.objects.get(id=instance.id)
        return instance


class ProviderUpdateSerializer(ProviderSerializer):
    class Meta:
        model = Provider
        fields = [
            "id",
            "address",
            "accounting_contact",
            "name",
            "url",
            "url_alias",
            "ticket_note",
            "email",
            "is_active",
            "logo",
            "colour_code",
            "support_contact",
            "http_protocol",
            "sub_domain",
            "is_configured",
            "created_on",
            "updated_on",
            "is_two_fa_enabled",
            "timezone",
        ]
        read_only_fields = ["url", "url_alias", "logo", "colour_code"]


class ProviderSelfUpdateSerializer(
    ProviderSerializerValidations, serializers.ModelSerializer
):
    class Meta:
        model = Provider
        fields = [
            "id",
            "address",
            "accounting_contact",
            "name",
            "url",
            "url_alias",
            "ticket_note",
            "email",
            "is_active",
            "logo",
            "colour_code",
            "support_contact",
            "http_protocol",
            "sub_domain",
            "is_configured",
            "created_on",
            "updated_on",
            "customer_instance_id",
            "company_url",
            "enable_url",
            "is_two_fa_enabled",
            "timezone",
        ]
        read_only_fields = [
            "id",
            "address",
            "accounting_contact",
            "name",
            "url",
            "email",
            "is_active",
            "http_protocol",
            "sub_domain",
            "is_configured",
            "created_on",
            "updated_on",
        ]


class ProviderInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = ["logo", "colour_code"]


class CustomerSerializer(serializers.ModelSerializer):
    address = AddressSerializer(required=False, read_only=True)

    class Meta:
        model = Customer
        fields = [
            "id",
            "address",
            "created_on",
            "updated_on",
            "crm_id",
            "name",
            "phone",
            "primary_contact",
            "is_active",
            "provider",
            "renewal_service_allowed",
            "is_ms_customer",
            "collector_service_allowed",
            "config_compliance_service_allowed",
            "config_compliance_service_write_allowed",
            "customer_order_visibility_allowed",
        ]
        read_only_fields = [
            "crm_id",
            "name",
            "provider",
            "address",
            "phone",
            "primary_contact",
            "renewal_service_allowed",
            "is_ms_customer",
            "collector_service_allowed",
            "config_compliance_service_allowed",
            "config_compliance_service_write_allowed",
        ]


class CustomerDetailsSerializer(CustomerSerializer):
    last_logged_in_user = serializers.SerializerMethodField(required=False)

    def get_last_logged_in_user(self, customer):
        last_logged_in_user = dict()
        user = CustomerService.get_most_recent_logged_in_user(customer.id)
        if user:
            last_logged_in_user = dict(
                name=user.name, last_login=user.last_login
            )
        return last_logged_in_user

    class Meta:
        model = Customer
        fields = [
            "id",
            "address",
            "created_on",
            "updated_on",
            "crm_id",
            "name",
            "phone",
            "primary_contact",
            "is_active",
            "provider",
            "users_count",
            "devices_count",
            "renewal_service_allowed",
            "last_logged_in_user",
            "network_note",
            "customer_distribution",
            "is_ms_customer",
            "collector_service_allowed",
            "config_compliance_service_allowed",
            "config_compliance_service_write_allowed",
            "customer_order_visibility_allowed",
        ]
        read_only_fields = [
            "crm_id",
            "name",
            "provider",
            "address",
            "phone",
            "primary_contact",
            "users_count",
            "devices_count",
        ]


class CustomerDetailsUpdateSerializer(CustomerSerializer):
    class Meta:
        model = Customer
        fields = ["network_note", "customer_distribution"]


class MSCustomerDetailsSerializer(CustomerDetailsSerializer):
    class Meta:
        model = Customer
        fields = [
            "id",
            "address",
            "created_on",
            "updated_on",
            "crm_id",
            "name",
            "phone",
            "primary_contact",
            "is_active",
            "provider",
            "users_count",
            "devices_count",
            "renewal_service_allowed",
            "last_logged_in_user",
            "network_note",
            "is_ms_customer",
            "collector_service_allowed",
            "config_compliance_service_allowed",
            "config_compliance_service_write_allowed",
            "customer_order_visibility_allowed",
        ]


class MSCustomerDetailsUpdateSerializer(CustomerDetailsUpdateSerializer):
    class Meta:
        model = Customer
        fields = ["network_note"]


class CustomerDropdownSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = [
            "id",
            "name",
            "crm_id",
            "account_manager_id",
            "account_manager_name",
        ]


class IntegrationCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = IntegrationCategory
        fields = ["id", "name"]
        read_only_fields = ["name"]


class IntegrationsListSerializer(serializers.ModelSerializer):
    pass


class ProviderIntegrationSerializer(serializers.ModelSerializer):
    def validate_authentication_info(self, attrs):
        super().validate(attrs)
        request = self.context.get("request")
        if request.method == "POST":
            t = ProviderIntegration.objects.filter(
                provider_id=request.provider.id, authentication_info=attrs
            ).exists()
        elif request.method == "PUT" or "PATCH":
            integration_id = request.parser_context["kwargs"]["pk"]
            t = (
                ProviderIntegration.objects.exclude(id=integration_id)
                .filter(
                    provider_id=request.provider.id, authentication_info=attrs
                )
                .exists()
            )
        if t:
            raise ValidationError(
                "Authentication config with the same credentials already exists."
            )
        return attrs

    class Meta:
        model = ProviderIntegration
        fields = [
            "id",
            "provider",
            "integration_type",
            "authentication_info",
            "other_config",
            "type",
            "category",
        ]
        read_only_fields = ["provider"]


class ERPIntegrationSerializer(ProviderIntegrationSerializer):
    def validate_integration_type(self, attrs):
        request = self.context.get("request")
        if request.method == "POST":
            if request.provider.integration_statuses.get(
                "crm_authentication_configured"
            ):
                raise ValidationError(
                    "You can not configure more than one Connectwise Integration."
                )
        return attrs

    # def validate_other_config(self, attrs):
    #     manufacturer_mapping = attrs.get("manufacturer_mapping")
    #     if manufacturer_mapping and not manufacturer_mapping.get("default"):
    #         raise ValidationError("Manufacturer Mapping needs to be set.")
    #     return attrs


class LogicMonitorIntegrationSerializer(ProviderIntegrationSerializer):
    def validate_integration_type(self, attrs):
        request = self.context.get("request")
        if request.method == "POST":
            lm_integration = (
                IntegrationService.get_dropbox_storage_integration_type_object()
            )
            if (
                attrs == lm_integration
                and ProviderIntegration.objects.filter(
                    provider_id=request.provider.id,
                    integration_type=lm_integration,
                ).exists()
            ):
                raise ValidationError(
                    "You can not configure more than one LogicMonitor Integration."
                )
        return attrs

    def validate_other_config(self, attrs):
        device_mappings = attrs.get("device_mappings")
        if device_mappings is None:
            raise ValidationError(
                "Manufacturer Mapping for at least one Group is mandatory."
            )
        config_mappings = attrs.get("config_mappings", {}).get("config_names")
        if config_mappings is None:
            raise ValidationError(
                "You need to specify at least one Config Name."
            )
        fields = {"group_ids", "system_categories", "cw_mnf_name"}
        for key, values in device_mappings.items():
            for value in values:
                if not isinstance(value, dict):
                    raise ValidationError(
                        f"Manufacturer Mapping for {key} is empty."
                    )
                attrs_fields = set(value.keys())
                diff = fields.difference(attrs_fields)
                if diff:
                    raise ValidationError(
                        f"Missing fields: {', '.join(diff)} for Manufacturer: {key}"
                    )
            if not isinstance(config_mappings, list):
                raise ValidationError(
                    "You need to specify the list of Config Names in a list. Example: ['Cisco_IOS','IOS_Config'] "
                )
            return attrs


class DropBoxIntegrationSerializer(ProviderIntegrationSerializer):
    def validate_integration_type(self, attrs):
        request = self.context.get("request")
        if request.method == "POST":
            if ProviderIntegration.objects.filter(
                provider_id=request.provider.id, integration_type=attrs
            ).exists():
                raise ValidationError(
                    "You can not configure more than one DropBox Integration."
                )
        return attrs


class DeviceCategoryIdsSerializer(serializers.Serializer):
    device_category_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True
    )


class EmptySerialMappingSerializer(serializers.Serializer):
    empty_serial_mapping = DeviceCategoryIdsSerializer(
        many=False, required=True
    )


class DeviceSerializer(serializers.Serializer):
    id = serializers.IntegerField(min_value=0, read_only=True, required=False)
    device_name = serializers.CharField(max_length=300, required=True)
    category = serializers.CharField(
        max_length=300, required=False, allow_null=True, allow_blank=True
    )
    service_contract_number = serializers.CharField(
        max_length=300, required=False, allow_blank=True, allow_null=True
    )
    site_id = serializers.IntegerField(
        min_value=0, required=False, allow_null=True
    )
    status_id = serializers.IntegerField(
        min_value=0, required=False, allow_null=True
    )
    location = serializers.CharField(
        max_length=300, required=False, allow_blank=True, allow_null=True
    )
    location_id = serializers.IntegerField(
        min_value=0, required=False, allow_null=True
    )
    serial_number = serializers.CharField(
        max_length=300, required=False, allow_blank=True, allow_null=True
    )
    manufacturer_id = serializers.CharField(required=True)
    instance_id = serializers.CharField(required=True)
    customer_notes = serializers.CharField(
        max_length=1000, required=False, allow_blank=True, allow_null=True
    )
    asset_tag = serializers.CharField(
        max_length=25, required=False, allow_blank=True, allow_null=True
    )
    installation_date = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%SZ", required=False
    )


class DeviceUpdateSeralizer(DeviceSerializer):
    installation_date = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%SZ", read_only=True
    )

    class Meta:
        read_only_fields = [
            "category",
            "category_id",
            "serial_number",
            "model_number",
            "tag_number",
            "manufacturer_id",
        ]


class ProviderDeviceUpdateSerializer(DeviceUpdateSeralizer):
    notes = serializers.CharField(
        max_length=1000, required=False, allow_blank=True, allow_null=True
    )


class ProviderDeviceSerializer(DeviceSerializer):
    notes = serializers.CharField(
        max_length=1000, required=False, allow_blank=True, allow_null=True
    )


class DeviceFieldMappingSerializer(serializers.Serializer):
    # Serial number can be empty/null for some configuration types
    serial_number = serializers.CharField(required=True, allow_null=True)
    device_name = serializers.CharField(required=True)
    service_contract_number = serializers.CharField(
        required=True, allow_null=True
    )
    purchase_date = serializers.CharField(required=True, allow_null=True)
    purchase_date_format = serializers.CharField(
        required=True, allow_null=True
    )
    expiration_date = serializers.CharField(required=True, allow_null=True)
    expiration_date_format = serializers.CharField(
        required=True, allow_null=True
    )
    installation_date = serializers.CharField(required=True, allow_null=True)
    installation_date_format = serializers.CharField(
        required=True, allow_null=True
    )
    device_price = serializers.CharField(required=True, allow_null=True)
    product_type = serializers.CharField(required=True, allow_null=True)
    address = serializers.CharField(required=True)
    instance_number = serializers.CharField(required=False, allow_null=True)
    configuration_type = serializers.CharField(required=False, allow_null=True)
    unique_identifier = serializers.CharField(required=False, allow_null=True)

    def validate(self, attrs):
        super().validate(attrs)
        if attrs.get("purchase_date") and not attrs.get(
            "purchase_date_format"
        ):
            raise ValidationError(
                {
                    "purchase_date_format": [
                        "Please do select the purchase date format"
                    ]
                }
            )
        if attrs.get("expiration_date") and not attrs.get(
            "expiration_date_format"
        ):
            raise ValidationError(
                {
                    "expiration_date_format": [
                        "Please do select the expiration date format"
                    ]
                }
            )
        if attrs.get("installation_date") and not attrs.get(
            "installation_date_format"
        ):
            raise ValidationError(
                {
                    "installation_date_format": [
                        "Please do select the installation date format"
                    ]
                }
            )
        return attrs


class DeviceImportTypeMetaSerializer(serializers.Serializer):
    product_type = serializers.CharField(required=True)
    include = serializers.BooleanField(required=True)
    ignore_zero_dollar_items = serializers.BooleanField(required=True)
    include_non_serials = serializers.BooleanField(required=True)


class DeviceImportAddressMetaSerializer(serializers.Serializer):
    address = serializers.CharField(required=True)
    site_id = serializers.IntegerField(required=True)


class DeviceImportFileMetaInfo(serializers.Serializer):
    file_name = serializers.CharField(required=True)
    field_mappings = DeviceFieldMappingSerializer(many=False)
    manufacturer_id = serializers.CharField(required=True)
    ignore_zero_dollar_items = serializers.BooleanField(required=True)
    filter_by_product_type = serializers.BooleanField(required=True)
    device_type_filter_meta = DeviceImportTypeMetaSerializer(many=True)
    address_field_meta = DeviceImportAddressMetaSerializer(many=True)
    configuration_types = serializers.JSONField(default={})

    def validate(self, attrs):
        super().validate(attrs)
        errors = {}
        if attrs.get("ignore_zero_dollar_items") and not attrs.get(
            "field_mappings"
        ).get("device_price"):
            errors["device_price"] = ["Device Price field mapping missing"]
        if attrs.get("filter_by_product_type") and not attrs.get(
            "field_mappings"
        ).get("product_type"):
            errors["product_type"] = ["Product Type field mapping missing"]
        if errors:
            raise ValidationError(errors)
        return attrs


class DeviceContractUpdateSerializer(serializers.Serializer):
    device_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )
    value = serializers.CharField(max_length=1000)


class DeviceCategoryUpdateSerializer(DeviceContractUpdateSerializer):
    pass


class DeviceStatusUpdateSerializer(serializers.Serializer):
    device_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )
    value = serializers.IntegerField()

    def validate_value(self, attrs):
        request = self.context.get("request")
        erp_client = request.provider.erp_client
        if attrs not in [
            erp_client.device_active_status_id,
            erp_client.device_inactive_status_id,
        ]:
            raise ValidationError("Requested value is invalid")
        return attrs


class DeviceSiteUpdateSerializer(serializers.Serializer):
    device_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )
    value = serializers.IntegerField()


class DeviceManufacturerUpdateSerializer(serializers.Serializer):
    device_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )
    value = serializers.IntegerField()


class DeviceIDListSerializer(serializers.Serializer):
    device_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )


class SiteSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=500)
    address_line_1 = serializers.CharField(
        max_length=1000, required=False, allow_blank=True, allow_null=True
    )
    address_line_2 = serializers.CharField(
        max_length=1000, required=False, allow_blank=True, allow_null=True
    )
    city = serializers.CharField(
        max_length=300, required=False, allow_blank=True, allow_null=True
    )
    state_id = serializers.IntegerField(allow_null=True, required=False)
    country_id = serializers.IntegerField()
    zip = serializers.CharField(
        max_length=10, required=False, allow_blank=True, allow_null=True
    )
    phone_number = serializers.CharField(
        max_length=20, required=False, allow_blank=True, allow_null=True
    )


class BulkCreateSitesSerializer(SiteSerializer):
    pass


class ContractStatusSerializer(serializers.Serializer):
    contract_status_id = serializers.IntegerField()
    contract_status = serializers.CharField(max_length=100)


class ReportsFiltersSerializer(serializers.Serializer):
    device_type = serializers.ListField(
        child=serializers.CharField(allow_null=True, allow_blank=True),
        allow_null=True,
        required=False,
    )
    site_id = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=False
    )

    contract_status_id = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=False
    )

    customer_id = serializers.ListField(
        child=serializers.IntegerField(), required=False, allow_empty=False
    )


class LifeCycleManagementReportSerializer(ReportsFiltersSerializer):
    report_type = serializers.JSONField(required=True)
    report_format = serializers.CharField(required=False)


class ReportBundleSerializer(serializers.Serializer):
    customer_id = serializers.ListField(
        child=serializers.IntegerField(), required=True, allow_empty=False
    )


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        fields = self.context["request"].query_params.get("fields")
        if fields:
            fields = fields.split(",")
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class IntegrationTypeSerializer(
    DynamicFieldsModelSerializer, serializers.ModelSerializer
):
    class Meta:
        model = IntegrationType
        fields = ["id", "name", "authentication_config"]
        read_only_fields = ["id", "name", "authentication_config"]


class CustomerNoteSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)
    flagged = serializers.BooleanField(required=True)
    note_type_id = serializers.IntegerField(required=True)


class DeviceCircuitInfoAssociationCreateSerializer(serializers.Serializer):
    circuit_info_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0)
    )
    device_crm_id = serializers.IntegerField()


class DeviceMonitoringAuthSerializer(serializers.Serializer):
    access_key = serializers.CharField(required=True)
    access_id = serializers.CharField(required=True)
    name = serializers.CharField(required=True)


class SystemTerritoryManagerUpdate(serializers.Serializer):
    manager_id = serializers.IntegerField()


class ImageUploadSerializer(serializers.Serializer):
    filename = serializers.ImageField(
        validators=[FileExtensionValidator(["png", "jpeg", "jpg"])]
    )


class SystemUploadAttachmentSerializer(serializers.Serializer):
    file_validators = [
        FileExtensionValidator(
            [
                "png",
                "jpeg",
                "jpg",
                "pdf",
                "txt",
                "text",
                "mpp",
                "doc",
                "docx",
                "docm",
                "xls",
                "xlsx",
                "ppt",
                "zip",
                "csv",
                "dxf",
                "psd",
                "vdx",
                "vsd",
                "vsdx",
                "pptx",
                "pptm",
            ]
        )
    ]
    RECORD_TYPES = Choices(("Opportunity", "Opportunity"))
    record_id = serializers.IntegerField()
    record_type = serializers.ChoiceField(choices=RECORD_TYPES)
    files = serializers.ListField(
        child=serializers.FileField(validators=file_validators), min_length=1
    )


class ScheduledReportTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduledReportTypes
        fields = "__all__"


class UserScheduledCreateUpdateReportSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        frequency = attrs.get("frequency")
        next_run = UserScheduledReport.get_next_run(frequency)
        attrs.update({"next_run": next_run})
        return attrs

    class Meta:
        model = UserScheduledReport
        exclude = ["user"]


class UserScheduledReportDetailSerializer(serializers.ModelSerializer):
    scheduled_report_type = ScheduledReportTypesSerializer(required=True)

    class Meta:
        model = UserScheduledReport
        exclude = ["user"]


class SubscriptionProductsSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(source="type.name", read_only=True)

    class Meta:
        model = SubscriptionProductMapping
        fields = ["id", "name", "type", "type_name"]


class SubscriptionProductTypeSerializer(serializers.ModelSerializer):
    products = SubscriptionProductsSerializer("products", many=True)

    def validate_name(self, name):
        request = self.context.get("request")
        if SubscriptionProductType.objects.filter(
            provider=request.provider, name=name
        ):
            raise ValidationError("Product with the same name already exists.")
        return name

    class Meta:
        model = SubscriptionProductType
        exclude = ["provider"]


class SubscriptionProductMappingCreateSerializer(serializers.Serializer):
    products = serializers.ListField(
        child=serializers.CharField(max_length=150)
    )
    name = serializers.CharField(max_length=150)

    def validate_name(self, name):
        request = self.context.get("request")
        if SubscriptionProductType.objects.filter(
            provider=request.provider, name=name
        ):
            raise ValidationError("Product with the same name already exists.")
        return name


class SubscriptionProductMappingUpdateSerializer(serializers.Serializer):
    products = serializers.ListField(
        child=serializers.CharField(max_length=150)
    )
    name = serializers.CharField(max_length=150)

    def validate_name(self, name):
        request = self.context.get("request")
        product_id = request.parser_context["kwargs"]["pk"]
        if SubscriptionProductType.objects.exclude(id=product_id).filter(
            provider=request.provider, name=name
        ):
            raise ValidationError("Product with the same name already exists.")
        return name


class SubscriptionFieldMappingSerializer(serializers.Serializer):
    subscription_id = serializers.CharField(required=True)
    subscription_name = serializers.CharField(required=True)
    end_customer = serializers.CharField(required=True)
    contract_number = serializers.CharField(required=False, allow_null=True)
    expiration_date = serializers.CharField(required=True, allow_null=True)
    expiration_date_format = serializers.CharField(
        required=True, allow_null=True
    )
    start_date = serializers.CharField(required=True, allow_null=True)
    start_date_format = serializers.CharField(required=True, allow_null=True)
    term_length = serializers.CharField(required=False)
    auto_renewal_term = serializers.CharField(required=False)
    licence_qty = serializers.CharField(required=False)
    model = serializers.CharField(required=False)
    status = serializers.CharField(required=False)
    billing_model = serializers.CharField(required=False)
    true_forward_date = serializers.CharField(required=False)
    true_forward_date_format = serializers.CharField(required=False)

    def validate(self, attrs):
        super().validate(attrs)
        if attrs.get("expiration_date") and not attrs.get(
            "expiration_date_format"
        ):
            raise ValidationError(
                {
                    "expiration_date_format": [
                        "Please do select the expiration date format"
                    ]
                }
            )
        if attrs.get("expiration_date") and attrs.get(
            "expiration_date_format"
        ):
            if (
                attrs.get("expiration_date_format")
                not in device_date_formats().values()
            ):
                raise ValidationError(
                    {"expiration_date_format": ["Invalid Date format"]}
                )
        if attrs.get("start_date") and not attrs.get("start_date_format"):
            raise ValidationError(
                {
                    "start_date_format": [
                        "Please do select the start date format"
                    ]
                }
            )
        if attrs.get("start_date") and attrs.get("start_date_format"):
            if (
                attrs.get("start_date_format")
                not in device_date_formats().values()
            ):
                raise ValidationError(
                    {"start_date_format": ["Invalid Date format"]}
                )
        if attrs.get("true_forward_date") and not attrs.get(
            "true_forward_date_format"
        ):
            raise ValidationError(
                {
                    "true_forward_date_format": [
                        "Please do select the True Forward Date Forward"
                    ]
                }
            )
        if attrs.get("true_forward_date") and attrs.get(
            "true_forward_date_format"
        ):
            if (
                attrs.get("true_forward_date_format")
                not in device_date_formats().values()
            ):
                raise ValidationError(
                    {"true_forward_date_format": ["Invalid Date format"]}
                )
        return attrs


class SubscriptionImportSerializer(serializers.Serializer):
    file_name = serializers.CharField(required=True)
    field_mappings = SubscriptionFieldMappingSerializer(many=False)
    manufacturer_id = serializers.CharField(required=True)
    customer_name_mapping = serializers.JSONField(required=True)
    status_mapping = serializers.JSONField(required=False)


class SusbscriptionGetSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    category_name = serializers.CharField(max_length=300, required=False)
    status = serializers.CharField(max_length=100, required=False)
    customer_id = serializers.IntegerField()
    customer_name = serializers.CharField(max_length=300, required=False)
    location_id = serializers.IntegerField(required=False)
    model_number = serializers.CharField(max_length=300, required=False)
    purchase_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )
    manufacturer_name = serializers.CharField(max_length=300, required=False)
    contract_status = ContractStatusSerializer()
    product_id = serializers.IntegerField(required=False)
    product_name = serializers.CharField(max_length=300, required=False)
    type_name = serializers.CharField(max_length=300, required=False)
    name = serializers.CharField(max_length=300, required=False)
    site_id = serializers.IntegerField(required=False)
    device_name = serializers.CharField(max_length=300, required=False)
    serial_number = serializers.CharField(max_length=300, required=False)
    category_id = serializers.IntegerField()
    service_contract_number = serializers.CharField(
        max_length=300, required=False
    )
    status_id = serializers.IntegerField(min_value=0, required=False)
    manufacturer_id = serializers.IntegerField(required=False)
    installation_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )
    expiration_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )
    term_length = serializers.IntegerField(
        required=False, max_value=90, min_value=1
    )
    auto_renewal_term = serializers.FloatField(
        required=False, max_value=60, min_value=1
    )
    licence_qty = serializers.IntegerField(required=False)
    billing_model = serializers.CharField(required=False)
    notes = serializers.CharField(required=False, allow_blank=True)
    true_forward_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )


class SusbscriptionGetSerializerForCustomerUser(SusbscriptionGetSerializer):
    notes = None


class SusbscriptionUpdateSerializer(serializers.Serializer):
    device_name = serializers.CharField(max_length=300, required=False)
    serial_number = serializers.CharField(max_length=300, required=False)
    category_id = serializers.IntegerField()
    service_contract_number = serializers.CharField(
        max_length=300, required=False, allow_blank=True, allow_null=True
    )
    status_id = serializers.IntegerField(min_value=0, required=False)
    manufacturer_id = serializers.CharField(required=False)
    installation_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )
    expiration_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )
    term_length = serializers.IntegerField(
        required=False, max_value=90, min_value=1
    )
    auto_renewal_term = serializers.FloatField(
        required=False, allow_null=True, max_value=60, min_value=1
    )
    licence_qty = serializers.IntegerField(required=False, allow_null=True)
    billing_model = serializers.CharField(required=False, allow_null=True)
    notes = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    true_forward_date = serializers.DateTimeField(
        format=settings.ISO_FORMAT, required=False
    )


class SusbscriptionUpdateSerializerForCustomerUser(
    SusbscriptionUpdateSerializer
):
    notes = None


class ReportDownloadStatsSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    name = serializers.CharField()
    download_count = serializers.IntegerField()
    last_downloaded_on = serializers.DateTimeField()

    class Meta:
        model = ReportDownloadStats
        fields = "__all__"


class ManualDeviceTypeMappingSerializer(serializers.ModelSerializer):
    def validate_type(self, type):
        provider = self.context.get("request").provider
        type_exist = ManualDeviceTypeMapping.objects.filter(
            type=type, provider=provider
        ).exists()
        if type_exist:
            raise ValidationError("Type already exists.")
        return type.strip().upper()

    class Meta:
        model = ManualDeviceTypeMapping
        fields = "__all__"
        read_only_fields = ("provider",)


class SubscriptionServiceProviderMappingListCreateSerializer(
    serializers.ModelSerializer
):
    def validate_service_provider_name(self, service_provider_name):
        provider = self.context.get("request").provider
        type_exist = SubscriptionServiceProviderMapping.objects.filter(
            service_provider_name=service_provider_name.upper(),
            provider=provider,
        ).exists()
        if type_exist:
            raise ValidationError("Service Provider mapping already exists.")
        return service_provider_name.strip().upper()

    class Meta:
        model = SubscriptionServiceProviderMapping
        fields = "__all__"
        read_only_fields = ("provider",)


class SubscriptionServiceProviderMappingUpdateDestroySerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = SubscriptionServiceProviderMapping
        fields = "__all__"
        read_only_fields = ("provider",)


class ConnectwisePhoneNumberMappingSettingSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = ConnectwisePhoneNumberMappingSetting
        fields = ("cell_phone", "office_phone", "last_updated_on")


class VendorTypeCustomerCreateSerializer(serializers.Serializer):
    territory_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )
    customer_site_name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    customer_identifier = serializers.CharField(
        required=True, max_length=15, allow_null=False, allow_blank=False
    )
    vendor_name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    address_line_1 = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    address_line_2 = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    city = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    state = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    zip = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    website = serializers.CharField(
        required=False, max_length=255, allow_null=True, allow_blank=True
    )


class VendorContactCreateSerializer(serializers.Serializer):
    customer_crm_id = serializers.IntegerField(required=True, allow_null=False)
    contact_type_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )
    first_name = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )
    last_name = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )
    title = serializers.CharField(allow_blank=True, allow_null=True)
    office_phone_number = serializers.CharField(
        required=True, allow_blank=True, allow_null=True
    )
    email = serializers.CharField(
        required=True, allow_blank=True, allow_null=True
    )


class TeamRoleCreateSerializer(serializers.Serializer):
    role_name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )


class AutomatedSubscriptionNotificationSettingSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = AutomatedSubscriptionNotificationSetting
        fields = (
            "provider",
            "receiver_email_ids",
            "sender",
            "email_subject",
            "email_body",
            "renewal_type_contact_crm_id",
            "weekly_send_schedule",
            "approaching_expiry_date_range",
            "config_type_crm_ids",
            "config_status_crm_ids",
            "manufacturer_crm_ids",
        )
        read_only_fields = ("id", "provider")


class ConfigurationMappingSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(
        max_length=150, allow_null=False, allow_blank=False
    )
    config_type_crm_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        allow_null=False,
        allow_empty=False,
    )

    def create(self, validated_data):
        mapping: ConfigurationMapping = ConfigurationMapping.objects.create(
            **validated_data
        )
        return mapping

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()
        return instance


class SubscriptionNotificationTriggerSerializer(serializers.Serializer):
    all_customers = serializers.BooleanField(required=True)
    customer_crm_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True, required=True
    )
