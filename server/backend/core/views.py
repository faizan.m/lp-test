import copy
import datetime
import os

import celery
import pytz
from celery.result import AsyncResult
from django.conf import settings
from django.db.models import (
    Count,
    Max,
    Value,
    CharField,
    IntegerField,
    DateTimeField,
)
from django.db.models.functions import Concat
from django_downloadview import HTTPDownloadView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status, filters
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import (
    JSONParser,
    MultiPartParser,
    FormParser,
    BaseParser,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import (
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)

from accounts.custom_permissions import (
    CustomPermissions,
    IsProviderOwner,
    IsCustomerUser,
    IsAcelavarUser,
    IsProviderUser,
    IsProviderOwnerOrAdmin,
    IsProviderOrCustomerUser,
)
from accounts.models import (
    Provider,
    Address,
    AccountingContact,
    UserType,
    Customer,
    CircuitInfo,
)
from accounts.serializers import CircuitInfoSerializer
from accounts.tasks import send_customer_distribution_update_email
from caching.service import CacheService
from core import utils
from core.integrations import Connectwise
from core.mixins import ProviderIntegrationSerializerMixin
from core.models import (
    ConnectwisePhoneNumberMappingSetting,
    IntegrationCategory,
    ManualDeviceTypeMapping,
    ProviderIntegration,
    IntegrationType,
    DeviceManufacturerData,
    DeviceTypes,
    ScheduledReportTypes,
    UserScheduledReport,
    SubscriptionProductMapping,
    SubscriptionProductType,
    SubscriptionData,
    ReportDownloadStats,
    SubscriptionServiceProviderMapping,
    AutomatedSubscriptionNotificationSetting,
    ConfigurationMapping,
)
from core.pagination import CustomPagination
from core.renderers import (
    DeviceReportCSVRenderer,
    DictToCSVRenderer,
    FinanceReportCSVRenderer,
)
from core.reports import generate_customer_device_management_report
from core.serializers import (
    ConnectwisePhoneNumberMappingSettingSerializer,
    ManualDeviceTypeMappingSerializer,
    ProviderSerializer,
    CustomerSerializer,
    IntegrationCategorySerializer,
    DeviceSerializer,
    CustomerDropdownSerializer,
    DeviceContractUpdateSerializer,
    CustomerDetailsSerializer,
    SiteSerializer,
    ContractStatusSerializer,
    ReportsFiltersSerializer,
    ProviderSelfUpdateSerializer,
    IntegrationTypeSerializer,
    ProviderInfoSerializer,
    DeviceImportFileMetaInfo,
    DeviceStatusUpdateSerializer,
    ProviderUpdateSerializer,
    LifeCycleManagementReportSerializer,
    CustomerNoteSerializer,
    CustomerDetailsUpdateSerializer,
    DeviceCircuitInfoAssociationCreateSerializer,
    DeviceMonitoringAuthSerializer,
    DeviceCategoryUpdateSerializer,
    DeviceManufacturerUpdateSerializer,
    DeviceSiteUpdateSerializer,
    ProviderDeviceSerializer,
    SystemTerritoryManagerUpdate,
    MSCustomerDetailsUpdateSerializer,
    MSCustomerDetailsSerializer,
    DeviceUpdateSeralizer,
    ProviderDeviceUpdateSerializer,
    DeviceIDListSerializer,
    ImageUploadSerializer,
    SystemUploadAttachmentSerializer,
    ScheduledReportTypesSerializer,
    UserScheduledCreateUpdateReportSerializer,
    UserScheduledReportDetailSerializer,
    SubscriptionProductTypeSerializer,
    SubscriptionProductMappingCreateSerializer,
    SubscriptionImportSerializer,
    SubscriptionProductMappingUpdateSerializer,
    SusbscriptionUpdateSerializer,
    ReportDownloadStatsSerializer,
    SusbscriptionGetSerializer,
    SusbscriptionGetSerializerForCustomerUser,
    SusbscriptionUpdateSerializerForCustomerUser,
    SubscriptionServiceProviderMappingListCreateSerializer,
    SubscriptionServiceProviderMappingUpdateDestroySerializer,
    EmptySerialMappingSerializer,
    BulkCreateSitesSerializer,
    VendorTypeCustomerCreateSerializer,
    VendorContactCreateSerializer,
    TeamRoleCreateSerializer,
    AutomatedSubscriptionNotificationSettingSerializer,
    ConfigurationMappingSerializer,
    SubscriptionNotificationTriggerSerializer,
    ReportBundleSerializer,
)
from core.services import (
    DeviceService,
    DashboardService,
    IntegrationService,
    ERPClientIntegrationService,
    DropboxIntegrationService,
    LogicMonitorIntegrationService,
    SubscriptionService,
    trigger_connectwise_sync_tasks,
    trigger_external_service_sync_tasks,
    SubscriptionNotificationService,
    ReportsBundleService,
)
from core.tasks.connectwise.connectwise import (
    cw_customer_user_full_sync,
    get_provider_ms_customer_field_mapping,
)
from core.tasks.dropbox.dropbox import (
    sync_file_storage_for_providers,
)
from core.tasks.services import TaskService
from core.tasks.tasks import (
    prepare_and_send_data_anomaly_report,
    prepare_and_send_device_pdf_report,
    send_customer_distribution_update_email,
    bulk_device_delete,
    remove_device_circuit_association,
    send_subscription_notification_email,
    prepare_and_send_devices_reports_bundle,
)
from core.utils import (
    device_date_formats,
    generate_random_id,
    convert_date_string_format,
    get_customer_id,
)
from core.webhook_service import (
    ConnectwiseCallbackService,
    LogicMonitorCallbackService,
)
from document.services.customer_order_tracking import FinanceReportService
from document.models import SOWVendorOnboardingSetting
from document.utils import get_sow_vendor_onboarding_settings
from exceptions.exceptions import NotFoundError
from renewal.services import RenewalService
from renewal.tasks import remove_device_from_renewal_request
from storage.file_upload import FileUploadService
from utils import (
    get_app_logger,
    get_valid_file_name,
)
from core.integrations.utils import generate_timestamped_file_name
from typing import List, Dict, Union, Optional
from core.connectwise_communication_item import (
    ConnectwiseCommunicationItemsService,
)
from core.exceptions import CWPhoneNumberMappingSettingNotConfigured
from document.tasks.sow_vendor_onboarding import (
    send_onboarding_email_to_vendor_primary_contact,
)
from core.exceptions import SubscriptionNotificationSettingNotConfigured

# Get an instance of a logger
logger = get_app_logger(__name__)


class ProviderListCreateAPIView(generics.ListCreateAPIView):
    """
    API View to list and create Provider
    """

    parser_classes = (JSONParser, MultiPartParser, FormParser)
    queryset = Provider.objects.none()
    serializer_class = ProviderSerializer
    permission_classes = (CustomPermissions,)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    pagination_class = CustomPagination
    search_fields = [
        "name",
        "url",
        "email",
        "is_active",
        "support_contact",
        "address__address_1",
        "address__address_2",
        "address__city",
        "address__state",
        "address__zip_code",
        "accounting_contact__first_name",
        "accounting_contact__last_name",
        "accounting_contact__email",
        "accounting_contact__phone",
        "accounting_contact__website",
    ]
    ordering_fields = [
        "name",
        "url",
        "email",
        "is_active",
        "support_contact",
        "address__address_1",
        "address__address_2",
        "address__city",
        "address__state",
        "address__zip_code",
        "accounting_contact__first_name",
        "accounting_contact__last_name",
        "accounting_contact__email",
        "accounting_contact__phone",
        "accounting_contact__website",
    ]

    def perform_create(self, serializer):
        logo = serializer.validated_data.pop("logo", None)
        data = copy.deepcopy(serializer.validated_data)
        address_data = data.pop("address", {})
        accounting_contact_data = data.pop("accounting_contact", {})

        # Create Address
        address = Address.objects.create(**address_data)

        # Create Accounting Contact
        accounting_contact = AccountingContact.objects.create(
            **accounting_contact_data
        )

        # Create Provider
        Provider.objects.create(
            address=address,
            accounting_contact=accounting_contact,
            logo=logo,
            **data,
        )

    def get_queryset(self):
        if self.request.user.type == UserType.ACCELAVAR:
            return Provider.objects.all()
        return Provider.objects.none()


class ProviderUpdateMixin(object):
    def perform_update(self, serializer):
        logo = serializer.validated_data.pop("logo", None)
        serializer.save()
        if logo:
            serializer.instance.logo = logo
            serializer.instance.save(update_fields=["logo"])


class ProviderRetrieveUpdateAPIView(
    ProviderUpdateMixin, generics.RetrieveUpdateAPIView
):
    """
    API View to Retrieve and Update Provider
    """

    parser_classes = (JSONParser, MultiPartParser, FormParser)
    queryset = Provider.objects.all()
    serializer_class = ProviderUpdateSerializer
    permission_classes = (CustomPermissions,)


class ProviderSelfRetrieveUpdateAPIView(
    ProviderUpdateMixin, generics.RetrieveUpdateAPIView
):
    """
    API View to Retrieve and Update Provider itself.
    """

    parser_classes = (JSONParser, MultiPartParser, FormParser)
    queryset = Provider.objects.all()
    serializer_class = ProviderSelfUpdateSerializer

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if self.request.method in ["PUT", "PATCH"]:
            permission_classes.append(IsProviderOwnerOrAdmin)
        return [permission() for permission in permission_classes]

    def get_object(self):
        return self.request.provider


class ProviderRetrieveAPIView(generics.RetrieveAPIView):
    """
    API View to Retrieve Provider logo and color - No Authentication
    """

    serializer_class = ProviderInfoSerializer
    permission_classes = ()

    def get_object(self):
        return self.request.provider


class CustomerListCreateAPIView(generics.ListCreateAPIView):
    """
    API View to List and Create Customers
    """

    queryset = Customer.objects.all()
    permission_classes = (CustomPermissions,)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    pagination_class = CustomPagination
    search_fields = [
        "name",
        "primary_contact",
        "is_active",
        "address__address_1",
        "address__address_2",
        "address__city",
        "address__state",
        "address__zip_code",
    ]

    ordering_fields = [
        "name",
        "primary_contact",
        "is_active",
        "address__address_1",
        "address__address_2",
        "address__city",
        "address__state",
        "address__zip_code",
    ]

    def get_serializer_class(self):
        full_info = self.request.query_params.get("full-info", "true")
        if full_info == "false":
            return CustomerDropdownSerializer
        return CustomerSerializer

    def get_queryset(self):
        if self.request.user.type == UserType.PROVIDER:
            return Customer.objects.filter(
                provider=self.request.provider
            ).order_by("name")
        return Customer.objects.none()


class CustomerRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    """
    API View to Retrieve and Update Customer.
    """

    queryset = Customer.objects.all()
    serializer_class = CustomerDetailsSerializer
    permission_classes = (CustomPermissions,)

    def perform_update(self, serializer):
        customer = copy.deepcopy(self.get_object())
        serializer.save()
        customer_distribution = serializer.data.get("customer_distribution")
        if customer.customer_distribution != customer_distribution:
            send_customer_distribution_update_email.delay(
                customer.id, self.request.provider.id, customer_distribution
            )
        if customer.is_ms_customer != serializer.data.get("is_ms_customer"):
            ms_customer_field = get_provider_ms_customer_field_mapping(
                self.request.provider.id
            )
            if ms_customer_field:
                self.request.provider.erp_client.update_customer_ms_customer_status(
                    customer.crm_id,
                    ms_customer_field,
                    serializer.data.get("is_ms_customer"),
                )


class CustomerProfileRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = Customer.objects.all()
    permission_classes = (IsAuthenticated, IsCustomerUser)

    def get_serializer_class(self):
        is_ms_customer_perm = (
            self.request.user.is_authenticated
            and self.request.user.type == UserType.CUSTOMER
            and self.request.customer.is_ms_customer
        )
        if not is_ms_customer_perm and self.request.method == "PUT":
            return MSCustomerDetailsUpdateSerializer
        elif not is_ms_customer_perm:
            return MSCustomerDetailsSerializer
        elif self.request.method == "PUT":
            return CustomerDetailsUpdateSerializer
        else:
            return CustomerDetailsSerializer

    def get_object(self):
        return self.request.customer

    def perform_update(self, serializer):
        customer = copy.deepcopy(self.get_object())
        serializer.save()
        customer_distribution = serializer.data.get("customer_distribution")
        if customer.customer_distribution != customer_distribution:
            send_customer_distribution_update_email.delay(
                customer.id, self.request.provider.id, customer_distribution
            )


class IntegrationCategoryListAPIView(generics.ListAPIView):
    """
    API View to list available integrations and their types
    """

    queryset = IntegrationCategory.objects.all()
    serializer_class = IntegrationCategorySerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("name",)


class IntegrationCategoryRetrieveAPIView(generics.RetrieveAPIView):
    """
    API View to get single config details
    """

    queryset = IntegrationCategory.objects.all()
    serializer_class = IntegrationCategorySerializer
    permission_classes = (IsAuthenticated, IsProviderUser)


class IntegrationTypeListAPIView(generics.ListAPIView):
    """
    API View to list available integration types
    """

    serializer_class = IntegrationTypeSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("name",)

    def get_queryset(self):
        category_id = self.kwargs.get("category_id")
        return IntegrationType.objects.filter(category_id=category_id)


class IntegrationTypeRetrieveAPIView(generics.RetrieveAPIView):
    """
    API View to get single config details
    """

    queryset = IntegrationType.objects.all()
    serializer_class = IntegrationTypeSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)


class ValidateAuthCredentials(APIView):
    """
    This view validates the authentication credentials provided by user for integration
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, *args, **kwargs):
        """
        Verifies authentication credentials provided by user for integration
        """

        integration = IntegrationType.objects.get(id=kwargs.get("type_id"))
        client = integration.client_class(request.data)
        data = dict(verified=client.validate_api_credentials())
        return Response(data, status=status.HTTP_200_OK)


class ProviderIntegrationListCreateAPIView(
    ProviderIntegrationSerializerMixin, generics.ListCreateAPIView
):
    pagination_class = None

    def get_permissions(self):
        permission_classes = [IsAuthenticated, IsProviderOrCustomerUser]
        if self.request.method == "POST":
            permission_classes.append(IsProviderOwner)
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        category, type_ = (
            self.request.query_params.get("category"),
            self.request.query_params.get("type"),
        )
        queryset = ProviderIntegration.objects.filter(
            provider=self.request.provider
        )
        if category:
            queryset = queryset.filter(
                integration_type__category__name__iexact=category
            )
        if type_:
            queryset = queryset.filter(integration_type__name__iexact=type_)
        return queryset

    def get_serializer_class(self):
        return self.get_provider_integration_serializer_class(
            self.request.data
        )

    def perform_create(self, serializer):
        provider = self.request.provider
        serializer.save(provider=provider)
        if (
            serializer.validated_data.get("integration_type")
            == IntegrationService.get_connectwise_integration_type_object()
        ):
            provider.integration_statuses[
                "crm_authentication_configured"
            ] = True
        elif (
            serializer.validated_data.get("integration_type")
            == IntegrationService.get_cisco_integration_type_object()
        ):
            if not provider.integration_statuses[
                "device_manufacturer_api_configured"
            ]:
                TaskService.sync_all_manufacturer_data(provider.id)
                provider.integration_statuses[
                    "device_manufacturer_api_configured"
                ] = True
        elif (
            serializer.validated_data.get("integration_type")
            == IntegrationService.get_dropbox_storage_integration_type_object()
        ):
            sync_file_storage_for_providers.delay()
        provider.save()


class ProviderIntegrationRetrieveUpdateAPIView(
    ProviderIntegrationSerializerMixin, generics.RetrieveUpdateAPIView
):
    def get_permissions(self):
        permission_classes = [IsAuthenticated, IsProviderOrCustomerUser]
        if self.request.method in ["PUT", "PATCH"]:
            permission_classes.append(IsProviderOwner)
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        return ProviderIntegration.objects.filter(
            provider=self.request.provider
        )

    def get_serializer_class(self):
        return self.get_provider_integration_serializer_class(
            self.request.data
        )

    def perform_update(self, serializer):
        self.update_provider_integration(
            self.request.provider, self.get_object(), serializer
        )


class ProviderIntegrationExtraConfigAPIView(APIView):
    """
    This view returns extra config for Provider Integration
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        try:
            integration = ProviderIntegration.objects.get(id=kwargs.get("pk"))
        except ProviderIntegration.DoesNotExist:
            raise NotFound("Invalid Provider Integration ID.")
        data = integration.get_extra_config()
        return Response(data=data, status=status.HTTP_200_OK)


class ProviderIntegrationEmptySerialMappingAPIView(APIView):
    """
    This view returns Empty Serial Mapping from Provider Integration
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)
    serializer_class = EmptySerialMappingSerializer

    def put(self, request, **kwargs):
        try:
            data = EmptySerialMappingSerializer(data=request.data)
            data.is_valid(raise_exception=True)
            integration = ProviderIntegration.objects.get(id=kwargs.get("pk"))
            extra_config = integration.other_config
            extra_config.update(data.validated_data)
            integration.other_config = extra_config
            integration.save()
        except ProviderIntegration.DoesNotExist:
            raise NotFound("Invalid Provider Integration ID.")
        return Response(data=extra_config, status=status.HTTP_200_OK)


class ProviderIntegrationStatusAPIView(APIView):
    """
    Returns Integration status
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        data = request.provider.integration_statuses
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceListCreateAPIView(APIView):
    """
    API View to GET and POST for Device Resource.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_full_info_devices = request.query_params.get("full-info", "true")
        show_active_only = False if show_inactive_devices == "true" else True
        show_full_info = True if show_full_info_devices == "true" else False
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        category_id = request.provider.get_all_device_categories()
        devices = DeviceService.get_device_list(
            provider_id=request.provider.id,
            customer_id=customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
            show_full_info=show_full_info,
        )
        if request.user.type == UserType.CUSTOMER:
            for device in devices:
                device.pop("notes", None)
        return Response(data=devices, status=status.HTTP_200_OK)

    def post(self, request, **kwargs):
        serializer = self.get_serializer()
        serializer.is_valid(raise_exception=True)
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        provider_id = request.provider.id
        manufacturer_id = serializer.validated_data.get("manufacturer_id")
        category_id = request.provider.get_device_category_id_by_manufacturer(
            manufacturer_id
        )
        erp_client = request.provider.erp_client
        data = serializer.validated_data

        is_duplicate, is_created = DeviceService.create_device(
            erp_client, customer_id, category_id, provider_id, data
        )
        if is_duplicate:
            raise ValidationError(
                {
                    "serial_number": [
                        "A device with the same serial number already exists"
                    ]
                }
            )
        return Response(status=status.HTTP_201_CREATED)

    def get_serializer(self):
        if (
            self.request.user.is_authenticated
            and self.request.user.type == UserType.CUSTOMER
        ):
            return DeviceSerializer(data=self.request.data)
        else:
            return ProviderDeviceSerializer(data=self.request.data)


class AllCustomerDeviceListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        category_id = request.provider.get_all_device_categories()
        devices = DeviceService.get_all_devices_for_provider(
            erp_client=request.provider.erp_client, category_id=category_id
        )
        return Response(data=devices, status=status.HTTP_200_OK)


class DeviceRetrieveUpdateAPIView(APIView):
    """
    API View to get device details and update device.
    TODO: Apply proper permission check for device
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        device_id = kwargs.get("device_id")
        try:
            device = DeviceService.get_device(
                request.provider.erp_client, device_id, request.provider.id
            )
        except NotFoundError:
            res = {"message": "Device not found"}
            return Response(data=res, status=status.HTTP_400_BAD_REQUEST)
        customer_id = get_customer_id(request, **kwargs)
        is_present = RenewalService.get_renewal_history_is_present(
            customer_id, device.get("id")
        )
        device.update({"renewal_history_is_present": is_present})
        if request.user.type == UserType.CUSTOMER:
            device.pop("notes", None)
        return Response(data=device, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        device_id = kwargs.get("device_id")
        serializer = self.get_serializer()
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        device = DeviceService.update_device(
            request.provider.id, customer_id, device_id, data
        )
        return Response(data=device, status=status.HTTP_204_NO_CONTENT)

    def get_serializer(self):
        if (
            self.request.user.is_authenticated
            and self.request.user.type == UserType.CUSTOMER
        ):
            return DeviceUpdateSeralizer(data=self.request.data)
        else:
            return ProviderDeviceUpdateSerializer(data=self.request.data)


class DeviceCategoriesRetrieveAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        data = ERPClientIntegrationService.get_device_categories(
            request.provider.id, request.provider.erp_client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceContractUpdateAPIView(APIView):
    """
    API View to update device contracts
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = DeviceContractUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = DeviceService.update_device_contracts(
            request.provider.id,
            customer_id,
            serializer.validated_data["device_ids"],
            serializer.validated_data["value"],
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceCategoryUpdateAPIView(APIView):
    """
    API View to update category of devices
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = DeviceCategoryUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        device_ids = serializer.validated_data["device_ids"]
        data = DeviceService.update_device_categories(
            request.provider.id,
            customer_id,
            serializer.validated_data["device_ids"],
            serializer.validated_data["value"],
        )
        bulk_device_delete(request.provider.id, device_ids)
        remove_device_circuit_association(request.provider.id, device_ids)
        remove_device_from_renewal_request(request.provider.id, device_ids)
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceStatusUpdateAPIView(APIView):
    """
    API View to update device statuses
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = DeviceStatusUpdateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        data = DeviceService.update_device_status(
            request.provider.id,
            customer_id,
            serializer.validated_data["device_ids"],
            serializer.validated_data["value"],
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceSiteUpdateAPIView(APIView):
    """
    API View to update device sites
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = DeviceSiteUpdateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        data = DeviceService.update_device_sites(
            request.provider.id,
            customer_id,
            request.provider.erp_client,
            serializer.validated_data["device_ids"],
            serializer.validated_data["value"],
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceManufacturerUpdateAPIView(APIView):
    """
    API View to update device manufacturers
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = DeviceManufacturerUpdateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        data = DeviceService.update_device_manufacturers(
            request.provider.id,
            customer_id,
            request.provider.erp_client,
            serializer.validated_data["device_ids"],
            serializer.validated_data["value"],
        )
        return Response(data=data, status=status.HTTP_200_OK)


class SiteListCreateAPIView(generics.ListCreateAPIView):
    """
    API view to list sites for the customer.
    """

    serializer_class = SiteSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_customer_sites(
            request.provider.id, customer_id, client
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        customer_id = utils.get_customer_cw_id(self.request, **self.kwargs)
        client = self.request.provider.erp_client
        data = ERPClientIntegrationService.create_customer_site(
            self.request.provider.id,
            customer_id,
            client,
            data=serializer.validated_data,
        )
        return Response(data=data, status=status.HTTP_201_CREATED)


class SiteBulkCreateAPIView(APIView):
    """
    API view to Bulk Create sites for the customer.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        serializer = BulkCreateSitesSerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        client = self.request.provider.erp_client
        data = ERPClientIntegrationService.bulk_create_customer_site(
            self.request.provider.id,
            customer_id,
            client,
            payload=serializer.validated_data,
        )
        return Response(data=data, status=status.HTTP_201_CREATED)


class SiteUpdateDeleteAPIView(UpdateAPIView, DestroyAPIView):
    """
    API view to update, delete customer site
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def put(self, request, *args, **kwargs):
        serializer = SiteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        client = request.provider.erp_client
        site_id = kwargs.get("site_id")
        data = ERPClientIntegrationService.update_customer_site(
            request.provider.id,
            customer_id,
            client,
            site_id,
            serializer.validated_data,
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        site_crm_id: int = self.kwargs.get("site_id")
        customer_crm_id: int = utils.get_customer_cw_id(request, **kwargs)
        self.request.provider.erp_client.delete_customer_site(
            customer_crm_id, site_crm_id
        )
        return Response(status=status.HTTP_204_NO_CONTENT)


class ManufacturerListAPIView(APIView):
    """
    API view to list Manufacturers.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_manufacturers(
            request.provider.id, client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceImportFileAPIView(APIView):
    """
    API View to create multiple devices from xlsx file
    """

    def post(self, request, **kwargs):
        serializer = DeviceImportFileMetaInfo(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_name = serializer.validated_data.get("file_name")
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        data = DeviceService.batch_create_or_update_devices_from_file(
            request.provider.id,
            customer_id,
            serializer.validated_data,
            file_name,
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DevicesFileUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        uploaded_file = request.FILES.get("filename")
        if not uploaded_file:
            raise ValidationError({"filename": ["filename is required."]})
        file_name = os.path.basename(uploaded_file.name)
        directory_path = f"{settings.MEDIA_ROOT}/providers/files"
        file_path = f"{directory_path}/{file_name}"
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
        try:
            with open(file_path, "wb+") as destination:
                for chunk in uploaded_file.chunks():
                    destination.write(chunk)

            url = FileUploadService.upload_file(file_path)
            return Response(data=url, status=status.HTTP_201_CREATED)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return Response(
                {"message": "Failed to upload file"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class ImageUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        serializer = ImageUploadSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        up_file = request.data["filename"]
        file_name, file_extension = os.path.splitext(up_file.name)
        file_name = get_valid_file_name(
            f"{file_name}-{generate_random_id(15)}{file_extension}"
        )
        directory_path = f"{settings.MEDIA_ROOT}/attachments/"
        file_path = f"{directory_path}{file_name}"
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
        try:
            with open(file_path, "wb+") as destination:
                for chunk in up_file.chunks():
                    destination.write(chunk)
            url = FileUploadService.upload_attachment(file_path)
            os.remove(file_path)
            data = dict(file_path=url, file_name=file_name)
            return Response(data=data, status=status.HTTP_200_OK)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return Response(
                {"message": "Failed to upload file."},
                status=status.HTTP_400_BAD_REQUEST,
            )


class ContractStatusListAPIView(APIView):
    """
    API View to list Contract Statuses.
    """

    def get(self, request):
        contract_statuses = DeviceManufacturerData.CONTRACT_STATUSES
        serializer = ContractStatusSerializer(contract_statuses, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class DeviceStatusListAPIView(APIView):
    """
    API View to list device Statuses.
    """

    def get(self, request):
        provider = request.provider
        statuses = provider.erp_client.get_device_statuses()
        return Response(data=statuses, status=status.HTTP_200_OK)


class DeviceDataExportAPIView(APIView):
    """
    API View to export device data to a csv file.
    TODO: Apply proper permissions check.
    """

    renderer_classes = (DictToCSVRenderer,)

    def post(self, request, **kwargs):
        serializer = DeviceIDListSerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        device_ids = request.data.get("device_ids")
        devices = DeviceService.get_device_list(
            provider_id=request.provider.id,
            customer_id=customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
        )
        if not devices:
            return Response(status=status.HTTP_204_NO_CONTENT)
        devices = list(
            filter(lambda device: device.get("id") in device_ids, devices)
        )
        site_ids = list(map(lambda x: x.get("site_id"), devices))
        customer_sites = (
            ERPClientIntegrationService.get_customer_complete_site_addresses(
                request.provider.id,
                customer_id,
                request.provider.erp_client,
                site_ids=site_ids,
            )
        )
        for device in devices:
            address = customer_sites.get(device.get("site_id"))
            device.update({"site_address": address})
        headers = {"Content-Disposition": "attachment; filename=Devices.csv"}
        return Response(
            data=devices, status=status.HTTP_200_OK, headers=headers
        )

    def get_renderers(self):
        serializer = DeviceIDListSerializer(data=self.request.data)
        if not serializer.is_valid():
            return [JSONRenderer()]
        else:
            return [DictToCSVRenderer()]


class DeviceTypeListAPIView(APIView):
    """API View to list different device types."""

    def get(self, request):
        api_types = DeviceTypes.objects.distinct()
        manual_types = ManualDeviceTypeMapping.objects.filter(
            provider_id=request.provider.id
        )
        device_types = (
            api_types.union(manual_types)
            .values_list("type", flat=True)
            .order_by("type")
        )
        return Response(data=device_types, status=status.HTTP_200_OK)


class ReportsAPIView(APIView):
    renderer_classes = (DeviceReportCSVRenderer,)

    def post(self, request, **kwargs):
        serializer = ReportsFiltersSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user.type == UserType.PROVIDER:
            if serializer.validated_data.get("customer_id"):
                customer_id = serializer.validated_data.get("customer_id")
                customer_id = list(
                    Customer.objects.filter(id__in=customer_id).values_list(
                        "crm_id", flat=True
                    )
                )
                serializer.validated_data.pop("customer_id")
            else:
                customer_id = []
        else:
            customer_id = utils.get_customer_cw_id(request, **kwargs)
        category_id = request.provider.get_all_device_categories()
        devices = DeviceService.get_device_list(
            provider_id=request.provider.id,
            customer_id=customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
        )
        filtered_devices = DeviceService.filter_devices_data(
            devices, serializer.validated_data
        )
        for device in filtered_devices:
            try:
                device["expiration_date"] = convert_date_string_format(
                    device.get("expiration_date"),
                    "%Y-%m-%dT%H:%M:%SZ",
                    "%m/%d/%Y",
                )
            except:
                device["expiration_date"] = ""

        if filtered_devices:
            if request.user.type == UserType.CUSTOMER:
                prepare_and_send_device_pdf_report.delay(
                    filtered_devices, request.user.id
                )
                return Response(
                    data=dict(message="Email has been sent."),
                    status=status.HTTP_200_OK,
                )
            else:
                headers = {
                    "Content-Disposition": "attachment; filename=report.csv"
                }
                return Response(
                    data=filtered_devices,
                    status=status.HTTP_200_OK,
                    headers=headers,
                )
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def get_renderers(self):
        if self.request.user.type == UserType.CUSTOMER:
            return [JSONRenderer()]


class CustomerDeviceManagementReportAPIVIew(APIView):
    def post(self, request, **kwargs):
        serializer = LifeCycleManagementReportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user.type == UserType.PROVIDER:
            if serializer.validated_data.get("customer_id"):
                customer_id = serializer.validated_data.get("customer_id")
                customer_id = list(
                    Customer.objects.filter(id__in=customer_id).values_list(
                        "crm_id", flat=True
                    )
                )
                serializer.validated_data.pop("customer_id")
            else:
                customer_id = []
        else:
            customer_id = utils.get_customer_cw_id(request, **kwargs)
        file_path, file_name = generate_customer_device_management_report(
            request.provider.id,
            customer_id,
            serializer.validated_data,
            request.user.type,
            request.user,
        )
        if file_path:
            url = FileUploadService.upload_file(file_path)
            data = dict(file_path=url, file_name=file_name)
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)


class CustomerDeviceManagementReportsBundleAPIVIew(APIView):
    def post(self, request, **kwargs):
        serializer = ReportBundleSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user.type == UserType.PROVIDER:
            if serializer.validated_data.get("customer_id"):
                customer_id = serializer.validated_data.get("customer_id")
                customer_id = list(
                    Customer.objects.filter(id__in=customer_id).values_list(
                        "crm_id", flat=True
                    )
                )
                serializer.validated_data.pop("customer_id")
            else:
                customer_id = []
        else:
            customer_id = utils.get_customer_cw_id(request, **kwargs)

        report_types_bundle = ReportsBundleService.report_types_bundle
        # Email will be sent with all the reports bundle
        prepare_and_send_devices_reports_bundle.delay(
            self.request.provider.id,
            self.request.user.id,
            self.request.user.type,
            customer_id,
            report_types_bundle,
        )
        return Response(
            data=dict(
                message="Report is being created, An Email will be sent shortly"
            ),
            status=status.HTTP_200_OK,
        )


class DevicesAnomalyReportAPIView(APIView):
    """
    API View to request device anomaly report
    """

    def post(self, request, **kwargs):
        prepare_and_send_data_anomaly_report.delay(
            request.provider.id, request.user.id
        )
        return Response(status=status.HTTP_200_OK)


class FinanceReportExportAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    renderer_classes = (FinanceReportCSVRenderer,)

    def get(self, request, *args, **kwargs):
        data = FinanceReportService.export_finance_report(
            provider=request.provider, filter_params=self.request.query_params
        )
        headers = {
            "Content-Disposition": "attachment; filename=Finance-Report.csv"
        }
        return Response(data=data, status=status.HTTP_200_OK, headers=headers)


class DateFormatListAPIView(APIView):
    """
    API View to return list of date formats supported
    """

    def get(self, request):
        return Response(data=device_date_formats(), status=status.HTTP_200_OK)


class CountryListAPIView(APIView):
    """
    API View to return list of countries
    """

    def get(self, request):
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_countries_list(
            request.provider.id, client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class StateListAPIView(APIView):
    """
    API View to return list of states
    """

    def get(self, request, **kwargs):
        country_id = kwargs.get("country_id")
        data = ERPClientIntegrationService.get_states_list(
            request.provider.id, country_id, request.provider.erp_client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class CountriesAndStatesListAPIView(APIView):
    """
    API View to return list of countries and their states.
    """

    def get(self, request):
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_countries_and_state_list(
            request.provider.id, client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class ConnectwiseBoardStatusListAPIView(APIView):
    """
    API View to return list of statuses for boards from connectwise.
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)

    def get(self, request, **kwargs):
        board_id = self.kwargs.get("board_id")
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_board_statuses(client, board_id)
        return Response(data=data, status=status.HTTP_200_OK)


class AcelaDashboardAPIView(APIView):
    permission_classes = (IsAuthenticated, IsAcelavarUser)

    def get(self, request):
        dashboard = DashboardService.get_acela_dashboard_metrics()
        return Response(dashboard)


class TriggerCWDataSyncTaskView(APIView):
    """
    API View to start async task to update connectwise  data.
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)

    def post(self, request):
        logger.info(
            "Connectwise data sync task for provider: {0} triggered by {1}".format(
                request.provider, request.user
            )
        )
        trigger_connectwise_sync_tasks()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TriggerExternalServicesDataSyncTaskView(APIView):
    """
    API View to start async task to update external services Cisco, Logicmonitor data.
    """

    permission_classes = (IsAuthenticated, IsProviderOwner)

    def post(self, request):
        logger.info(
            "External Integrations data sync task for provider: {0} triggered by {1}".format(
                request.provider, request.user
            )
        )
        trigger_external_service_sync_tasks()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DeviceManufacturerAPIRetryCountResetView(APIView):
    """
    API View to reset device manufacturer api retry count.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def post(self, request, serial_number):
        DeviceService.reset_device_manufacturer_api_retry_count(
            request.provider.id, serial_number
        )
        return Response(status=status.HTTP_200_OK)


class CustomerReportingTypeRetrieveAPIView(APIView):
    """
    API View to return the types of Life Cycle Management report types that can be generated.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request):
        is_ms_customer = (
            True
            if request.user.type == UserType.PROVIDER
            or (request.user.customer and request.user.customer.is_ms_customer)
            else False
        )
        data = DeviceService.get_customer_reporting_types(is_ms_customer)
        return Response(data=data, status=status.HTTP_200_OK)


class CustomerNoteTypesListAPIVIew(APIView):
    """
    API View to list and create customer notes.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_company_note_types(client)
        return Response(data=data, status=status.HTTP_200_OK)


class CustomerNotesListCreateAPIVIew(APIView):
    """
    API View to list and create customer notes.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_company_notes(
            client, customer_id=customer_id
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def post(self, request, **kwargs):
        serializer = CustomerNoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        client = request.provider.erp_client
        data = ERPClientIntegrationService.create_company_note(
            client, customer_id, serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_200_OK)


class CustomerNotesRetrieveUpdateAPIVIew(APIView):
    """
    API View to retrieve and update customer note.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        note_id = kwargs.get("note_id")
        client = request.provider.erp_client
        data = ERPClientIntegrationService.get_company_note(
            client, customer_id, note_id
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):
        serializer = CustomerNoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        note_id = kwargs.get("note_id")
        client = request.provider.erp_client
        data = ERPClientIntegrationService.update_company_note(
            client, customer_id, note_id, serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_200_OK)


class FileStorageFileListingAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request, **kwargs):
        customer_id = None
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        provider = request.provider
        path = request.query_params.get("path")
        path = path or ""
        data = DropboxIntegrationService.get_files_folder_list(
            customer_id, provider, path
        )
        return Response(data=data, status=status.HTTP_200_OK)


class FileStorageRootFileListingAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        access_token = request.data.get("access_token")
        if access_token is None:
            raise ValidationError(
                dict(access_token="access_token cannot be empty")
            )
        data = DropboxIntegrationService.get_root_folder_list(access_token)
        return Response(data=data, status=status.HTTP_200_OK)


class FileStorageFileRetrieveAPIView(HTTPDownloadView, APIView):
    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request, **kwargs):
        customer_id = None
        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("No customer found.")
        provider = request.provider
        path = request.query_params.get("path")
        path = path or ""
        data = DropboxIntegrationService.get_file(customer_id, provider, path)
        return Response(data=data, status=status.HTTP_200_OK)


class AvailableCircuitInfoForAssociationListAPIView(generics.ListAPIView):
    serializer_class = CircuitInfoSerializer
    pagination_class = None

    def get_queryset(self):
        customer_id = get_customer_id(self.request, **self.kwargs)
        queryset = CircuitInfo.objects.filter(
            customer=customer_id, is_deleted=False, device_crm_id__isnull=True
        )
        return queryset


class DeviceCircuitInfoAssociationCreateAPIView(generics.CreateAPIView):
    serializer_class = DeviceCircuitInfoAssociationCreateSerializer

    def perform_create(self, serializer):
        customer_id = get_customer_id(self.request, **self.kwargs)
        circuit_info_ids = serializer.data.get("circuit_info_ids")
        device_crm_id = serializer.data.get("device_crm_id")
        CircuitInfo.objects.filter(
            customer_id=customer_id, device_crm_id=device_crm_id
        ).update(device_crm_id=None)
        CircuitInfo.objects.filter(id__in=circuit_info_ids).update(
            device_crm_id=device_crm_id
        )
        return Response(status=status.HTTP_201_CREATED)


class DeviceCircuitInfoAssociationListAPIView(generics.ListAPIView):
    serializer_class = CircuitInfoSerializer
    pagination_class = None

    def get_queryset(self):
        customer_id = get_customer_id(self.request, **self.kwargs)
        device_crm_id = self.kwargs.get("device_id")
        queryset = CircuitInfo.objects.filter(
            customer_id=customer_id,
            device_crm_id=device_crm_id,
            is_deleted=False,
        )
        return queryset


class GetDeviceMonitoringGroups(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        serializer = DeviceMonitoringAuthSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = LogicMonitorIntegrationService.get_group_list(
            serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_200_OK)


class GetDeviceSystemCategories(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        serializer = DeviceMonitoringAuthSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        group_id = self.kwargs.get("group_id")
        data = LogicMonitorIntegrationService.get_system_categories_for_group(
            serializer.validated_data, group_id
        )
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceMonitoringDefaultKeyMappings(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        data = LogicMonitorIntegrationService.get_key_mappings()
        return Response(data=data, status=status.HTTP_200_OK)


class SystemMembersAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        data = ERPClientIntegrationService.get_members(
            request.provider.id, request.provider.erp_client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class SystemTerritoryListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        data = ERPClientIntegrationService.get_territories(
            request.provider.id, request.provider.erp_client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class SystemTerritoryRetrieveUpdateAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        territory_id = self.kwargs["id"]
        data = ERPClientIntegrationService.get_territory(
            self.request.provider.id,
            self.request.provider.erp_client,
            territory_id,
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):
        serializer = SystemTerritoryManagerUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)
        territory_id = self.kwargs["id"]
        territory = ERPClientIntegrationService.get_territory(
            self.request.provider.id,
            self.request.provider.erp_client,
            territory_id,
        )
        territory.update(serializer.validated_data)
        data = ERPClientIntegrationService.update_territory(
            self.request.provider.id,
            self.request.provider.erp_client,
            territory_id,
            territory,
        )
        return Response(data=data, status=status.HTTP_200_OK)


class SystemBusinessUnitListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        data = ERPClientIntegrationService.get_business_units(
            request.provider.erp_client
        )
        return Response(data=data, status=status.HTTP_200_OK)


class SystemAttachmentUploadAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        serializer = SystemUploadAttachmentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        data = ERPClientIntegrationService.upload_attachments(
            self.request.provider.erp_client, data
        )
        return Response(data=data, status=status.HTTP_200_OK)


class ScheduledReportCategoriesRetrieveAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        data = dict(ScheduledReportTypes.CATEGORY_CHOICES)
        return Response(data=data, status=status.HTTP_200_OK)


class ScheduledReportFrequenciesRetrieveAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        data = dict(UserScheduledReport.FREQUENCY_CHOICES)
        return Response(data=data, status=status.HTTP_200_OK)


class ScheduledReportTypesListAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ScheduledReportTypesSerializer
    pagination_class = None

    def get_queryset(self):
        category = self.request.query_params.get("category")
        if category:
            return ScheduledReportTypes.objects.filter(
                category__iexact=category
            )
        return ScheduledReportTypes.objects.all()


class ScheduleUserReportListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    pagination_class = None

    def get_queryset(self):
        return UserScheduledReport.objects.filter(user=self.request.user)

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        user_id = self.request.user.id
        if UserScheduledReport.objects.filter(
            user=user_id,
            scheduled_report_type=data.get("scheduled_report_type"),
        ):
            raise ValidationError(
                {"detail": ["Report schedule already exists"]}
            )
        data["user_id"] = user_id
        user_scheduled_report = UserScheduledReport.objects.create(**data)
        data = {
            "id": user_scheduled_report.id,
            "frequency": user_scheduled_report.frequency,
            "scheduled_report_type": user_scheduled_report.scheduled_report_type.id,
        }
        return Response(data=data, status=status.HTTP_201_CREATED)

    def get_serializer_class(self):
        if self.request.method in ["POST"]:
            return UserScheduledCreateUpdateReportSerializer
        else:
            return UserScheduledReportDetailSerializer


class ScheduleUserReportRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return UserScheduledReport.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.request.method in ["PUT"]:
            return UserScheduledCreateUpdateReportSerializer
        else:
            return UserScheduledReportDetailSerializer


class PollingTaskStatusAPIView(APIView):
    """
    API View to return the status of the task
    """

    def get(self, request, **kwargs):
        task_id = self.kwargs.get("task_id")
        res = AsyncResult(task_id)
        data = {"status": res.status}
        if res.status == celery.states.SUCCESS and res.result:
            data.update({"result": res.result})
        return Response(data=data, status=status.HTTP_200_OK)


#####################################################################
############ Subscription Views #####################################
#####################################################################


class SubscriptionListAPIView(APIView):
    """
    API View to list Subscriptions.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        category_id = request.provider.get_all_subscription_categories()
        if category_id is None:
            raise ValidationError("Subscription Mapping Not Configured.")
        subscriptions = SubscriptionService.get_subscription_list(
            request.provider.id,
            customer_id,
            request.provider.erp_client,
            category_id,
            show_active_only,
        )
        if request.user.type == UserType.CUSTOMER:
            serializer = SusbscriptionGetSerializerForCustomerUser(
                subscriptions, many=True
            )
        else:
            serializer = SusbscriptionGetSerializer(subscriptions, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class SubscriptionUpdateAPIView(APIView):
    serializer_class = SusbscriptionUpdateSerializer
    queryset = SubscriptionData.objects.none()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        if request.user.type == UserType.CUSTOMER:
            serializer = SusbscriptionUpdateSerializerForCustomerUser(
                data=request.data, partial=partial
            )
        else:
            serializer = SusbscriptionUpdateSerializer(
                data=request.data, partial=partial
            )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, **kwargs)
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def perform_update(self, serializer, **kwargs):
        provider_id = self.request.provider.id
        customer_id = utils.get_customer_id(self.request, **kwargs)
        customer = get_object_or_404(
            Customer, provider=self.request.provider, id=customer_id
        )
        subscription_id = self.kwargs.get("subscription_id")
        SubscriptionService.update_subscription(
            provider_id, customer, subscription_id, serializer.data
        )


class SubscriptionProductMappingListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get_queryset(self):
        queryset = SubscriptionProductType.objects.filter(
            provider=self.request.provider
        ).prefetch_related("products")
        return queryset

    def perform_create(self, serializer):
        provider = self.request.provider
        to_be_created = serializer.validated_data.get("products")
        obj = SubscriptionProductType.objects.create(
            provider=provider, name=serializer.validated_data["name"]
        )
        products = [
            SubscriptionProductMapping(name=name, type=obj, provider=provider)
            for name in to_be_created
        ]
        duplicate = set(
            SubscriptionProductMapping.objects.filter(
                provider=provider, name__in=to_be_created
            ).values_list("name", flat=True)
        )
        if duplicate:
            raise ValidationError(
                {
                    "products": f"Products with the same name { ', '.join(list(duplicate))} already exists."
                }
            )
        SubscriptionProductMapping.objects.bulk_create(products)

    def get_serializer_class(self):
        if self.request.method in ["POST"]:
            return SubscriptionProductMappingCreateSerializer
        else:
            return SubscriptionProductTypeSerializer


class SubscriptionProductMappingListAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)
    serializer_class = SubscriptionProductTypeSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = SubscriptionProductType.objects.filter(
            provider=self.request.provider
        ).prefetch_related("products")
        return queryset


class SubscriptionProductMappingUpdateDestroyAPIView(
    generics.UpdateAPIView, generics.DestroyAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = SubscriptionProductMappingUpdateSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = SubscriptionProductType.objects.filter(
            provider=self.request.provider
        ).prefetch_related("products")
        return queryset

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        provider = self.request.provider
        instance = self.get_object()
        instance.name = serializer.validated_data.get("name")
        instance.save()
        products = serializer.validated_data.get("products")
        existing_products = list(self.get_object().products.values())
        to_be_deleted = [
            x.get("id")
            for x in list(
                filter(
                    lambda x: x.get("name") not in products, existing_products
                )
            )
        ]
        if to_be_deleted:
            SubscriptionProductMapping.objects.filter(
                id__in=to_be_deleted
            ).delete()
        existing_products = list(
            self.get_object().products.values_list("name", flat=True)
        )
        to_be_created = set(
            filter(lambda x: x not in existing_products, products)
        )
        if to_be_created:
            products = [
                SubscriptionProductMapping(
                    name=name, type=self.get_object(), provider=provider
                )
                for name in to_be_created
            ]
            existing_product_names = set(
                SubscriptionProductMapping.objects.filter(
                    provider=provider
                ).values_list("name", flat=True)
            )
            duplicate = to_be_created.intersection(existing_product_names)
            if duplicate:
                raise ValidationError(
                    {
                        "products": f"Products with the same name { ', '.join(list(duplicate))} already exists."
                    }
                )
            SubscriptionProductMapping.objects.bulk_create(products)
        return Response(status=status.HTTP_200_OK)


class SubscriptionImportAPIView(APIView):
    """
    API View to import Subscription from xlsx file.
    """

    def post(self, request, **kwargs):
        serializer = SubscriptionImportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_name = serializer.validated_data.get("file_name")
        manufacturer_id = serializer.validated_data.get("manufacturer_id")
        category_id = (
            request.provider.get_subscription_category_id_by_manufacturer(
                manufacturer_id
            )
        )
        data = (
            SubscriptionService.batch_create_or_update_subscriptions_from_file(
                request.provider.id,
                category_id,
                serializer.validated_data,
                file_name,
            )
        )
        if data is None:
            return Response(
                {"message": "Failed to import subscription."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(data=data, status=status.HTTP_200_OK)


class ReportDownloadStatsAPIView(generics.ListAPIView):
    serializer_class = ReportDownloadStatsSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        "user__first_name",
        "user__last_name",
        "report_type",
        "report_sub_type",
    )

    def get_queryset(self):
        queryset = (
            ReportDownloadStats.objects.select_related()
            .values("user", "report_type", "report_sub_type")
            .annotate(
                download_count=Count("id", output_field=IntegerField()),
                last_downloaded_on=Max(
                    "downloaded_on", output_field=DateTimeField()
                ),
                name=Concat(
                    "user__first_name",
                    Value(" "),
                    "user__last_name",
                    output_field=CharField(),
                ),
            )
            .filter(user__provider=self.request.provider)
            .order_by("-last_downloaded_on")
        )
        return queryset


class TimezoneListAPIView(APIView):
    """
    Returns the list of timezones
    """

    def get(self, *args, **kwargs):
        pretty_timezone_choices = []
        for tz in pytz.common_timezones:
            now = datetime.datetime.now(pytz.timezone(tz))
            ofs = now.strftime("%z")
            pretty_timezone_choices.append(
                (int(ofs), tz, "(GMT%s) %s" % (ofs, tz))
            )
        pretty_timezone_choices.sort()
        for i in range(len(pretty_timezone_choices)):
            pretty_timezone_choices[i] = pretty_timezone_choices[i][1:]

        data = [
            {"timezone": tz, "display_name": display_name}
            for tz, display_name in pretty_timezone_choices
        ]
        return Response(data=data, status=status.HTTP_200_OK)


class ManualDeviceTypeMappingListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ManualDeviceTypeMappingSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        queryset = ManualDeviceTypeMapping.objects.filter(provider=provider)
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)
        CacheService.invalidate_cache()


class ManualDeviceTypeMappingUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ManualDeviceTypeMappingSerializer

    def get_queryset(self):
        provider = self.request.provider
        queryset = ManualDeviceTypeMapping.objects.filter(provider=provider)
        return queryset

    def perform_update(self, serializer):
        serializer.save()
        CacheService.invalidate_cache()

    def perform_destroy(self, instance):
        instance.delete()
        CacheService.invalidate_cache()


class SubscriptionServiceProviderMappingListCreateAPIView(
    generics.ListCreateAPIView
):
    serializer_class = SubscriptionServiceProviderMappingListCreateSerializer
    pagination_class = None

    def get_queryset(self):
        provider = self.request.provider
        queryset = SubscriptionServiceProviderMapping.objects.filter(
            provider=provider
        )
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)
        CacheService.invalidate_cache()


class SubscriptionServiceProviderMappingUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = (
        SubscriptionServiceProviderMappingUpdateDestroySerializer
    )

    def get_queryset(self):
        provider = self.request.provider
        queryset = SubscriptionServiceProviderMapping.objects.filter(
            provider=provider
        )
        return queryset

    def perform_update(self, serializer):
        serializer.save()
        CacheService.invalidate_cache()

    def perform_destroy(self, instance):
        instance.delete()
        CacheService.invalidate_cache()


class ConnectwiseCallbackAPIView(APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        provider_id = self.kwargs.get("provider_id")
        provider = get_object_or_404(
            Provider.objects.filter(is_active=True), id=provider_id
        )
        res = ConnectwiseCallbackService.process(provider, request.data)
        if res:
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class PlainTextParser(BaseParser):
    """
    Plain text parser will parse the byte string object data into a string
    """

    media_type = "application/json; charset=UTF-8"

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representation of the body from the request.
        """
        data = stream.read()
        return data.decode("UTF-8")


class LogicMonitorCallbackAPIView(APIView):
    permission_classes = []

    parser_classes = [PlainTextParser]

    def post(self, request, *args, **kwargs):
        provider_id = self.kwargs.get("provider_id")
        provider = get_object_or_404(
            Provider.objects.filter(is_active=True), id=provider_id
        )
        res = LogicMonitorCallbackService.process(provider, request.data)
        if res:
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ListCWCommunicationTypes(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        erp_client: Connectwise = provider.erp_client
        communication_types = erp_client.get_communication_types()
        return Response(data=communication_types, status=status.HTTP_200_OK)


class ListCWPhoneCommunicationTypes(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        provider: Provider = request.provider
        erp_client: Connectwise = provider.erp_client
        communication_types = erp_client.get_phone_communication_types()
        return Response(data=communication_types, status=status.HTTP_200_OK)


class ConnectwisePhoneNumberMappingSettingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    serializer_class = ConnectwisePhoneNumberMappingSettingSerializer

    def get_queryset(self):
        return ConnectwisePhoneNumberMappingSetting.objects.filter(
            provider=self.request.provider
        )

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):
        try:
            ConnectwisePhoneNumberMappingSetting.objects.get(
                provider=self.request.provider
            )
        except ConnectwisePhoneNumberMappingSetting.DoesNotExist:
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError("Setting already exists for the Provider.")
        cw_customer_user_full_sync.apply_async(
            (self.request.provider.id,), queue="high"
        )

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)
        cw_customer_user_full_sync.apply_async(
            (self.request.provider.id,), queue="high"
        )


class UploadFileToDropboxAPIView(APIView):
    """
    API View to upload file to a customer sub-folder in Dropbox.
    """

    parser_classes = [
        MultiPartParser,
    ]
    permission_classes = [IsAuthenticated, IsProviderOrCustomerUser]

    def post(self, request, **kwargs):
        customer_id = None
        provider_id = self.request.provider.id
        customer_sub_folder = self.request.query_params.get("path")
        if not customer_sub_folder:
            raise NotFound("No customer sub-folder selected.")

        if self.request.user.type == UserType.PROVIDER:
            customer_id = self.kwargs.get("customer_id")
        elif self.request.user.type == UserType.CUSTOMER:
            customer_id = self.request.customer.id
        if not customer_id:
            raise NotFound("Customer not selected. Please select one.")

        try:
            provider = Provider.objects.get(id=provider_id)
        except Provider.DoesNotExist:
            logger.debug(f"Provider with given {provider_id=} does not exist!")
            return Response(
                {"message": "Could not upload file to Dropbox."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            logger.debug(f"Customer with given {customer_id=} does not exist!")
            return Response(
                {"message": "Could not upload file to Dropbox."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        uploaded_file = request.FILES.get("uploaded_file")
        file_name = generate_timestamped_file_name(uploaded_file.name)

        dropbox_client = DropboxIntegrationService.get_storage_api_client(
            provider
        )
        customer_folder_name = dropbox_client.get_dropbox_customer_folder_name(
            customer
        )
        dropbox_path = f"/{dropbox_client.APP_FOLDER_NAME}/{customer_folder_name}/{customer_sub_folder}/{file_name}"

        try:
            file_content = uploaded_file.read()
        except (FileNotFoundError, OSError) as err:
            logger.error(err)
            return Response(
                {
                    "message": "Error uploading file to Dropbox. Please try again later."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            dropbox_client.upload_file(file_content, dropbox_path)
            logger.info(f"Uploading {file_name=} to Dropbox")
        except Exception as err:
            logger.error(err)
            return Response(
                {
                    "message": "Error uploading file to Dropbox. Please try again later."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(
            {"message": "Successfully uploaded the file to Dropbox."},
            status=status.HTTP_200_OK,
        )


class CustomerTypesListAPIView(generics.ListAPIView):
    """
    API view to list CW customer types.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        customer_types: List[Dict] = provider.erp_client.get_company_types()
        return Response(data=customer_types, status=status.HTTP_200_OK)


class CustomerContactTypesListAPIView(generics.ListAPIView):
    """
    API view to list CW customer contact types.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        customer_contact_types: List[
            Dict
        ] = provider.erp_client.get_contact_types()
        return Response(data=customer_contact_types, status=status.HTTP_200_OK)


class VendorTypeCustomersListCreateAPIView(generics.ListCreateAPIView):
    """
    API view to list and create customers that are of type vendors.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = VendorTypeCustomerCreateSerializer

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            get_sow_vendor_onboarding_settings(self.request.provider)
        )
        vendor_status_ids: List[
            int
        ] = (
            sow_vendor_onboarding_setting.get_cw_status_ids_for_vendors_listing()
        )
        vendors: List[Dict] = provider.erp_client.get_vendor_type_customers(
            vendor_status_ids
        )
        return Response(data=vendors, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            get_sow_vendor_onboarding_settings(self.request.provider)
        )
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            vendor: Dict = provider.erp_client.create_vendor_type_customer(
                serializer.validated_data, sow_vendor_onboarding_setting
            )
            return Response(data=vendor, status=status.HTTP_201_CREATED)


class VendorContactCreateAPIView(generics.CreateAPIView):
    """
    API view to create customer contact for customer of type vendor.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = VendorContactCreateSerializer

    def get_cw_phone_number_mapping_setting(
        self,
    ) -> ConnectwisePhoneNumberMappingSetting:
        phone_number_mapping_setting: ConnectwisePhoneNumberMappingSetting = (
            ConnectwiseCommunicationItemsService(
                self.request.provider
            ).get_phone_number_mapping()
        )
        if not phone_number_mapping_setting:
            raise CWPhoneNumberMappingSettingNotConfigured()
        return phone_number_mapping_setting

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            customer_crm_id: int = serializer.validated_data.pop(
                "customer_crm_id"
            )
            phone_number_mapping_setting: ConnectwisePhoneNumberMappingSetting = (
                self.get_cw_phone_number_mapping_setting()
            )
            vendor_contact: Dict = provider.erp_client.create_customer_contact(
                customer_crm_id,
                serializer.validated_data,
                phone_number_mapping_setting,
            )
            # Send onboarding email to vendor primary contact
            contact_email: str = vendor_contact.get("email")
            send_onboarding_email_to_vendor_primary_contact.apply_async(
                (provider.id, contact_email), queue="high"
            )
            logger.info(
                f"Task to send onboarding email to vendor ID: {customer_crm_id} triggered for "
                f"provider: {provider.name}, ID: {provider.id}."
            )
            return Response(
                data=vendor_contact, status=status.HTTP_201_CREATED
            )


class CustomerStatusesListAPIView(generics.ListAPIView):
    """
    API view to list CW customer statuses.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        customer_statuses: List[
            Dict
        ] = provider.erp_client.get_customer_statuses()
        return Response(data=customer_statuses, status=status.HTTP_200_OK)


class TeamRolesListCreateAPIView(generics.ListCreateAPIView):
    """
    API view to list, create CW team roles.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = TeamRoleCreateSerializer

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        team_roles: List[Dict] = provider.erp_client.get_team_roles()
        return Response(data=team_roles, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            role: Dict = provider.erp_client.create_team_role(
                serializer.validated_data
            )
            return Response(data=role, status=status.HTTP_201_CREATED)


class AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve, udpate automated subscription notification setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = AutomatedSubscriptionNotificationSettingSerializer

    def get_object(self):
        try:
            setting: AutomatedSubscriptionNotificationSetting = (
                self.request.provider.subscription_notification_setting
            )
        except AutomatedSubscriptionNotificationSetting.DoesNotExist:
            raise NotFound(
                f"Automated Subscription Notification Setting not configured for the Provider!"
            )
        return setting

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            provider.subscription_notification_setting
        except AutomatedSubscriptionNotificationSetting.DoesNotExist:
            serializer.save(provider=provider)
        else:
            raise ValidationError(
                f"Automated Subscription Notification Setting already exists for the Provider!"
            )


class ConfigurationMappingListCreateAPIView(generics.ListCreateAPIView):
    """
    API view to list create configuration mapping.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = ConfigurationMappingSerializer

    def get_queryset(self):
        config_mappings: "QuerySet[ConfigurationMapping]" = (
            ConfigurationMapping.objects.filter(provider=self.request.provider)
        )
        return config_mappings

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class ConfigurationMappingRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    """
    API view to list create configuration mapping.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = ConfigurationMappingSerializer

    def get_object(self):
        mapping: ConfigurationMapping = get_object_or_404(
            ConfigurationMapping.objects.filter(
                provider_id=self.request.provider.id
            ),
            id=self.kwargs.get("pk"),
        )
        return mapping

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class SubscriptionNotificationTriggerAPIView(generics.CreateAPIView):
    """
    API view to send subscription notification to custoemrs.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = SubscriptionNotificationTriggerSerializer

    def post(self, request, *args, **kwargs):
        try:
            self.request.provider.subscription_notification_setting
        except AutomatedSubscriptionNotificationSetting.DoesNotExist:
            raise SubscriptionNotificationSettingNotConfigured()
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            all_customers: bool = serializer.validated_data.get(
                "all_customers"
            )
            customer_crm_ids: List[int] = serializer.validated_data.get(
                "customer_crm_ids", []
            )
            task = send_subscription_notification_email.apply_async(
                (
                    self.request.provider.id,
                    all_customers,
                    customer_crm_ids,
                ),
                queue="high",
            )
            return Response(
                data=dict(task_id=task.id), status=status.HTTP_202_ACCEPTED
            )


class SubscriptionNotificationEmailPreviewAPIView(generics.ListAPIView):
    """
    API view to preview subscription notification email.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        try:
            self.request.provider.subscription_notification_setting
        except AutomatedSubscriptionNotificationSetting.DoesNotExist:
            raise SubscriptionNotificationSettingNotConfigured()
        preview: str = SubscriptionNotificationService(
            self.request.provider
        ).generate_subscription_notification_email_preview()
        return Response(data=dict(html=preview), status=status.HTTP_200_OK)
