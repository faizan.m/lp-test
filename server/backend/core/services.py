import copy
import datetime
import os
import tempfile
from typing import Any, Dict, Generator, List, Tuple, Union, Optional
import asyncio
import aiohttp
import numpy as np
import pandas as pd
from django.conf import settings
from django.db import transaction, IntegrityError, DataError
from django.db.models import Count, F
from django.template.loader import get_template
from rest_framework.exceptions import ValidationError
from weasyprint import CSS, HTML
from aiohttp.client import ClientSession
from accounts.models import Customer, Provider, User
from caching.service import CacheService, caching
from core.integrations import DropBox, LogicMonitor
from core.integrations.file_storage.dropbox.utils import get_valid_folder_name
from core.models import (
    DeviceInfo,
    DeviceManufacturerData,
    DeviceManufacturerMissingData,
    DeviceTypes,
    IntegrationType,
    ManualDeviceTypeMapping,
    ProviderIntegration,
    SubscriptionData,
    SubscriptionProductMapping,
    SubscriptionProductType,
    SubscriptionServiceProviderMapping,
    AutomatedSubscriptionNotificationSetting,
)
from core.tasks.connectwise.tasks import (
    batch_create_or_update_devices,
    batch_create_or_update_subscriptions,
    bulk_update_devices_by_value,
    bulk_update_devices_categories,
)
from core.tasks.services import TaskService
from core.utils import (
    generate_random_id,
    group_list_of_dictionary_by_key,
    remove_keys_with_none_and_empty_value,
    device_date_formats,
    convert_date_string_to_date_object,
    chunks_of_list_with_max_item_count,
)
from exceptions import exceptions
from exceptions.exceptions import APIError
from utils import get_app_logger
from core.integrations.erp.connectwise.connectwise import (
    CWConditions,
    CWFieldsFilter,
    Condition,
    Operators,
)
from django.utils import timezone
from core.email import AutomatedSubscriptionNotificationEmail
from django.template.loader import render_to_string
from celery.utils.log import get_task_logger

# Get an instance of a logger
logger = get_app_logger(__name__)
task_logger = get_task_logger(__name__)


CONFIG_ENDPOINT: str = (
    f"https://cw.lookingpoint.com/v4_6_release/apis/3.0/company/configurations"
)


class DeviceService(object):
    DEVICE_TYPES_DATA = None

    @classmethod
    def get_all_devices_for_provider(cls, erp_client, category_id):
        fields = ["id", "serialNumber", "name"]
        devices = erp_client.get_devices(
            category_id=category_id, required_fields=fields
        )
        return devices

    @classmethod
    def get_device_list(
        cls,
        provider_id,
        customer_id,
        erp_client,
        category_id,
        show_active_only=True,
        show_full_info=True,
        apply_device_display_filters=True,
    ):
        """
        Returns list of devices.
        :param provider_id:
        :param show_active_only:
        :param erp_client: ERP Client object.
        :param customer_id: Customer ID.
        :param category_id: Device Category ID.
        :return:
        """
        cache_key = CacheService.CACHE_KEYS.DEVICE_LIST.format(
            provider_id, customer_id
        )
        if CacheService.exists(cache_key) and isinstance(customer_id, int):
            data = CacheService.get_data(
                cache_key, erp_client, show_active_only, show_full_info
            )
            if data is not None:
                return data

        devices = DeviceService.get_devices(
            erp_client, customer_id, category_id, show_active_only
        )
        if apply_device_display_filters:
            devices = DeviceService._apply_device_display_filters(
                devices, provider_id
            )
        if show_full_info:
            devices = cls._add_device_eol_data(devices, provider_id)
            devices = cls._add_device_type(devices)
            devices = cls._add_device_info(provider_id, devices)
            devices = cls._add_non_cisco_flag(provider_id, devices)
            devices = cls._add_non_cisco_device_data(devices, provider_id)
            devices = cls._add_manual_device_types(devices, provider_id)
        if isinstance(customer_id, int):
            CacheService.set_data(
                cache_key,
                devices,
                erp_client,
                show_active_only,
                show_full_info,
            )
        return devices

    @classmethod
    def get_devices(
        cls, erp_client, customer_id, category_id, show_active_only
    ):
        if show_active_only:
            devices = erp_client.get_active_devices(
                customer_id=customer_id, category_id=category_id
            )
        else:
            devices = erp_client.get_devices(
                customer_id=customer_id, category_id=category_id
            )
        return devices

    @classmethod
    def _add_device_info(cls, provider_id, devices):
        """
        This function adds device info to devices.
        """
        devices_dict = group_list_of_dictionary_by_key(devices, "id")
        _device_info_dict = list(
            DeviceInfo.objects.filter(
                provider_id=provider_id,
                device_crm_id__in=tuple(devices_dict.keys()),
            ).values(
                "serial_number",
                "device_crm_id",
                "instance_id",
                "monitoring_data",
                "customer_notes",
                "asset_tag",
            )
        )
        _device_info_dict = group_list_of_dictionary_by_key(
            _device_info_dict, "device_crm_id"
        )
        for _device_id, _device_data in devices_dict.items():
            _device_info_data = _device_info_dict.get(_device_id)
            if _device_info_data:
                _device_data[0].update(_device_info_data[0])
            devices_dict[_device_id] = _device_data[0]
        return list(devices_dict.values())

    @classmethod
    def _add_non_cisco_flag(cls, provider_id, devices):
        """
        Adds non_cisco flag to devices. If True, the device is a non cisco device
        Returns:

        """
        provider = Provider.objects.get(id=provider_id)
        cisco_manufacturer_ids = provider.get_cisco_manufacturer_ids()
        cisco_category_ids = provider.get_cisco_category_ids()
        if any([cisco_manufacturer_ids, cisco_category_ids]):
            for device in devices:
                non_cisco = True
                category_id = (
                    int(device.get("category_id"))
                    if device.get("category_id")
                    else None
                )
                manufacturer_id = (
                    int(device.get("manufacturer_id"))
                    if device.get("manufacturer_id")
                    else None
                )
                if (
                    manufacturer_id in cisco_manufacturer_ids
                    or category_id in cisco_category_ids
                ):
                    non_cisco = False
                device.update({"non_cisco": non_cisco})
        return devices

    @classmethod
    def get_device_list_with_instance_id(
        cls,
        erp_client,
        customer_id,
        category_id,
        show_active_only=True,
        provider_id=None,
    ):
        devices = cls.get_device_list(
            provider_id=provider_id,
            customer_id=customer_id,
            erp_client=erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
        )
        for device in devices:
            instance_id = cls.get_instance_id(device.get("id"), provider_id)
            device.update(dict(instance_id=instance_id))
        return devices

    @classmethod
    def get_device(cls, erp_client, device_id, provider_id=None):
        """
        Returns device details for single device.
        """
        # fetch device details
        device = erp_client.get_device(device_id)
        # add device manufacturer data to device
        device = cls._add_device_eol_data(device, provider_id)
        device = cls._add_non_cisco_flag(provider_id, device)
        device = cls._add_non_cisco_device_data(device, provider_id)
        # add device type
        cls._add_device_type(device)
        device = cls._add_manual_device_types(device, provider_id)

        # add device manufacturer api call meta info
        device = device[0]
        manufacturer_api_meta = (
            cls._get_device_manufacturer_api_call_meta_info(
                device.get("serial_number"), provider_id
            )
        )
        device.update(manufacturer_api_meta)
        device = cls.get_device_info(device, provider_id)
        return device

    @classmethod
    def get_device_info(cls, device, provider_id):
        try:
            device_info = DeviceInfo.objects.get(
                provider_id=provider_id, device_crm_id=device.get("id")
            )
            instance_id = device_info.instance_id
            monitoring_data = device_info.monitoring_data
            customer_notes = device_info.customer_notes
            asset_tag = device_info.asset_tag
            device.update(
                dict(
                    instance_id=instance_id,
                    monitoring_data=monitoring_data,
                    customer_notes=customer_notes,
                    asset_tag=asset_tag,
                )
            )
            return device
        except DeviceInfo.DoesNotExist:
            return device

    @classmethod
    def _get_device_manufacturer_api_call_meta_info(
        cls, serial_number, provider_id
    ):
        """
        Returns meta info related to Manufacturer API calls made for the serial number.
        :param serial_number: Device Serial Number
        :param provider_id: Provider Id
        :return:
        """
        manufacturer_api_meta = dict(retry_count=-1, max_retry_reached=False)
        if serial_number:
            try:
                manufacturer_api_call_meta = (
                    DeviceManufacturerMissingData.objects.get(
                        provider_id=provider_id, serial_number=serial_number
                    )
                )
                missing_data_retry_count = (
                    manufacturer_api_call_meta.retry_count
                )
                max_retry_count_reached = (
                    True
                    if missing_data_retry_count
                    == settings.CISCO_MISSING_DATA_MAX_RETRY
                    else False
                )
                manufacturer_api_meta["retry_count"] = missing_data_retry_count
                manufacturer_api_meta[
                    "max_retry_reached"
                ] = max_retry_count_reached
                manufacturer_api_meta[
                    "is_serial_number_invalid"
                ] = manufacturer_api_call_meta.is_invalid
            except DeviceManufacturerMissingData.DoesNotExist:
                pass
        return manufacturer_api_meta

    @classmethod
    def get_expiring_devices(
        cls, erp_client, customer_id, category_id, provider_id
    ):
        """
        Returns list of devices with contract status= EXPIRING
        """
        devices = cls.get_device_list(
            provider_id=provider_id,
            customer_id=customer_id,
            erp_client=erp_client,
            category_id=category_id,
        )
        return list(
            filter(
                lambda device: device.get("contract_status")
                == str(DeviceManufacturerData.EXPIRING),
                devices,
            )
        )

    @classmethod
    def get_instance_id(cls, device_id, provider_id):
        try:
            device_info = DeviceInfo.objects.get(
                provider_id=provider_id, device_crm_id=device_id
            )
            return device_info.instance_id
        except DeviceInfo.DoesNotExist:
            return None

    @classmethod
    def get_expired_devices(
        cls, erp_client, customer_id, category_id, provider_id
    ):
        """
        Returns list of devices with contract status = EXPIRED
        :param erp_client:
        :param customer_id:
        :param category_id:
        :return:
        """
        devices = cls.get_device_list(
            provider_id=provider_id,
            customer_id=customer_id,
            erp_client=erp_client,
            category_id=category_id,
        )
        return list(
            filter(
                lambda device: device.get("contract_status")
                == str(DeviceManufacturerData.EXPIRED),
                devices,
            )
        )

    @classmethod
    def get_expiring_devices_count(
        cls, erp_client, customer_id, category_id, provider_id
    ):
        """
        Returns count of expiring devices
        """
        expiring_devices = cls.get_expiring_devices(
            erp_client, customer_id, category_id, provider_id
        )
        return len(expiring_devices)

    @classmethod
    def get_expired_devices_count(
        cls, erp_client, customer_id, category_id, provider_id
    ):
        """
        Returns count of expiring devices
        """
        expired_devices = cls.get_expired_devices(
            erp_client, customer_id, category_id, provider_id
        )
        return len(expired_devices)

    @classmethod
    def _add_device_eol_data(cls, device_data, provider_id):
        """
        Adds Data from Device manufacturer to device_data
        :param client: Device Manufacturer Service client
        :param device_data: List of devices.
        :return:
        """
        if isinstance(device_data, dict):
            device_data = [device_data]
        if device_data:
            device_data_by_serial_number = group_list_of_dictionary_by_key(
                device_data, "serial_number"
            )
            device_serial_numbers = list(device_data_by_serial_number.keys())
            device_manufacturer_data = DeviceManufacturerData.objects.filter(
                device_serial_number__in=set(device_serial_numbers),
                provider_id=provider_id,
            ).values("device_serial_number", "data", "updated_on")
            device_manufacturer_data = group_list_of_dictionary_by_key(
                device_manufacturer_data, "device_serial_number"
            )
            present_in_missing_data_table = (
                DeviceManufacturerMissingData.objects.filter(
                    provider_id=provider_id,
                    serial_number__in=set(device_serial_numbers),
                ).values_list("serial_number", flat=True)
            )
            missing_data_serials = []
            for data in device_data:
                _eol_data = cls._get_eol_data(
                    data,
                    device_manufacturer_data,
                    missing_data_serials,
                    present_in_missing_data_table,
                )
                data.update(_eol_data)
            # Fetch instance numbers list for missing serial numbers
            serial_numbers_which_are_instance_numbers = (
                DeviceInfo.objects.filter(
                    provider_id=provider_id,
                    is_serial_instance_id=True,
                    instance_id__in=missing_data_serials,
                ).values_list("serial_number", flat=True)
            )

            # Remove instance ids from missing_data_serials
            missing_data_serials = set(missing_data_serials) - set(
                serial_numbers_which_are_instance_numbers
            )

            # Make entry to missing data table
            missing_data_objects = []
            for serial_number in set(missing_data_serials):
                _device_data = device_data_by_serial_number.get(serial_number)[
                    0
                ]
                if len(serial_number) > 100:
                    logger.info(
                        f"Serial Number length > 100. Device Data: {_device_data}"
                    )
                    continue
                obj = DeviceManufacturerMissingData(
                    serial_number=serial_number,
                    provider_id=provider_id,
                    device_crm_id=_device_data.get("id"),
                    device_category_id=_device_data.get("category_id"),
                )
                missing_data_objects.append(obj)
            try:
                DeviceManufacturerMissingData.objects.bulk_create(
                    missing_data_objects
                )
            except (DataError, IntegrityError) as e:
                logger.info(e)
        return device_data

    @classmethod
    def generate_device_report(cls, devices):
        df = pd.DataFrame.from_dict(devices, dtype=str)
        df = df.replace("\.0", "", regex=True)
        reorder_list = [
            str(DeviceManufacturerData.EXPIRED),
            str(DeviceManufacturerData.EXPIRING),
            str(DeviceManufacturerData.ACTIVE),
            str(DeviceManufacturerData.NOT_AVAILABLE),
        ]
        df.contract_status = pd.Categorical(
            df.contract_status, categories=reorder_list
        )
        if "EOL_date" in df.columns:
            df["EOL_date"] = pd.to_datetime(
                df["EOL_date"], errors="coerce"
            ).dt.strftime("%m/%d/%Y")
            df["EOL_date"].replace({"NaT": "-"}, inplace=True)
        if "expiration_date" in df.columns:
            df["expiration_date"] = pd.to_datetime(
                df["expiration_date"], errors="coerce"
            ).dt.strftime("%m/%d/%Y")
            df["expiration_date"].replace({"NaT": "-"}, inplace=True)
        df.sort_values(
            by=["site_id", "contract_status"],
            ascending=[False, True],
            inplace=True,
        )
        df = df.replace((np.nan, "nan"), "-", regex=True)
        df = df.reset_index()
        template = get_template("reports/report_template.html")
        template_vars = {
            "title": "Device Report",
            "table_html": df.to_dict("result"),
        }

        html_out = template.render(template_vars)
        TEMP_DIR = tempfile.gettempdir()
        file_path = os.path.join(TEMP_DIR, f"{generate_random_id(15)}.pdf")
        HTML(string=html_out).write_pdf(
            target=file_path,
            stylesheets=[
                CSS(os.path.join(settings.STATIC_ROOT, "css/cash.css"))
            ],
        )
        return file_path

    @classmethod
    def add_site_address_to_device_data(cls, provider_id, devices):
        provider = Provider.objects.get(id=provider_id)
        erp_client = provider.erp_client
        devices_by_customer_id_dict = group_list_of_dictionary_by_key(
            devices, "customer_id"
        )
        for _customer_id, _device_list in devices_by_customer_id_dict.items():
            _devices_by_site_id_dict = group_list_of_dictionary_by_key(
                _device_list, "site_id"
            )
            _site_ids = list(_devices_by_site_id_dict.keys())
            if _site_ids:
                _customer_sites = (
                    erp_client.get_customer_complete_site_addresses(
                        _customer_id, site_ids=tuple(_site_ids)
                    )
                )
                for _device in _device_list:
                    address = _customer_sites.get(_device.get("site_id"))
                    _device.update({"site_address": address})
        return devices

    @classmethod
    def _get_eol_data(
        cls,
        data,
        device_manufacturer_data,
        missing_data_serials,
        present_in_missing_data_table,
    ):
        """ """
        eol_data = {}
        serial_number = data.get("serial_number")
        if serial_number:
            _data = device_manufacturer_data.get(serial_number)
            if (
                not _data
                and serial_number not in present_in_missing_data_table
            ):
                missing_data_serials.append(serial_number)
            elif _data:
                _data = _data[0]
                for _mnf_data_key in ["eox_data", "search_data"]:
                    _mnf_data = _data.get("data").get(_mnf_data_key)
                    if _mnf_data:
                        _mnf_data = remove_keys_with_none_and_empty_value(
                            _mnf_data
                        )
                        eol_data.update(_mnf_data)
                eol_data.update({"updated_on": _data.get("updated_on")})
            eol_data.update(
                DeviceManufacturerData.get_device_contract_status(
                    data.get("expiration_date")
                )
            )
        return eol_data

    @classmethod
    def load_device_types_table(cls, model_numbers):
        """
        Returns data from DeviceTypes Model.
        :return:
        """
        device_types = DeviceTypes.objects.filter(
            product_id__in=model_numbers
        ).values()
        device_types_dict = {}
        for item in device_types:
            device_types_dict[item["product_id"]] = {
                "device_type_id": item["id"],
                "device_type": item["type"],
                "suggested_software_update": item["suggested_software_update"],
                **item["data"],
            }
        return device_types_dict

    @classmethod
    def get_manual_type_mappings(
        cls, provider_id: int, model_numbers: List[str]
    ) -> Dict[str, str]:
        """
        Returns added manual model and type mapping. Also evaluates prefix.

        """
        manual_type_mappings: Dict[str, str] = dict(
            ManualDeviceTypeMapping.objects.filter(
                provider_id=provider_id
            ).values_list("type", "model_ids")
        )
        manual_type_mappings = {
            model: _type
            for _type, models in manual_type_mappings.items()
            for model in models
        }
        matched_manual_type_mappings: Dict[str, str] = dict()
        for manual_model, _type in manual_type_mappings.items():
            if manual_model.endswith("*"):
                for model in model_numbers:
                    if model.startswith(manual_model[:-1]):
                        matched_manual_type_mappings[model] = _type
            elif manual_model in model_numbers:
                matched_manual_type_mappings[manual_model] = _type

        return matched_manual_type_mappings

    @classmethod
    def get_device_types(
        cls, model_numbers: List, provider_id: int
    ) -> Dict[str, str]:
        """
        Returns dict containing mapping of model number to device types.
        This combines data coming from API and the manual mappings added.

        """
        device_types_from_device_info_table: Dict[str, str] = dict(
            DeviceTypes.objects.filter(
                product_id__in=model_numbers
            ).values_list("product_id", "type")
        )
        matched_manual_type_mappings = cls.get_manual_type_mappings(
            provider_id, model_numbers
        )
        device_types = {
            **device_types_from_device_info_table,
            **matched_manual_type_mappings,
        }
        return device_types

    @classmethod
    def _add_device_type(cls, devices):
        """
        Adds type of device based on the device_name key.
        Returns updated device_data.
        """
        devices = devices or []
        model_numbers = (device.get("model_number") for device in devices)
        device_types_data = cls.load_device_types_table(model_numbers)
        default_type_data = dict(device_type_id="", device_type="")
        for device in devices:
            type_data = device_types_data.get(device.get("model_number"))
            if not type_data:
                type_data = default_type_data
            device.update(type_data)
        return devices

    @classmethod
    def _add_non_cisco_device_data(cls, devices, provider_id):
        """
        Populate keys for non cisco devices from monitoring data
        """
        for device in devices:
            if device.get("non_cisco"):
                device = cls.get_device_info(device, provider_id)
                if device.get("monitoring_data"):
                    device["display_name"] = (
                        device.get("monitoring_data", {})
                        .get(settings.LOGICMONITOR, {})
                        .get("display_name")
                    )
                    device["model_number"] = (
                        device.get("monitoring_data", {})
                        .get(settings.LOGICMONITOR, {})
                        .get("model")
                    )
        return devices

    @classmethod
    def _add_manual_device_types(cls, devices, provider_id):
        """
        Reads explicit mapping for model and type and populates device_type
        for both cisco and non-cisco devices
        """
        model_numbers = {
            device.get("model_number")
            for device in devices
            if device.get("model_number") is not None
        }
        manual_type_mappings = cls.get_manual_type_mappings(
            provider_id, list(model_numbers)
        )
        for device in devices:
            device_type = manual_type_mappings.get(device.get("model_number"))
            if device_type:
                device["device_type"] = device_type
        return devices

    @staticmethod
    def _apply_device_display_filters(
        devices: List[Dict], provider_id: int
    ) -> List[Dict]:
        """
        Apply asset management filters added in the settings. Remove the devices
        which matches the filter and return the remaining filters.

        """
        filtered_devices: List[Dict] = []
        name_filters: Dict = {}
        serial_number_filter: Dict = {}
        provider: Provider = Provider.objects.get(id=provider_id)
        device_ui_filters: Dict = provider.get_device_ui_filters()
        if device_ui_filters:
            name_filters = device_ui_filters.get("name_filters")
            serial_number_filter = device_ui_filters.get("serial_filters")
        if any([name_filters, serial_number_filter]):
            for device in devices:
                serial_number: str = device.get("serial_number")
                device_name: str = device.get("device_name")
                if serial_number:
                    serial_number_matches: Generator[bool, Any, None] = (
                        serial_number.startswith(serial_filter)
                        for serial_filter in serial_number_filter
                    )
                    if any(serial_number_matches):
                        continue
                if device_name:
                    name_matches: Generator[bool, Any, None] = (
                        device_name.startswith(name_filter)
                        for name_filter in name_filters
                    )
                    if any(name_matches):
                        continue
                filtered_devices.append(device)
        else:
            return devices
        return filtered_devices

    @classmethod
    def filter_devices_data(cls, devices, filter_conditions):
        """

        :param devices:
        :param filter_conditions:
        :return:
        """
        filtered_devices = devices
        for key, value in filter_conditions.items():
            filtered_devices = filter(
                lambda device: device.get(key, None) in value, filtered_devices
            )
            filtered_devices = list(filtered_devices)
        return list(filtered_devices)

    @classmethod
    def create_device(
        cls, erp_client, customer_id, category_id, provider_id, data
    ):
        """
        Creates a new device in ERP.
        Returns two boolean values: (is_duplicate, is_created)
        is_duplicate: True If a device with the same serial_number exits.
        is_created: True If the device has been created
        """
        device_serial_numbers = erp_client.get_device_serial_numbers(
            customer_id, category_id
        )
        serial_number = data["serial_number"]
        if serial_number in device_serial_numbers:
            return True, False
        else:
            provider = Provider.objects.get(id=provider_id)
            if provider.is_configured:
                try:
                    TaskService.sync_device_data_on_create(
                        provider_id,
                        data.get("manufacturer_id"),
                        serial_number,
                        False,
                        True,
                    )
                    try:
                        device = DeviceManufacturerData.objects.get(
                            device_serial_number=serial_number,
                            provider_id=provider_id,
                        )
                        for _mnf_data_key in ["eox_data", "search_data"]:
                            _mnf_data = device.data.get(_mnf_data_key, {})
                            data.update(_mnf_data)
                        if data and data.get("expiration_date"):
                            data[
                                "expiration_date"
                            ] = datetime.datetime.strftime(
                                datetime.datetime.strptime(
                                    data["expiration_date"], "%Y-%m-%d"
                                ),
                                "%Y-%m-%dT%H:%M:%SZ",
                            )
                    except DeviceManufacturerData.DoesNotExist:
                        pass
                except ProviderIntegration.DoesNotExist:
                    pass
                if data and data.get("installation_date"):
                    data["installation_date"] = data.get(
                        "installation_date"
                    ).strftime("%Y-%m-%dT%H:%M:%SZ")
            device_data = erp_client.add_device(
                customer_id, category_id, data=data
            )
        if device_data:
            serial_number = device_data.get("serial_number")
            instance_id = data.get("instance_id")
            _device_info_data = dict(
                provider=Provider.objects.get(id=provider_id),
                serial_number=serial_number,
                instance_id=instance_id,
                is_serial_instance_id=True
                if instance_id == serial_number
                else False,
                device_crm_id=device_data.get("id"),
                category_id=category_id,
                customer_notes=data.get("customer_notes", None),
                asset_tag=data.get("asset_tag", None),
            )
            cls.device_info_update_or_create(_device_info_data)

        CacheService.invalidate_data(
            [
                CacheService.CACHE_KEYS.DEVICE_LIST.format(
                    provider_id, customer_id
                )
            ]
        )
        return False, True

    @classmethod
    def device_info_update_or_create(cls, _device_info_data):
        provider = _device_info_data.get("provider")
        device_crm_id = _device_info_data.pop("device_crm_id")
        device_info = DeviceInfo.objects.update_or_create(
            device_crm_id=device_crm_id,
            provider=provider,
            defaults=_device_info_data,
        )
        return device_info

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.DEVICE_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def update_device(cls, provider_id, customer_id, device_id, device_data):
        provider = Provider.objects.get(id=provider_id)
        if customer_id:
            cw_device = cls.get_device(
                provider.erp_client, device_id, provider_id
            )
            if customer_id != cw_device.get("customer_id"):
                raise ValidationError("Device belongs to a different Customer")
        device = provider.erp_client.update_device(device_id, device_data)
        serial_number = device_data.get("serial_number")
        instance_id = device_data.get("instance_id")
        category_id = device.get("category_id")
        _device_info_data = dict(
            provider=Provider.objects.get(id=provider_id),
            serial_number=serial_number,
            instance_id=instance_id,
            is_serial_instance_id=True
            if instance_id == serial_number
            else False,
            device_crm_id=device_id,
            customer_notes=device_data.get("customer_notes", None),
            asset_tag=device_data.get("asset_tag", None),
            category_id=category_id,
        )
        cls.device_info_update_or_create(_device_info_data)
        device.update(dict(instance_id=instance_id))
        return device

    @classmethod
    def update_device_contracts(
        cls, provider_id, customer_id, device_ids, value
    ):
        task = bulk_update_devices_by_value.delay(
            provider_id,
            customer_id,
            device_ids=device_ids,
            value=value,
            field_name="tagNumber",
        )
        return {"task_id": task.task_id}

    @classmethod
    def update_device_categories(
        cls, provider_id, customer_id, device_ids, category_id
    ):
        task = bulk_update_devices_categories.delay(
            provider_id,
            customer_id,
            device_ids=device_ids,
            category_id=category_id,
        )
        return {"task_id": task.task_id}

    @classmethod
    def update_device_status(cls, provider_id, customer_id, device_ids, value):
        task = bulk_update_devices_by_value.delay(
            provider_id,
            customer_id,
            device_ids=device_ids,
            value=value,
            field_name="status/id",
        )
        return {"task_id": task.task_id}

    @classmethod
    def update_device_sites(
        cls, provider_id, customer_id, erp_client, device_ids, site_id
    ):
        sites = erp_client.get_customer_sites(customer_id=customer_id)
        sites = group_list_of_dictionary_by_key(sites, "site_id")
        if site_id not in sites:
            raise ValidationError(
                "Requested Site Id does not exist for the Customer"
            )
        task = bulk_update_devices_by_value.delay(
            provider_id,
            customer_id,
            device_ids=device_ids,
            value=site_id,
            field_name="site/id",
        )
        return {"task_id": task.task_id}

    @classmethod
    def update_device_manufacturers(
        cls, provider_id, customer_id, erp_client, device_ids, manufacturer_id
    ):
        manufacturers = erp_client.get_manufacturers()
        manufacturers = group_list_of_dictionary_by_key(manufacturers, "id")
        if manufacturer_id not in manufacturers:
            raise ValidationError("Requested Manufacturer Id does not exist")
        task = bulk_update_devices_by_value.delay(
            provider_id,
            customer_id,
            device_ids=device_ids,
            value=manufacturer_id,
            field_name="manufacturer/id",
        )
        return {"task_id": task.task_id}

    @classmethod
    def get_combined_monitoring_data(cls, monitoring_data):
        """
        Combine Monitoring Data from different sources
        Args:
            monitoring_data: Dict containing monitoring data

        Returns: Dict

        """
        monitoring_data = monitoring_data or {}
        solarwinds_data = monitoring_data.pop(settings.SOLARWINDS, {})
        logicmonitor_data = monitoring_data.pop(settings.LOGICMONITOR, {})
        monitoring_data.update(logicmonitor_data)
        monitoring_data.update(solarwinds_data)
        return monitoring_data

    @classmethod
    def batch_create_or_update_devices_from_file(
        cls, provider_id, customer_id, data, file_name
    ):
        task = batch_create_or_update_devices.apply_async(
            (provider_id, customer_id, data, file_name), queue="high"
        )
        return {"task_id": task.task_id}

    @classmethod
    def reset_device_manufacturer_api_retry_count(
        cls, provider_id, serial_number
    ):
        """
        Reset Device manufacturer api call retry count
        :param provider_id: Provider Id
        :param serial_number: Device Serial Number
        :return: None
        """
        DeviceManufacturerMissingData.objects.filter(
            provider_id=provider_id, serial_number=serial_number
        ).update(retry_count=0)

    def device_info_bulk_create_or_update(
        cls, _device_info_data_list, provider_id
    ):
        _device_info_data_dict = group_list_of_dictionary_by_key(
            _device_info_data_list, "device_crm_id"
        )
        _device_info_data_dict_device_crm_ids = _device_info_data_dict.keys()
        _device_info_device_crm_ids_for_update = DeviceInfo.objects.filter(
            provider_id=provider_id,
            device_crm_id__in=_device_info_data_dict_device_crm_ids,
        ).values_list("device_crm_id", flat=True)
        _device_info_device_crm_ids_for_create = list(
            set(_device_info_data_dict_device_crm_ids)
            - set(_device_info_device_crm_ids_for_update)
        )
        _device_info_dict_for_create = {
            key: _device_info_data_dict[key][0]
            for key in _device_info_device_crm_ids_for_create
        }

        _device_info_dict_for_update = {
            key: _device_info_data_dict[key][0]
            for key in _device_info_device_crm_ids_for_update
        }
        if _device_info_dict_for_create:
            _device_info_objects = [
                DeviceInfo(**value)
                for key, value in _device_info_dict_for_create.items()
            ]
            DeviceInfo.objects.bulk_create(_device_info_objects)

        if _device_info_dict_for_update:
            lambda data: DeviceInfo.objects.update(
                **data
            ), _device_info_dict_for_update.values()

    @classmethod
    def get_customer_reporting_types(cls, is_ms_customer=True):
        LIFECYCLE_MANAGEMENT_REPORT = "LIFECYCLE_MANAGEMENT_REPORT"
        DEVICE_COVERAGE_REPORT = "DEVICE_COVERAGE_REPORT"
        DEVICE_REPORT = "DEVICE_REPORT"
        SOFTWARE_MANAGEMENT_REPORT = "SOFTWARE_MANAGEMENT_REPORT"

        eol_device_report = dict(
            name="End Of Life Devices",
            category=LIFECYCLE_MANAGEMENT_REPORT,
            display_name="Lifecycle management report",
            type="REPORT01",
        )
        eol_within_12_months_device_report = dict(
            name="Coming End Of Life Devices (within 12 months)",
            category=LIFECYCLE_MANAGEMENT_REPORT,
            display_name="Lifecycle management report",
            type="REPORT02",
        )
        eol_more_than_12_months_device_report = dict(
            name="Coming End Of Life Devices (> 12 months)",
            category=LIFECYCLE_MANAGEMENT_REPORT,
            display_name="Lifecycle management report",
            type="REPORT03",
        )
        exp_devices = dict(
            name="Expired Devices",
            category=DEVICE_COVERAGE_REPORT,
            display_name="Device coverage report",
            type="REPORT04",
        )
        expiring_in_90_days_devices = dict(
            name="Expiring Devices (within 90 days)",
            category=DEVICE_COVERAGE_REPORT,
            display_name="Device coverage report",
            type="REPORT05",
        )
        expiring_in_12_months_devices = dict(
            name="Expiring Devices (within 12 months)",
            category=DEVICE_COVERAGE_REPORT,
            display_name="Device coverage report",
            type="REPORT06",
        )
        device_report = dict(
            name="All Device Report",
            category=DEVICE_REPORT,
            display_name="Device report",
            type="REPORT07",
        )
        soft_managed_devices_report = dict(
            name="Managed Devices Report",
            category=SOFTWARE_MANAGEMENT_REPORT,
            display_name="Managed Devices Report",
            type="REPORT08",
        )
        soft_all_devices_report = dict(
            name="All Devices Report",
            category=SOFTWARE_MANAGEMENT_REPORT,
            display_name="All Devices Report",
            type="REPORT09",
        )

        REPORTS = dict(
            LIFECYCLE_MANAGEMENT_REPORT=dict(
                REPORT01=eol_device_report,
                REPORT02=eol_within_12_months_device_report,
                REPORT03=eol_more_than_12_months_device_report,
            ),
            DEVICE_COVERAGE_REPORT=dict(
                REPORT04=exp_devices,
                REPORT05=expiring_in_90_days_devices,
                REPORT06=expiring_in_12_months_devices,
            ),
            DEVICE_REPORT=dict(REPORT07=device_report),
        )
        if is_ms_customer:
            REPORTS.update(
                SOFTWARE_MANAGEMENT_REPORT=dict(
                    REPORT08=soft_managed_devices_report,
                    REPORT09=soft_all_devices_report,
                )
            )
        return REPORTS


class DashboardService(object):
    @classmethod
    def get_providers_count(cls):
        """
        Returns Count of Providers
        """
        return Provider.objects.filter(is_active=True).count()

    @classmethod
    def get_top_five_providers_by_customer_count(cls):
        """
        Returns top 5 providers by customer count
        :return:
        """
        queryset = (
            Customer.objects.filter(is_active=True)
            .values("provider_id")
            .annotate(name=F("provider_id__name"), customers_count=Count("id"))
            .values("customers_count", "name")
            .order_by("-customers_count")[:5]
        )
        return list(queryset)

    @classmethod
    def get_top_five_providers_by_users_activity(cls):
        """
        Returns top 5 providers by most number of active customer users who have logged in at least once.
        """
        queryset = (
            User.customers.filter(last_login__isnull=False, is_active=True)
            .values("provider_id")
            .annotate(
                name=F("provider_id__name"), users_count=Count("provider_id")
            )
            .values("users_count", "name")
            .order_by("-users_count")[:5]
        )
        return list(queryset)

    @classmethod
    def get_acela_dashboard_metrics(cls):
        dashboard = {
            "providers_count": cls.get_providers_count(),
            "top_five_providers_by_customer_count": cls.get_top_five_providers_by_customer_count(),
            "top_five_providers_by_users_activity": cls.get_top_five_providers_by_users_activity(),
        }
        return dashboard


class IntegrationService(object):
    @classmethod
    def get_connectwise_integration_type_object(cls):
        return IntegrationType.objects.get(name="CONNECTWISE")

    @classmethod
    def get_cisco_integration_type_object(cls):
        return IntegrationType.objects.get(name="CISCO")

    @classmethod
    def get_logic_monitor_integration_type_object(cls):
        return IntegrationType.objects.get(name="LOGIC_MONITOR")

    @classmethod
    def get_dropbox_storage_integration_type_object(cls):
        return IntegrationType.objects.get(name="DROPBOX")

    @classmethod
    def get_palo_alto_integration_type_object(cls):
        return IntegrationType.objects.get(name="PALO_ALTO")


class LogicMonitorIntegrationService(object):
    @classmethod
    def _get_client(cls, provider):
        logic_monitor_integration_type = (
            IntegrationService.get_logic_monitor_integration_type_object()
        )
        try:
            provider_integration = ProviderIntegration.objects.get(
                provider=provider,
                integration_type=logic_monitor_integration_type,
            )
            client = LogicMonitor(
                authentication_config=provider_integration.authentication_info,
                extra_config=provider_integration.other_config,
            )
            data = client.check_connection()
            if data:
                return client
            else:
                return None
        except:
            return None

    @classmethod
    def get_monitoring_api_client(cls, provider):
        return LogicMonitorIntegrationService._get_client(provider)

    @classmethod
    def get_group_list(cls, data):
        client = LogicMonitor(authentication_config=data)
        if client.check_connection():
            return client.get_device_groups()

    @classmethod
    def get_system_categories_for_group(cls, data, group_id):
        client = LogicMonitor(authentication_config=data)
        if client.check_connection():
            return client.get_system_categories_for_group(group_id)

    @classmethod
    def get_key_mappings(cls):
        return LogicMonitor.default_key_mappings()

    @classmethod
    def get_customer_group_mapping(cls, provider):
        client = LogicMonitorIntegrationService.get_monitoring_api_client(
            provider
        )
        if client is not None:
            return client.get_customer_group_mappings()

    @classmethod
    def get_alerts_by_group(cls, provider, group_id):
        client = LogicMonitorIntegrationService.get_monitoring_api_client(
            provider
        )
        if client is not None:
            result = []
            alerts = client.get_alerts_by_group_id(group_id)
            for alert in alerts:
                result.append(
                    dict(
                        device=alert.monitor_object_name,
                        datasource=alert.resource_template_name,
                        instance=alert.instance_name,
                        datapoint=alert.data_point_name,
                        value=alert.alert_value,
                        began=datetime.datetime.strftime(
                            datetime.datetime.fromtimestamp(alert.start_epoch),
                            "%Y-%m-%dT%H:%M:%SZ",
                        ),
                        threshold=alert.threshold,
                    )
                )
            return result


class ERPClientIntegrationService(object):
    @classmethod
    def get_company_notes(cls, erp_client, customer_id):
        data = erp_client.get_company_notes(customer_id)
        return data

    @classmethod
    def create_company_note(cls, erp_client, customer_id, data):
        data = erp_client.create_company_note(customer_id, data)
        return data

    @classmethod
    def get_company_note(cls, erp_client, customer_id, note_id):
        data = erp_client.get_company_note(customer_id, note_id)
        return data

    @classmethod
    def update_company_note(cls, erp_client, customer_id, note_id, data):
        data = erp_client.update_company_note(customer_id, note_id, data)
        return data

    @classmethod
    def get_company_note_types(cls, erp_client):
        data = erp_client.get_company_note_types()
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SITE_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def get_customer_sites(cls, provider_id, customer_id, erp_client):
        data = erp_client.get_customer_sites(customer_id=customer_id)
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SITE_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def create_customer_site(cls, provider_id, customer_id, erp_client, data):
        data = erp_client.create_customer_site(customer_id, data)
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SITE_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def bulk_create_customer_site(
        cls, provider_id, customer_id, erp_client, payload
    ):
        sites_data = []
        for data in payload:
            data = erp_client.create_customer_site(customer_id, data)
            sites_data.append(data)
        return sites_data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SITE_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def update_customer_site(
        cls, provider_id, customer_id, erp_client, site_id, data
    ):
        data = erp_client.update_customer_site(customer_id, site_id, data)
        return data

    @classmethod
    def get_customer_complete_site_addresses(
        cls, provider_id, customer_id, erp_client, site_ids
    ):
        data = erp_client.get_customer_complete_site_addresses(
            customer_id, site_ids=site_ids
        )
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.MANUFACTURER_LIST,
        cache_type=CacheService.CACHE_TYPE.PROVIDER,
    )
    def get_manufacturers(cls, provider_id, erp_client):
        data = erp_client.get_manufacturers()
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.COUNTRIES_LIST,
        cache_type=CacheService.CACHE_TYPE.PROVIDER,
    )
    def get_countries_list(cls, provider_id, erp_client):
        data = erp_client.get_countries_list()
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.STATE_LIST,
        cache_type=CacheService.CACHE_TYPE.PROVIDER,
    )
    def get_states_list(cls, provider_id, country_id, erp_client):
        data = erp_client.get_states_list(country_id)
        return data

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.COUNTRIES_AND_STATE_LIST,
        cache_type=CacheService.CACHE_TYPE.PROVIDER,
    )
    def get_countries_and_state_list(cls, provider_id, erp_client):
        data = erp_client.get_countries_and_state_list()
        return data

    @classmethod
    def get_board_statuses(cls, erp_client, board_id):
        data = erp_client.get_board_statuses(board_id)
        return data

    @classmethod
    def get_device_categories(cls, provider_id, erp_client):
        return erp_client.get_device_categories()

    @classmethod
    def get_members(cls, provider_id, erp_client):
        return erp_client.get_members()

    @classmethod
    def get_territories(cls, provider_id, erp_client):
        return erp_client.get_territories()

    @classmethod
    def get_territory(cls, provider_id, erp_client, territory_id):
        return erp_client.get_territory(territory_id)

    @classmethod
    def update_territory(cls, provider_id, erp_client, territory_id, data):
        try:
            return erp_client.update_territory(territory_id, data)
        except APIError as err:
            if err.status_code == 500:
                return
            else:
                raise err

    @classmethod
    def get_business_units(cls, erp_client):
        return erp_client.get_system_departments()

    @classmethod
    def upload_attachments(cls, erp_client, payload):
        result = {}
        for file in payload.get("files"):
            try:
                document_payload = {
                    "file": file,
                    "record_type": payload.get("record_type"),
                    "record_id": payload.get("record_id"),
                }
                response = erp_client.upload_system_attachments(
                    document_payload
                )
                result[file.name] = response
            except APIError as e:
                logger.error(
                    f"Error while uploading file for record {payload.get('record_id')} "
                    f"and type {payload.get('record_type')}, {e}"
                )
                result[file.name] = "UPLOAD_FAILED"
        return result


class DropboxIntegrationService(object):
    @classmethod
    def _get_client(cls, provider):
        dropbox_integration_type = (
            IntegrationService.get_dropbox_storage_integration_type_object()
        )
        try:
            provider_integration = ProviderIntegration.objects.get(
                provider=provider, integration_type=dropbox_integration_type
            )
            drpbox = DropBox(provider_integration.authentication_info)
            if drpbox.check_connection():
                return drpbox.client
            else:
                return None
        except:
            return None

    @classmethod
    def get_storage_api_client(cls, provider):
        return DropboxIntegrationService._get_client(provider)

    @classmethod
    def get_root_folder_list(cls, access_token):
        client = DropBox(dict(access_token=access_token)).client
        if client is not None:
            return client.get_files_folder_list(path="", is_root=True)

    @classmethod
    def get_files_folder_list(cls, customer_id, provider, path, limit=None):
        client = cls.get_storage_api_client(provider)
        if client is not None:
            try:
                customer = Customer.objects.get(id=customer_id)
                _name = cls._get_customer_folder_name(customer)
                path = "/" + os.path.join(
                    *[client.APP_FOLDER_NAME, _name, path]
                )
                return client.get_files_folder_list(path, limit)
            except Customer.DoesNotExist:
                raise exceptions.NotFoundError(message="Invalid Customer ID.")

    @classmethod
    def get_file(cls, customer_id, provider, path):
        client = cls.get_storage_api_client(provider)
        if client is not None:
            try:
                customer = Customer.objects.get(id=customer_id)
                _name = cls._get_customer_folder_name(customer)
                path = "/" + os.path.join(
                    *[client.APP_FOLDER_NAME, _name, path]
                )
                return client.get_file(path)
            except Customer.DoesNotExist:
                raise exceptions.NotFoundError(message="Invalid Customer ID.")

    @classmethod
    def create_folders(cls, provider, customer_crm_id_name_tuples):
        client = cls.get_storage_api_client(provider)
        if client is not None:
            _folder_paths = []
            for _crm_id, _name in customer_crm_id_name_tuples:
                _name = get_valid_folder_name(_name)
                _path = "{}-{}".format(_name, _crm_id)
                _folder_paths.extend(
                    [
                        "/"
                        + os.path.join(
                            *[client.APP_FOLDER_NAME, _path, "Projects"]
                        ),
                        "/"
                        + os.path.join(
                            *[client.APP_FOLDER_NAME, _path, "Diagrams"]
                        ),
                        "/"
                        + os.path.join(
                            *[client.APP_FOLDER_NAME, _path, "General"]
                        ),
                    ]
                )
            return client.create_folders_batch(_folder_paths)

    @classmethod
    def rename_customer_folder_name(cls, customer_id, old_customer_name):
        customer = Customer.objects.get(id=customer_id)
        client = cls.get_storage_api_client(customer.provider)
        if client is not None:
            old_customer_name = get_valid_folder_name(old_customer_name)
            from_path = "{}-{}".format(old_customer_name, customer.crm_id)
            from_path = "/" + os.path.join(
                *[client.APP_FOLDER_NAME, from_path]
            )
            to_path = cls._get_customer_folder_name(customer)
            to_path = "/" + os.path.join(*[client.APP_FOLDER_NAME, to_path])
            client.move_files_or_folders(from_path=from_path, to_path=to_path)

    @classmethod
    def _get_customer_folder_name(cls, customer):
        _name = get_valid_folder_name(customer.name)
        return "{}-{}".format(_name, customer.crm_id)


class SubscriptionService:
    @classmethod
    def get_subscription_product_mapping(cls, provider, subscription_names):
        subscription_mappings = (
            SubscriptionProductMapping.objects.filter(provider=provider)
            .annotate(
                product_name=F("type__name"),
                type_name=F("name"),
                product_id=F("type__id"),
            )
            .values("product_name", "type_name", "product_id")
        )
        matched_mappings = []
        for mapping in subscription_mappings:
            type_name = mapping.get("type_name")
            if type_name.endswith("*"):
                for name in subscription_names:
                    if name.startswith(type_name[:-1]):
                        matched_mappings.append(
                            {
                                "product_name": mapping.get("product_name"),
                                "type_name": name,
                                "product_id": mapping.get("product_id"),
                            }
                        )
            elif type_name in subscription_names:
                matched_mappings.append(mapping)

        subscription_mappings = group_list_of_dictionary_by_key(
            matched_mappings, "type_name"
        )
        return subscription_mappings

    @classmethod
    def get_subscription_list(
        cls,
        provider_id,
        customer_id,
        erp_client,
        category_id,
        show_active_only=True,
    ):
        # cache_key = CacheService.CACHE_KEYS.SUBSCRIPTION_LISTING.format(
        #     provider_id, customer_id
        # )
        # if CacheService.exists(cache_key) and isinstance(customer_id, int):
        #     data = CacheService.get_data(cache_key, erp_client, show_active_only)
        #     if data is not None:
        #         return data
        subscriptions = SubscriptionService.get_subscriptions(
            erp_client, customer_id, category_id, show_active_only
        )
        provider = Provider.objects.get(id=provider_id)
        subscription_names = [
            subscription.get("device_name") for subscription in subscriptions
        ]
        subscription_names = set(subscription_names)
        subscription_mappings = cls.get_subscription_product_mapping(
            provider, subscription_names
        )
        for subscription in subscriptions:
            if subscription.get("device_name") in subscription_mappings:
                subscription["product_name"] = subscription_mappings.get(
                    subscription.get("device_name")
                )[0]["product_name"]
                subscription["product_id"] = subscription_mappings.get(
                    subscription.get("device_name")
                )[0]["product_id"]
                subscription["type_name"] = subscription.get("device_name")
        subscriptions = SubscriptionService.merge_subscriptions_data(
            subscriptions, provider_id
        )
        # if isinstance(customer_id, int):
        #     CacheService.set_data(
        #         cache_key, subscriptions, erp_client, show_active_only
        #     )

        # Adding manufacturer name on the basis of settings
        subscriptions_provider_mappings: Dict[str, str] = dict(
            SubscriptionServiceProviderMapping.objects.filter(
                provider=provider_id
            ).values_list("service_provider_name", "provider_prefix")
        )
        subscriptions_provider_mappings = {
            provider_prefix: _prefix
            for _prefix, service_provider in subscriptions_provider_mappings.items()
            for provider_prefix in service_provider
        }
        for (
            _provider_prefix,
            _provider_name,
        ) in subscriptions_provider_mappings.items():
            if _provider_prefix.endswith("*"):
                for subscription in subscriptions:
                    if subscription.get("serial_number"):
                        if subscription.get("serial_number").startswith(
                            _provider_prefix[:-1]
                        ):
                            subscription["manufacturer_name"] = _provider_name
        return subscriptions

    @classmethod
    def get_subscriptions(
        cls, erp_client, customer_id, category_id, show_active_only
    ):
        if show_active_only:
            subscriptions = erp_client.get_active_devices(
                customer_id=customer_id, category_id=category_id
            )
        else:
            subscriptions = erp_client.get_devices(
                customer_id=customer_id, category_id=category_id
            )
        return subscriptions

    @classmethod
    def batch_create_or_update_subscriptions_from_file(
        cls, provider_id, category_id, data, file_name
    ):
        task = batch_create_or_update_subscriptions.apply_async(
            (provider_id, category_id, data, file_name), queue="high"
        )
        return {"task_id": task.task_id}

    @classmethod
    def merge_subscriptions_data(cls, subscriptions, provider_id):
        """
        This method the database table for the Subscriptions data and merge the data
        in the subscriptions
        :param subscriptions: List of Subscriptions
        :param provider_id: Provider ID
        :return: List of subscriptions
        """
        subscription_ids = [
            subscription.get("id") for subscription in subscriptions
        ]
        subscription_data = list(
            SubscriptionData.objects.filter(
                provider_id=provider_id,
                subscription_crm_id__in=set(subscription_ids),
            ).values("subscription_crm_id", "data")
        )
        subscription_data = group_list_of_dictionary_by_key(
            subscription_data, "subscription_crm_id"
        )
        for subscription in subscriptions:
            subscription_id = subscription.get("id")
            data = subscription_data.get(subscription_id)
            if data:
                subscription.update(data[0].get("data"))

            contract_status = (
                SubscriptionData.get_subscription_contract_status(
                    subscription.get("expiration_date")
                )
            )
            subscription.update({"contract_status": contract_status})
        return subscriptions

    @classmethod
    def update_subscription_model_data(
        cls, provider_id, customer_id, subscription_crm_id, subscription
    ):
        """
        Update data for SubscriptionData model
        :param subscription_crm_id: Subscription crm id
        :param subscription: data
        :return: None
        """
        data = dict(
            auto_renewal_term=subscription.get("auto_renewal_term"),
            licence_qty=subscription.get("licence_qty"),
            term_length=subscription.get("term_length"),
            billing_model=subscription.get("billing_model"),
            true_forward_date=subscription.get("true_forward_date"),
        )
        # remove none keys
        data = {k: v for k, v in data.items() if v is not None}
        if not data:
            return
        params = {
            "subscription_id": subscription.get("serial_number"),
            "subscription_category_id": subscription.get("category_id"),
        }
        if data:
            params.update({"data": data})

        SubscriptionData.objects.update_or_create(
            provider_id=provider_id,
            customer_id=customer_id,
            subscription_crm_id=subscription_crm_id,
            defaults=params,
        )

    @classmethod
    def update_subscription(
        cls, provider_id, customer, subscription_id, subscription_data
    ):
        """
        Update Subscription to CW and Acela DB
        :param provider_id: Provider Id
        :param customer: Customer Object
        :param subscription_id: Subscription CRM ID
        :param subscription_data: Data to be updated
        """
        provider = Provider.objects.get(id=provider_id)
        if customer:
            cw_device = DeviceService.get_device(
                provider.erp_client, subscription_id, provider_id
            )
            if not cw_device:
                raise ValidationError({"error": "Invalid Subscription Id."})
            if customer.crm_id != cw_device.get("customer_id"):
                raise ValidationError(
                    {
                        "error": "You do not have permission to perform this action."
                    }
                )
        with transaction.atomic():
            provider.erp_client.update_device(
                subscription_id, subscription_data
            )
            cls.update_subscription_model_data(
                provider_id, customer.id, subscription_id, subscription_data
            )

    @classmethod
    def create_subscription_product_mapping(cls, provider):
        """
        Add default Subscription Product types and Subscription product mapping for the provider
        Args:
            provider: Provider object

        Returns: None

        """
        product_types = [
            "Webex",
            "Cloud Infrastructure",
            "CloudLock",
            "Data Center",
            "Umbrella",
            "SD-WAN",
        ]
        products_mapping = {
            "A-FLEX": "Webex",
            "A-FLEX-CC": "Webex",
            "A-SPK-CUWP-CLOUD": "Webex",
            "A-SPK-EDU": "Webex",
            "A-SPK-EMP-COUNT": "Webex",
            "A-SPK-NAMED-USER": "Webex",
            "A-SPK-SH": "Webex",
            "A-WX-EMP-COUNT": "Webex",
            "A-WX-NAMED-USER": "Webex",
            "C1-ISR-ADD": "Cloud Infrastructure",
            "C1-N9K-ADD-T": "Cloud Infrastructure",
            "CLOUDLOCK-SUB": "CloudLock",
            "DC-MGT-SAAS": "Data Center",
            "UMBRELLA-SUB": "Umbrella",
            "VEDGE-1000-AC-K9": "SD-WAN",
            "VEDGE-100B-AC-K9": "SD-WAN",
        }

        s_product_types_create = [
            SubscriptionProductType(name=_name, provider=provider)
            for _name in product_types
        ]
        if s_product_types_create:
            SubscriptionProductType.objects.bulk_create(s_product_types_create)
        s_products_mapping = []
        for _name, _type in products_mapping.items():
            _type = SubscriptionProductType.objects.get(
                name=_type, provider=provider
            )
            s_products_mapping.append(
                SubscriptionProductMapping(
                    name=_name, type=_type, provider=provider
                )
            )
        SubscriptionProductMapping.objects.bulk_create(s_products_mapping)


def trigger_connectwise_sync_tasks():
    """
    Trigger all tasks related to connectwise data sync
    Returns: None

    """
    from core.tasks.connectwise.connectwise import (
        cw_data_synchronization_task,
        sync_cw_device_data,
    )

    cw_data_synchronization_task()
    sync_cw_device_data()


def trigger_external_service_sync_tasks():
    """
    Trigger external service like cisco, Logicmonitor data sync tasks
    Returns: None

    """
    from core.tasks.cisco.tasks import (
        cisco_missing_data_task,
        device_manufacturer_data_sync_task,
        sync_cisco_device_search_lines,
        sync_cisco_device_type_task,
        sync_cisco_subscription_data,
    )
    from core.tasks.logic_monitor.logic_monitor import (
        logic_monitor_sync_device_name,
        sync_logic_monitor_config_data,
    )

    cisco_missing_data_task.delay()
    device_manufacturer_data_sync_task.delay()
    sync_cisco_device_type_task.delay()
    sync_cisco_device_search_lines.delay()
    logic_monitor_sync_device_name.delay()
    sync_logic_monitor_config_data.delay()
    sync_cisco_subscription_data.delay()


class SubscriptionNotificationService:
    """
    Handles automated subscriptions notifications.
    """

    def __init__(self, provider: Provider):
        self.provider = provider
        self.subscription_notification_setting = (
            provider.subscription_notification_setting
        )

    async def _get_expiring_subscriptions_for_customer(
        self, session: ClientSession, customer_crm_id: int
    ):
        """
        Get expiring customer subscriptions using the filters configured in AutomatedSubscriptionNotificationSetting

        Args:
            session: ClientSession
            customer_crm_id: Customer CRM ID

        Returns:
            Expiring customer subscriptions
        """
        fields: str = (
            f"id,name,serialNumber,modelNumber,type/name,type/id,status/id,status/name,"
            f"manufacturer/id,manufacturer/name,company/id,company/name,warrantyExpirationDate,needsRenewalFlag"
        )
        conditions: str = (
            f"serialNumber != null and company/id in ({customer_crm_id},) "
        )
        if self.subscription_notification_setting.config_type_crm_ids:
            config_type_crm_ids: str = ",".join(
                str(num)
                for num in self.subscription_notification_setting.config_type_crm_ids
            )
            conditions += f"and type/id in ({config_type_crm_ids},) "
        if self.subscription_notification_setting.config_status_crm_ids:
            status_type_crm_ids: str = ",".join(
                str(num)
                for num in self.subscription_notification_setting.config_status_crm_ids
            )
            conditions += f"and status/id in ({status_type_crm_ids},) "
        if self.subscription_notification_setting.manufacturer_crm_ids:
            manufacturer_crm_ids: str = ",".join(
                str(num)
                for num in self.subscription_notification_setting.manufacturer_crm_ids
            )
            conditions += f"and manufacturer/id in ({manufacturer_crm_ids},) "

        query_params: Dict = dict(
            fields=fields, conditions=conditions, pagesize=1000
        )
        # Retry the request 3 times in case of timeout. Wait for 1, 2, 3 seconds respectively.
        for i in range(3):
            try:
                async with session.get(
                    CONFIG_ENDPOINT, params=query_params
                ) as response:
                    if response.status == 200:
                        subscriptions: List[Dict] = await response.json()
                        return subscriptions
                    else:
                        task_logger.error(
                            f"{response.status} error while fetching customer subscriptions. Error: {response.content}"
                        )
                        return []
            except asyncio.TimeoutError:
                await asyncio.sleep(i + 1)
        task_logger.info(f"Time out fetching customer subscription!")
        return []

    @staticmethod
    def format_term_end_date(term_end_date: str) -> str:
        """
        Format term end date.

        Args:
            term_end_date: Term end date

        Returns:
            Term end date
        """
        date_formats: Dict = device_date_formats()
        required_format: str = date_formats.get("MM/DD/YYYY")
        term_end_date: datetime = convert_date_string_to_date_object(
            term_end_date, settings.ISO_FORMAT
        )
        term_end_date: str = term_end_date.strftime(required_format)
        return term_end_date

    @staticmethod
    def get_days_until_term_end_date_value(
        term_end_date: str,
    ) -> Union[str, int]:
        """
        Get days until term end date value.

        Args:
            term_end_date: Term end date

        Returns:
            Days until term end date value.
        """
        current_date: date = timezone.now().date()
        term_end_date: date = convert_date_string_to_date_object(
            term_end_date, settings.ISO_FORMAT
        )
        if term_end_date <= current_date:
            return "OVERDUE"
        else:
            delta = term_end_date - current_date
            remaining_days: int = delta.days
            return remaining_days

    def process_subscriptions(self, subscriptions: List[Dict]) -> List[Dict]:
        """
        Process subscriptions. Transforms subscription data into required format.

        Args:
            subscriptions: Subscriptions

        Returns:
            Subscriptions
        """
        model_numbers: Set[str] = {
            subscription.get("modelNumber")
            for subscription in subscriptions
            if subscription.get("modelNumber") not in {"", None}
        }
        products: List[Dict] = list()
        for model_numbers in chunks_of_list_with_max_item_count(
            list(model_numbers), 25
        ):
            products_details: List[
                Dict
            ] = self.provider.erp_client.get_product_catalogs_using_model_number(
                model_numbers
            )
            products.extend(products_details)
        products_by_product_id: Dict[str, Dict] = {
            product.get("part_number"): product for product in products
        }
        processed_subscriptions: List[Dict] = list()
        for subscription in subscriptions:
            expiration_date: str = subscription.get(
                "warrantyExpirationDate", ""
            )
            term_end_date: str = (
                self.format_term_end_date(expiration_date)
                if expiration_date
                else ""
            )
            days_until_term_end_date: str = (
                self.get_days_until_term_end_date_value(expiration_date)
                if expiration_date
                else ""
            )
            model_number: str = subscription.get("modelNumber", "")
            product_description: str = (
                products_by_product_id.get(model_number, {}).get(
                    "description", "-"
                )
                if model_number
                else "-"
            )
            processed_subscriptions.append(
                dict(
                    subscription_id=subscription.get("serialNumber"),
                    subscription_type=subscription.get("name"),
                    product_description=product_description,
                    term_end_date=term_end_date,
                    auto_renew="Yes"
                    if subscription.get("needsRenewalFlag", False)
                    else "No",
                    days_until_term_end_date=days_until_term_end_date,
                )
            )
        return processed_subscriptions

    def filter_subscriptions_by_approaching_term_end_dates(
        self, subscriptions: List[Dict]
    ) -> List[Dict]:
        """
        Filter out subscriptions by configured apporaching term end dates.

        Args:
            subscriptions: Subscriptions

        Returns:
            Subscriptions
        """
        required_subscriptions: List[Dict] = list()
        expiry_date_ranges: List[Tuple[date, date]] = [
            AutomatedSubscriptionNotificationSetting.get_approaching_expiry_dates(
                date_range
            )
            for date_range in self.subscription_notification_setting.approaching_expiry_date_range
        ]
        for subscription in subscriptions:
            term_end_date: str = subscription.get("warrantyExpirationDate", "")
            if not term_end_date:
                continue
            term_end_date: date = convert_date_string_to_date_object(
                term_end_date, settings.ISO_FORMAT
            )
            for date_range in expiry_date_ranges:
                if date_range[0] <= term_end_date <= date_range[1]:
                    required_subscriptions.append(subscription)
                    break
        return required_subscriptions

    async def get_expiring_subscriptions_data_for_customer(
        self, session: ClientSession, customer_crm_id: int
    ) -> List[Dict]:
        """
        Get expiring subscriptions data for customer.

        Args:
            session: ClientSession
            customer_crm_id: Customer CRM ID

        Returns:
            Expiring subscriptions
        """
        expiring_subscriptions: List[
            Dict
        ] = await self._get_expiring_subscriptions_for_customer(
            session, customer_crm_id
        )
        if not expiring_subscriptions:
            return []
        expiring_subscriptions: List[
            Dict
        ] = self.filter_subscriptions_by_approaching_term_end_dates(
            expiring_subscriptions
        )
        expiring_subscriptions: List[Dict] = self.process_subscriptions(
            expiring_subscriptions
        )
        return expiring_subscriptions

    def get_customer_renewal_contact_details(
        self, customer_crm_id: int
    ) -> Dict:
        """
        Get customer renewal type contact details.

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Customer renewal type contact details.
        """
        renewal_contact_details: Dict = {}
        contacts: List[Dict] = self.provider.erp_client.get_customer_users(
            customer_id=customer_crm_id,
            active_users_only=True,
            type_filter=self.subscription_notification_setting.renewal_type_contact_crm_id,
        )
        if contacts:
            renewal_contact_details: Dict = dict(
                first_name=contacts[0].get("first_name"),
                last_name=contacts[0].get("last_name"),
                title=contacts[0].get("title"),
                email=contacts[0].get("email"),
            )
        return renewal_contact_details

    @staticmethod
    def render_subscription_notice_email_body(
        render_context: Dict[str, Any]
    ) -> str:
        """
        Render subscription notice email body.

        Args:
            render_context: Render context

        Returns:
            Rendered subscription notice email body
        """
        template_directory_path: str = (
            AutomatedSubscriptionNotificationSetting.get_email_template_directory_path()
        )
        template_path: str = os.path.join(
            template_directory_path, "subscription_notice_body.html"
        )
        rendered_email_body: str = render_to_string(
            template_path, render_context
        )
        return rendered_email_body

    async def send_expiring_subscription_notification_to_customer(
        self, session: ClientSession, customer: Customer
    ) -> Dict:
        """
        Send expiring subscription notification to customer.

        Args:
            session: ClientSession
            customer: Customer

        Returns:
            None
        """
        setting: AutomatedSubscriptionNotificationSetting = (
            self.subscription_notification_setting
        )
        renewal_contact: Dict = self.get_customer_renewal_contact_details(
            customer.crm_id
        )
        sender_details: Dict = setting.get_sender_details()
        if not sender_details:
            logger.info(
                f"Cannot send subscription notification email to customer: {customer.name}, CRM ID: {customer.crm_id}. "
                f"Error: Sender details not found."
            )
            return dict(
                email_sent=False,
                reason="Sender details not found.",
                customer_name=customer.name,
                customer_crm_id=customer.crm_id,
            )
        expiring_subscriptions: List[
            Dict
        ] = await self.get_expiring_subscriptions_data_for_customer(
            session, customer.crm_id
        )
        if not expiring_subscriptions:
            logger.info(
                f"Cannot send subscription notification email to customer: {customer.name}, CRM ID: {customer.crm_id}. "
                f"Error: Expiring subscriptions not found."
            )
            return dict(
                email_sent=False,
                reason="Expiring subscriptions not found.",
                customer_name=customer.name,
                customer_crm_id=customer.crm_id,
            )
        render_context: Dict = dict(
            subject=setting.email_subject,
            email_body=setting.email_body,
            renewal_contact_details=renewal_contact,
            sender_details=sender_details,
            expiring_subscriptions=expiring_subscriptions,
            customer_name=customer.name,
            logo=settings.LOOKINGPOINT_LOGO,
        )
        email_body: str = self.render_subscription_notice_email_body(
            render_context
        )
        subject: str = f"{setting.email_subject} - {customer.name}"
        AutomatedSubscriptionNotificationEmail(
            context=dict(
                provider=self.provider,
                email_body=email_body,
                subject=subject,
            ),
            subject=subject,
            from_email=sender_details.get("email"),
        ).send(
            to=setting.receiver_email_ids,
            log_error=False,
        )
        task_logger.info(
            f"Subscription notification email sent for customer: {customer.name}."
        )
        return dict(
            email_sent=True,
            reason=None,
            customer_name=customer.name,
            customer_crm_id=customer.crm_id,
        )

    async def async_send_expiring_subscription_notification_to_customers(
        self, customers: "QuerySet[Customer]"
    ) -> List[Dict]:
        """
        Trigger async tasks to send expiring subscription notifications to customers.

        Args:
            customers: Customers

        Returns:
            None
        """
        cw_headers: Dict[
            str, Any
        ] = self.provider.erp_client.client.session.headers
        cw_headers.update(
            {
                "clientId": self.provider.erp_client.client.client_id,
                "Content-Type": "application/json",
            }
        )
        tasks: List = list()
        async with ClientSession(headers=cw_headers) as session:
            for customer in customers:
                tasks.append(
                    asyncio.ensure_future(
                        self.send_expiring_subscription_notification_to_customer(
                            session, customer
                        )
                    )
                )
            mail_details: List[Dict] = await asyncio.gather(*tasks)
            return mail_details

    def send_expiring_subscription_notification_to_customers(
        self, customers: "QuerySet[Customer]"
    ) -> List[Dict]:
        """
        Send expiring subscriptin notification to customers.

        Args:
            customers: Customers

        Returns:
            None
        """
        mail_details: List[Dict] = asyncio.run(
            self.async_send_expiring_subscription_notification_to_customers(
                customers
            )
        )
        return mail_details

    def generate_subscription_notification_email_preview(self) -> str:
        """
        Generate subscription notification email preview.

        Returns:
            Subscription notification email preview
        """
        setting: AutomatedSubscriptionNotificationSetting = (
            self.subscription_notification_setting
        )
        render_context: Dict = dict(
            subject=setting.email_subject,
            email_body=setting.email_body,
            renewal_contact_details=dict(
                first_name="First name",
                last_name="Last name",
                title="Title",
                email="renewal-contact@email.com",
            ),
            sender_details=dict(
                name=setting.sender.name,
                email=setting.sender.email,
                signature=setting.sender.profile.email_signature.get(
                    "email_signature", None
                ),
            ),
            expiring_subscriptions=[
                dict(
                    subscription_id="Sub123",
                    subscription_type="Config 123",
                    product_description="Config 101 description",
                    term_end_date="4/10/2023",
                    auto_renew="No",
                    days_until_term_end_date="OVERDUE",
                ),
                dict(
                    subscription_id="Sub345",
                    subscription_type="Config 345",
                    product_description="Config 345 description",
                    term_end_date="20/10/2023",
                    auto_renew="Yes",
                    days_until_term_end_date=10,
                ),
            ],
            customer_name="CUSTOMER_NAME",
            logo=settings.LOOKINGPOINT_LOGO,
        )
        preview: str = self.render_subscription_notice_email_body(
            render_context
        )
        return preview


class ReportsBundleService:
    report_types_bundle = [
        {
            "report_type": {
                "name": "End Of Life all devices",
                "category": "LIFECYCLE_MANAGEMENT_REPORT",
                "display_name": "Lifecycle management report",
                "type": "REPORT01",
            }
        },
        {
            "report_type": {
                "name": "Coming EOL within 12 months",
                "category": "LIFECYCLE_MANAGEMENT_REPORT",
                "display_name": "Lifecycle management report",
                "type": "REPORT02",
            }
        },
        {
            "report_type": {
                "name": "Coming EOL after 12 months",
                "category": "LIFECYCLE_MANAGEMENT_REPORT",
                "display_name": "Lifecycle management report",
                "type": "REPORT03",
            }
        },
        {
            "report_type": {
                "name": "Expired Devices",
                "category": "DEVICE_COVERAGE_REPORT",
                "display_name": "Device coverage report",
                "type": "REPORT04",
            }
        },
        {
            "report_type": {
                "name": "Expiring in 90 days",
                "category": "DEVICE_COVERAGE_REPORT",
                "display_name": "Device coverage report",
                "type": "REPORT05",
            }
        },
        {
            "report_type": {
                "name": "Expiring in 12 months",
                "category": "DEVICE_COVERAGE_REPORT",
                "display_name": "Device coverage report",
                "type": "REPORT06",
            }
        },
        {
            "report_type": {
                "name": "All Devices",
                "category": "SOFTWARE_MANAGEMENT_REPORT",
                "display_name": "All Devices Report",
                "type": "REPORT09",
            }
        },
    ]
