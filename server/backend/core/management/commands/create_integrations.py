"""
Django Command to create Integration Categories and Types.
"""

from django.core.management import BaseCommand

from core.migration_scripts import create_integration_categories


class Command(BaseCommand):
    """
    Django Command to create Integration Categories and Types.
    """

    help = "Create Integration categories and Types"

    def handle(self, *args, **options):
        create_integration_categories()
        self.stdout.write(
            self.style.SUCCESS(
                "Integration Categories and Types created successfully."
            )
        )
