"""
Django Command to create Integration Categories and Types.
"""

from django.core.management import BaseCommand

from core.migration_scripts import add_crm_id_to_existing_customer_users


class Command(BaseCommand):
    def handle(self, *args, **options):
        add_crm_id_to_existing_customer_users()
        self.stdout.write(
            self.style.SUCCESS("User CRM Ids updated successfully.")
        )
