"""
Django Command to Update Connectwise Integration configs.
"""

from django.core.management import BaseCommand

from core.migration_scripts import update_integration_type_configs


class Command(BaseCommand):
    """
    Django Command to create Integration Categories and Types.
    """

    def handle(self, *args, **options):
        from core.models import IntegrationType

        connectwise = IntegrationType.objects.get(name="CONNECTWISE")
        update_integration_type_configs(connectwise)
        self.stdout.write(
            self.style.SUCCESS(
                "Connectwise Configurations Updated Successfully."
            )
        )
