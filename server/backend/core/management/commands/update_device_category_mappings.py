"""
Django Command to Update Device Category Mapping.
"""

from django.core.management import BaseCommand

from core.migration_scripts import update_device_category_mappings


class Command(BaseCommand):
    """
    Django Command to create Integration Categories and Types.
    """

    def handle(self, *args, **options):
        update_device_category_mappings()
        self.stdout.write(
            self.style.SUCCESS(
                "Device Category Mappings Updated Successfully."
            )
        )
