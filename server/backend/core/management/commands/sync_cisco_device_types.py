"""
Django Command to sync cisco device types data.
"""
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from core.tasks.cisco.tasks import sync_cisco_device_type_task

        sync_cisco_device_type_task.delay()
        self.stdout.write(
            self.style.SUCCESS("Cisco update device type sync task started.")
        )
