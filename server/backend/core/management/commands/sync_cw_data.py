"""
Django Command to sync connectwise data.
"""

from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from core.tasks.connectwise.connectwise import (
            cw_data_synchronization_task,
        )

        cw_data_synchronization_task.delay(True)
        self.stdout.write(self.style.SUCCESS("CW data sync task started."))
