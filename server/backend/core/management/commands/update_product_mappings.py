"""
Django Command to Update Product Mapping.
"""

from django.core.management import BaseCommand

from core.migration_scripts import create_subscription_product_mapping


class Command(BaseCommand):
    """
    Django Command to create Integration Categories and Types.
    """

    def handle(self, *args, **options):
        create_subscription_product_mapping()
        self.stdout.write(
            self.style.SUCCESS("Product Mappings Updated Successfully.")
        )
