from django.core.management import BaseCommand

from core.migration_scripts import load_device_types_mapping


class Command(BaseCommand):
    """
    Django Command to create Integration Categories and Types.
    """

    help = "Create Device Type and Model Mapping."

    def handle(self, *args, **options):
        res = load_device_types_mapping()
        if res:
            self.stdout.write(
                self.style.SUCCESS("Device Type Mapping created Successfully")
            )
        else:
            self.stdout.write(
                self.style.SUCCESS("Device Type Mapping creation Failed")
            )
