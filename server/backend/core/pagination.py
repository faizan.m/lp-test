from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    page_size_query_param = "page_size"

    def get_paginated_response(self, data):
        return Response(
            {
                "links": {
                    # 'next': self.get_next_link(),
                    # 'previous': self.get_previous_link(),
                    "next_page_number": self.page.next_page_number()
                    if self.page.has_next()
                    else None,
                    "previous_page_number": self.page.previous_page_number()
                    if self.page.has_previous()
                    else None,
                    "page_number": self.page.number,
                },
                "count": self.page.paginator.count,
                "page_size": self.page_size,
                "results": data,
            }
        )

    def paginate_queryset(self, queryset, request, view=None):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        full_info = request.query_params.get("full-info", "true")
        pagination = request.query_params.get("pagination", "true")
        if full_info == "false" or pagination == "false":
            return None
        return super(CustomPagination, self).paginate_queryset(
            queryset, request, view
        )
