"""
We need to pull a bunch of data out of ConnectWise for an audit. We don’t need a UI, and thinking you guys could develop
a script to pull out opportunity attachments and populate a dropbox folder. We need to pull all opportunity documents
and upload into a folder structure for just the top 20 Customers during 2021. Can you let me know if this is something
we can get completed this week? Let me know if we need a call tonight to clarify.

We have the dropbox API account which we can use, but will want to create a new folder under ”Customer Doc Access”
The new folder can be called “2021 – CUSTOMER AUDIT”

Under 2021 – CUSTOMER AUDIT
For each each Customer in the top 20 we would create a customer folder (Customer Name) – (CUSTOMER Number)

For the top 20 list where Opportunity is WON and WON date is 2021 would get a folder for each won opportunity
under the company name. (Opportunity Name) – (Opportunity Number)-(WON Date)

All Documents for the associated opportunity would be uploaded to that folder.

End Structure

20 Customer Directories
Under each customer directory a directory would exist for each won opportunity named as described above.

"""
from datetime import datetime

from dropbox.files import WriteMode

from accounts.models import Customer, Provider
from core.integrations.erp.connectwise.connectwise import (
    CWConditions,
    Condition,
    Operators,
)
from core.services import DropboxIntegrationService, logger
import asyncio
from concurrent.futures import ThreadPoolExecutor
from functools import partial

ROOT_FOLDER = "Customer Doc Access"
AUDIT_FOLDER = "GOV 5YR – CUSTOMER AUDIT"
YEARS = ["2021", "2020", "2019", "2018", "2017"]
provider_id = 1
customer_names = [
    "City of Hayward",
    "GGB - Golden Gate Bridge District",
    "Edmonds School District",
    "Union Sanitary District",
    "Patterson Unified School District",
    "City of Daly City",
    "City of Berkeley",
    "City of Pleasanton",
    "City of Eureka",
    "City of Alameda",
    "Union Sanitary District",
    "Brisbane School District",
    "Menlo Park Fire Protection District",
    "Port Of Oakland",
    "City of Los Altos",
    "Contra Costa County Fire Protection",
    "District",
]

customers_19_20s = [
    "Rivian Automotive, LLC",
    "Cadence",
    "Crocs",
    "Restoration Hardware",
    "El Camino Hospital",
    "Save Mart",
    "Palantir",
    "Vocera",
    "First Tech Federal Credit Union",
    "City of Hayward",
    "Armanino  LLP",
    "Quantum Corp",
    "Hydrofarm",
    "Infinera Corporation",
    "Educational Employees Credit Union",
]
AUDIT_FOLDER_FOR_2YR = "2YR – CUSTOMER AUDIT"


def sync_audit_log(provider_id, customer_names, year, audit_folder):
    provider = Provider.objects.get(id=provider_id)
    dropbox_client = DropboxIntegrationService.get_storage_api_client(provider)
    start_date = f"{year}-01-01T00:00:00Z"
    start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%SZ")
    start_date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")

    end_date = f"{year}-12-31T23:59:59Z"
    end_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%SZ")
    end_date = end_date.strftime("%Y-%m-%dT%H:%M:%SZ")
    print(f"Syncing for Year: {year}")
    for customer_name in customer_names:
        print(f"Customer Name : {customer_name}")
        try:
            provider = Provider.objects.get(id=provider_id)
            customer = Customer.objects.get(
                provider=provider, name=customer_name
            )
        except Customer.DoesNotExist:
            logger.debug(f"No customer exist with {customer_name=}")
            print("customer DOES NOT EXISTS!")
            continue
        AUDIT_FOLDER_WITH_YEAR = f"{audit_folder}/{year}"
        dropbox_path = dropbox_client.get_customer_audit_folder_path(
            ROOT_FOLDER, AUDIT_FOLDER_WITH_YEAR, customer
        )
        won_status = [2]
        filters = [
            CWConditions(
                datetime_conditions=[
                    Condition(
                        "closedDate", Operators.GREATER_THEN, start_date
                    ),
                    Condition(
                        "closedDate", Operators.LESS_THEN_EQUALS, end_date
                    ),
                ]
            )
        ]
        company_won_quotes = provider.erp_client.get_company_quotes(
            customer_id=customer.crm_id,
            statuses=won_status,
            stages=None,
            business_unit_id=None,
            filters=filters,
        )
        print(f"Company Quotes Count: {len(company_won_quotes)}")
        for quote in company_won_quotes:
            quote_closed_date = datetime.strptime(
                quote.get("closed_date"), "%Y-%m-%dT%H:%M:%Sz"
            )
            quote_closed_date = quote_closed_date.strftime("%Y-%m-%d")
            dropbox_quote_path = f"{dropbox_path}/{quote.get('name')} - {quote.get('id')} - {quote_closed_date}"
            try:
                quote_documents = provider.erp_client.get_quote_documents(
                    quote.get("id")
                )
            except Exception as e:
                print(f"Error {e}")
                continue
            print(
                f"Total number of attached documents {len(quote_documents)} with quote {quote.get('id')}"
            )
            for quote_document in quote_documents:
                download_document_url = (
                    provider.erp_client.get_document_download_url(
                        quote_document.get("id")
                    )
                )
                dropbox_complete_path = (
                    f"{dropbox_quote_path}/{quote_document.get('file_name')}"
                )
                cw_client = provider.erp_client.client
                attached_file = cw_client._get(download_document_url)
                dropbox_client.upload_file(
                    attached_file.content,
                    dropbox_complete_path,
                    mode=WriteMode("overwrite"),
                )
                print(f"Dopbox Final Path :: {dropbox_complete_path}")
        logger.info(
            f"Uploading device import file for customer: {customer.name} to Dropbox"
        )


async def async_audit_log_method(
    provider_id, customer_names, years, audit_folder
):
    with ThreadPoolExecutor(max_workers=10) as executor:
        loop = asyncio.get_event_loop()
        tasks = [
            loop.run_in_executor(
                executor,
                partial(
                    sync_audit_log,
                    provider_id,
                    customer_names,
                    year,
                    audit_folder,
                ),
            )
            for year in years
        ]
        response = await asyncio.gather(*tasks)
        return response


def audit_call_async_task(provider_id, customer_names, years=None):
    if not years:
        years = YEARS
        audit_folder = AUDIT_FOLDER
    else:
        audit_folder = AUDIT_FOLDER_FOR_2YR
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    future = asyncio.ensure_future(
        async_audit_log_method(
            provider_id,
            customer_names,
            years,
            audit_folder,
        )
    )
    response = loop.run_until_complete(future)
