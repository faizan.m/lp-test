import json

from rest_framework import serializers
from reversion.models import Revision, Version

from accounts.models import User


class RevisionSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    meta_data = serializers.SerializerMethodField()

    def get_meta_data(self, obj):
        try:
            return json.loads(obj.comment)
        except json.decoder.JSONDecodeError:
            return obj.comment

    class Meta:
        model = Revision
        fields = "__all__"


class VersionSerializer(serializers.ModelSerializer):
    revision = RevisionSerializer()
    diff = serializers.JSONField()

    class Meta:
        model = Version
        fields = ("id", "revision", "diff")


class VersionSerializerWithoutDiff(serializers.ModelSerializer):
    revision = RevisionSerializer()
    snapshot_data = serializers.SerializerMethodField()

    def get_snapshot_data(self, obj):
        return obj.field_dict

    class Meta:
        model = Version
        fields = "__all__"


class SOWVersionSerializer(serializers.ModelSerializer):
    revision = RevisionSerializer()
    snapshot_data = serializers.SerializerMethodField()
    diff = serializers.JSONField()

    def get_snapshot_data(self, obj):
        version_data = obj.field_dict
        version_data["user"] = version_data.get("user_id")
        version_data["category"] = version_data.get("category_id")
        version_data["customer"] = version_data.get("customer_id")
        user = User.objects.get(id=version_data.get("user_id"))
        author = User.objects.get(id=version_data.get("author_id"))
        updated_by = User.objects.get(id=version_data.get("updated_by_id"))
        version_data.update(
            dict(
                user_name=user.name,
                author_name=author.name,
                updated_by_name=updated_by.name,
            )
        )
        return version_data

    class Meta:
        model = Version
        fields = ("id", "revision", "snapshot_data", "diff")


class SOWVersionCreateHistorySerializer(serializers.ModelSerializer):
    revision = RevisionSerializer()
    snapshot_data = serializers.SerializerMethodField()

    def get_snapshot_data(self, obj):
        version_data = obj.field_dict
        version_data["user"] = version_data.get("user_id")
        version_data["category"] = version_data.get("category_id")
        version_data["customer"] = version_data.get("customer_id")
        user = User.objects.get(id=version_data.get("user_id"))
        author = User.objects.get(id=version_data.get("author_id"))
        updated_by = User.objects.get(id=version_data.get("updated_by_id"))
        version_data.update(
            dict(
                user_name=user.name,
                author_name=author.name,
                updated_by_name=updated_by.name,
            )
        )
        return version_data

    class Meta:
        model = Version
        fields = "__all__"


class SOWTemplateVersionSerializer(serializers.ModelSerializer):
    revision = RevisionSerializer()
    snapshot_data = serializers.SerializerMethodField()
    diff = serializers.JSONField()

    def get_snapshot_data(self, obj):
        version_data = obj.field_dict
        author = User.objects.get(id=version_data.get("author_id"))
        updated_by = User.objects.get(id=version_data.get("updated_by_id"))
        version_data.update(
            dict(
                author_name=author.name,
                updated_by_name=updated_by.name,
            )
        )
        return version_data

    class Meta:
        model = Version
        fields = ("id", "revision", "snapshot_data", "diff")


class SOWTemplateVersionWithoutDiffSerializer(serializers.ModelSerializer):
    revision = RevisionSerializer()
    snapshot_data = serializers.SerializerMethodField()

    def get_snapshot_data(self, obj):
        version_data = obj.field_dict
        author = User.objects.get(id=version_data.get("author_id"))
        updated_by = User.objects.get(id=version_data.get("updated_by_id"))
        version_data.update(
            dict(
                author_name=author.name,
                updated_by_name=updated_by.name,
            )
        )
        return version_data

    class Meta:
        model = Version
        fields = ("id", "revision", "snapshot_data")
