from typing import Any, Dict, List

from deepdiff import DeepDiff
from django.db.models import Exists, IntegerField, Max, Min, OuterRef
from django.db.models.functions import Cast
from reversion.models import Version

from config_compliance.models import Rule, RuleVariable
from document.models import SOWDocument, SOWTemplate


class ChangeHistoryService:
    @staticmethod
    def get_object_versions(
        obj, add_diff=True, exclude_types=None, show_iterables_diff=True
    ):
        queryset = Version.objects.get_for_object(obj).select_related(
            "revision", "revision__user"
        )
        if add_diff:
            queryset = ChangeHistoryService.add_diff_to_queryset(
                queryset, exclude_types, show_iterables_diff
            )
        return queryset

    @staticmethod
    def add_diff_to_queryset(
        queryset, exclude_types=None, show_iterables_diff=True
    ):
        first_ver = True
        prev_version = None
        for ver in reversed(queryset):
            if first_ver:
                first_ver = False
                ver.diff = None
            else:
                diff = ChangeHistoryService.get_version_diffs(
                    ver,
                    prev_version,
                    exclude_types=exclude_types,
                    show_iterables_diff=show_iterables_diff,
                )
                ver.diff = diff
            prev_version = ver
        return queryset

    @staticmethod
    def get_version_diffs(
        new_version: Version,
        old_version: Version,
        exclude_paths: set = None,
        exclude_types: List = None,
        show_iterables_diff: bool = True,
    ) -> Dict:
        """
        Returns dict containing diff of two versions
        """
        new_version_data: Dict[Any, Any] = new_version.field_dict
        old_version_data: Dict[Any, Any] = old_version.field_dict
        exclude_paths = exclude_paths or set()
        exclude_paths = exclude_paths.union(
            {
                "root['updated_on']",
                "root['created_on']",
                "root['minor_version']",
                "root['major_version']",
            }
        )
        exclude_types = exclude_types or []
        diff: DeepDiff = DeepDiff(
            old_version_data,
            new_version_data,
            ignore_order=True,
            exclude_paths=exclude_paths,
            verbose_level=2,
            exclude_types=exclude_types,
        )
        version_diff = diff.to_dict()
        if not show_iterables_diff:
            for field, val in new_version_data.items():
                if isinstance(val, list) and val != old_version_data.get(
                    field
                ):
                    if version_diff.get("values_changed") is None:
                        version_diff["values_changed"] = {}
                    version_diff["values_changed"][f"root['{field}']"] = {
                        "new_value": val,
                        "old_value": old_version_data.get(field),
                    }
        version_diff.pop("type_changes", None)
        return version_diff

    @staticmethod
    def _get_model_create_history(model, provider_id):
        provider_object_ids = model.objects.filter(
            provider_id=provider_id
        ).values("id")
        subquery = (
            Version.objects.get_for_model(model)
            .values("object_id")
            .annotate(oldest_pk=Min("pk"))
            .values("oldest_pk")
        )
        created = (
            Version.objects.annotate(
                obj_id_int=Cast("object_id", output_field=IntegerField())
            )
            .filter(obj_id_int__in=provider_object_ids, id__in=subquery)
            .select_related("revision", "revision__user")
        )
        return created

    @staticmethod
    def _get_model_delete_history(model, provider_id):
        deleted = Version.objects.get_deleted(model).select_related(
            "revision", "revision__user"
        )
        deleted_for_provider = []
        for record in deleted:
            if record.field_dict.get("provider_id") == provider_id:
                deleted_for_provider.append(record)
        return deleted_for_provider

    @staticmethod
    def get_rule_create_history(provider_id):
        created = ChangeHistoryService._get_model_create_history(
            Rule, provider_id
        )
        return created

    @staticmethod
    def get_rule_delete_history(provider_id):
        deleted = ChangeHistoryService._get_model_delete_history(
            Rule, provider_id
        )
        return deleted

    @staticmethod
    def get_sow_document_create_history(provider_id):
        created = ChangeHistoryService._get_model_create_history(
            SOWDocument, provider_id
        )
        return created

    @staticmethod
    def get_sow_document_delete_history(provider_id):
        deleted = ChangeHistoryService._get_model_delete_history(
            SOWDocument, provider_id
        )
        return deleted

    @staticmethod
    def get_sow_template_create_history(provider_id):
        created = ChangeHistoryService._get_model_create_history(
            SOWTemplate, provider_id
        )
        return created

    @staticmethod
    def get_sow_template_delete_history(provider_id):
        model_qs = SOWTemplate.objects.annotate(
            _pk_to_object_id=Cast("pk", Version._meta.get_field("object_id"))
        ).filter(
            _pk_to_object_id=OuterRef("object_id"),
            is_disabled=False,
            provider_id=provider_id,
        )
        subquery = (
            Version.objects.get_for_model(SOWTemplate)
            .annotate(pk_not_exists=~Exists(model_qs))
            .filter(pk_not_exists=True)
            .values("object_id")
            .annotate(latest_pk=Max("pk"))
            .values("latest_pk")
        )

        deleted = (
            Version.objects.get_for_model(SOWTemplate)
            .filter(pk__in=subquery)
            .select_related("revision", "revision__user")
        )
        return deleted

    @staticmethod
    def get_variables_create_history(provider_id):
        created = ChangeHistoryService._get_model_create_history(
            RuleVariable, provider_id
        )
        return created

    @staticmethod
    def get_variables_delete_history(provider_id):
        deleted = ChangeHistoryService._get_model_delete_history(
            RuleVariable, provider_id
        )
        return deleted
