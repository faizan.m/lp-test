from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from reversion.models import Version

from config_compliance.models import (
    Rule,
    RuleVariable,
    RuleVariableCustomerMapping,
)
from core.change_history.serializer import (
    SOWTemplateVersionSerializer,
    SOWTemplateVersionWithoutDiffSerializer,
    SOWVersionCreateHistorySerializer,
    SOWVersionSerializer,
    VersionSerializer,
    VersionSerializerWithoutDiff,
)
from core.change_history.services import ChangeHistoryService
from document.models import SOWDocument, SOWTemplate


class RevertMixin(object):
    def post(self, request, *args, **kwargs):
        version_id = request.data.get("version_id")
        version = Version.objects.get(id=version_id)
        version.revision.revert()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RuleChangeHistoryListAPIView(generics.ListCreateAPIView):
    serializer_class = VersionSerializer

    def get_queryset(self):
        rule_id = self.kwargs.get("pk")
        rule = Rule.objects.get(provider=self.request.provider, id=rule_id)
        queryset = ChangeHistoryService.get_object_versions(
            rule, exclude_types=[list], show_iterables_diff=False
        )
        return queryset


class RuleCreateHistoryListAPIView(generics.ListAPIView):
    serializer_class = VersionSerializerWithoutDiff

    def get_queryset(self):
        provider_id = self.request.provider.id
        created = ChangeHistoryService.get_rule_create_history(provider_id)
        return created


class RuleDeleteHistoryListRevertAPIView(
    RevertMixin, generics.ListCreateAPIView
):
    serializer_class = VersionSerializerWithoutDiff

    def get_queryset(self):
        provider_id = self.request.provider.id
        deleted = ChangeHistoryService.get_rule_delete_history(provider_id)
        return deleted


class RuleVariablesChangeHistoryListAPIView(generics.ListCreateAPIView):
    serializer_class = VersionSerializer

    def get_queryset(self):
        var_id = self.kwargs.get("pk")
        customer_id = self.kwargs.get("customer_id")
        var = get_object_or_404(
            RuleVariable.objects.all(),
            provider=self.request.provider,
            id=var_id,
        )
        if (
            customer_id
            and var.customer_variables.filter(customer_id=customer_id).exists()
        ):
            customer_var = RuleVariableCustomerMapping.objects.get(
                provider=self.request.provider,
                variable_id=var_id,
                customer_id=customer_id,
            )
            queryset = ChangeHistoryService.get_object_versions(
                customer_var, exclude_types=[list], show_iterables_diff=False
            )
        else:
            queryset = ChangeHistoryService.get_object_versions(
                var, exclude_types=[list], show_iterables_diff=False
            )
        return queryset


class SOWDocumentChangeHistoryListAPIView(generics.ListCreateAPIView):
    serializer_class = SOWVersionSerializer

    def get_queryset(self):
        document_id = self.kwargs.get("pk")
        document = SOWDocument.objects.get(
            provider=self.request.provider, id=document_id
        )
        queryset = ChangeHistoryService.get_object_versions(document)
        return queryset


class SOWDocumentCreateHistoryListAPIView(generics.ListAPIView):
    serializer_class = SOWVersionCreateHistorySerializer

    def get_queryset(self):
        provider_id = self.request.provider.id
        created = ChangeHistoryService.get_sow_document_create_history(
            provider_id
        )
        return created


class SOWDocumentDeleteHistoryListRevertAPIView(
    RevertMixin, generics.ListCreateAPIView
):
    serializer_class = VersionSerializerWithoutDiff

    def get_queryset(self):
        provider_id = self.request.provider.id
        deleted = ChangeHistoryService.get_sow_document_delete_history(
            provider_id
        )
        return deleted


class SOWTemplateChangeHistoryListAPIView(generics.ListCreateAPIView):
    serializer_class = SOWTemplateVersionSerializer

    def get_queryset(self):
        document_id = self.kwargs.get("pk")
        template = SOWTemplate.objects.get(
            provider=self.request.provider, id=document_id
        )
        queryset = ChangeHistoryService.get_object_versions(template)
        return queryset


class SOWTemplateCreateHistoryListAPIView(generics.ListAPIView):
    serializer_class = SOWTemplateVersionWithoutDiffSerializer

    def get_queryset(self):
        provider_id = self.request.provider.id
        created = ChangeHistoryService.get_sow_template_create_history(
            provider_id
        )
        return created


class SOWTemplateDeleteHistoryListRevertAPIView(
    RevertMixin, generics.ListCreateAPIView
):
    serializer_class = SOWTemplateVersionWithoutDiffSerializer

    def get_queryset(self):
        provider_id = self.request.provider.id
        deleted = ChangeHistoryService.get_sow_template_delete_history(
            provider_id
        )
        return deleted

    def post(self, request, *args, **kwargs):
        version_id = request.data.get("version_id")
        version = Version.objects.get(id=version_id)
        template_id = version.object_id
        SOWTemplate.objects.filter(id=template_id).update(is_disabled=False)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ComplianceVariablesCreateHistoryListAPIView(generics.ListAPIView):
    serializer_class = VersionSerializerWithoutDiff

    def get_queryset(self):
        provider_id = self.request.provider.id
        created = ChangeHistoryService.get_variables_create_history(
            provider_id
        )
        return created


class ComplianceVariablesDeleteHistoryListRevertAPIView(
    RevertMixin, generics.ListCreateAPIView
):
    serializer_class = VersionSerializerWithoutDiff

    def get_queryset(self):
        provider_id = self.request.provider.id
        deleted = ChangeHistoryService.get_variables_delete_history(
            provider_id
        )
        return deleted
