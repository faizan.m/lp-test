from rest_framework import status
from rest_framework.exceptions import APIException


class InvalidConfigurations(APIException):
    """
    Exception used on Invalid Configurations
    """

    status_code = status.HTTP_412_PRECONDITION_FAILED
    default_detail = "Invalid Configuration."


class NotConfigured(InvalidConfigurations):
    default_detail = "Provider settings not configured."


class ERPAuthenticationNotConfigured(InvalidConfigurations):
    default_detail = "Authentication Credentials not configured for ERP."


class DeviceInfoAPINotConfigured(InvalidConfigurations):
    default_detail = "Device Manufacturer API not configured."


class DeviceMonitoringAPINotConfigured(InvalidConfigurations):
    default_detail = "Provider Monitoring Integration not done."


class SmartnetReceivingSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Smartnet Receiving Settings not configured for the Provider."
    )


class IngramAPISettingNotConfigured(InvalidConfigurations):
    default_detail = "Ingram API not configured for the Provider."


class OperationEmailTemplateSettingsNotConfigured(InvalidConfigurations):
    default_detail = (
        "Operation Email Template Settings not configured for the Provider"
    )


class CWPhoneNumberMappingSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "CW phone number mapping setting not configured for the Provider!"
    )


class SubscriptionNotificationSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Automated Subscription Notification Setting not configured "
        "for the Provider!"
    )
