from dataclasses import dataclass
from typing import Dict, List, Optional

from core.integrations.erp.connectwise.dataclasses import (
    ConnectwiseCommunicationItem,
)
from core.models import ConnectwisePhoneNumberMappingSetting
from core.services import logger


@dataclass
class UserPhone:
    cell_phone_number: str
    cell_phone_number_id: int
    office_phone: str
    office_phone_id: int


class ConnectwiseCommunicationItemsService:
    def __init__(self, provider):
        self.provider = provider

    def get_phone_number_mapping(self):
        """
        Get ConnectwisePhoneNumberMappingSetting for provider
        """
        try:
            return ConnectwisePhoneNumberMappingSetting.objects.get(
                provider=self.provider
            )
        except ConnectwisePhoneNumberMappingSetting.DoesNotExist:
            logger.info(
                "No connectwise phone number setting exist for the provider."
            )
            return None

    def parse_communication_items(
        self, communication_items: List[ConnectwiseCommunicationItem]
    ) -> Optional[UserPhone]:
        """
        Parses the list of CommunicationItem objects and returns UserPhone instance.
        :param communication_items: list of communication Items
        """
        phone_number_mappings: ConnectwisePhoneNumberMappingSetting = (
            self.get_phone_number_mapping()
        )
        if phone_number_mappings is None:
            logger.info(
                f"Can not parse communication_items as no ConnectwisePhoneNumberMappingSetting exist "
                "for the {self.provider}"
            )
            return
        cell_phone, cell_phone_crm_id, office_phone, office_phone_crm_id = (
            None,
            None,
            None,
            None,
        )
        for communication_item in communication_items:
            if communication_item.type_id == phone_number_mappings.cell_phone:
                cell_phone = communication_item.value
                cell_phone_crm_id = communication_item.id
            if (
                communication_item.type_id
                == phone_number_mappings.office_phone
            ):
                office_phone = communication_item.value
                office_phone_crm_id = communication_item.id
        return UserPhone(
            cell_phone, cell_phone_crm_id, office_phone, office_phone_crm_id
        )

    def get_user_phones(self, phone_communication_items: List[Dict]):
        cw_communication_items = []
        for communication_item in phone_communication_items:
            cw_communication_items.append(
                ConnectwiseCommunicationItem.from_json(communication_item)
            )
        user_phones = self.parse_communication_items(cw_communication_items)
        return user_phones
