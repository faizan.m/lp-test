import mock
from django.urls import reverse
from core.models import ConfigurationMapping
from test_utils import BaseProviderTestCase, TestClientMixin
from rest_framework.response import Response
from rest_framework.request import Request
from unittest.mock import MagicMock
from faker import Faker
from core.views import (
    CustomerTypesListAPIView,
    CustomerContactTypesListAPIView,
    VendorTypeCustomersListCreateAPIView,
    VendorContactCreateAPIView,
    CustomerStatusesListAPIView,
    TeamRolesListCreateAPIView,
    AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView,
    ConfigurationMappingRetrieveUpdateDestroyAPIView,
    ConfigurationMappingListCreateAPIView,
    SubscriptionNotificationTriggerAPIView,
    SubscriptionNotificationEmailPreviewAPIView,
)
from document.tests.factories import SOWVendorOnboardingSettingFactory
from faker import Faker
from typing import List, Dict, Optional, Union
from core.tests.factories import (
    ConnectwisePhoneNumberMappingSettingFactory,
    AutomatedSubscriptionNotificationSettingFactory,
    ConfigurationMappingFactory,
)


class TestCustomerTypesListAPIView(BaseProviderTestCase):
    """
    Test view: CustomerTypesListAPIView
    """

    customer_types_list_url: str = reverse(
        "api_v1_endpoints:providers:list-customer-types"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_customer_types(self):
        fake = Faker()
        self.provider.erp_client = MagicMock()
        self.provider.erp_client.get_company_types.return_value = [
            dict(
                customer_type_id=fake.pyint(),
                customer_type_name=fake.company(),
            ),
            dict(
                customer_type_id=fake.pyint(),
                customer_type_name=fake.company(),
            ),
        ]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.customer_types_list_url,
            self.provider,
            self.user,
        )
        view = CustomerTypesListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)


class TestCustomerContactTypesListAPIView(BaseProviderTestCase):
    """
    Test view: CustomerContactTypesListAPIView
    """

    customer_contact_types_list_url: str = reverse(
        "api_v1_endpoints:providers:list-customer-contact-types"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_customer_contact_types(self):
        fake = Faker()
        self.provider.erp_client = MagicMock()
        self.provider.erp_client.get_contact_types.return_value = [
            dict(id=fake.pyint(), description=fake.job()),
            dict(id=fake.pyint(), description=fake.job()),
        ]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.customer_contact_types_list_url,
            self.provider,
            self.user,
        )
        view = CustomerContactTypesListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)


class TestVendorTypeCustomersListCreateAPIView(BaseProviderTestCase):
    """
    Test view: VendorTypeCustomersListCreateAPIView
    """

    vendor_type_customers_list_create_url: str = reverse(
        "api_v1_endpoints:providers:list-create-vendor-type-customers"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_vendor_type_customers(self):
        SOWVendorOnboardingSettingFactory.create(
            provider=self.provider,
            vendor_list_statuses=[
                dict(
                    customer_status_name="Company INC.",
                    customer_status_crm_id=100,
                )
            ],
        )
        fake = Faker()
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_vendor_type_customers.return_value = [
            dict(id=fake.pyint(), name=fake.company())
        ]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.vendor_type_customers_list_create_url,
            self.provider,
            self.user,
        )
        view = VendorTypeCustomersListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.provider.erp_client.get_vendor_type_customers.assert_called_with(
            [100]
        )

    def test_vendor_type_customer_creation(self):
        fake = Faker()
        SOWVendorOnboardingSettingFactory.create(
            provider=self.provider,
        )
        request_body: Dict = dict(
            territory_crm_id=fake.pyint(),
            customer_site_name=fake.company(),
            customer_identifier=fake.word(),
            vendor_name=fake.company(),
            address_line_1=fake.street_address(),
            address_line_2=fake.street_address(),
            city=fake.city(),
            state=fake.city(),
            zip=fake.postcode(),
            website=fake.url(),
        )
        self.provider.erp_client = mock.MagicMock()
        vendor_data: Dict = dict(id=fake.pyint(), name=fake.company())
        self.provider.erp_client.create_vendor_type_customer.return_value = (
            vendor_data
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.vendor_type_customers_list_create_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = VendorTypeCustomersListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 201)
        self.provider.erp_client.create_vendor_type_customer.assert_called()
        self.assertEqual(response.data, vendor_data)

    def test_error_creating_vendor_when_sow_vendor_onbaording_settings_not_configured(
        self,
    ):
        fake = Faker()
        request_body: Dict = dict(
            customer_site_name=fake.company(),
            customer_identifier=fake.word(),
            vendor_name=fake.company(),
            address_line_1=fake.street_address(),
            address_line_2=fake.street_address(),
            city=fake.city(),
            state=fake.city(),
            zip=fake.postcode(),
            website=fake.url(),
        )
        self.provider.erp_client = mock.MagicMock()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.vendor_type_customers_list_create_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = VendorTypeCustomersListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_error_listing_vendor_customers_when_sow_vendor_onbaording_settings_not_configured(
        self,
    ):
        self.provider.erp_client = mock.MagicMock()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.vendor_type_customers_list_create_url,
            self.provider,
            self.user,
        )
        view = VendorTypeCustomersListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)


class TestVendorContactCreateAPIView(BaseProviderTestCase):
    """
    Test view: VendorContactCreateAPIView
    """

    vendor_contact_create_url = reverse(
        "api_v1_endpoints:providers:create-vendor-contact"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @mock.patch(
        "core.views.send_onboarding_email_to_vendor_primary_contact.apply_async"
    )
    def test_create_vendor_contact(
        self, send_onboarding_email_to_vendor_primary_contact_call
    ):
        fake = Faker()
        ConnectwisePhoneNumberMappingSettingFactory.create(
            provider=self.provider
        )
        request_body: Dict = dict(
            customer_crm_id=fake.pyint(),
            contact_type_crm_id=fake.pyint(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            title=fake.bs(),
            office_phone_number=fake.msisdn(),
            email=fake.email(),
        )
        mock_contact_email: str = "mockvendor@test.com"
        vendor_contact_details: Dict = dict(
            id=fake.pyint(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            email=mock_contact_email,
        )
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.create_customer_contact.return_value = (
            vendor_contact_details
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.vendor_contact_create_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = VendorContactCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 201)
        self.provider.erp_client.create_customer_contact.assert_called()
        send_onboarding_email_to_vendor_primary_contact_call.assert_called_with(
            (self.provider.id, mock_contact_email), queue="high"
        )
        self.assertEqual(response.data, vendor_contact_details)

    def test_error_creating_vendor_contact_when_cw_phone_number_mapping_not_configured(
        self,
    ):
        fake = Faker()
        request_body: Dict = dict(
            customer_crm_id=fake.pyint(),
            contact_type_crm_id=fake.pyint(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            title=fake.bs(),
            office_phone_number=fake.msisdn(),
            email=fake.email(),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.vendor_contact_create_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = VendorContactCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)


class TestCustomerStatusesListAPIView(BaseProviderTestCase):
    """
    Test view: CustomerStatusesListAPIView
    """

    customer_statuses_url = reverse(
        "api_v1_endpoints:providers:list-customer-statuses"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_customer_statuses(self):
        fake = Faker()
        self.provider.erp_client = MagicMock()
        self.provider.erp_client.get_customer_statuses.return_value = [
            dict(
                customer_status_crm_id=fake.pyint(),
                customer_status_name=fake.job(),
                is_inactive=fake.pybool(),
            ),
            dict(
                customer_status_crm_id=fake.pyint(),
                customer_status_name=fake.job(),
                is_inactive=fake.pybool(),
            ),
        ]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.customer_statuses_url,
            self.provider,
            self.user,
        )
        view = CustomerStatusesListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)


class TestTeamRolesListCreateAPIView(BaseProviderTestCase):
    """
    Test view: TeamRolesListCreateAPIView
    """

    team_role_list_create_url: str = reverse(
        "api_v1_endpoints:providers:list-create-team-roles"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    def test_listing_of_team_roles(self):
        self.provider.erp_client = MagicMock()
        team_roles_list: List[Dict] = [
            dict(
                role_crm_id=self.fake.pyint(),
                role_name=self.fake.job(),
                account_manager_flag=self.fake.pybool(),
                tech_flag=self.fake.pybool(),
                sales_flag=self.fake.pybool(),
            )
        ]
        self.provider.erp_client.get_team_roles.return_value = team_roles_list
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.team_role_list_create_url,
            self.provider,
            self.user,
        )
        view = TeamRolesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_creation_of_team_role(self):
        self.provider.erp_client = MagicMock()
        request_body: Dict = dict(
            role_name=self.fake.job(),
        )
        self.provider.erp_client.create_team_role.return_value = MagicMock()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.team_role_list_create_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = TeamRolesListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 201)
        self.provider.erp_client.create_team_role.assert_called_with(
            request_body
        )


class TestAutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView(
    BaseProviderTestCase
):
    """
    Test view: AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView
    """

    setting_url: str = reverse(
        "api_v1_endpoints:providers:subscription-notices-setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    def test_creation_of_subscription_notices_setting(self):
        request_body: Dict = dict(
            receiver_email_ids=[self.fake.email()],
            sender=self.user.id,
            email_subject=self.fake.text(max_nb_chars=10),
            email_body=self.fake.text(max_nb_chars=10),
            renewal_type_contact_crm_id=1,
            weekly_send_schedule=["FRIDAY", "SUNDAY"],
            approaching_expiry_date_range=["EXPIRED"],
            config_type_crm_ids=[1, 2],
            config_status_crm_ids=[10, 20],
            manufacturer_crm_ids=[11, 12],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual(
            [11, 12],
            self.provider.subscription_notification_setting.manufacturer_crm_ids,
        )

    def test_error_creating_setting_if_already_exists(self):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        request_body: Dict = dict(
            receiver_email_ids=[self.fake.email()],
            sender=self.user.id,
            email_subject=self.fake.text(max_nb_chars=10),
            email_body=self.fake.text(max_nb_chars=10),
            renewal_type_contact_crm_id=1,
            weekly_send_schedule=["FRIDAY", "SUNDAY"],
            approaching_expiry_date_range=["EXPIRED"],
            config_type_crm_ids=[1, 2],
            config_status_crm_ids=[10, 20],
            manufacturer_crm_ids=[11, 12],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_retrieval_of_subscription_notices_setting(self):
        AutomatedSubscriptionNotificationSettingFactory(
            provider=self.provider,
            sender=self.user,
            receiver_email_ids=["abc@alphabet.com"],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.setting_url,
            self.provider,
            self.user,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            ["abc@alphabet.com"], response.data.get("receiver_email_ids")
        )

    def test_error_retrieving_setting_if_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.setting_url,
            self.provider,
            self.user,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(404, response.status_code)

    def test_update_to_subscription_notices_setting(self):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        request_body: Dict = dict(
            receiver_email_ids=[self.fake.email()],
            sender=self.user.id,
            email_subject=self.fake.text(max_nb_chars=10),
            email_body=self.fake.text(max_nb_chars=10),
            renewal_type_contact_crm_id=1,
            weekly_send_schedule=["FRIDAY", "SUNDAY"],
            approaching_expiry_date_range=["EXPIRED"],
            config_type_crm_ids=[1, 2],
            config_status_crm_ids=[10, 20],
            manufacturer_crm_ids=[11, 12],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            [1, 2],
            self.provider.subscription_notification_setting.config_type_crm_ids,
        )

    def test_error_updating_subscription_notices_setting_if_not_configured(
        self,
    ):
        request_body: Dict = dict(
            receiver_email_ids=[self.fake.email()],
            sender=self.user.id,
            email_subject=self.fake.text(max_nb_chars=10),
            email_body=self.fake.text(max_nb_chars=10),
            renewal_type_contact_crm_id=1,
            weekly_send_schedule=["FRIDAY", "SUNDAY"],
            approaching_expiry_date_range=["EXPIRED"],
            config_type_crm_ids=[1, 2],
            config_status_crm_ids=[10, 20],
            manufacturer_crm_ids=[11, 12],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = (
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(404, response.status_code)


class TestConfigurationMappingListCreateAPIView(BaseProviderTestCase):
    """
    Test view: ConfigurationMappingListCreateAPIView
    """

    url: str = reverse("api_v1_endpoints:providers:config-mapping-list-create")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    def test_creation_of_config_mapping(self):
        request_body: Dict = dict(name="ABC", config_type_crm_ids=[1, 2])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = ConfigurationMappingListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual([1, 2], response.data.get("config_type_crm_ids"))

    def test_listing_of_config_mappings(self):
        ConfigurationMappingFactory.create_batch(2, provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = ConfigurationMappingListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(response.data.get("results")))


class TestConfigurationMappingRetrieveUpdateDestroyAPIView(
    BaseProviderTestCase
):
    """
    Test view: ConfigurationMappingRetrieveUpdateDestroyAPIView
    """

    url: str = (
        "api_v1_endpoints:providers:config-mapping-retrive-update-destroy"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    def test_config_mappping_update(self):
        mapping: ConfigurationMapping = ConfigurationMappingFactory(
            provider=self.provider, name="Skynet", config_type_crm_ids=[1, 2]
        )
        request_body: Dict = dict(name="Skyfall", config_type_crm_ids=[3, 4])
        url: str = reverse(self.url, kwargs=dict(pk=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = ConfigurationMappingRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=mapping.id)
        self.assertEqual(200, response.status_code)
        mapping.refresh_from_db()
        self.assertEqual("Skyfall", mapping.name)
        self.assertEqual([3, 4], mapping.config_type_crm_ids)

    def test_retrieve_config_mapping(self):
        mapping: ConfigurationMapping = ConfigurationMappingFactory(
            provider=self.provider, name="Skynet", config_type_crm_ids=[1, 2]
        )
        url: str = reverse(self.url, kwargs=dict(pk=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = ConfigurationMappingRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=mapping.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data.get("name"), mapping.name)

    def test_delete_config_mapping(self):
        mapping: ConfigurationMapping = ConfigurationMappingFactory(
            provider=self.provider, name="Skynet", config_type_crm_ids=[1, 2]
        )
        url: str = reverse(self.url, kwargs=dict(pk=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = ConfigurationMappingRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=mapping.id)
        self.assertEqual(204, response.status_code)
        mapping_exists: bool = ConfigurationMapping.objects.filter(
            id=mapping.id
        ).exists()
        self.assertFalse(mapping_exists)


class TestSubscriptionNotificationTriggerAPIView(BaseProviderTestCase):
    """
    Test view: SubscriptionNotificationTriggerAPIView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:send-subscription-notification"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    @mock.patch("core.views.send_subscription_notification_email.apply_async")
    def test_trigger_subscription_notice_emails_for_all_customers(
        self, send_subscription_notification_email
    ):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        request_body: Dict = dict(all_customers=True, customer_crm_ids=[])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SubscriptionNotificationTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(202, response.status_code)
        send_subscription_notification_email.assert_called_with(
            (self.provider.id, True, []), queue="high"
        )

    @mock.patch("core.views.send_subscription_notification_email.apply_async")
    def test_trigger_subscription_notice_emails_for_selected_customers(
        self, send_subscription_notification_email
    ):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        request_body: Dict = dict(all_customers=False, customer_crm_ids=[1, 2])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SubscriptionNotificationTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(202, response.status_code)
        send_subscription_notification_email.assert_called_with(
            (self.provider.id, False, [1, 2]), queue="high"
        )

    def test_error_triggering_subscription_notice_emails_when_setting_not_configured(
        self,
    ):
        request_body: Dict = dict(all_customers=False, customer_crm_ids=[1, 2])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SubscriptionNotificationTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)


class TestSubscriptionNotificationEmailPreviewAPIView(BaseProviderTestCase):
    """
    Test view: SubscriptionNotificationEmailPreviewAPIView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:preview-subscription-notification-email"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    @mock.patch(
        "core.services.SubscriptionNotificationService.generate_subscription_notification_email_preview",
        return_value="<p>Hello! You have an email</p>",
    )
    def test_subscription_notification_email_preview(
        self, generate_subscription_notification_email_preview
    ):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = SubscriptionNotificationEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            "<p>Hello! You have an email</p>", response.data.get("html")
        )

    def test_error_prviewing_email_if_setting_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = SubscriptionNotificationEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)
