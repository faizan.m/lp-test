import random

from core.models import (
    ConnectwisePhoneNumberMappingSetting,
    AutomatedSubscriptionNotificationSetting,
    ConfigurationMapping,
)
import factory
from accounts.tests.factories import ProviderFactory, ProviderUserFactory
from faker import Faker
import datetime
from factory.fuzzy import FuzzyChoice
from core.models import (
    ProviderIntegration,
    IntegrationType,
    IntegrationCategory,
)

fake = Faker()


class ConnectwisePhoneNumberMappingSettingFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = ConnectwisePhoneNumberMappingSetting

    provider = factory.SubFactory(ProviderFactory)
    cell_phone = fake.pyint(min_value=1, max_value=10)
    office_phone = fake.pyint(min_value=1, max_value=10)
    last_updated_on = factory.LazyFunction(datetime.datetime.now)


class AutomatedSubscriptionNotificationSettingFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = AutomatedSubscriptionNotificationSetting

    provider = factory.SubFactory(ProviderFactory)
    receiver_email_ids = [fake.email(), fake.email()]
    sender = factory.SubFactory(ProviderUserFactory)
    email_subject = fake.text(max_nb_chars=10)
    email_body = fake.text(max_nb_chars=10)
    renewal_type_contact_crm_id = fake.pyint(min_value=1)
    weekly_send_schedule = ["MONDAY", "TUESDAY"]
    approaching_expiry_date_range = [
        AutomatedSubscriptionNotificationSetting.EXPIRED,
        AutomatedSubscriptionNotificationSetting.NEXT_15_DAYS,
    ]
    config_type_crm_ids = [fake.pyint(min_value=1), fake.pyint(min_value=1)]
    config_status_crm_ids = [fake.pyint(min_value=1), fake.pyint(min_value=1)]
    manufacturer_crm_ids = [fake.pyint(min_value=1)]


class ConfigurationMappingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConfigurationMapping

    provider = factory.SubFactory(ProviderFactory)
    name = fake.name()
    config_type_crm_ids = [fake.pyint(), fake.pyint()]


class IntegrationCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = IntegrationType

    name = random.choice(
        ["CRM", "DEVICE_INFO", "DEVICE_MONITOR", "STORAGE_SERVICE"]
    )


class IntegrationTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = IntegrationType

    name = random.choice(["CISCO", "DROPBOX", "LOGIC_MONITOR", "CONNECTWISE"])
    category = factory.SubFactory(IntegrationCategoryFactory)


class ProviderIntegrationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProviderIntegration

    provider = factory.SubFactory(ProviderFactory)
    integration_type = factory.SubFactory(IntegrationTypeFactory)
    # Initialize these as needed in tests
    authentication_info = {}
    other_config = {}
