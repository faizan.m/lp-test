import mock
from typing import List, Dict
from core.services import SubscriptionNotificationService
from test_utils import BaseProviderTestCase
from unittest.mock import AsyncMock, Mock
from core.tests.factories import (
    AutomatedSubscriptionNotificationSettingFactory,
)
from datetime import datetime
from accounts.tests.factories import CustomerFactory


class TestSubscriptionNotificationService(BaseProviderTestCase):
    """
    Test class: SubscriptionNotificationService
    """

    service_class = SubscriptionNotificationService

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_format_term_end_date(self):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        term_end_date: str = "2023-05-04T11:46:53Z"
        result = self.service_class(
            provider=self.provider
        ).format_term_end_date(term_end_date)
        self.assertEqual("05/04/2023", result)

    @mock.patch("core.services.timezone")
    def test_get_days_until_term_end_date_value(self, mock_timezone):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        mock_timezone.now.return_value = datetime(day=1, month=1, year=2023)
        term_end_date: str = "2023-01-04T11:46:53Z"
        result = self.service_class(
            provider=self.provider
        ).get_days_until_term_end_date_value(term_end_date)
        self.assertEqual(3, result)

    @mock.patch("core.services.timezone")
    def test_process_subscriptions(self, mock_timezone):
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_product_catalogs_using_model_number.return_value = [
            dict(
                catalog_crm_id=1,
                part_number="M123",
                description="This is description for M123",
            ),
            dict(
                catalog_crm_id=2,
                part_number="M456",
                description="This is description for M456",
            ),
        ]
        mock_timezone.now.return_value = datetime(day=1, month=5, year=2023)
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        mock_subscriptions: List[Dict] = [
            dict(
                serialNumber="SN123",
                name="A123",
                warrantyExpirationDate="2023-05-04T11:46:53Z",
                needsRenewalFlag=True,
                modelNumber="M123",
            ),
            dict(
                serialNumber="SN456",
                name="A456",
                warrantyExpirationDate="2023-05-10T11:46:53Z",
                needsRenewalFlag=False,
                modelNumber="M456",
            ),
        ]
        result: List[Dict] = self.service_class(
            provider=self.provider
        ).process_subscriptions(mock_subscriptions)
        expected_result: List[Dict] = [
            dict(
                subscription_id="SN123",
                subscription_type="A123",
                term_end_date="05/04/2023",
                auto_renew="Yes",
                days_until_term_end_date=3,
                product_description="This is description for M123",
            ),
            dict(
                subscription_id="SN456",
                subscription_type="A456",
                term_end_date="05/10/2023",
                auto_renew="No",
                days_until_term_end_date=9,
                product_description="This is description for M456",
            ),
        ]
        self.assertEqual(result, expected_result)

    def test_filter_subscriptions_by_approaching_term_end_dates(self):
        AutomatedSubscriptionNotificationSettingFactory(
            provider=self.provider, approaching_expiry_date_range=["EXPIRED"]
        )
        mock_subscriptions: List[Dict] = [
            dict(
                serialNumber="SN123",
                name="A123",
                warrantyExpirationDate="2022-05-04T11:46:53Z",
                needsRenewalFlag=True,
            ),
            dict(
                serialNumber="SN456",
                name="A456",
                warrantyExpirationDate="2050-05-10T11:46:53Z",
                needsRenewalFlag=False,
            ),
        ]
        result: List[Dict] = self.service_class(
            provider=self.provider
        ).filter_subscriptions_by_approaching_term_end_dates(
            mock_subscriptions
        )
        self.assertEqual(mock_subscriptions[0], result[0])

    def test_get_customer_renewal_contact_details(self):
        AutomatedSubscriptionNotificationSettingFactory(
            provider=self.provider, renewal_type_contact_crm_id=3
        )
        self.provider.erp_client = mock.MagicMock()
        mock_contact_details: Dict = dict(
            first_name="John",
            last_name="Wick",
            title="Elite assassin",
            email="john@wick.com",
        )
        self.provider.erp_client.get_customer_users.return_value = [
            mock_contact_details,
        ]
        result = self.service_class(
            provider=self.provider
        ).get_customer_renewal_contact_details(101)
        self.assertEqual(result, mock_contact_details)

    def test_render_subscription_notice_email_body(self):
        AutomatedSubscriptionNotificationSettingFactory(provider=self.provider)
        result: str = self.service_class(
            provider=self.provider
        ).render_subscription_notice_email_body({})
        self.assertTrue(len(result) > 0)

    def test_generate_subscription_notification_email_preview(self):
        AutomatedSubscriptionNotificationSettingFactory(
            provider=self.provider, sender=self.user
        )
        result: str = self.service_class(
            provider=self.provider
        ).generate_subscription_notification_email_preview()
        self.assertTrue(len(result) > 0)

    @mock.patch("core.services.AutomatedSubscriptionNotificationEmail.send")
    @mock.patch(
        "core.services.SubscriptionNotificationService.render_subscription_notice_email_body",
        return_value="<p>Hello World!</p>",
    )
    @mock.patch(
        "core.services.SubscriptionNotificationService.get_customer_renewal_contact_details",
        return_value=dict(
            first_name="John",
            last_name="Wick",
            title="Assassin",
            email="john@wick.com",
        ),
    )
    @mock.patch(
        "core.services.SubscriptionNotificationService.get_expiring_subscriptions_data_for_customer",
        new_callable=AsyncMock(
            return_value=[
                dict(
                    subscription_id="SN123",
                    subscription_type="A123",
                    product_description="",
                    term_end_date="05/04/2023",
                    auto_renew="Yes",
                    days_until_term_end_date=3,
                )
            ]
        ),
    )
    async def test_send_expiring_subscription_notification_to_customer(
        self,
        get_expiring_subscriptions_data_for_customer,
        get_customer_renewal_contact_details,
        render_subscription_notice_email_body,
        send_email,
    ):
        session = AsyncMock()
        customer = CustomerFactory(provider=self.provider)
        AutomatedSubscriptionNotificationSettingFactory(
            provider=self.provider, sender=self.user
        )
        # Test when expiring subscriptions exist
        email_status: Dict = await self.service_class(
            provider=self.provider
        ).send_expiring_subscription_notification_to_customer(
            session, customer
        )
        self.assertEqual(
            dict(
                email_sent=True,
                reason=None,
                customer_name=customer.name,
                customer_crm_id=customer.crm_id,
            ),
            email_status,
        )
        get_expiring_subscriptions_data_for_customer.assert_awaited_once()
        send_email.assert_called()
        # Test when expiring subcriptions do not exist
        get_expiring_subscriptions_data_for_customer.retunrn_value = []
        email_status: Dict = await self.service_class(
            provider=self.provider
        ).send_expiring_subscription_notification_to_customer(
            session, customer
        )
        self.assertEqual(
            dict(
                email_sent=False,
                reason="Expiring subscriptions not found.",
                customer_name=customer.name,
                customer_crm_id=customer.crm_id,
            ),
            email_status,
        )
