import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from storages.backends.s3boto3 import S3Boto3Storage


class MediaStorage(S3Boto3Storage):
    """
    Storage class to store files to S3
    """

    location = "media"


class OverwriteStorage(FileSystemStorage):
    """
    Storage class to store files in filesystem with file overwrite
    """

    def get_available_name(self, name, max_length=None):
        """
        Returns a filename that's free on the target storage system, and
        available for new content to be written to.
        """
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name

    def upload_file(self, local_file_path):
        return local_file_path


class TemporaryS3Storage(S3Boto3Storage):
    querystring_auth = True
    location = "media"
    default_acl = "private"

    def upload_file(self, local_file_path):
        filename = local_file_path.lstrip("/")
        with open(local_file_path, "rb") as f_in:
            self.save(filename, f_in)
        return self.url(filename)


class PermanentS3Storage(S3Boto3Storage):
    querystring_auth = False
    location = "media/attachments"
    default_acl = "public-read"

    def upload_file(self, local_file_path):
        filename = os.path.basename(local_file_path)
        with open(local_file_path, "rb") as f_in:
            self.save(filename, f_in)
        return self.url(filename)

#
# class LogsS3Storage(S3Boto3Storage):
#     querystring_auth = False
#     location = "logs/merge-customers"
#     default_acl = "public-read"
#
#     def upload_file(self, local_file_path):
#         filename = os.path.basename(local_file_path)
#         with open(local_file_path, "rb") as f_in:
#             self.save(filename, f_in)
#         return self.url(filename)

def get_storage_class():
    """
    Returns storage class instance to store files
    :return: Storage class Instance
    """
    # If running in DEBUG mode return File system storage
    if settings.DEBUG:
        return OverwriteStorage()
    else:
        # Return AWS s3 storage class instance
        return MediaStorage()


def get_temporary_storage_class():
    # If running in DEBUG mode return File system storage
    if settings.DEBUG:
        return OverwriteStorage()
    else:
        # Return AWS s3 storage class instance
        return TemporaryS3Storage()


def get_permanent_storage_class():
    # If running in DEBUG mode return File system storage
    if settings.DEBUG:
        return OverwriteStorage()
    else:
        # Return AWS s3 storage class instance
        return PermanentS3Storage()
