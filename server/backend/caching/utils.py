import functools

from redis import RedisError

from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


def handle_caching_exception():
    """
    Decorator function to handle exceptions in Caching.
    """

    def decorator(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            try:
                return func(self, *args, **kwargs)
            except RedisError as err:
                logger.info(
                    "Caching Service: Error connecting to Redis --error: {}".format(
                        err.args
                    )
                )
                return

        return wrapper

    return decorator
