import functools

from box import Box
from django.core.cache import cache

from caching.utils import handle_caching_exception


class CacheService(object):
    """
    Set cache type according to the use case. Values: PROVIDER - If caching is done using provider id. CUSTOMER - If
    caching is done using customer id and provider id
    """

    CACHE_TYPE = Box(PROVIDER="PROVIDER", CUSTOMER="CUSTOMER")
    CACHE_KEYS = Box(
        DEVICE_LIST="DEVICE_LIST-{}-{}",
        SERVICE_DASHBOARD="SERVICE_DASHBOARD-{}-{}",
        ORDERS_DASHBOARD="ORDERS_DASHBOARD-{}-{}",
        SITE_LIST="SITE_LIST-{}-{}",
        MANUFACTURER_LIST="MANUFACTURER_LIST-{}",
        COUNTRIES_LIST="COUNTRIES_LIST-{}",
        STATE_LIST="STATE_LIST-{}",
        COUNTRIES_AND_STATE_LIST="COUNTRIES_AND_STATE_LIST-{}",
        OPEN_SERVICE_REQUEST_TICKETS="OPEN_SERVICE_REQUEST_TICKETS-{}-{}",
        CLOSED_SERVICE_REQUEST_TICKETS="CLOSED_SERVICE_REQUEST_TICKETS-{}-{}",
        OPEN_QUOTES_DASHBOARD="OPEN_QUOTES_DASHBOARD-{}-{}",
        OPPORTUNITY_LIST="OPPORTUNITY_LIST-{}-{}",
        SUBSCRIPTION_LISTING="SUBSCRIPTION_LISTING-{}-{}",
        CONFIGURATION_LISTING="CONFIGURATION_LISTING-{}",
        ALERTS_LISTING="ALERTS_LISTING-{}-{}",
        SERVICE_TICKET_LISTING="SERVICE_TICKET_LISTING-{}-{}",
        SOW_DASHBOARD_RAW_DATA_TABLE="SOW_DASHBOARD_RAW_DATA_TABLE-{}",
        DEVICE_CONFIG_ID="DEVICE_CONFIG_ID-{}"
    )

    @classmethod
    @handle_caching_exception()
    def exists(cls, key):
        return key in cache

    @classmethod
    @handle_caching_exception()
    def get_data(cls, key, *args):
        hash_value = CacheService._get_params_hash(*args)
        data = cache.get(key, {}).get(hash_value, None)
        return data

    @classmethod
    @handle_caching_exception()
    def invalidate_data(cls, keys):
        if not isinstance(keys, list):
            keys = [keys]
        for key in keys:
            cache.delete(key)

    @classmethod
    @handle_caching_exception()
    def set_data(cls, key, data, *args):
        hash_value = CacheService._get_params_hash(*args)
        if CacheService.exists(key):
            c_data = cache.get(key)
            c_data[hash_value] = data
        else:
            c_data = {hash_value: data}
        return cache.set(key, c_data)

    @classmethod
    @handle_caching_exception()
    def invalidate_cache(cls):
        cache.clear()

    @classmethod
    def _get_params_hash(cls, *args):
        hash_value = hash(args)
        return hash_value


def caching(cache_key, cache_type, invalidate=False):
    def decorator(func):
        """
        Decorator to handle caching for Third party API calls.
        """

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            key = cache_key
            if cache_type == CacheService.CACHE_TYPE.CUSTOMER:
                provider_id = args[0]
                customer_id = args[1]
                key = key.format(provider_id, customer_id)
                meta_keys = args[2:]
            else:
                provider_id = args[0]
                key = key.format(provider_id)
                meta_keys = args[1:]
            if invalidate:
                CacheService.invalidate_data([key])
                return func(self, *args, **kwargs)
            if CacheService.exists(key):
                data = CacheService.get_data(key, meta_keys)
                if data is not None:
                    return data
            data = func(self, *args, **kwargs)
            CacheService.set_data(key, data, meta_keys)
            return data

        return wrapper

    return decorator
