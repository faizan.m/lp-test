from collections import defaultdict
from typing import List, Dict, Optional, Set, Union, Any
from uuid import UUID

from celery import chain, group, shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import QuerySet
from django.utils import timezone

from accounts.models import User, Provider, Customer
from core.exceptions import ERPAuthenticationNotConfigured
from document.services.quote_service import QuoteService
from document.utils import download_file
from exceptions.exceptions import APIError
from sales.emails import (
    SalesActivityMail,
    AutomatedCollectionNoticeEmail,
    AgreementUpdateEmail,
)
from sales.integrations.pax8.pax8 import PAX8APIAsyncClient
from sales.integrations.pax8.pax8 import get_pax8_api_async_client
from sales.models import (
    AutomatedCollectionNoticesSettings,
    Agreement,
    PAX8Product,
    PAX8ProductPricingInfo,
    PAX8IntegrationSettings,
)
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
)
from sales.services.collections_service import CollectionInvoicesService
from sales.services.pax8_service import (
    PAX8ProductsDataPayloadHandler,
    PAX8ProductPricingDataPayloadHandler,
)
from utils import DbOperation

logger = get_task_logger(__name__)


@shared_task
def send_sales_activity_email(
    provider_id,
    recipients: str,
    request_user_id: str,
    context,
) -> None:
    """
    Sends the sales activity email to the assigned user.

    """
    request_user = User.providers.get(
        id=request_user_id, provider_id=provider_id
    )
    from_email = f"{request_user.name} <{settings.DEFAULT_FROM_EMAIL}>"
    provider = Provider.objects.get(id=provider_id)
    context.update(provider=provider)
    SalesActivityMail(
        context=context,
        files=None,
        remote_attachments=None,
        images=None,
        from_email=from_email,
    ).send(to=[recipients])
    logger.info(f"Sales activity email sent to {recipients}")


@shared_task
def send_collection_notices(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Send automated collection notice email. Automated Collection Notices Setting needs to be configured for task to run.
    Runs as periodic task for all providers.

    Args:
        provider_ids: Optional list of provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in providers:
        if not AutomatedCollectionNoticesSettings.configured_for_provider(
            provider
        ):
            logger.debug(
                f"Automated Collection Notices Setting not configured for the "
                f"provider: {provider.name}, ID: {provider.id}. "
                f"Automated collection notice emails will not be sent."
            )
            continue
        collection_notice_setting: AutomatedCollectionNoticesSettings = (
            provider.automated_collection_notices_setting
        )
        if collection_notice_setting.scheduled_for_today():
            customers: "QuerySet[Customer]" = Customer.objects.filter(
                provider_id=provider.id, is_active=True, is_deleted=False
            )
            collection_service: CollectionInvoicesService = (
                CollectionInvoicesService(provider)
            )
            for customer in customers:
                (
                    email_sent,
                    error,
                ) = collection_service.send_collection_notice_email_for_customer(
                    customer
                )
                if email_sent:
                    logger.info(
                        f"Automated collection notice email sent for customer: {customer.name}, "
                        f"customer_crm_id: {customer.crm_id} for provider: {provider.name}, ID: {provider.id}"
                    )
                else:
                    logger.info(
                        f"Could not send collection notice email for provider: {provider.name}, ID: {provider.id}. "
                        f"Error: {error}"
                    )
            logger.info(
                f"Finished sending automated collection notice emails for customers of "
                f"the provider: {provider.name}, ID: {provider.id}!"
            )
    return


@shared_task
def trigger_collection_notice_emails(
    provider_id: int, all_customers: bool, customer_crm_ids: List[int]
) -> Dict:
    """
    Task to trigger automated collection notice emails. The emails will either be sent to
    for all customer or for selected customers.

    Args:
        provider_id: Provider ID
        all_customers: True if need to send for all customers else False
        customer_crm_ids: Customer CRM IDs
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    logger.info(
        f"Task to send collection notice emails triggered for {provider_id=}, {all_customers=}, {customer_crm_ids=}"
    )
    if not AutomatedCollectionNoticesSettings.configured_for_provider(
        provider
    ):
        reason: str = f"Automated Collection Notice setting not configured for provider: {provider.name}"
        logger.info(f"Skipped sending collection notice emails. {reason}")
        return dict(operation_status="FAILURE", reason=reason, payload={})
    else:
        if all_customers:
            customers: "QuerySet[Customer]" = Customer.objects.filter(
                provider_id=provider_id, is_active=True, is_deleted=False
            )
        else:
            customers: "QuerySet[Customer]" = Customer.objects.filter(
                provider_id=provider_id, crm_id__in=customer_crm_ids
            )
        collection_service: CollectionInvoicesService = (
            CollectionInvoicesService(provider)
        )
        skipped_customers: List[str] = list()
        for customer in customers:
            (
                email_sent,
                error,
            ) = collection_service.send_collection_notice_email_for_customer(
                customer
            )
            if email_sent:
                logger.info(
                    f"Collection notice email sent for customer: {customer.name}, CRM ID: {customer.crm_id}"
                )
            else:
                skipped_customers.append(customer.name)
                logger.info(
                    f"Could not send collection notice email for customer: {customer.name}, CRM ID: {customer.crm_id} "
                    f"Error: {error}"
                )
        return dict(
            operation_status="SUCCESS",
            reason=None,
            payload=dict(
                skipped_customers=skipped_customers,
                reason="Invoices not found",
            ),
        )


@shared_task
def send_agreement_to_account_manager(
    agreement_id: int, provider_id: int, user_id: UUID
):
    """
    Task to send agreement PDF to customer account manager.

    Args:
        agreement_id: Agreement ID
        provider_id: Provider ID
        user_id: User ID

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    try:
        erp_client = provider.erp_client
    except ERPAuthenticationNotConfigured as exc:
        logger.debug(
            f"Provider: {provider.name}, ID: {provider.id}. {exc}. "
            f"Skip sending agreement to account manager."
        )
        return
    agreement: Agreement = Agreement.objects.get(
        id=agreement_id, provider_id=provider_id
    )
    try:
        customer: Customer = Customer.objects.get(
            id=agreement.customer_id, provider_id=provider_id
        )
    except Customer.DoesNotExist:
        logger.debug(
            f"Customer with ID: {agreement.customer_id} does not exist in DB!"
            f"Skip sending agreement to account manager."
        )
        return

    cw_customer = erp_client.get_customer(customer.crm_id)
    territory_manager_id: int = cw_customer.get("territory_manager_id", None)
    if not territory_manager_id:
        logger.error(
            f"Territory Manager ID not present for Customer: {customer.name}, ID: {customer.crm_id} of "
            f"provider: {provider.name}, ID: {provider.id}. "
            f"Skip sending agreement to account manager."
        )
        return
    territory_manager = erp_client.get_member(territory_manager_id)
    territory_manager_email_id: str = territory_manager.get("email")
    if not territory_manager_id:
        logger.error(
            f"Territory manager email ID not found for territory manager with ID: {territory_manager_id} of "
            f"customer: {customer.name}, ID: {customer.crm_id} "
            f"of provider: {provider.name}, ID: {provider.id}. "
            f"Skip sending agreement to account manager."
        )
        return

    user: User = User.objects.get(id=user_id, provider_id=provider_id)
    quote = dict()
    try:
        quote = QuoteService.get_quote(provider_id, agreement.quote_id)
    except Exception:
        logger.debug(
            f"Quote doesn't exist in CW with ID: {agreement.quote_id} of agreement: {agreement.name}, ID: {agreement.id}"
        )

    agreement_service: AgreementTemplatePreviewService = (
        AgreementTemplatePreviewService()
    )
    try:
        agreement_pdf: Dict = agreement_service.download_agreement_pdf(
            agreement
        )
    except Exception:
        logger.error(
            f"Error generating PDF preview for agreement: {agreement.name}, ID: {agreement.id}, "
            f"provider: {provider.name}, ID: {provider.id}. Skip sending agreement to account manager."
        )
        return

    file_path = agreement_pdf.get("file_path")
    file_name: str = agreement_pdf.get("file_name")
    attachments = [(file_name, file_path)]
    files = list()
    remote_attachments = list()
    if settings.DEBUG:
        files = attachments
    else:
        remote_attachments = attachments
    email_context: Dict = dict(
        customer_name=customer.name,
        agreement_name=agreement.name,
        quote=quote,
        terrritory_manager=territory_manager,
        provider=provider,
        user=user,
    )
    AgreementUpdateEmail(
        context=email_context,
        files=files,
        remote_attachments=remote_attachments,
    ).send(
        to=[territory_manager_email_id],
    )
    logger.info(
        f"Successfully sent agreement: {agreement.name}, ID: {agreement.id} of provider: {provider.name} "
        f"to account manager."
    )


@shared_task
def sync_pax8_products(
    provider_ids: Optional[List[int]] = None,
) -> Dict[int, Dict]:
    """
    Task to sync pax8 products data in DB. Syncs pax8 product data in the model PAX8Product.

    Args:
        provider_ids: List of provider IDs.

    Returns:
        Provider sync result.
    """
    provider_sync_result: Dict[int, Dict] = defaultdict(dict)
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing pax8 products data.")
        try:
            pax8_integration_settings: PAX8IntegrationSettings = (
                provider.pax8_integration_settings
            )
        except PAX8IntegrationSettings.DoesNotExist:
            logger.info(
                f"PAX8 interation settings not configured for the "
                f"provider: {provider.name}, ID: {provider.id}. "
                f"Skipped syncing PAX8 products for the provider."
            )
            continue
        pax8_client: PAX8APIAsyncClient = get_pax8_api_async_client(
            pax8_integration_settings.client_id,
            pax8_integration_settings.client_secret,
        )
        # Fetch all products from PAX8
        pax8_products: List[Dict] = pax8_client.get_all_pax8_products()
        pax8_products_in_db: Set[str] = set(
            PAX8Product.objects.filter(provider=provider).values_list(
                "crm_id", flat=True
            )
        )
        logger.info(
            f"Found {len(pax8_products)} products in PAX8 for provider: {provider.name}, ID: {provider.id}"
        )
        pax8_products_in_erp: Set[str] = set()
        for product in pax8_products:
            product_crm_id: str = product.get("id")
            if product_crm_id in pax8_products_in_db:
                success, errors = PAX8ProductsDataPayloadHandler(
                    provider, DbOperation.UPDATE, product
                ).process()
                if success:
                    result["updated"] += 1
                    logger.info(
                        f"Updated pax8 product with crm_id: {product_crm_id}"
                    )
                else:
                    logger.info(
                        f"Update failed for pax8 product: {product_crm_id}, "
                        f"Errors: {errors}"
                    )
                    result["updated_failed"] += 1
                    update_errors[product_crm_id].append(errors)
            else:
                success, errors = PAX8ProductsDataPayloadHandler(
                    provider, DbOperation.CREATE, product
                ).process()
                if success:
                    result["created"] += 1
                    logger.info(
                        f"Created pax8 product with crm_id: {product_crm_id}"
                    )
                else:
                    logger.info(
                        f"Create failed for pax8 product: {product_crm_id}, "
                        f"Errors: {errors}"
                    )
                    result["create_failed"] += 1
                    create_errors[product_crm_id].append(errors)
            pax8_products_in_erp.add(product_crm_id)

        pax8_products_to_delete: Set[str] = (
            pax8_products_in_db - pax8_products_in_erp
        )
        logger.info(
            f"Found {len(pax8_products_to_delete)} pax8 products to delete from DB."
        )
        if pax8_products_to_delete:
            res, errors = PAX8ProductsDataPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload=dict(
                    crm_ids_of_products_to_delete=pax8_products_to_delete
                ),
            ).process()
            if res:
                result["deleted"] += len(pax8_products_to_delete)
                logger.info(
                    f"Deleted {len(pax8_products_to_delete)} pax8 products from DB."
                )
            else:
                result["delete_failed"] += len(pax8_products_to_delete)
                logger.info("Delete Failed.")
        provider_sync_result[provider.id] = dict(
            update_errors=update_errors,
            create_errors=create_errors,
            result=result,
        )
        logger.info(
            f"PAX8 products data sync finished for provider: {provider.name}, "
            f"ID: {provider.id}."
        )
    return provider_sync_result


@shared_task
def sync_pax8_products_pricing_data(
    provider_ids: Optional[List[int]] = None,
) -> Dict[int, Dict]:
    """
    Task to sync pax8 product pricing data in DB. The pricing data is stored in model PAX8ProductPricingInfo.

    Args:
        provider_ids: List of provider IDs.

    Returns:
        Provider sync result.
    """
    logger.info("Syncing PAX8 product pricing data for providers.")
    provider_result: Dict[int, Dict] = dict()
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        logger.info(
            f"Syncing pax8 products pricing data for provider: {provider.name}, ID: {provider.id}"
        )
        result: Dict = defaultdict(int)
        update_errors: Dict = defaultdict(list)
        create_errors: Dict = defaultdict(list)
        try:
            pax8_integration_settings: PAX8IntegrationSettings = (
                provider.pax8_integration_settings
            )
        except PAX8IntegrationSettings.DoesNotExist:
            logger.info(
                f"PAX8 interation settings not configured for the "
                f"provider: {provider.name}, ID: {provider.id}. "
                f"Skipped syncing PAX8 products data for the provider."
            )
            continue
        # Fetch products pricing data for those pax8 products that exist in DB
        crm_ids_of_pax8_products_in_db: Set[str] = set(
            PAX8Product.objects.filter(provider_id=provider.id).values_list(
                "crm_id", flat=True
            )
        )
        pax8_api_client: PAX8APIAsyncClient = get_pax8_api_async_client(
            pax8_integration_settings.client_id,
            pax8_integration_settings.client_secret,
        )
        pax8_products_pricing_data: List[
            Dict
        ] = pax8_api_client.get_all_pax8_produts_pricing_data(
            list(crm_ids_of_pax8_products_in_db)
        )
        # Get CRM IDs of those products that have existing pricing data in DB
        crm_ids_of_existing_product_pricing_data: Set[str] = set(
            PAX8ProductPricingInfo.objects.filter(
                provider_id=provider.id,
                pax8_product__crm_id__in=list(crm_ids_of_pax8_products_in_db),
            ).values_list("pax8_product__crm_id", flat=True)
        )
        for product_pricing_data in pax8_products_pricing_data:
            product_crm_id: str = product_pricing_data.get("product_crm_id")
            if product_crm_id in crm_ids_of_existing_product_pricing_data:
                success, errors = PAX8ProductPricingDataPayloadHandler(
                    provider, DbOperation.UPDATE, product_pricing_data
                ).process()
                if success:
                    result["updated"] += 1
                    logger.info(
                        f"Updated pricing data for product with crm_id: {product_crm_id}"
                    )
                else:
                    logger.info(
                        f"Pricing data update failed for pax8 product: {product_crm_id}, "
                        f"Errors: {errors}"
                    )
                    result["updated_failed"] += 1
                    update_errors[product_crm_id].append(errors)
            else:
                success, errors = PAX8ProductPricingDataPayloadHandler(
                    provider, DbOperation.CREATE, product_pricing_data
                ).process()
                if success:
                    result["created"] += 1
                    logger.info(
                        f"Created pricing data for pax8 product with crm_id: {product_crm_id}"
                    )
                else:
                    logger.info(
                        f"Pricing data create failed for pax8 product: {product_crm_id}, "
                        f"Errors: {errors}"
                    )
                    result["create_failed"] += 1
                    create_errors[product_crm_id].append(errors)
        provider_result[provider.id] = dict(
            create_errors=create_errors,
            update_errors=update_errors,
            result=result,
        )
        logger.info(
            f"Finished syncing PAX8 product pricing data for provider: {provider.name}, ID: {provider.id}"
        )
    logger.info("Finished syncing PAX8 products data for providers.")
    return provider_result


@shared_task
def sync_pax8_products_and_pricing_data(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Triggers task to sync:
        1. PAX8 products and
        2. PAX8 products pricing data.

    Args:
        provider_ids: List of provider IDs.
    """
    tasks = [
        group(
            sync_pax8_products.si(provider_ids),
            sync_pax8_products_pricing_data.si(provider_ids),
        ),
    ]
    chain(*tasks).apply_async(args=(), kwargs={"queue": "high"})


@shared_task
def upload_agreement_pdf_to_cw(provider_id: int, agreement_id: int):
    """
    Task to upload agreement PDF to Connectwise.

    Args:
        provider_id: ID of Provider
        agreement_id: ID of agreement
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    agreement: Agreement = Agreement.objects.get(
        id=agreement_id, provider_id=provider_id
    )

    agreement_preview_service: AgreementTemplatePreviewService = (
        AgreementTemplatePreviewService()
    )
    agreement_pdf: Dict[
        str, str
    ] = agreement_preview_service.download_agreement_pdf(agreement)

    quote_attachments: List[Dict] = provider.erp_client.get_quote_documents(
        agreement.quote_id
    )
    for attachment in quote_attachments:
        # Agreement PDF file name ends with '- AGR'
        attachment_name: str = attachment.get("server_file_name").upper()
        agreement_file_name_suffix: str = (
            f"- {agreement_preview_service.AGREEMENT_PDF_SUFFIX}.pdf".upper()
        )
        if attachment_name.endswith(agreement_file_name_suffix):
            provider.erp_client.get_delete_documents(attachment.get("id"))

    if settings.DEBUG:
        file_path: str = agreement_pdf.get("file_path")
    else:
        file_path: str = download_file(agreement_pdf.get("file_path"))

    try:
        with open(file_path, "rb") as file:
            content_file = ContentFile(file.read())
            content_file.name = agreement_pdf.get("file_name")
            payload: Dict = dict(
                file=content_file,
                record_type="Opportunity",
                record_id=agreement.quote_id,
            )
            provider.erp_client.upload_system_attachments(payload)
    except (FileNotFoundError, OSError) as err:
        logger.error(
            f"System error reading agreement pdf. Agreement ID: {agreement.id}, "
            f"Provider ID: {provider.id}. Error: {err}"
        )
        return
    except APIError as err:
        logger.error(
            f"Error uploading agreement pdf to CW. Agreement ID: {agreement.id}, "
            f"Provider ID: {provider.id}. Error: {err}"
        )
        return
    logger.info(
        f"Successfully uploaded agreement PDF to CW. Agreement ID: {agreement.id}, Provider ID:{provider_id} "
    )


@shared_task
def upload_agreement_service_detail_document_to_cw(
    provider_id: int, agreement_id: int
) -> None:
    """
    Task to upload agreement service detail document PDF to CW.

    Args:
        provider_id: Provider ID
        agreement_id: Agreement ID

    Returns:
        None
    """
    agreement: Agreement = Agreement.objects.get(
        id=agreement_id, provider_id=provider_id
    )
    provider: Provider = agreement.provider

    agreement_preview_service: AgreementTemplatePreviewService = (
        AgreementTemplatePreviewService()
    )
    service_detail_document: Dict = (
        agreement_preview_service.generate_agreement_service_detail_pdf(
            agreement
        )
    )

    quote_attachments: List[Dict] = provider.erp_client.get_quote_documents(
        agreement.quote_id
    )
    for attachment in quote_attachments:
        # Agreement service detail PDF file name startswith 'Subscription Detail -'
        attachment_name: str = attachment.get("server_file_name").upper()
        agreement_service_detail_file_name_prefix: str = (
            f"{agreement_preview_service.SERVICE_DETAIL_PDF_PREFIX.upper()} -"
        )
        if attachment_name.startswith(
            agreement_service_detail_file_name_prefix
        ):
            provider.erp_client.get_delete_documents(attachment["id"])

    if settings.DEBUG:
        file_path: str = service_detail_document.get("file_path")
    else:
        file_path: str = download_file(
            service_detail_document.get("file_path")
        )

    try:
        with open(file_path, "rb") as file:
            content_file = ContentFile(file.read())
            content_file.name = service_detail_document.get("file_name")
            payload: Dict = dict(
                file=content_file,
                record_type="Opportunity",
                record_id=agreement.quote_id,
            )
            provider.erp_client.upload_system_attachments(payload)
    except (FileNotFoundError, OSError) as err:
        logger.error(
            f"System error reading agreement service detail document. Agreement ID: {agreement.id}, "
            f"Provider ID: {provider.id}. Error: {err}"
        )
        return
    except APIError as err:
        logger.error(
            f"Error uploading agreement service detail document to CW. Agreement ID: {agreement.id}, "
            f"Provider ID: {provider.id}. Error: {err}"
        )
        return
    logger.info(
        f"Successfully uploaded agreement service detail document to CW. Agreement ID: {agreement.id}, "
        f"Provider ID: {provider.id}"
    )
    return


@shared_task
def upload_agreement_pdf_and_service_detail_pdf_to_cw(
    provider_id: int, agreement_id: int
) -> None:
    """
    Task to upload agreement PDF and service detail pdf to CW.

    Args:
        provider_id: Provider ID
        agreement_id: Agreement ID

    Returns:
        None
    """
    upload_agreement_pdf_to_cw.apply_async(
        (provider_id, agreement_id), queue="high"
    )
    upload_agreement_service_detail_document_to_cw.apply_async(
        (provider_id, agreement_id), queue="high"
    )
    return
