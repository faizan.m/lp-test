from typing import Union, Any

from django import template
from utils import add_comma_to_number, add_comma_to_number_in_string_format

register = template.Library()


@register.filter(name="add_commas")
def add_comma(number: Union[int, float]) -> str:
    """
    Adds commas at 1000th places of a number
    E.g. If num = 1234567, returns: 1,234,567
    If num = 1234567.123, returns: 1,234,567.123
    """
    number: str = add_comma_to_number(number)
    return number


@register.filter(name="currency")
def add_comma_and_currency_symbol(
    number: Union[int, float], currency: str = "$"
) -> Union[str, Any]:
    """
    Adds commas at 1000th places in a number
    E.g. If num = 1234567, returns: $1,234,567.00
    If num = 1234567.123, returns: $1,234,567.12
    """
    if not isinstance(number, (int, float)):
        return number
    # Sets precision to 2 decimal places
    number_string: str = "{0:.2f}".format(number)
    number_with_commas: str = add_comma_to_number_in_string_format(
        number_string
    )
    return f"{currency}{number_with_commas}"
