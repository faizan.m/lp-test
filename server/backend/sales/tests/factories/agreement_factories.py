import factory
from faker import Factory
from sales.models import Agreement, AgreementTemplate, AgreementMarginSettings
from accounts.tests.factories import (
    ProviderFactory,
    CustomerFactory,
    ProviderUserFactory,
)
import datetime
from sales.tests.utils import (
    get_sample_products_data,
    get_sample_sections_data,
    get_mock_pax8_products_data_for_model,
)

faker = Factory.create()


class AgreementTemplateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AgreementTemplate

    provider = factory.SubFactory(ProviderFactory)
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    author = factory.SubFactory(ProviderUserFactory)
    updated_by = factory.SubFactory(ProviderUserFactory)
    name = faker.name()
    is_disabled = False
    major_version = 1
    minor_version = 0
    min_discount = faker.pyint()
    max_discount = faker.pyint()
    version_description = faker.text()
    is_current_version = True
    root_template = None
    parent_template = None
    sections = get_sample_sections_data()
    products = get_sample_products_data()
    pax8_products = factory.LazyFunction(get_mock_pax8_products_data_for_model)


class AgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Agreement

    provider = factory.SubFactory(ProviderFactory)
    customer = factory.SubFactory(CustomerFactory)
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    name = faker.name()
    author = factory.SubFactory(ProviderUserFactory)
    user = factory.SubFactory(ProviderUserFactory)
    updated_by = factory.SubFactory(ProviderUserFactory)
    template = factory.SubFactory(AgreementTemplateFactory)
    quote_id = faker.pyint()
    customer_cost = faker.pydecimal(
        left_digits=2, right_digits=2, positive=True
    )
    margin = faker.pydecimal(left_digits=2, right_digits=2, positive=True)
    margin_percentage = faker.pydecimal(
        left_digits=2, right_digits=2, positive=True
    )
    internal_cost = faker.pydecimal(
        left_digits=2, right_digits=2, positive=True
    )
    is_disabled = False
    major_version = 1
    minor_version = 0
    version_description = faker.text()
    is_current_version = True
    root_agreement = None
    parent_agreement = None
    sections = get_sample_sections_data()
    products = get_sample_products_data()
    discount_percentage = faker.pyint()
    total_discount = faker.pydecimal(
        left_digits=2, right_digits=2, positive=True
    )
    pax8_products = factory.LazyFunction(get_mock_pax8_products_data_for_model)


class AgreementMarginSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AgreementMarginSettings

    margin_limit = faker.pyint()
    provider = factory.SubFactory(ProviderFactory)
