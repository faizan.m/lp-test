from sales.models import (
    AutomatedCollectionNoticesSettings,
    PAX8IntegrationSettings,
    PAX8Product,
    PAX8ProductPricingInfo,
)
import factory
from faker import Faker
from accounts.tests.factories import ProviderFactory
from typing import List, Dict, Union, Optional
from factory.fuzzy import FuzzyChoice
import factory
from ..utils import (
    get_mock_send_to_emails_list,
    get_mock_weekly_send_schedule,
    get_mock_billing_statuses,
    get_mock_vendor_names,
    get_mock_uuid_value,
)
import uuid

fake = Faker()


class AutomatedCollectionNoticesSettingsFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = AutomatedCollectionNoticesSettings
        django_get_or_create = ("provider", "send_from_email")

    provider = factory.SubFactory(ProviderFactory)
    weekly_send_schedule = factory.LazyFunction(get_mock_weekly_send_schedule)
    send_to_email = factory.LazyFunction(get_mock_send_to_emails_list)
    send_from_email = fake.email()
    invoice_exclude_patterns = ["*PAT", "PAT*"]
    billing_statuses = factory.LazyFunction(get_mock_billing_statuses)
    approaching_due_date_ageing = fake.pyint()
    email_body_template = fake.text(max_nb_chars=10)
    subject = fake.text(max_nb_chars=10)
    contact_type_crm_id = fake.pyint()


class PAX8IntegrationSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PAX8IntegrationSettings
        django_get_or_create = ("provider",)

    provider = factory.SubFactory(ProviderFactory)
    client_id = str(uuid.uuid4())
    client_secret = str(uuid.uuid4())
    vendors = factory.LazyFunction(get_mock_vendor_names)


class PAX8ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PAX8Product
        django_get_or_create = ("provider", "crm_id")

    provider = factory.SubFactory(ProviderFactory)
    crm_id = factory.LazyFunction(get_mock_uuid_value)
    name = fake.sentence(nb_words=10)
    vendor_name = fake.company()
    short_description = fake.bs()
    sku = fake.bban()
    vendor_sku = None
    alt_vendor_sku = None


class PAX8ProductPricingInfoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PAX8ProductPricingInfo
        django_get_or_create = ("provider", "pax8_product")

    provider = factory.SubFactory(ProviderFactory)
    pax8_product = factory.SubFactory(PAX8ProductFactory)
    pricing_data = list()
