from datetime import date
from typing import Dict, List
from unittest.mock import patch

import mock
from django.forms import model_to_dict
from django.test import TestCase
from faker import Faker
from rest_framework.exceptions import ValidationError

from accounts.models import Customer
from accounts.tests.factories import (
    CustomerFactory,
    ProviderUserFactory,
    ProfileFactory,
    ProviderFactory,
)
from sales.models import Agreement, AgreementTemplate
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
    AgreementService,
)
from sales.services.pax8_calculations import PAX8BillingTerms
from sales.tests.factories import AgreementTemplateFactory, AgreementFactory
from sales.tests.utils import (
    get_valid_request_payload,
    get_mock_uuid_value,
)
from test_utils import BaseProviderTestCase

MOCK_FILE_PATH: str = "www.mockfilestorage.com/mock_file_name"
MOCK_FILE_NAME: str = "sample preview.pdf"


class TestAgreementTemplatePreviewService(TestCase):
    """
    Test methods of AgreementTemplatePreviewService class
    """

    service_class = AgreementTemplatePreviewService()

    @classmethod
    def setUpTestData(cls):
        pass

    def test_add_extra_info_to_template_context(self):
        """
        Test the method: add_extra_info_to_template_context()
        """
        context: Dict = dict()
        current_date = date.today().strftime("%m/%d/%Y")
        self.service_class.add_extra_info_to_template_context(context)
        self.assertEqual(context.get("current_date"), current_date)

    def test_get_agreement_context_from_model_instance(self):
        """
        Test the method: get_agreement_context_from_model_instance()
        """
        provider: Provider = ProviderFactory.create()
        user: User = ProviderUserFactory.create(provider=provider)
        profile: Profile = ProfileFactory.create(user=user)
        agreement: Agreement = AgreementFactory.create(
            user=user, provider=provider
        )
        agreement_context: Dict = dict(
            name=agreement.name,
            sections=agreement.sections,
            products=agreement.products,
            version_description=agreement.version_description,
            author_name=agreement.author.name,
            version=agreement.version,
            company_name=agreement.customer.name,
            account_manager_name=agreement.customer.account_manager_name,
            customer_contact=agreement.user.name,
            customer_phone=agreement.user.profile.full_office_phone_number,
            discount_percentage=agreement.discount_percentage,
            pax8_products=agreement.pax8_products,
            updated_on=agreement.updated_on.strftime("%Y.%m.%d"),
        )
        context: Dict = (
            self.service_class.get_agreement_context_from_model_instance(
                agreement
            )
        )
        self.assertDictEqual(agreement_context, context)

    def test_get_context_from_validated_data(self):
        """
        Test the method: get_context_from_validated_data()
        """
        user = ProviderUserFactory.create()
        profile = ProfileFactory.create(user=user)
        customer: Customer = CustomerFactory.build()
        validated_data: Dict = dict(
            customer=customer, minor_version=0, major_version=1, user=user
        )
        expected_output: Dict = dict(
            company_name=customer.name,
            account_manager_name=customer.account_manager_name,
            customer_contact=user.name,
            customer_phone=user.profile.full_office_phone_number,
            version="1.0",
        )
        context: Dict = self.service_class.get_context_from_validated_data(
            validated_data
        )
        self.assertDictEqual(expected_output, context)

    def test_get_header_for_pdf_preview(self):
        """
        Test the method: get_header_for_pdf_preview()
        """
        header = self.service_class.get_header_for_pdf_preview({})
        self.assertEqual(len(header.get("source")) > 0, True)

    def test_get_footer_for_pdf_preview(self):
        """
        Test the method: get_footer_for_pdf_preview()
        """
        footer = self.service_class.get_footer_for_pdf_preview({})
        self.assertEqual(len(footer.get("source")) > 0, True)

    def test_get_template_payload_from_model_instance(self):
        """
        Test the method: get_template_payload_from_model_instance()
        """
        template: AgreementTemplate = AgreementTemplateFactory()
        expected_payload: Dict = dict(
            name=template.name,
            sections=template.sections,
            products=template.products,
            version_description=template.version_description,
            author_name=template.author.name,
            version=template.version,
            pax8_products=template.pax8_products,
        )
        template_payload: Dict = (
            self.service_class.get_template_payload_from_model_instance(
                template
            )
        )
        self.assertDictEqual(expected_payload, template_payload)

    def test_get_rendered_html_str(self):
        """
        Test the method: get_rendered_html_str()
        """
        html = self.service_class.get_rendered_html_str(
            self.service_class.TEMPLATE_DIR,
            self.service_class.TEMPLATE_PDF,
            get_valid_request_payload(),
        )
        self.assertEqual(len(html) > 0, True)

    def test_parse_special_variables_in_agreement_sections(self):
        """
        Test the method: parse_special_variables_in_agreement_sections
        """
        mock_context: Dict = dict(
            sections=[
                dict(
                    name="Section Name",
                    content="Section markdown content",
                    id=1,
                    is_locked=False,
                    value_html='<span class="ql-mention-denotation-char">@</span>{Customer Name}</span>'
                    '<span class="ql-mention-denotation-char">@</span>{Page Break}</span>',
                )
            ],
            company_name="mockCustomerInc.",
        )
        updated_context: Dict = dict(
            sections=[
                dict(
                    name="Section Name",
                    content="Section markdown content",
                    id=1,
                    is_locked=False,
                    value_html='<span class="ql-mention-denotation-char"></span>mockCustomerInc.</span>'
                    '<span class="ql-mention-denotation-char"></span><div style="page-break-after:always;"></div></span>',
                )
            ],
            company_name="mockCustomerInc.",
        )
        self.service_class.parse_special_variables_in_agreement_sections(
            mock_context
        )
        self.assertEqual(updated_context, mock_context)

    def test_parse_special_variables_in_template_sections(self):
        """
        Test the method: parse_special_variables_in_template_sections
        """
        mock_context: Dict = dict(
            sections=[
                dict(
                    name="Section Name",
                    content="Section markdown content",
                    id=1,
                    is_locked=False,
                    value_html='<span class="ql-mention-denotation-char">@</span>{Page Break}</span>',
                )
            ],
        )
        updated_context: Dict = dict(
            sections=[
                dict(
                    name="Section Name",
                    content="Section markdown content",
                    id=1,
                    is_locked=False,
                    value_html='<span class="ql-mention-denotation-char"></span><div style="page-break-after:always;"></div></span>',
                )
            ],
        )
        self.service_class.parse_special_variables_in_agreement_sections(
            mock_context
        )
        self.assertEqual(updated_context, mock_context)

    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "generate_agreement_pdf_preview"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_agreement_context_from_model_instance"
    )
    def test_download_agreement_pdf(
        self,
        get_agreement_context_from_model_instance,
        generate_agreement_pdf_preview,
    ):
        agreement: Agreement = AgreementFactory.create()
        get_agreement_context_from_model_instance.return_value = dict()
        agreement_pdf_download_details: Dict = dict(
            file_name="File Name", file_path="File path"
        )
        generate_agreement_pdf_preview.return_value = (
            agreement_pdf_download_details
        )
        result: Dict = self.service_class.download_agreement_pdf(agreement)
        self.assertEqual(result, agreement_pdf_download_details)
        get_agreement_context_from_model_instance.assert_called_with(agreement)
        generate_agreement_pdf_preview.assert_called_with({})

    def test__get_products_summary_by_billing_frequency(self):
        fake = Faker()
        products: List[Dict] = [
            dict(
                billing_option=Agreement.PER_MONTH,
                name=fake.name(),
                quantity=fake.pyint(),
                customer_cost=fake.pyfloat(),
                extended_cost=50,
                description=fake.text(max_nb_chars=5),
                is_comment=False,
            ),
            dict(
                billing_option=Agreement.ONE_TIME,
                name=fake.name(),
                quantity=fake.pyint(),
                customer_cost=fake.pyfloat(),
                extended_cost=100,
                description=fake.text(max_nb_chars=5),
                is_comment=False,
            ),
            dict(
                billing_option=Agreement.PER_ANNUM,
                name=fake.name(),
                quantity=fake.pyint(),
                customer_cost=fake.pyfloat(),
                extended_cost=150,
                description=fake.text(max_nb_chars=5),
                is_comment=False,
            ),
        ]
        summary: Dict = (
            self.service_class._get_products_summary_by_billing_frequency(
                products
            )
        )
        self.assertEqual(50, summary.get("total_monthly_cost"))
        self.assertEqual(100, summary.get("total_one_time_cost"))
        self.assertEqual(150, summary.get("total_annual_cost"))
        self.assertEqual(850, summary.get("estimated_annual_cost"))

    def test___get_preview_data_for_product_with_zero_quantity(self):
        fake = Faker()
        product: Dict = dict(
            name=fake.name(),
            quantity=fake.pyint(),
            customer_cost=fake.pyfloat(),
            internal_cost=fake.pyfloat(),
            description=fake.text(max_nb_chars=5),
            is_comment=False,
        )
        billing_option: str = Agreement.PER_ANNUM
        expected_response: Dict = dict(
            billing_option=billing_option,
            name=product.get("name"),
            quantity=product.get("quantity"),
            customer_cost=round(product.get("customer_cost")),
            extended_cost=0,
            description=product.get("description"),
            is_comment=False,
        )
        result: Dict = self.service_class._get_preview_data_for_product_with_zero_quantity(
            product, billing_option
        )
        self.assertEqual(expected_response, result)

    @patch("sales.services.agreement_template_service.Products")
    def test__get_preview_data_for_valid_product(self, mock_Products):
        fake = Faker()
        discount_percent: int = 40
        product: Dict = dict(
            products=[
                dict(
                    name="ABC",
                    margin=fake.pyint(),
                    description="LOL",
                    customer_cost=fake.pyint(),
                    internal_cost=fake.pyint(),
                    quantity=10,
                )
            ],
            is_bundle=True,
            product_type=Agreement.SERVICE,
            billing_option=Agreement.PER_ANNUM,
            id=fake.pyint(),
            is_checked=fake.pybool(),
            selected_product=0,
            is_required=fake.pybool(),
            comment=None,
        )
        instance = mock_Products.return_value
        mock_product_instance: Product = mock.MagicMock()
        mock_product_instance.name = "ABC"
        mock_product_instance.quantity = 10
        mock_product_instance.original_customer_cost_per_unit = 100
        mock_product_instance.extended_cost = 1200
        mock_product_instance.description = "LOL"
        instance.products = [mock_product_instance]
        expected_result: Dict = dict(
            billing_option=Agreement.PER_ANNUM,
            name="ABC",
            quantity=10,
            customer_cost=100,
            extended_cost=1200,
            description="LOL",
            is_comment=False,
        )
        result: Dict = self.service_class._get_preview_data_for_valid_product(
            product, discount_percent
        )
        self.assertEqual(expected_result, result)

    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "_get_pax8_product_calculation_data_for_agreement_preview"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "_get_product_calculation_data_for_agreement_preview"
    )
    def test_add_product_data_to_agreement_render_context(
        self,
        _get_product_calculation_data_for_agreement_preview,
        _get_pax8_product_calculation_data_for_agreement_preview,
    ):
        fake = Faker()
        context: Dict[str, Any] = dict(
            pax8_products=dict(
                products=[
                    dict(
                        product_crm_id=get_mock_uuid_value(),
                        product_name=fake.bs(),
                    )
                ]
            ),
            products=[
                dict(
                    products=[],
                    is_bundle=fake.pybool(),
                    product_type=Agreement.COMMENT,
                    billing_option=Agreement.NA,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=None,
                    is_required=fake.pybool(),
                    comment="This is comment",
                )
            ],
        )
        _get_product_calculation_data_for_agreement_preview.return_value = (
            dict(estimated_annual_cost=100)
        )
        _get_pax8_product_calculation_data_for_agreement_preview.return_value = dict(
            billing_summary=dict(estimated_pax8_annual_cost=150)
        )
        self.service_class.add_product_data_to_agreement_render_context(
            context
        )
        self.assertEqual(250, context.get("total_agreement_extended_cost"))
        _get_product_calculation_data_for_agreement_preview.assert_called()
        _get_pax8_product_calculation_data_for_agreement_preview.assert_called()

    @patch(
        "sales.services.agreement_template_service."
        "generate_pdf_using_pdf_shift",
        return_value=dict(file_path="file_path_url", file_name="file_name"),
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_footer_for_pdf_preview"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_header_for_pdf_preview"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_rendered_html_str"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_agreement_pdf_file_name"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "parse_special_variables_in_agreement_sections"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "add_extra_info_to_template_context"
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "add_product_data_to_agreement_render_context"
    )
    def test_generate_agreement_pdf_preview(
        self,
        add_product_data_to_agreement_render_context,
        add_extra_info_to_template_context,
        parse_special_variables_in_agreement_sections,
        get_agreement_pdf_file_name,
        get_rendered_html_str,
        get_header_for_pdf_preview,
        get_footer_for_pdf_preview,
        generate_pdf_using_pdf_shift,
    ):
        agreement: Agreement = AgreementFactory.create()
        context: Dict[str, Any] = model_to_dict(agreement)
        result: Dict[
            str, str
        ] = self.service_class.generate_agreement_pdf_preview(context)
        self.assertEqual("file_path_url", result.get("file_path"))
        add_product_data_to_agreement_render_context.assert_called()
        add_extra_info_to_template_context.assert_called()
        parse_special_variables_in_agreement_sections.assert_called()
        get_agreement_pdf_file_name.assert_called()
        get_rendered_html_str.assert_called()
        get_header_for_pdf_preview.assert_called()
        get_footer_for_pdf_preview.assert_called()
        generate_pdf_using_pdf_shift.assert_called()

    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "_get_pax8_products_calculation_data_for_preview",
        return_value=dict(),
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "_get_products_calculation_data_for_agreement_template_preview",
        return_value=dict(),
    )
    def test_add_products_data_to_agreement_template_render_context(
        self,
        _get_products_calculation_data_for_agreement_template_preview,
        _get_pax8_products_calculation_data_for_preview,
    ):
        context: Dict = dict(products=dict(a=1), pax8_products=dict(b=2))
        self.service_class.add_products_data_to_agreement_template_render_context(
            context
        )
        self.assertIn("products_preview_data", context.keys())
        self.assertIn("pax8_products_preview_data", context.keys())
        _get_products_calculation_data_for_agreement_template_preview.assert_called_with(
            dict(a=1)
        )
        _get_pax8_products_calculation_data_for_preview.assert_called_with(
            dict(b=2)
        )

    def test__get_products_calculation_data_for_agreement_template_preview(
        self,
    ):
        fake = Faker()
        products: List[Dict] = [
            dict(
                products=[
                    dict(
                        name="A",
                        margin=20,
                        description="AAA",
                        customer_cost=1000,
                        internal_cost=400,
                        quantity=10,
                    )
                ],
                is_bundle=True,
                product_type=Agreement.SERVICE,
                billing_option=Agreement.PER_ANNUM,
                id=fake.pyint(),
                is_checked=fake.pybool(),
                selected_product=0,
                is_required=fake.pybool(),
                comment=None,
            ),
            dict(
                products=[
                    dict(
                        name="B",
                        margin=30,
                        description="BBB",
                        customer_cost=2000,
                        internal_cost=800,
                        quantity=5,
                    )
                ],
                is_bundle=True,
                product_type=Agreement.SERVICE,
                billing_option=Agreement.ONE_TIME,
                id=fake.pyint(),
                is_checked=fake.pybool(),
                selected_product=0,
                is_required=fake.pybool(),
                comment=None,
            ),
            dict(
                products=[
                    dict(
                        name="C",
                        margin=40,
                        description="CCC",
                        customer_cost=1500,
                        internal_cost=700,
                        quantity=15,
                    )
                ],
                is_bundle=True,
                product_type=Agreement.SERVICE,
                billing_option=Agreement.PER_MONTH,
                id=fake.pyint(),
                is_checked=fake.pybool(),
                selected_product=0,
                is_required=fake.pybool(),
                comment=None,
            ),
        ]
        product_preview_data: Dict = dict(
            billing_option=Agreement.PER_ANNUM,
            products_list=[
                dict(
                    name="A",
                    quantity=10,
                    customer_cost=1000,
                    extended_cost=120000,
                    description="AAA",
                )
            ],
        )
        result: Dict = self.service_class._get_products_calculation_data_for_agreement_template_preview(
            products
        )
        self.assertEqual(10000, result.get("total_one_time_cost"))
        self.assertEqual(22500, result.get("total_monthly_cost"))
        self.assertEqual(120000, result.get("total_annual_cost"))
        self.assertDictEqual(product_preview_data, result.get("products")[0])

    def test__get_pax8_products_calculation_data_for_preview(self):
        fake = Faker()
        pax8_products: Dict[str, Any] = dict(
            name="Prods",
            products=[
                dict(
                    billing_term="Term A",
                    margin=20,
                    partner_buy_rate=55.40,
                    product_name="A",
                    quantity=10,
                    sku="AA",
                    vendor_name="Vendor A",
                    short_description="Description A",
                    commitment_term="Commit A",
                ),
                dict(
                    billing_term=fake.bs(),
                    margin=30,
                    partner_buy_rate=25.75,
                    product_name="B",
                    quantity=15,
                    sku="BB",
                    vendor_name=fake.company(),
                    short_description=fake.bs(),
                    commitment_term=fake.name(),
                ),
            ],
        )
        expected_pax8_product_preview_data: Dict = dict(
            product_name="A",
            sku="AA",
            short_description="Description A",
            commitment_term="Commit A",
            billing_term="Term A",
            extended_cost=692.5,
            quantity=10,
            customer_cost=69.25,
        )
        result: Dict = (
            self.service_class._get_pax8_products_calculation_data_for_preview(
                pax8_products
            )
        )
        self.assertEqual(1244.35, result.get("total_pax8_extended_cost"))
        self.assertEqual(
            expected_pax8_product_preview_data,
            result.get("pax8_products_list")[0],
        )

    def test__get_product_calculation_data_for_agreement_preview(self):
        fake = Faker()
        context: Dict = dict(
            discount_percentage=35,
            products=[
                dict(
                    products=[],
                    is_bundle=True,
                    product_type=Agreement.COMMENT,
                    billing_option=Agreement.NA,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=None,
                    is_required=fake.pybool(),
                    comment="Woah!",
                ),
                dict(
                    products=[
                        dict(
                            name="A",
                            margin=20,
                            description="AAA",
                            customer_cost=1000,
                            internal_cost=400,
                            quantity=0,
                        )
                    ],
                    is_bundle=True,
                    product_type=Agreement.SERVICE,
                    billing_option=Agreement.PER_MONTH,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=0,
                    is_required=fake.pybool(),
                    comment=None,
                ),
                dict(
                    products=[
                        dict(
                            name="A",
                            margin=20,
                            description="AAA",
                            customer_cost=1000,
                            internal_cost=400,
                            quantity=10,
                        )
                    ],
                    is_bundle=True,
                    product_type=Agreement.SERVICE,
                    billing_option=Agreement.PER_ANNUM,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=0,
                    is_required=fake.pybool(),
                    comment=None,
                ),
                dict(
                    products=[
                        dict(
                            name="B",
                            margin=30,
                            description="BBB",
                            customer_cost=2000,
                            internal_cost=800,
                            quantity=5,
                        )
                    ],
                    is_bundle=True,
                    product_type=Agreement.SERVICE,
                    billing_option=Agreement.ONE_TIME,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=0,
                    is_required=fake.pybool(),
                    comment=None,
                ),
                dict(
                    products=[
                        dict(
                            name="C",
                            margin=40,
                            description="CCC",
                            customer_cost=1500,
                            internal_cost=700,
                            quantity=15,
                        )
                    ],
                    is_bundle=True,
                    product_type=Agreement.SERVICE,
                    billing_option=Agreement.PER_MONTH,
                    id=fake.pyint(),
                    is_checked=fake.pybool(),
                    selected_product=0,
                    is_required=fake.pybool(),
                    comment=None,
                ),
            ],
        )
        result: Dict = self.service_class._get_product_calculation_data_for_agreement_preview(
            context
        )
        self.assertEqual(10000, result.get("total_one_time_cost"))
        self.assertEqual(18300, result.get("total_monthly_cost"))
        self.assertEqual(94800, result.get("total_annual_cost"))
        self.assertEqual(324400, result.get("estimated_annual_cost"))
        # Extended cost for comment and product with zero quantity should be 0
        self.assertEqual(0, result.get("products")[0].get("extended_cost"))
        self.assertEqual(0, result.get("products")[1].get("extended_cost"))

    @patch(
        "sales.services.agreement_template_service."
        "generate_pdf_using_pdf_shift",
        return_value=dict(file_path="url", file_name="file_name.pdf"),
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_footer_for_pdf_preview",
        return_value="<p>Footer HTML!</p>",
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_header_for_pdf_preview",
        return_value="<p>Header HTML!</p>",
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "get_rendered_html_str",
        return_value="<p>Rendered HTML!</p>",
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "parse_special_variables_in_template_sections",
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "add_extra_info_to_template_context",
    )
    @patch(
        "sales.services.agreement_template_service.AgreementTemplatePreviewService."
        "add_products_data_to_agreement_template_render_context",
    )
    def test_generate_agreement_template_pdf_preview(
        self,
        add_products_data_to_agreement_template_render_context,
        add_extra_info_to_template_context,
        parse_special_variables_in_template_sections,
        get_rendered_html_str,
        get_header_for_pdf_preview,
        get_footer_for_pdf_preview,
        generate_pdf_using_pdf_shift,
    ):
        agreement: Agreement = AgreementFactory.create()
        context: Dict = model_to_dict(agreement)
        result: Dict = (
            self.service_class.generate_agreement_template_pdf_preview(context)
        )
        self.assertEqual("url", result.get("file_path"))
        add_products_data_to_agreement_template_render_context.assert_called()
        add_extra_info_to_template_context.assert_called()
        parse_special_variables_in_template_sections.assert_called()
        get_rendered_html_str.assert_called()
        get_header_for_pdf_preview.assert_called()
        get_footer_for_pdf_preview.assert_called()
        generate_pdf_using_pdf_shift.assert_called()

    def test__get_pax8_products_billing_summary(self):
        fake = Faker()
        pax8_products_for_preview: List[Dict] = [
            dict(
                is_comment=True,
                comment="This is a comment",
            ),
            dict(
                is_comment=False,
                comment=None,
                product_name=fake.name(),
                sku=fake.text(max_nb_chars=5),
                short_description=fake.text(max_nb_chars=5),
                commitment_term=fake.text(max_nb_chars=5),
                billing_term=PAX8BillingTerms.MONTHLY,
                extended_cost=100,
                quantity=fake.pyint(),
                customer_cost=fake.pyint(),
                annual_extended_cost=1200,
            ),
            dict(
                is_comment=False,
                comment=None,
                product_name=fake.name(),
                sku=fake.text(max_nb_chars=5),
                short_description=fake.text(max_nb_chars=5),
                commitment_term=fake.text(max_nb_chars=5),
                billing_term=PAX8BillingTerms.ONE_TIME,
                extended_cost=200,
                quantity=fake.pyint(),
                customer_cost=fake.pyint(),
                annual_extended_cost=200,
            ),
        ]
        expected_result = dict(
            extended_cost_by_billing_term={"Monthly": 100, "One-Time": 200},
            estimated_pax8_annual_cost=1400,
        )
        billing_summary: Dict = (
            self.service_class._get_pax8_products_billing_summary(
                pax8_products_for_preview
            )
        )
        self.assertEqual(expected_result, billing_summary)

    def test__get_pax8_product_calculation_data_for_agreement_preview(self):
        pax8_products: Dict = dict(
            name="Meow",
            products=[
                dict(
                    billing_term="Monthly",
                    margin=30,
                    partner_buy_rate=35.60,
                    product_name="A",
                    quantity=7,
                    sku="ABCD1",
                    vendor_name="Vendor 1",
                    short_description="Description",
                    commitment_term="Commitment",
                    is_comment=False,
                    comment=None,
                ),
                dict(
                    billing_term="One-Time",
                    margin=45,
                    partner_buy_rate=45.90,
                    product_name="B",
                    quantity=15,
                    sku="ABCD2",
                    vendor_name="Vendor 2",
                    short_description="Description",
                    commitment_term="Commitment",
                    is_comment=False,
                    comment=None,
                ),
                dict(
                    billing_term=None,
                    margin=None,
                    partner_buy_rate=None,
                    product_name=None,
                    quantity=None,
                    sku=None,
                    vendor_name=None,
                    short_description=None,
                    commitment_term=None,
                    is_comment=True,
                    comment="This is comment!",
                ),
            ],
        )
        result: Dict = self.service_class._get_pax8_product_calculation_data_for_agreement_preview(
            pax8_products
        )
        self.assertEqual(True, True)
        self.assertEqual("Meow", result.get("name"))
        self.assertEqual(
            356.02,
            result.get("billing_summary")
            .get("extended_cost_by_billing_term")
            .get("Monthly"),
        )
        self.assertEqual(
            1251.75,
            result.get("billing_summary")
            .get("extended_cost_by_billing_term")
            .get("One-Time"),
        )
        self.assertEqual(
            5523.99,
            result.get("billing_summary").get("estimated_pax8_annual_cost"),
        )


class TestAgreementService(BaseProviderTestCase):
    """
    Test AgreementService class
    """

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.test_class: AgreementService = AgreementService(cls.provider)
        cls.fake = Faker()

    @patch(
        "sales.services.agreement_template_service.QuoteService.create_forecast_for_agreement_products"
    )
    def test__create_forecast_for_products__when_no_forecast_creation_error(
        self, create_forecast_for_agreement_products
    ):
        quote_id: int = self.fake.pyint()
        pax8_products: List[Dict] = [
            dict(
                customer_cost=self.fake.pyint(),
                internal_cost=self.fake.pyint(),
                qunatity=self.fake.pyint(),
                description=self.fake.word(),
            )
        ]
        products: List = list()
        create_forecast_for_agreement_products.return_value = dict(id=100)
        result: Dict = self.test_class.create_forecast_for_products(
            quote_id, pax8_products, products
        )
        expected_result: Dict = dict(
            forecast_id=100,
            forecast_error_details=dict(
                error_in_forecast_creation=False,
                error_message=None,
            ),
        )
        self.assertEqual(result, expected_result)
        create_forecast_for_agreement_products.assert_called_with(
            self.provider, quote_id, pax8_products, products
        )

    @patch(
        "sales.services.agreement_template_service.QuoteService.create_forecast_for_agreement_products"
    )
    def test__create_forecast_for_products__when_forecast_creation_error(
        self, create_forecast_for_agreement_products
    ):
        quote_id: int = self.fake.pyint()
        pax8_products: List[Dict] = [
            dict(
                customer_cost=self.fake.pyint(),
                internal_cost=self.fake.pyint(),
                quantity=self.fake.pyint(),
                description=self.fake.word(),
            )
        ]
        products: List[Dict] = get_valid_request_payload().get("products")
        error_message: str = "Error creating forecast!"
        create_forecast_for_agreement_products.side_effect = [
            ValidationError(error_message)
        ]
        result: Dict = self.test_class.create_forecast_for_products(
            quote_id, pax8_products, products
        )
        self.assertEqual(result.get("forecast_id"), None)
        self.assertEqual(
            result.get("forecast_error_details").get(
                "error_in_forecast_creation"
            ),
            True,
        )

    @patch(
        "sales.services.agreement_template_service.AgreementService.create_forecast_for_products"
    )
    def test_create_agreement(self, create_forecast_for_products):
        create_forecast_for_products.return_value = dict(
            forecast_id=200,
            forecast_error_details=dict(
                error_in_forecast_creation=False, error_message=False
            ),
        )
        agreement: Agreement = AgreementFactory.build(provider=self.provider)
        template: AgreementTemplate = AgreementTemplateFactory.create(
            provider=self.provider
        )
        customer: Customer = CustomerFactory(provider=self.provider)
        user: User = self.user
        create_payload: Dict = dict(
            name=agreement.name,
            template=template,
            customer=customer,
            user=self.user,
            quote_id=100,
            sections=agreement.sections,
            products=agreement.products,
            min_discount=agreement.min_discount,
            max_discount=agreement.max_discount,
            version_description=agreement.version_description,
            customer_cost=agreement.customer_cost,
            margin=agreement.margin,
            margin_percentage=agreement.margin_percentage,
            internal_cost=agreement.internal_cost,
            discount_percentage=agreement.discount_percentage,
            total_discount=agreement.total_discount,
        )
        result: Agreement = self.test_class.create_agreement(
            user, create_payload=create_payload
        )
        self.assertEqual(result.name, create_payload.get("name"))
        self.assertEqual(result.version, "1.0")
        self.assertEqual(result.root_agreement_id, None)
        self.assertEqual(result.parent_agreement_id, None)
        self.assertEqual(result.is_current_version, True)

    def test__get_agreement_field_values_as_per_scenario__for_rollback(self):
        current_agreement: Agreement = AgreementFactory.build(
            provider=self.provider, major_version=2, minor_version=1
        )
        query_params: Dict = dict(rollback="true")
        result: Dict = (
            self.test_class._get_agreement_field_values_as_per_scenario(
                current_agreement, self.user, query_params
            )
        )
        expected_result: Dict = dict(
            author=current_agreement.author,
            major_version=3,
            minor_version=0,
        )
        self.assertEqual(expected_result, result)

    def test__get_agreement_field_values_as_per_scenario__when_updating_major_version(
        self,
    ):
        current_agreement: Agreement = AgreementFactory.build(
            provider=self.provider, major_version=2, minor_version=1
        )
        query_params: Dict = dict(update_major_version="true")
        result: Dict = (
            self.test_class._get_agreement_field_values_as_per_scenario(
                current_agreement, self.user, query_params
            )
        )
        expected_result: Dict = dict(
            author=current_agreement.author,
            major_version=3,
            minor_version=0,
        )
        self.assertEqual(expected_result, result)

    def test__get_agreement_field_values_as_per_scenario__when_setting_user_as_author(
        self,
    ):
        current_agreement: Agreement = AgreementFactory.build(
            provider=self.provider, major_version=2, minor_version=1
        )
        query_params: Dict = dict(set_user_as_author="true")
        result: Dict = (
            self.test_class._get_agreement_field_values_as_per_scenario(
                current_agreement, self.user, query_params
            )
        )
        expected_result: Dict = dict(
            author=self.user,
            major_version=2,
            minor_version=2,
        )
        self.assertEqual(expected_result, result)

    def test__get_agreement_field_values_as_per_scenario__for_normal_update(
        self,
    ):
        current_agreement: Agreement = AgreementFactory.build(
            provider=self.provider, major_version=2, minor_version=1
        )
        result: Dict = (
            self.test_class._get_agreement_field_values_as_per_scenario(
                current_agreement, self.user, dict()
            )
        )
        expected_result: Dict = dict(
            author=current_agreement.author,
            major_version=2,
            minor_version=2,
        )
        self.assertEqual(expected_result, result)

    @patch(
        "sales.services.agreement_template_service.AgreementService.create_forecast_for_products"
    )
    def test_update_agreement(self, create_forecast_for_products):
        create_forecast_for_products.return_value = dict(
            forecast_id=200,
            forecast_error_details=dict(
                error_in_forecast_creation=False, error_message=False
            ),
        )
        current_version_agreement: Agreement = AgreementFactory.create(
            provider=self.provider, major_version=2, minor_version=3
        )
        agreement: Agreement = AgreementFactory.build()
        update_payload: Dict = dict(
            template=current_version_agreement.template,
            customer=current_version_agreement.customer,
            user=current_version_agreement.user,
            name=agreement.name,
            quote_id=100,
            sections=agreement.sections,
            products=agreement.products,
            min_discount=agreement.min_discount,
            max_discount=agreement.max_discount,
            version_description=agreement.version_description,
            customer_cost=agreement.customer_cost,
            margin=agreement.margin,
            margin_percentage=agreement.margin_percentage,
            internal_cost=agreement.internal_cost,
            discount_percentage=agreement.discount_percentage,
            total_discount=agreement.total_discount,
        )
        result: Agreement = self.test_class.update_agreement(
            current_version_agreement, self.user, update_payload, dict()
        )
        self.assertEqual(current_version_agreement.is_current_version, False)
        self.assertEqual(result.name, update_payload.get("name"))
        self.assertEqual(result.is_current_version, True)
        self.assertEqual(result.version, "2.4")
        self.assertEqual(
            result.parent_agreement_id, current_version_agreement.id
        )
        self.assertEqual(
            result.root_agreement_id, current_version_agreement.id
        )
