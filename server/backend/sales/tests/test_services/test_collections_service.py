import copy
from datetime import datetime, timedelta
from typing import Dict, List
from unittest import mock
from unittest.mock import patch

from factory.faker import faker
from factory.fuzzy import FuzzyText

from accounts.tests.factories import (
    ProfileFactory,
    ProviderUserFactory,
    CustomerFactory,
)
from sales.services.collections_service import CollectionInvoicesService
from sales.tests.factories.sales_factories import (
    AutomatedCollectionNoticesSettingsFactory,
)
from test_utils import BaseProviderTestCase


class TestCollectionInvoicesService(BaseProviderTestCase):
    """
    Test service class: CollectionInvoicesService
    """

    service_class = CollectionInvoicesService

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_get_provider_user_details(self):
        """
        Test method: get_provider_user_details
        """
        provider_user = ProviderUserFactory(provider=self.provider)
        ProfileFactory.create(
            user=provider_user,
            email_signature=dict(
                email_signature="<p>mock signature html</p>",
                email_signature_markdown="mock signature markdown",
            ),
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        provider_user_details: Dict = (
            collection_service.get_provider_user_details(provider_user.email)
        )
        expected_payload: Dict = dict(
            name=provider_user.name,
            phone_number=provider_user.profile.full_office_phone_number,
            email=provider_user.email,
            title=provider_user.profile.title,
            email_signature=provider_user.profile.email_signature.get(
                "email_signature"
            ),
        )
        self.assertDictEqual(expected_payload, provider_user_details)

    def test_exclude_pattern_matched_from_starting(self):
        """
        Test method: exclude_pattern_matched for patterns matching at start
        """
        patterns_to_match: List[str] = ["PAT*", "INV*", "*AM"]
        mock_invoice_number: str = "PAT-IN12345"
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        is_pattern_matched: bool = collection_service.exclude_pattern_matched(
            mock_invoice_number, patterns_to_match
        )
        self.assertEqual(is_pattern_matched, True)

    def test_exclude_pattern_matched_from_end(self):
        """
        Test method: exclude_pattern_matched for patterns matching at end
        """
        patterns_to_match: List[str] = ["INV*", "*PAT"]
        mock_invoice_number: str = "IN12345-PAT"
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        is_pattern_matched: bool = collection_service.exclude_pattern_matched(
            mock_invoice_number, patterns_to_match
        )
        self.assertEqual(is_pattern_matched, True)

    def test_filter_invoices_using_exclude_patterns(self):
        fake = faker.Faker()
        mock_invoices_data: List[Dict] = [
            dict(
                invoice_crm_id=fake.pyint(),
                invoice_number="PAT-IN12345",
                billing_status_crm_id=fake.pyint(),
                billing_status_name="Closed",
                customer_crm_id=fake.pyint(),
                customer_name=fake.company(),
                customer_po=FuzzyText().fuzz(),
                billing_term_name=f"As per {fake.name()}",
                due_date=fake.date_this_month(),
                balance=fake.pyfloat(),
            ),
            dict(
                invoice_crm_id=fake.pyint(),
                invoice_number="12345-PAT",
                billing_status_crm_id=fake.pyint(),
                billing_status_name="Open",
                customer_crm_id=fake.pyint(),
                customer_name=fake.company(),
                customer_po=FuzzyText().fuzz(),
                billing_term_name=f"As per {fake.name()}",
                due_date=fake.date_this_month(),
                balance=fake.pyfloat(),
            ),
        ]
        mock_exclude_patterns: List[str] = ["PAT*", "IN*"]
        expected_filtered_invoice: Dict = mock_invoices_data[1]
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        actual_filtered_invoice: List[
            Dict
        ] = collection_service.filter_invoices_using_exclude_patterns(
            mock_invoices_data, mock_exclude_patterns
        )
        self.assertDictEqual(
            expected_filtered_invoice, actual_filtered_invoice[0]
        )

    def test_split_past_and_approaching_due_date_invoices(self):
        """
        Test method: split_past_and_approaching_due_date_invoices
        """
        fake = faker.Faker()
        mock_customer_crm_id: int = 100
        mock_customer_name: str = fake.company()
        now = datetime.now()
        past_datetime = now - timedelta(days=3)
        future_datetime = now + timedelta(days=3)
        mock_customer_invoices_data: List[Dict] = [
            dict(
                invoice_crm_id=fake.pyint(),
                invoice_number="PAT-IN12345",
                billing_status_crm_id=fake.pyint(),
                billing_status_name="Closed",
                customer_crm_id=mock_customer_crm_id,
                customer_name=mock_customer_name,
                customer_po=FuzzyText().fuzz(),
                billing_term_name=f"As per {fake.name()}",
                due_date=past_datetime.strftime("%Y-%m-%dT%H:%M:%SZ"),
                balance=fake.pyfloat(),
            ),
            dict(
                invoice_crm_id=fake.pyint(),
                invoice_number="12345-PAT",
                billing_status_crm_id=fake.pyint(),
                billing_status_name="Open",
                customer_crm_id=mock_customer_crm_id,
                customer_name=mock_customer_name,
                customer_po=FuzzyText().fuzz(),
                billing_term_name=f"As per {fake.name()}",
                due_date=future_datetime.strftime("%Y-%m-%dT%H:%M:%SZ"),
                balance=fake.pyfloat(),
            ),
        ]
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        actual_modified_invoice_data: Dict = (
            collection_service.split_past_and_approaching_due_date_invoices(
                mock_customer_invoices_data
            )
        )
        expected_modified_invoices_data: Dict = dict(
            invoices_past_due_date=[mock_customer_invoices_data[0]],
            invoices_with_approaching_due_date=[
                mock_customer_invoices_data[1]
            ],
        )
        self.assertEqual(
            expected_modified_invoices_data, actual_modified_invoice_data
        )

    def test_get_invoices_data_from_cw(self):
        """
        Test method: get_invoices_data_from_cw
        """
        fake = faker.Faker()
        automated_collection_notices_setting: AutomatedCollectionNoticesSettings = AutomatedCollectionNoticesSettingsFactory.create(
            provider=self.provider
        )
        mock_cw_invoice_data: List[Dict] = [
            dict(
                invoice_crm_id=fake.pyint(),
                invoice_number="PAT-IN12345",
                billing_status_crm_id=fake.pyint(),
                billing_status_name="Closed",
                customer_crm_id=fake.pyint(),
                customer_name=fake.company(),
                customer_po=FuzzyText().fuzz(),
                billing_term_name=f"As per {fake.name()}",
                due_date=fake.date_this_month(),
                balance=fake.pyfloat(),
            )
        ]
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.get_invoices.return_value = (
            mock_cw_invoice_data
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        actual_cw_invoice_data = collection_service.get_invoices_data_from_cw(
            automated_collection_notices_setting, 123
        )
        self.assertEqual(actual_cw_invoice_data, mock_cw_invoice_data)

    def test_format_due_date_of_invoices(self):
        """
        Test method: format_due_date_of_invoices
        """
        fake = faker.Faker()
        mock_customer_name: str = fake.company()
        due_date_1_object = datetime(2022, 12, 6)
        due_date_1: str = due_date_1_object.strftime("%Y-%m-%dT%H:%M:%SZ")
        formatted_due_date_1: str = due_date_1_object.strftime("%-m/%d/%Y")
        due_date_2_object = datetime(2022, 12, 6)
        due_date_2: str = due_date_2_object.strftime("%Y-%m-%dT%H:%M:%SZ")
        formatted_due_date_2: str = due_date_2_object.strftime("%-m/%d/%Y")
        mock_customer_invoices_data: Dict = dict(
            invoices_past_due_date=[
                dict(
                    invoice_crm_id=fake.pyint(),
                    invoice_number="PAT-IN12345",
                    billing_status_crm_id=fake.pyint(),
                    billing_status_name="Closed",
                    customer_crm_id=100,
                    customer_name=mock_customer_name,
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"As per {fake.name()}",
                    due_date=due_date_1,
                    balance=fake.pyfloat(),
                )
            ],
            invoices_with_approaching_due_date=[
                dict(
                    invoice_crm_id=fake.pyint(),
                    invoice_number="12345-PAT",
                    billing_status_crm_id=fake.pyint(),
                    billing_status_name="Open",
                    customer_crm_id=100,
                    customer_name=mock_customer_name,
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"As per {fake.name()}",
                    due_date=due_date_2,
                    balance=fake.pyfloat(),
                )
            ],
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        collection_service.format_due_date_of_invoices(
            mock_customer_invoices_data
        )
        expected_formatted_due_dates: Dict = dict(
            due_date_1=formatted_due_date_1, due_date_2=formatted_due_date_2
        )
        actual_formatted_due_date: Dict = dict(
            due_date_1=mock_customer_invoices_data["invoices_past_due_date"][
                0
            ].get("due_date"),
            due_date_2=mock_customer_invoices_data[
                "invoices_with_approaching_due_date"
            ][0].get("due_date"),
        )
        self.assertDictEqual(
            expected_formatted_due_dates, actual_formatted_due_date
        )

    def test_get_rendered_automated_collection_notice_email_body(self):
        """
        Test method: get_rendered_automated_collection_notice_email_body
        """
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        rendered_template: str = collection_service.get_rendered_automated_collection_notice_email_body(
            {}
        )
        self.assertTrue(len(rendered_template) > 0)

    def test_get_customer_accounting_contact_details(self):
        """
        Test method: get_customer_accounting_contact_details
        """
        fake = faker.Faker()
        automated_collection_setting: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        )
        mock_customer_crm_id: int = 100
        contact_type_crm_id: int = (
            automated_collection_setting.contact_type_crm_id
        )
        mock_customer_users_data: List[Dict] = [
            dict(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                title=fake.job(),
                email=fake.email(),
            ),
            dict(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                title=fake.job(),
                email=fake.email(),
            ),
        ]
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.get_customer_users.return_value = (
            mock_customer_users_data
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        customer_accounting_contact_details = (
            collection_service.get_customer_accounting_contact_details(
                mock_customer_crm_id, contact_type_crm_id
            )
        )
        self.assertEqual(
            customer_accounting_contact_details, mock_customer_users_data[0]
        )

    @patch(
        "sales.tests.test_services.test_collections_service.CollectionInvoicesService.get_email_sent_details",
        return_value="Tuesday, October 18, 2022 12:26 AM",
    )
    def test_get_email_context_data(self, get_email_sent_details_call):
        """
        Test method: get_email_context_data
        """
        mock_provider_user_details: Dict = dict(
            name=self.user.name,
            phone_number=self.user.profile.full_office_phone_number,
            email=self.user.email,
            title=self.user.profile.title,
            email_signature=self.user.profile.email_signature.get(
                "email_signature", None
            ),
        )
        automated_collection_notices_setting: AutomatedCollectionNoticesSettings = AutomatedCollectionNoticesSettingsFactory.create(
            provider=self.provider, send_from_email=self.user.email
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        expected_email_context: Dict = dict(
            sender_details=mock_provider_user_details,
            email_sent_details="Tuesday, October 18, 2022 12:26 AM",
            subject=automated_collection_notices_setting.subject,
            email_body=automated_collection_notices_setting.email_body_template,
            to_emails_list=automated_collection_notices_setting.send_to_email,
        )
        actual_email_context: Dict = (
            collection_service.get_email_context_data()
        )
        self.assertEqual(expected_email_context, actual_email_context)

    @patch(
        "sales.tests.test_services.test_collections_service.CollectionInvoicesService."
        "get_collection_notice_email_preview",
    )
    def test_get_collection_notice_email_preview(
        self, mock_get_collection_notice_email_preview
    ):
        """
        Test method: get_collection_notice_email_preview
        """
        mock_html_preview: str = "<html><body>Email preview</body></html>"
        mock_get_collection_notice_email_preview.return_value = (
            mock_html_preview
        )
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        actual_email_context: str = (
            collection_service.get_collection_notice_email_preview()
        )
        self.assertEqual(actual_email_context, mock_html_preview)

    def test_get_formatted_balance(self):
        """
        Test method: get_formatted_balance
        """
        balance: float = 123456.7
        expected_result: str = "123,456.70"
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        formatted_balance: str = collection_service.get_formatted_balance(
            balance
        )
        self.assertEqual(expected_result, formatted_balance)

    def test_format_balance_of_invoices(self):
        """
        Test method: format_balance_of_invoices
        """
        fake = faker.Faker()
        mock_customer_name: str = fake.company()
        due_date_1_object = fake.date_time_this_month()
        due_date_1: str = due_date_1_object.strftime("%Y-%m-%dT%H:%M:%SZ")
        due_date_2_object = fake.future_datetime()
        due_date_2: str = due_date_2_object.strftime("%Y-%m-%dT%H:%M:%SZ")
        mock_customer_invoices_data: Dict = dict(
            invoices_past_due_date=[
                dict(
                    invoice_crm_id=fake.pyint(),
                    invoice_number="PAT-IN12345",
                    billing_status_crm_id=fake.pyint(),
                    billing_status_name="Closed",
                    customer_crm_id=100,
                    customer_name=mock_customer_name,
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"As per {fake.name()}",
                    due_date=due_date_1,
                    balance=123456.789,
                )
            ],
            invoices_with_approaching_due_date=[
                dict(
                    invoice_crm_id=fake.pyint(),
                    invoice_number="12345-PAT",
                    billing_status_crm_id=fake.pyint(),
                    billing_status_name="Open",
                    customer_crm_id=100,
                    customer_name=mock_customer_name,
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"As per {fake.name()}",
                    due_date=due_date_1,
                    balance=123456,
                )
            ],
        )
        expected_invoices_data: Dict = copy.deepcopy(
            mock_customer_invoices_data
        )
        expected_invoices_data["invoices_past_due_date"][0][
            "balance"
        ] = "123,456.79"
        expected_invoices_data["invoices_with_approaching_due_date"][0][
            "balance"
        ] = "123,456.00"
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        collection_service.format_balance_of_invoices(
            mock_customer_invoices_data
        )
        self.assertEqual(expected_invoices_data, mock_customer_invoices_data)

    @patch(
        "sales.services.collections_service.AutomatedCollectionNoticeEmail.send"
    )
    @patch(
        "sales.services.collections_service.CollectionInvoicesService."
        "get_rendered_automated_collection_notice_email_body"
    )
    @patch(
        "sales.services.collections_service.CollectionInvoicesService.get_customer_accounting_contact_details"
    )
    @patch(
        "sales.services.collections_service.CollectionInvoicesService.get_email_context_data"
    )
    @patch(
        "sales.services.collections_service.CollectionInvoicesService.get_invoices_data_for_customers"
    )
    def test_send_collection_notice_email_for_customer(
        self,
        get_invoices_data_for_customers,
        get_email_context_data,
        get_customer_accounting_contact_details,
        get_rendered_automated_collection_notice_email_body,
        send_email,
    ):
        # Test when invoices found
        customer: Customer = CustomerFactory(provider=self.provider)
        get_invoices_data_for_customers.return_value = {}
        collection_service: CollectionInvoicesService = self.service_class(
            provider=self.provider
        )
        (
            email_sent,
            error,
        ) = collection_service.send_collection_notice_email_for_customer(
            customer
        )
        self.assertFalse(email_sent)
        self.assertEqual("Invoices not found for customer!", error)
        # Test when invoice data exists
        # Test cases for the called methods alredy exist. Mock methods with minimum data so that the email is triggered
        AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        fake = faker.Faker()
        get_invoices_data_for_customers.return_value = dict(
            invoices_past_due_date=[
                dict(
                    invoice_crm_id=fake.pyint(),
                    invoice_number="PAT-IN12345",
                    billing_status_crm_id=fake.pyint(),
                    billing_status_name="Closed",
                    customer_crm_id=100,
                    customer_name=fake.company(),
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"As per {fake.name()}",
                    due_date="06/19/2023",
                    balance=fake.pyfloat(),
                )
            ]
        )
        get_email_context_data.return_value = dict(
            subject="This is subject",
            sender_details=dict(email="apple@google.com"),
        )
        get_customer_accounting_contact_details.return_value = dict(
            first_name="First", last_name="Last", email="v5test@yopmail.com"
        )
        get_rendered_automated_collection_notice_email_body.return_value = (
            "<p>Hello!</p>"
        )
        (
            email_sent,
            error,
        ) = collection_service.send_collection_notice_email_for_customer(
            customer
        )
        self.assertTrue(email_sent)
        send_email.assert_called()
