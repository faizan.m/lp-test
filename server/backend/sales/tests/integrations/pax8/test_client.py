# from sales.integrations.pax8.client import PAX8Client
# from unittest import TestCase
# from faker import Faker
# from unittest.mock import MagicMock, Mock, patch
#
#
# class TestPAX8Client(TestCase):
#
#     api_client = MagicMock("sales.integrations.pax8.client.PAX8Client", autospec=True).return_value
#
#     def test__set_auth_token(self):
#         """
#         Test method: _set_auth_token
#         """
#         mock_auth_token: str = "A1B2C3D4E5F6G7H8I9"
#         mock_call = self.api_client._set_auth_token
#         self.api_client._set_auth_token(mock_auth_token)
#         mock_call.assert_called_with((mock_auth_token))
#
#     def test__set_token_expiry(self):
#         """
#         Test method: _set_token_expiry
#         """
#         token_expiry_in_seconds: int = 10
#         mock_call = self.api_client._set_token_expiry
#         self.api_client._set_token_expiry(token_expiry_in_seconds)
#         mock_call.assert_called_with((token_expiry_in_seconds))
#
#
#
