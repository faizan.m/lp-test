import uuid
from typing import Dict, List, Union, Tuple

from factory.faker import faker
from factory.fuzzy import FuzzyChoice
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from factory.fuzzy import FuzzyText
from sales.models import (
    AgreementTemplate,
    Agreement,
    AutomatedCollectionNoticesSettings,
)
import random


PAX8_PRODUCT_BILLING_TERMS: List[str] = ["Monthly", "One-Time", "Trial"]
PAX8_PRODUCT_COMMITMENT_TERMS: List[str] = ["Monthly", "1-Year"]
PAX8_PRODUCT_PRICING_TYPE: List[str] = ["flat", "volume"]
PAX8_PRODUCT_UNIT_OF_MEASUREMENT: List[str] = ["Each", "Instance"]


def render_response(response: Response) -> Response:
    """
    Method to explicitly render API response.

    Args:
        response: Non rendered API Response object

    Returns:
        Rendered Response object
    """
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
    response.renderer_context = {}
    response.render()
    return response


def add_query_params_to_url(
    url: str, query_params: Dict, object_id: Union[int, None] = None
) -> str:
    """
    Method to format a URL with object ID and query parameters.

    Args:
        url: URL endpoint
        object_id: ID of object
        query_params: Query parameters

    Returns:
        URL with query parameters
    """
    new_url: str = f"{url}/{object_id}?" if object_id else f"{url}?"
    for key, value in query_params.items():
        new_url += f"{key}={value}&"
    return new_url


def get_sample_sections_data(empty: bool = False) -> List:
    """
    Method to generate sections data for Agreement Template
    """
    if empty:
        return []
    fake = faker.Faker()
    return [
        {
            "name": fake.name(),
            "content": fake.text(max_nb_chars=100),
            "is_locked": fake.pybool(),
            "id": fake.pyint(),
            "value_html": fake.text(max_nb_chars=100),
        },
        {
            "name": fake.name(),
            "content": fake.text(max_nb_chars=100),
            "is_locked": fake.pybool(),
            "id": fake.pyint(),
            "value_html": fake.text(max_nb_chars=100),
        },
    ]


def get_sample_products_data(empty: bool = False) -> List:
    """
    Method to generate sample product data for Agreement Template
    """
    if empty:
        return []
    fake = faker.Faker()
    product_types: List[str] = [
        AgreementTemplate.SERVICE,
        AgreementTemplate.HARDWARE,
    ]
    billing_options: List[str] = [
        AgreementTemplate.PER_ANNUM,
        AgreementTemplate.PER_MONTH,
        AgreementTemplate.ONE_TIME,
    ]
    return [
        {
            "products": [
                {
                    "name": fake.name(),
                    "margin": fake.pyint(),
                    "description": fake.text(max_nb_chars=100),
                    "customer_cost": fake.pyint(),
                    "internal_cost": fake.pyint(),
                    "quantity": fake.pyint(),
                }
            ],
            "is_bundle": False,
            "product_type": FuzzyChoice(choices=product_types).fuzz(),
            "billing_option": FuzzyChoice(choices=billing_options).fuzz(),
            "id": fake.pyint(),
            "is_checked": fake.pybool(),
            "selected_product": 0,
            "is_required": fake.pybool(),
            "comment": None,
        },
        {
            "products": [
                {
                    "name": fake.name(),
                    "margin": fake.pyint(),
                    "description": fake.text(max_nb_chars=100),
                    "customer_cost": fake.pyint(),
                    "internal_cost": fake.pyint(),
                    "quantity": fake.pyint(),
                },
                {
                    "name": fake.name(),
                    "margin": fake.pyint(),
                    "description": fake.text(max_nb_chars=100),
                    "customer_cost": fake.pyint(),
                    "internal_cost": fake.pyint(),
                    "quantity": fake.pyint(),
                },
            ],
            "is_bundle": True,
            "product_type": FuzzyChoice(choices=product_types).fuzz(),
            "billing_option": FuzzyChoice(choices=billing_options).fuzz(),
            "id": fake.pyint(),
            "is_checked": fake.pybool(),
            "selected_product": 1,
            "is_required": fake.pybool(),
            "comment": None,
        },
    ]


def get_invalid_request_payload() -> Dict:
    """
    Method to generate an invalid request payload

    Returns:
        Invalid request payload
    """
    fake = faker.Faker()
    # Omit some required fields and add wrong schema for sections
    return dict(
        sections={"name": fake.name()},
        version_description=fake.text(),
    )


def create_versions_of_template(obj) -> Tuple[int, int, int]:
    """
    Method to create multiple version of a template
    Useful in cases where we need template with version
    E.g. version listing, deletion, etc.

    Args:
        obj: Object of TestAgreementTemplatesAPIs class

    Returns:
        Template IDs tuple
    """
    from sales.tests.factories.agreement_factories import (
        AgreementTemplateFactory,
    )

    # Create a scenario where we have multiple versions of a template
    first_version_template: AgreementTemplate = AgreementTemplateFactory(
        provider=obj.provider,
        author=obj.user,
        updated_by=obj.user,
        is_current_version=False,
    )
    second_version_template: AgreementTemplate = AgreementTemplateFactory(
        provider=obj.provider,
        author=obj.user,
        updated_by=obj.user,
        major_version=1,
        minor_version=1,
        parent_template=first_version_template,
        root_template=first_version_template,
        is_current_version=True,
    )
    return (
        first_version_template.id,
        second_version_template.id,
        second_version_template.root_template_id,
    )


def create_versions_of_agreement(obj) -> Tuple[int, int, int]:
    """
    Method to create multiple version of agreement
    Useful in cases where we need agreement with version
    E.g. version listing, deletion, etc.

    Args:
        obj: Object of TestAgreement class

    Returns:
        Agreement IDs tuple
    """
    from sales.tests.factories import AgreementFactory

    # Create a scenario where we have multiple versions of a template
    first_version_agreement: Agreement = AgreementFactory(
        provider=obj.provider,
        template=obj.template,
        customer=obj.customer,
        user=obj.user,
        author=obj.user,
        updated_by=obj.user,
        is_current_version=False,
    )
    second_version_agreement: Agreement = AgreementFactory(
        provider=obj.provider,
        template=obj.template,
        customer=obj.customer,
        user=obj.user,
        author=obj.user,
        updated_by=obj.user,
        major_version=1,
        minor_version=1,
        parent_agreement=first_version_agreement,
        root_agreement=first_version_agreement,
        is_current_version=True,
    )
    return (
        first_version_agreement.id,
        second_version_agreement.id,
        second_version_agreement.root_agreement_id,
    )


def get_mock_send_to_emails_list() -> List[str]:
    """
    Get a list of random emails

    Returns:
        List containing random emails
    """
    fake = faker.Faker()
    email_list: List[str] = [fake.email() for i in range(5)]
    return email_list


def get_mock_weekly_send_schedule() -> List[str]:
    days_choices: List[str] = [
        day
        for day, day in AutomatedCollectionNoticesSettings.WEEKLY_SCHEDULE_CHOICES
    ]
    random_day_1: str = FuzzyChoice(choices=days_choices).fuzz()
    return [random_day_1]


def get_mock_billing_statuses() -> List[Dict]:
    fake = faker.Faker()
    return [
        dict(billing_status_id=fake.pyint(), billing_status_name=fake.name()),
        dict(billing_status_id=fake.pyint(), billing_status_name=fake.name()),
    ]


def get_mock_cw_invoices_data() -> List[Dict]:
    fake = faker.Faker()
    return [
        dict(
            invoice_crm_id=fake.pyint(),
            invoice_number="PAT-IN12345",
            billing_status_crm_id=fake.pyint(),
            billing_status_name="Closed",
            customer_crm_id=fake.pyint(),
            customer_name=fake.company(),
            customer_po=FuzzyText().fuzz(),
            billing_term_name=f"As per {fake.name()}",
            due_date=fake.datetime_this_decade(),
            balance=fake.pyfloat(),
        ),
        dict(
            invoice_crm_id=fake.pyint(),
            invoice_number="IN12345-PAT",
            billing_status_crm_id=fake.pyint(),
            billing_status_name="Open",
            customer_crm_id=fake.pyint(),
            customer_name=fake.company(),
            customer_po=FuzzyText().fuzz(),
            billing_term_name=f"As per {fake.name()}",
            due_date=fake.datetime_this_decade(),
            balance=fake.pyfloat(),
        ),
    ]


def get_mock_uuid_value() -> str:
    """
    Get uuid value.

    Returns:
        UUID value.
    """
    return str(uuid.uuid4())


def get_mock_vendor_names() -> List[str]:
    """
    Get list of mock vendor names.

    Returns:
        List of mock vendor names.
    """
    fake = faker.Faker()
    return [fake.company() for _ in range(5)]


def get_mock_billing_term() -> str:
    """
    Get mock billing term.

    Returns:
        Mock billing term.
    """
    return random.choice(PAX8_PRODUCT_BILLING_TERMS)


def get_mock_commitment_term() -> str:
    """
    Get mock commitment term.

    Returns:
        Mock commitment term.
    """
    return random.choice(PAX8_PRODUCT_COMMITMENT_TERMS)


def get_mock_pricing_type() -> str:
    """
    Get mock pricing type.

    Returns:
        Mock pricing type.
    """
    return random.choice(PAX8_PRODUCT_PRICING_TYPE)


def get_mock_unit_of_measurement() -> str:
    """
    Get mock unit of measurement.

    Returns:
        Mock unit of measurement.
    """
    return random.choice(PAX8_PRODUCT_UNIT_OF_MEASUREMENT)


def get_mock_pricing_rates() -> List[Dict]:
    """
    Get mock pricing rate data.

    Returns:
        Mock pricing rate data for a pax8 product.
    """
    fake = faker.Faker()
    return [
        dict(
            partner_buy_rate=fake.pyfloat(min_value=0, max_value=500),
            suggested_retail_price=fake.pyfloat(min_value=0, max_value=500),
            start_quantity_range=fake.pyint(min_value=0, max_value=5),
            end_quantity_range=fake.pyint(min_value=5, max_value=10),
            charge_type="Per Unit",
        )
    ]


def get_mock_pax8_product_pricing_data() -> List[Dict]:
    """
    Get mock pax8 product pricing data.

    Returns:
        Mock PAX8 product pricing data.
    """
    fake = faker.Faker()
    return [
        dict(
            billing_term=get_mock_billing_term(),
            commitment_term=get_mock_commitment_term(),
            commitment_term_in_months=fake.pyint(min_value=1, max_value=36),
            pricing_type=get_mock_pricing_type(),
            unit_of_measurement=get_mock_unit_of_measurement(),
            pricin_rates=get_mock_pricing_rates(),
        )
    ]


def get_mock_agreement_pax8_products_data() -> List[Dict]:
    """
    Get mock PAX8 product data.

    Returns:
        Mock pax8 product data.
    """
    fake = faker.Faker()
    return [
        dict(
            product_crm_id=get_mock_uuid_value(),
            product_name=fake.bs(),
            vendor_name=fake.company(),
            short_description=fake.text(max_nb_chars=5),
            sku=fake.bban(),
            billing_term=get_mock_billing_term(),
            commitment_term=get_mock_commitment_term(),
            commitment_term_in_months=fake.pyint(min_value=1, max_value=36),
            pricing_type=get_mock_pricing_type(),
            unit_of_measurement=get_mock_unit_of_measurement(),
            partner_buy_rate=fake.pyfloat(min_value=100, max_value=1000),
            suggested_retail_price=fake.pyfloat(min_value=100, max_value=1000),
            start_quantity_range=fake.pyint(min_value=1, max_value=10),
            end_quantity_range=fake.pyint(min_value=1, max_value=10),
            charge_type="Per Unit",
            internal_cost=fake.pyfloat(min_value=100, max_value=1000),
            customer_cost=fake.pyfloat(min_value=100, max_value=1000),
            margin=fake.pyfloat(min_value=100, max_value=1000),
            revenue=fake.pyfloat(min_value=100, max_value=1000),
            total_margin=fake.pyfloat(min_value=100, max_value=1000),
            quantity=fake.pyint(min_value=1, max_value=10),
            min_quantity=fake.pyint(min_value=1, max_value=5),
            max_quantity=fake.pyint(min_value=6, max_value=10),
            is_comment=False,
            comment=None,
            id=fake.pyint(),
        )
    ]


def get_mock_pax8_products_data_for_model() -> Dict:
    """
    Get mock pax8 product data as required for storing in model.

    Returns:
        Mock pax8 product data.
    """
    fake = faker.Faker()
    return dict(
        name=fake.name(), products=get_mock_agreement_pax8_products_data()
    )


def get_valid_request_payload() -> Dict:
    """
    Method to generate a valid request payload

    Returns:
        Valid request payload
    """
    fake = faker.Faker()
    return dict(
        sections=get_sample_sections_data(),
        products=get_sample_products_data(),
        name=fake.name(),
        min_discount=fake.pyint(),
        max_discount=fake.pyint(),
        version_description=fake.text(),
        pax8_products=get_mock_pax8_products_data_for_model(),
    )
