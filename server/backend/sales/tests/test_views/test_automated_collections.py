from typing import List, Dict
from unittest.mock import patch

import faker
import mock
from django.urls import reverse
from rest_framework.request import Request
from rest_framework.response import Response

from sales.models import AutomatedCollectionNoticesSettings
from sales.tests.factories.sales_factories import (
    AutomatedCollectionNoticesSettingsFactory,
)
from sales.views.automated_collections import (
    AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView,
    BillingStatusesListAPIView,
    SendAutomatedCollectionNoticeEmailTaskTriggerAPIView,
    CollectionNoticeEmailPreviewAPIView,
)
from test_utils import BaseProviderTestCase
from test_utils import TestClientMixin


class TestAutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView(
    BaseProviderTestCase
):
    """
    Test the view: AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:automated-collection-notices-setting"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_retrieval_of_automated_collection_notices_settings(self):
        """
        Test retrieval of Automated Collection Notices Settings
        """
        AutomatedCollectionNoticesSettingsFactory.create(
            provider=self.provider
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_error_when_retrieving_settings_if_not_already_configured(self):
        """
        API should raise Not Found error when Automated Collection Notices Settings not
        configured for the Provider.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 404)

    def test_creation_of_automated_collection_notices_settings(self):
        """
        Test creation of Automated Collection Notices Setting for the Provider
        """
        mock_settings: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.build()
        )
        request_paylaod: Dict = dict(
            weekly_send_schedule=mock_settings.weekly_send_schedule,
            send_to_email=mock_settings.send_to_email,
            send_from_email=mock_settings.send_from_email,
            invoice_exclude_patterns=mock_settings.invoice_exclude_patterns,
            approaching_due_date_ageing=mock_settings.approaching_due_date_ageing,
            email_body_template=mock_settings.email_body_template,
            billing_statuses=mock_settings.billing_statuses,
            subject=mock_settings.subject,
            contact_type_crm_id=mock_settings.contact_type_crm_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_paylaod
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        request_paylaod["provider_id"] = self.provider.id
        setting: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettings.objects.get(
                provider=self.provider
            )
        )
        self.assertEqual(response.status_code, 201)
        actual_setting_data: Dict = dict(
            provider_id=setting.provider_id,
            weekly_send_schedule=setting.weekly_send_schedule,
            send_to_email=setting.send_to_email,
            send_from_email=setting.send_from_email,
            invoice_exclude_patterns=setting.invoice_exclude_patterns,
            approaching_due_date_ageing=setting.approaching_due_date_ageing,
            email_body_template=setting.email_body_template,
            billing_statuses=setting.billing_statuses,
            subject=setting.subject,
            contact_type_crm_id=setting.contact_type_crm_id,
        )
        self.assertDictEqual(request_paylaod, actual_setting_data)

    def test_error_creating_settings_if_already_configured_for_the_provider(
        self,
    ):
        """
        API should raise error when creating settings if the Automated Collection Notices Settings
        already exists for the Provider.
        """
        AutomatedCollectionNoticesSettingsFactory.create(
            provider=self.provider
        )
        mock_settings: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.build()
        )
        request_paylaod: Dict = dict(
            weekly_send_schedule=mock_settings.weekly_send_schedule,
            send_to_email=mock_settings.send_to_email,
            send_from_email=mock_settings.send_from_email,
            invoice_exclude_patterns=mock_settings.invoice_exclude_patterns,
            approaching_due_date_ageing=mock_settings.approaching_due_date_ageing,
            email_body_template=mock_settings.email_body_template,
            billing_statuses=mock_settings.billing_statuses,
            subject=mock_settings.subject,
            test_creation_of_automated_collection_notices_settings=mock_settings.contact_type_crm_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_paylaod
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        request_paylaod["provider_id"] = self.provider.id
        self.assertEqual(response.status_code, 400)

    def test_updating_to_automated_collection_notices_settings(self):
        """
        Test to update the Automated Collection Notices Settings
        """
        configured_setting: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.create(
                provider=self.provider
            )
        )
        mock_settings: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.build()
        )
        update_payload: Dict = dict(
            weekly_send_schedule=mock_settings.weekly_send_schedule,
            send_to_email=mock_settings.send_to_email,
            send_from_email=mock_settings.send_from_email,
            invoice_exclude_patterns=mock_settings.invoice_exclude_patterns,
            approaching_due_date_ageing=mock_settings.approaching_due_date_ageing,
            email_body_template=mock_settings.email_body_template,
            billing_statuses=mock_settings.billing_statuses,
            subject=mock_settings.subject,
            contact_type_crm_id=mock_settings.contact_type_crm_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=update_payload
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        configured_setting.refresh_from_db()
        configured_setting_data: Dict = dict(
            weekly_send_schedule=configured_setting.weekly_send_schedule,
            send_to_email=configured_setting.send_to_email,
            send_from_email=configured_setting.send_from_email,
            invoice_exclude_patterns=configured_setting.invoice_exclude_patterns,
            approaching_due_date_ageing=configured_setting.approaching_due_date_ageing,
            email_body_template=configured_setting.email_body_template,
            billing_statuses=configured_setting.billing_statuses,
            subject=configured_setting.subject,
            contact_type_crm_id=configured_setting.contact_type_crm_id,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(update_payload, configured_setting_data)

    def test_error_creating_setting_for_invalid_payload(self):
        """
        API should raise error when creating Automated Collection Notices Settings
        for invalid request payload
        """
        mock_settings: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.build()
        )
        request_paylaod: Dict = dict(
            send_from_email=mock_settings.send_from_email,
            invoice_exclude_patterns=mock_settings.invoice_exclude_patterns,
            email_body_template=mock_settings.email_body_template,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_paylaod
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_error_updating_setting_for_invalid_payload(self):
        """
        API should raise error when updating Automated Collection Notices Settings
        for invalid request payload
        """
        AutomatedCollectionNoticesSettingsFactory.create(
            provider=self.provider
        )
        mock_settings: AutomatedCollectionNoticesSettings = (
            AutomatedCollectionNoticesSettingsFactory.build()
        )
        request_paylaod: Dict = dict(
            send_from_email=mock_settings.send_from_email,
            invoice_exclude_patterns=mock_settings.invoice_exclude_patterns,
            email_body_template=mock_settings.email_body_template,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=request_paylaod
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_error_when_retrieving_settings_if_user_is_not_authorized(self):
        """
        API should raise error for unauthorized access.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
        )
        view = (
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestBillingStatusesListAPIView(BaseProviderTestCase):
    """
    Test the view: AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView
    """

    url: str = reverse("api_v1_endpoints:providers:list-billing-statuses")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_billing_statuses(self):
        """
        Test listing of billing statuses.
        """
        provider: Provider = self.provider
        provider.erp_client = mock.Mock()
        mock_cw_billing_statuses: List[Dict] = [
            {"billing_status_crm_id": 1, "billing_status_name": "New"},
            {"billing_status_crm_id": 2, "billing_status_name": "Closed"},
        ]
        provider.erp_client.get_billing_statuses_from_cw.return_value = (
            mock_cw_billing_statuses
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = BillingStatusesListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, mock_cw_billing_statuses)

    def test_api_error_in_billing_statuses_listing_for_unauthorized_access(
        self,
    ):
        """
        API should raise error for unauthorized access.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
        )
        view = BillingStatusesListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestSendAutomatedCollectionNoticeEmailTaskTriggerAPIView(
    BaseProviderTestCase
):
    """
    Test the view: SendAutomatedCollectionNoticeEmailTaskTriggerAPIView
    """

    url: str = reverse("api_v1_endpoints:providers:send-collection-notices")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "sales.views.automated_collections.trigger_collection_notice_emails.apply_async",
    )
    def test_sending_collection_notices_email_for_specific_customers(
        self, trigger_collection_notice_emails
    ):
        """
        Test sending collection notice emails for specific customers.
        """
        AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        fake = faker.Faker()
        request_body: Dict = dict(customer_crm_ids=[1, 2], all_customers=False)
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SendAutomatedCollectionNoticeEmailTaskTriggerAPIView().as_view()
        response: Response = view(request)
        trigger_collection_notice_emails.assert_called_with(
            args=(self.provider.id, False, [1, 2]), queue="high"
        )
        self.assertTrue("task_id" in response.data)

    @patch(
        "sales.views.automated_collections.trigger_collection_notice_emails.apply_async",
        return_value=mock.MagicMock(),
    )
    def test_sending_collection_notices_email_for_all_customers(
        self, trigger_collection_notice_emails
    ):
        """
        Test sending collection notice emails for all customers.
        """
        AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        request_body: Dict = dict(customer_crm_ids=[], all_customers=True)
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SendAutomatedCollectionNoticeEmailTaskTriggerAPIView().as_view()
        response: Response = view(request)
        trigger_collection_notice_emails.assert_called_with(
            args=(self.provider.id, True, []), queue="high"
        )
        self.assertTrue("task_id" in response.data)

    def test_error_sending_collection_notice_email_for_invalid_request_body(
        self,
    ):
        """
        API should raise validation error for invalid request body.
        """
        AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        request_body: Dict = dict(customer_crm_ids=[])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SendAutomatedCollectionNoticeEmailTaskTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_api_error_triggering_notice_email_when_setting_not_configured(
        self,
    ):
        """
        API should raise error when automated collection notice setting not configured for the Provider.
        """
        request_body: Dict = dict(customer_crm_ids=[1, 2], all_customers=False)
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = SendAutomatedCollectionNoticeEmailTaskTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_api_error_in_sending_collection_notice_email_for_unauthorized_user(
        self,
    ):
        """
        API should raise error for unauthorized access.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider
        )
        view = SendAutomatedCollectionNoticeEmailTaskTriggerAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestCollectionNoticeEmailPreviewAPIView(BaseProviderTestCase):
    """
    Test the view: AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView
    """

    url: str = reverse("api_v1_endpoints:providers:preview-collection-notice")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "sales.views.automated_collections.CollectionInvoicesService.get_collection_notice_email_preview",
    )
    def test_collection_notice_preview(
        self, mock_get_collection_notice_email_preview
    ):
        """
        Test preview of collection notice email.
        """
        AutomatedCollectionNoticesSettingsFactory(provider=self.provider)
        mock_html_preview: str = "<html><body>Email preview</body></html>"
        mock_get_collection_notice_email_preview.return_value = (
            mock_html_preview
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = CollectionNoticeEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertTrue(len(response.data.get("html")) > 0)

    def test_api_error_in_previewing_collection_notice_email_when_setting_not_configured(
        self,
    ):
        """
        API should raise error when automated collection notice setting not configured for the Provider.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = CollectionNoticeEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_api_error_in_previewing_collection_notice_email_for_unauthorized_user(
        self,
    ):
        """
        API should raise error for unauthorized access.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider
        )
        view = CollectionNoticeEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)
