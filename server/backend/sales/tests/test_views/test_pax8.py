from sales.views.pax8 import (
    PAX8IntegrationSettingsCreateRetrieveUpdateAPIView,
    PAX8IntegrationValidationAPIView,
    PAX8ProductsListAPIView,
    PAX8VendorsListAPIView,
    PAX8ProductDetailsAPIView,
)

import json
from datetime import datetime, timedelta
from typing import Dict, List
from unittest.mock import patch

from django.db.models import Q
from django.urls import reverse
from factory.faker import faker
from factory.fuzzy import FuzzyChoice
from rest_framework.request import Request
from rest_framework.response import Response
from test_utils import TestClientMixin, BaseProviderTestCase
from faker import Faker
from sales.models import PAX8IntegrationSettings
from sales.tests.factories.sales_factories import (
    PAX8IntegrationSettingsFactory,
    PAX8ProductFactory,
    PAX8ProductPricingInfoFactory,
)
from sales.tests.utils import (
    get_mock_pax8_product_pricing_data,
    add_query_params_to_url,
    get_mock_uuid_value,
)


def create_pax8_integration_settings_for_the_provider(
    provider, vendor_names_list: List[str] = list()
) -> None:
    """
    Creates PAX8 integration settings for the provider.

    Args:
        provider: Provider
        vendor_names_list: List of vendor names.

    Returns:
        None
    """
    PAX8IntegrationSettingsFactory.create(
        provider=provider, vendors=vendor_names_list
    )
    return


class TestPAX8IntegrationSettingsCreateRetrieveUpdateAPIView(
    BaseProviderTestCase
):
    """
    Test view: PAX8IntegrationSettingsCreateRetrieveUpdateAPIView
    """

    pax8_settings_url: str = reverse(
        "api_v1_endpoints:providers:pax8-settings-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_creation_of_pax8_settings(self):
        fake = Faker()
        request_body: Dict = dict(
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=["vendor1"],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pax8_settings_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual(response.data, request_body)

    def test_retrieval_of_pax8_settings(self):
        fake = Faker()
        PAX8IntegrationSettingsFactory(
            provider=self.provider,
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=[fake.company()],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_settings_url,
            self.provider,
            self.user,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)

    def test_update_of_pax8_settings(self):
        fake = Faker()
        PAX8IntegrationSettingsFactory(
            provider=self.provider,
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=[fake.company()],
        )
        request_body: Dict = dict(
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=["vendor1"],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.pax8_settings_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, request_body)

    def test_error_updating_pax8_setting_for_invalid_request_body(self):
        fake = Faker()
        PAX8IntegrationSettingsFactory(
            provider=self.provider,
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=[fake.company()],
        )
        request_body: Dict = dict()
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.pax8_settings_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_error_retrieving_pax8_setting_if_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_settings_url,
            self.provider,
            self.user,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_error_updating_pax8_setting_if_not_configured(self):
        fake = Faker()
        request_body: Dict = dict(
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=["vendor1"],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.pax8_settings_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_error_creating_pax8_setting_if_already_exists(self):
        fake = Faker()
        PAX8IntegrationSettingsFactory(
            provider=self.provider,
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=[fake.company()],
        )
        request_body: Dict = dict(
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
            vendors=["vendor1"],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pax8_settings_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationSettingsCreateRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)


class TestPAX8IntegrationValidationAPIView(BaseProviderTestCase):
    """
    Test view: PAX8IntegrationValidationAPIView
    """

    pax8_validation_url: str = reverse(
        "api_v1_endpoints:providers:validate-pax8-settings"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch("sales.views.pax8.validate_pax8_credentials", return_value=True)
    def test_validation_of_pax8_credentials(
        self, mock_validate_pax8_credentials
    ):
        fake = Faker()
        request_body: Dict = dict(
            client_id=fake.text(max_nb_chars=10),
            client_secret=fake.text(max_nb_chars=10),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pax8_validation_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = PAX8IntegrationValidationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data.get("is_valid"), True)

    def test_error_validating_pax8_credentials_for_invalid_request_body(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.pax8_validation_url,
            self.provider,
            self.user,
            data={},
        )
        view = PAX8IntegrationValidationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)


class TestPAX8ProductsListAPIView(BaseProviderTestCase):
    """
    Test view: PAX8ProductsListAPIView
    """

    pax8_products_list_url: str = reverse(
        "api_v1_endpoints:providers:list-pax8-products"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider,
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_products_list_url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data.get("results")), 1)
        self.assertEqual(
            response.data.get("results")[0].get("product_crm_id"),
            pax8_product_1.crm_id,
        )

    def test_api_error_when_pax8_integration_settings_not_configured_for_the_provider(
        self,
    ):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_products_list_url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_vendor_name_filter_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name: str = "Mock PAX8 vendor"
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name=vendor_name
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_prouct_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_prouct_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url, query_params=dict(vendor=vendor_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_alphabetical_ordering_by_product_name_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="PAX8 A"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="PAX8 B"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_1.id, pricing_info_2.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="product_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_reverse_alphabetical_ordering_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="PAX8 A"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="PAX8 B"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_2.id, pricing_info_1.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="-product_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_alphabetical_ordering_by_vendor_name_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name="Vendor A"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name="Vendor B"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_1.id, pricing_info_2.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="vendor_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_reverse_alphabetical_ordering_by_vendor_name_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name="Vendor A"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name="Vendor B"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_2.id, pricing_info_1.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="-vendor_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_alphabetical_ordering_by_sku_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, sku="SKU-1"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, sku="SKU-2"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_1.id, pricing_info_2.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="sku"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_reverse_alphabetical_ordering_by_sku_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, sku="SKU-1"
        )
        pricing_info_1: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, sku="SKU-2"
        )
        pricing_info_2: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        expected_order: List[int] = [pricing_info_2.id, pricing_info_1.id]
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(ordering="-sku"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_search_by_product_name_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        product_name: str = "Mock PAX8 Product1"
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name=product_name
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider,
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(search=product_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_by_vendor_name_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name: str = "Mock PAX8 Vendor"
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, vendor_name=vendor_name
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider,
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(search=vendor_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_by_short_description_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        short_description: str = "Mock description about PAX81 product."
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, short_description=short_description
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider,
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(search=short_description),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_by_sku_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        sku: str = "SKU-1"
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, sku=sku
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider,
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(search=sku),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_include_filter_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A101 for commerce use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A102 for non profit use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(include="commerce"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_exclude_filter_on_pax8_products_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A101 for commerce use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A102 for commerce use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(exclude="commerce"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 0)

    def test_include_and_exclude_filter_on_pax8_product_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        pax8_product_1: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A101 for commerce and non profit use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_1,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        pax8_product_2: PAX8ProductFactory = PAX8ProductFactory(
            provider=self.provider, name="A102 for commerce use"
        )
        PAX8ProductPricingInfoFactory(
            provider=self.provider,
            pax8_product=pax8_product_2,
            pricing_data=get_mock_pax8_product_pricing_data(),
        )
        url: str = add_query_params_to_url(
            self.pax8_products_list_url,
            query_params=dict(exclude="non profit", include="commerce"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)


class TestPAX8VendorsListAPIView(BaseProviderTestCase):
    """
    Test view: PAX8VendorsListAPIView
    """

    pax8_vendors_list_url: str = reverse(
        "api_v1_endpoints:providers:list-pax8-vendors"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_pax8_vendors_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name_1: str = "Mock vendor 1"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_1)
        vendor_name_2: str = "Mock vendor 2"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_2)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_vendors_list_url,
            self.provider,
            self.user,
        )
        view = PAX8VendorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 2)

    def test_api_error_listing_vendors_if_pax8_integration_settings_not_configured(
        self,
    ):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.pax8_vendors_list_url,
            self.provider,
            self.user,
        )
        view = PAX8VendorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_search_by_vendor_name_on_pax8_vendors_listing(self):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name: str = "Mock vendor 1"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name)
        PAX8ProductFactory(provider=self.provider)
        url: str = add_query_params_to_url(
            self.pax8_vendors_list_url, query_params=dict(search=vendor_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8VendorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_alphabetical_ordering_by_vendor_name_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name_1: str = "Mock vendor A"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_1)
        vendor_name_2: str = "Mock vendor B"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_2)
        url: str = add_query_params_to_url(
            self.pax8_vendors_list_url,
            query_params=dict(ordering="vendor_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8VendorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(
            response.data.get("results")[0].get("vendor_name"), vendor_name_1
        )

    def test_reverse_alphabetical_ordering_by_vendor_name_on_pax8_products_listing(
        self,
    ):
        create_pax8_integration_settings_for_the_provider(self.provider)
        vendor_name_1: str = "Mock vendor A"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_1)
        vendor_name_2: str = "Mock vendor B"
        PAX8ProductFactory(provider=self.provider, vendor_name=vendor_name_2)
        url: str = add_query_params_to_url(
            self.pax8_vendors_list_url,
            query_params=dict(ordering="-vendor_name"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8VendorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(
            response.data.get("results")[0].get("vendor_name"), vendor_name_2
        )


class TestPAX8ProductDetailsAPIView(BaseProviderTestCase):
    """
    Test view: PAX8ProductDetailsAPIView
    """

    pax8_product_details_endpoint_name: str = (
        "api_v1_endpoints:providers:pax8-product-details"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_get_pax8_product_details(self):
        product_info: PAX8ProductPricingInfo = PAX8ProductPricingInfoFactory(
            provider=self.provider
        )
        url: str = reverse(
            self.pax8_product_details_endpoint_name,
            kwargs=dict(crm_id=product_info.pax8_product.crm_id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductDetailsAPIView().as_view()
        response: Response = view(
            request, crm_id=product_info.pax8_product.crm_id
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get("id"), product_info.id)

    def test_error_when_product_does_not_exist_in_db(self):
        mock_crm_id: str = get_mock_uuid_value()
        url: str = reverse(
            self.pax8_product_details_endpoint_name,
            kwargs=dict(crm_id=mock_crm_id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductDetailsAPIView().as_view()
        response: Response = view(request, crm_id=mock_crm_id)
        self.assertEqual(response.status_code, 404)

    def test_error_when_invalid_crm_id_is_passed_in_url_path(self):
        mock_crm_id: str = "absc-234234-dfasdfac"
        url: str = reverse(
            self.pax8_product_details_endpoint_name,
            kwargs=dict(crm_id=mock_crm_id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PAX8ProductDetailsAPIView().as_view()
        response: Response = view(request, crm_id=mock_crm_id)
        self.assertEqual(response.status_code, 400)
