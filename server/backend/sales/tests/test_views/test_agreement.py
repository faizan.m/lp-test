from datetime import datetime, timedelta
from typing import Dict
from unittest.mock import patch

from django.db.models import Q
from django.urls import reverse
from factory.faker import faker
from faker import Factory
from rest_framework.request import Request
from rest_framework.response import Response

from accounts.tests.factories import (
    CustomerFactory,
    ProviderFactory,
)
from sales.models import Agreement, AgreementMarginSettings
from sales.tests.factories import (
    AgreementFactory,
    AgreementTemplateFactory,
    AgreementMarginSettingsFactory,
)
from sales.tests.utils import (
    get_sample_products_data,
    get_sample_sections_data,
    create_versions_of_agreement,
    add_query_params_to_url,
    get_invalid_request_payload,
    get_mock_pax8_products_data_for_model,
)
from sales.views import (
    AgreementListCreateAPIView,
    AgreementRetrieveUpdateDestroyAPIView,
    AgreementMarginSettingsListCreateAPIView,
    AgreementMarginSettingsRetrieveAPIView,
    AgreementVersioningAPIView,
    AgreementPDFDownloadAPIView,
    AgreementsAuditLogsListAPIView,
    SendAgreementToAccountManagerAPIView,
    SalesAgreementAuthorsListAPIView,
)
from test_utils import TestClientMixin, BaseProviderTestCase
from accounts.tests.factories import ProviderUserFactory

# The following import is required to mock service class calls,
# otherwise tests will fail
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
)

faker = Factory.create()


class TestAgreement(BaseProviderTestCase):
    url = reverse("api_v1_endpoints:providers:list-create-agreements")
    versions_listing_url: str = (
        "api_v1_endpoints:providers:agreement-version-history-view"
    )
    agreement_preview_url = reverse(
        "api_v1_endpoints:providers:agreement-preview"
    )
    agreement_history_url = reverse(
        "api_v1_endpoints:providers:agreements-audit-logs"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.template = AgreementTemplateFactory()
        cls.setup_provider_and_user()
        cls.agreement = AgreementFactory(
            provider=cls.provider, customer=cls.customer, user=cls.user
        )

    @patch(
        "sales.services.agreement_template_service.AgreementService.create_agreement"
    )
    def test_creation_of_agreement(self, create_agreement):
        agreement = AgreementFactory.build()
        agreement_name: str = "Mock agreement name"
        data: Dict = dict(
            name=agreement_name,
            template=self.template.id,
            customer=self.customer.id,
            user=self.user.id,
            quote_id=agreement.quote_id,
            customer_cost=agreement.customer_cost,
            margin=agreement.margin,
            margin_percentage=agreement.margin_percentage,
            internal_cost=agreement.internal_cost,
            sections=get_sample_sections_data(),
            products=get_sample_products_data(),
            version_description=agreement.version_description,
            discount_percentage=agreement.discount_percentage,
            total_discount=agreement.total_discount,
            pax8_products=agreement.pax8_products,
        )
        create_agreement.return_value = agreement
        request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=data
        )
        view = AgreementListCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(201, response.status_code)
        create_agreement.assert_called()

    def test_get_agreement(self):
        request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = AgreementRetrieveUpdateDestroyAPIView.as_view()
        response = view(request, pk=self.agreement.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(self.agreement.id, response.data.get("id"))
        self.assertEqual(self.agreement.name, response.data.get("name"))

    @patch(
        "sales.services.agreement_template_service.AgreementService.update_agreement"
    )
    def test_update_agreement(self, update_agreement):
        updated_payload: Dict = dict(
            name="Updated agreement",
            template=self.template.id,
            customer=self.customer.id,
            user=self.user.id,
            quote_id=self.agreement.quote_id,
            customer_cost=self.agreement.customer_cost,
            margin=self.agreement.margin,
            margin_percentage=self.agreement.margin_percentage,
            internal_cost=self.agreement.internal_cost,
            sections=get_sample_sections_data(),
            products=get_sample_products_data(),
            version_description=self.agreement.version_description,
            discount_percentage=self.agreement.discount_percentage,
            total_discount=self.agreement.total_discount,
            pax8_products=self.agreement.pax8_products,
        )
        update_agreement.return_value = AgreementFactory.build()
        request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=updated_payload
        )
        view = AgreementRetrieveUpdateDestroyAPIView.as_view()
        response = view(request, pk=self.agreement.id)
        self.assertEqual(200, response.status_code)
        update_agreement.assert_called()

    def test_delete_agreement(self):
        agreement = AgreementFactory()
        request = TestClientMixin.api_factory_client_request(
            "DELETE", self.url, agreement.provider, self.user
        )
        view = AgreementRetrieveUpdateDestroyAPIView.as_view()
        response = view(request, pk=agreement.id)
        self.assertEqual(204, response.status_code)

    @patch(
        "sales.services.agreement_template_service.AgreementService.update_agreement"
    )
    def test_agreement_rollback(self, update_agreement):
        """
        Method to test rollback of agreement.
        """
        # Consider a scenario where we have at least 2 versions
        # of agreement for rollback
        (
            first_version_agreement_id,
            second_version_agreement_id,
            root_agreement_id,
        ) = create_versions_of_agreement(self)
        rollback_request_body: Dict = dict(
            name="Rollback agreement",
            template=self.template.id,
            customer=self.customer.id,
            user=self.user.id,
            quote_id=self.agreement.quote_id,
            customer_cost=self.agreement.customer_cost,
            margin=self.agreement.margin,
            margin_percentage=self.agreement.margin_percentage,
            internal_cost=self.agreement.internal_cost,
            sections=get_sample_sections_data(),
            products=get_sample_products_data(),
            version_description=self.agreement.version_description,
            discount_percentage=self.agreement.discount_percentage,
            total_discount=self.agreement.total_discount,
            pax8_products=dict(),
        )
        update_agreement.return_value = AgreementFactory(
            name=rollback_request_body.get("name"),
            template_id=rollback_request_body.get("template"),
            customer_id=rollback_request_body.get("customer"),
            user_id=rollback_request_body.get("user"),
            quote_id=rollback_request_body.get("quote_id"),
            customer_cost=rollback_request_body.get("customer_cost"),
            margin=rollback_request_body.get("margin"),
            margin_percentage=rollback_request_body.get("margin_percentage"),
            internal_cost=rollback_request_body.get("internal_cost"),
            sections=rollback_request_body.get("sections"),
            products=rollback_request_body.get("products"),
            version_description=rollback_request_body.get(
                "version_description"
            ),
            discount_percentage=rollback_request_body.get(
                "discount_percentage"
            ),
            total_discount=rollback_request_body.get("total_discount"),
            pax8_products=dict(),
            root_agreement_id=root_agreement_id,
        )
        view = AgreementRetrieveUpdateDestroyAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url,
            object_id=second_version_agreement_id,
            query_params=dict(rollback="true"),
        )
        request = TestClientMixin.api_factory_client_request(
            "PUT",
            request_url,
            self.provider,
            self.user,
            data=rollback_request_body,
        )
        response = view(request, pk=second_version_agreement_id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data.get("is_current_version"), True)
        self.assertEqual(
            response.data.get("root_agreement"), root_agreement_id
        )
        self.assertEqual(
            response.data.get("name"),
            rollback_request_body.get("name"),
        )

    def test_listing_of_agreement_versions(self):
        """
        Method to list all versions of agreement
        """
        # Consider a scenario where we have multiple versions of agreements
        (
            first_version_agreement_id,
            second_version_agreement_id,
            root_agreement_id,
        ) = create_versions_of_agreement(self)
        root_agreement_id: int = root_agreement_id
        agreement_versions_count: int = Agreement.objects.filter(
            Q(root_agreement_id=root_agreement_id) | Q(id=root_agreement_id),
            provider_id=self.provider.id,
        ).count()
        view = AgreementVersioningAPIView().as_view()
        list_request = TestClientMixin.api_factory_client_request(
            "GET",
            reverse(
                self.versions_listing_url, args=[second_version_agreement_id]
            ),
            self.provider,
            self.user,
        )
        response = view(list_request, pk=second_version_agreement_id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            len(response.data.get("results")), agreement_versions_count
        )

    def test_should_give_error_for_unauthorized_request(self):
        request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider
        )
        view = AgreementListCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(401, response.status_code)

    def test_provider_filter(self):
        request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = AgreementListCreateAPIView.as_view()
        response = view(request)
        agreements_count = Agreement.objects.filter(
            provider=self.provider
        ).count()
        self.assertEqual(agreements_count, len(response.data["results"]))

    def test_customer_filter(self):
        customer_2 = CustomerFactory(provider=self.provider)
        AgreementFactory(provider=self.provider, customer=customer_2)
        view = AgreementListCreateAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url,
            query_params=dict(
                customer=customer_2.id,
            ),
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            1,
            len(response.data.get("results")),
        )

    def test_created_on_date_range_filter(self):
        """
        Method to test date range filters
        """
        end_date: datetime.date = datetime.now().date()
        start_date: datetime.date = end_date - timedelta(days=30)
        AgreementFactory.create_batch(
            3,
            provider=self.template.provider,
            created_on=faker.date_time_between(start_date, end_date),
            updated_on=faker.date_time_between(start_date, end_date),
        )
        # Filter documents for last 15 days
        created_after: datetime.date = start_date + timedelta(days=15)
        created_before: datetime.date = end_date + timedelta(days=1)
        filtered_agreement_by_date_range = Agreement.objects.filter(
            provider=self.template.provider,
            is_current_version=True,
            created_on__date__range=[created_after, created_before],
        )
        filtered_agreement_by_date_range_count: int = (
            filtered_agreement_by_date_range.count()
        )
        view = AgreementListCreateAPIView().as_view()
        url = add_query_params_to_url(
            self.url,
            query_params=dict(
                created_after=created_after.strftime("%Y-%m-%d"),
                created_before=created_before.strftime("%Y-%m-%d"),
            ),
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", url, self.template.provider, self.user
        )
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            filtered_agreement_by_date_range_count,
            len(response.data.get("results")),
        )

    def test_updated_on_date_range_filter(self):
        """
        Method to test date range filters
        """
        end_date: datetime.date = datetime.now().date()
        start_date: datetime.date = end_date - timedelta(days=30)
        AgreementFactory.create_batch(
            3,
            provider=self.template.provider,
            updated_on=faker.date_time_between(start_date, end_date),
        )
        # Filter documents for last 15 days
        updated_after: datetime.date = start_date + timedelta(days=15)
        updated_before: datetime.date = end_date + timedelta(days=1)
        filtered_agreement_by_date_range = Agreement.objects.filter(
            provider=self.template.provider,
            is_current_version=True,
            updated_on__date__range=[updated_after, updated_before],
        )
        filtered_agreement_by_date_range_count: int = (
            filtered_agreement_by_date_range.count()
        )
        view = AgreementListCreateAPIView().as_view()
        url = add_query_params_to_url(
            self.url,
            query_params=dict(
                updated_after=updated_after.strftime("%Y-%m-%d"),
                updated_before=updated_before.strftime("%Y-%m-%d"),
            ),
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", url, self.template.provider, self.user
        )
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            filtered_agreement_by_date_range_count,
            len(response.data.get("results")),
        )

    def test_author_filter(self):
        author_1: User = ProviderUserFactory(
            provider=self.provider, first_name="Michael", last_name="Jackson"
        )
        AgreementFactory.create(
            provider=self.provider, author=author_1, is_current_version=True
        )
        author_2: User = ProviderUserFactory(
            provider=self.provider, first_name="Lady", last_name="Gaga"
        )
        AgreementFactory.create(
            provider=self.provider, author=author_2, is_current_version=True
        )
        view = AgreementListCreateAPIView().as_view()
        url: str = add_query_params_to_url(
            self.url,
            query_params=dict(author_id=str(author_1.id)),
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        response: Response = view(request)
        self.assertEqual(
            1,
            len(response.data.get("results")),
        )

    @patch(
        "sales.tests.test_views.test_agreement.AgreementTemplatePreviewService.generate_agreement_pdf_preview",
        return_value=dict(
            file_path="https://file-storage.com/file_name",
            file_name="sample preview.pdf",
        ),
    )
    def test_agreement_pdf_preview_for_existing_agreement(
        self, generate_agreement_pdf_preview
    ):
        """
        Method to test existing agreement PDF preview
        """
        view = AgreementPDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.agreement_preview_url,
            self.provider,
            self.user,
            data=dict(
                existing_agreement_preview=True, agreement_id=self.agreement.id
            ),
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        generate_agreement_pdf_preview.assert_called()

    def test_failure_of_pdf_preview_for_existing_agreement_when_request_is_invalid(
        self,
    ):
        """
        Method to test existing agreement PDF preview
        """
        view = AgreementPDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.agreement_preview_url,
            self.provider,
            self.user,
            data=dict(existing_agreement_preview=True),
        )
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    @patch(
        "sales.tests.test_views.test_agreement.AgreementTemplatePreviewService.generate_agreement_pdf_preview",
        return_value=dict(
            file_path="https://file-storage.com/file_name",
            file_name="sample preview.pdf",
        ),
    )
    def test_pdf_preview_before_agreement_creation(
        self, generate_agreement_pdf_preview
    ):
        """
        Method to test PDF preview of agreement before creation
        """
        agreement = AgreementFactory.build()
        data: Dict = dict(
            name="Mock agreement name",
            customer=self.customer.id,
            sections=get_sample_sections_data(),
            products=get_sample_products_data(),
            version_description=agreement.version_description,
            discount_percentage=agreement.discount_percentage,
            minor_version=agreement.minor_version,
            major_version=agreement.major_version,
            user=self.user.id,
        )
        view = AgreementPDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.agreement_preview_url,
            self.provider,
            self.user,
            data=dict(
                pre_agreement_create_preview=True, agreement_payload=data
            ),
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        generate_agreement_pdf_preview.assert_called()

    def test_failure_of_pdf_preview_before_agreement_creation_for_invalid_request(
        self,
    ):
        """
        Method to test failure of Agreement PDF preview for invalid request body
        """
        view = AgreementPDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.agreement_preview_url,
            self.provider,
            self.user,
            data=get_invalid_request_payload(),
        )
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    @patch(
        "sales.services.agreement_template_service.AgreementService.create_forecast_for_products"
    )
    def test_agreement_history(self, create_forecast_for_products):
        create_forecast_for_products.return_value = dict(
            forecast_id=100,
            forecast_error_details=dict(
                error_in_forecast_creation=False,
                error_message=None,
            ),
        )
        agreement: Agreement = AgreementFactory.build()
        data: Dict = dict(
            name="Mock agreement name",
            template=self.template.id,
            customer=self.customer.id,
            user=self.user.id,
            quote_id=agreement.quote_id,
            customer_cost=agreement.customer_cost,
            margin=agreement.margin,
            margin_percentage=agreement.margin_percentage,
            internal_cost=agreement.internal_cost,
            sections=get_sample_sections_data(),
            products=get_sample_products_data(),
            version_description=agreement.version_description,
            discount_percentage=agreement.discount_percentage,
            total_discount=agreement.total_discount,
            pax8_products=dict(),
        )
        request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=data
        )
        view = AgreementListCreateAPIView.as_view()
        response = view(request)
        data["name"] = "Updated Agreement"
        request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=data
        )
        view = AgreementRetrieveUpdateDestroyAPIView.as_view()
        view(request, pk=response.data.get("id"))
        request = TestClientMixin.api_factory_client_request(
            "GET", self.agreement_history_url, self.provider, self.user
        )
        view = AgreementsAuditLogsListAPIView.as_view()
        response = view(request)
        self.assertEqual(200, response.status_code)


class TestAgreementMarginSettings(BaseProviderTestCase):
    url = reverse(
        "api_v1_endpoints:providers:agreement-margin-settings-list-create"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.agreement_margin_settings = AgreementMarginSettingsFactory(
            provider=cls.provider
        )

    def test_create_agreement_margin_settings(self):
        provider = ProviderFactory()
        data = {"margin_limit": 30}
        request = TestClientMixin.api_factory_client_request(
            "POST", self.url, provider, self.user, data=data
        )
        view = AgreementMarginSettingsListCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(201, response.status_code)

    def test_get_agreement_margin_settings(self):
        request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = AgreementMarginSettingsListCreateAPIView.as_view()
        response = view(request)
        agreement_margin_settings_count = (
            AgreementMarginSettings.objects.filter(
                provider=self.provider
            ).count()
        )
        self.assertEqual(
            agreement_margin_settings_count, len(response.data["results"])
        )

    def test_get_agreement_margin_settings_by_id(self):
        request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = AgreementMarginSettingsRetrieveAPIView.as_view()
        response = view(request, pk=self.agreement_margin_settings.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            self.agreement_margin_settings.id, response.data.get("id")
        )
        self.assertEqual(
            self.agreement_margin_settings.margin_limit,
            response.data.get("margin_limit"),
        )

    def test_update_agreement_margin_settings_by_id(self):
        updated_payload = {
            "margin_limit": faker.pyint(),
        }
        request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=updated_payload
        )
        view = AgreementMarginSettingsRetrieveAPIView.as_view()
        response = view(request, pk=self.agreement_margin_settings.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            updated_payload.get("margin_limit"),
            response.data.get("margin_limit"),
        )
        self.assertEqual(
            self.agreement_margin_settings.id, response.data.get("id")
        )

    def test_create_duplicate_margin_settings_for_provider(self):
        data = {"margin_limit": self.agreement_margin_settings.margin_limit}
        request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=data
        )
        view = AgreementMarginSettingsListCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(400, response.status_code)


class TestSendAgreementToAccountManagerAPIView(BaseProviderTestCase):
    """
    Test the view: SendAgreementToAccountManagerAPIView
    """

    send_to_account_manager_endpoint = (
        "api_v1_endpoints:providers:send-agreement-to-account-manager"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "sales.views.agreement.send_agreement_to_account_manager.apply_async"
    )
    def test_sending_agreement_to_account_manager(
        self, mock_send_agreement_to_account_manager
    ):
        agreement: Agreement = AgreementFactory(provider=self.provider)
        url: str = reverse(
            self.send_to_account_manager_endpoint,
            kwargs=dict(agreement_id=agreement.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", url, self.provider, self.user
        )
        view = SendAgreementToAccountManagerAPIView().as_view()
        response: Response = view(request, agreement_id=agreement.id)
        mock_send_agreement_to_account_manager.assert_called_with(
            (agreement.id, self.provider.id, self.user.id), queue="high"
        )
        self.assertEqual(response.status_code, 200)

    def test_error_sending_previous_version_agreement_to_account_manager(self):
        agreement: Agreement = AgreementFactory(
            provider=self.provider, is_current_version=False
        )
        url: str = reverse(
            self.send_to_account_manager_endpoint,
            kwargs=dict(agreement_id=agreement.id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", url, self.provider, self.user
        )
        view = SendAgreementToAccountManagerAPIView().as_view()
        response: Response = view(request, agreement_id=agreement.id)
        self.assertEqual(response.status_code, 404)


class TestSalesAgreementAuthorsListAPIView(BaseProviderTestCase):
    """
    Test view: SalesAgreementAuthorsListAPIView
    """

    authors_list_url: str = reverse(
        "api_v1_endpoints:providers:agreement-authors-list"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_agreement_authors(self):
        author_1: User = ProviderUserFactory(
            provider=self.provider, first_name="Johny", last_name="Depp"
        )
        AgreementFactory.create(provider=self.provider, author=author_1)
        AgreementFactory.create(provider=self.provider, author=author_1)
        author_2: User = ProviderUserFactory(
            provider=self.provider, first_name="Alain", last_name="Delon"
        )
        AgreementFactory.create(provider=self.provider, author=author_2)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.authors_list_url, self.provider, self.user
        )
        view = SalesAgreementAuthorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(2, len(response.data))
