import json
from datetime import datetime, timedelta
from typing import Dict, List
from unittest.mock import patch

from django.db.models import Q
from django.urls import reverse
from factory.faker import faker
from factory.fuzzy import FuzzyChoice
from rest_framework.request import Request
from rest_framework.response import Response

from accounts.tests.factories import ProviderUserFactory
from sales.models import AgreementTemplate
from sales.tests.factories import AgreementTemplateFactory
from sales.tests.utils import (
    render_response,
    add_query_params_to_url,
    get_valid_request_payload,
    get_invalid_request_payload,
    create_versions_of_template,
)
from sales.views import (
    SalesAgreementTemplateListCreateAPIView,
    SalesAgreementTemplateRetrieveUpdateDestroyAPIView,
    SalesAgreementTemplateVersioningAPIView,
    AgreementTemplateDropdownVersionListing,
    SalesAgreementTemplatePDFDownloadAPIView,
    AgreementTemplateAuditLogsListAPIView,
    SalesAgreementTemplateAuthorsListAPIView,
)
from test_utils import TestClientMixin, BaseProviderTestCase

# The following import is required to mock service class calls,
# otherwise tests will fail
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
)


class TestAgreementTemplatesAPIs(BaseProviderTestCase):
    url = reverse(
        "api_v1_endpoints:providers:agreement-template-list-create-view"
    )
    version_history_url: str = (
        "api_v1_endpoints:providers:agreement-template-version-history-view"
    )
    versions_dropdown_listing_url: str = "api_v1_endpoints:providers:agreement-template-dropdown-versions-listing-view"
    template_preview_url = reverse(
        "api_v1_endpoints:providers:agreement-template-preview"
    )
    template_history_url = reverse(
        "api_v1_endpoints:providers:agreement-templates-audit-log"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_template_creation_for_valid_request_body(self):
        """
        Method to test Agreement Template creation.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=get_valid_request_payload(),
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(201, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(response_data.get("version"), "1.0")
        self.assertEqual(response_data.get("is_current_version"), True)
        self.assertEqual(
            len(response_data.get("pax8_products").get("products")), 1
        )

    def test_templates_listing(self):
        """
        Method to test Agreement Templates listing.
        """
        fake = faker.Faker()
        AgreementTemplateFactory.create_batch(
            5,
            provider=self.provider,
            is_current_version=fake.pybool(),
            is_disabled=fake.pybool(),
        )
        agreement_templates_count: int = AgreementTemplate.objects.filter(
            provider=self.provider, is_current_version=True, is_disabled=False
        ).count()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider, self.user
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        response: Response = render_response(view(request))
        response_data: Dict = json.loads(response.content)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            agreement_templates_count, len(response_data.get("results"))
        )

    def test_template_update_for_valid_request_body(self):
        """
        Method to test update of existing agreement template.
        """
        existing_template: AgreementTemplate = AgreementTemplateFactory(
            provider=self.provider, author=self.user, updated_by=self.user
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.url,
            self.provider,
            self.user,
            data=get_valid_request_payload(),
        )
        view = SalesAgreementTemplateRetrieveUpdateDestroyAPIView().as_view()
        response: Response = render_response(
            view(request, template_id=existing_template.id)
        )
        response_data: Dict = json.loads(response.content)
        existing_template.refresh_from_db()
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            response_data.get("version"),
            f"{existing_template.major_version}.{existing_template.minor_version+1}",
        )
        self.assertEqual(existing_template.is_current_version, False)
        self.assertEqual(response_data.get("is_current_version"), True)
        self.assertEqual(
            response_data.get("root_template"),
            existing_template.get_root_template_id(),
        )
        self.assertEqual(
            response_data.get("parent_template"), existing_template.id
        )

    def test_template_rollback_for_valid_request_body(self):
        """
        Method to test rollback of a template.
        """
        # Consider a scenario where we have at least 2 versions
        # of a template for rollback
        (
            first_version_template_id,
            second_version_template_id,
            root_template_id,
        ) = create_versions_of_template(self)
        rollback_request_body: Dict = get_valid_request_payload()
        view = SalesAgreementTemplateRetrieveUpdateDestroyAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url,
            object_id=second_version_template_id,
            query_params=dict(rollback="true"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            request_url,
            self.provider,
            self.user,
            data=rollback_request_body,
        )
        response: Response = render_response(
            view(request, template_id=second_version_template_id)
        )
        self.assertEqual(200, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(response_data.get("is_current_version"), True)
        self.assertEqual(response_data.get("root_template"), root_template_id)
        self.assertEqual(
            response_data.get("name"),
            rollback_request_body.get("name"),
        )

    def test_template_deletion_for_valid_template(self):
        """
        Method to test deletion of template
        """
        (
            first_version_template_id,
            second_version_template_id,
            root_template_id,
        ) = create_versions_of_template(self)
        view = SalesAgreementTemplateRetrieveUpdateDestroyAPIView().as_view()
        delete_request: Request = TestClientMixin.api_factory_client_request(
            "DELETE", self.url, self.provider, self.user
        )
        delete_response: Response = render_response(
            view(delete_request, template_id=second_version_template_id)
        )
        self.assertEqual(204, delete_response.status_code)
        template_is_disabled_statuses: List[bool] = list(
            (
                AgreementTemplate.objects.filter(
                    provider_id=self.provider.id,
                    id__in=[
                        first_version_template_id,
                        second_version_template_id,
                    ],
                ).values_list("is_disabled", flat=True)
            )
        )
        self.assertEqual(all(template_is_disabled_statuses), True)

    def test_listing_of_template_versions(self):
        """
        Method to list all versions of a template
        """
        # Consider a scenario where we have multiple versions of a template
        (
            first_version_template_id,
            second_version_template_id,
            root_template_id,
        ) = create_versions_of_template(self)
        root_template_id: int = root_template_id
        template_versions_count: int = AgreementTemplate.objects.filter(
            Q(root_template_id=root_template_id) | Q(id=root_template_id),
            provider_id=self.provider.id,
        ).count()
        view = SalesAgreementTemplateVersioningAPIView().as_view()
        list_request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            reverse(
                self.version_history_url, args=[second_version_template_id]
            ),
            self.provider,
            self.user,
        )
        response: Response = render_response(
            view(list_request, template_id=second_version_template_id)
        )
        self.assertEqual(200, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(
            len(response_data.get("results")), template_versions_count
        )

    def test_listing_of_template_dropdown_versions(self):
        """
        Method to list all versions of a template
        """
        # Consider a scenario where we have multiple versions of a template
        (
            first_version_template_id,
            second_version_template_id,
            root_template_id,
        ) = create_versions_of_template(self)
        template_versions_count: int = AgreementTemplate.objects.filter(
            provider=self.provider,
            is_disabled=False,
            is_current_version=True,
        ).count()
        view = AgreementTemplateDropdownVersionListing().as_view()
        list_request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            reverse(self.versions_dropdown_listing_url),
            self.provider,
            self.user,
        )
        response = view(list_request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(response.data), template_versions_count)

    def test_filtering_by_template_name(self):
        """
        Method to test name filter
        """
        fake = faker.Faker()
        document_name_choices: List = [
            "Document A",
            "Document B",
        ]
        AgreementTemplateFactory.create_batch(
            5,
            name=FuzzyChoice(choices=document_name_choices),
            provider=self.provider,
            is_current_version=fake.pybool(),
        )
        template_name_to_filter: str = document_name_choices[0]
        filtered_templates_by_name_count = AgreementTemplate.objects.filter(
            provider=self.provider,
            name=template_name_to_filter,
            is_current_version=True,
        ).count()
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url,
            query_params=dict(
                name=template_name_to_filter,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(
            filtered_templates_by_name_count,
            len(response_data.get("results")),
        )

    def test_filtering_by_created_on_date_range(self):
        """
        Method to test date range filters
        """
        fake = faker.Faker()
        end_date: datetime.date = datetime.now().date()
        start_date: datetime.date = end_date - timedelta(days=30)
        AgreementTemplateFactory.create_batch(
            3,
            provider=self.provider,
            created_on=fake.date_time_between(start_date, end_date),
            updated_on=fake.date_time_between(start_date, end_date),
        )
        # Filter documents for last 15 days
        created_after: datetime.date = start_date + timedelta(days=15)
        created_before: datetime.date = end_date + timedelta(days=1)
        filtered_template_by_date_range = AgreementTemplate.objects.filter(
            provider=self.provider,
            is_current_version=True,
            created_on__date__range=[created_after, created_before],
        )
        filtered_template_by_date_range_count: int = (
            filtered_template_by_date_range.count()
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request_url = add_query_params_to_url(
            self.url,
            query_params=dict(
                created_after=created_after.strftime("%Y-%m-%d"),
                created_before=created_before.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        response_data = json.loads(response.content)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            filtered_template_by_date_range_count,
            len(response_data.get("results")),
        )

    def test_filtering_by_updated_on_date_range(self):
        """
        Method to test date range filters
        """
        fake = faker.Faker()
        end_date: datetime.date = datetime.now().date()
        start_date: datetime.date = end_date - timedelta(days=30)
        AgreementTemplateFactory.create_batch(
            3,
            provider=self.provider,
            updated_on=fake.date_time_between(start_date, end_date),
        )
        # Filter documents for last 15 days
        updated_after: datetime.date = start_date + timedelta(days=15)
        updated_before: datetime.date = end_date + timedelta(days=1)
        filtered_agreement_template_by_date_range = (
            AgreementTemplate.objects.filter(
                provider=self.provider,
                is_current_version=True,
                updated_on__date__range=[updated_after, updated_before],
            )
        )
        filtered_agreement_template_by_date_range_count: int = (
            filtered_agreement_template_by_date_range.count()
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request_url = add_query_params_to_url(
            self.url,
            query_params=dict(
                updated_after=updated_after.strftime("%Y-%m-%d"),
                updated_before=updated_before.strftime("%Y-%m-%d"),
            ),
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            filtered_agreement_template_by_date_range_count,
            len(response.data.get("results")),
        )

    def test_ordering_by_template_name(self):
        """
        Method to test ordering on template listing endpoint
        """
        # Test both ascending and  descending ordering
        AgreementTemplateFactory.create_batch(
            3, provider=self.provider, author=self.user, updated_by=self.user
        )
        templates = AgreementTemplate.objects.filter(
            provider=self.provider, is_current_version=True
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        # Test ordering by name in ascending order
        order_by_name_ascending = templates.order_by("name")
        request_url = add_query_params_to_url(
            self.url, query_params=dict(ordering="name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.content)
        response_template_ids: List = [
            template.get("id") for template in response_data.get("results")
        ]
        self.assertListEqual(
            response_template_ids,
            list(order_by_name_ascending.values_list("id", flat=True)),
        )
        # Test ordering by name in descending order
        order_by_name_descending = templates.order_by("-name")
        request_url = add_query_params_to_url(
            self.url, query_params=dict(ordering="-name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.content)
        response_template_ids: List = [
            template.get("id") for template in response_data.get("results")
        ]
        self.assertListEqual(
            response_template_ids,
            list(order_by_name_descending.values_list("id", flat=True)),
        )

    def test_ordering_by_created_on(self):
        """
        Method to test ordering by created_on
        """
        AgreementTemplateFactory.create_batch(
            3, provider=self.provider, author=self.user, updated_by=self.user
        )
        templates = AgreementTemplate.objects.filter(
            provider=self.provider, is_current_version=True
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        # Test ordering by created_on in ascending order
        order_by_created_on_ascending = templates.order_by("created_on")
        request_url = add_query_params_to_url(
            self.url, query_params=dict(ordering="created_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.content)
        response_template_ids: List = [
            template.get("id") for template in response_data.get("results")
        ]
        self.assertListEqual(
            response_template_ids,
            list(order_by_created_on_ascending.values_list("id", flat=True)),
        )
        # Test ordering by created_on in descending order
        order_by_created_on_descending = templates.order_by("-created_on")
        request_url = add_query_params_to_url(
            self.url, query_params=dict(ordering="-created_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data = json.loads(response.content)
        response_template_ids: List = [
            template.get("id") for template in response_data.get("results")
        ]
        self.assertListEqual(
            response_template_ids,
            list(order_by_created_on_descending.values_list("id", flat=True)),
        )

    def test_searching_by_template_name(self):
        """
        Method to test search on template name
        """
        name_choices: List = ["Template A", "Statement", "Official"]
        AgreementTemplateFactory.create_batch(
            5,
            name=FuzzyChoice(choices=name_choices),
            provider=self.provider,
        )
        # Test search for template name
        queryset_for_search_by_name = AgreementTemplate.objects.filter(
            name=name_choices[0], provider=self.provider
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url, query_params=dict(search="Template A")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(
            queryset_for_search_by_name.count(),
            len(response_data.get("results")),
        )

    def test_searching_by_author_name(self):
        """
        Method to test search on author name
        """
        AgreementTemplateFactory.create_batch(
            4,
            provider=self.provider,
            author=FuzzyChoice(choices=[self.user, ProviderUserFactory()]),
        )
        # Test search for author first name
        queryset_for_search_by_author_first_name = (
            AgreementTemplate.objects.filter(
                provider=self.provider, author=self.user
            )
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request_url: str = add_query_params_to_url(
            self.url, query_params=dict(search=self.user.name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", request_url, self.provider, self.user
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        response_data: Dict = json.loads(response.content)
        self.assertEqual(
            queryset_for_search_by_author_first_name.count(),
            len(response_data.get("results")),
        )

    def test_agreement_templates_history(self):
        data = get_valid_request_payload()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=data,
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(201, response.status_code)
        response_data: Dict = json.loads(response.content)
        data["name"] = "Updated Template"
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.url,
            self.provider,
            self.user,
            data=data,
        )
        view = SalesAgreementTemplateRetrieveUpdateDestroyAPIView().as_view()
        response: Response = render_response(
            view(request, template_id=response_data.get("id"))
        )
        request = TestClientMixin.api_factory_client_request(
            "GET", self.template_history_url, self.provider, self.user
        )
        view = AgreementTemplateAuditLogsListAPIView.as_view()
        response = view(request)
        self.assertEqual(200, response.status_code)

    def test_failure_of_template_creation_for_invalid_request_body(self):
        """
        Method to test failure to create new template in case of
        invalid request body
        """
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=get_invalid_request_payload(),
        )
        response: Response = render_response(view(request))
        self.assertEqual(400, response.status_code)

    def test_failure_of_template_update_for_invalid_request_body(self):
        """
        Method to test failure of template update in case of
        invalid request body
        """
        existing_template: AgreementTemplate = AgreementTemplateFactory(
            provider=self.provider, author=self.user, updated_by=self.user
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.url,
            self.provider,
            self.user,
            data=get_invalid_request_payload(),
        )
        view = SalesAgreementTemplateRetrieveUpdateDestroyAPIView().as_view()
        response: Response = render_response(
            view(request, template_id=existing_template.id)
        )
        self.assertEqual(400, response.status_code)

    def test_error_for_unauthorized_user(self):
        """
        Method to test error for unauthorized request
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.url, self.provider
        )
        view = SalesAgreementTemplateListCreateAPIView().as_view()
        response: Response = render_response(view(request))
        self.assertEqual(401, response.status_code)

    @patch(
        "sales.tests.test_views.test_templates.AgreementTemplatePreviewService.generate_agreement_template_pdf_preview",
        return_value=dict(
            file_path="https://file-storage.com/file_name",
            file_name="sample preview.pdf",
        ),
    )
    def test_template_pdf_preview_for_existing_template(
        self, generate_agreement_template_pdf_preview
    ):
        """
        Method to test preview of an existing template
        """
        template: AgreementTemplate = AgreementTemplateFactory(
            provider=self.provider, author=self.user
        )
        view = SalesAgreementTemplatePDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.template_preview_url,
            self.provider,
            self.user,
            data=dict(existing_template_preview=True, template_id=template.id),
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        generate_agreement_template_pdf_preview.assert_called()

    def test_failure_of_template_pdf_preview_for_invalid_request(self):
        """
        Method to test failure of template preview when the request body is invalid.
        This takes into account the edge when the preview is neither for existing template or
        before template creation
        """
        view = SalesAgreementTemplatePDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.template_preview_url,
            self.provider,
            self.user,
            data={},
        )
        response: Response = render_response(view(request))
        self.assertEqual(400, response.status_code)

    def test_failure_of_existing_template_pdf_preview_for_invalid_request(
        self,
    ):
        """
        Method to test failure of PDF preview when request body is invalid for
        existing template preview
        """
        view = SalesAgreementTemplatePDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.template_preview_url,
            self.provider,
            self.user,
            data=dict(existing_template_preview=True),
        )
        response: Response = render_response(view(request))
        self.assertEqual(400, response.status_code)

    @patch(
        "sales.tests.test_views.test_templates.AgreementTemplatePreviewService.generate_agreement_template_pdf_preview",
        return_value=dict(
            file_path="https://file-storage.com/file_name",
            file_name="sample preview.pdf",
        ),
    )
    def test_template_pdf_preview_before_template_creation(
        self, generate_agreement_template_pdf_preview
    ):
        """
        Method to test PDF preview before template creation
        """
        view = SalesAgreementTemplatePDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.template_preview_url,
            self.provider,
            self.user,
            data=dict(
                pre_template_create_preview=True,
                template_payload=get_valid_request_payload(),
            ),
        )
        response: Response = render_response(view(request))
        self.assertEqual(200, response.status_code)
        generate_agreement_template_pdf_preview.assert_called()

    def test_failure_of_pdf_preview_before_template_creation_for_invalid_request(
        self,
    ):
        """
        Method to test failure of PDF preview in case of invalid request body
        """
        view = SalesAgreementTemplatePDFDownloadAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.template_preview_url,
            self.provider,
            self.user,
            data=dict(
                pre_template_create_preview=True,
                template_payload=get_invalid_request_payload(),
            ),
        )
        response: Response = render_response(view(request))
        self.assertEqual(400, response.status_code)


class TestSalesAgreementTemplateAuthorsListAPIView(BaseProviderTestCase):
    """
    Test view: SalesAgreementTemplateAuthorsListAPIView
    """

    authors_list_url: str = reverse(
        "api_v1_endpoints:providers:agreement-template-authors-list"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_agreement_template_authors(self):
        author_1: User = ProviderUserFactory(
            provider=self.provider, first_name="Johny", last_name="Depp"
        )
        AgreementTemplateFactory.create(
            provider=self.provider, author=author_1
        )
        AgreementTemplateFactory.create(
            provider=self.provider, author=author_1
        )
        author_2: User = ProviderUserFactory(
            provider=self.provider, first_name="Alain", last_name="Delon"
        )
        AgreementTemplateFactory.create(
            provider=self.provider, author=author_2
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.authors_list_url, self.provider, self.user
        )
        view = SalesAgreementTemplateAuthorsListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(2, len(response.data))
