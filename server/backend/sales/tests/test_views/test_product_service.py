from typing import Dict

from django.test import TestCase

from sales.models import AgreementTemplate
from sales.services.product_service import Products, MONTHS_IN_YEAR
from sales.tests.utils import (
    get_sample_products_data,
)


class TestProductService(TestCase):
    """
    Test Products and Product dataclasses
    """

    @classmethod
    def setUpTestData(cls):
        cls.service_class = Products

    def test_products_class_calculation_for_template(self):
        """
        Test calculations on products data for template
        """
        payload: Dict = get_sample_products_data()[0]
        payload.pop("selected_product", None)
        payload.pop("is_required", None)
        payload.pop("comment", None)
        payload.update(discount_percentage=0)
        product: Dict = payload.get("products")[0]
        total_extended_cost: int = (
            product.get("quantity")
            * product.get("customer_cost")
            * MONTHS_IN_YEAR
            if payload.get("billing_option") == AgreementTemplate.PER_ANNUM
            else product.get("quantity") * product.get("customer_cost")
        )
        total_customer_cost: int = product.get("customer_cost")
        expected_result: Dict = dict(
            total_extended_cost=total_extended_cost,
            total_customer_cost=total_customer_cost,
        )
        product: Products = self.service_class(**payload)
        actual_result: Dict = dict(
            total_extended_cost=product.total_extended_cost,
            total_customer_cost=product.total_customer_cost,
        )
        self.assertDictEqual(expected_result, actual_result)

    def test_products_class_initialization_for_template(self):
        """
        Test the initialization of Products dataclass for template
        """
        payload: Dict = get_sample_products_data()[1]
        payload.pop("selected_product", None)
        payload.pop("is_required", None)
        payload.pop("comment", None)
        payload.update(discount_percentage=0)
        product: Products = self.service_class(**payload)
        self.assertEqual(len(payload.get("products")), len(product.products))

    def test_products_class_initialization_for_agreement(self):
        """
        Test initialization of Products dataclass for agreement
        """
        payload: Dict = get_sample_products_data()[1]
        selected_product_index = payload.get("selected_product")
        payload.pop("is_required", None)
        payload.pop("comment", None)
        payload.update(
            selected_product_index=selected_product_index,
            discount_percentage=10,
        )
        payload.pop("selected_product", None)
        product: Products = self.service_class(**payload)
        # Only one product should be selected from bundle
        self.assertEqual(len(product.products), 1)

    def test_products_class_calculation_for_agreement(self):
        """
        Test calculations on products data for agreement
        """
        payload: Dict = get_sample_products_data()[0]
        discount_percentage = (
            0
            if payload.get("billing_option") == AgreementTemplate.ONE_TIME
            else 50
        )
        payload.pop("is_required", None)
        payload.pop("comment", None)
        payload.update(
            selected_product_index=payload.get("selected_product"),
            discount_percentage=discount_percentage,
        )
        payload.pop("selected_product", None)
        product: Dict = payload.get("products")[0]
        customer_cost: int = product.get("customer_cost")
        internal_cost: int = product.get("internal_cost")
        margin_per_unit: int = customer_cost - internal_cost
        discount: float = round(
            (discount_percentage / 100) * margin_per_unit, 2
        )
        customer_cost: float = round(customer_cost - discount, 2)
        total_extended_cost: float = round(
            product.get("quantity") * customer_cost * MONTHS_IN_YEAR
            if payload.get("billing_option") == AgreementTemplate.PER_ANNUM
            else product.get("quantity") * customer_cost
        )
        product: Products = self.service_class(**payload)
        actual_total_extended_cost = product.total_extended_cost
        self.assertEqual(total_extended_cost, actual_total_extended_cost)
