from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import ReadOnlyField
from rest_framework.request import Request
from typing import List, Dict, Optional, Union
from accounts.models import Provider, User, Customer
from auditlog.models import LogEntry
from auditlog.serializers import MODEL_DISPLAY_NAMES
from sales.models import (
    SalesActivity,
    SalesActivityStatusSetting,
    SalesActivityPrioritySetting,
    SalesActivityCustomView,
    AgreementTemplate,
    Agreement,
    AgreementMarginSettings,
    AutomatedCollectionNoticesSettings,
    PAX8IntegrationSettings,
    PAX8Product,
    PAX8ProductPricingInfo,
)
from sales.utils import is_preview
from utils import (
    name_contains_forbidden_characters,
    FORBIDDEN_CHARS_LIST,
    is_valid_uuid,
)


class UserRelatedFieldReadSerializer(serializers.Serializer):
    id = ReadOnlyField(read_only=True)
    name = ReadOnlyField(read_only=True)
    profile_pic = serializers.ImageField(
        source="profile.profile_pic", read_only=True
    )


class CustomerInfoSerializer(serializers.Serializer):
    id = ReadOnlyField(read_only=True)
    name = ReadOnlyField(read_only=True)
    account_manager_name = ReadOnlyField(read_only=True)
    account_manager_id = ReadOnlyField(read_only=True)


class PrioritySerializer(serializers.Serializer):
    id = ReadOnlyField(read_only=True)
    title = ReadOnlyField(read_only=True)
    color = serializers.CharField(read_only=True)
    rank = serializers.IntegerField(read_only=True)


class StatusSerializer(serializers.Serializer):
    id = ReadOnlyField(read_only=True)
    title = ReadOnlyField(read_only=True)
    color = serializers.CharField(read_only=True)


class SalesActivityReadSerializer(serializers.ModelSerializer):
    last_updated_by = UserRelatedFieldReadSerializer()
    created_by = UserRelatedFieldReadSerializer()
    assigned_to = UserRelatedFieldReadSerializer()
    customer = CustomerInfoSerializer()
    priority = PrioritySerializer()
    status = StatusSerializer()
    age = serializers.IntegerField(source="age.days", read_only=True)

    class Meta:
        model = SalesActivity
        fields = (
            "id",
            "description",
            "customer",
            "customer_contact",
            "last_updated_by",
            "notes",
            "created_by",
            "assigned_to",
            "due_date",
            "priority",
            "status",
            "activity_type",
            "age",
            "opportunity_id",
            "updated_on",
        )


class SalesActivityWriteSerializer(serializers.ModelSerializer):
    id = ReadOnlyField(read_only=True)

    class Meta:
        model = SalesActivity
        fields = (
            "id",
            "description",
            "customer",
            "customer_contact",
            "notes",
            "assigned_to",
            "due_date",
            "priority",
            "status",
            "activity_type",
            "opportunity_id",
        )


class SalesActivityStatusSettingSerializer(serializers.ModelSerializer):
    in_use = serializers.BooleanField(read_only=True)

    def validate_title(self, title):
        """
        Validates for a provider, title for status with same name should not be exist.
        """
        request = self.context.get("request")
        try:
            status_settings = SalesActivityStatusSetting.objects.get(
                title=title, provider_id=request.provider
            )
        except SalesActivityStatusSetting.DoesNotExist:
            return title

        if self.instance and self.instance.title == status_settings.title:
            return title

        raise ValidationError(
            f"Title with name {title} is already added for status setting with this provider"
        )

    def validate_is_default_closed_status(self, is_default_closed_status):
        """
        Validates for a provider, closed status should not be already True.
        """
        request = self.context.get("request")
        if is_default_closed_status:
            try:
                status_settings = SalesActivityStatusSetting.objects.filter(
                    provider_id=request.provider,
                    is_default_closed_status=is_default_closed_status,
                ).count()
            except SalesActivityStatusSetting.DoesNotExist:
                return is_default_closed_status

            if status_settings > 1:
                raise ValidationError(
                    f"is_default_closed_status is already True for this provider"
                )
        return is_default_closed_status

    class Meta:
        model = SalesActivityStatusSetting
        fields = (
            "id",
            "title",
            "color",
            "is_default",
            "is_closed",
            "is_default_closed_status",
            "in_use",
        )


class SalesActivityPrioritySettingSerializer(serializers.ModelSerializer):
    in_use = serializers.BooleanField(read_only=True)

    def validate_rank(self, rank):
        """
        Validates for a provider, rank should not be exist.
        """
        request = self.context.get("request")
        try:
            priority_settings = SalesActivityPrioritySetting.objects.get(
                rank=rank, provider_id=request.provider
            )
        except SalesActivityPrioritySetting.DoesNotExist:
            return rank

        if self.instance and self.instance.rank == priority_settings.rank:
            return rank

        raise ValidationError(
            f"Rank {rank} is already assigned with this provider"
        )

    def validate_title(self, title):
        """
        Validates for a provider, title for priority with same name should not be exist.
        """
        request = self.context.get("request")
        try:
            priority_settings = SalesActivityPrioritySetting.objects.get(
                title=title, provider_id=request.provider
            )
        except SalesActivityPrioritySetting.DoesNotExist:
            return title

        if self.instance and self.instance.title == priority_settings.title:
            return title

        raise ValidationError(
            f"Title with name {title} is already added for priority setting with this provider"
        )

    class Meta:
        model = SalesActivityPrioritySetting
        fields = ("id", "title", "color", "is_default", "rank", "in_use")


class SalesActivityCustomViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesActivityCustomView
        fields = ("custom_activity_view",)


class PAX8ProductSerializer(serializers.Serializer):
    product_crm_id = serializers.CharField(allow_null=True, allow_blank=False)
    product_name = serializers.CharField(allow_null=True, allow_blank=False)
    vendor_name = serializers.CharField(allow_null=True, allow_blank=False)
    short_description = serializers.CharField(
        allow_null=True, allow_blank=True
    )
    sku = serializers.CharField(allow_null=True, allow_blank=False)
    billing_term = serializers.CharField(allow_null=True, allow_blank=False)
    commitment_term = serializers.CharField(allow_null=True, allow_blank=True)
    commitment_term_in_months = serializers.IntegerField(allow_null=True)
    pricing_type = serializers.CharField(allow_null=True, allow_blank=True)
    unit_of_measurement = serializers.CharField(
        allow_null=True, allow_blank=True
    )
    partner_buy_rate = serializers.FloatField(allow_null=True)
    suggested_retail_price = serializers.FloatField(allow_null=True)
    start_quantity_range = serializers.IntegerField(allow_null=True)
    end_quantity_range = serializers.IntegerField(allow_null=True)
    charge_type = serializers.CharField(allow_blank=True, allow_null=True)
    internal_cost = serializers.FloatField(allow_null=True)
    customer_cost = serializers.FloatField(allow_null=True)
    margin = serializers.FloatField(allow_null=True)
    revenue = serializers.FloatField(allow_null=True)
    total_margin = serializers.FloatField(allow_null=True)
    quantity = serializers.IntegerField(allow_null=True)
    min_quantity = serializers.IntegerField(allow_null=True)
    max_quantity = serializers.IntegerField(allow_null=True)
    is_comment = serializers.BooleanField()
    comment = serializers.CharField(allow_null=True, required=False)
    id = serializers.IntegerField(allow_null=False)

    def validate_product_crm_id(self, product_crm_id: Optional[int]):
        if product_crm_id and not is_valid_uuid(product_crm_id):
            raise ValidationError("Product CRM ID must be a valid UUID!")
        return product_crm_id


class AgreementTemplatePAX8ProductsDataListSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    products = PAX8ProductSerializer(
        many=True, required=False, allow_null=True
    )


class AgreementProductsSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, max_length=150, allow_null=False, allow_blank=False
    )
    description = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    internal_cost = serializers.FloatField(allow_null=True)
    customer_cost = serializers.FloatField(allow_null=True)
    margin = serializers.IntegerField(allow_null=True)
    quantity = serializers.IntegerField(allow_null=True)


class AgreementTemplateSectionsSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    content = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )
    is_locked = serializers.BooleanField(required=True)
    id = serializers.IntegerField(required=True)
    value_html = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )


class AgreementTemplateProductsSerializer(serializers.Serializer):
    billing_option = serializers.ChoiceField(
        choices=AgreementTemplate.BILLING_OPTIONS
    )
    product_type = serializers.ChoiceField(
        choices=AgreementTemplate.PRODUCT_TYPES
    )
    is_bundle = serializers.BooleanField(required=True)
    id = serializers.IntegerField(required=True)
    is_checked = serializers.BooleanField(required=True)
    selected_product = serializers.IntegerField(required=True, allow_null=True)
    products = AgreementProductsSerializer(many=True, required=False)
    comment = serializers.CharField(required=False, allow_null=True)
    is_required = serializers.BooleanField(required=True)


class AgreementTemplateCreateSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = AgreementTemplate
        fields = [
            "id",
            "name",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "pax8_products",
        ]
        read_only_fields = ["id"]

    def validate_name(self, name: str):
        request: Request = self.context.get("request")
        provider: Provider = request.provider
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Template name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        # Validation of name not required for preview
        if is_preview(request.data):
            return name
        # Don't allow new template creation with name same as that of
        # existing template unless it's version update
        if AgreementTemplate.objects.filter(
            provider_id=provider.id,
            name=name,
            is_current_version=True,
            is_disabled=False,
        ).exists():
            raise ValidationError(
                "A template with given name already exists! Try using different name."
            )
        return name

    def create(self, validated_data):
        sections = validated_data.pop("sections", [])
        products = validated_data.pop("products", [])
        agreement_template = AgreementTemplate.objects.create(
            sections=sections, products=products, **validated_data
        )
        return agreement_template


class AgreementTemplateListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    provider = serializers.IntegerField(source="provider_id", read_only=True)
    name = serializers.CharField(read_only=True)
    author = serializers.CharField(source="author_id", read_only=True)
    author_name = serializers.CharField(source="author.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    is_disabled = serializers.BooleanField(read_only=True)
    minor_version = serializers.IntegerField(read_only=True)
    major_version = serializers.IntegerField(read_only=True)
    created_on = serializers.DateTimeField(read_only=True)
    updated_on = serializers.DateTimeField(read_only=True)
    version = serializers.CharField(read_only=True)
    is_current_version = serializers.BooleanField(read_only=True)
    root_template = serializers.IntegerField(
        source="root_template_id", read_only=True
    )
    parent_template = serializers.IntegerField(
        source="parent_template_id", read_only=True
    )


class AgreementTemplateUpdateSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = AgreementTemplate
        fields = [
            "id",
            "provider",
            "author",
            "name",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "pax8_products",
        ]
        read_only_fields = ["id", "provider", "author"]

    def validate_name(self, name: str):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Template name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        request = self.context.get("request")
        template_id: int = self.context.get("view").kwargs.get("template_id")
        if (
            AgreementTemplate.objects.filter(
                provider_id=request.provider.id,
                name=name,
                is_current_version=True,
                is_disabled=False,
            )
            .exclude(id=template_id)
            .exists()
        ):
            raise ValidationError(
                f"Template with the same name already exists! Try using a different name."
            )
        return name


class AgreementTemplateSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    author_name = serializers.CharField(source="author.name")
    updated_by_name = serializers.CharField(source="updated_by.name")
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = AgreementTemplate
        fields = [
            "id",
            "provider",
            "author",
            "updated_by",
            "name",
            "is_disabled",
            "major_version",
            "minor_version",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "created_on",
            "updated_on",
            "version",
            "is_current_version",
            "root_template",
            "parent_template",
            "author_name",
            "updated_by_name",
            "pax8_products",
        ]
        read_only_fields = fields


class AgreementTemplateDropdownVersionListingSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = AgreementTemplate
        fields = [
            "id",
            "name",
            "version",
            "parent_template",
        ]


class AgreementListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    forecast_data = serializers.JSONField(read_only=True)
    is_disabled = serializers.BooleanField(read_only=True)
    is_current_version = serializers.BooleanField(read_only=True)
    customer_name = serializers.CharField(
        source="customer.name", read_only=True
    )
    author_name = serializers.CharField(source="author.name", read_only=True)
    updated_on = serializers.DateTimeField(read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    version = serializers.CharField(read_only=True)


class AgreementSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    author_name = serializers.CharField(source="author.name", read_only=True)
    user_name = serializers.CharField(source="user.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    template_name = serializers.CharField(
        source="template.name", read_only=True
    )
    template_version = serializers.CharField(
        source="template.version", read_only=True
    )
    customer_name = serializers.CharField(
        source="customer.name", read_only=True
    )
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = Agreement
        fields = [
            "id",
            "name",
            "template",
            "template_name",
            "template_version",
            "customer",
            "user",
            "quote_id",
            "is_disabled",
            "version",
            "major_version",
            "minor_version",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "customer_cost",
            "margin",
            "margin_percentage",
            "internal_cost",
            "is_current_version",
            "root_agreement",
            "parent_agreement",
            "author_name",
            "user_name",
            "updated_by_name",
            "created_on",
            "updated_on",
            "discount_percentage",
            "total_discount",
            "customer_name",
            "pax8_products",
            "forecast_data",
        ]
        read_only_fields = fields


class AgreementCreateSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    template = serializers.PrimaryKeyRelatedField(
        queryset=AgreementTemplate.objects.all()
    )
    customer = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all()
    )
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    author_name = serializers.CharField(source="author.name", read_only=True)
    user_name = serializers.CharField(source="user.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    template_name = serializers.CharField(
        source="template.name", read_only=True
    )
    template_version = serializers.CharField(
        source="template.version", read_only=True
    )
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = Agreement
        fields = [
            "id",
            "name",
            "template",
            "template_name",
            "template_version",
            "customer",
            "user",
            "quote_id",
            "is_disabled",
            "major_version",
            "minor_version",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "customer_cost",
            "margin",
            "margin_percentage",
            "internal_cost",
            "is_current_version",
            "root_agreement",
            "parent_agreement",
            "author_name",
            "user_name",
            "updated_by_name",
            "created_on",
            "updated_on",
            "discount_percentage",
            "total_discount",
            "pax8_products",
            "forecast_data",
        ]

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Agreement name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        request: Request = self.context.get("request")
        if Agreement.objects.filter(
            provider_id=request.provider.id,
            name=name,
            is_current_version=True,
            is_disabled=False,
        ).exists():
            raise ValidationError(
                "An agreement with given name already exists! Try using different name."
            )
        return name

    def create(self, validated_data):
        sections = validated_data.pop("sections", [])
        products = validated_data.pop("products", [])
        pax8_products = validated_data.pop("pax8_products", dict())
        agreement = Agreement.objects.create(
            sections=sections,
            products=products,
            pax8_products=pax8_products,
            **validated_data,
        )
        return agreement


class AgreementRetrieveUpdateSerializer(serializers.ModelSerializer):
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    author_name = serializers.CharField(source="author.name", read_only=True)
    user_name = serializers.CharField(source="user.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    customer_name = serializers.CharField(
        source="customer.name", read_only=True
    )
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=True
    )

    class Meta:
        model = Agreement
        fields = [
            "id",
            "name",
            "template",
            "customer",
            "customer_name",
            "user",
            "quote_id",
            "is_disabled",
            "version",
            "major_version",
            "minor_version",
            "min_discount",
            "max_discount",
            "version_description",
            "sections",
            "products",
            "customer_cost",
            "margin",
            "margin_percentage",
            "internal_cost",
            "author_name",
            "user_name",
            "updated_by_name",
            "created_on",
            "updated_on",
            "discount_percentage",
            "total_discount",
            "pax8_products",
        ]

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Agreement name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        request = self.context.get("request")
        agreement_id: int = self.context.get("view").kwargs.get("pk")
        if (
            Agreement.objects.filter(
                provider_id=request.provider.id,
                name=name,
                is_current_version=True,
                is_disabled=False,
            )
            .exclude(id=agreement_id)
            .exists()
        ):
            raise ValidationError(
                f"An agreement with the same name already exists! Try using a different name."
            )
        return name


class SalesAgreementTemplateAuthorsListSerializer(serializers.Serializer):
    author_id = serializers.CharField(
        source="author.id", allow_null=True, read_only=True
    )
    author_name = serializers.CharField(
        source="author.name", allow_null=True, read_only=True
    )
    profile_url = serializers.ImageField(
        read_only=True, allow_null=True, source="author.profile.profile_pic"
    )


class AgreementMarginSettingsSerializer(serializers.ModelSerializer):
    def validate_margin_limit(self, margin_limit):
        request = self.context.get("request")
        if (
            AgreementMarginSettings.objects.filter(
                provider=request.provider
            ).exists()
            and not self.instance
        ):
            raise ValidationError(
                f"Margin limit for provider {request.provider} already exists!"
            )
        return margin_limit

    class Meta:
        model = AgreementMarginSettings
        exclude = ("provider",)


class SalesAgreementTemplatePreviewSerializer(
    AgreementTemplateCreateSerializer
):
    major_version = serializers.IntegerField(required=False, allow_null=True)
    minor_version = serializers.IntegerField(required=False, allow_null=True)
    author_name = serializers.CharField(required=False, allow_null=True)

    class Meta:
        model = AgreementTemplate
        fields = AgreementTemplateCreateSerializer.Meta.fields + [
            "major_version",
            "minor_version",
            "author_name",
        ]


class AgreementPreviewSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    sections = AgreementTemplateSectionsSerializer(many=True, required=False)
    products = AgreementTemplateProductsSerializer(many=True, required=False)
    customer = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all()
    )
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), required=True
    )
    major_version = serializers.IntegerField(required=False, allow_null=True)
    minor_version = serializers.IntegerField(required=False, allow_null=True)
    version_description = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    discount_percentage = serializers.IntegerField(required=True)
    author_name = serializers.CharField(required=False, allow_null=True)
    pax8_products = AgreementTemplatePAX8ProductsDataListSerializer(
        required=False, allow_null=True
    )

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Agreement name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        return name


class HistorySerializer(serializers.ModelSerializer):
    action = serializers.CharField(source="get_action_display")
    actor = serializers.StringRelatedField()
    model_display_name = serializers.SerializerMethodField()

    @staticmethod
    def get_model_display_name(obj):
        display_name = MODEL_DISPLAY_NAMES.get(
            obj.content_type.model, obj.content_type.model
        )
        return display_name

    class Meta:
        model = LogEntry
        fields = (
            "action",
            "actor",
            "remote_addr",
            "timestamp",
            "additional_data",
            "changes_str",
            "object_repr",
            "model_display_name",
        )


class BillingStatusesSerializer(serializers.Serializer):
    billing_status_id = serializers.IntegerField(
        allow_null=False, required=True
    )
    billing_status_name = serializers.CharField(
        allow_null=False, allow_blank=False, required=True
    )


class AutomatedCollectionNoticesSettingRetrieveSerializer(
    serializers.BaseSerializer
):
    """
    Read only serializer implementation as recommended in DRF docs
    """

    def to_representation(self, instance):
        return {
            "provider_id": instance.provider_id,
            "weekly_send_schedule": instance.weekly_send_schedule,
            "send_to_email": instance.send_to_email,
            "send_from_email": instance.send_from_email,
            "invoice_exclude_patterns": instance.invoice_exclude_patterns,
            "approaching_due_date_ageing": instance.approaching_due_date_ageing,
            "email_body_template": instance.email_body_template,
            "billing_statuses": BillingStatusesSerializer(
                instance.billing_statuses, many=True
            ).data,
            "subject": instance.subject,
            "contact_type_crm_id": instance.contact_type_crm_id,
        }


class AutomatedCollectionNoticesSettingCreateUpdateSerializer(
    serializers.ModelSerializer
):
    billing_statuses = BillingStatusesSerializer(many=True, required=True)
    contact_type_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )

    class Meta:
        model = AutomatedCollectionNoticesSettings
        fields = (
            "provider_id",
            "weekly_send_schedule",
            "send_to_email",
            "send_from_email",
            "invoice_exclude_patterns",
            "approaching_due_date_ageing",
            "email_body_template",
            "billing_statuses",
            "subject",
            "contact_type_crm_id",
        )
        read_only_fields = ("provider_id",)

    def create(self, validated_data):
        automated_collection_notices_setting: AutomatedCollectionNoticesSettings = AutomatedCollectionNoticesSettings.objects.create(
            **validated_data
        )
        return automated_collection_notices_setting

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()
        return instance


class CustomerAutomatedCollectionNoticeEmailTaskDataSerializer(
    serializers.Serializer
):
    customer_crm_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        required=True,
        allow_null=False,
        allow_empty=True,
    )
    all_customers = serializers.BooleanField(required=True)

    def validate_customer_crm_ids(self, customer_crm_ids):
        all_customers: bool = self.context.get("request").data.get(
            "all_customers"
        )
        # customer_crm_ids should not be empty when all_customers is False
        if all_customers is False and (not customer_crm_ids):
            raise ValidationError("customer_crm_ids should not be empty!")
        return customer_crm_ids


class PAX8IntegrationSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PAX8IntegrationSettings
        fields = ["client_id", "client_secret", "vendors"]


class PAX8CredentialsPayloadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PAX8IntegrationSettings
        fields = ["client_id", "client_secret"]


class PAX8ProductCreateSerializer(serializers.Serializer):
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    crm_id = serializers.CharField(
        max_length=150,
        required=True,
        allow_null=False,
        allow_blank=False,
    )
    name = serializers.CharField(
        max_length=150,
        required=True,
        allow_null=False,
        allow_blank=False,
    )
    vendor_name = serializers.CharField(
        max_length=150,
        required=True,
        allow_null=True,
        allow_blank=True,
    )
    short_description = serializers.CharField(
        required=False, allow_blank=True, allow_null=True
    )
    sku = serializers.CharField(
        max_length=150,
        required=True,
        allow_null=True,
        allow_blank=True,
    )
    vendor_sku = serializers.CharField(
        max_length=150,
        required=False,
        allow_null=True,
        allow_blank=True,
    )
    alt_vendor_sku = serializers.CharField(
        max_length=150,
        required=False,
        allow_null=True,
        allow_blank=True,
    )


class PAX8ProductUpdateSerializer(PAX8ProductCreateSerializer):
    provider = None


class PricingRatesSerializer(serializers.Serializer):
    partner_buy_rate = serializers.FloatField(required=False, allow_null=True)
    suggested_retail_price = serializers.FloatField(
        required=False, allow_null=True
    )
    start_quantity_range = serializers.IntegerField(
        required=False, allow_null=True
    )
    end_quantity_range = serializers.IntegerField(
        required=False, allow_null=True
    )
    charge_type = serializers.CharField(
        max_length=50, required=False, allow_null=True, allow_blank=True
    )


class PricingDataSerializer(serializers.Serializer):
    billing_term = serializers.CharField(
        max_length=150, required=False, allow_null=True, allow_blank=True
    )
    commitment_term = serializers.CharField(
        max_length=150, required=False, allow_null=True, allow_blank=True
    )
    commitment_term_in_months = serializers.IntegerField(allow_null=True)
    pricing_type = serializers.CharField(
        max_length=50, required=False, allow_null=True, allow_blank=True
    )
    unit_of_measurement = serializers.CharField(
        max_length=50, required=False, allow_null=True, allow_blank=True
    )
    pricing_rates = PricingRatesSerializer(many=True, required=False)


class PAX8ProductPricingInfoCreateSerializer(serializers.Serializer):
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    pax8_product = serializers.PrimaryKeyRelatedField(
        queryset=PAX8Product.objects.all()
    )
    pricing_data = PricingDataSerializer(many=True, required=False)


class PAX8ProductPricingInfoUpdateSerializer(
    PAX8ProductPricingInfoCreateSerializer
):
    provider = None


class PAX8ProductsListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    product_crm_id = serializers.CharField(read_only=True)
    product_name = serializers.CharField(read_only=True)
    vendor_name = serializers.CharField(
        read_only=True, allow_null=True, allow_blank=True
    )
    short_description = serializers.CharField(
        read_only=True, allow_null=True, allow_blank=True
    )
    sku = serializers.CharField(
        read_only=True, allow_null=True, allow_blank=True
    )
    pricing_data = PricingDataSerializer(
        many=True, read_only=True, required=False
    )


class PAX8VendorsListSerializer(serializers.Serializer):
    vendor_name = serializers.CharField(read_only=True)


class PAX8ProductDetailsSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    product_crm_id = serializers.CharField(
        source="pax8_product.crm_id", read_only=True
    )
    product_name = serializers.CharField(
        source="pax8_product.name", read_only=True
    )
    vendor_name = serializers.CharField(
        source="pax8_product.vendor_name",
        read_only=True,
        allow_null=True,
        allow_blank=True,
    )
    short_description = serializers.CharField(
        source="pax8_product.short_description",
        read_only=True,
        allow_null=True,
        allow_blank=True,
    )
    sku = serializers.CharField(
        source="pax8_product.sku",
        read_only=True,
        allow_null=True,
        allow_blank=True,
    )
    pricing_data = PricingDataSerializer(
        many=True, read_only=True, required=False
    )
