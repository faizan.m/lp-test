import json
import os
from datetime import date
from typing import Dict, List, Any, Optional, Union, Tuple

from django.conf import settings
from django.forms.models import model_to_dict
from django.template.loader import render_to_string
from rest_framework.exceptions import ValidationError

from accounts.models import Customer, Provider, User
from core.utils import generate_random_id
from document.services.change_request import ChangeRequestSOWService
from document.services.quote_service import (
    QuoteService,
    group_pax8_products_by_billing_term,
    group_pax8_products_by_vendor,
)
from sales import utils as sales_utils
from sales.models import AgreementTemplate, Agreement
from sales.services.pax8_calculations import (
    PAX8ProductCalculator,
    PAX8Calculator,
)
from sales.services.product_service import Product
from sales.services.product_service import Products
from utils import (
    get_app_logger,
    get_customer_contact_details,
    group_list_of_items_by_key,
    generate_pdf_using_pdf_shift,
)

logger = get_app_logger(__name__)


class AgreementTemplatePreviewService:
    TEMPLATE_DIR: str = AgreementTemplate.get_agreement_template_directory()
    # Template names
    TEMPLATE_PDF: str = "template_pdf.html"
    TEMPLATE_HEADER_PDF: str = "template_header_pdf.html"
    TEMPLATE_FOOTER_PDF: str = "template_footer_pdf.html"
    AGREEMENT_PDF: str = "agreement_pdf.html"
    AGREEMENT_SERVICE_DETAIL: str = "agreement_service_detail.html"
    # Preview names
    PRE_TEMPLATE_CREATION_PREVIEW: str = "pre_template_create_preview"
    EXISTING_TEMPLATE_PREVIEW: str = "existing_template_preview"
    PRE_AGREEMENT_CREATE_PREVIEW: str = "pre_agreement_create_preview"
    EXISTING_AGREEMENT_PREVIEW: str = "existing_agreement_preview"
    SERVICE_DETAIL_PDF_PREFIX: str = "Subscription Detail"
    AGREEMENT_PDF_SUFFIX: str = "AGR"

    @classmethod
    def get_rendered_html_str(
        cls, template_dir_path, template_name: str, template_payload: Dict
    ) -> str:
        """
        Get rendered html string

        Args:
            template_dir_path: Template directory path
            template_name: HTML template name
            template_payload: Template context

        Returns:
            Rendered html string
        """
        TEMPLATE_PATH = os.path.join(template_dir_path, template_name)
        rendered_html_str: str = render_to_string(
            TEMPLATE_PATH, template_payload
        )
        return rendered_html_str

    @staticmethod
    def _get_pax8_products_billing_summary(
        pax8_products_for_preview: List[Dict],
    ) -> Dict[str, Any]:
        billing_summary: Dict[str, Any] = dict()

        if not pax8_products_for_preview:
            return billing_summary

        billing_term_to_extended_cost_mapping: Dict[
            str, Union[int, float]
        ] = dict()
        pax8_annual_cost: float = 0

        for product in pax8_products_for_preview:
            if product.get("is_comment", False):
                continue

            pax8_annual_cost += product.get("annual_extended_cost")

            billing_term: str = product.get("billing_term")
            extended_cost: Union[int, float] = product.get("extended_cost")
            if billing_term in set(
                billing_term_to_extended_cost_mapping.keys()
            ):
                billing_term_to_extended_cost_mapping[
                    billing_term
                ] += extended_cost
            else:
                billing_term_to_extended_cost_mapping[
                    billing_term
                ] = extended_cost

        billing_summary.update(
            extended_cost_by_billing_term=billing_term_to_extended_cost_mapping,
            estimated_pax8_annual_cost=pax8_annual_cost,
        )
        return billing_summary

    @classmethod
    def _get_pax8_product_calculation_data_for_agreement_preview(
        cls, pax8_products: Dict
    ):
        products: List[Dict] = pax8_products.get("products", [])
        if not products:
            return dict()

        preview_data: Dict[str, Any] = dict()
        pax8_products_for_preview: List[Dict] = list()

        for product in products:
            if product.get("is_comment", False):
                pax8_products_for_preview.append(
                    dict(
                        is_comment=True,
                        comment=product.get("comment"),
                    )
                )
            else:
                computed_product: PAX8ProductCalculator = (
                    PAX8ProductCalculator(
                        billing_term=product.get("billing_term"),
                        margin_percent=product.get("margin"),
                        partner_buy_rate=product.get("partner_buy_rate"),
                        product_name=product.get("product_name"),
                        quantity=product.get("quantity"),
                        sku=product.get("sku"),
                        vendor_name=product.get("vendor_name"),
                        short_description=product.get("short_description", ""),
                        commitment_term=product.get("commitment_term", ""),
                    )
                )
                pax8_products_for_preview.append(
                    dict(
                        is_comment=False,
                        comment=None,
                        product_name=computed_product.product_name,
                        sku=computed_product.sku,
                        short_description=computed_product.short_description,
                        commitment_term_in_months=product.get(
                            "commitment_term_in_months"
                        ),
                        billing_term=computed_product.billing_term,
                        extended_cost=computed_product.extended_cost,
                        quantity=computed_product.quantity,
                        customer_cost=computed_product.customer_cost_per_unit,
                        annual_extended_cost=computed_product.annual_extended_cost,
                    )
                )
        billing_summary: Dict[
            str, Any
        ] = cls._get_pax8_products_billing_summary(pax8_products_for_preview)
        preview_data.update(
            name=pax8_products.get("name"),
            pax8_products_list=pax8_products_for_preview,
            billing_summary=billing_summary,
        )
        return preview_data

    @staticmethod
    def _get_pax8_products_calculation_data_for_preview(
        pax8_products: Dict[str, Any],
    ) -> Dict[str, Any]:
        """
        Get PAX8 product calculation data for preview.

        Args:
            pax8_products: PAX8 product

        Returns:
            PAX8 product calculation data
        """
        if not pax8_products.get("products", []):
            return dict()

        preview_data: Dict[str, Any] = dict(name=pax8_products.get("name"))
        pax8_products_list_for_preview: List[Dict] = list()

        pax8_calculator: PAX8Calculator = PAX8Calculator(
            pax8_products_list=pax8_products.get("products")
        )
        computed_pax8_products: List[
            Dict
        ] = pax8_calculator.get_summary_of_each_product()
        for product in computed_pax8_products:
            pax8_products_list_for_preview.append(
                dict(
                    product_name=product.get("product_name"),
                    sku=product.get("sku"),
                    short_description=product.get("short_description"),
                    commitment_term=product.get("commitment_term"),
                    billing_term=product.get("billing_term"),
                    extended_cost=product.get("extended_cost"),
                    quantity=product.get("quantity"),
                    customer_cost=product.get("customer_cost"),
                )
            )
        pax8_products_summary: Dict = (
            pax8_calculator.get_summary_of_all_products()
        )
        total_extended_cost: Union[float, int] = pax8_products_summary.get(
            "total_revenue"
        )
        preview_data.update(
            pax8_products_list=pax8_products_list_for_preview,
            total_pax8_extended_cost=total_extended_cost,
        )
        return preview_data

    @staticmethod
    def get_template_payload_from_model_instance(
        template: AgreementTemplate,
    ) -> Dict:
        """
        Get required context data from template model instance

        Args:
            template: Agreement Template

        Returns:
            Template context
        """
        required_field: List[str] = [
            "name",
            "sections",
            "products",
            "version_description",
            "pax8_products",
        ]
        template_payload: Dict = model_to_dict(template, fields=required_field)
        template_payload.update(
            author_name=template.author.name, version=template.version
        )
        return template_payload

    @classmethod
    def get_header_for_pdf_preview(cls, template_context: Dict) -> Dict:
        """
        Get header for PDF preview

        Args:
            template_context: Template context

        Returns:
            Header context
        """
        header_html_str: str = cls.get_rendered_html_str(
            cls.TEMPLATE_DIR, cls.TEMPLATE_HEADER_PDF, template_context
        )
        header = {"source": header_html_str}
        return header

    @classmethod
    def get_footer_for_pdf_preview(cls, template_context: Dict) -> Dict:
        """
        Get footer for PDF preview

        Args:
            template_context: Template context

        Returns:
            Footer context
        """
        footer_html_str: str = cls.get_rendered_html_str(
            cls.TEMPLATE_DIR, cls.TEMPLATE_FOOTER_PDF, template_context
        )
        footer = {"source": footer_html_str, "spacing": "100px"}
        return footer

    @classmethod
    def add_extra_info_to_template_context(cls, template_context: Dict):
        """
        Update context with required data such as logo, current_date, etc.

        Args:
            template_context: Template context
        """
        template_context.update(
            current_date=date.today().strftime("%m/%d/%Y"),
            logo=settings.LOOKINGPOINT_LOGO,
        )

    @classmethod
    def get_agreement_pdf_file_name(cls, context: Dict[str, Any]) -> str:
        """
        Get file name for agreement PDF.

        Args:
            context: Template render context

        Returns:
            File name
        """
        file_name: str = (
            f"{context.get('company_name')} - {context.get('name')} - v{context.get('version')} - "
            f"{context.get('updated_on')} - {cls.AGREEMENT_PDF_SUFFIX}.pdf"
        )
        return file_name

    @classmethod
    def _get_product_calculation_data_for_agreement_preview(
        cls, context: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        Get product calculation data for agreement preview

        Args:
            context: Agreement render context

        Returns:
            None
        """
        # Please refer to products schema for agreements and templates first for better understanding
        # All product dicts in products_for_preview have consistent structure which is used in calculations
        if not context.get("products", []):
            return dict()

        products_for_preview: List[Dict] = list()

        agreement_products: List[Dict] = context.pop("products", [])
        for product_bundle in agreement_products:
            billing_option: str = product_bundle.get("billing_option")
            # If product type is comment, extended_cost = 0
            if product_bundle.get("product_type") == Agreement.COMMENT:
                products_for_preview.append(
                    dict(
                        is_comment=True,
                        comment=product_bundle.get("comment"),
                        billing_option=billing_option,
                        name=None,
                        quantity=None,
                        customer_cost=None,
                        extended_cost=0,
                        description=None,
                    )
                )
            else:
                products_list: List[Dict] = product_bundle.get("products")
                selected_product_index: Optional[int] = product_bundle.get(
                    "selected_product"
                )
                if selected_product_index is None or (
                    selected_product_index >= len(products_list)
                ):
                    continue
                if products_list[selected_product_index].get("quantity") == 0:
                    product_preview_data: Dict = (
                        cls._get_preview_data_for_product_with_zero_quantity(
                            products_list[selected_product_index],
                            billing_option,
                        )
                    )
                    products_for_preview.append(product_preview_data)
                else:
                    product_preview_data: Dict = (
                        cls._get_preview_data_for_valid_product(
                            product_bundle,
                            context.get("discount_percentage", 0),
                        )
                    )
                    products_for_preview.append(product_preview_data)
        products_summary_by_billing_frequency: Dict[
            str, Union[int, float]
        ] = cls._get_products_summary_by_billing_frequency(
            products_for_preview
        )
        preview_data: Dict[str, Any] = dict(
            **products_summary_by_billing_frequency,
            products=products_for_preview,
        )
        return preview_data

    @staticmethod
    def _get_preview_data_for_product_with_zero_quantity(
        product: Dict, billing_option: str
    ):
        """
        Get preview data for product having 0 quantity.

        Args:
            product: Individual product dict
            billing_option: Billing option

        Returns:
            Product preview data (Dict)
        """
        # If a product has zero quantity, it is only shown in preview but not part of
        # calculations. Hence extended_cost = 0
        return dict(
            billing_option=billing_option,
            name=product.get("name"),
            quantity=product.get("quantity"),
            customer_cost=round(product.get("customer_cost")),
            extended_cost=0,
            description=product.get("description"),
            is_comment=False,
        )

    @staticmethod
    def _get_preview_data_for_valid_product(
        product_bundle: Dict[str, Any], discount_percent: int
    ):
        """
        Get preview data for a product with quantity > 0 in a format required for preview.

        Args:
            product_bundle: Product
            discount_percent: Discount percent

        Returns:
            Product preview data (Dict)
        """
        # The product calculations are performed by Products class
        billing_option = product_bundle.get("billing_option")
        product_calculator: Products = Products(
            billing_option=billing_option,
            product_type=product_bundle.get("product_type"),
            is_bundle=product_bundle.get("is_bundle"),
            id=product_bundle.get("id"),
            is_checked=product_bundle.get("is_checked"),
            products=product_bundle.get("products"),
            selected_product_index=product_bundle.get("selected_product"),
            discount_percentage=0
            if billing_option == AgreementTemplate.ONE_TIME
            else discount_percent,
        )
        # A product bundle has list of products out of which only one product is selected
        # and used in calculation.
        _product: Product = product_calculator.products[0]
        return dict(
            billing_option=billing_option,
            name=_product.name,
            quantity=_product.quantity,
            customer_cost=_product.original_customer_cost_per_unit,
            extended_cost=_product.extended_cost,
            description=_product.description,
            is_comment=False,
        )

    @staticmethod
    def _get_products_summary_by_billing_frequency(
        products: List[Dict],
    ) -> Dict[str, Any]:
        """
        Get product summary for various billing frequencies.

        Args:
            products: List of products for preview.

        Returns:
            Product summary (Dict)
        """
        total_one_time_cost: int = 0
        total_monthly_cost: int = 0
        total_annual_cost: int = 0

        for product in products:
            billing_option: str = product.get("billing_option")
            if billing_option == Agreement.ONE_TIME:
                total_one_time_cost += product.get("extended_cost")
            elif billing_option == Agreement.PER_MONTH:
                total_monthly_cost += product.get("extended_cost")
            elif billing_option == Agreement.PER_ANNUM:
                total_annual_cost += product.get("extended_cost")
        estimated_annual_cost: int = round(
            (total_one_time_cost + total_monthly_cost * 12 + total_annual_cost)
        )
        return dict(
            total_one_time_cost=total_one_time_cost,
            total_monthly_cost=total_monthly_cost,
            total_annual_cost=total_annual_cost,
            estimated_annual_cost=estimated_annual_cost,
        )

    @classmethod
    def add_product_data_to_agreement_render_context(
        cls, context: Dict[str, Any]
    ) -> None:
        """
        Add agreement product and calculation data to agreement render context for preview.

        Args:
            context: Agreement render context

        Returns:
            None
        """
        product_calculation_data: Dict[
            str, Any
        ] = cls._get_product_calculation_data_for_agreement_preview(context)
        context.update(product_preview_data=product_calculation_data)

        pax8_products_preview_data: Dict[
            str, Any
        ] = cls._get_pax8_product_calculation_data_for_agreement_preview(
            context.get("pax8_products")
        )
        context.update(pax8_products_preview_data=pax8_products_preview_data)

        total_product_extended_cost: Union[
            int, float
        ] = product_calculation_data.get("estimated_annual_cost", 0)
        total_pax8_product_extended_cost: Union[
            int, float
        ] = pax8_products_preview_data.get("billing_summary", {}).get(
            "estimated_pax8_annual_cost", 0
        )
        total_agreement_extended_cost: float = round(
            (total_pax8_product_extended_cost + total_product_extended_cost), 2
        )
        if total_agreement_extended_cost > 0:
            context.update(
                total_agreement_extended_cost=total_agreement_extended_cost
            )
        return

    @classmethod
    def generate_agreement_pdf_preview(
        cls, context: Dict[str, Any]
    ) -> Dict[str, str]:
        """
        Generate agreement PDF preview.

        Args:
            context: Agreement render context

        Returns:
            Tuple(file_path, file_name)
        """
        cls.add_product_data_to_agreement_render_context(context)
        cls.add_extra_info_to_template_context(context)
        cls.parse_special_variables_in_agreement_sections(context)
        file_name: str = cls.get_agreement_pdf_file_name(context)
        rendered_template_html: str = cls.get_rendered_html_str(
            cls.TEMPLATE_DIR, cls.AGREEMENT_PDF, context
        )
        header: Dict = cls.get_header_for_pdf_preview(context)
        footer: Dict = cls.get_footer_for_pdf_preview(context)
        if settings.DEBUG:
            local_path: bool = True
        else:
            local_path: bool = False
        preview: Dict[str, str] = generate_pdf_using_pdf_shift(
            rendered_template_html,
            file_name,
            local_path=local_path,
            header=header,
            footer=footer,
        )
        return preview

    @staticmethod
    def get_agreement_context_from_model_instance(
        agreement: Agreement,
    ) -> Dict:
        """
        Get required context data from Agreement instance

        Args:
            agreement: Agreement

        Returns:
            Agreement context
        """
        required_field: List[str] = [
            "name",
            "sections",
            "products",
            "version_description",
            "discount_percentage",
            "pax8_products",
        ]
        template_payload: Dict = model_to_dict(
            agreement, fields=required_field
        )
        template_payload.update(
            author_name=agreement.author.name,
            version=agreement.version,
            company_name=agreement.customer.name,
            account_manager_name=agreement.customer.account_manager_name,
            customer_contact=agreement.user.name,
            customer_phone=agreement.user.profile.full_office_phone_number,
            updated_on=agreement.updated_on.strftime("%Y.%m.%d"),
        )
        return template_payload

    @staticmethod
    def get_context_from_validated_data(validated_data: Dict) -> Dict:
        """
        Get required context from validated data

        Args:
            validated_data: Validated request data

        Returns:
            Context for agreement
        """
        customer: Customer = validated_data.pop("customer")
        major_version = validated_data.pop("major_version", None)
        minor_version = validated_data.pop("minor_version", None)
        user = validated_data.pop("user")
        version: str = (
            "1.0"
            if major_version is None and minor_version is None
            else f"{major_version}.{minor_version}"
        )
        context_data: Dict = dict(
            company_name=customer.name,
            account_manager_name=customer.account_manager_name,
            customer_contact=user.name,
            customer_phone=user.profile.full_office_phone_number,
            version=version,
        )
        return context_data

    @staticmethod
    def parse_special_variables_in_agreement_sections(context: Dict) -> None:
        """
        Replace @{Customer Name} placeholder with customer name.
        Replace @{Page Break} placeholder with page break html.

        Args:
            context: Template render context

        Returns:
            None
        """
        company_name: str = context.get("company_name", "")
        sections: List[Dict] = context.pop("sections", [])
        sales_utils.substitute_customer_name_placeholder_in_agreement_sections(
            sections, company_name
        )
        sales_utils.substitute_page_break_placeholder_in_sections(sections)
        context.update(sections=sections)
        return

    @staticmethod
    def parse_special_variables_in_template_sections(context: Dict) -> None:
        """
        Replace @{Page Break} placeholder with page break html.

        Args:
            context: Template render context

        Returns:
            None
        """
        sections: List[Dict] = context.pop("sections", [])
        sales_utils.substitute_page_break_placeholder_in_sections(sections)
        context.update(sections=sections)
        return

    def download_agreement_pdf(self, agreement: Agreement) -> Dict[str, str]:
        """
        Download agreement PDF.

        Args:
            agreement: Agreement

        Returns:
            File download details.
        """
        agreement_payload: Dict = (
            self.get_agreement_context_from_model_instance(agreement)
        )
        agreement_pdf: Dict[str, str] = self.generate_agreement_pdf_preview(
            agreement_payload
        )
        return agreement_pdf

    @staticmethod
    def get_render_context_for_agreement_service_detail_document(
        agreement: Agreement,
    ) -> Dict[str, Any]:
        """
        Get render context for agreement service detail document.

        Args:
            agreement: Agreement

        Returns:
            Render context
        """
        render_context: Dict[str, Any] = dict()

        provider: Provider = agreement.provider

        quote_id: int = agreement.quote_id
        try:
            quote: Dict = QuoteService.get_quote(provider.id, quote_id)
        except Exception:
            logger.info(
                f"No Quote present for  agreement with ID: {agreement.id} with Quote ID as {quote_id}"
            )
            quote: Dict = dict()
        render_context.update(quote_name=quote.get("name", "-"))

        agreement_service: AgreementService = AgreementService(provider)
        customer_details: Dict = (
            agreement_service.get_customer_details_for_service_detail_document(
                agreement
            )
        )
        render_context.update(customer_details=customer_details)

        internal_contact_details: Dict = agreement_service.get_internal_contact_details_for_service_detail_document(
            agreement
        )
        render_context.update(internal_contacts=internal_contact_details)

        pax8_products_summary: Dict[
            str, Any
        ] = agreement_service.get_pax8_products_service_detail_summary(
            agreement
        )
        render_context.update(pax8_products_summary=pax8_products_summary)

        products_summary: Dict[
            str, Any
        ] = agreement_service.generate_products_service_detail_summary(
            agreement
        )
        render_context.update(products_summary=products_summary)

        financial_summary: Dict[
            str, Union[int, float]
        ] = agreement_service.generate_service_detail_financial_summary(
            products_summary, pax8_products_summary
        )
        render_context.update(financial_summary=financial_summary)

        current_date: str = date.today().strftime("%m/%d/%Y")
        render_context.update(current_date=current_date)
        return render_context

    @classmethod
    def get_rendered_agreement_service_detail_document(
        cls, render_context: Dict
    ) -> str:
        """
        Get rendered HTML for agreement service detail document.

        Args:
            render_context: Render context

        Returns:
            Rendered HTML for agreement service detail document.
        """
        service_detail_template_path: str = os.path.join(
            cls.TEMPLATE_DIR, cls.AGREEMENT_SERVICE_DETAIL
        )
        rendered_html_str: str = render_to_string(
            service_detail_template_path, render_context
        )
        return rendered_html_str

    @classmethod
    def _generate_agreement_service_detail_document_file_name(
        cls,
        agreement: Agreement,
    ) -> str:
        """
        Generate agreement service detail document file name.

        Args:
            agreement: Agreement

        Returns:
            Agreement service detail document file name.
        """
        # NOTE: The flow to delete existing service detail documents in CW is based on the naming
        # of service detail file name. First refer to the workflow first before making any changes.
        file_name: str = (
            f"{cls.SERVICE_DETAIL_PDF_PREFIX} - {agreement.name}.pdf"
        )
        return file_name

    @classmethod
    def generate_agreement_service_detail_pdf(
        cls, agreement: Agreement
    ) -> Dict[str, str]:
        """
        Generate agreement service detail document PDF.

        Args:
            agreement: Agreement

        Returns:
            Agreement service detail document PDF.
        """
        render_context: Dict[
            str, Any
        ] = cls.get_render_context_for_agreement_service_detail_document(
            agreement
        )
        rendered_service_detail_html: str = (
            cls.get_rendered_agreement_service_detail_document(render_context)
        )
        file_name: str = (
            cls._generate_agreement_service_detail_document_file_name(
                agreement
            )
        )
        if settings.DEBUG:
            local_path: bool = True
        else:
            local_path: bool = False
        preview: Dict[str, str] = generate_pdf_using_pdf_shift(
            rendered_service_detail_html,
            file_name,
            local_path=local_path,
            header=None,
            footer=None,
        )
        return preview

    @staticmethod
    def _get_products_calculation_data_for_agreement_template_preview(
        agreement_products: List[Dict],
    ) -> Dict[str, Any]:
        """
        Get product calculation data required for agreement template preview.

        Args:
            agreement_products: Agreement products

        Returns:
            Products calculation data (Dict)
        """
        # Please refer to products schema of agreement and templates for better understanding.
        if not agreement_products:
            return dict()

        product_list_for_preview: List = list()

        total_one_time_cost: int = 0
        total_monthly_cost: int = 0
        total_annual_cost: int = 0

        for product in agreement_products:
            product_bundle: Dict[str, Any] = dict()
            products_in_bundle: List[Dict] = list()

            billing_option: str = product.get("billing_option")
            product_calculator: Products = Products(
                billing_option=billing_option,
                product_type=product.get("product_type"),
                is_bundle=product.get("is_bundle"),
                id=product.get("id"),
                is_checked=product.get("is_checked"),
                products=product.get("products"),
                discount_percentage=0,
            )
            computed_products: List[Product] = product_calculator.products
            for computed_product in computed_products:
                products_in_bundle.append(
                    dict(
                        name=computed_product.name,
                        quantity=computed_product.quantity,
                        customer_cost=computed_product.original_customer_cost_per_unit,
                        extended_cost=computed_product.extended_cost,
                        description=computed_product.description,
                    )
                )
            product_bundle.update(
                billing_option=billing_option, products_list=products_in_bundle
            )
            product_list_for_preview.append(product_bundle)

            if billing_option == AgreementTemplate.ONE_TIME:
                total_one_time_cost += product_calculator.total_extended_cost
            elif billing_option == AgreementTemplate.PER_MONTH:
                total_monthly_cost += product_calculator.total_extended_cost
            elif billing_option == Agreement.PER_ANNUM:
                total_annual_cost += product_calculator.total_extended_cost

        preview_data: Dict[str, Any] = dict(
            products=product_list_for_preview,
            total_one_time_cost=total_one_time_cost,
            total_monthly_cost=total_monthly_cost,
            total_annual_cost=total_annual_cost,
        )
        return preview_data

    @classmethod
    def add_products_data_to_agreement_template_render_context(
        cls, render_context: Dict[str, Any]
    ) -> None:
        """
        Add product and pax8 product data to agreement template render context

        Args:
            render_context: Render context
        """
        products_preview_data: Dict[
            str, Any
        ] = cls._get_products_calculation_data_for_agreement_template_preview(
            render_context.get("products")
        )
        render_context.update(products_preview_data=products_preview_data)

        pax8_products_preview_data: Dict[
            str, Any
        ] = cls._get_pax8_products_calculation_data_for_preview(
            render_context.get("pax8_products")
        )
        render_context.update(
            pax8_products_preview_data=pax8_products_preview_data
        )
        return

    @classmethod
    def generate_agreement_template_pdf_preview(
        cls, render_context: Dict[str, Any]
    ) -> Dict[str, str]:
        """
        Generate agreement template PDF preview.

        Args:
            render_context: PDF render context

        Returns:
            Dict(file_path, file_name)
        """
        cls.add_products_data_to_agreement_template_render_context(
            render_context
        )
        cls.add_extra_info_to_template_context(render_context)
        cls.parse_special_variables_in_template_sections(render_context)
        html_template_name: str = cls.TEMPLATE_PDF
        rendered_template_html: str = cls.get_rendered_html_str(
            cls.TEMPLATE_DIR, html_template_name, render_context
        )
        file_name: str = f"{render_context.get('name')}.pdf"
        header: Dict = cls.get_header_for_pdf_preview(render_context)
        footer: Dict = cls.get_footer_for_pdf_preview(render_context)
        if settings.DEBUG:
            local_path: bool = True
        else:
            local_path: bool = False
        preview: Dict[str, str] = generate_pdf_using_pdf_shift(
            rendered_template_html,
            file_name,
            local_path=local_path,
            header=header,
            footer=footer,
        )
        return preview


class AgreementService:
    """
    Service class that handles operations related to agreements and templates.
    """

    def __init__(self, provider: Provider):
        self.provider: Provider = provider

    def create_forecast_for_products(
        self,
        quote_id: int,
        pax8_products: List[Dict],
        products: List[Dict],
        **kwargs,
    ) -> Dict[str, Any]:
        """
        Create forecast for normal products and PAX8 products.

        Args:
            quote_id: Quote ID
            pax8_products: PAX8 products.
            products: Products

        Returns:
            Created forecast data.
        """
        forecast_data: Dict[str, Any] = dict(
            forecast_id=None,
            forecast_error_details=dict(
                error_in_forecast_creation=False,
                error_message=None,
            ),
        )
        if not pax8_products and not products:
            return forecast_data

        try:
            forecast: Dict = (
                QuoteService.create_forecast_for_agreement_products(
                    self.provider, quote_id, pax8_products, products, **kwargs
                )
            )
            forecast_id: int = forecast.get("id")
            forecast_data: Dict = dict(
                forecast_id=forecast_id,
                forecast_error_details=dict(
                    error_in_forecast_creation=False,
                    error_message=None,
                ),
            )
        except ValidationError as err:
            error_message: str = f"Error creating forecast for opportunity ID: {quote_id}. Error: {err}"
            logger.error(error_message)
            forecast_data: Dict = dict(
                forecast_id=None,
                forecast_error_details=dict(
                    error_in_forecast_creation=True,
                    error_message=error_message,
                ),
            )
        return forecast_data

    def create_agreement(
        self,
        user: User,
        create_payload: Dict[str, Any],
    ) -> Agreement:
        """
        Create new agreement.

        For a new agreement:
            major_version=1
            minor_version=0
            is_current_vesion=True
            root_agreement=NULL
            parent_agreement=NULL

        Args:
            user: User
            create_payload: Create payload

        Returns:
            Agreement
        """
        quote_id: int = create_payload.get("quote_id")
        pax8_products: List[Dict] = create_payload.get(
            "pax8_products", dict()
        ).get("products", list())
        products: List[Dict] = create_payload.get("products", list())
        discount_percentage: int = create_payload.get("discount_percentage", 0)
        forecast_data: Dict = self.create_forecast_for_products(
            quote_id,
            pax8_products,
            products,
            discount_percentage=discount_percentage,
        )
        agreement: Agreement = Agreement.objects.create(
            provider=self.provider,
            author=user,
            updated_by=user,
            is_current_version=True,
            root_agreement=None,
            parent_agreement=None,
            forecast_data=forecast_data,
            **create_payload,
        )
        return agreement

    @staticmethod
    def _get_agreement_field_values_as_per_scenario(
        current_agreement: Agreement, user: User, query_params: Dict
    ) -> Dict:
        """
        Get values of major_version, minor_version, author depending on scenario. These values are
        provided by UI through query params.

        Args:
            current_agreement: Current version agreement
            user: User
            query_params: Query params

        Returns:
            Scenario specific values.
        """
        if json.loads(query_params.get("rollback", "false")):
            return dict(
                author=current_agreement.author,
                major_version=current_agreement.major_version + 1,
                minor_version=0,
            )
        else:
            set_user_as_author: bool = json.loads(
                query_params.get("set_user_as_author", "false")
            )
            author: User = (
                user if set_user_as_author else current_agreement.author
            )

            update_major_version: bool = query_params.get(
                "update_major_version"
            )
            if update_major_version:
                major_version: int = current_agreement.major_version + 1
                minor_version: int = 0
            else:
                major_version: int = current_agreement.major_version
                minor_version: int = current_agreement.minor_version + 1
            return dict(
                author=author,
                major_version=major_version,
                minor_version=minor_version,
            )

    def update_agreement(
        self,
        current_version_agreement: Agreement,
        user: User,
        update_payload: Dict[str, Any],
        query_params: Dict,
    ) -> Agreement:
        """
        Update agreement. Sets is_current_version of current agreement to False. Create a new
        agreement with updated values.
        For updated agreement:
            parent_agreement = current version agreement
            is_current_version = True

        The values of min_version, major_version, and author depend
        on scenario and are provided by UI through query_params.

        Args:
            current_version_agreement: Current version agreement
            user: Request user
            update_payload: Update payload
            query_params: Query params.

        Returns:
            Agreement with updated data.
        """
        # Mark current version agreement as False.
        current_version_agreement.is_current_version = False
        current_version_agreement.save()

        quote_id: int = update_payload.get("quote_id")
        pax8_products: List[Dict] = update_payload.get(
            "pax8_products", dict()
        ).get("products", list())
        discount_percentage: int = update_payload.get("discount_percentage", 0)
        products: List[Dict] = update_payload.get("products", list())
        forecast_data: Dict[str, Any] = self.create_forecast_for_products(
            quote_id,
            pax8_products,
            products,
            discount_percentage=discount_percentage,
        )
        update_payload.update(forecast_data=forecast_data)

        # Values of fields such as author, major_version, minor_version are scenario specific.
        # These values are selected depending on the query_params.
        scenario_specific_fields: Dict = (
            self._get_agreement_field_values_as_per_scenario(
                current_version_agreement, user, query_params
            )
        )
        new_version_agreement: Agreement = Agreement.objects.create(
            provider=current_version_agreement.provider,
            updated_by=user,
            is_current_version=True,
            root_agreement_id=current_version_agreement.get_root_agreement_id(),
            parent_agreement_id=current_version_agreement.id,
            **scenario_specific_fields,
            **update_payload,
        )
        return new_version_agreement

    @staticmethod
    def get_customer_details_for_service_detail_document(
        agreement: Agreement,
    ) -> Dict:
        """
        Get customer details for agreement service detail document.

        Contains details of:
            Customer name,
            Primary contact name,
            Primary contact phone number

        Args:
            agreement: Agreement

        Returns:
            Dict
        """
        customer_contact_details: Dict = get_customer_contact_details(
            agreement.provider, agreement.customer.crm_id
        )
        customer_contact_details.update(customer_name=agreement.customer.name)
        return customer_contact_details

    @staticmethod
    def get_internal_contact_details_for_service_detail_document(
        agreement: Agreement,
    ):
        """
        Get internal contact details for agreement service detail document.

        Contains details:
            territory_manager_name
            agreement_author_name

        Args:
            agreement: Agreement

        Returns:
            Dict
        """
        provider: Provider = agreement.provider
        customer: Dict = provider.erp_client.get_customer(
            agreement.customer.crm_id
        )
        territory_manager: Dict = provider.erp_client.get_member(
            customer.get("territory_manager_id")
        )
        internal_contact_details: Dict[str, str] = dict(
            territory_manager_name=f"{territory_manager.get('first_name')} {territory_manager.get('last_name')}",
            agreement_author_name=agreement.author.name,
        )
        return internal_contact_details

    @staticmethod
    def get_pax8_products_data(
        agreement: Agreement,
    ) -> List[Dict]:
        """
        Get pax8 products data.

        Args:
            agreement: Agreement

        Returns:
            PAX8 products data
        """
        pax8_products: List[Dict] = agreement.pax8_products.get(
            "products", None
        )
        if not pax8_products:
            return list()
        else:
            pax8_calculator: PAX8Calculator = PAX8Calculator(
                pax8_products_list=pax8_products
            )
            pax8_products_data: List[
                Dict
            ] = pax8_calculator.get_summary_of_each_product()
            return pax8_products_data

    @staticmethod
    def _calculate_margin_percent(
        margin: Union[int, float], base_cost: Union[int, float]
    ) -> Union[int, float]:
        try:
            margin_percent: Union[int, float] = round(
                (margin / base_cost) * 100, 2
            )
        except ZeroDivisionError:
            margin_percent = 0
        return margin_percent

    def _generate_summary_for_vendor_products(
        self,
        computed_pax8_products: List["PAX8ProductCalculator"],
    ) -> Dict[str, Union[int, float]]:
        """
        Generate summary for given vendor products.

        Args:
            computed_pax8_products: List of PAX8ProductCalculator instances

        Returns:
            Summary for all vendor pax8 products.
        """
        # NOTE: All the products in computed_pax8_products have the same billing term.
        total_extended_cost: float = round(
            sum([product.extended_cost for product in computed_pax8_products]),
            2,
        )
        total_internal_cost: float = round(
            sum(
                [
                    product.total_internal_cost
                    for product in computed_pax8_products
                ]
            ),
            2,
        )
        total_margin: float = round(
            sum([product.total_margin for product in computed_pax8_products]),
            2,
        )
        margin_percent: float = self._calculate_margin_percent(
            total_margin, total_extended_cost
        )
        # Calculate annual values
        annual_extended_cost: float = round(
            sum(
                [
                    product.annual_extended_cost
                    for product in computed_pax8_products
                ]
            ),
            2,
        )
        annual_internal_cost: float = round(
            sum(
                [
                    product.annual_internal_cost
                    for product in computed_pax8_products
                ]
            ),
            2,
        )
        annual_margin: float = round(
            sum([product.annual_margin for product in computed_pax8_products]),
            2,
        )
        return dict(
            total_extended_cost=total_extended_cost,
            total_internal_cost=total_internal_cost,
            total_margin=total_margin,
            margin_percent=margin_percent,
            annual_extended_cost=annual_extended_cost,
            annual_internal_cost=annual_internal_cost,
            annual_margin=annual_margin,
        )

    def _generate_annual_summary(
        self,
        annual_summary_line_items: List[Dict],
    ) -> Dict[str, Union[int, float]]:
        """
        Generate annual summary of all products.

        Args:
            annual_summary_line_items: List of annual summary line items

        Returns:
            Annual summary
        """
        annual_extended_cost: int = 0
        annual_internal_cost: int = 0
        annual_margin: int = 0

        annual_extended_cost += round(
            sum(
                [
                    item.get("annual_revenue")
                    for item in annual_summary_line_items
                ]
            ),
            2,
        )
        annual_internal_cost += round(
            sum(
                [item.get("annual_cost") for item in annual_summary_line_items]
            ),
            2,
        )
        annual_margin += round(
            sum(
                [
                    item.get("annual_margin")
                    for item in annual_summary_line_items
                ]
            ),
            2,
        )
        annual_margin_percent: float = self._calculate_margin_percent(
            annual_margin, annual_extended_cost
        )
        return dict(
            total_annual_revenue=annual_extended_cost,
            total_annual_cost=annual_internal_cost,
            total_annual_margin=annual_margin,
            annual_margin_percent=annual_margin_percent,
        )

    def _generate_summary_of_vendors(
        self, vendor_details: List[Dict[str, Union[int, float]]]
    ) -> Dict[str, Union[int, float]]:
        """
        Generate summary of all vendors.

        Args:
            vendor_details: List of vendor summaries.

        Returns:
            Final summary of all vendors.
        """
        total_revenue: float = round(
            sum([vendor.get("revenue") for vendor in vendor_details]),
            2,
        )
        total_cost: float = round(
            sum([vendor.get("cost") for vendor in vendor_details]),
            2,
        )
        total_margin: float = round(
            sum([vendor.get("margin") for vendor in vendor_details]),
            2,
        )
        total_margin_percent: float = self._calculate_margin_percent(
            total_margin, total_revenue
        )
        return dict(
            total_revenue=total_revenue,
            total_cost=total_cost,
            total_margin=total_margin,
            margin_percent=total_margin_percent,
        )

    def get_pax8_products_service_detail_summary(
        self,
        agreement: Agreement,
    ) -> Dict:
        """
        Get pax8 products service detail summary.

        Args:
            agreement: Agreement

        Returns:
            PAX8 service detail summary
        """
        if not agreement.pax8_products.get("products", []):
            return dict()

        pax8_products: List[Dict] = agreement.pax8_products.get("products")
        computed_pax8_products: List[PAX8ProductCalculator] = [
            PAX8ProductCalculator(
                billing_term=product.get("billing_term"),
                margin_percent=product.get("margin"),
                partner_buy_rate=product.get("partner_buy_rate"),
                product_name=product.get("product_name"),
                quantity=product.get("quantity"),
                sku=product.get("sku"),
                vendor_name=product.get("vendor_name"),
                short_description=product.get("short_description"),
                commitment_term=product.get("commitment_term"),
            )
            for product in pax8_products
            if not product.get("is_comment")
        ]

        vendor_summary_by_billing_term: Dict[str, Any] = dict()
        annual_summary_line_items: List[Dict] = list()

        products_grouped_by_billing_term: Dict[
            str, List[PAX8ProductCalculator]
        ] = group_pax8_products_by_billing_term(computed_pax8_products)

        for billing_term, products in products_grouped_by_billing_term.items():
            products_grouped_by_vendors: Dict[
                str, List[PAX8ProductCalculator]
            ] = group_pax8_products_by_vendor(products)

            # Summary for a vendor for current billing term
            vendor_summary_line_items: List[Dict] = list()

            for vendor, products_list in products_grouped_by_vendors.items():
                vendor_products_summary: Dict[
                    str, Union[int, float]
                ] = self._generate_summary_for_vendor_products(products_list)
                vendor_summary_line_items.append(
                    dict(
                        billing_term=billing_term,
                        vendor_name=vendor,
                        revenue=vendor_products_summary.get(
                            "total_extended_cost"
                        ),
                        cost=vendor_products_summary.get(
                            "total_internal_cost"
                        ),
                        margin=vendor_products_summary.get("total_margin"),
                        margin_percent=vendor_products_summary.get(
                            "margin_percent"
                        ),
                    )
                )
                annual_summary_line_items.append(
                    dict(
                        billing_term=billing_term,
                        vendor_name=vendor,
                        annual_revenue=vendor_products_summary.get(
                            "annual_extended_cost"
                        ),
                        annual_cost=vendor_products_summary.get(
                            "annual_internal_cost"
                        ),
                        annual_margin=vendor_products_summary.get(
                            "annual_margin"
                        ),
                        annual_margin_percent=vendor_products_summary.get(
                            "margin_percent"
                        ),
                    )
                )

            all_vendors_summary: Dict[
                str, Any
            ] = self._generate_summary_of_vendors(vendor_summary_line_items)
            billing_term_summary: Dict[str, Any] = dict(
                billing_term=billing_term,
                vendors=vendor_summary_line_items,
                total_revenue=all_vendors_summary.get("total_revenue"),
                total_cost=all_vendors_summary.get("total_cost"),
                total_margin=all_vendors_summary.get("total_margin"),
                total_margin_percent=all_vendors_summary.get("margin_percent"),
            )
            vendor_summary_by_billing_term.update(
                {billing_term: billing_term_summary}
            )
        annual_summary: Dict[str, Any] = self._generate_annual_summary(
            annual_summary_line_items
        )
        return dict(
            annual_summary=annual_summary,
            vendor_summary_by_billing_term=vendor_summary_by_billing_term,
            annual_summary_line_items=annual_summary_line_items,
        )

    @staticmethod
    def generate_pax8_products_summary_for_agreement(
        agreement: Agreement,
    ) -> Dict[str, Union[int, float]]:
        """
        Get financial summary of pax8 products.

        Args:
            agreement: Agreement

        Returns:
            PAX8 products summary
        """
        pax8_products: List[Dict] = agreement.pax8_products.get(
            "products", None
        )
        if not pax8_products:
            return dict()

        pax8_calculator: PAX8Calculator = PAX8Calculator(
            pax8_products_list=pax8_products
        )
        pax8_summary: Dict[
            str, float
        ] = pax8_calculator.get_summary_of_all_products()
        return pax8_summary

    @staticmethod
    def _get_computed_products(
        products: List[Dict], discount_percent: int
    ) -> List[Product]:
        computed_products: List[Product] = list()
        if not products:
            return computed_products

        for product in products:
            if product.get("product_type") == Agreement.COMMENT:
                continue
            products_list: List[Dict] = product.get("products")
            selected_product_index: Optional[int] = product.get(
                "selected_product"
            )
            if selected_product_index is not None:
                selected_product: Dict = products_list[selected_product_index]
                computed_product: Product = Product(
                    name=selected_product.get("name"),
                    description=selected_product.get("description"),
                    original_internal_cost_per_unit=selected_product.get(
                        "internal_cost"
                    ),
                    original_customer_cost_per_unit=selected_product.get(
                        "customer_cost"
                    ),
                    margin_percent=selected_product.get("margin"),
                    quantity=selected_product.get("quantity"),
                    billing_option=product.get("billing_option"),
                    discount_percentage=discount_percent,
                )
                computed_products.append(computed_product)
        return computed_products

    def _get_computed_products_grouped_by_billing_option(
        self, products: List[Dict], discount_percent: int
    ) -> Dict[str, List[Product]]:
        computed_products: List[Product] = self._get_computed_products(
            products, discount_percent
        )
        products_grouped_by_billing_option: Dict[
            str, List[Product]
        ] = group_list_of_items_by_key(
            computed_products, lambda product: product.billing_option
        )
        return products_grouped_by_billing_option

    @staticmethod
    def _get_product_details(
        products_list: List[Product],
    ) -> Tuple[List[Dict], List[Dict]]:
        product_details: List[Dict] = list()
        annual_product_details: List[Dict] = list()

        for product in products_list:
            product_details.append(
                dict(
                    name=product.name,
                    revenue=product.extended_cost,
                    cost=product.total_internal_cost_for_all_units,
                    margin=product.total_margin_for_all_units,
                    margin_percent=product.discounted_margin_percent,
                    billing_option=product.billing_option,
                )
            )
            annual_product_details.append(
                dict(
                    name=product.name,
                    annual_revenue=product.annual_extended_cost,
                    annual_cost=product.annual_internal_cost,
                    annual_margin=product.annual_margin,
                    annual_margin_percent=product.discounted_margin_percent,
                    billing_option=product.billing_option,
                )
            )
        return product_details, annual_product_details

    def generate_products_service_detail_summary(
        self, agreement: Agreement
    ) -> Dict[str, Any]:
        service_detail_summary: Dict[str, Any] = dict()

        agreement_products: List[Dict] = agreement.products
        if not agreement_products:
            return service_detail_summary

        products_grouped_by_billing_option: Dict[
            str, List[Product]
        ] = self._get_computed_products_grouped_by_billing_option(
            agreement_products, agreement.discount_percentage
        )

        billing_option_summary: Dict[str, Any] = dict()
        annual_summary_line_items: List[Dict] = list()

        for (
            billing_option,
            product_list,
        ) in products_grouped_by_billing_option.items():
            (
                product_details,
                annual_product_details,
            ) = self._get_product_details(product_list)
            total_revenue: float = round(
                sum([product.get("revenue") for product in product_details]), 2
            )
            total_cost: float = round(
                sum([product.get("cost") for product in product_details]), 2
            )
            total_margin: float = round(
                sum([product.get("margin") for product in product_details]),
                2,
            )
            margin_percent: float = self._calculate_margin_percent(
                total_margin, total_revenue
            )
            billing_option_summary[billing_option] = dict(
                revenue=total_revenue,
                cost=total_cost,
                margin=total_margin,
                margin_percent=margin_percent,
                products=product_details,
            )
            annual_summary_line_items.extend(annual_product_details)

        annual_summary: Dict[
            str, Union[int, float]
        ] = self._generate_annual_summary(annual_summary_line_items)
        return dict(
            annual_summary=annual_summary,
            billing_option_summary=billing_option_summary,
            annual_summary_line_items=annual_summary_line_items,
        )

    @staticmethod
    def get_products_data(agreement: Agreement) -> List[Dict]:
        """
        Get agreement products data.
        Contains following details:
            name,
            description,
            internal_cost,
            customer_cost,
            margin,
            margin_percent,
            quantity,
            billing_option,
            extended_cost,
            total_internal_cost,

        Args:
            agreement: Agreement

        Returns:
            List of Dicts
        """
        agreement_products: List[Dict] = agreement.products
        if not agreement_products:
            return list()

        products_data: List[Dict] = list()
        for product in agreement_products:
            products_list: List[Dict] = product.get("products")
            selected_product_index: Optional[int] = product.get(
                "selected_product"
            )
            if selected_product_index is not None:
                selected_product: Dict = products_list[selected_product_index]
                # Product class performs calculation for each product
                product_instance: Product = Product(
                    name=selected_product.get("name"),
                    description=selected_product.get("description"),
                    original_internal_cost_per_unit=selected_product.get(
                        "internal_cost"
                    ),
                    original_customer_cost_per_unit=selected_product.get(
                        "customer_cost"
                    ),
                    margin_percent=selected_product.get("margin"),
                    quantity=selected_product.get("quantity"),
                    billing_option=product.get("billing_option"),
                    discount_percentage=agreement.discount_percentage,
                )
                products_data.append(
                    dict(
                        name=product_instance.name,
                        description=product_instance.description,
                        internal_cost=product_instance.original_internal_cost_per_unit,
                        customer_cost=product_instance.original_customer_cost_per_unit,
                        margin=product_instance.total_margin_for_all_units,
                        margin_percent=product_instance.margin_percent,
                        quantity=product_instance.quantity,
                        billing_option=product_instance.billing_option,
                        extended_cost=product_instance.extended_cost,
                        total_internal_cost=product_instance.total_internal_cost_for_all_units,
                    )
                )
        return products_data

    def get_products_summary(self, agreement: Agreement) -> Dict:
        """
        Get financial summary of agreement products.
        Contains following details:
            total_revenue,
            total_cost,
            margin,
            margin_percent,

        Args:
            agreement: Agreement

        Returns:
            Financial summary data.
        """
        products: List[Dict] = self.get_products_data(agreement)
        if not products:
            return dict()

        total_revenue: int = 0
        total_cost: int = 0
        for product in products:
            total_revenue += product.get("extended_cost")
            total_cost += product.get("total_internal_cost")

        total_margin: int = total_revenue - total_cost
        try:
            margin_percent: float = round(
                (total_margin / total_revenue) * 100, 2
            )
        except ZeroDivisionError:
            margin_percent = 0.00
        return dict(
            total_revenue=total_revenue,
            total_cost=total_cost,
            margin=total_margin,
            margin_percent=margin_percent,
        )

    def generate_service_detail_financial_summary(
        self,
        products_summary: Dict[str, Any],
        pax8_products_summary: Dict[str, Any],
    ) -> Dict[str, Union[int, float]]:
        """
        Generate service detail financial summary for agreement. Contains details such as total revenue, total cost,
        total margin, margin percent.

        Args:
            products_summary: Products summary
            pax8_products_summary: PAX8 products summary


        Returns:
            Financial summary of agreement
        """
        # pax8_products_summary: Dict = (
        #     self.generate_pax8_products_summary_for_agreement(agreement)
        # )
        # products_summary: Dict = self.get_products_summary(agreement)
        # Calculate financial summary taking into account both products and pax8 products.

        total_revenue: float = round(
            products_summary.get("annual_summary", {}).get(
                "total_annual_revenue", 0
            )
            + pax8_products_summary.get("annual_summary", {}).get(
                "total_annual_revenue", 0
            ),
            2,
        )
        total_cost: float = round(
            products_summary.get("annual_summary", {}).get(
                "total_annual_cost", 0
            )
            + pax8_products_summary.get("annual_summary", {}).get(
                "total_annual_cost", 0
            ),
            2,
        )
        total_margin: float = round(
            products_summary.get("annual_summary", {}).get(
                "total_annual_margin", 0
            )
            + pax8_products_summary.get("annual_summary", {}).get(
                "total_annual_margin", 0
            ),
            2,
        )
        total_margin_percent: float = self._calculate_margin_percent(
            total_margin, total_revenue
        )
        return dict(
            agreement_revenue=total_revenue,
            agreement_cost=total_cost,
            agreement_margin=total_margin,
            agreement_margin_percent=total_margin_percent,
        )
