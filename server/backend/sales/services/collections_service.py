from accounts.models import Provider, User, Customer
from typing import Dict, List, Optional, Union, Tuple
from sales.models import AutomatedCollectionNoticesSettings
from django.template.loader import render_to_string
from core.utils import group_list_of_dictionary_by_key
import os
from datetime import datetime, date
from django.utils import timezone
from utils import (
    convert_utc_datetime_to_provider_timezone,
    change_precision_of_number,
    add_comma_to_number_in_string_format,
)
from django.conf import settings
from core.utils import convert_date_string_to_date_object, device_date_formats
from factory import faker
from factory.fuzzy import FuzzyText
from sales.emails import AutomatedCollectionNoticeEmail


class CollectionInvoicesService:
    AUTOMATED_COLLECTION_NOTICE_TEMPLATE: str = (
        "collection_notice_email_body.html"
    )
    SENT_EMAIL_DETAILS_FORMAT: str = "%A, %B %-d, %Y %-I:%M %p"

    def __init__(self, provider: Provider):
        self.provider = provider

    def get_provider_user_details(self, user_email_id: str) -> Dict:
        """
        Get details of provider user from DB

        Args:
            user_email_id: User email ID

        Returns:
            Provider user details
        """
        provider_user_details: Dict = {}
        try:
            provider_user: User = User.providers.get(
                provider_id=self.provider.id, email=user_email_id
            )
        except User.DoesNotExist:
            return provider_user_details
        provider_user_details.update(
            name=provider_user.name,
            email=user_email_id,
            phone_number=provider_user.profile.full_office_phone_number,
            title=provider_user.profile.title,
            email_signature=provider_user.profile.email_signature.get(
                "email_signature", None
            ),
        )
        return provider_user_details

    @staticmethod
    def exclude_pattern_matched(
        invoice_number: str, exclude_patterns: List[str]
    ) -> bool:
        """
        Matches exclude patterns with the invoice number.

        Args:
            invoice_number: Invoice number
            exclude_patterns: Pattern to match

        Returns:
            True if any pattern matched else False
        """

        for pattern in exclude_patterns:
            if pattern.startswith("*") and invoice_number.endswith(
                pattern[1:]
            ):
                return True
            elif pattern.endswith("*") and invoice_number.startswith(
                pattern[:-1]
            ):
                return True
        return False

    def filter_invoices_using_exclude_patterns(
        self, invoices_data: List[Dict], exclude_patterns: List[str]
    ) -> List[Optional[Dict]]:
        """
        Exclude invoices that start or end with exclude patterns configured in Automated Collection Notices Setting.

        Args:
            invoices_data: Invoices data
            exclude_patterns: Exclude patterns configured in Automated Collection Notices Setting

        Returns:
            Filtered out invoices
        """
        required_invoices = [
            invoice
            for invoice in invoices_data
            if not (
                self.exclude_pattern_matched(
                    invoice.get("invoice_number"), exclude_patterns
                )
            )
        ]
        return required_invoices

    @staticmethod
    def split_past_and_approaching_due_date_invoices(
        invoices: List[Dict],
    ) -> Dict[str, List[Dict]]:
        """
        Split the invoices among past due and approaching due date type.

        Args:
            invoices: Invoices

        Returns:
            Modified invoices data split among past due and approaching due date type.
        """
        current_date: date = timezone.now().date()
        invoices_past_due_date: List[Dict] = []
        invoices_with_approaching_due_date: List[Dict] = []
        for invoice in invoices:
            due_date = convert_date_string_to_date_object(
                invoice.get("due_date"), settings.ISO_FORMAT
            )
            if due_date <= current_date:
                delta = current_date - due_date
                days_past_due_date: int = delta.days
                invoice.update(days_past_due_date=days_past_due_date)
                invoices_past_due_date.append(invoice)
            else:
                delta = due_date - current_date
                approaching_due_date_in_days: int = delta.days
                invoice.update(
                    approaching_due_date_in_days=approaching_due_date_in_days
                )
                invoices_with_approaching_due_date.append(invoice)
        return dict(
            invoices_past_due_date=invoices_past_due_date,
            invoices_with_approaching_due_date=invoices_with_approaching_due_date,
        )

    def get_invoices_data_from_cw(
        self,
        automated_collection_notices_setting: AutomatedCollectionNoticesSettings,
        customer_crm_id: int,
    ) -> List[Optional[Dict]]:
        """
        Fetch invoice data from CW. Fetch invoices having balance greate than 0 that are
        due and also those that are going to be due in approaching_due_date_ageing days.

        Args:
            automated_collection_notices_setting: Automated Collection Notices Setting
            customer_crm_id: Customer CRM ID

        Returns:
            Invoices data
        """
        approaching_due_date_ageing: int = (
            automated_collection_notices_setting.approaching_due_date_ageing
        )
        invoice_status_ids: List[
            Optional[int]
        ] = automated_collection_notices_setting.get_billing_status_ids()
        current_datetime: datetime = timezone.now()
        cw_invoices_data: List[Dict] = self.provider.erp_client.get_invoices(
            approaching_due_date_ageing,
            invoice_status_ids,
            [customer_crm_id],
            current_datetime=current_datetime,
            all=True,
        )
        return cw_invoices_data

    @staticmethod
    def format_due_date_of_invoices(
        invoice_data: Dict[str, List[Dict]]
    ) -> None:
        """
        Format due date of invoices as required in email notice.

        Args:
            invoice_data: Customer invoices

        Returns:
            Invoice data with formatted due date
        """
        date_formats: Dict = device_date_formats()
        required_format: str = date_formats.get("MM/DD/YYYY")
        for invoice in invoice_data["invoices_past_due_date"]:
            initial_due_date = convert_date_string_to_date_object(
                invoice.get("due_date"), settings.ISO_FORMAT
            )
            invoice["due_date"] = initial_due_date.strftime(required_format)
        for invoice in invoice_data["invoices_with_approaching_due_date"]:
            initial_due_date = convert_date_string_to_date_object(
                invoice.get("due_date"), settings.ISO_FORMAT
            )
            invoice["due_date"] = initial_due_date.strftime(required_format)
        return

    def get_invoices_data_for_customers(
        self, customer_crm_id: int
    ) -> Dict[str, List[Dict]]:
        """
        Get due invoices for customer.

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Due invoices data
        """
        automated_collection_notice_setting: AutomatedCollectionNoticesSettings = (
            self.provider.automated_collection_notices_setting
        )
        invoices: List[Dict] = self.get_invoices_data_from_cw(
            automated_collection_notice_setting, customer_crm_id
        )
        if not invoices:
            return {}
        exclude_patterns: Optional[
            List[str]
        ] = automated_collection_notice_setting.invoice_exclude_patterns
        required_invoices: List[Dict] = (
            self.filter_invoices_using_exclude_patterns(
                invoices, exclude_patterns
            )
            if exclude_patterns
            else invoices
        )
        if not required_invoices:
            return {}
        invoices_data: Dict[
            str, List[Dict]
        ] = self.split_past_and_approaching_due_date_invoices(
            required_invoices
        )
        self.format_due_date_of_invoices(invoices_data)
        self.format_balance_of_invoices(invoices_data)
        return invoices_data

    def get_customer_accounting_contact_details(
        self, customer_crm_id: int, contact_type_crm_id: int
    ) -> Dict:
        """
        Get details of customer accounting contact from CW.

        Args:
            customer_crm_id: Customer CW ID
            contact_type_crm_id: Contact type CW ID

        Returns:
            Contact details
        """
        customer_accounting_contact_details: Dict = {}
        customer_users = self.provider.erp_client.get_customer_users(
            customer_id=customer_crm_id,
            active_users_only=True,
            type_filter=contact_type_crm_id,
        )
        # Select the first contact
        if customer_users:
            customer_user: Dict = customer_users[0]
            customer_accounting_contact_details: Dict = dict(
                first_name=customer_user.get("first_name"),
                last_name=customer_user.get("last_name"),
                title=customer_user.get("title"),
                email=customer_user.get("email"),
            )
        return customer_accounting_contact_details

    def get_automated_collection_notice_email_template_path(self) -> str:
        """
        Get automated collection notice email template path.

        Returns:
            Automated collection notice email template path.
        """
        directory_path: str = (
            AutomatedCollectionNoticesSettings.get_emails_template_directory_path()
        )
        template_path: str = os.path.join(
            directory_path, self.AUTOMATED_COLLECTION_NOTICE_TEMPLATE
        )
        return template_path

    def get_rendered_automated_collection_notice_email_body(
        self, template_context: Dict
    ) -> str:
        """
        Get rendered collection notice email body.

        Args:
            template_context: Template context

        Returns:
            Rendered template
        """
        template_path: str = (
            self.get_automated_collection_notice_email_template_path()
        )
        email_body: str = render_to_string(template_path, template_context)
        return email_body

    def get_email_sent_details(self) -> str:
        """
        Get datetime details of when email is sent
        """
        current_datetime = timezone.now()
        provider_datetime = convert_utc_datetime_to_provider_timezone(
            current_datetime, self.provider.timezone
        )
        datetime_details: str = provider_datetime.strftime(
            self.SENT_EMAIL_DETAILS_FORMAT
        )
        return datetime_details

    def get_email_context_data(self) -> Dict:
        """
        Get email context data such as sender email, subject, to emails list, etc.

        Returns:
            Email context data
        """
        automated_collection_notices_setting: AutomatedCollectionNoticesSettings = (
            self.provider.automated_collection_notices_setting
        )
        sender_details: Dict = self.get_provider_user_details(
            automated_collection_notices_setting.send_from_email
        )
        send_to_emails_list: List[
            str
        ] = automated_collection_notices_setting.send_to_email
        subject: str = automated_collection_notices_setting.subject
        email_sent_details: str = self.get_email_sent_details()
        email_body_context: Dict = dict(
            sender_details=sender_details,
            email_sent_details=email_sent_details,
            subject=subject,
            email_body=automated_collection_notices_setting.email_body_template,
            to_emails_list=send_to_emails_list,
        )
        return email_body_context

    def get_collection_notice_email_preview(self) -> str:
        """
        Generate preview of collection notice email. Populates customer data with arbitrary values.
        The subject, email body, etc. field values are populated with automated collection notice email setting values.

        Returns:
            Preview of collection notice email.
        """
        customer_name_for_preview: str = "CUSTOMER NAME"
        customer_contact_details_for_preview: Dict = dict(
            first_name="CUSTOMER_CONTACT_NAME",
            email="customer-contact-email@site.com",
        )
        customer_invoice_data: Dict = dict(
            invoices_past_due_date=[
                dict(
                    invoice_number="INVOICE NUMBER 123",
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"BILLING TERM NAME",
                    due_date="DUE DATE",
                    balance="BALANCE",
                    days_past_due_date="NO OF DAYS",
                )
            ],
            invoices_with_approaching_due_date=[
                dict(
                    invoice_number="INVOICE NUMBER 456",
                    customer_po=FuzzyText().fuzz(),
                    billing_term_name=f"BILLING TERM NAME",
                    due_date="DUE DATE",
                    balance="BALANCE",
                    approaching_due_date_in_days="NO OF DAYS",
                )
            ],
        )
        email_body_context_data: Dict = self.get_email_context_data()
        subject_prefix: str = email_body_context_data.get("subject")
        subject: str = subject_prefix + " - " + customer_name_for_preview
        email_body_context_data.update(
            dict(
                customer_name=customer_name_for_preview,
                customer_invoice_data=customer_invoice_data,
                subject=subject,
                customer_accounting_contact_details=customer_contact_details_for_preview,
            )
        )
        rendered_email_body: str = (
            self.get_rendered_automated_collection_notice_email_body(
                email_body_context_data
            )
        )
        return rendered_email_body

    @staticmethod
    def get_formatted_balance(balance: Union[float, int]) -> str:
        """
        Formats balance by adding comma at thousandth place with precision at 2 decimal places.
        E.g. If balance=123456.456, returns 123,456.46

        Args:
            balance: Invoice balance

        Returns:
            Formatted balance
        """
        balance: str = change_precision_of_number(balance, 2)
        balance: str = add_comma_to_number_in_string_format(balance)
        return balance

    def format_balance_of_invoices(
        self, invoice_data: Dict[str, List[Dict]]
    ) -> None:
        """
        Formats balance of invoices.

        Args:
            invoice_data: Invoice data

        Returns:
            Invoice data with formatted balance.
        """
        for invoice in invoice_data["invoices_past_due_date"]:
            formatted_balance: str = self.get_formatted_balance(
                invoice.get("balance", 0)
            )
            invoice.update(balance=formatted_balance)

        for invoice in invoice_data["invoices_with_approaching_due_date"]:
            formatted_balance: str = self.get_formatted_balance(
                invoice.get("balance", 0)
            )
            invoice.update(balance=formatted_balance)
        return

    def send_collection_notice_email_for_customer(
        self, customer: Customer
    ) -> Tuple[bool, Optional[str]]:
        """
        Send collection notice email for customer

        Args:
            customer: Customer

        Returns:
            Boolean indicating if email sent, error message if any.
        """
        invoice_data: Dict[
            str, List[Dict]
        ] = self.get_invoices_data_for_customers(customer.crm_id)
        if not invoice_data:
            return False, "Invoices not found for customer!"

        customer_name: str = customer.name
        email_body_context_data: Dict = self.get_email_context_data()
        subject_prefix: str = email_body_context_data.get("subject")
        subject: str = subject_prefix + " - " + customer_name
        contact_type_crm_id: int = (
            self.provider.automated_collection_notices_setting.contact_type_crm_id
        )
        customer_contact_details: Dict = (
            self.get_customer_accounting_contact_details(
                customer.crm_id, contact_type_crm_id
            )
        )
        email_body_context_data.update(
            dict(
                customer_name=customer_name,
                customer_invoice_data=invoice_data,
                subject=subject,
                customer_accounting_contact_details=customer_contact_details,
                logo=settings.LOOKINGPOINT_LOGO,
            )
        )
        rendered_email_body: str = (
            self.get_rendered_automated_collection_notice_email_body(
                email_body_context_data
            )
        )
        AutomatedCollectionNoticeEmail(
            context=dict(
                provider=self.provider,
                email_body=rendered_email_body,
                subject=subject,
            ),
            files=None,
            remote_attachments=None,
            images=None,
            subject=subject,
            from_email=email_body_context_data.get("sender_details").get(
                "email"
            ),
        ).send(
            to=email_body_context_data.get("to_emails_list"),
            log_error=False,
        )
        return True, None
