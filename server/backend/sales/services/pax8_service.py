from sales.models import PAX8Product, PAX8ProductPricingInfo
from accounts.models import Provider
from utils import DbOperation
from typing import List, Dict, Set, Optional, Union, Tuple
from sales.serializers import (
    PAX8ProductPricingInfoCreateSerializer,
    PAX8ProductCreateSerializer,
    PAX8ProductPricingInfoUpdateSerializer,
    PAX8ProductUpdateSerializer,
)
from django.utils import timezone
from utils import get_app_logger
from django.db import IntegrityError

logger = get_app_logger(__name__)


class PAX8ProductsDataPayloadHandler:
    """
    Handles PAX8 products data sync operations.
    """

    def __init__(
        self,
        provider: Provider,
        action: DbOperation,
        payload: Optional[Dict] = None,
    ):
        self.provider = provider
        self.action = action
        self.payload = payload
        self.pax8_product_service = PAX8ProductsService(self.provider)

    def transform_payload(self) -> Dict:
        """
        Transforms product data payload as required for saving in DB.
        """
        transformed_payload: Dict = dict()
        transformed_payload.update(provider=self.provider.id)
        transformed_payload.update(crm_id=self.payload.pop("id"))
        transformed_payload.update(
            vendor_name=self.payload.pop("vendorName", None)
        )
        transformed_payload.update(name=self.payload.pop("name", None))
        transformed_payload.update(
            short_description=self.payload.pop("shortDescription", None)
        )
        transformed_payload.update(sku=self.payload.pop("sku", None))
        transformed_payload.update(
            vendor_sku=self.payload.pop("vendorSku", None)
        )
        transformed_payload.update(
            alt_vendor_sku=self.payload.pop("altVendorSku", None)
        )
        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the PAX8 product data payload before create or update operation.
        """
        serializer = PAX8ProductCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = PAX8ProductUpdateSerializer
        pax8_product_data_serializer = serializer(data=self.payload)
        is_valid: bool = pax8_product_data_serializer.is_valid(
            raise_exception=False
        )
        if not is_valid:
            errors: List = pax8_product_data_serializer.errors
            return is_valid, errors
        self.payload = pax8_product_data_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_ids: Optional[List[str]] = None) -> None:
        """
        Creates, updates or deletes the PAX8 product in DB depending on whether the
        product exists in DB or not.
        """
        if self.action == DbOperation.UPDATE:
            crm_id: str = self.payload.pop("crm_id")
            product_exists_in_db: bool = (
                self.pax8_product_service.pax8_product_exists_in_db(crm_id)
            )
            if product_exists_in_db:
                self.pax8_product_service.update_pax8_product(
                    crm_id, self.payload
                )
        elif self.action == DbOperation.CREATE:
            self.pax8_product_service.create_pax8_product(self.payload)
        else:
            self.pax8_product_service.delete_pax8_products(crm_ids)

    def process(self) -> Tuple[bool, List]:
        success: bool = True
        errors: List = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action()
            else:
                return is_valid, errors
        else:
            crm_ids_of_products_to_delete: List[str] = self.payload.pop(
                "crm_ids_of_products_to_delete"
            )
            self.perform_action(crm_ids_of_products_to_delete)
        return success, errors


class PAX8ProductsService:
    def __init__(self, provider: Provider):
        self.provider = provider

    def update_pax8_product(self, crm_id: str, payload: Dict) -> None:
        """
        Update pax8 product in DB.

        Args:
            crm_id: PAX8 product CRM ID
            payload: PAX8Product Update payload

        Returns:
            None
        """
        payload.update(updated_on=timezone.now())
        PAX8Product.objects.filter(
            provider_id=self.provider.id, crm_id=crm_id
        ).update(**payload)
        return None

    @staticmethod
    def create_pax8_product(payload: Dict) -> Optional[PAX8Product]:
        """
        Creates PAX8 product in DB.

        Args:
            payload: PAX8Product create payload

        Returns:
            PAX8Product instance
        """
        try:
            pax8_product: PAX8Product = PAX8Product.objects.create(**payload)
        except IntegrityError as err:
            logger.error(
                f"Error creating PAX8 product. Payload: {payload}. "
                f"Error: {err}"
            )
            return None
        return pax8_product

    def delete_pax8_products(self, crm_ids: List[str]) -> None:
        """
        Deletes pax8 products from DB

        Args:
            crm_ids: List of PAX8 product CRM IDs

        Returns:
            None
        """
        PAX8Product.objects.filter(
            provider_id=self.provider.id, crm_id__in=crm_ids
        ).delete()
        return

    def pax8_product_exists_in_db(self, crm_id: str) -> bool:
        """
        Checks if PAX8 product with given CRM ID exists in DB or not.

        Args:
            crm_id: PAX8 product CRM ID

        Returns:
            True if exists else False
        """
        return PAX8Product.objects.filter(
            provider_id=self.provider.id, crm_id=crm_id
        ).exists()

    def pax8_product_pricing_data_exists_in_db(self, product_id: int) -> bool:
        """
        Checks if product pricing data exists for a product exists in DB or not.

        Args:
            product_id: PAX8Product ID

        Returns:
            True if exists else False
        """
        return PAX8ProductPricingInfo.objects.filter(
            provider_id=self.provider.id, pax8_product_id=product_id
        ).exists()

    def update_pax8_product_pricing_data(
        self, pax8_product_id: int, payload: Dict
    ) -> None:
        """
        Updates pax8 product pricing data in DB.

        Args:
            pax8_product_id: PAX8Product ID
            payload: PAX8ProductPricingInfo update payload

        Returns:
            None
        """
        payload.update(updated_on=timezone.now())
        PAX8ProductPricingInfo.objects.filter(
            provider_id=self.provider.id, pax8_product_id=pax8_product_id
        ).update(**payload)
        return None

    @staticmethod
    def create_pax8_product_pricing_data(
        payload: Dict,
    ) -> Optional[PAX8ProductPricingInfo]:
        """
        Create pax8 product pricing data in DB.

        Args:
            payload: PAX8ProductPricingInfo create payload.

        Returns:
            PAX8ProductPricingInfo instance
        """
        try:
            pax8_product_pricing_data: PAX8ProductPricingInfo = (
                PAX8ProductPricingInfo.objects.create(**payload)
            )
        except IntegrityError as err:
            logger.error(
                f"Error creating PAX8 product pricing data. Payload: {payload}. "
                f"Error: {err}"
            )
            return None
        return pax8_product_pricing_data

    def get_id_of_pax8_product(self, product_crm_id: str) -> Optional[int]:
        """
        Get DB ID of PAX8Product with given crm ID.

        Args:
            product_crm_id: PAX8 product CRM ID.

        Returns:
            DB ID of PAX8Product if exist else None
        """
        try:
            pax8_product: PAX8Product = PAX8Product.objects.get(
                provider_id=self.provider.id, crm_id=product_crm_id
            )
        except PAX8Product.DoesNotExist:
            logger.error(
                f"PAX8 product with crm_id: {product_crm_id} does not exist for "
                f"provider: {self.provider.name}, ID: {self.provider.id}"
            )
            return None
        return pax8_product.id


class PAX8ProductPricingDataPayloadHandler:
    """
    Handles PAX8 product pricing data sync operations.
    """

    def __init__(
        self,
        provider: Provider,
        action: DbOperation,
        payload: Optional[Dict] = None,
    ):
        self.provider = provider
        self.action = action
        self.payload = payload
        self.pax8_product_service = PAX8ProductsService(self.provider)

    def extract_payload(self):
        extracted_payload: Dict = dict()
        extracted_payload.update(
            product_crm_id=self.payload.pop("product_crm_id")
        )
        pricing_data: List[Dict] = list()
        content: List[Dict] = self.payload.pop("content", list())
        for item in content:
            data: Dict = dict()
            data.update(
                billing_term=item.get("billingTerm", None),
                commitment_term=item.get("commitmentTerm", None),
                commitment_term_in_months=item.get(
                    "commitmentTermInMonths", None
                ),
                pricing_type=item.get("type", None),
                unit_of_measurement=item.get("unitOfMeasurement", None),
            )
            rates: List[Dict] = item.get("rates", list())
            pricing_rates: List[Dict] = list()
            for rate in rates:
                pricing_rates.append(
                    dict(
                        partner_buy_rate=rate.get("partnerBuyRate", None),
                        suggested_retail_price=rate.get(
                            "suggestedRetailPrice", None
                        ),
                        start_quantity_range=rate.get(
                            "startQuantityRange", None
                        ),
                        end_quantity_range=rate.get("endQuantityRange", None),
                        charge_type=rate.get("chargeType", None),
                    )
                )
            data.update(pricing_rates=pricing_rates)
            pricing_data.append(data)
        extracted_payload.update(pricing_data=pricing_data)
        self.payload = extracted_payload

    def transform_payload(self) -> Dict:
        """
        Transforms product data pricing payload as required for saving in DB.
        """
        transformed_payload: Dict = dict()
        transformed_payload.update(provider=self.provider.id)
        product_crm_id: str = self.payload.pop("product_crm_id")
        pax8_product_id: int = (
            self.pax8_product_service.get_id_of_pax8_product(product_crm_id)
        )
        transformed_payload.update(pax8_product=pax8_product_id)
        transformed_payload.update(
            pricing_data=self.payload.pop("pricing_data", list())
        )
        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the PAX8 product pricing data payload before create or update operation.
        """
        serializer = PAX8ProductPricingInfoCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = PAX8ProductPricingInfoUpdateSerializer
        pax8_product_pricing_data_serializer = serializer(data=self.payload)
        is_valid: bool = pax8_product_pricing_data_serializer.is_valid(
            raise_exception=False
        )
        if not is_valid:
            errors: List = pax8_product_pricing_data_serializer.errors
            return is_valid, errors
        self.payload: Dict = (
            pax8_product_pricing_data_serializer.validated_data
        )
        return is_valid, list()

    def perform_action(self) -> None:
        """
        Creates, updates or deletes the PAX8 product pricing data in DB depending on whether the
        pricing data exists in DB or not.
        """
        if self.action == DbOperation.UPDATE:
            pax8_product_id: int = self.payload.pop("pax8_product")
            product_pricing_data_exists_in_db: bool = self.pax8_product_service.pax8_product_pricing_data_exists_in_db(
                pax8_product_id
            )
            if product_pricing_data_exists_in_db:
                self.pax8_product_service.update_pax8_product_pricing_data(
                    pax8_product_id, self.payload
                )
        elif self.action == DbOperation.CREATE:
            self.pax8_product_service.create_pax8_product_pricing_data(
                self.payload
            )
        return

    def process(self) -> Tuple[bool, List]:
        success: bool = True
        errors: List = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action()
            else:
                return is_valid, errors
        return success, errors
