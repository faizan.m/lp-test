from dataclasses import dataclass, field
from typing import List, Dict, Optional


class PAX8BillingTerms:
    """
    All PAX8 billing terms. The terms may change in future.
    """

    MONTHLY: str = "Monthly"
    ANNUAL: str = "Annual"
    ONE_TIME: str = "One-Time"
    TRIAL: str = "Trial"
    THREE_YEAR: str = "3-Year"
    TWO_YEAR: str = "2-Year"


@dataclass
class PAX8ProductCalculator:
    """
    Dataclass that performs various calcualtions for individual PAX8 product.
    All needed calculations are initialized as instance attributes.
    """

    billing_term: str
    margin_percent: int
    partner_buy_rate: float
    product_name: str
    quantity: int
    sku: str
    vendor_name: str
    short_description: str
    commitment_term: str
    internal_cost_per_unit: float = field(init=False)
    customer_cost_per_unit: float = field(init=False)
    margin_per_unit: float = field(init=False)
    total_margin: float = field(init=False)
    extended_cost: float = field(init=False)
    total_internal_cost: float = field(init=False)
    annual_extended_cost: float = field(init=False)
    annual_internal_cost: float = field(init=False)
    annual_margin: float = field(init=False)

    def __post_init__(self):
        self.internal_cost_per_unit: float = round(self.partner_buy_rate, 2)
        self.customer_cost_per_unit: float = self._calculate_customer_cost(
            self.margin_percent, self.internal_cost_per_unit
        )
        self.margin_per_unit: float = self._calculate_margin_per_unit(
            self.customer_cost_per_unit, self.internal_cost_per_unit
        )
        self.total_margin: float = self._calculate_total_margin(
            self.margin_per_unit, self.quantity
        )
        # Extended cost is total revenue of a pax8 product
        self.extended_cost: float = self._calculate_extended_cost(
            self.customer_cost_per_unit, self.quantity
        )
        self.total_internal_cost: float = self._calculate_total_internal_cost(
            self.internal_cost_per_unit, self.quantity
        )
        self.annual_extended_cost: float = (
            self._calculate_annual_extended_cost(self.extended_cost)
        )
        self.annual_internal_cost: float = (
            self._calculate_annual_internal_cost(self.total_internal_cost)
        )
        self.annual_margin: float = self._calculate_annual_margin(
            self.total_margin
        )

    @staticmethod
    def _calculate_customer_cost(
        margin_percent: int, internal_cost: float
    ) -> float:
        """
        Calculate customer cost.

        Args:
            margin_percent: Margin percent
            internal_cost: Internal cost

        Returns:
            Customer cost
        """
        customer_cost: float = round(
            (internal_cost / (1 - (margin_percent / 100))), 2
        )
        return customer_cost

    @staticmethod
    def _calculate_margin_per_unit(
        customer_cost: float, internal_cost: float
    ) -> float:
        """
        Calculate margin per unit.

        Args:
            customer_cost: Customer cost
            internal_cost: Internal cost

        Returns:
            Maring per unit
        """
        margin: float = round((customer_cost - internal_cost), 2)
        return margin

    @staticmethod
    def _calculate_total_margin(
        margin_per_unit: float, quantity: int
    ) -> float:
        """
        Calculate total margin for all units.

        Args:
            margin_per_unit: Margin for one unit
            quantity: Quantity of units

        Returns:
            Total margin
        """
        total_margin: float = round(margin_per_unit * quantity, 2)
        return total_margin

    @staticmethod
    def _calculate_extended_cost(customer_cost: float, quantity: int) -> float:
        """
        Calculate extended cost.

        Args:
            customer_cost: Customer cost
            quantity: Quantity of units

        Returns:
            Extended cost
        """
        extended_cost: float = round(customer_cost * quantity, 2)
        return extended_cost

    @staticmethod
    def _calculate_total_internal_cost(
        internal_cost_per_unit: float, quantity: int
    ) -> float:
        """
        Calculate total internal cost for all units

        Args:
            internal_cost_per_unit: Internal cost per unit
            quantity: Product quantity

        Returns:
            Total internal cost
        """
        total_internal_cost: float = round(
            internal_cost_per_unit * quantity, 2
        )
        return total_internal_cost

    def _calculate_annual_extended_cost(self, extended_cost: float) -> float:
        """
        Calculate annual extended cost.

        Args:
            extended_cost: Extended cost

        Returns:
            Annual extended cost
        """
        if self.billing_term == PAX8BillingTerms.MONTHLY:
            annual_extended_cost: float = round(extended_cost * 12, 2)
        else:
            annual_extended_cost: float = extended_cost
        return annual_extended_cost

    def _calculate_annual_internal_cost(
        self, total_internal_cost: float
    ) -> float:
        """
        Calculate annual internal cost.

        Args:
            total_internal_cost: Internal cost for all units

        Returns:
            Annual internal cost
        """
        if self.billing_term == PAX8BillingTerms.MONTHLY:
            annual_internal_cost: float = round(total_internal_cost * 12, 2)
        else:
            annual_internal_cost: float = total_internal_cost
        return annual_internal_cost

    def _calculate_annual_margin(self, total_margin: float) -> float:
        """
        Calculate annual margin.

        Args:
            total_margin: Total margin for all units.

        Returns:
            Annual margin
        """
        if self.billing_term == PAX8BillingTerms.MONTHLY:
            annual_margin: float = round(total_margin * 12, 2)
        else:
            annual_margin: float = total_margin
        return annual_margin


@dataclass
class PAX8Calculator:
    """
    A dataclass that performs calculations for PAX8 products.
    Calculations for individual PAX8 product are performed by PAX8ProductCalculator class.
    This class performs calculations for all PAX8 products.
    """

    pax8_products_list: List[Dict]
    _pax8_products: List[PAX8ProductCalculator] = field(init=False)
    _calculation_data: Dict = field(init=False)

    def __post_init__(self):
        if not self.pax8_products_list:
            self._pax8_products: List = list()
            self._calculation_data: Dict = dict()
        else:
            self._pax8_products: List[PAX8ProductCalculator] = [
                PAX8ProductCalculator(
                    billing_term=product.get("billing_term"),
                    margin_percent=product.get("margin"),
                    partner_buy_rate=product.get("partner_buy_rate"),
                    product_name=product.get("product_name"),
                    quantity=product.get("quantity"),
                    sku=product.get("sku"),
                    vendor_name=product.get("vendor_name"),
                    short_description=product.get("short_description", ""),
                    commitment_term=product.get("commitment_term", ""),
                )
                for product in self.pax8_products_list
                if not product.get("is_comment")
            ]
            self._calculation_data: Dict = self._calculate_all_values()

    def _calculate_all_values(self) -> Dict:
        total_revenue: float = self._calculate_total_revenue(
            self._pax8_products
        )
        total_cost: float = self._calculate_total_cost(self._pax8_products)
        total_margin: float = self._calculate_total_margin(self._pax8_products)
        total_margin_percent: float = self._calculate_total_margin_percent(
            total_margin, total_revenue
        )
        return dict(
            total_revenue=total_revenue,
            total_cost=total_cost,
            total_margin=total_margin,
            total_margin_percent=total_margin_percent,
        )

    @staticmethod
    def _calculate_total_revenue(
        pax8_products: List[PAX8ProductCalculator],
    ) -> float:
        """
        Calculate total revenue by adding extended cost of all pax8 products.

        Args:
            pax8_products: List of PAX8ProductCalculator instances

        Returns:
            Total revenue
        """
        total_revenue: float = 0.0
        for product in pax8_products:
            total_revenue += product.extended_cost
        return round(total_revenue, 2)

    @staticmethod
    def _calculate_total_cost(
        pax8_products: List[PAX8ProductCalculator],
    ) -> float:
        """
        Calculate total cost by adding internal cost of all pax8 products.

        Args:
            pax8_products: List of PAX8ProductCalculator instances

        Returns:
            Total cost
        """
        total_cost: float = 0.0
        for product in pax8_products:
            total_cost += product.total_internal_cost
        return round(total_cost, 2)

    @staticmethod
    def _calculate_total_margin(
        pax8_products: List[PAX8ProductCalculator],
    ) -> float:
        """
        Calculate total margin by adding total margin of all pax8 products.

        Args:
            pax8_products: List of PAX8ProductCalculator instances

        Returns:
            Total margin
        """
        total_margin: float = 0.0
        for product in pax8_products:
            total_margin += product.total_margin
        return round(total_margin, 2)

    @staticmethod
    def _calculate_total_margin_percent(
        total_margin: float, total_revenue: float
    ) -> float:
        """
        Calculate total margin percent.

        Args:
            total_margin: Total margin
            total_revenue: Total revenue

        Returns:
            Total margin percent
        """
        try:
            total_margin_percent: float = round(
                (total_margin / total_revenue) * 100, 2
            )
        except ZeroDivisionError:
            total_margin_percent = 0.00
        return total_margin_percent

    def get_summary_of_each_product(self) -> List[Dict]:
        """
        Get summary of each PAX8 product.

        Returns:
            List of dicts.
        """
        products_summary: List[Dict] = [
            dict(
                product_name=product.product_name,
                sku=product.sku,
                quantity=product.quantity,
                internal_cost=product.internal_cost_per_unit,
                customer_cost=product.customer_cost_per_unit,
                margin_per_unit=product.margin_per_unit,
                total_margin=product.total_margin,
                extended_cost=product.extended_cost,
                billing_term=product.billing_term,
                vendor_name=product.vendor_name,
                short_description=product.short_description,
                commitment_term=product.commitment_term,
                total_internal_cost=product.total_internal_cost,
                margin_percent=product.margin_percent,
            )
            for product in self._pax8_products
        ]
        return products_summary

    def get_summary_of_all_products(self) -> Dict[str, float]:
        """
        Get summary of all pax8 products.

        Returns:
            PAX8 products summary.
        """
        return dict(
            total_revenue=self._calculation_data.get("total_revenue"),
            total_cost=self._calculation_data.get("total_cost"),
            total_margin=self._calculation_data.get("total_margin"),
            total_margin_percent=self._calculation_data.get(
                "total_margin_percent"
            ),
        )
