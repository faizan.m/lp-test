from dataclasses import dataclass, field
from typing import List, Dict, Optional

from sales.models import AgreementTemplate
from utils import get_app_logger

logger = get_app_logger(__name__)

MONTHS_IN_YEAR = 12


@dataclass
class Product:
    name: str
    description: str
    original_internal_cost_per_unit: float
    original_customer_cost_per_unit: float
    margin_percent: int
    quantity: int
    billing_option: str
    discount_percentage: int
    extended_cost: float = field(init=False)
    discounted_customer_cost: float = field(init=False)
    margin_per_unit: float = field(init=False)
    total_margin_for_all_units: int = field(init=False)
    total_internal_cost_for_all_units: int = field(init=False)

    def __post_init__(self):
        # margin_percent is margin percent per unit
        self.discount_percentage: int = self._get_discount_percent(
            self.discount_percentage
        )
        initial_margin_per_unit: float = round(
            self.original_customer_cost_per_unit
            - self.original_internal_cost_per_unit,
            2,
        )
        discount_per_unit: float = self._calculate_discount_per_unit(
            self.discount_percentage, initial_margin_per_unit
        )
        # Margin per unit after discount
        self.margin_per_unit: float = (
            self._calculate_discounted_margin_per_unit(
                initial_margin_per_unit, discount_per_unit
            )
        )
        self.discounted_customer_cost: float = (
            self._calculate_discounted_customer_cost(
                self.original_customer_cost_per_unit, discount_per_unit
            )
        )
        self.extended_cost: int = self._calculate_extended_cost()
        self.total_margin_for_all_units: int = self._calculate_total_margin(
            self.margin_per_unit, self.quantity
        )
        self.total_internal_cost_for_all_units: int = (
            self._calculate_total_internal_cost(
                self.original_internal_cost_per_unit, self.quantity
            )
        )

    @property
    def annual_extended_cost(self) -> int:
        """
        Annual extended cost
        """
        annual_extended_cost: int = self.extended_cost
        if self.billing_option == AgreementTemplate.PER_MONTH:
            annual_extended_cost: int = annual_extended_cost * 12
        return annual_extended_cost

    @property
    def annual_internal_cost(self) -> int:
        """
        Annual internal cost.
        """
        annual_internal_cost: int = self.total_internal_cost_for_all_units
        if self.billing_option == AgreementTemplate.PER_MONTH:
            annual_internal_cost: int = annual_internal_cost * 12
        return annual_internal_cost

    @property
    def annual_margin(self) -> int:
        """
        Annual margin.
        """
        annual_margin: int = self.total_margin_for_all_units
        if self.billing_option == AgreementTemplate.PER_MONTH:
            annual_margin: int = annual_margin * 12
        return annual_margin

    @property
    def discounted_margin_percent(self) -> float:
        """
        The margin percent for extended cost. This is after discount has been applied.
        """
        try:
            margin_percent: float = round(
                (self.annual_margin / self.annual_extended_cost) * 100, 2
            )
        except ZeroDivisionError:
            margin_percent = 0
        return margin_percent

    def _get_discount_percent(self, discount_percentage: int) -> int:
        """
        Get discount percent as per billing option.

        Args:
            discount_percentage: discount_percentage

        Returns:
            Discount percent
        """
        # Discount percent does not apply to One Time billing option
        discount_percent: int = (
            0
            if self.billing_option == AgreementTemplate.ONE_TIME
            else discount_percentage
        )
        return discount_percent

    @staticmethod
    def _calculate_discount_per_unit(
        discount_percent: int, initial_margin_per_unit: float
    ):
        """
        Calculate discount per unit.

        Args:
            discount_percent: Discount percent
            initial_margin_per_unit: Maring per unit without discount

        Returns:
            Discount per unit.
        """
        discount: float = round(
            (discount_percent / 100) * initial_margin_per_unit, 2
        )
        return discount

    @staticmethod
    def _calculate_discounted_margin_per_unit(
        initial_margin_per_unit: float, discount_per_unit: float
    ) -> float:
        """
        Calculate discounted margin per unit.

        Args:
            initial_margin_per_unit: Margin per unit before discount
            discount_per_unit: Discount per unit

        Returns:
            Discounted margin per unit
        """
        discounted_margin_per_unit: float = round(
            initial_margin_per_unit - discount_per_unit, 2
        )
        return discounted_margin_per_unit

    @staticmethod
    def _calculate_discounted_customer_cost(
        original_customer_cost_per_unit: float, discount_per_unit: float
    ) -> float:
        """
        Calculate discounted customer cost.

        Args:
            original_customer_cost_per_unit: Customer cost per unit before discount
            discount_per_unit: Discount per unit

        Returns:
            Discounted customer cost.
        """
        discounted_customer_cost: float = round(
            original_customer_cost_per_unit - discount_per_unit, 2
        )
        return discounted_customer_cost

    def _calculate_extended_cost(self) -> int:
        """
        Calculate extended cost.

        Returns:
            Extended cost.
        """
        extended_cost: float = self.discounted_customer_cost * self.quantity
        if self.billing_option == AgreementTemplate.PER_ANNUM:
            extended_cost *= MONTHS_IN_YEAR
        return round(extended_cost)

    def _calculate_total_margin(
        self, margin_per_unit: float, quantity: int
    ) -> int:
        """
        Calculate total margin for all units.

        Args:
            margin_per_unit: Discounted margin per unit.
            quantity: Products quantity

        Returns:
            Total margin for all units.
        """
        total_margin_for_all_units: float = margin_per_unit * quantity
        if self.billing_option == AgreementTemplate.PER_ANNUM:
            total_margin_for_all_units *= MONTHS_IN_YEAR
        return round(total_margin_for_all_units)

    def _calculate_total_internal_cost(
        self, original_internal_cost_per_unit: float, quantity: int
    ) -> int:
        """
        Calculate total internal cost.

        Args:
            original_internal_cost_per_unit: Internal cost per unit.
            quantity: Products quantity

        Returns:
            Total internal cost for all units.
        """
        total_internal_cost_for_all_units: float = (
            original_internal_cost_per_unit * quantity
        )
        if self.billing_option == AgreementTemplate.PER_ANNUM:
            total_internal_cost_for_all_units *= MONTHS_IN_YEAR
        return round(total_internal_cost_for_all_units)


@dataclass
class Products:
    billing_option: str
    product_type: str
    is_bundle: bool
    id: int
    is_checked: bool
    products: List
    discount_percentage: int
    selected_product_index: Optional[int] = None
    total_customer_cost: float = field(init=False)
    total_extended_cost: float = field(init=False)

    def __post_init__(self):
        self.total_customer_cost: float = 0
        self.total_extended_cost: float = 0
        initial_products_list: List[Dict] = self.products
        selected_product_index: Optional[int] = self.selected_product_index

        if selected_product_index is not None:
            selected_product: Dict = initial_products_list[
                selected_product_index
            ]
            selected_product_payload: Dict = self.get_product_payload(
                selected_product
            )
            product = Product(**selected_product_payload)
            self.total_customer_cost += product.discounted_customer_cost
            self.total_extended_cost += product.extended_cost
            self.products = [product]
        else:
            _products = []
            for product_dict in initial_products_list:
                payload = self.get_product_payload(product_dict)
                product = Product(**payload)
                _products.append(product)
                self.total_customer_cost += product.discounted_customer_cost
                self.total_extended_cost += product.extended_cost
            self.products = _products

    def get_product_payload(self, product: Dict) -> Dict:
        # Round off values for calculation, so that
        # it matches the values in preview
        product_info: Dict = dict(
            name=product.get("name"),
            description=product.get("description"),
            original_internal_cost_per_unit=round(
                product.get("internal_cost")
            ),
            original_customer_cost_per_unit=round(
                product.get("customer_cost")
            ),
            margin_percent=round(product.get("margin")),
            quantity=product.get("quantity"),
            billing_option=self.billing_option,
            discount_percentage=self.discount_percentage,
        )
        return product_info
