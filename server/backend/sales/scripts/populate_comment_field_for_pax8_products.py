from sales.models import Agreement, AgreementTemplate
from typing import List, Dict, Optional, Union
from accounts.models import Provider

import random


def populate_comment_field_for_existing_pax8_products(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Script to update existing pax8 products data of agreement and templates.

    Adds following fields to pax8 products schema:
        is_comment
        comment
        id

    Args:
        provider_ids: List of provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(is_active=True, id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        agreements: "QuerySet[Agreement]" = Agreement.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {agreements.count()} agreements for the Provider: {provider.name}, ID: {provider.id}."
        )
        for agreement in agreements:
            pax8_products: Dict = agreement.pax8_products
            products: List[Dict] = pax8_products.pop("products", [])
            minimum_one_product_updated: bool = False

            if products:
                for product in products:
                    product_fields: Set[str] = set(product.keys())
                    if "is_comment" not in product_fields:
                        product.update(is_comment=False)
                        minimum_one_product_updated = True
                    if "comment" not in product_fields:
                        product.update(comment=None)
                        minimum_one_product_updated = True
                    if "id" not in product_fields:
                        product.update(id=random.randint(1, 10**6))
                        minimum_one_product_updated = True

                if minimum_one_product_updated:
                    pax8_products.update(products=products)
                    Agreement.objects.filter(
                        provider_id=provider.id, id=agreement.id
                    ).update(pax8_products=pax8_products)
                    print(
                        f"Populated pax8 product data for agreement ID: {agreement.id}"
                    )

        templates: "QuerySet[AgreementTemplate]" = (
            AgreementTemplate.objects.filter(provider_id=provider.id)
        )
        for template in templates:
            pax8_products: Dict = template.pax8_products
            products: List[Dict] = pax8_products.pop("products", [])
            minimum_one_product_updated: bool = False

            if products:
                for product in products:
                    product_fields: Set[str] = set(product.keys())
                    if "is_comment" not in product_fields:
                        product.update(is_comment=False)
                        minimum_one_product_updated = True
                    if "comment" not in product_fields:
                        product.update(comment=None)
                        minimum_one_product_updated = True
                    if "id" not in product_fields:
                        product.update(id=random.randint(1, 10**6))
                        minimum_one_product_updated = True

                if minimum_one_product_updated:
                    pax8_products.update(products=products)
                    AgreementTemplate.objects.filter(
                        provider_id=provider.id, id=template.id
                    ).update(pax8_products=pax8_products)
                    print(
                        f"Populated pax8 product data for template ID: {template.id}"
                    )
        print(
            f"Updated pax8 product data for provider: {provider.name}, ID: {provider.id}"
        )
    print(f"Succesfully udpated agreement and templates!")
