from typing import List, Optional

from django.db.models import QuerySet

from accounts.models import Provider
from sales.models import Agreement
from utils import get_app_logger

logger = get_app_logger(__name__)


def update_min_and_max_discount_of_agreements(
    provider_ids: Optional[List[int]] = [],
):
    """
    Script to update min_discount and max_discount of agreements.
    If min_discount < 0, set min_discount = 0
    If max_discount <=0, set max_discount = 100

    Args:
        provider_ids: List of Provider IDs
    """
    providers: "Queryset[Provider]" = (
        Provider.objects.filter(id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    print(f"Found {providers.count()} Providers")
    for provider in providers:
        provider_id: int = provider.id
        min_discount_agreements: "QuerySet[Agreement]" = (
            Agreement.objects.filter(
                provider_id=provider_id, min_discount__lt=0
            )
        )
        min_discount_agreements_count: int = min_discount_agreements.count()

        if min_discount_agreements_count > 0:
            print(
                f"Found {min_discount_agreements_count} agreements with min_discount < 0 for the "
                f"Provider: {provider.name}, ID: {provider_id}"
            )
            try:
                min_discount_agreements.update(min_discount=0)
                print(
                    f"Updated min discount of agreements for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not update min discount of agreements for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )

        max_discount_agreements: "QuerySet[Agreement]" = (
            Agreement.objects.filter(
                provider_id=provider_id, max_discount__lte=0
            )
        )
        max_discount_agreements_count: int = max_discount_agreements.count()
        if max_discount_agreements_count > 0:
            print(
                f"Found {max_discount_agreements_count} agreements with max_discount <=0 for the "
                f"Provider: {provider.name}, ID: {provider_id}"
            )
            try:
                max_discount_agreements.update(max_discount=100)
                print(
                    f"Updated max discount of agreements for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not update max discount of agreements for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
    print(
        f"Agreements min_discount and max_discount update done for all providers!"
    )
