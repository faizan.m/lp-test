from sales.models import AgreementTemplate, Agreement
from typing import List, Set, Dict, Optional, Union
from accounts.models import Provider
from sales.models import AgreementTemplate, Agreement


def populate_is_required_and_comment_fields_for_existing_products(
    provider_ids: List[int] = None,
):
    """
    Script to populate is_required and comment values in agreement and template products for existing agreement and
    templates.

    Args:
        provider_ids: List of Provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        agreement_templates: "QuerySet[AgreementTemplate]" = (
            AgreementTemplate.objects.filter(provider_id=provider.id)
        )
        print(
            f"Found {agreement_templates.count()} agreement templates for Provider: {provider.name}, "
            f"ID: {provider.id}"
        )
        for template in agreement_templates:
            # This variable indicate if need to update agreement products or not.
            # Saves unnecessary update to DB.
            need_to_update_template: bool = False
            products: List[Dict] = template.products

            for product in products:
                product_fields: Set[str] = set(product.keys())
                if "comment" not in product_fields:
                    need_to_update_template = True
                    product.update(comment=None)
                if "is_required" not in product_fields:
                    need_to_update_template = True
                    product.update(is_required=True)
            if need_to_update_template:
                AgreementTemplate.objects.filter(
                    id=template.id, provider_id=provider.id
                ).update(products=products)
                print(f"Updated agreement template with ID: {template.id}")
        print(
            f"Populated is_required and comment values for agreement templates of provider: {provider.name}. \n"
        )

        agreements: "QuerySet[Agreement]" = Agreement.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {agreements.count()} agreement for the Provider: {provider.name}, "
            f"ID: {provider.id}"
        )
        for agreement in agreements:
            # This variable indicate if need to update agreement products or not.
            # Saves unnecessary update to DB.
            need_to_update_agreement: bool = False
            products: List[Dict] = agreement.products

            for product in products:
                product_fields: Set[str] = set(product.keys())
                if "comment" not in product_fields:
                    need_to_update_agreement = True
                    product.update(comment=None)
                if "is_required" not in product_fields:
                    need_to_update_agreement = True
                    product.update(is_required=True)
            if need_to_update_agreement:
                Agreement.objects.filter(
                    id=agreement.id, provider_id=provider.id
                ).update(products=products)
                print(f"Updated agreement with ID: {agreement.id}")
        print(
            f"Populated is_required and comment values for agreements of provider: {provider.name}. \n\n"
        )
    print(f"Successfully populated data for agreement products!")
