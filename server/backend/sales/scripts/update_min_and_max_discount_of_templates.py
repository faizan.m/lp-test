from typing import List, Optional

from django.db.models import QuerySet

from accounts.models import Provider
from sales.models import AgreementTemplate
from utils import get_app_logger

logger = get_app_logger(__name__)


def update_min_and_max_discount_of_agreement_templates(
    provider_ids: Optional[List[int]] = [],
):
    """
    Script to update  min discount and max discount of existing templates
    If min_discount < 0, set min_discount = 0
    If max_discount <=0, set max_discount = 100

    Args:
        provider_ids: List of Provider IDs
    """
    providers: "Queryset[Provider]" = (
        Provider.objects.filter(id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    print(f"Found {providers.count()} Providers")
    for provider in providers:
        provider_id: int = provider.id
        # Templates with min discont less than 0
        min_discount_agreement_templates: "QuerySet[AgreementTemplate]" = (
            AgreementTemplate.objects.filter(
                provider_id=provider_id, min_discount__lt=0
            )
        )
        min_discount_agreement_templates_count: int = (
            min_discount_agreement_templates.count()
        )
        if min_discount_agreement_templates_count > 0:
            print(
                f"Found {min_discount_agreement_templates_count} agreement templates with min discount "
                f"less than 0 for the "
                f"Provider: {provider.name}, ID: {provider_id}"
            )
            try:
                # If min discount less than 0, then set min_discount = 0
                min_discount_agreement_templates.update(min_discount=0)
                print(
                    f"Updated min discount of agreement templates for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not update min discount of agreement templates for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )

        # Templates with max discount <= 0
        max_discount_agreement_templates: "QuerySet[AgreementTemplate]" = (
            AgreementTemplate.objects.filter(
                provider_id=provider_id, max_discount__lte=0
            )
        )
        max_discount_agreement_templates_count = (
            max_discount_agreement_templates.count()
        )
        if max_discount_agreement_templates_count > 0:
            print(
                f"Found {max_discount_agreement_templates_count} agreement templates with max discount <= 0 "
                f"for the Provider: {provider.name}, ID: {provider_id}"
            )
            try:
                # If max_discount <= 0, then set max_discount = 100
                max_discount_agreement_templates.update(max_discount=100)
                print(
                    f"Updated max discount of agreement templates for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not update max discount of agreement templates for the "
                    f"Provider: {provider.name}, ID: {provider_id}"
                )
    print(
        f"Agreement template max discount and min discount update done for all providers!"
    )
