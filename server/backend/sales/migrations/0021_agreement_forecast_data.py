# Generated by Django 3.2.14 on 2023-02-08 09:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0020_auto_20221230_0655'),
    ]

    operations = [
        migrations.AddField(
            model_name='agreement',
            name='forecast_data',
            field=models.JSONField(default=dict),
        ),
    ]
