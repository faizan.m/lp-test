# Generated by Django 3.2.14 on 2022-10-14 09:50

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0107_auto_20220715_0743'),
        ('sales', '0015_merge_0014_auto_20220715_0636_0014_auto_20220825_0930'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutomatedCollectionNoticesSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weekly_send_schedule', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(choices=[('Monday', 'Monday'), ('Tuesday', 'Tuesday'), ('Wednesday', 'Wednesday'), ('Thursday', 'Thursday'), ('Friday', 'Friday'), ('Saturday', 'Saturday'), ('Sunday', 'Sunday')], max_length=15), size=None)),
                ('send_to_email', django.contrib.postgres.fields.ArrayField(base_field=models.EmailField(max_length=254), size=None)),
                ('send_from_email', models.EmailField(max_length=254)),
                ('invoice_exclude_patterns', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=20), blank=True, default=list, size=None)),
                ('billing_statuses', models.JSONField(default=list)),
                ('approaching_due_date_ageing', models.IntegerField()),
                ('email_body_template', models.TextField()),
                ('subject', models.CharField(max_length=200)),
                ('provider', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='automated_collection_notices_setting', to='accounts.provider')),
            ],
        ),
    ]
