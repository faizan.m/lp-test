from core.exceptions import InvalidConfigurations


class AutomatedCollectionNoticeSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Automated Collection Notice Setting nor Configured for the Provider!"
    )


class PAX8SettingsNotConfigured(InvalidConfigurations):
    default_detail = (
        "PAX8 integration settings not configured for the Provider!"
    )
