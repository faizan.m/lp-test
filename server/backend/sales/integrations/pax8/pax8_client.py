import datetime
import json
from typing import Dict, Tuple

import requests
from requests import Response, Session, Request
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from exceptions.exceptions import build_error
from utils import get_app_logger
from typing import Optional, List, Dict, Union, Generator

# Get an instance of a logger
logger = get_app_logger(__name__)


class PAX8Client:

    AUTH_URL: str = "https://token-manager.pax8.com/auth/token"
    AUDIENCE: str = "api://p8p.client"

    def __init__(self, client_id: str, client_secret: str) -> None:
        self._client_id = client_id
        self._client_secret = client_secret
        if not (self._client_id and self._client_secret):
            raise ValueError("Must provide client_id and client_secret.")
        self._auth_token = None
        self._token_expiry = None
        if not hasattr(self, "_session"):
            self._create_session()

    def _set_auth_token(self, token: str) -> None:
        """
        Sets auth token.

        Args:
            token: PAX8 auth token
        """
        self._auth_token = token

    def _set_token_expiry(self, duration: int) -> None:
        """
        Sets token expiry datetime.

        Args:
            duration: Duration in seconds
        """
        self._token_expiry = datetime.datetime.now() + datetime.timedelta(
            seconds=duration
        )

    @staticmethod
    def _get_auth_token_request_header() -> Dict:
        """
        Get request headers for obtaining auth token from PAX8.

        Returns:
            Request headers
        """
        headers: Dict = {"Content-Type": "application/json"}
        return headers

    def _get_default_request_headers(self) -> Dict:
        """
        Get default request headers for PAX8 requests.

        Returns:
            Request headers
        """
        headers: Dict = {
            "Authorization": f"Bearer {self._auth_token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
        return headers

    def _get_auth_payload(self) -> Dict:
        """
        Get request payload for obtaining auth token.

        Returns:
            Request payload
        """
        payload: Dict = dict(
            clientId=self._client_id,
            clientSecret=self._client_secret,
            audience=self.AUDIENCE,
        )
        return payload

    def _generate_auth_token(self) -> Tuple[str, int]:
        """
        Generate auth token for PAX8 requests.

        Returns:
            Auth token, token expiry
        """
        auth_payload: Dict = self._get_auth_payload()
        payload: str = json.dumps(auth_payload)
        headers: Dict = self._get_auth_token_request_header()
        response: Response = requests.post(
            self.AUTH_URL, headers=headers, data=payload
        )
        if not str(response.status_code).startswith("2"):
            raise self._build_api_error(
                response,
                message="Error generating PAX8 auth token!",
                errors=response.json().get("errors"),
            )
        response_data: Dict = response.json()
        token: str = response_data.get("token")
        expiry: int = response_data.get("expiresIn")
        return token, expiry

    def _token_has_expired(self) -> bool:
        """
        Checks if auth token has expired.

        Returns:
            True if token has expired else False
        """
        now = datetime.datetime.now()
        return self._token_expiry < now

    def _build_session(self) -> Session:
        """
        Builds request session.

        Returns:
            None
        """
        default_headers: Dict = self._get_default_request_headers()
        session = requests.Session()
        retry = Retry(total=3, backoff_factor=0.1)
        adapter = HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        session.headers = default_headers
        return session

    def _create_session(self) -> None:
        """
        Create a request sesssion using PAX8 auth token.
        Sets instance attributes: _auth_token, _token_expiry
        """
        auth_token, expiry = self._generate_auth_token()
        self._set_auth_token(auth_token)
        self._set_token_expiry(expiry)
        self._session = self._build_session()

    def _get(self, url, params: Dict = dict(), **kwargs) -> Response:
        """
        Performs GET request using session.

        Args:
            url: Request URL
            params: Query params
            **kwargs: keyword arguments

        Returns:
            Response object
        """
        return self._session.get(url, params=params, **kwargs)

    def get_all_results(
        self, url, params: Dict = dict(), **kwargs
    ) -> Generator[Response, None, None]:
        """
        Fetches all pages in response.

        Args:
            url: Request URL
            params: Query params
            **kwargs: keyword arguments

        Returns:
            Response object
        """
        # PAX8 API pagination has 0-based indexing
        page_number: Optional[int] = 0

        if not params:
            params: Dict = dict(size=200, page=page_number)
        else:
            params.update(size=200, page=page_number)

        while page_number is not None:
            response: Response = self._get(url, params=params, **kwargs)
            page_number: int = response.json().get("page").get("number")
            total_pages: int = response.json().get("page").get("totalPages")

            if page_number < total_pages:
                page_number += 1
                params.update(page=page_number)
            else:
                page_number = None

            yield response

    def _post(
        self,
        url,
        params: Dict = dict(),
        data: Dict = dict(),
        **kwargs,
    ) -> Response:
        """
        Performs POST request using session.

        Args:
            url: Request URL
            params: Query params
            data: Request body
            **kwargs: Keyword arguments

        Returns:
            Response object
        """
        return self._session.post(url, params=params, data=data, **kwargs)

    def _put(
        self,
        url,
        params: Dict = dict(),
        data: Dict = dict(),
        **kwargs,
    ) -> Response:
        """
        Performs PUT request using session.

        Args:
            url: Request URL
            params: Query params
            data: Request body
            **kwargs: Keyword arguments

        Returns:
            Response object
        """
        return self._session.put(url, params=params, data=data, **kwargs)

    def _patch(
        self,
        url,
        params: Dict = dict(),
        data: Dict = dict(),
        **kwargs,
    ) -> Response:
        """
        Performs PATCH request using session.

        Args:
            url: Request URL
            params: Query params
            data: Request body
            **kwargs: Keyword arguments

        Returns:
            Response object
        """
        return self._session.patch(url, params=params, data=data, **kwargs)

    def _delete(
        self,
        url,
        **kwargs,
    ) -> Response:
        """
        Performs DELETE request using session.

        Args:
            url: Request URL
            **kwargs: keyword arguments

        Returns:
            Response object
        """
        return self._session.delete(url, **kwargs)

    @staticmethod
    def _build_api_error(response, message=None, errors=None):
        return build_error(
            response.status_code,
            source="PAX8",
            message=message,
            errors=errors,
        )

    def _handle_response(
        self, response: Response, raise_exception: bool = True
    ) -> Response:
        if not str(response.status_code).startswith("2") and raise_exception:
            raise self._build_api_error(
                response, "PAX8 error!", response.json().get("errors")
            )
        return response

    def _get_callable_request_method(self, method: str) -> object:
        """
        Returns callable instance method object mapped to different HTTP requests.

        Args:
            method: HTTP method.

        Returns:
            Object
        """
        # Mapped HTTP request methods to instance methods.
        allowed_requests: Dict = {
            "GET": self._get,
            "POST": self._post,
            "PUT": self._put,
            "PATCH": self._patch,
            "DELETE": self._delete,
        }
        return allowed_requests[method.upper()]

    def request(
        self,
        method: str,
        url: str,
        params: Dict = dict(),
        data: Dict = dict(),
        raise_exception=True,
        **kwargs,
    ) -> Response:
        """

        Args:
            method: Request method
            url: Request URL
            params: Query params
            data: Request body
            raise_exception: Raise exception for error
            **kwargs: Keyword arguments

        Returns:
            Response object
        """
        if self._token_has_expired():
            self._create_session()
        response: Response = self._get_callable_request_method(method)(
            url, params=params, data=data, **kwargs
        )
        return self._handle_response(response, raise_exception)

    def get_session_info(self) -> Dict:
        session_info: Dict = dict(
            auth_token=self._auth_token,
            token_expiry=self._token_expiry,
            headers=self._session.headers,
        )
        return session_info
