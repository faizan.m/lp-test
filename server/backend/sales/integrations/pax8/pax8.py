import asyncio
import functools
import math
from typing import List, Dict, Optional, Generator, Any, Union
from urllib.parse import urlparse

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientResponseError
from requests import Response
from requests.exceptions import (
    ConnectionError,
    Timeout,
    ConnectTimeout,
    ReadTimeout,
    HTTPError,
)

from core.utils import chunks_of_list_with_max_item_count
from exceptions.exceptions import AuthenticationError
from sales.integrations.pax8.pax8_client import PAX8Client
from utils import get_app_logger

# Get an instance of a logger
logger = get_app_logger(__name__)


def async_retry(
    num_of_retries: int = 3,
    seconds_delay: Union[int, float] = 1.5,
    backoff: int = 2,
    exception_class=Exception,
):
    """
    Decorator to retry async api requests.
    Retries the async API request for 'num_of_retries' times by wating for some time.
    Catches the exception of class 'exception_class'.
    If fails for all attempts, returns None.

    Args:
        num_of_retries: Number of retries
        seconds_delay: Delay in seconds
        backoff: Backoff factor
        exception_class: Exception to catch. Default is Exception.
    """

    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            remaining_tries = num_of_retries
            while remaining_tries > 0:
                try:
                    return await func(*args, **kwargs)
                except exception_class as e:
                    remaining_tries -= 1
                    if remaining_tries == 0:
                        return None
                    else:
                        logger.warning(
                            f"Error: {e}. Retrying in {seconds_delay * backoff ** (num_of_retries - remaining_tries)} "
                            f"seconds."
                        )
                        await asyncio.sleep(
                            seconds_delay
                            * backoff ** (num_of_retries - remaining_tries)
                        )

        return wrapper

    return decorator


class PAX8APIClient:
    BASE_URL: str = "https://api.pax8.com/v1/"
    PRODUCTS_URL: str = BASE_URL + "products"

    def __init__(self, client_id: str, client_secret: str):
        self.client = self._get_pax8_client(client_id, client_secret)

    @staticmethod
    def _get_pax8_client(
        client_id: str, client_secret: str
    ) -> Optional[PAX8Client]:
        try:
            pax8_client: PAX8Client = PAX8Client(client_id, client_secret)
        except AuthenticationError:
            return None
        except (
            ConnectionError,
            TimeoutError,
            Timeout,
            ConnectTimeout,
            ReadTimeout,
            HTTPError,
        ) as err:
            logger.error(err)
            return None
        return pax8_client

    def list_all_pax8_products(self) -> List[Dict]:
        """
        Get all pax8 products.

        Returns:
            List of pax8 products.
        """
        products: Generator[Response] = self.client.get_all_results(
            self.PRODUCTS_URL
        )
        products_data: List[Dict] = [
            product.json().get("content") for product in products
        ]
        return products_data

    def get_product(self, product_id: str) -> Dict:
        """
        Get pax8 product data for given product ID.

        Args:
            product_id: pax8 product ID

        Returns:
            Product data
        """
        url: str = f"{self.PRODUCTS_URL}/{product_id}"
        product: Response = self.client.request("GET", url)
        return product.json()

    def get_total_no_of_pax8_products(self) -> int:
        """
        Get total no of pax8 produts as listed in pagination info.

        Returns:
            Total no. of pax8 products.
        """
        products: Dict = self.client.request("GET", self.PRODUCTS_URL).json()
        total_no_of_products: int = products.get("page").get("totalElements")
        return total_no_of_products

    def get_products_for_vendors(self, vendor_names: List[str]) -> List[Dict]:
        """
        Get all product for given vendors.

        Args:
            vendor_names: List of vendor names.

        Returns:
            Products for the vendors.
        """
        url: str = f"{self.PRODUCTS_URL}"
        query_params: Dict = dict(
            vendorName=",".join(vendor_names),
        )
        products: Generator[Response] = self.client.get_all_results(
            url, params=query_params
        )
        products_data: List[Dict] = [product.json() for product in products]
        return products_data


class PAX8APIAsyncClient:
    BASE_URL: str = "https://api.pax8.com/v1/"
    PRODUCTS_URL: str = BASE_URL + "products"

    def __init__(self, pax8_api_client: PAX8APIClient):
        self._pax8_api_client: Optional[PAX8APIClient] = pax8_api_client

    @staticmethod
    async def async_get_pax8_products(
        session: ClientSession, url: str, params: Dict
    ) -> Dict:
        """
        Performs async GET request to fetch pax8 products.

        Args:
            session: aiohttp client
            url: Products URL
            params: Query params

        Returns:
            PAX8 products data.
        """
        async with session.get(url, params=params) as response:
            content: Dict = await response.json()
            return content

    async def async_get_all_pax8_products(
        self, total_no_of_products: int
    ) -> List[Dict]:
        """
        Get all pax8 products data by performing multiple async GET request.

        Args:
            total_no_of_products: Total no of pax8 products

        Returns:
            All pax8 products data.
        """
        url: str = self._pax8_api_client.PRODUCTS_URL
        # PAX8 has maximum page size of 200
        page_size: int = 200
        pages_to_fetch: int = self.get_total_no_of_pages(
            total_no_of_products, page_size
        )
        tasks: List = []
        # Use the headers from PAX8Client
        session_info: Dict = self._pax8_api_client.client.get_session_info()
        async with ClientSession(
            headers=session_info.get("headers")
        ) as session:
            for i in range(pages_to_fetch):
                params: Dict = dict(page=i, size=page_size)
                tasks.append(
                    asyncio.ensure_future(
                        self.async_get_pax8_products(session, url, params)
                    )
                )
            results: List[Dict] = await asyncio.gather(*tasks)
            return results

    def get_all_pax8_products(self) -> List[Dict]:
        """
        Driver method to get all pax8 products by performing multiple async GET requests.

        Returns:
            All pax8 products data.
        """
        all_products: List[Dict] = list()
        total_no_of_products: int = (
            self._pax8_api_client.get_total_no_of_pax8_products()
        )
        products: List[Dict] = asyncio.run(
            self.async_get_all_pax8_products(total_no_of_products)
        )
        for product in products:
            all_products.extend(product.get("content"))
        return all_products

    @staticmethod
    def get_total_no_of_pages(
        total_no_of_products: int, page_size: int
    ) -> int:
        """
        Get total no of API pages using page size and total no of products.

        Args:
            total_no_of_products: Total no of pax8 produts
            page_size: Page size

        Returns:
            Number of pages
        """
        # PAX8 APIs support 0-based page numbers
        total_no_of_pages: int = math.ceil(total_no_of_products / page_size)
        return total_no_of_pages

    @staticmethod
    def extract_product_crm_id_from_url(url: str) -> str:
        """
        Extracts product CRM ID from product pricing data url.

        Args:
            url: Product pricing data url.

        Returns:
            PAX8 product CRM ID
        """
        path: str = urlparse(url).path
        product_crm_id: str = path.split("/")[-2]
        return product_crm_id

    @async_retry(exception_class=ClientResponseError)
    async def get_pax8_product_pricing_data(
        self, session: ClientSession, url: str
    ) -> Optional[Dict[str, Any]]:
        """
        Perfrom async GET request to fetch PAX8 product pricing data.

        Args:
            session: ClientSession
            url: Product pricing url

        Returns:
            Product pricing data
        """
        async with session.get(url) as response:
            # The PAX8 product pricing data response does not contain the original product id.
            # Hence, add original product ID in response.
            product_crm_id: str = self.extract_product_crm_id_from_url(url)
            if response.status == 200:
                pricing_data: Dict[str, Any] = await response.json()
                pricing_data.update(product_crm_id=product_crm_id)
                return pricing_data
            elif response.status == 429:
                raise ClientResponseError(
                    status=response.status,
                    message=f"429 Error fetching PAX8 pricing data!",
                    request_info=response.request_info,
                    history=response.history,
                )
            else:
                logger.error(
                    f"{response.status} Error fetching pricing data for product ID: {product_crm_id}"
                )
                return None

    async def rate_limited_get_pax8_product_pricing_data(
        self, session: ClientSession, url: str, semaphore
    ) -> Optional[Dict]:
        """
        Perfrom limted async GET request to fetch PAX8 product pricing data.

        Args:
            session: ClientSession
            url: Product pricing url

        Returns:
            Product pricing data
        """
        async with semaphore:
            return await self.get_pax8_product_pricing_data(session, url)

    async def get_pax8_products_pricing_data(
        self, product_crm_ids_list: List[str]
    ) -> List[Dict]:
        """
        Performs multiple async GET requests to fetch pax8 product pricing data.

        Args:
            product_crm_ids_list: List of pax8 product CRM IDs.

        Returns:
            PAX8 product pricing data.
        """
        tasks: List = list()
        # Reuse the headers from PAX8Client
        session_info: Dict = self._pax8_api_client.client.get_session_info()
        # Limit the number of concurrent coroutines that can run.
        MAX_CONCURRENT_REQUESTES: int = 10
        semaphore = asyncio.Semaphore(MAX_CONCURRENT_REQUESTES)

        async with ClientSession(
            headers=session_info.get("headers")
        ) as session:
            for product_crm_id in product_crm_ids_list:
                url: str = f"{self._pax8_api_client.PRODUCTS_URL}/{product_crm_id}/pricing"
                tasks.append(
                    asyncio.ensure_future(
                        self.rate_limited_get_pax8_product_pricing_data(
                            session, url, semaphore
                        )
                    )
                )
            results: List[Optional[Dict]] = await asyncio.gather(*tasks)
            results: List[Dict] = [
                item for item in results if item is not None
            ]
            return results

    def get_all_pax8_produts_pricing_data(
        self, product_crm_ids: List[str]
    ) -> List[Dict]:
        """
        Get pricing data of multiple pax8 products by making multiple GET requests.

        Args:
            product_crm_ids: List of PAX8 product CRM IDs.

        Returns:
            PAX8 product pricing data.
        """
        all_products_pricing_data: List[Dict] = list()
        list_of_product_crm_ids_list: Generator[
            List[str], None, None
        ] = chunks_of_list_with_max_item_count(product_crm_ids, 100)
        for product_crm_ids_list in list_of_product_crm_ids_list:
            products_pricing_data: List[Dict] = asyncio.run(
                self.get_pax8_products_pricing_data(product_crm_ids_list)
            )
            all_products_pricing_data.extend(products_pricing_data)
        return all_products_pricing_data


def validate_pax8_credentials(client_id: str, client_secret: str):
    """
    Validate API credentials for PAX8 integration.

    Args:
        client_id: Client ID
        client_secret: Client secret

    Returns:
        True if valid credentials else False
    """
    pax8_client: PAX8Client = PAX8APIClient(client_id, client_secret).client
    return (
        True
        if (
            isinstance(pax8_client, PAX8Client)
            and hasattr(pax8_client, "_auth_token")
        )
        else False
    )


def get_pax8_api_client(
    client_id: str, client_secret: str
) -> Optional[PAX8APIClient]:
    """
    Get PAX8APIClient instance.

    Args:
        client_id: PAX8 client ID
        client_secret: PAX8 client secret

    Returns:
        PAX8APIClient instance
    """
    pax8_api_client: Optional[PAX8APIClient] = PAX8APIClient(
        client_id, client_secret
    )
    return pax8_api_client


def get_pax8_api_async_client(
    client_id: str, client_secret: str
) -> Optional[PAX8APIAsyncClient]:
    pax8_api_client: Optional[PAX8APIClient] = get_pax8_api_client(
        client_id, client_secret
    )
    pax8_api_async_client: PAX8APIAsyncClient = PAX8APIAsyncClient(
        pax8_api_client
    )
    return pax8_api_async_client
