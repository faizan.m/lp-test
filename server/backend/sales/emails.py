from accounts.mail import BaseEmailMessage


class SalesActivityMail(BaseEmailMessage):
    template_name = "emails/sales_activity_email_template.html"


class AutomatedCollectionNoticeEmail(BaseEmailMessage):
    template_name = "collection_notice_emails/automated_collection_notice_email_template.html"


class AgreementUpdateEmail(BaseEmailMessage):
    """
    Email for sending to account manager after agreement creation/update.
    """
    template_name = "emails/agreement_update_email.html"
