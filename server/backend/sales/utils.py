from typing import Dict, Set, Union, List

from sales.services import agreement_template_service
from utils import add_page_break_in_html


def is_preview(request_data: Dict) -> bool:
    previews: Set[str] = {
        agreement_template_service.AgreementTemplatePreviewService.PRE_TEMPLATE_CREATION_PREVIEW,
        agreement_template_service.AgreementTemplatePreviewService.EXISTING_TEMPLATE_PREVIEW,
        agreement_template_service.AgreementTemplatePreviewService.PRE_AGREEMENT_CREATE_PREVIEW,
        agreement_template_service.AgreementTemplatePreviewService.EXISTING_AGREEMENT_PREVIEW,
    }
    # Validation of name not required for preview
    if any([request_data.get(preview) for preview in previews]):
        return True
    else:
        return False


def prepare_amount_for_preview(number: Union[float, int]) -> str:
    """
    Prepare currency amount for preview by rounding off, adding commas and $ sign

    Args:
        number: Amount

    Returns:
        Amount formatted for use in preview
    """
    # E.g. if number = 1234567.52, returns $1,234,568
    number = round(number)
    number_str = "{:,}".format(number)
    number_str = "$" + number_str
    return number_str


def substitute_page_break_placeholder_in_sections(
    sections: List[Dict],
) -> None:
    """
    Replace @{Page Break} placeholder with page break html value in section html content.
    NOTE: This depends on the placeholder values embedded by UI using Quill editor. May not work if the placeholder
    value changes.

    Args:
        sections: Sections

    Returns:
        None
    """
    for section in sections:
        section["value_html"] = add_page_break_in_html(
            section.get("value_html")
        )
    return


def substitute_customer_name_placeholder_in_agreement_sections(
    sections: List[Dict], customer_name: str
) -> None:
    """
    Replace @{Customer Name} placeholder with customer name in section html content.
    NOTE: This depends on the placeholder values embedded by UI using Quill editor. May not work if the placeholder
    value changes.

    Args:
        sections: Sections
        customer_name: Customer name

    Returns:
        None
    """
    for section in sections:
        section["value_html"] = section.get("value_html").replace(
            '<span class="ql-mention-denotation-char">@</span>{Customer Name}',
            f'<span class="ql-mention-denotation-char"></span>{customer_name}',
        )
    return
