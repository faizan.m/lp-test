import json
from typing import Dict

from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, filters
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.custom_permissions import IsProviderUser
from auditlog.models import LogEntry
from sales.filterset import SalesAgreementTemplateFilterset
from sales.models import AgreementTemplate
from sales.serializers import (
    AgreementTemplateCreateSerializer,
    AgreementTemplateSerializer,
    AgreementTemplateUpdateSerializer,
    SalesAgreementTemplateAuthorsListSerializer,
    AgreementTemplateDropdownVersionListingSerializer,
    SalesAgreementTemplatePreviewSerializer,
    HistorySerializer,
    AgreementTemplateListSerializer,
)
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
)
from utils import get_app_logger
from drf_yasg.utils import swagger_auto_schema
from django.utils.decorators import method_decorator
from drf_yasg import openapi

logger = get_app_logger(__name__)


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        operation_description="List agreement templates",
        operation_summary="This endpoint supports listing of active templates, listing of disabled templates"
        "and listing of template authors.",
        manual_parameters=[
            openapi.Parameter(
                "show_disabled",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                default="false",
                description="Used for listing disabled templates",
            ),
            openapi.Parameter(
                "list_authors",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                default="false",
                description="Used for listing template authors",
            ),
        ],
        responses={
            "200": AgreementTemplateSerializer(),
        },
    ),
)
@method_decorator(
    name="post",
    decorator=swagger_auto_schema(
        operation_description="Create template",
        request_body=AgreementTemplateCreateSerializer(),
        responses={"201": AgreementTemplateSerializer()},
    ),
)
class SalesAgreementTemplateListCreateAPIView(generics.ListCreateAPIView):
    """
    API View to list and create agreement templates
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = SalesAgreementTemplateFilterset
    search_fields = [
        "name",
        "version_description",
        "author__first_name",
        "author__last_name",
        "updated_by__first_name",
        "updated_by__last_name",
    ]
    ordering_fields = ["name", "updated_on", "created_on"]

    def get_queryset(self):
        provider_id: int = self.request.provider.id
        show_disabled: bool = json.loads(
            self.request.query_params.get("show_disabled", "false")
        )
        return (
            AgreementTemplate.objects.only(
                "id",
                "provider",
                "name",
                "author",
                "updated_by",
                "is_disabled",
                "major_version",
                "minor_version",
                "created_on",
                "updated_on",
                "is_current_version",
                "root_template",
                "parent_template",
            )
            .select_related("author", "updated_by")
            .filter(
                provider_id=provider_id,
                is_current_version=True,
                is_disabled=show_disabled,
            )
        )

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            agreement_template: AgreementTemplate = (
                AgreementTemplate.objects.create(
                    provider=self.request.provider,
                    author=self.request.user,
                    updated_by=self.request.user,
                    is_current_version=True,
                    root_template=None,
                    parent_template=None,
                    **serializer.validated_data,
                )
            )
            return Response(
                AgreementTemplateSerializer(agreement_template).data,
                status=status.HTTP_201_CREATED,
            )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return AgreementTemplateCreateSerializer
        else:
            return AgreementTemplateListSerializer


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        operation_description="Get templates details",
        operation_summary="Get active template details",
        responses={
            "200": AgreementTemplateSerializer(),
        },
    ),
)
@method_decorator(
    name="put",
    decorator=swagger_auto_schema(
        operation_description="Update and rollback of templates",
        operation_summary='For update, "update_major_version" and "set_user_as_author" are optional,'
        'Use "rollback" query param for rollback',
        manual_parameters=[
            openapi.Parameter(
                "update_major_version",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                description="Used for updating major version in update",
            ),
            openapi.Parameter(
                "set_user_as_author",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                description="Used for setting user as template author",
            ),
            openapi.Parameter(
                "rollback",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                description="Used for template rollback",
            ),
        ],
        request_body=AgreementTemplateUpdateSerializer(),
        responses={
            "200": AgreementTemplateSerializer(),
        },
    ),
)
@method_decorator(
    name="delete",
    decorator=swagger_auto_schema(
        operation_description="Disable template",
        responses={
            "204": None,
        },
    ),
)
class SalesAgreementTemplateRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    """
    API View to retrieve, update and destroy agreement templates.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def get_object(self):
        template: AgreementTemplate = get_object_or_404(
            AgreementTemplate.objects.all(),
            id=self.kwargs.get("template_id"),
            provider_id=self.request.provider.id,
            is_current_version=True,
        )
        return template

    def get_field_values(self, current_template: AgreementTemplate) -> Dict:
        is_rollback: bool = json.loads(
            self.request.query_params.get("rollback", "false")
        )
        if is_rollback:
            return dict(
                author=current_template.author,
                major_version=current_template.major_version + 1,
                minor_version=0,
            )
        else:
            update_major_version: bool = json.loads(
                self.request.query_params.get("update_major_version", "false")
            )
            set_user_as_author: bool = json.loads(
                self.request.query_params.get("set_user_as_author", "false")
            )
            return dict(
                author=self.request.user
                if set_user_as_author
                else current_template.author,
                major_version=current_template.major_version + 1
                if update_major_version
                else current_template.major_version,
                minor_version=0
                if update_major_version
                else current_template.minor_version + 1,
            )

    def get_new_version_template(
        self,
        current_template: AgreementTemplate,
        serializer: AgreementTemplateUpdateSerializer,
    ) -> AgreementTemplate:
        # Get values of field such as author, major_version and minor_version
        # which depends on query_parameters
        other_fields: Dict = self.get_field_values(current_template)
        new_version_template: AgreementTemplate = (
            AgreementTemplate.objects.create(
                provider=current_template.provider,
                updated_by=self.request.user,
                is_current_version=True,
                root_template_id=current_template.get_root_template_id(),
                parent_template_id=current_template.id,
                **other_fields,
                **serializer.validated_data,
            )
        )
        return new_version_template

    def put(self, request, *args, **kwargs):
        current_template: AgreementTemplate = self.get_object()
        serializer: AgreementTemplateUpdateSerializer = self.get_serializer(
            data=self.request.data
        )
        if serializer.is_valid(raise_exception=True):
            current_template.is_current_version = False
            current_template.save()
            new_version_template: AgreementTemplate = (
                self.get_new_version_template(current_template, serializer)
            )
            return Response(
                data=AgreementTemplateSerializer(new_version_template).data,
                status=status.HTTP_200_OK,
            )

    def perform_destroy(self, instance):
        root_template_id: int = instance.get_root_template_id()
        queryset = AgreementTemplate.objects.filter(
            Q(root_template_id=root_template_id) | Q(id=root_template_id),
            provider_id=self.request.provider.id,
        ).order_by("-created_on")
        # Manually save the latest template to log into the audit log table
        if queryset:
            queryset[0].is_disabled = True
            queryset[0].save()
        queryset.update(is_disabled=True)

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return AgreementTemplateUpdateSerializer
        else:
            return AgreementTemplateSerializer


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        operation_description="List all versions of a template. Includes template details.",
        responses={
            "200": AgreementTemplateSerializer(),
        },
    ),
)
class SalesAgreementTemplateVersioningAPIView(generics.ListAPIView):
    """
    API for version related operations on Agreement Template.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = AgreementTemplateSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = SalesAgreementTemplateFilterset
    search_fields = [
        "name",
        "version_description",
        "author__first_name",
        "author__last_name",
        "updated_by__first_name",
        "updated_by__last_name",
    ]
    ordering_fields = ["name", "updated_on", "created_on"]

    def get_object(self):
        template: AgreementTemplate = get_object_or_404(
            AgreementTemplate.objects.all(), id=self.kwargs.get("template_id")
        )
        return template

    def get_queryset(self):
        # List all versions of a template
        current_template: AgreementTemplate = self.get_object()
        root_template_id: int = current_template.get_root_template_id()
        return AgreementTemplate.objects.filter(
            Q(root_template_id=root_template_id) | Q(id=root_template_id),
            provider_id=self.request.provider.id,
        ).order_by("created_on")


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        operation_description="List all versions of a template",
        responses={
            "200": AgreementTemplateDropdownVersionListingSerializer(),
        },
    ),
)
class AgreementTemplateDropdownVersionListing(generics.ListAPIView):

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None
    serializer_class = AgreementTemplateDropdownVersionListingSerializer

    def get_queryset(self):
        return AgreementTemplate.objects.filter(
            provider_id=self.request.provider.id,
            is_disabled=False,
            is_current_version=True,
        ).order_by("created_on")


@method_decorator(
    name="post",
    decorator=swagger_auto_schema(
        operation_description="Preview templates",
        operation_summary="This endpoint can be used for previewing existing templates and"
        " before creation.\n"
        'To preview existing templates: "pre_template_create_preview"=true and '
        "template payload in request body.\n"
        'For preview before template creation: "existing_template_preview"=true and template_id in'
        "request body",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "pre_template_create_preview": openapi.Schema(
                    type=openapi.TYPE_BOOLEAN
                ),
                "template_payload": openapi.Schema(type=openapi.TYPE_OBJECT),
                "existing_template_preview": openapi.Schema(
                    type=openapi.TYPE_BOOLEAN
                ),
                "template_id": openapi.Schema(type=openapi.TYPE_INTEGER),
            },
        ),
        responses={
            "200": openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    "file_path": openapi.Schema(type=openapi.TYPE_STRING),
                    "file_name": openapi.Schema(type=openapi.TYPE_STRING),
                },
            ),
        },
    ),
)
class SalesAgreementTemplatePDFDownloadAPIView(generics.GenericAPIView):
    """
    API View to preview agreement templates.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = SalesAgreementTemplatePreviewSerializer

    def get_template_payload(self) -> Dict:
        """
        Get template context as per the scenario

        Returns:
            Template context dict
        """
        request_data: Dict = self.request.data
        template_payload: Dict = {}
        # Preview before template creation
        if request_data.get("pre_template_create_preview", False):
            payload: Dict = request_data.get("template_payload", {})
            if not payload:
                raise ValidationError(
                    "template_payload is required for preview"
                )
            serializer = self.get_serializer(data=payload)
            if serializer.is_valid(raise_exception=True):
                template_payload: Dict = serializer.validated_data
                major_version = serializer.validated_data.get(
                    "major_version", None
                )
                minor_version = serializer.validated_data.get(
                    "minor_version", None
                )
                version: str = (
                    "1.0"
                    if major_version is None and minor_version is None
                    else f"{major_version}.{minor_version}"
                )
                author_name: str = template_payload.get("author_name", None)
                template_payload.update(
                    author_name=author_name
                    if author_name
                    else self.request.user.name,
                    version=version,
                )
        # Preview for an existing template
        elif request_data.get("existing_template_preview", False):
            template_id: int = request_data.get("template_id", None)
            if not template_id:
                raise ValidationError(
                    "template_id is required for previewing an existing template"
                )
            template: AgreementTemplate = get_object_or_404(
                AgreementTemplate.objects.all(), id=template_id
            )
            template_payload: Dict = AgreementTemplatePreviewService.get_template_payload_from_model_instance(
                template
            )
        if not template_payload:
            raise ValidationError(
                "Request body payload is invalid! "
                "Valid payload should be used for either pre_template_create_preview or existing_template_preview"
            )
        return template_payload

    def post(self, request, *args, **kwargs):
        payload: Dict = self.get_template_payload()
        preview: Dict = AgreementTemplatePreviewService.generate_agreement_template_pdf_preview(
            payload
        )
        return Response(data=preview, status=status.HTTP_200_OK)


class AgreementTemplateAuditLogsListAPIView(generics.ListAPIView):
    serializer_class = HistorySerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ["actor__id", "object_repr", "action"]
    ordering_fields = [
        "timestamp",
    ]

    def get_queryset(self):
        provider = self.request.provider
        queryset = (
            LogEntry.objects.get_for_app("sales")
            .select_related("content_type", "actor")
            .filter(provider=provider, content_type__model="agreementtemplate")
        )
        # Remove the Update logs which has root template id as empty
        queryset = queryset.filter(
            ~Q(additional_data__root_template_id__exact="") | ~Q(action=1)
        )
        for query in queryset:
            # Marking action as delete if is_disabled is true
            if query.additional_data.get("is_disabled"):
                query.action = 2
            # Marking action as update in place of create for updated templates
            if query.action == 0 and query.additional_data.get(
                "root_template_id"
            ):
                query.action = 1
        return queryset


class SalesAgreementTemplateAuthorsListAPIView(generics.ListAPIView):
    """
    API view to list template authors.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = SalesAgreementTemplateAuthorsListSerializer

    def get_queryset(self):
        return (
            AgreementTemplate.objects.select_related(
                "author", "author__profile"
            )
            .filter(provider_id=self.request.provider.id)
            .order_by()
            .distinct("author")
            .only("author")
        )
