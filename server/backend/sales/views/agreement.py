import json
from typing import Dict
from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, filters, status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.custom_permissions import IsProviderUser
from auditlog.models import LogEntry
from sales.filterset import AgreementFilterSet
from sales.models import Agreement, AgreementMarginSettings
from sales.serializers import (
    AgreementCreateSerializer,
    AgreementRetrieveUpdateSerializer,
    AgreementMarginSettingsSerializer,
    AgreementSerializer,
    AgreementPreviewSerializer,
    HistorySerializer,
    AgreementListSerializer,
    SalesAgreementTemplateAuthorsListSerializer,
)
from sales.services.agreement_template_service import (
    AgreementTemplatePreviewService,
    AgreementService,
)
from utils import get_app_logger
from django.db import transaction
from sales.tasks import (
    send_agreement_to_account_manager,
    upload_agreement_pdf_and_service_detail_pdf_to_cw,
)
from django.utils import timezone

# Get an instance of a logger
logger = get_app_logger(__name__)


class AgreementViewMixin:
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_queryset(self: generics.GenericAPIView):
        provider = self.request.provider
        show_disabled: bool = json.loads(
            self.request.query_params.get("show_disabled", "false")
        )
        queryset = Agreement.objects.select_related(
            "author", "updated_by", "customer", "user", "template"
        ).filter(
            provider=provider,
            is_current_version=True,
            is_disabled=show_disabled,
        )
        return queryset


class AgreementListCreateAPIView(
    AgreementViewMixin, generics.ListCreateAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "version_description",
        "customer__name",
        "template__name",
        "author__first_name",
        "author__last_name",
        "updated_by__first_name",
        "updated_by__last_name",
    ]
    ordering_fields = [
        "customer__name",
        "name",
        "created_on",
        "updated_on",
    ]

    filterset_class = AgreementFilterSet

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            agreement_service: AgreementService = AgreementService(
                self.request.provider
            )
            agreement: Agreement = agreement_service.create_agreement(
                self.request.user, serializer.validated_data
            )
            transaction.on_commit(
                lambda: upload_agreement_pdf_and_service_detail_pdf_to_cw.apply_async(
                    (self.request.provider.id, agreement.id), queue="high"
                )
            )
            email_account_manager: bool = json.loads(
                self.request.query_params.get("email-account-manager", "false")
            )
            if email_account_manager:
                transaction.on_commit(
                    lambda: send_agreement_to_account_manager.apply_async(
                        (
                            agreement.id,
                            self.request.provider.id,
                            str(self.request.user.id),
                        ),
                        queue="high",
                    )
                )
                logger.info(
                    f"Task to send agreement to account manager triggered after creation of agreement: "
                    f"{agreement.name}, ID: {agreement.id} of provider: {self.request.provider.name}, "
                    f"ID: {self.request.provider.id}"
                )
            return Response(
                AgreementSerializer(agreement).data,
                status=status.HTTP_201_CREATED,
            )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return AgreementCreateSerializer
        else:
            return AgreementListSerializer


class AgreementRetrieveUpdateDestroyAPIView(
    AgreementViewMixin, generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def put(self, request, *args, **kwargs):
        agreement_id: int = self.kwargs.get("pk")
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                current_version_agreement = Agreement.objects.get(
                    id=agreement_id,
                    is_current_version=True,
                    provider=self.request.provider,
                )
            except Agreement.DoesNotExist:
                return Response(
                    data={"Agreement matching query does not exists"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            agreement_service: AgreementService = AgreementService(
                self.request.provider
            )
            updated_agreement: Agreement = agreement_service.update_agreement(
                current_version_agreement,
                self.request.user,
                serializer.validated_data,
                self.request.query_params,
            )
            transaction.on_commit(
                lambda: upload_agreement_pdf_and_service_detail_pdf_to_cw.apply_async(
                    (self.request.provider.id, updated_agreement.id),
                    queue="high",
                )
            )
            email_account_manager: bool = json.loads(
                self.request.query_params.get("email-account-manager", "false")
            )
            if email_account_manager:
                transaction.on_commit(
                    lambda: send_agreement_to_account_manager.apply_async(
                        (
                            updated_agreement.id,
                            self.request.provider.id,
                            str(self.request.user.id),
                        ),
                        queue="high",
                    )
                )
                logger.info(
                    f"Task to send agreement to account manager triggered after update of agreement: "
                    f"{updated_agreement.name}, ID: {updated_agreement.id} of provider: "
                    f"{self.request.provider.name}, ID: {self.request.provider.id}"
                )
            return Response(
                data=AgreementSerializer(updated_agreement).data,
                status=status.HTTP_200_OK,
            )

    def perform_destroy(self, instance):
        root_agreement_id: int = instance.get_root_agreement_id()
        queryset = Agreement.objects.filter(
            Q(root_agreement_id=root_agreement_id) | Q(id=root_agreement_id),
            provider=self.request.provider,
        ).order_by("-created_on")
        # Manually save the latest agreement to log into the audit log table
        if queryset:
            queryset[0].is_disabled = True
            queryset[0].save()
        queryset.update(is_disabled=True)

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return AgreementRetrieveUpdateSerializer
        else:
            return AgreementCreateSerializer


class AgreementVersioningAPIView(generics.ListAPIView):
    """
    API for version related operations on Agreement.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = AgreementRetrieveUpdateSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = AgreementFilterSet
    search_fields = [
        "name",
        "version_description",
        "author__first_name",
        "author__last_name",
        "updated_by__first_name",
        "updated_by__last_name",
    ]
    ordering_fields = ["name", "updated_on", "created_on"]

    def get_object(self):
        agreement: Agreement = get_object_or_404(
            Agreement.objects.all(), id=self.kwargs.get("pk")
        )
        return agreement

    def get_queryset(self):
        # List all versions of a template
        current_agreement: Agreement = self.get_object()
        root_agreement_id: int = current_agreement.get_root_agreement_id()
        return Agreement.objects.filter(
            Q(root_agreement_id=root_agreement_id) | Q(id=root_agreement_id),
            provider_id=self.request.provider.id,
        ).order_by("created_on")


class AgreementMarginSettingsListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = AgreementMarginSettingsSerializer

    def get_queryset(self):
        queryset = AgreementMarginSettings.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class AgreementMarginSettingsRetrieveAPIView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = AgreementMarginSettingsSerializer

    def get_queryset(self):
        queryset = AgreementMarginSettings.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)


class AgreementPDFDownloadAPIView(generics.GenericAPIView):
    """
    API View to preview agreements
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = AgreementPreviewSerializer

    def get_agreement_payload(self) -> Dict:
        """
        Get agreement context as per the scenario

        Returns:
            Agreement context
        """
        request_data: Dict = self.request.data
        agreement_payload: Dict = {}
        # Preview before agreement creation
        if request_data.get("pre_agreement_create_preview", False):
            payload: Dict = request_data.get("agreement_payload", {})
            if not payload:
                raise ValidationError(
                    "agreement_payload is required for preview"
                )
            serializer = self.get_serializer(data=payload)
            if serializer.is_valid(raise_exception=True):
                agreement_payload: Dict = serializer.validated_data
                extra_context: Dict = AgreementTemplatePreviewService.get_context_from_validated_data(
                    serializer.validated_data
                )
                author_name: str = agreement_payload.get("author_name", None)
                agreement_payload.update(
                    author_name=author_name
                    if author_name
                    else self.request.user.name,
                    **extra_context,
                    updated_on=timezone.now().strftime("%Y.%m.%d"),
                )
        # Preview for an existing agreement
        elif request_data.get("existing_agreement_preview", False):
            agreement_id: int = request_data.get("agreement_id", None)
            if not agreement_id:
                raise ValidationError(
                    "agreement_id is required for previewing an existing agreement"
                )
            agreement: Agreement = get_object_or_404(
                Agreement.objects.all(), id=agreement_id
            )
            agreement_payload: Dict = AgreementTemplatePreviewService.get_agreement_context_from_model_instance(
                agreement
            )

        if not agreement_payload:
            raise ValidationError(
                "Request body payload is invalid! "
                "Valid payload should be used for either pre_agreement_create_preview or existing_agreement_preview"
            )
        return agreement_payload

    def post(self, request, *args, **kwargs):
        payload: Dict = self.get_agreement_payload()
        preview: Dict = (
            AgreementTemplatePreviewService.generate_agreement_pdf_preview(
                payload
            )
        )
        return Response(data=preview, status=status.HTTP_200_OK)


class AgreementsAuditLogsListAPIView(generics.ListAPIView):
    serializer_class = HistorySerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ["actor__id", "object_repr", "action"]
    ordering_fields = [
        "timestamp",
    ]

    def get_queryset(self):
        provider = self.request.provider
        queryset = (
            LogEntry.objects.get_for_app("sales")
            .select_related("content_type", "actor")
            .filter(provider=provider, content_type__model="agreement")
        )
        # Remove the Update logs which has root agreement id as empty
        queryset = queryset.filter(
            ~Q(additional_data__root_agreement_id__exact="") | ~Q(action=1)
        )
        for query in queryset:
            # Marking action as delete if is_disabled is true
            if query.additional_data.get("is_disabled"):
                query.action = 2
            # Marking action as update in place of create for updated agreements
            if query.action == 0 and query.additional_data.get(
                "root_agreement_id"
            ):
                query.action = 1
        return queryset


class SendAgreementToAccountManagerAPIView(generics.CreateAPIView):
    """
    API view to send agreement to account manager.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def post(self, request, *args, **kwargs):
        provider_id: int = self.request.provider.id
        agreement_id: int = self.kwargs.get("agreement_id")
        agreement: Agreement = get_object_or_404(
            Agreement.objects.all(),
            id=agreement_id,
            provider_id=provider_id,
            is_current_version=True,
        )
        task = send_agreement_to_account_manager.apply_async(
            (
                agreement.id,
                provider_id,
                self.request.user.id,
            ),
            queue="high",
        )
        return Response(dict(task_id=task.id), status=status.HTTP_200_OK)


class SalesAgreementAuthorsListAPIView(generics.ListAPIView):
    """
    API view to list agreement authors.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = SalesAgreementTemplateAuthorsListSerializer

    def get_queryset(self):
        return (
            Agreement.objects.select_related("author", "author__profile")
            .only("author")
            .filter(provider_id=self.request.provider.id)
            .order_by()
            .distinct("author")
        )
