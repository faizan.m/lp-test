from django.db.models import (
    Exists,
    OuterRef,
    ExpressionWrapper,
    F,
    DurationField,
)
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from django.conf import settings
from accounts.custom_permissions import IsProviderUser
from accounts.models import User
from accounts.utils import get_user_base_url
from sales.filterset import (
    SalesActivityFilterSet,
)
from sales.models import (
    SalesActivity,
    SalesActivityPrioritySetting,
    SalesActivityStatusSetting,
    SalesActivityCustomView,
)
from sales.serializers import (
    SalesActivityPrioritySettingSerializer,
    SalesActivityReadSerializer,
    SalesActivityStatusSettingSerializer,
    SalesActivityWriteSerializer,
    SalesActivityCustomViewSerializer,
)
from sales.tasks import send_sales_activity_email


# Create your views here.


class SalesActivityViewMixin:
    def get_queryset(self: generics.GenericAPIView):
        provider = self.request.provider
        current_date = timezone.now()
        age = ExpressionWrapper(
            current_date - F("created_on"), output_field=DurationField()
        )
        filter_conditions = dict()
        if (
            not self.request.query_params.get("status")
            and self.request.method == "GET"
        ):
            filter_conditions["status__is_closed"] = False
        queryset = (
            SalesActivity.objects.select_related(
                "customer",
                "customer_contact",
                "last_updated_by",
                "last_updated_by__profile",
                "created_by",
                "created_by__profile",
                "assigned_to",
                "assigned_to__profile",
                "status",
                "priority",
            )
            .filter(provider=provider, **filter_conditions)
            .only(
                "notes",
                "description",
                "customer__name",
                "customer__account_manager_name",
                "customer__account_manager_id",
                "customer_contact",
                "last_updated_by__first_name",
                "last_updated_by__last_name",
                "last_updated_by__profile__profile_pic",
                "assigned_to__first_name",
                "assigned_to__last_name",
                "assigned_to__profile__profile_pic",
                "created_by__first_name",
                "created_by__last_name",
                "created_by__profile__profile_pic",
                "due_date",
                "priority__title",
                "priority__color",
                "priority__rank",
                "status__title",
                "status__color",
                "activity_type",
            )
            .annotate(age=age)
            .order_by("priority__rank", "-updated_on")
        )
        return queryset


class SalesActivityListCreateAPIView(
    SalesActivityViewMixin, generics.ListCreateAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "customer__name",
        "customer__account_manager_name",
        "description",
        "notes",
    ]
    ordering_fields = [
        "customer__name",
        "customer__account_manager_name",
        "priority__rank",
        "assigned_to__first_name",
        "due_date",
        "age",
    ]

    filterset_class = SalesActivityFilterSet

    def perform_create(self, serializer):
        provider = self.request.provider
        created_by = self.request.user
        last_updated_by = self.request.user
        assigned_to_user = serializer.validated_data["assigned_to"]
        if created_by != assigned_to_user:
            context = dict()
            context["email_subject"] = "New Activity Assigned"
            recipients = assigned_to_user.email
            context["assigned_to_user_name"] = assigned_to_user.name
            context["created_by_user_name"] = created_by.name
            context["created_by_user_email"] = created_by.email
            user = User.objects.get(id=self.request.user.id)
            context[
                "url"
            ] = f'{get_user_base_url(user, self.request.META["HTTP_ORIGIN"])}/{settings.SALE_ACTIVITY_URL}'
            send_sales_activity_email.apply_async(
                (provider.id, recipients, created_by.id, context),
                queue="high",
            )
        serializer.save(
            provider=provider,
            last_updated_by=last_updated_by,
            created_by=created_by,
        )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return SalesActivityWriteSerializer
        else:
            return SalesActivityReadSerializer


class SalesActivityRetrieveUpdateDestroyAPIView(
    SalesActivityViewMixin, generics.RetrieveUpdateDestroyAPIView
):

    permission_classes = (IsAuthenticated, IsProviderUser)

    def perform_update(self, serializer):
        provider = self.request.provider
        last_updated_by = self.request.user
        serializer.save(provider=provider, last_updated_by=last_updated_by)

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return SalesActivityWriteSerializer
        else:
            return SalesActivityReadSerializer


class SalesActivityStatusSettingViewMixin:
    def get_queryset(self: generics.GenericAPIView):
        in_use = Exists(
            SalesActivity.objects.filter(
                provider=OuterRef("provider"), status_id=OuterRef("id")
            )
        )
        queryset = (
            SalesActivityStatusSetting.objects.filter(
                provider=self.request.provider
            )
            .annotate(in_use=in_use)
            .order_by("created_on")
        )
        return queryset


class SalesActivityStatusSettingListCreateAPIView(
    SalesActivityStatusSettingViewMixin, generics.ListCreateAPIView
):

    serializer_class = SalesActivityStatusSettingSerializer

    def perform_create(self, serializer):
        if serializer.validated_data.get("is_default_closed_status"):
            SalesActivityStatusSetting.objects.filter(
                provider=self.request.provider
            ).update(is_default_closed_status=False)
        serializer.save(provider=self.request.provider)


class SalesActivityStatusSettingRetrieveUpdateDestroyAPIView(
    SalesActivityStatusSettingViewMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = SalesActivityStatusSettingSerializer

    def validate_status(self):
        provider = self.request.provider
        status_id = self.kwargs.get("pk")
        existing_status = SalesActivity.objects.filter(
            provider=provider, status_id=status_id
        )
        if existing_status.exists():
            raise ValidationError("Status already in use. Can't be deleted.")

    def perform_update(self, serializer):
        instance: SalesActivityStatusSetting = self.get_object()
        if serializer.validated_data.get(
            "is_default"
        ) and instance.is_default != serializer.validated_data.get(
            "is_default"
        ):
            SalesActivityStatusSetting.objects.filter(
                provider=self.request.provider
            ).exclude(id=instance.id).update(is_default=False)

        if serializer.validated_data.get(
            "is_default_closed_status"
        ) and instance.is_default_closed_status != serializer.validated_data.get(
            "is_default_closed_status"
        ):
            SalesActivityStatusSetting.objects.filter(
                provider=self.request.provider
            ).exclude(id=instance.id).update(is_default_closed_status=False)
        serializer.save(provider=self.request.provider)

    def perform_destroy(self, instance):
        self.validate_status()
        instance.delete()


class SalesActivityPrioritySettingViewMixin:
    def get_queryset(self: generics.GenericAPIView):
        in_use = Exists(
            SalesActivity.objects.filter(
                provider=OuterRef("provider"), priority_id=OuterRef("id")
            )
        )
        queryset = (
            SalesActivityPrioritySetting.objects.filter(
                provider=self.request.provider
            )
            .annotate(in_use=in_use)
            .order_by("-updated_on")
        )
        return queryset


class SalesActivityPrioritySettingListCreateAPIView(
    SalesActivityPrioritySettingViewMixin, generics.ListCreateAPIView
):

    serializer_class = SalesActivityPrioritySettingSerializer

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)


class SalesActivityPrioritySettingRetrieveUpdateDestroyAPIView(
    SalesActivityPrioritySettingViewMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    serializer_class = SalesActivityPrioritySettingSerializer

    def validate_priority(self):
        provider = self.request.provider
        priority_id = self.kwargs.get("pk")
        existing_priority = SalesActivity.objects.filter(
            provider=provider, priority_id=priority_id
        )
        if existing_priority.exists():
            raise ValidationError("Priority already in use. Can't be deleted.")

    def perform_update(self, serializer):
        instance: SalesActivityPrioritySetting = self.get_object()
        if serializer.validated_data.get(
            "is_default"
        ) and instance.is_default != serializer.validated_data.get(
            "is_default"
        ):
            SalesActivityPrioritySetting.objects.filter(
                provider=self.request.provider
            ).exclude(id=instance.id).update(is_default=False)
        serializer.save(provider=self.request.provider)

    def perform_destroy(self, instance):
        self.validate_priority()
        instance.delete()


class SalesActivityCustomViewMixin:
    def get_queryset(self: generics.GenericAPIView):
        provider = self.request.provider
        user = self.request.user
        queryset = SalesActivityCustomView.objects.filter(
            provider=provider, user=user
        ).order_by("-updated_on")
        return queryset

    def perform_save(self: generics.GenericAPIView, serializer):
        provider = self.request.provider
        user = self.request.user
        serializer.save(provider=provider, user=user)


class SalesActivityCustomViewListCreateAPIView(
    SalesActivityCustomViewMixin, generics.ListCreateAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = SalesActivityCustomViewSerializer
    pagination_class = None

    def perform_create(self, serializer):
        self.perform_save(serializer)


class SalesActivityCustomViewRetrieveUpdateDestroyAPIView(
    SalesActivityCustomViewMixin, generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = SalesActivityCustomViewSerializer
    pagination_class = None

    def perform_update(self, serializer):
        self.perform_save(serializer)
