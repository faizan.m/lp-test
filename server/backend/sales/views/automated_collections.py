from typing import List, Dict

from rest_framework import status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.generics import (
    CreateAPIView,
    RetrieveUpdateAPIView,
    ListAPIView,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from sales.exceptions import AutomatedCollectionNoticeSettingNotConfigured
from sales.models import AutomatedCollectionNoticesSettings
from sales.serializers import (
    AutomatedCollectionNoticesSettingRetrieveSerializer,
    AutomatedCollectionNoticesSettingCreateUpdateSerializer,
    CustomerAutomatedCollectionNoticeEmailTaskDataSerializer,
)
from sales.services.collections_service import CollectionInvoicesService
from sales.tasks import (
    trigger_collection_notice_emails,
)
from utils import get_app_logger

logger = get_app_logger(__name__)


class AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView(
    CreateAPIView, RetrieveUpdateAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            automated_collection_notices_setting: AutomatedCollectionNoticesSettings = (
                provider.automated_collection_notices_setting
            )
        except AutomatedCollectionNoticesSettings.DoesNotExist:
            serializer.save(provider=provider)
        else:
            raise ValidationError(
                f"Automated Collection Notice Setting already exists for the Provider: {provider.name}!"
            )

    def get_object(self):
        provider: Provider = self.request.provider
        try:
            automated_collection_notices_setting: AutomatedCollectionNoticesSettings = (
                provider.automated_collection_notices_setting
            )
        except AutomatedCollectionNoticesSettings.DoesNotExist:
            raise NotFound(
                f"Automated Collection Notices Setting not configured for the Provider: {provider.name}"
            )
        return automated_collection_notices_setting

    def get_serializer_class(self):
        if self.request.method == "GET":
            return AutomatedCollectionNoticesSettingRetrieveSerializer
        else:
            return AutomatedCollectionNoticesSettingCreateUpdateSerializer


class BillingStatusesListAPIView(ListAPIView):
    """
    API to list billing statuses.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        billing_statuses: List[
            Dict
        ] = provider.erp_client.get_billing_statuses_from_cw()
        return Response(data=billing_statuses, status=status.HTTP_200_OK)


class SendAutomatedCollectionNoticeEmailTaskTriggerAPIView(CreateAPIView):
    """
    API View to trigger task that sends automated collection notice emails.
    The collection notice emails can be sent for either specific customers or
    for all customers.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = CustomerAutomatedCollectionNoticeEmailTaskDataSerializer

    def post(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        if not AutomatedCollectionNoticesSettings.configured_for_provider(
            provider
        ):
            raise AutomatedCollectionNoticeSettingNotConfigured()
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            all_customers: bool = serializer.validated_data.get(
                "all_customers"
            )
            if all_customers:
                customer_crm_ids = []
            else:
                customer_crm_ids: List[int] = serializer.validated_data.get(
                    "customer_crm_ids"
                )
            task = trigger_collection_notice_emails.apply_async(
                args=(provider.id, all_customers, customer_crm_ids),
                queue="high",
            )
            logger.info(
                f"Task to send automated collection notice emails triggered by "
                f"provider user: {self.request.user.name}, ID: {self.request.user.id}, "
                f"belonging to provider: {provider.name}, ID: {provider.id}."
            )
            return Response(
                data=dict(task_id=task.task_id), status=status.HTTP_200_OK
            )


class CollectionNoticeEmailPreviewAPIView(ListAPIView):
    """
    API view to preview automated collection notice email.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        try:
            automated_collection_notices_setting: AutomatedCollectionNoticesSettings = (
                provider.automated_collection_notices_setting
            )
        except AutomatedCollectionNoticesSettings.DoesNotExist:
            raise AutomatedCollectionNoticeSettingNotConfigured()
        collection_service: CollectionInvoicesService = (
            CollectionInvoicesService(provider)
        )
        email_preview: str = (
            collection_service.get_collection_notice_email_preview()
        )
        return Response(
            data=dict(html=email_preview), status=status.HTTP_200_OK
        )
