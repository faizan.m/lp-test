from accounts.models import Provider
from typing import List, Dict, Optional, Union
from rest_framework.generics import (
    CreateAPIView,
    RetrieveUpdateAPIView,
    ListAPIView,
    RetrieveAPIView,
)
from sales.models import PAX8IntegrationSettings
from rest_framework.permissions import IsAuthenticated
from accounts.custom_permissions import IsProviderUser
from sales.serializers import (
    PAX8IntegrationSettingsSerializer,
    PAX8CredentialsPayloadSerializer,
    PAX8ProductsListSerializer,
    PAX8VendorsListSerializer,
    PAX8ProductDetailsSerializer,
)
from sales.exceptions import PAX8SettingsNotConfigured
from rest_framework.exceptions import ValidationError
from sales.integrations.pax8.pax8 import (
    PAX8APIClient,
    validate_pax8_credentials,
)
from rest_framework.response import Response
from rest_framework import status
from sales.integrations.pax8.pax8 import PAX8APIClient
from sales.models import PAX8Product, PAX8ProductPricingInfo
from django.db.models import F, QuerySet
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework.backends import DjangoFilterBackend
from sales.filterset import PAX8ProductListFilterSet
from rest_framework.exceptions import NotFound
from utils import is_valid_uuid
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db.models import Q


class PAX8IntegrationSettingsCreateRetrieveUpdateAPIView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve and update PAX8 settings for the provider.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PAX8IntegrationSettingsSerializer

    def get_object(self):
        provider: Provider = self.request.provider
        try:
            pax8settings: PAX8IntegrationSettings = (
                provider.pax8_integration_settings
            )
        except PAX8IntegrationSettings.DoesNotExist:
            raise PAX8SettingsNotConfigured()
        return pax8settings

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            provider.pax8_integration_settings
        except PAX8IntegrationSettings.DoesNotExist:
            serializer.save(provider=provider)
        else:
            raise ValidationError(
                f"PAX8 Integration Setting already exists for the Provider: {provider.name}!"
            )


class PAX8IntegrationValidationAPIView(CreateAPIView):
    """
    API view to validate PAX8 credentials.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PAX8CredentialsPayloadSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            is_valid: bool = validate_pax8_credentials(
                **serializer.validated_data
            )
            return Response(
                data=dict(is_valid=is_valid), status=status.HTTP_200_OK
            )


class PAX8ProductsListAPIView(ListAPIView):
    """
    API view to list PAX8 products with pricing info.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PAX8ProductsListSerializer
    filter_backends = (
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "pax8_product__name",
        "pax8_product__vendor_name",
        "pax8_product__short_description",
        "pax8_product__sku",
    )
    ordering_fields = ("product_name", "vendor_name", "sku")
    filterset_class = PAX8ProductListFilterSet

    def build_search_query(self):
        """
        Construct a query for include & exclude search values.
        """
        include_params: str = self.request.query_params.get("include", "")
        exclude_params: str = self.request.query_params.get("exclude", "")
        query = Q()
        if include_params:
            include_params: List[str] = [
                value.strip() for value in include_params.split(",")
            ]
            for search_param in include_params:
                query &= (
                    Q(pax8_product__name__icontains=search_param)
                    | Q(
                        pax8_product__short_description__icontains=search_param
                    )
                    | Q(pax8_product__sku__icontains=search_param)
                    | Q(pax8_product__vendor_name__icontains=search_param)
                )
        if exclude_params:
            exclude_params: List[str] = [
                value.strip() for value in exclude_params.split(",")
            ]
            for search_param in exclude_params:
                query &= (
                    ~Q(pax8_product__name__icontains=search_param)
                    & ~Q(
                        pax8_product__short_description__icontains=search_param
                    )
                    & ~Q(pax8_product__sku__icontains=search_param)
                    & ~Q(pax8_product__vendor_name__icontains=search_param)
                )
        return query

    def get_queryset(self):
        provider: Provider = self.request.provider

        filter_params: Dict = dict(provider_id=provider.id)
        try:
            pax8_integration_settings: PAX8IntegrationSettings = (
                provider.pax8_integration_settings
            )
        except PAX8IntegrationSettings.DoesNotExist:
            raise PAX8SettingsNotConfigured()
        vendor_names_list: List[str] = pax8_integration_settings.vendors
        if vendor_names_list:
            filter_params.update(
                pax8_product__vendor_name__in=vendor_names_list
            )
        query = self.build_search_query()
        queryset: "QuerySet[PAX8ProductPricingInfo]" = (
            PAX8ProductPricingInfo.objects.select_related("pax8_product")
            .filter(query, **filter_params)
            .only("id", "pax8_product", "pricing_data", "updated_on")
            .annotate(
                product_crm_id=F("pax8_product__crm_id"),
                product_name=F("pax8_product__name"),
                vendor_name=F("pax8_product__vendor_name"),
                sku=F("pax8_product__sku"),
                short_description=F("pax8_product__short_description"),
            )
            .order_by("-updated_on")
        )
        return queryset


class PAX8VendorsListAPIView(ListAPIView):
    """
    API view to list PAX8 vendors.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PAX8VendorsListSerializer
    filter_backends = (OrderingFilter, SearchFilter)
    search_fields = ("vendor_name",)
    ordering_fields = ("vendor_name",)

    def get_queryset(self) -> "QuerySet[PAX8Product]":
        provider: Provider = self.request.provider
        try:
            provider.pax8_integration_settings
        except PAX8IntegrationSettings.DoesNotExist:
            raise PAX8SettingsNotConfigured()
        queryset: "QuerySet[PAX8Product]" = (
            PAX8Product.objects.filter(provider_id=provider.id)
            .only("vendor_name")
            .distinct("vendor_name")
            .order_by("vendor_name")
        )
        return queryset


class PAX8ProductDetailsAPIView(RetrieveAPIView):
    """
    API view to retrive PAX8 product details.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = PAX8ProductDetailsSerializer

    def get_object(self):
        product_crm_id: str = self.kwargs.get("crm_id")
        if not is_valid_uuid(product_crm_id):
            raise ValidationError("PAX8 product CRM ID must be valid UUID!")
        provider: Provider = self.request.provider
        try:
            pax8_product_info: PAX8ProductPricingInfo = (
                PAX8ProductPricingInfo.objects.select_related(
                    "pax8_product"
                ).get(
                    provider_id=provider.id,
                    pax8_product__crm_id=product_crm_id,
                )
            )
        except PAX8ProductPricingInfo.DoesNotExist:
            raise NotFound("PAX8 product with given CRM ID does not exist!")
        return pax8_product_info
