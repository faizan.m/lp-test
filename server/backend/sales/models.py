import os.path

from django.db.models import JSONField
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.functional import cached_property
from auditlog.registry import auditlog, update_only_auditlog
from accounts.model_utils import TimeStampedModel
from typing import List, Dict, Optional, Set
from django.utils import timezone

# Create your models here.


class SalesActivity(TimeStampedModel):

    STATEMENT_OF_WORK = "Statement of work"
    HARDWARE_BOM = "Hardware BoM"
    RESEARCH_AND_RESPONDE = "Research and Respond"
    TYPE_CHOICES = (
        (STATEMENT_OF_WORK, STATEMENT_OF_WORK),
        (HARDWARE_BOM, HARDWARE_BOM),
        (RESEARCH_AND_RESPONDE, RESEARCH_AND_RESPONDE),
    )

    description = models.TextField()
    customer = models.ForeignKey(
        "accounts.Customer", on_delete=models.CASCADE, related_name="+"
    )
    customer_contact = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    notes = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    assigned_to = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True
    )
    due_date = models.DateField()
    priority = models.ForeignKey(
        "SalesActivityPrioritySetting",
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    status = models.ForeignKey(
        "SalesActivityStatusSetting",
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    activity_type = ArrayField(
        models.CharField(max_length=50, choices=TYPE_CHOICES),
        blank=True,
        default=list,
    )
    opportunity_id = models.IntegerField(null=True)


class SalesActivityStatusSetting(TimeStampedModel):

    title = models.CharField(
        max_length=100, help_text="Title for this status."
    )
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this status.",
    )
    is_default = models.BooleanField(default=False)
    is_closed = models.BooleanField(default=False)
    is_default_closed_status = models.BooleanField(default=False)


class SalesActivityPrioritySetting(TimeStampedModel):

    title = models.CharField(
        max_length=100, help_text="Title for this status."
    )
    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    color = models.CharField(
        max_length=10,
        help_text="Hex Code for the color to be shown for this status.",
    )
    is_default = models.BooleanField(default=False)
    rank = models.IntegerField()

    class Meta:
        unique_together = ("provider", "rank")


class SalesActivityCustomView(TimeStampedModel):

    user = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+"
    )
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    custom_activity_view = JSONField(default=dict)

    class Meta:
        indexes = [
            models.Index(fields=["user", "provider"]),
        ]


class AgreementTemplate(TimeStampedModel):
    PER_MONTH = "Monthly"
    PER_ANNUM = "Annually"
    ONE_TIME = "One Time"
    HARDWARE = "Hardware"
    SERVICE = "Service"
    COMMENT = "Comment"
    NA = "NA"
    BILLING_OPTIONS = [
        (PER_MONTH, PER_MONTH),
        (PER_ANNUM, PER_ANNUM),
        (ONE_TIME, ONE_TIME),
        (NA, NA),
    ]
    PRODUCT_TYPES = [
        (HARDWARE, HARDWARE),
        (SERVICE, SERVICE),
        (COMMENT, COMMENT),
    ]
    name = models.CharField(max_length=150, null=False, blank=False)
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="agreement_templates",
    )
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="created_agreement_templates",
    )
    updated_by = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="updated_agreement_templates",
    )
    is_disabled = models.BooleanField(default=False)
    sections = JSONField(default=list, null=False)
    products = JSONField(default=list, null=False)
    major_version = models.PositiveIntegerField(
        null=True, blank=True, default=1
    )
    minor_version = models.PositiveIntegerField(
        null=True, blank=True, default=0
    )
    min_discount = models.IntegerField(default=0)
    max_discount = models.IntegerField(default=100)
    version_description = models.TextField(default="", null=False, blank=True)
    is_current_version = models.BooleanField(default=False)
    root_template = models.ForeignKey(
        "self", null=True, related_name="children", on_delete=models.CASCADE
    )
    parent_template = models.ForeignKey(
        "self",
        null=True,
        related_name="child_template",
        on_delete=models.CASCADE,
    )
    pax8_products = JSONField(default=dict)

    class Meta:
        ordering = ["-updated_on"]

    def __str__(self):
        return self.name

    @cached_property
    def version(self):
        return f"{self.major_version}.{self.minor_version}"

    def get_root_template_id(self) -> int:
        if self.root_template is None and self.parent_template is None:
            return self.id
        else:
            return self.root_template_id

    @staticmethod
    def get_agreement_template_directory():
        file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            os.path.join("agreement_templates"),
        )
        return file_path

    def get_additional_data(self):
        if self.root_template:
            return {
                "template_id": self.id,
                "root_template_id": self.root_template.id,
                "is_disabled": self.is_disabled,
                "version": self.version,
            }
        else:
            return {
                "template_id": self.id,
                "root_template_id": "",
                "is_disabled": self.is_disabled,
                "version": self.version,
            }


class Agreement(TimeStampedModel):
    PER_MONTH = "Monthly"
    PER_ANNUM = "Annually"
    ONE_TIME = "One Time"
    HARDWARE = "Hardware"
    SERVICE = "Service"
    COMMENT = "Comment"
    NA = "NA"
    BILLING_OPTIONS = [
        (PER_MONTH, PER_MONTH),
        (PER_ANNUM, PER_ANNUM),
        (ONE_TIME, ONE_TIME),
        (NA, NA),
    ]
    PRODUCT_TYPES = [
        (HARDWARE, HARDWARE),
        (SERVICE, SERVICE),
        (COMMENT, COMMENT),
    ]
    name = models.CharField(max_length=150)
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="agreements",
    )
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="+",
    )
    updated_by = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="+",
    )
    template = models.ForeignKey(
        "sales.AgreementTemplate",
        on_delete=models.CASCADE,
        related_name="agreements",
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="agreements",
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        "accounts.User", related_name="agreements", on_delete=models.CASCADE
    )
    quote_id = models.IntegerField(unique=False)
    is_disabled = models.BooleanField(default=False)
    sections = JSONField(default=list, null=False)
    products = JSONField(default=list, null=False)
    major_version = models.PositiveIntegerField(
        null=True, blank=True, default=1
    )
    minor_version = models.PositiveIntegerField(
        null=True, blank=True, default=0
    )
    min_discount = models.IntegerField(default=0)
    max_discount = models.IntegerField(default=100)
    version_description = models.TextField(default="", null=False, blank=True)
    customer_cost = models.DecimalField(decimal_places=2, max_digits=10)
    margin = models.DecimalField(decimal_places=2, max_digits=10)
    margin_percentage = models.DecimalField(decimal_places=2, max_digits=10)
    internal_cost = models.DecimalField(decimal_places=2, max_digits=10)
    is_current_version = models.BooleanField(default=False)
    root_agreement = models.ForeignKey(
        "self", null=True, related_name="children", on_delete=models.CASCADE
    )
    parent_agreement = models.ForeignKey(
        "self",
        null=True,
        related_name="child_agreement",
        on_delete=models.CASCADE,
    )
    discount_percentage = models.IntegerField(default=0)
    total_discount = models.DecimalField(
        decimal_places=2, max_digits=10, default=0
    )
    pax8_products = JSONField(default=dict)
    forecast_data = JSONField(default=dict)

    class Meta:
        ordering = ["-updated_on"]

    def __str__(self):
        return self.name

    @cached_property
    def version(self):
        return f"{self.major_version}.{self.minor_version}"

    def get_root_agreement_id(self) -> int:
        if self.root_agreement is None and self.parent_agreement is None:
            return self.id
        else:
            return self.root_agreement_id

    def get_additional_data(self):
        if self.root_agreement:
            return {
                "agreement_id": self.id,
                "root_agreement_id": self.root_agreement.id,
                "is_disabled": self.is_disabled,
                "version": self.version,
            }
        else:
            return {
                "agreement_id": self.id,
                "root_agreement_id": "",
                "is_disabled": self.is_disabled,
                "version": self.version,
            }


class AgreementMarginSettings(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="agreement_margin_settings",
    )
    margin_limit = models.IntegerField(default=25)

    class Meta:
        unique_together = ("provider", "margin_limit")


common_exclude_fields = [
    "id",
    "created_on",
    "updated_on",
    "provider",
]
auditlog.register(Agreement, exclude_fields=common_exclude_fields)
auditlog.register(AgreementTemplate, exclude_fields=common_exclude_fields)


class AutomatedCollectionNoticesSettings(models.Model):
    """
    Model to store automated collection notices settings for a Provider.
    """

    MONDAY = "Monday"
    TUESDAY = "Tuesday"
    WEDNESDAY = "Wednesday"
    THURSDAY = "Thursday"
    FRIDAY = "Friday"
    SATURDAY = "Saturday"
    SUNDAY = "Sunday"
    WEEKLY_SCHEDULE_CHOICES = [
        (MONDAY, MONDAY),
        (TUESDAY, TUESDAY),
        (WEDNESDAY, WEDNESDAY),
        (THURSDAY, THURSDAY),
        (FRIDAY, FRIDAY),
        (SATURDAY, SATURDAY),
        (SUNDAY, SUNDAY),
    ]
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="automated_collection_notices_setting",
    )
    weekly_send_schedule = ArrayField(
        base_field=models.CharField(
            blank=False,
            null=False,
            choices=WEEKLY_SCHEDULE_CHOICES,
            max_length=15,
        ),
        blank=False,
    )
    send_to_email = ArrayField(
        base_field=models.EmailField(blank=False, null=False),
        blank=False,
    )
    send_from_email = models.EmailField(blank=False)
    invoice_exclude_patterns = ArrayField(
        base_field=models.CharField(blank=False, null=False, max_length=20),
        blank=True,
        default=list,
    )
    billing_statuses = JSONField(default=list)
    approaching_due_date_ageing = models.IntegerField(null=False)
    email_body_template = models.TextField(blank=False)
    subject = models.CharField(null=False, blank=False, max_length=200)
    contact_type_crm_id = models.PositiveIntegerField(null=True)

    def __str__(self):
        return f"Automated Collection Notices Setting for Provider ID: {self.provider_id}"

    def get_billing_status_ids(self) -> List[Optional[int]]:
        billing_statuses: Optional[List[Dict]] = self.billing_statuses
        if not billing_statuses:
            return []
        else:
            billing_status_ids: List[int] = [
                status.get("billing_status_id") for status in billing_statuses
            ]
            return billing_status_ids

    @staticmethod
    def get_emails_template_directory_path() -> str:
        template_directory: str = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            os.path.join("collection_notice_emails"),
        )
        return template_directory

    @classmethod
    def configured_for_provider(cls, provider) -> bool:
        """
        Check if automated collection notices setting  configured for Provider or not.

        Args:
            provider: Provider

        Returns:
            True if configured else False
        """
        try:
            provider.automated_collection_notices_setting
            return True
        except AutomatedCollectionNoticesSettings.DoesNotExist:
            return False

    def scheduled_for_today(self) -> bool:
        """
        Check if emails schedule configured for today.

        Returns:
            True if scheduled else False
        """
        scheduled_days: Set[str] = {
            day.upper() for day in self.weekly_send_schedule
        }
        current_datetime = timezone.now().date()
        today: str = current_datetime.strftime("%A").upper()
        return today in scheduled_days


class PAX8IntegrationSettings(models.Model):
    """
    Model to store PAX8 API integration settings.
    """

    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="pax8_integration_settings",
    )
    client_id = models.CharField(max_length=150, blank=False, null=False)
    client_secret = models.CharField(max_length=150, blank=False, null=False)
    vendors = ArrayField(
        base_field=models.CharField(max_length=200, blank=False, null=False),
        blank=True,
        default=list,
    )


class PAX8Product(TimeStampedModel):
    """
    Model to store PAX8 product information.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="pax8_products",
    )
    crm_id = models.CharField(max_length=150, blank=False, null=False)
    name = models.CharField(max_length=150, blank=False, null=False)
    vendor_name = models.CharField(max_length=150, null=True, blank=True)
    short_description = models.TextField(blank=True, null=True)
    sku = models.CharField(max_length=150, null=True, blank=True)
    vendor_sku = models.CharField(max_length=150, null=True, blank=True)
    alt_vendor_sku = models.CharField(max_length=150, null=True, blank=True)


class PAX8ProductPricingInfo(TimeStampedModel):
    """
    Model to store PAX8 product pricing information.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="+",
    )
    pax8_product = models.OneToOneField(
        "sales.PAX8Product",
        on_delete=models.CASCADE,
        related_name="pricing_info",
    )
    pricing_data = JSONField(default=list)
