# import django_filters
from django_filters import rest_framework as filters


from common.custom_filters import ListFilter, ArrayFieldContainsFilter
from sales.models import Agreement
from sales.models import (
    SalesActivity,
    AgreementTemplate,
    PAX8ProductPricingInfo,
)


class SalesActivityFilterSet(filters.FilterSet):
    assigned_to = ListFilter()
    created_by = ListFilter()
    customer = ListFilter()
    status = ListFilter()
    priority = ListFilter()
    activity_type = ArrayFieldContainsFilter()

    due_date_after = filters.DateFilter(
        field_name="due_date",
        lookup_expr=("gte"),
    )
    due_date_before = filters.DateFilter(
        field_name="due_date",
        lookup_expr=("lte"),
    )

    class Meta:
        model = SalesActivity
        fields = [
            "assigned_to",
            "created_by",
            "customer_id",
            "status",
            "priority",
            "activity_type",
        ]


class SalesAgreementTemplateFilterset(filters.FilterSet):
    name = filters.CharFilter(field_name="name", lookup_expr=("iexact"))
    updated_after = filters.DateFilter(
        field_name="updated_on", lookup_expr="date__gte"
    )
    updated_before = filters.DateFilter(
        field_name="updated_on", lookup_expr="date__lte"
    )
    created_after = filters.DateFilter(
        field_name="created_on", lookup_expr="date__gte"
    )
    created_before = filters.DateFilter(
        field_name="created_on", lookup_expr="date__lte"
    )
    author_id = filters.CharFilter(
        field_name="author_id", lookup_expr=("exact")
    )

    class Meta:
        model = AgreementTemplate
        fields = [
            "name",
            "updated_after",
            "updated_before",
            "created_after",
            "created_before",
            "author_id",
        ]


class AgreementFilterSet(filters.FilterSet):
    customer = ListFilter()
    updated_by = ListFilter()
    template = ListFilter()

    created_after = filters.DateFilter(
        field_name="created_on",
        lookup_expr="date__gte",
    )
    created_before = filters.DateFilter(
        field_name="created_on",
        lookup_expr="date__lte",
    )
    updated_after = filters.DateFilter(
        field_name="updated_on",
        lookup_expr="date__gte",
    )
    updated_before = filters.DateFilter(
        field_name="updated_on",
        lookup_expr="date__lte",
    )
    author_id = filters.CharFilter(
        field_name="author_id", lookup_expr=("exact")
    )

    class Meta:
        model = Agreement
        fields = ["customer", "updated_by", "template", "author_id"]


class PAX8ProductListFilterSet(filters.FilterSet):
    vendor = filters.CharFilter(
        field_name="pax8_product__vendor_name", lookup_expr=("iexact")
    )

    class Meta:
        model = PAX8ProductPricingInfo
        fields = ["vendor"]
