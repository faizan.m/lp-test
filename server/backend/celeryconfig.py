# Celery Settings
import os

from celery.schedules import crontab
from django.conf.global_settings import TIME_ZONE

from celery_routes import CELERY_QUEUES, CELERY_ROUTES, default

broker_url = os.environ.get("CELERY_BROKER_URL", "redis://redis:6379/0")
result_backend = os.environ.get(
    "CELERY_RESULT_BACKEND", "redis://redis:6379/0"
)
accept_content = ["application/json"]
task_ignore_result = False
task_serializer = "json"
result_serializer = "json"
timezone = TIME_ZONE

task_default_queue = default
task_default_exchange = default
task_default_routing_key = default
task_queues = CELERY_QUEUES
task_routes = CELERY_ROUTES

beat_schedule = {
    # Periodic task to fetch data for entries in Cisco Missing Data table.
    "Cisco-Missing-Data-Task": {
        "task": "core.tasks.cisco.tasks.cisco_missing_data_task",
        "schedule": crontab(minute=30, hour="*/1"),
    },
    # Periodic task to Update data in Device Manufacturer table.
    "Cisco-data-daily-Update-Task": {
        "task": "core.tasks.cisco.tasks.device_manufacturer_data_sync_task",
        "schedule": crontab(minute=0, hour=0),
    },
    # Periodic task to sync cw data
    "CW-data-syc-task": {
        "task": "core.tasks.connectwise.connectwise.cw_data_synchronization_task",
        "schedule": crontab(minute=0, hour="*/1"),
    },
    # Periodic task to sync customer and customer users
    # Runs at 12.30 AM everyday
    "CW-customer-data-full-sync": {
        "task": "core.tasks.connectwise.connectwise.cw_customer_data_full_sync",
        "schedule": crontab(minute=30, hour=0),
    },
    # Periodic task to fetch data fro entries in Cisco Missing Data for Non alphanumeric serial numbers
    "Cisco-Missing-Data-Sync-For-Non-Alnum-SerialNumbers-Task": {
        "task": "core.tasks.cisco.tasks.cisco_missing_data_for_non_alnum_serial_numbers",
        "schedule": crontab(minute=0, hour="*/3"),
    },
    # Periodic task to update device types for devices in Device manufacturer data.
    "Cisco-Device-Sync-Device-Type-Task": {
        "task": "core.tasks.cisco.tasks.sync_cisco_device_type_task",
        "schedule": crontab(minute=15, hour="*/1"),
    },
    # Periodic task to update cisco search cases in ConnectWIse and in Acela.
    "Cisco-Device-Sync-Search-Cases-Task": {
        "task": "core.tasks.cisco.tasks.sync_cisco_device_search_lines",
        "schedule": crontab(minute="0", hour="*/5"),
    },
    # Periodic task to sync cisco subscription data in Acela and ConnectWise.
    "Cisco-Subscription-Data-Sync-Task": {
        "task": "core.tasks.cisco.tasks.sync_cisco_subscription_data",
        "schedule": crontab(minute=0, hour=22),
    },
    # Periodic task to sync device data from acela to Connectwise.
    "CW-device-data-sync-task": {
        "task": "core.tasks.connectwise.connectwise.sync_cw_device_data",
        "schedule": crontab(minute=0, hour=23),
    },
    # Delete entries from RenewalHaltedDevice with created date before 90 days
    "remove-halted-device-task": {
        "task": "renewal.tasks.delete_devices_on_renewal_hold_status",
        "schedule": crontab(minute=0, hour=0),
    },
    # Sync Logic Monitor data to update device name on CW side
    "LM-sync-device-name": {
        "task": "core.tasks.logic_monitor.logic_monitor.logic_monitor_sync_device_name",
        "schedule": crontab(minute=0, hour=22),
    },
    # Sync file storage directory structure for customer for each Provider
    # "File-storage-sync": {
    #     "task": "core.tasks.dropbox.dropbox.sync_file_storage_for_providers",
    #     "schedule": crontab(minute=0, hour=2),
    # },
    # Send Schedule User reports
    "User-Scheduled-Reports": {
        "task": "core.tasks.tasks.send_scheduled_user_device_reports",
        "schedule": crontab(minute=0, hour=10),
    },
    # Sync Logic Monitor Device Config data.
    "Sync-Logic-Monitor-Config": {
        "task": "core.tasks.logic_monitor.logic_monitor.sync_logic_monitor_config_data",
        "schedule": crontab(minute="0", hour="*/6"),
    },
    # expected engineering hours metric calculation task
    # Run every hour on sunday and monday
    "Schedule-Expected-Engineering-Hours-Metric-Task": {
        "task": "document.tasks.document_tasks.expected_engineering_hours_metric_task",
        "schedule": crontab(day_of_week="0,1", hour="*/1", minute="0"),
    },
    # Recalculate Config Compliance Score task
    # Run every 30 minutes
    "Calculate-Config-Compliance-Score": {
        "task": "config_compliance.tasks.compliance.store_config_compliance_score",
        "schedule": crontab(minute=0, hour="*/4"),
    },
    # Sync projects from connectwise.
    # Runs every 1 hour in the 10th minute.
    "Project-Sync-Task": {
        "task": "project_management.tasks.project_tasks.sync_projects",
        "schedule": crontab(minute=10, hour="*/1"),
    },
    # Sync projects engineers from connectwise.
    # Runs every 1 hour in the 10th minute.
    "Project-Engineer-Sync-Task": {
        "task": "project_management.tasks.project_tasks.sync_projects_engineers",
        "schedule": crontab(minute=10, hour="*/1"),
    },
    # Moved project items to archive
    # Runs every mid-night
    "Archive-Project-Items": {
        "task": "project_management.tasks.project_tasks.archive_project_items",
        "schedule": crontab(minute=0, hour=1),
    },
    # Periodic task to sync CW & Ingram PO data for operations dashboard
    # Runs every 6 hours
    "Operations-Dashboard-Sync-Chain-Task": {
        "task": "document.tasks.operations_dashboard.sync_order_tracking_dashboard_data",
        "schedule": crontab(minute=0, hour="*/6"),
    },
    # Periodic task to sync CW sales orders and products data for customer dashboard
    # Run every 8 hours
    "Customer-Sales-Orders-Sync-Chain-Task": {
        "task": "document.tasks.customer_order_tracking.sync_sales_order_tracking_data",
        "schedule": crontab(minute=0, hour="*/8"),
    },
    # Periodic task to sync the status of overdue critical path items and action items
    # of a project.
    # Runs every 6 hours at 30th minute
    "Project-Items-Status-Sync": {
        "task": "project_management.tasks.project_tasks.sync_status_of_critical_path_and_action_items",
        "schedule": crontab(minute=30, hour="*/6"),
    },
    # Periodic task to send collection notice emails. The emails are sent only on scheduled day.
    # Runs every day at 9 AM PST (4 PM UTC time)
    "Automated-Collection-Notice-Email-Task": {
        "task": "sales.tasks.send_collection_notices",
        "schedule": crontab(minute=0, hour=16)
    },
    # Periodic task to sync PAX8 products and pricing data.
    # Runs every day at 12.30 AM UTC
    "PAX8-Products-Sync-Task": {
        "task": "sales.tasks.sync_pax8_products_and_pricing_data",
        "schedule": crontab(minute=30, hour=0)
    },
    # Send FIRE report email.
    # Run everyday at 9 AM PST (4 PM UTC time)
    "FIRE-Report-Email-Task": {
        "task": "document.tasks.fire_report.send_fire_report_email",
        "schedule": crontab(minute=0, hour=16)
    },
    # Send expiring subscription notification emails.
    # Runs every day at 9 AM PST (4 PM UTC time)
    "Subscription-Notification-Email-Task": {
        "task": "core.tasks.tasks.send_automated_subscription_notification_emails",
        "schedule": crontab(minute=0, hour=16)
    },
    # Send order tracking weekly recap emails.
    # Runs every day at 8 AM PST (3 PM UTC time)
    "Order-Tracking-Weekly-Recap-Email-Task": {
        "task": "document.tasks.customer_order_tracking.send_automated_order_tracking_weekly_recap_emails",
        "schedule": crontab(minute=0, hour=15)
    },
    # Sync opportunities
    # Runs every 6 hours at 30th minute
    "Sync-Opportunities": {
        "task": "document.tasks.operations_dashboard.sync_opportunities",
        "schedule": crontab(minute=30, hour="*/6")
    }
}
