from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework_jwt.views import refresh_jwt_token

from accounts.circuit_import_views import CircuitImportAPIView
from accounts.views import (
    ChangePasswordView,
    CircuitInfoListCreateAPIView,
    CircuitInfoReportDownloadAPIView,
    CircuitInfoRetrieveUpdateDestroyAPIView,
    CircuitInfoTypesListCreateAPIView,
    CircuitInfoTypesRetrieveUpdateDeleteAPIView,
    CircuitReportingTypeRetrieveAPIView,
    CustomerEscalationFieldsListAPIView,
    CustomerUserMetricDashboardAPIView,
    CustomerUserRetrieveUpdateAPIView,
    ForgotPasswordView,
    JWTView,
    ListCreateCustomerEscalationAPIView,
    ListCreateCustomerUserAPIView,
    ListCreateProviderUserAPIView,
    ListCreateSuperuserUserAPIView,
    ProfilePicUpdateAPIView,
    ProfileRetrieveUpdateAPIView,
    ProviderUserRetrieveUpdateDeleteAPIView,
    RequestPhoneVerification,
    ResendUserActivationAPIView,
    RetrieveUpdateDestroyCustomerEscalationAPIView,
    SetPasswordView,
    SuperuserUserRetrieveUpdateDeleteAPIView,
    TwoFactorAuthCallbackAPIView,
    TwoFactorAuthenticationRequestAPIView,
    TwoFactorAuthenticationValidateAPIView,
    UidTokenValidationAPIView,
    UserActivationAPIView,
    UserTypeRoleListAPIView,
    ValidatePhoneVerification,
    VerifyUniqueEmailAPIView,
    LoggedEmailDataListAPIView,
    LoggedEmailDataDestroyAPIView,
    ResendLoggedEmailAPIView,
    FeatureStatusListCreateAPIView,
    FeatureStatusRetrieveUpdateAPIView,
    MergingCustomerAPIView,
    FeatureWiseAccessListCreateUpdateAPIView,
    FeaturesListAPIView,
    ListProviderUserAPIView,
    User2FAConfigStatusAPIView,
    AuthyOTPValidatorAPIView,
)
from collector.views import (
    CollectorDataWebhook,
    CollectorDownloadURLAPIView,
    CollectorHeartBeatAPIView,
    CollectorQueryListCreateAPIView,
    CollectorQueryListWebhook,
    CollectorQueryRetrieveDeleteAPIView,
    CollectorQueryUpdateWebhook,
    CollectorServiceIntegrationCreateAPIView,
    CollectorServiceIntegrationUpdateAPIView,
    CollectorServiceTypeListAPIView,
    CollectorSettingsRetrieveUpdateAPIView,
    CustomerCollectorListCreateAPIView,
    CustomerCollectorRetrieveUpdateDeleteAPIView,
    TokenView,
    UninstallCollectorAPIView,
)
from config_compliance.views import (
    CustomerRuleAssignmentUpdateAPIView,
    DeviceComplianceDetailAPIView,
    DeviceConfigComplianceReportAPIView,
    DeviceConfigurationSearchAPIView,
    DeviceMonitoringConfigListAPIView,
    DeviceRuleComplianceReportAPIView,
    RerunComplianceStatsTask,
    RuleClassificationListCreateAPIView,
    RuleFunctionsListAPIView,
    RuleLevelsListAPIView,
    RuleListCreateAPIView,
    RuleRetrieveUpdateDestroyAPIView,
    RuleVariableListCreateAPIView,
    RuleVariableRetrieveUpdateDestroyAPIView,
    RuleVariableTypeListAPIView,
    RulesDashboardListAPIView,
    TriggerConfigSyncAPIView,
    ComplianceDevicesListAPIView,
)
from core.change_history.views import (
    ComplianceVariablesCreateHistoryListAPIView,
    ComplianceVariablesDeleteHistoryListRevertAPIView,
    RuleChangeHistoryListAPIView,
    RuleCreateHistoryListAPIView,
    RuleDeleteHistoryListRevertAPIView,
    RuleVariablesChangeHistoryListAPIView,
    SOWDocumentChangeHistoryListAPIView,
    SOWDocumentCreateHistoryListAPIView,
    SOWDocumentDeleteHistoryListRevertAPIView,
    SOWTemplateChangeHistoryListAPIView,
    SOWTemplateCreateHistoryListAPIView,
    SOWTemplateDeleteHistoryListRevertAPIView,
)
from core.views import (
    AcelaDashboardAPIView,
    AllCustomerDeviceListAPIView,
    AvailableCircuitInfoForAssociationListAPIView,
    ConnectwiseBoardStatusListAPIView,
    ConnectwiseCallbackAPIView,
    LogicMonitorCallbackAPIView,
    ConnectwisePhoneNumberMappingSettingCreateRetrieveUpdateView,
    ContractStatusListAPIView,
    CountriesAndStatesListAPIView,
    CountryListAPIView,
    CustomerDeviceManagementReportAPIVIew,
    CustomerListCreateAPIView,
    CustomerNoteTypesListAPIVIew,
    CustomerNotesListCreateAPIVIew,
    CustomerNotesRetrieveUpdateAPIVIew,
    CustomerProfileRetrieveUpdateAPIView,
    CustomerReportingTypeRetrieveAPIView,
    CustomerRetrieveUpdateAPIView,
    DateFormatListAPIView,
    DeviceCategoriesRetrieveAPIView,
    DeviceCategoryUpdateAPIView,
    DeviceCircuitInfoAssociationCreateAPIView,
    DeviceCircuitInfoAssociationListAPIView,
    DeviceContractUpdateAPIView,
    DeviceDataExportAPIView,
    DeviceImportFileAPIView,
    DeviceListCreateAPIView,
    DeviceManufacturerAPIRetryCountResetView,
    DeviceManufacturerUpdateAPIView,
    DeviceMonitoringDefaultKeyMappings,
    DeviceRetrieveUpdateAPIView,
    DeviceSiteUpdateAPIView,
    DeviceStatusListAPIView,
    DeviceStatusUpdateAPIView,
    DeviceTypeListAPIView,
    DevicesAnomalyReportAPIView,
    DevicesFileUploadView,
    FileStorageFileListingAPIView,
    FileStorageFileRetrieveAPIView,
    FileStorageRootFileListingAPIView,
    GetDeviceMonitoringGroups,
    GetDeviceSystemCategories,
    ImageUploadView,
    IntegrationCategoryListAPIView,
    IntegrationCategoryRetrieveAPIView,
    IntegrationTypeListAPIView,
    IntegrationTypeRetrieveAPIView,
    ListCWCommunicationTypes,
    ListCWPhoneCommunicationTypes,
    ManualDeviceTypeMappingListCreateAPIView,
    ManualDeviceTypeMappingUpdateDestroyAPIView,
    ManufacturerListAPIView,
    PollingTaskStatusAPIView,
    ProviderIntegrationExtraConfigAPIView,
    ProviderIntegrationListCreateAPIView,
    ProviderIntegrationRetrieveUpdateAPIView,
    ProviderIntegrationStatusAPIView,
    ProviderListCreateAPIView,
    ProviderRetrieveAPIView,
    ProviderRetrieveUpdateAPIView,
    ProviderSelfRetrieveUpdateAPIView,
    ReportDownloadStatsAPIView,
    ReportsAPIView,
    ScheduleUserReportListCreateAPIView,
    ScheduleUserReportRetrieveUpdateAPIView,
    ScheduledReportCategoriesRetrieveAPIView,
    ScheduledReportFrequenciesRetrieveAPIView,
    ScheduledReportTypesListAPIView,
    SiteListCreateAPIView,
    SiteUpdateDeleteAPIView,
    StateListAPIView,
    SiteBulkCreateAPIView,
    SubscriptionImportAPIView,
    SubscriptionListAPIView,
    SubscriptionProductMappingListAPIView,
    SubscriptionProductMappingListCreateAPIView,
    SubscriptionProductMappingUpdateDestroyAPIView,
    SubscriptionServiceProviderMappingListCreateAPIView,
    SubscriptionServiceProviderMappingUpdateDestroyAPIView,
    SubscriptionUpdateAPIView,
    SystemAttachmentUploadAPIView,
    SystemBusinessUnitListAPIView,
    SystemMembersAPIView,
    SystemTerritoryListAPIView,
    SystemTerritoryRetrieveUpdateAPIView,
    TimezoneListAPIView,
    TriggerCWDataSyncTaskView,
    TriggerExternalServicesDataSyncTaskView,
    ValidateAuthCredentials,
    ProviderIntegrationEmptySerialMappingAPIView,
    UploadFileToDropboxAPIView,
    FinanceReportExportAPIView,
    CustomerTypesListAPIView,
    CustomerContactTypesListAPIView,
    VendorTypeCustomersListCreateAPIView,
    VendorContactCreateAPIView,
    CustomerStatusesListAPIView,
    TeamRolesListCreateAPIView,
    AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView,
    ConfigurationMappingListCreateAPIView,
    ConfigurationMappingRetrieveUpdateDestroyAPIView,
    SubscriptionNotificationTriggerAPIView,
    SubscriptionNotificationEmailPreviewAPIView,
    CustomerDeviceManagementReportsBundleAPIVIew,
)
from core.workflow.smartnet_receiving.views import (
    CustomerPurchaseOrderListAPIView,
    ProviderSmartnetReceivingSettingsCheck,
    PurchaseOrderStatusesListAPIView,
    PurchaseOrderTicketListAPIView,
    SNTEMailNotificationRecipientListAPIView,
    ShipmentMethodsListAPIView,
    SmartnetReceivingEmailAttachmentDestroyAPIView,
    SmartnetReceivingEmailAttachmentListCreateAPIView,
    SmartnetReceivingImportAPIView,
    SmartnetReceivingImportFileValidationAPIView,
    SmartnetReceivingSettingRetrieveUpdateAPIView,
    SmartnetWorkflowOpportunityCreateAPIView,
    TestSNTNotificationEmail,
    PartialReceiveStatusListAPIView,
    MSAgentProviderAssociationListCreateUpdateAPIView,
    PurchaseOrderTicketNotesListAPIView,
    SalesOrderStatusesListAPIView,
)
from document.views import (
    AdvancedCustomerNotesListCreateAPIView,
    AdvancedCustomerNotesRetrieveUpdateDestroyAPIView,
    ChangeRequestCreateAPIView,
    GenerateEmailTemplate,
    IngramAPISettingRetrieveUpdateAPIView,
    OperationEmailTemplatePurchaseOrderListAPIView,
    OperationEmailTemplateSettingsAPIView,
    OperationPurchaseOrderDetailsListAPIView,
    OperationPurchaseOrdersHistoryExportAPIView,
    OperationPurchaseOrdersHistoryListAPIView,
    OrderReceivingLineItemListAPIView,
    OrderTrackingSettingsAPIView,
    PurchaseOrderIngramAPIDataView,
    QuoteBusinessUnitAPIView,
    QuoteListCreateAPIView,
    QuoteRetrieveUpdateAPIView,
    QuoteStagesAPIView,
    QuoteStatusAPIView,
    QuoteTypesAPIView,
    SOWBaseTemplateAPIView,
    SOWCategoryListCreateAPIView,
    SOWCategoryRetrieveUpdateAPIView,
    SOWDashboardRawDataAPIView,
    SOWDashboardRawDataExportAPIView,
    SOWDocumentCreateListAPIView,
    SOWDocumentDownloadAPIView,
    SOWDocumentPreviewAPIView,
    SOWDocumentRetrieveUpdateDestroyAPIView,
    SOWDocumentTypesListAPIView,
    SOWServiceActivityDashboardAPIView,
    SOWServiceHistoryCreatedWonDashboardAPIView,
    SOWServiceHistoryExpectedWonDashboardAPIView,
    SOWSettingsRetrieveUpdateAPIView,
    SOWTemplateListCreateAPIView,
    SOWTemplatePreview,
    SOWTemplateRetrieveUpdateDestroyAPIView,
    SendAccountManagerEmail,
    ServiceCatalogListCreateAPIView,
    ServiceCatalogRetrieveUpdateDestroyAPIView,
    ServiceCategoryListCreateAPIView,
    UpdatePurchaseOrderLineItemAPIView,
    ValidateIngramAPICredentials,
    SOWDocumentLastUpdatedStatsAPIView,
    SOWDocumentAuthorsListAPIView,
    SOWDocumentCountByTechnologyAPIView,
    SOWDashboardTableDataAPIView,
    SOWDashboardMarginChartAPIView,
    SOWDocumentGrossProfitCalculationAPIView,
    SOWDocumentCalculationDetailsAPIView,
    SalesOrderEmailTemplateGenerationAPIView,
    ServiceTicketsListAPIView,
    AllQuoteStagesListAPIView,
    CustomerOpportunitiesListAPIView,
    ExistingSOWTemplatePreview,
    POLineItemsStatuses,
    SoWHourlyResourcesDescriptionMappingListCreateAPIView,
    SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView,
    PurchaseOrderDetailsAPIView,
    PurchaseOrderLineItemsListAPIView,
    AutomatedWeeklyRecapSettingCreateRetrieveUpdateView,
    TriggerWeeklyRecapEmailAPIView,
    WeeklyRecapEmailPreviewAPIView,
)
from document.views.bill_of_materials import (
    BillOfMaterialsInfoAPIView,
)
from document.views.document import (
    SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView,
    SOWVendorOnboardingAttachmentsListCreateAPIView,
    SOWVendorOnboardingAttachmentsDestroyAPIView,
    DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView,
)
from project_management.timesheet_entry.views import (
    ChargeCodesListAPIView,
    TimeEntryCreateAPIView,
    TimeEntryListAPIView,
    TimesheetEntryDraftListCreateView,
    TimesheetEntryDraftRetrieveUpdateDestroyView,
    TimesheetEntrySettingsCreateRetrieveUpdateView,
    TimesheetPeriodProjectsListAPIView,
    TimesheetPeriodsListAPIView,
    TimeEntryUpdateDeleteAPIView,
)
from project_management.views import (
    ActivityStatusListCreateAPIView,
    ActivityStatusRetrieveUpdateDestroyAPIView,
    AdditionalSettingCreateRetrieveUpdateView,
    ChangeRequestMailTemplateCreateRetrieveUpdateView,
    CheckProjectSettingsAPIView,
    CustomerContactRoleListCreateBulkDeleteView,
    DateStatusListCreateAPIView,
    EngineerRoleMappingCreateRetrieveUpdateView,
    MeetingMailTemplateCreateRetrieveUpdateView,
    MeetingTemplateListCreateView,
    MeetingTemplateRetrieveUpdateDestroyView,
    MilestoneStatusListCreateAPIView,
    MilestoneStatusRetrieveUpdateDestroyAPIView,
    OverallStatusListCreateAPIView,
    OverallStatusRetrieveUpdateDestroyAPIView,
    PreSalesEngineerRoleMappingCreateRetrieveUpdateView,
    ProjectBoardListCreateAPIView,
    ProjectBoardRetrieveUpdateDestroyAPIView,
    ProjectBoardsListView,
    ProjectPhaseListCreateAPIView,
    ProjectPhaseRetrieveUpdateDestroyAPIView,
    ProjectRoleListAPIView,
    ProjectStatusListView,
    ProjectTypeListCreateAPIView,
    ProjectTypeRetrieveUpdateDestroyAPIView,
    ServiceBoardsListView,
    TimeStatusListCreateAPIView,
    ProjectRoleTypeListCreateBulkDeleteView,
    ProjectRateMappingListCreateAPIView,
    ProjectRateMappingRetrieveUpdateDestroyAPIView,
    AfterHoursMappingCreateRetrieveUpdateView,
)
from project_management.views.audit_logs import (
    ProjectAuditLogsListAPIView,
    ProjectSettingsAuditLogsListAPIView,
)
from project_management.views.change_request import (
    ChangeRequestEmailActionAPIView,
    ChangeRequestListCreateAPIView,
    ChangeRequestRetrieveUpdateDestroyView,
    ProcessChangeRequest,
    CancelledCRSettingCreateRetrieveUpdateView,
)
from project_management.views.meeting_views import (
    CloseOutMeetingCompleteActionView,
    CloseOutMeetingCreateView,
    CloseOutMeetingRetrieveUpdateDestroyView,
    CustomerTouchMeetingCompleteActionView,
    CustomerTouchMeetingCreateView,
    CustomerTouchMeetingRetrieveUpdateDestroyView,
    KickoffMeetingCompleteActionView,
    KickoffMeetingCreateView,
    KickoffMeetingRetrieveUpdateDestroyView,
    MeetingArchiveRetrieveAPI,
    MeetingAttendeeDeleteView,
    MeetingAttendeeListCreateAPIView,
    MeetingDocumentDeleteView,
    MeetingDocumentDownloadView,
    MeetingDocumentListCreateView,
    MeetingNoteListCreateView,
    MeetingNoteUpdateDeleteView,
    MeetingPDFPreview,
    ProjectMeetingListView,
    ProjectMeetingUpdateDeleteView,
    StatusMeetingCompleteActionView,
    StatusMeetingCreateView,
    StatusMeetingRetrieveUpdateDestroyView,
    MeetingExternalAttendeeListCreateAPIView,
    MeetingExternalAttendeeDeleteAPIView,
    CompletedMeetingPDFPreview,
)
from project_management.views.pmo_metrics import (
    PMOMetricsReviewTabDataAPIView,
    PMOMetricsBudgetReviewTabAPIView,
    PMOMetricsManagerProjectsCountChartAPIView,
    PMOMetricsProjectsCountByTypeChartAPIView,
    PMOMetricsOverallStatusProjectsCountChartAPIView,
    PMOProjectCountByEstimatedEndDateRangeChartAPIView,
    PMOMetricsNotesListCreateAPIView,
    PMOMetricsNotesRetrieveUpdateDestroyAPIView,
)
from project_management.views.project_views import (
    ActionItemListCreateView,
    ActionItemUpdateDestroyView,
    CriticalPathItemListCreateView,
    CriticalPathItemUpdateDestroyView,
    ProjectAccountManagerRetrieveView,
    ProjectActivePhaseListAPIView,
    ProjectActiveStateUpdateView,
    ProjectActivityStatusListCreateAPIView,
    ProjectCustomerContactListView,
    ProjectCustomerContactRetrieveUpdateDestroyView,
    ProjectDashboardView,
    ProjectDocumentListCreateView,
    ProjectEngineerListView,
    ProjectItemsArchiveListView,
    ProjectListAPIView,
    ProjectManagersListView,
    ProjectNoteDeleteView,
    ProjectNoteListCreateView,
    ProjectPreSalesEngineerListView,
    ProjectRetrieveUpdateAPIView,
    ProjectTeamMemberListAPIView,
    ProjectTicketsListView,
    RiskItemImpactChoicesListView,
    RiskItemListCreateView,
    RiskItemUpdateDestroyView,
    WorkRoleListAPIView,
    WorkTypeListAPIView,
    ProjectMetricsAPIView,
    ProjectCWNotesListAPIVIew,
    ProjectAdditionalContactsListCreateAPIView,
    ProjectAdditionalContactsRetrieveUpdateDestroyAPIView,
    ProjectEngineersListView,
    ClosedProjectsListAPIView,
    ProjectDocumentsDeleteAPIView,
)
from renewal.views import (
    CompletedRenewalListAPIView,
    CustomerNewRenewalsListAPIView,
    RenewAPIView,
    RenewalHaltedDevicesUpdateView,
    RenewalHistoryListAPIView,
    RenewalRejectAPIView,
    RenewalRequestCreateAPIView,
    RenewalRequestListAPIView,
    RenewalRequestReportAPIView,
    RenewalRequestSummaryListAPIView,
    RenewalTypesListAPIView,
    ServiceLevelTypesListAPIView,
)
from reporting.views import (
    CustomerAnalysisReportListView,
    CustomerAnalysisReportView,
    SOWLinkedProjectsAuditReportAPIView,
)
from sales.views import (
    SalesActivityCustomViewListCreateAPIView,
    SalesActivityCustomViewRetrieveUpdateDestroyAPIView,
    SalesActivityListCreateAPIView,
    SalesActivityPrioritySettingListCreateAPIView,
    SalesActivityPrioritySettingRetrieveUpdateDestroyAPIView,
    SalesActivityRetrieveUpdateDestroyAPIView,
    SalesActivityStatusSettingListCreateAPIView,
    SalesActivityStatusSettingRetrieveUpdateDestroyAPIView,
    AgreementListCreateAPIView,
    AgreementRetrieveUpdateDestroyAPIView,
    AgreementMarginSettingsListCreateAPIView,
    AgreementMarginSettingsRetrieveAPIView,
    AgreementVersioningAPIView,
    AgreementPDFDownloadAPIView,
    AgreementsAuditLogsListAPIView,
    SendAgreementToAccountManagerAPIView,
    SalesAgreementAuthorsListAPIView,
)
from sales.views.automated_collections import (
    AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView,
    BillingStatusesListAPIView,
    SendAutomatedCollectionNoticeEmailTaskTriggerAPIView,
    CollectionNoticeEmailPreviewAPIView,
)
from sales.views.pax8 import (
    PAX8IntegrationSettingsCreateRetrieveUpdateAPIView,
    PAX8IntegrationValidationAPIView,
    PAX8ProductsListAPIView,
    PAX8VendorsListAPIView,
    PAX8ProductDetailsAPIView,
)
from sales.views.template import (
    SalesAgreementTemplateListCreateAPIView,
    SalesAgreementTemplateRetrieveUpdateDestroyAPIView,
    SalesAgreementTemplateVersioningAPIView,
    AgreementTemplateDropdownVersionListing,
    SalesAgreementTemplatePDFDownloadAPIView,
    AgreementTemplateAuditLogsListAPIView,
    SalesAgreementTemplateAuthorsListAPIView,
)
from service_requests.views import (
    ConnectwiseTicketDocumentsDownloadView,
    LifecycleManagementDashboardAPIView,
    OpenQuotesDashboardAPIView,
    OrdersDashboardAPIView,
    QuoteDocumentsAPIView,
    ServiceDashboardAPIView,
    ServiceTicketDocumentCreateAPIView,
    ServiceTicketListCreateAPIView,
    ServiceTicketNoteCreateAPIView,
    ServiceTicketNoteRetrieveAPIView,
    ServiceTicketRetrieveUpdateAPIView,
    ServiceTicketStatus,
)
from document.views.order_tracking_dashboard import (
    OperationDashboardSettingsCreateRetrieveUpdateView,
    OrderTrackingOpenPOsByDistributorAPIView,
    OrderTrackingTableDataAPIView,
    OrderTrackingLastCustomerUpdateAPIView,
    OrderTrackingOrdersByEstimatedShippedDateAPIView,
    SyncFailedTicketsListAPIView,
    OrderTrackingOrdersShippedNotReceivedAPIView,
    CustomerSalesOrderDataAPIView,
    CustomerProductItemsAPIView,
    SalesOrdersStatusSettingsCreateRetrieveUpdateView,
    CustomFieldsListAPIView,
    UnlinkedPurchaseOrdersListAPIView,
    SalesOrderToPurchaseOrderLinkingListCreateAPIView,
    SalesOrderToPurchaseOrderLinkDeleteAPIView,
    IgnoreFailedTicketsUpdateAPIView,
    OpportunityTicketSOPOLinkingListAPIView,
    TicketOpportunitySOLinkingAPIView,
    UnmappedServiceTicketsFromSoListAPIView,
    UnmappedSosFromServiceTicketsListAPIView,
    MappedPosWithSoListAPIView,
    IgnoreSalesOrdersAPIView,
    RevokeIgnoreSalesOrdersAPIView,
    IgnoreSalesOrdersListAPIView,
)
from document.views.bill_of_materials import (
    BillOfMaterialsInfoAPIView,
)
from document.views.fire_report import (
    FireReportPartnerSiteListCreateAPIView,
    FireReportGenerationAPIView,
    FireReportSettingCreateRetriveUpdateAPIView,
    FireReportPartnerSitesRetrieveUpdateDestroyAPIView,
    PartnerSiteSyncErrorListAPIView,
)
from document.views.client360 import (
    Client360DashboardSettingCreateRetrieveUpdateView,
    OpenOpportunitiesChartAPIView,
    WonOpportuntiesChartAPIView,
    OpenPurchaseOrdersChartAPIView,
    OpenServiceTicketsChartAPIView
)

schema_view = get_schema_view(
    openapi.Info(
        title="Lookingpoint API",
        default_version="v1",
        description="""Lookingpoint API's
The `swagger-ui` view can be found [here](/swagger).
The `ReDoc` view can be found [here](/redoc).
The swagger YAML document can be found [here](/swagger.yaml).
""",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# user account related urls
accounts_url_patterns = (
    [
        # user types and roles list
        path(
            "user-types-roles",
            UserTypeRoleListAPIView.as_view(),
            name="user_types_and_roles",
        ),
        # login endpoint
        path("login", JWTView.as_view(), name="login"),
        # request two factor authentication
        path(
            "request-authentication",
            TwoFactorAuthenticationRequestAPIView.as_view(),
            name="request-authentication",
        ),
        # validate two factor authentication
        path(
            "validate-authentication",
            TwoFactorAuthenticationValidateAPIView.as_view(),
            name="validate-authentication",
        ),
        # Request phone number verification
        path(
            "request-phone-verification",
            RequestPhoneVerification.as_view(),
            name="request-phone-verification",
        ),
        # Validate phone number verification
        path(
            "validate-phone-validation",
            ValidatePhoneVerification.as_view(),
            name="validate-phone-validation",
        ),
        # refresh token
        path("token-refresh", refresh_jwt_token),
        # send set user password link
        path(
            "forgot-password",
            ForgotPasswordView.as_view(),
            name="forgot-password",
        ),
        # confirm user password
        path("set-password", SetPasswordView.as_view(), name="set_password"),
        # change password
        path(
            "change-password",
            ChangePasswordView.as_view(),
            name="change-password",
        ),
        # profile get and update
        path(
            "profile", ProfileRetrieveUpdateAPIView.as_view(), name="profile"
        ),
        # validate uid and token
        path(
            "validate-uid-token",
            UidTokenValidationAPIView.as_view(),
            name="validate-uid-token",
        ),
        # send activation email
        re_path(
            r"users/(?P<pk>[0-9A-Za-z_\-]+)/send-activation-email",
            ResendUserActivationAPIView.as_view(),
            name="resend-email",
        ),
        # Verify Unique Email
        path(
            "verify-unique-email",
            VerifyUniqueEmailAPIView.as_view(),
            name="verify-unique-email",
        ),
        # API to update profile picture
        path(
            "profile/update-picture",
            ProfilePicUpdateAPIView.as_view(),
            name="profile-pic",
        ),
        re_path(
            r"user/(?P<pk>[0-9A-Za-z_\-]+)/two-fa-status",
            User2FAConfigStatusAPIView.as_view(),
            name="user-2fa-status",
        ),
        path(
            "verify-otp",
            AuthyOTPValidatorAPIView.as_view(),
            name="verify-otp",
        ),
    ],
    "accounts",
)

superuser_url_patterns = (
    [
        # Users list create endpoint
        path(
            "users",
            ListCreateSuperuserUserAPIView.as_view(),
            name="list-create-user",
        ),
        # Users retrieve, update endpoint
        re_path(
            r"^users/(?P<pk>[0-9A-Za-z_\-]+)$",
            SuperuserUserRetrieveUpdateDeleteAPIView.as_view(),
            name="retrieve-update-user",
        ),
        # Provider List Create endpoint
        path(
            "providers",
            ProviderListCreateAPIView.as_view(),
            name="list-create-providers",
        ),
        # Provider Get and Update Endpoint
        path(
            "providers/<int:pk>",
            ProviderRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-provider",
        ),
        # Provider Users List and Create
        path(
            "providers/<int:provider_id>/users",
            ListCreateProviderUserAPIView.as_view(),
            name="list-create-user",
        ),
        # Provider Users Retrieve and Update
        re_path(
            r"^providers/(?P<provider_id>\d+)/users/(?P<pk>[0-9A-Za-z_\-]+)$",
            ProviderUserRetrieveUpdateDeleteAPIView.as_view(),
            name="retrieve-update-user",
        ),
        # dashboard
        path("dashboard", AcelaDashboardAPIView.as_view(), name="dashboard"),
    ],
    "superusers",
)
provider_pmo_url_patterns = [
    path(
        "settings/project-types",
        ProjectTypeListCreateAPIView.as_view(),
        name="list-create-project-types",
    ),
    path(
        "settings/project-types/<int:pk>",
        ProjectTypeRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-project-types",
    ),
    path(
        "settings/activity-status",
        ActivityStatusListCreateAPIView.as_view(),
        name="list-create-activity-status",
    ),
    path(
        "settings/activity-status/<int:pk>",
        ActivityStatusRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-activity-status",
    ),
    path(
        "settings/milestone-status",
        MilestoneStatusListCreateAPIView.as_view(),
        name="list-create-milestone-status",
    ),
    path(
        "settings/milestone-status/<int:pk>",
        MilestoneStatusRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-milestone-status",
    ),
    path(
        "settings/project-rate-mappings",
        ProjectRateMappingListCreateAPIView.as_view(),
        name="list-create-project-rate-mappings",
    ),
    path(
        "settings/project-rate-mappings/<int:pk>",
        ProjectRateMappingRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-project-rate-mappings",
    ),
    path(
        "settings/project-boards/<int:board_id>/overall-statuses",
        OverallStatusListCreateAPIView.as_view(),
        name="list-create-overall-status",
    ),
    path(
        "settings/project-boards/<int:board_id>/overall-statuses/<int:pk>",
        OverallStatusRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-overall-status",
    ),
    path(
        "settings/project-boards",
        ProjectBoardListCreateAPIView.as_view(),
        name="list-project-boards",
    ),
    path(
        "settings/service-boards",
        ServiceBoardsListView.as_view(),
        name="list-service-boards",
    ),
    path(
        "settings/project-boards/<int:pk>",
        ProjectBoardRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-overall-status",
    ),
    path(
        "settings/time-status",
        TimeStatusListCreateAPIView.as_view(),
        name="list-create-time-status",
    ),
    path(
        "settings/date-status",
        DateStatusListCreateAPIView.as_view(),
        name="list-create-date-status",
    ),
    path(
        "settings/project-types/<int:project_type_id>/phases",
        ProjectPhaseListCreateAPIView.as_view(),
        name="list-create-project-phases",
    ),
    path(
        "settings/project-types/<int:project_type_id>/phases/<int:pk>",
        ProjectPhaseRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-project-phases",
    ),
    path(
        "settings/customer-contact-roles",
        CustomerContactRoleListCreateBulkDeleteView.as_view(),
        name="list-create-delete-customer-contact-roles",
    ),
    path(
        "settings/project-role-types",
        ProjectRoleTypeListCreateBulkDeleteView.as_view(),
        name="list-create-delete-project-role-types",
    ),
    path(
        "settings/project-roles",
        ProjectRoleListAPIView.as_view(),
        name="list-project-roles",
    ),
    path(
        "settings/engineering-roles",
        EngineerRoleMappingCreateRetrieveUpdateView.as_view(),
        name="engineering-role-create-retrieve-update",
    ),
    path(
        "settings/pre-sales-engineering-roles",
        PreSalesEngineerRoleMappingCreateRetrieveUpdateView.as_view(),
        name="pre-sales-engineering-role-create-retrieve-update",
    ),
    path(
        "settings/after-hours-mapping",
        AfterHoursMappingCreateRetrieveUpdateView.as_view(),
        name="after-hours-work-type-mapping-create-retrieve-update",
    ),
    path(
        "settings/meeting-mail-templates",
        MeetingMailTemplateCreateRetrieveUpdateView.as_view(),
        name="meeting-mail-template",
    ),
    path(
        "settings/change-request-mail-template",
        ChangeRequestMailTemplateCreateRetrieveUpdateView.as_view(),
        name="change-request-mail-template",
    ),
    path(
        "settings/meeting-mail-templates/<int:pk>",
        MeetingMailTemplateCreateRetrieveUpdateView.as_view(),
        name="meeting-mail-template",
    ),
    path(
        "settings/meeting-templates",
        MeetingTemplateListCreateView.as_view(),
        name="list-create-meeting-templates",
    ),
    path(
        "settings/meeting-templates/<int:pk>",
        MeetingTemplateRetrieveUpdateDestroyView.as_view(),
        name="retrieve-update-destroy-meeting-templates",
    ),
    path(
        "settings/additional-settings",
        AdditionalSettingCreateRetrieveUpdateView.as_view(),
        name="pmo-additional-settings",
    ),
    path(
        "settings/check",
        CheckProjectSettingsAPIView.as_view(),
        name="check-settings",
    ),
    path(
        "settings/history",
        ProjectSettingsAuditLogsListAPIView.as_view(),
        name="pmo-settings-audit-logs",
    ),
    path(
        "project-boards",
        ProjectBoardsListView.as_view(),
        name="list-project-boards",
    ),
    path(
        "project-statuses",
        ProjectStatusListView.as_view(),
        name="list-project-statuses",
    ),
    path("projects", ProjectListAPIView.as_view(), name="list-projects"),
    path(
        "sow-linked-closed-projects",
        ClosedProjectsListAPIView.as_view(),
        name="list-sow-lined-closed-projects",
    ),
    path(
        "projects/<int:pk>",
        ProjectRetrieveUpdateAPIView.as_view(),
        name="update-project-detail",
    ),
    path(
        "projects/<int:project_id>/phases",
        ProjectActivePhaseListAPIView.as_view(),
        name="list-project-active-phases",
    ),
    path(
        "projects/<int:project_id>/phases/<int:pk>",
        ProjectActiveStateUpdateView.as_view(),
        name="update-project-active-phase",
    ),
    path(
        "projects/<int:project_id>/notes",
        ProjectNoteListCreateView.as_view(),
        name="list-create-project-notes",
    ),
    path(
        "projects/<int:project_id>/notes/<int:pk>",
        ProjectNoteDeleteView.as_view(),
        name="delete-project-notes",
    ),
    path(
        "projects/<int:project_id>/cw-notes",
        ProjectCWNotesListAPIVIew.as_view(),
        name="project-cw-notes",
    ),
    path(
        "projects/<int:project_id>/documents",
        ProjectDocumentListCreateView.as_view(),
        name="list-create-project-documents",
    ),
    path(
        "projects/<int:project_id>/documents/<int:doc_id>/delete",
        ProjectDocumentsDeleteAPIView.as_view(),
        name="delete-project-document",
    ),
    path(
        "projects/<int:project_id>/critical-path-items",
        CriticalPathItemListCreateView.as_view(),
        name="list-create-project-critical-path-items",
    ),
    path(
        "projects/<int:project_id>/critical-path-items/<int:pk>",
        CriticalPathItemUpdateDestroyView.as_view(),
        name="update-delete-project-critical-path-items",
    ),
    path(
        "projects/<int:project_id>/action-items",
        ActionItemListCreateView.as_view(),
        name="list-create-project-action-items",
    ),
    path(
        "projects/<int:project_id>/action-items/<int:pk>",
        ActionItemUpdateDestroyView.as_view(),
        name="update-delete-project-action-items",
    ),
    path(
        "projects/<int:project_id>/risk-items",
        RiskItemListCreateView.as_view(),
        name="list-create-project-risk-items",
    ),
    path(
        "projects/<int:project_id>/risk-items/<int:pk>",
        RiskItemUpdateDestroyView.as_view(),
        name="update-delete-project-risk-items",
    ),
    path(
        "projects/<int:project_id>/statuses",
        ProjectActivityStatusListCreateAPIView.as_view(),
        name="list-create-project-activity-statuses",
    ),
    path(
        "risk-item-impacts",
        RiskItemImpactChoicesListView.as_view(),
        name="list-risk-item-impacts",
    ),
    path(
        "dashboard", ProjectDashboardView.as_view(), name="project-dashboard"
    ),
    path(
        "projects/<int:project_id>/meetings",
        ProjectMeetingListView.as_view(),
        name="list-project-meeting",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:pk>",
        ProjectMeetingUpdateDeleteView.as_view(),
        name="update-delete-project-meeting",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/attendees",
        MeetingAttendeeListCreateAPIView.as_view(),
        name="list-create-meeting-attendees",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/attendees/<int:pk>",
        MeetingAttendeeDeleteView.as_view(),
        name="delete-meeting-attendees",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/external_attendees",
        MeetingExternalAttendeeListCreateAPIView.as_view(),
        name="list-create-external-meeting-attendees",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/external_attendees/<int:pk>/delete",
        MeetingExternalAttendeeDeleteAPIView.as_view(),
        name="delete-external-meeting-attendee",
    ),
    path(
        "projects/<int:project_id>/status-meetings",
        StatusMeetingCreateView.as_view(),
        name="create-status-meeting",
    ),
    path(
        "projects/<int:project_id>/status-meetings/<int:meeting_id>",
        StatusMeetingRetrieveUpdateDestroyView.as_view(),
        name="update-retrieve-delete-status-meeting",
    ),
    path(
        "projects/<int:project_id>/kickoff-meetings",
        KickoffMeetingCreateView.as_view(),
        name="create-kickoff-meeting",
    ),
    path(
        "projects/<int:project_id>/kickoff-meetings/<int:meeting_id>",
        KickoffMeetingRetrieveUpdateDestroyView.as_view(),
        name="update-retrieve-delete-kickoff-meeting",
    ),
    path(
        "projects/<int:project_id>/customertouch-meetings",
        CustomerTouchMeetingCreateView.as_view(),
        name="create-customertouch-meeting",
    ),
    path(
        "projects/<int:project_id>/customertouch-meetings/<int:meeting_id>",
        CustomerTouchMeetingRetrieveUpdateDestroyView.as_view(),
        name="update-retrieve-delete-customertouch-meeting",
    ),
    path(
        "projects/<int:project_id>/customertouch-meetings/<int:meeting_id>/actions/complete",
        CustomerTouchMeetingCompleteActionView.as_view(),
        name="complete-customertouch-meeting",
    ),
    path(
        "projects/<int:project_id>/closeout-meetings",
        CloseOutMeetingCreateView.as_view(),
        name="create-closeout-meeting",
    ),
    path(
        "projects/<int:project_id>/closeout-meetings/<int:meeting_id>",
        CloseOutMeetingRetrieveUpdateDestroyView.as_view(),
        name="update-retrieve-delete-closeout-meeting",
    ),
    path(
        "projects/<int:project_id>/closeout-meetings/<int:meeting_id>/actions/complete",
        CloseOutMeetingCompleteActionView.as_view(),
        name="complete-closeout-meeting",
    ),
    path(
        "projects/<int:project_id>/kickoff-meetings/<int:meeting_id>/actions/complete",
        KickoffMeetingCompleteActionView.as_view(),
        name="complete-kickoff-meeting",
    ),
    path(
        "projects/<int:project_id>/status-meetings/<int:meeting_id>/actions/complete",
        StatusMeetingCompleteActionView.as_view(),
        name="complete-status-meeting",
    ),
    path(
        "projects/<int:project_id>/engineers",
        ProjectEngineerListView.as_view(),
        name="list-project-engineers",
    ),
    path(
        "projects/<int:project_id>/pre-sales-engineers",
        ProjectPreSalesEngineerListView.as_view(),
        name="list-project-pre-sales-engineers",
    ),
    path(
        "projects/<int:project_id>/additional-contacts",
        ProjectAdditionalContactsListCreateAPIView.as_view(),
        name="list-create-additional-contacts",
    ),
    path(
        "projects/<int:project_id>/additional-contacts/<int:pk>",
        ProjectAdditionalContactsRetrieveUpdateDestroyAPIView.as_view(),
        name="update-delete-additional-contacts",
    ),
    path(
        "projects/<int:project_id>/team-members",
        ProjectTeamMemberListAPIView.as_view(),
        name="list-project-team-members",
    ),
    path(
        "projects/managers",
        ProjectManagersListView.as_view(),
        name="list-project-managers",
    ),
    path(
        "projects/engineers",
        ProjectEngineersListView.as_view(),
        name="list-project-engineers",
    ),
    path(
        "projects/<int:project_id>/customer-contacts",
        ProjectCustomerContactListView.as_view(),
        name="list-project-customer-contacts",
    ),
    path(
        "projects/<int:project_id>/customer-contacts/<int:contact_crm_id>",
        ProjectCustomerContactRetrieveUpdateDestroyView.as_view(),
        name="update-delete-project-customer-contacts",
    ),
    path(
        "projects/<int:project_crm_id>/tickets",
        ProjectTicketsListView.as_view(),
        name="list-project-tickets",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/notes",
        MeetingNoteListCreateView.as_view(),
        name="list-create-meeting-notes",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/notes/<int:pk>",
        MeetingNoteUpdateDeleteView.as_view(),
        name="update-delete-meeting-notes",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/archive",
        MeetingArchiveRetrieveAPI.as_view(),
        name="create-retrieve-archived-meeting",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/preview-pdf",
        MeetingPDFPreview.as_view(),
        name="preview-meeting-attachment",
    ),
    path(
        "projects/<int:project_id>/completed-meetings/<int:meeting_id>/preview-pdf",
        CompletedMeetingPDFPreview.as_view(),
        name="preview-completed-meeting-attachment",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/documents",
        MeetingDocumentListCreateView.as_view(),
        name="list-create-meeting-documents",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/documents/<int:pk>",
        MeetingDocumentDeleteView.as_view(),
        name="delete-meeting-documents",
    ),
    path(
        "projects/<int:project_id>/meetings/<int:meeting_id>/documents/<int:pk>/download",
        MeetingDocumentDownloadView.as_view(),
        name="download-meeting-documents",
    ),
    path(
        "projects/<int:project_id>/account-manager",
        ProjectAccountManagerRetrieveView.as_view(),
        name="project-account-manager",
    ),
    path(
        "projects/<int:project_id>/archived-items",
        ProjectItemsArchiveListView.as_view(),
        name="list-project-archived-items",
    ),
    path(
        "projects/<int:project_id>/change-requests",
        ChangeRequestListCreateAPIView.as_view(),
        name="list-create-change-requests",
    ),
    path(
        "projects/<int:project_id>/change-requests/<int:pk>",
        ChangeRequestRetrieveUpdateDestroyView.as_view(),
        name="retrieve-update-destroy-change-requests",
    ),
    path(
        "projects/<int:project_id>/change-requests/<int:pk>/process",
        ProcessChangeRequest.as_view(),
        name="process-change-requests",
    ),
    path(
        "projects/<int:project_id>/change-requests/<int:pk>/send-email",
        ChangeRequestEmailActionAPIView.as_view(),
        name="send-change-request-email",
    ),
    path(
        "cancelled-change-requests/setting",
        CancelledCRSettingCreateRetrieveUpdateView.as_view(),
        name="cancelled-change-request-setting",
    ),
    path(
        "projects/<int:project_id>/history",
        ProjectAuditLogsListAPIView.as_view(),
        name="list-project-log-history",
    ),
    path(
        "projects/audit/report",
        SOWLinkedProjectsAuditReportAPIView.as_view(),
        name="download-sow-lined-closed-projects-reports",
    ),
    path(
        "timesheet-entries",
        TimeEntryListAPIView.as_view(),
        name="list-time-entries",
    ),
    path(
        "timesheet-entries/charge-codes",
        ChargeCodesListAPIView.as_view(),
        name="list-charge-codes",
    ),
    path(
        "timesheet-entries/timesheet-periods",
        TimesheetPeriodsListAPIView.as_view(),
        name="list-timesheet-periods",
    ),
    path(
        "timesheet-entries/settings",
        TimesheetEntrySettingsCreateRetrieveUpdateView.as_view(),
        name="time-sheet-entry-priovider-settings",
    ),
    path(
        "timesheet-entries/drafts/<int:pk>",
        TimesheetEntryDraftRetrieveUpdateDestroyView.as_view(),
        name="update-time-entry-drafts",
    ),
    path(
        "timesheet-entries/drafts",
        TimesheetEntryDraftListCreateView.as_view(),
        name="retrieve-time-entry-drafts",
    ),
    path(
        "timesheet-entries/projects",
        TimesheetPeriodProjectsListAPIView.as_view(),
        name="retrieve-time-entry-projects",
    ),
    path(
        "timesheet-entries/update-delete",
        TimeEntryUpdateDeleteAPIView.as_view(),
        name="time-entry-update-delete",
    ),
    path(
        "timesheet-entries/submit",
        TimeEntryCreateAPIView.as_view(),
        name="submit-time-entries",
    ),
    path(
        "projects/project-dashboard-graph",
        ProjectMetricsAPIView.as_view(),
        name="project-dashboard-graph",
    ),
    path(
        "projects/<int:project_id>/order-tracking",
        BillOfMaterialsInfoAPIView.as_view(),
        name="project-BOM",
    ),
    path(
        "projects/metrics/review",
        PMOMetricsReviewTabDataAPIView.as_view(),
        name="pmo-metrics-review",
    ),
    path(
        "projects/metrics/budget-review",
        PMOMetricsBudgetReviewTabAPIView.as_view(),
        name="pmo-metrics-budget-review",
    ),
    path(
        "projects/metrics/manager-projects-count",
        PMOMetricsManagerProjectsCountChartAPIView.as_view(),
        name="pmo-metrics-manager-projects-count-chart",
    ),
    path(
        "projects/metrics/project-type-count",
        PMOMetricsProjectsCountByTypeChartAPIView.as_view(),
        name="pmo-metrics-projects-by-type-count-chart",
    ),
    path(
        "projects/metrics/project-status-count",
        PMOMetricsOverallStatusProjectsCountChartAPIView.as_view(),
        name="pmo-metrics-projects-count-by-status-chart",
    ),
    path(
        "projects/metrics/project-date-range-count",
        PMOProjectCountByEstimatedEndDateRangeChartAPIView.as_view(),
        name="pmo-metrics-projects-count-within-days-range-chart",
    ),
    path(
        "projects/metrics/notes",
        PMOMetricsNotesListCreateAPIView.as_view(),
        name="pmo-metrics-notes-tab-create-list",
    ),
    path(
        "projects/metrics/notes/<int:note_id>",
        PMOMetricsNotesRetrieveUpdateDestroyAPIView.as_view(),
        name="pmo-metrics-notes-tab-retreive-update-destroy",
    ),
]

sales_url_patterns = [
    path(
        "sales-activities",
        SalesActivityListCreateAPIView.as_view(),
        name="list-create-sales-activity",
    ),
    path(
        "sales-activities/<int:pk>",
        SalesActivityRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-sales-activity",
    ),
    path(
        "sales-activities/settings/activity-statuses",
        SalesActivityStatusSettingListCreateAPIView.as_view(),
        name="list-create-sales-activity-status",
    ),
    path(
        "sales-activities/settings/activity-status/<int:pk>",
        SalesActivityStatusSettingRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-sales-activity-status",
    ),
    path(
        "sales-activities/settings/priority-statuses",
        SalesActivityPrioritySettingListCreateAPIView.as_view(),
        name="list-create-sales-activity-status",
    ),
    path(
        "sales-activities/settings/priority-status/<int:pk>",
        SalesActivityPrioritySettingRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-sales-activity-status",
    ),
    path(
        "sales-activities/custom-view",
        SalesActivityCustomViewListCreateAPIView.as_view(),
        name="list-create-sales-activity-custom-views",
    ),
    path(
        "sales-activities/custom-view/<int:pk>",
        SalesActivityCustomViewRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-sales-activity-custom-view",
    ),
    path(
        "agreements",
        AgreementListCreateAPIView.as_view(),
        name="list-create-agreements",
    ),
    path(
        "agreements/<int:pk>",
        AgreementRetrieveUpdateDestroyAPIView.as_view(),
        name="retrieve-update-destroy-agreement",
    ),
    path(
        "agreements/<int:pk>/version-history",
        AgreementVersioningAPIView.as_view(),
        name="agreement-version-history-view",
    ),
    path(
        "agreements/history",
        AgreementsAuditLogsListAPIView.as_view(),
        name="agreements-audit-logs",
    ),
    path(
        "agreement/authors",
        SalesAgreementAuthorsListAPIView.as_view(),
        name="agreement-authors-list",
    ),
    path(
        "agreement-template/authors",
        SalesAgreementTemplateAuthorsListAPIView.as_view(),
        name="agreement-template-authors-list",
    ),
    path(
        "agreement-templates",
        SalesAgreementTemplateListCreateAPIView.as_view(),
        name="agreement-template-list-create-view",
    ),
    path(
        "agreement-templates/<int:template_id>",
        SalesAgreementTemplateRetrieveUpdateDestroyAPIView.as_view(),
        name="agreement-template-retrieve-update-destroy",
    ),
    path(
        "agreement-templates/<int:template_id>/version-history",
        SalesAgreementTemplateVersioningAPIView.as_view(),
        name="agreement-template-version-history-view",
    ),
    path(
        "agreement-templates/history",
        AgreementTemplateAuditLogsListAPIView.as_view(),
        name="agreement-templates-audit-log",
    ),
    path(
        "agreement-template-dropdown-versions-listing",
        AgreementTemplateDropdownVersionListing.as_view(),
        name="agreement-template-dropdown-versions-listing-view",
    ),
    path(
        "agreement-margin-settings",
        AgreementMarginSettingsListCreateAPIView.as_view(),
        name="agreement-margin-settings-list-create",
    ),
    path(
        "agreement-margin-settings/<int:pk>",
        AgreementMarginSettingsRetrieveAPIView.as_view(),
        name="agreement-margin-settings-retrieve-update",
    ),
    path(
        "agreement-templates/preview",
        SalesAgreementTemplatePDFDownloadAPIView.as_view(),
        name="agreement-template-preview",
    ),
    path(
        "agreements/preview",
        AgreementPDFDownloadAPIView.as_view(),
        name="agreement-preview",
    ),
    path(
        "agreements/<int:agreement_id>/send-to-account-manager",
        SendAgreementToAccountManagerAPIView.as_view(),
        name="send-agreement-to-account-manager",
    ),
    path(
        "agreement/pax8/products",
        PAX8ProductsListAPIView.as_view(),
        name="list-pax8-products",
    ),
    path(
        "agreement/pax8/vendors",
        PAX8VendorsListAPIView.as_view(),
        name="list-pax8-vendors",
    ),
    path(
        "agreement/pax8/products/<str:crm_id>",
        PAX8ProductDetailsAPIView.as_view(),
        name="pax8-product-details",
    ),
]

provider_url_patterns = (
    [  # Sales urls
        path("sales/", include(sales_url_patterns)),
        # provider pmo urls
        path("pmo/", include(provider_pmo_url_patterns)),
        # Get and Update Provider Profile.
        path(
            "profile",
            ProviderSelfRetrieveUpdateAPIView.as_view(),
            name="profile",
        ),
        # Get Provider Profile defaults with no authentication
        path(
            "profile-info",
            ProviderRetrieveAPIView.as_view(),
            name="profile-info",
        ),
        # Users list create endpoint
        path(
            "<int:provider_id>/users",
            ListProviderUserAPIView.as_view(),
            name="list-create-user",
        ),
        path(
            "users",
            ListCreateProviderUserAPIView.as_view(),
            name="list-create-user",
        ),
        # Users retrieve, update endpoint
        re_path(
            r"^users/(?P<pk>[0-9A-Za-z_\-]+)$",
            ProviderUserRetrieveUpdateDeleteAPIView.as_view(),
            name="retrieve-update-delete-user",
        ),
        # user feature status
        path(
            "user/feature/list",
            FeaturesListAPIView.as_view(),
            name="features-list",
        ),
        path(
            "user/feature/status",
            FeatureStatusListCreateAPIView.as_view(),
            name="list-create-user-feature-status",
        ),
        path(
            "user/feature/status/<int:pk>",
            FeatureStatusRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-user-feature-status",
        ),
        path(
            "user/feature/access",
            FeatureWiseAccessListCreateUpdateAPIView.as_view(),
            name="list-create-update-user-feature-status",
        ),
        # Countries List endpoint
        path("countries", CountryListAPIView.as_view(), name="list-countries"),
        # States List endpoint
        path(
            "countries/<int:country_id>/states",
            StateListAPIView.as_view(),
            name="list-countries",
        ),
        # combined api for countries and states list.
        path(
            "countries-states",
            CountriesAndStatesListAPIView.as_view(),
            name="countries-and-states-list",
        ),
        # Customer List and Create
        path(
            "customers",
            CustomerListCreateAPIView.as_view(),
            name="list-create-customer",
        ),
        # Customer Retrieve Update API
        path(
            "customers/<int:pk>",
            CustomerRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-customer",
        ),
        path(
            "customers/merge",
            MergingCustomerAPIView.as_view(),
            name="merge-multiple-customers-into-one",
        ),
        # Customer Users List and Create
        path(
            "customers/<int:customer_id>/users",
            ListCreateCustomerUserAPIView.as_view(),
            name="list-create-user",
        ),
        # Customer Users Retrieve and Update
        re_path(
            r"^customers/(?P<customer_id>\d+)/users/(?P<pk>[0-9A-Za-z_\-]+)$",
            CustomerUserRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-user",
        ),
        # Customer Users Activation Endpoint
        re_path(
            r"^customers/(?P<customer_id>\d+)/users/(?P<pk>[0-9A-Za-z_\-]+)/activate$",
            UserActivationAPIView.as_view(),
            name="activate-customer-user",
        ),
        # Provider Customer Sites endpoints
        path(
            "customers/<int:customer_id>/sites",
            SiteListCreateAPIView.as_view(),
            name="list-create-sites",
        ),
        # Provider Customer Bulk Create Sites endpoints
        path(
            "customers/<int:customer_id>/bulk-create-sites",
            SiteBulkCreateAPIView.as_view(),
            name="bulk-create-sites",
        ),
        # Provider Customer Sites endpoints
        path(
            "customers/<int:customer_id>/sites/<int:site_id>",
            SiteUpdateDeleteAPIView.as_view(),
            name="update-delete-site",
        ),
        # Provider Customer Devices list create endpoints
        path(
            "customers/<int:customer_id>/devices",
            DeviceListCreateAPIView.as_view(),
            name="device-list-create",
        ),
        # Provider Customer Software Subscription list endpoints
        path(
            "customers/<int:customer_id>/subscriptions",
            SubscriptionListAPIView.as_view(),
            name="subscriptions-list",
        ),
        path(
            "customers/<int:customer_id>/subscriptions/<int:subscription_id>",
            SubscriptionUpdateAPIView.as_view(),
            name="subscription-update",
        ),
        # Provider Customer Device Retrieve Update endpoint
        path(
            "customers/<int:customer_id>/devices/<int:device_id>",
            DeviceRetrieveUpdateAPIView.as_view(),
            name="device-retrieve-update",
        ),
        # Provider Customer Devices list create endpoints
        path(
            "customers/<int:customer_id>/devices/batch-create",
            DeviceImportFileAPIView.as_view(),
            name="device-list-create",
        ),
        # Provider Customer Devices contract update endpoint
        path(
            "customers/<int:customer_id>/devices/contract-update",
            DeviceContractUpdateAPIView.as_view(),
            name="device-contract-update",
        ),
        # Customer Devices status update endpoint
        path(
            "customers/<int:customer_id>/devices/status-update",
            DeviceStatusUpdateAPIView.as_view(),
            name="device-status-update",
        ),
        # Customer Devices category update endpoint
        path(
            "customers/<int:customer_id>/devices/category-update",
            DeviceCategoryUpdateAPIView.as_view(),
            name="device-category-update",
        ),
        # Customer Devices site update endpoint
        path(
            "customers/<int:customer_id>/devices/site-update",
            DeviceSiteUpdateAPIView.as_view(),
            name="device-site-update",
        ),
        # Customer Devices manufacturer update endpoint
        path(
            "customers/<int:customer_id>/devices/manufacturer-update",
            DeviceManufacturerUpdateAPIView.as_view(),
            name="device-manufacturer-update",
        ),
        # Provider Customer Devices data export endpoint
        path(
            "customers/<int:customer_id>/devices/export",
            DeviceDataExportAPIView.as_view(),
            name="export-device-data",
        ),
        # Provider manufacturers list endpoint
        path(
            "manufacturers",
            ManufacturerListAPIView.as_view(),
            name="manufacturer-list",
        ),
        # Provider Integration List and create endpoints
        path(
            "integrations",
            ProviderIntegrationListCreateAPIView.as_view(),
            name="integrations-list-create",
        ),
        # Provider Integrations Retrieve and Update endpoints
        path(
            "integrations/<int:pk>",
            ProviderIntegrationRetrieveUpdateAPIView.as_view(),
            name="integrations-list-create",
        ),
        # Provider Integrations Extra Config Retrieve endpoints
        path(
            "integrations/<int:pk>/extra-configs",
            ProviderIntegrationExtraConfigAPIView.as_view(),
            name="integration-extra-config",
        ),
        # Provider Integrations Update Empty Serial Mapping endpoints
        path(
            "integrations/<int:pk>/empty-serial-mapping",
            ProviderIntegrationEmptySerialMappingAPIView.as_view(),
            name="empty-serial-mapping",
        ),
        # Board Statuses List API
        path(
            "boards/<int:board_id>/statuses",
            ConnectwiseBoardStatusListAPIView.as_view(),
            name="list-board-statuses",
        ),
        # Provider Integration statuses
        path(
            "integrations/config-status",
            ProviderIntegrationStatusAPIView.as_view(),
            name="integrations-statuses",
        ),
        # List new renewals
        path(
            "customers/<int:customer_id>/new-renewals",
            CustomerNewRenewalsListAPIView.as_view(),
            name="new-renewals-list",
        ),
        # Create New Renewal Request
        path(
            "customers/<int:customer_id>/renewal-requests",
            RenewalRequestCreateAPIView.as_view(),
            name="create-renewal-request",
        ),
        # Get Pending Renewal Requests
        path(
            "customers/<int:customer_id>/pending-renewal-requests",
            RenewalRequestListAPIView.as_view(),
            name="list-renewal-requests",
        ),
        # Export Pending Renewal Requests
        path(
            "customers/<int:customer_id>/export-pending-renewal-requests",
            RenewalRequestReportAPIView.as_view(),
            name="report-pending-renewal",
        ),
        #
        path(
            "renewal-requests-summary",
            RenewalRequestSummaryListAPIView.as_view(),
            name="renewal-requests-summary",
        ),
        # Renew Devices
        path(
            "customers/<int:customer_id>/actions/renew",
            RenewAPIView.as_view(),
            name="renew",
        ),
        # Reject Renewal Request
        path(
            "customers/<int:customer_id>/renewals/<int:id>/reject",
            RenewalRejectAPIView.as_view(),
            name="reject-renewal-request",
        ),
        # Get all completed Renewal Requests for provider
        path(
            "completed-renewal-requests",
            CompletedRenewalListAPIView.as_view(),
            name="list-completed-renewal-requests",
        ),
        # Get Completed Renewal Requests for specific customer
        path(
            "customers/<int:customer_id>/completed-renewal-requests",
            CompletedRenewalListAPIView.as_view(),
            name="list-completed-renewal-requests",
        ),
        # Device Renewal History API.
        path(
            "customers/<int:customer_id>/devices/<int:device_id>/renewal-history",
            RenewalHistoryListAPIView.as_view(),
            name="Renewal history",
        ),
        # Remove device from halted renewal state
        path(
            "customers/<int:customer_id>/devices/<int:device_id>/actions/continue-renewal",
            RenewalHaltedDevicesUpdateView.as_view(),
            name="remove-renewal-halt",
        ),
        # Get all Notes for specific customers
        path(
            "customers/<int:customer_id>/notes",
            CustomerNotesListCreateAPIVIew.as_view(),
            name="list-create-customer-note",
        ),
        # Retrieve and update Customer Note.
        path(
            "customers/<int:customer_id>/notes/<int:note_id>",
            CustomerNotesRetrieveUpdateAPIVIew.as_view(),
            name="retrieve-update-customer-note",
        ),
        # Dashboard API
        path(
            "service-dashboard",
            ServiceDashboardAPIView.as_view(),
            name="service-dashboard",
        ),
        # Lifecycle Management Dashboard API
        path(
            "lifecycle-dashboard",
            LifecycleManagementDashboardAPIView.as_view(),
            name="lifecycle-dashboard",
        ),
        # Order dashboard API
        path(
            "orders-dashboard",
            OrdersDashboardAPIView.as_view(),
            name="orders-dashboard",
        ),
        # Customer User Metrics Dashboard API
        path(
            "customer-user-metrics",
            CustomerUserMetricDashboardAPIView.as_view(),
            name="customer-user-metrics",
        ),
        # Provider Devices report endpoint
        path(
            "devices/report",
            ReportsAPIView.as_view(),
            name="report-device-data",
        ),
        # Generate Device data anomaly report
        path(
            "devices/anomaly-report",
            DevicesAnomalyReportAPIView.as_view(),
            name="anomaly-device-report-data",
        ),
        # List Create Manual Model-Type Mapping
        path(
            "model-type-mapping",
            ManualDeviceTypeMappingListCreateAPIView.as_view(),
            name="model-type-mapping-list-create",
        ),
        # List Create Manual Model-Type Mapping
        path(
            "model-type-mapping/<int:pk>",
            ManualDeviceTypeMappingUpdateDestroyAPIView.as_view(),
            name="model-type-mapping-retrieve-update-destroy",
        ),
        # List Create Subscription Service Provider Mapping
        path(
            "subscription-service-provider-mapping",
            SubscriptionServiceProviderMappingListCreateAPIView.as_view(),
            name="subscription-service-provider-mapping-list-create",
        ),
        # List Create Manual Model-Type Mapping
        path(
            "subscription-service-provider-mapping/<int:pk>",
            SubscriptionServiceProviderMappingUpdateDestroyAPIView.as_view(),
            name="subscription-service-provider-mapping-retrieve-update-destroy",
        ),
        # Trigger async task to sync CW device data
        path(
            "actions/cw-device-data-sync",
            TriggerCWDataSyncTaskView.as_view(),
            name="cw-device-sync",
        ),
        # Trigger external services data sync tasks
        path(
            "actions/sync-external-services-data",
            TriggerExternalServicesDataSyncTaskView.as_view(),
            name="sync-external-services-data",
        ),
        # Devices file upload
        path(
            "devices-file-upload",
            DevicesFileUploadView.as_view(),
            name="device-file-upload",
        ),
        # Lifecycle Management Report
        path(
            "customer-device-management-report",
            CustomerDeviceManagementReportAPIVIew.as_view(),
            name="customer-device-management-report",
        ),
        # download all the Lifecycle Management Reports in a bundle excel
        path(
            "customer-device-management-reports-bundle",
            CustomerDeviceManagementReportsBundleAPIVIew.as_view(),
            name="download-customer-device-management-reports-bundle",
        ),
        # List Customer Note Types.
        path(
            "customers/note-types",
            CustomerNoteTypesListAPIVIew.as_view(),
            name="list-customer-note-types",
        ),
        # Customer Escalation List-Create
        path(
            "customers/<int:customer_id>/customer-escalation",
            ListCreateCustomerEscalationAPIView.as_view(),
            name="list-create-customer-escalation",
        ),
        # Customer Escalation Retrieve-Update-Destroy
        path(
            "customers/<int:customer_id>/customer-escalation/<int:pk>",
            RetrieveUpdateDestroyCustomerEscalationAPIView.as_view(),
            name="retrieve-update-delete-customer-escalation",
        ),
        # Provider Circuit List-Create
        path(
            "customers/<int:customer_id>/circuit-info",
            CircuitInfoListCreateAPIView.as_view(),
            name="list-create-circuit-info",
        ),
        # Provider Circuit Retrieve-Update
        path(
            "customers/<int:customer_id>/circuit-info/<int:pk>",
            CircuitInfoRetrieveUpdateDestroyAPIView.as_view(),
            name="retrieve-update-circuit-info",
        ),
        # Provider Circuit Report
        path(
            "customers/<int:customer_id>/circuit-info/report",
            CircuitInfoReportDownloadAPIView.as_view(),
            name="circuit-info-report",
        ),
        # Provider Circuit Info import
        path(
            "customers/<int:customer_id>/circuit-info/import",
            CircuitImportAPIView.as_view(),
            name="circuit-import",
        ),
        # Retrieve root folder structure while file storage authentication
        path(
            "file-storage/folders",
            FileStorageRootFileListingAPIView.as_view(),
            name="retrieve-root-level-folders",
        ),
        # Customer List Files
        path(
            "customers/<int:customer_id>/file-storage",
            FileStorageFileListingAPIView.as_view(),
            name="retrieve-customer-files",
        ),
        # Customer Download Files
        path(
            "customers/<int:customer_id>/file-storage/download",
            FileStorageFileRetrieveAPIView.as_view(),
            name="retrieve-download-files",
        ),
        # Upload files to Dropbox
        path(
            "customers/<int:customer_id>/file-storage/upload",
            UploadFileToDropboxAPIView.as_view(),
            name="dropbox-file-upload",
        ),
        # Retrieve available CircuitInfo available for Association
        path(
            "customers/<int:customer_id>/circuit-device-association/available",
            AvailableCircuitInfoForAssociationListAPIView.as_view(),
            name="circuit-device-association-available",
        ),
        # Create CircuitInfo-Device Association
        path(
            "customers/<int:customer_id>/circuit-device-association/create",
            DeviceCircuitInfoAssociationCreateAPIView.as_view(),
            name="create-circuit-device-association",
        ),
        # List CircuitInfo-Device Association for Device
        path(
            "customers/<int:customer_id>/circuit-device-association/device/<int:device_id>",
            DeviceCircuitInfoAssociationListAPIView.as_view(),
            name="list-circuit-device-association",
        ),
        # List Device Monitoring Groups
        path(
            "device-monitoring/groups",
            GetDeviceMonitoringGroups.as_view(),
            name="device-monitoring-groups",
        ),
        # List all System Categories for all the devices in the group
        path(
            "device-monitoring/groups/<int:group_id>/system-categories",
            GetDeviceSystemCategories.as_view(),
            name="device-monitoring-system-categories",
        ),
        # List all Default Key Mappings for Device Monitoring.
        path(
            "device-monitoring/default-key-mappings",
            DeviceMonitoringDefaultKeyMappings.as_view(),
            name="device-monitoring-system-categories",
        ),
        # List Service Tickets
        path(
            "customers/<int:customer_id>/service-tickets",
            ServiceTicketListCreateAPIView.as_view(),
            name="list-service-tickets",
        ),
        # Get Service Ticket
        path(
            "customers/<int:customer_id>/service-tickets/<int:pk>",
            ServiceTicketRetrieveUpdateAPIView.as_view(),
            name="retrieve-service-tickets",
        ),
        # Create Service Ticket Note
        path(
            "customers/<int:customer_id>/service-tickets/<int:pk>/notes",
            ServiceTicketNoteCreateAPIView.as_view(),
            name="create-ticket-note",
        ),
        # Download Service Ticket documents
        path(
            "customers/<int:customer_id>/service-tickets/<int:pk>/documents/<int:document_id>",
            ConnectwiseTicketDocumentsDownloadView.as_view(),
            name="download-ticket-attachments",
        ),
        # Service Ticket Note
        path(
            "customers/<int:customer_id>/service-ticket-note",
            ServiceTicketNoteRetrieveAPIView.as_view(),
            name="service-ticket-note",
        ),
        # Upload Service Ticket Documents
        path(
            "customers/<int:customer_id>/service-tickets/<int:pk>/documents",
            ServiceTicketDocumentCreateAPIView.as_view(),
            name="upload-ticket-attachments",
        ),
        path("quotes/types", QuoteTypesAPIView.as_view(), name="quote-types"),
        path(
            "quotes/statuses",
            QuoteStatusAPIView.as_view(),
            name="quote-statuses",
        ),
        path(
            "quotes/business-units",
            QuoteBusinessUnitAPIView.as_view(),
            name="quote-business-units",
        ),
        path(
            "quotes/stages", QuoteStagesAPIView.as_view(), name="quote-stages"
        ),
        path(
            "quotes/all-stages",
            AllQuoteStagesListAPIView.as_view(),
            name="all-quote-stages",
        ),
        path(
            "customers/<int:customer_id>/quotes",
            QuoteListCreateAPIView.as_view(),
            name="list-create-quote",
        ),
        path(
            "quotes/<int:quote_id>",
            QuoteRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-quote",
        ),
        path(
            "document/base-templates",
            SOWBaseTemplateAPIView.as_view(),
            name="sow-base-templates",
        ),
        path(
            "document/templates",
            SOWTemplateListCreateAPIView.as_view(),
            name="sow-templates",
        ),
        path(
            "document/templates/create-history",
            SOWTemplateCreateHistoryListAPIView.as_view(),
            name="sow-templates-create-history",
        ),
        path(
            "document/templates/delete-history",
            SOWTemplateDeleteHistoryListRevertAPIView.as_view(),
            name="sow-templates-delete-history",
        ),
        path(
            "document/templates/<int:pk>",
            SOWTemplateRetrieveUpdateDestroyAPIView.as_view(),
            name="sow-template",
        ),
        path(
            "document/templates/<int:pk>/change-history",
            SOWTemplateChangeHistoryListAPIView.as_view(),
            name="sow-template-change-history",
        ),
        path(
            "document",
            SOWDocumentCreateListAPIView.as_view(),
            name="list-create-document",
        ),
        path(
            "document/<int:pk>",
            SOWDocumentRetrieveUpdateDestroyAPIView.as_view(),
            name="document",
        ),
        path(
            "document/<int:pk>/send-account-manager-email",
            SendAccountManagerEmail.as_view(),
            name="document-am-email",
        ),
        path(
            "document/<int:pk>/change-history",
            SOWDocumentChangeHistoryListAPIView.as_view(),
            name="document-history",
        ),
        path(
            "document/create-history",
            SOWDocumentCreateHistoryListAPIView.as_view(),
            name="document-create-history",
        ),
        path(
            "document/delete-history",
            SOWDocumentDeleteHistoryListRevertAPIView.as_view(),
            name="document-delete-history",
        ),
        path(
            "document/types",
            SOWDocumentTypesListAPIView.as_view(),
            name="document-types",
        ),
        path(
            "document/<int:id>/download",
            SOWDocumentDownloadAPIView.as_view(),
            name="document-download",
        ),
        path(
            "document/category",
            SOWCategoryListCreateAPIView.as_view(),
            name="document-category",
        ),
        path(
            "document/category/<int:pk>",
            SOWCategoryRetrieveUpdateAPIView.as_view(),
            name="document-category-update",
        ),
        path(
            "document/preview",
            SOWDocumentPreviewAPIView.as_view(),
            name="document-preview",
        ),
        path(
            "document/template/<int:template_id>/preview",
            ExistingSOWTemplatePreview.as_view(),
            name="sow-template-preview",
        ),
        path(
            "document/template/preview",
            SOWTemplatePreview.as_view(),
            name="template-preview",
        ),
        path(
            "document/sow/vendor-onboarding-setting",
            SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView.as_view(),
            name="sow-vendor-onboarding-setting-create-retrieve-update",
        ),
        path(
            "document/sow/vendor-onboarding-setting/attachments",
            SOWVendorOnboardingAttachmentsListCreateAPIView.as_view(),
            name="vendor-onboarding-setting-attachment-list-create",
        ),
        path(
            "document/sow/vendor-onboarding-setting/attachments/<int:id>",
            SOWVendorOnboardingAttachmentsDestroyAPIView.as_view(),
            name="vendor-onboarding-setting-attachment-delete",
        ),
        path(
            "document/creator-role-setting",
            DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView.as_view(),
            name="document-creator-role-setting-create-retrieve-update",
        ),
        path(
            "document/sow/resource-description",
            SoWHourlyResourcesDescriptionMappingListCreateAPIView.as_view(),
            name="resource-description-list-create",
        ),
        path(
            "document/sow/resource-description/<int:id>",
            SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView.as_view(),
            name="resource-description-retrieve-update-destroy",
        ),
        path("change-request", ChangeRequestCreateAPIView.as_view()),
        path("members", SystemMembersAPIView.as_view(), name="members"),
        path(
            "territories",
            SystemTerritoryListAPIView.as_view(),
            name="territories",
        ),
        path(
            "territories/<int:id>",
            SystemTerritoryRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-territories",
        ),
        path(
            "business-units",
            SystemBusinessUnitListAPIView.as_view(),
            name="list-business-units",
        ),
        path("upload-attachments", SystemAttachmentUploadAPIView.as_view()),
        # List create service categories
        path(
            "service-categories",
            ServiceCategoryListCreateAPIView.as_view(),
            name="list-create-service-categories",
        ),
        # List Create service catalog
        path(
            "service-catalogs",
            ServiceCatalogListCreateAPIView.as_view(),
            name="list-create-service-catalogs",
        ),
        # Retrieve update Service Catalog
        path(
            "service-catalogs/<int:pk>",
            ServiceCatalogRetrieveUpdateDestroyAPIView.as_view(),
            name="list-create-service-catalogs",
        ),
        path(
            "subscription/product-mappings",
            SubscriptionProductMappingListCreateAPIView.as_view(),
            name="list-subscription-product-mapping",
        ),
        path(
            "subscription/product-mappings/<int:pk>",
            SubscriptionProductMappingUpdateDestroyAPIView.as_view(),
            name="update-subscription-product-mapping",
        ),
        # Provider Customer Devices list create endpoints
        path(
            "subscription/batch-create",
            SubscriptionImportAPIView.as_view(),
            name="subscription-import",
        ),
        # Provider Device Statuses list
        path(
            "devices/statuses",
            DeviceStatusListAPIView.as_view(),
            name="status-list",
        ),
        # Customer Analysis reports meta list API
        path(
            "customer-analysis-reports/meta",
            CustomerAnalysisReportListView.as_view(),
            name="list-customer-analysis-report-types",
        ),
        path(
            "customers/<int:customer_id>/customer-analysis-reports",
            CustomerAnalysisReportView.as_view(),
            name="download-customer-analysis-reports",
        ),
        # Provider Customer Devices Configs List endpoint
        path(
            "customers/<int:customer_id>/monitoring-configs",
            DeviceMonitoringConfigListAPIView.as_view(),
            name="monitoring-configs",
        ),
        # Provider customers Device Compliance Detail
        path(
            "customers/<int:customer_id>/devices/<int:device_id>/compliance",
            DeviceComplianceDetailAPIView.as_view(),
            name="device-config-compliance-detail",
        ),
        path(
            "customers/<int:customer_id>/devices/compliance",
            ComplianceDevicesListAPIView.as_view(),
            name="device-config-compliance-detail",
        ),
        # Provider Customer Devices Configs Search endpoint
        path(
            "customers/<int:customer_id>/configurations",
            DeviceConfigurationSearchAPIView.as_view(),
            name="device-configs-search",
        ),
        # Provider Customer Rules Dashboard endpoint
        path(
            "customers/<int:customer_id>/rules/dashboard",
            RulesDashboardListAPIView.as_view(),
            name="rules-monitoring-dashboard",
        ),
        # Provider customers Device Config Compliance Report
        path(
            "customers/<int:customer_id>/compliance-report",
            DeviceConfigComplianceReportAPIView.as_view(),
            name="config-compliance-report",
        ),
        # Provider customers Device Config Compliance Report
        path(
            "customers/<int:customer_id>/devices/<int:device_id>/compliance-report",
            DeviceRuleComplianceReportAPIView.as_view(),
            name="config-compliance-report",
        ),
        # Provider Customer Trigger Config sync task
        path(
            "customers/<int:customer_id>/config-sync",
            TriggerConfigSyncAPIView.as_view(),
            name="config-sync-task-trigger",
        ),
        # Provider SOW Document settings endpoints
        path(
            "sow-doc-settings",
            SOWSettingsRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-sow-doc-settings",
        ),
        # Provider SOW Document settings endpoints
        path(
            "order-tracking-settings",
            OrderTrackingSettingsAPIView.as_view(),
            name="retrieve-update-order-tracking-settings",
        ),
        # Report Download stats
        path(
            "report-download-stats",
            ReportDownloadStatsAPIView.as_view(),
            name="list-report-download-stats",
        ),
        # Download finance report
        path(
            "operations/finance/report",
            FinanceReportExportAPIView.as_view(),
            name="export-finance-report",
        ),
        # List Rule Classification Levels
        path(
            "config-compliance/rule-levels",
            RuleLevelsListAPIView.as_view(),
            name="config-compliance-rule-level",
        ),
        # List Rule Classification Levels
        path(
            "config-compliance/rule-functions",
            RuleFunctionsListAPIView.as_view(),
            name="config-compliance-rule-function",
        ),
        # List Create Rule Classification
        path(
            "config-compliance/rule-classifications",
            RuleClassificationListCreateAPIView.as_view(),
            name="config-compliance-rule-classification-list-create",
        ),
        # List Create Compliance Rule
        path(
            "config-compliance/rules",
            RuleListCreateAPIView.as_view(),
            name="config-compliance-rule-list-create",
        ),
        # Retrieve Update Compliance Rule
        path(
            "config-compliance/rules/<int:pk>",
            RuleRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-rule-retrieve-update",
        ),
        # Get Rule object change history
        path(
            "config-compliance/rules/<int:pk>/change-history",
            RuleChangeHistoryListAPIView.as_view(),
            name="config-compliance-rule-change-history",
        ),
        # Get Rule create history
        path(
            "config-compliance/rules/create-history",
            RuleCreateHistoryListAPIView.as_view(),
            name="config-compliance-rule-create-history",
        ),
        # Get Rule delete History
        path(
            "config-compliance/rules/delete-history",
            RuleDeleteHistoryListRevertAPIView.as_view(),
            name="config-compliance-rule-delete-history-revert",
        ),
        # List Create Compliance Rule For Customer
        path(
            "customers/<int:customer_id>/config-compliance/rules",
            RuleListCreateAPIView.as_view(),
            name="config-compliance-rule-list-create-for-customer",
        ),
        # Retrieve Update Destroy Create Compliance Rule For Customer
        path(
            "customers/<int:customer_id>/config-compliance/rules/<int:pk>",
            RuleRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-rule-retrieve-update-for-customer",
        ),
        # Retrieve Update Destroy Create Compliance Rule For Customer
        path(
            "customers/<int:customer_id>/config-compliance/rules/<int:rule_id>/<str:action>",
            CustomerRuleAssignmentUpdateAPIView.as_view(),
            name="config-compliance-rule-assigment-update-for-customer",
        ),
        # List Rule Variable Types
        path(
            "config-compliance/variable-types",
            RuleVariableTypeListAPIView.as_view(),
            name="config-compliance-variable-types",
        ),
        # List Create Rule Variable
        path(
            "config-compliance/variables",
            RuleVariableListCreateAPIView.as_view(),
            name="config-compliance-variable-list-create",
        ),
        path(
            "config-compliance/variables/create-history",
            ComplianceVariablesCreateHistoryListAPIView.as_view(),
            name="config-compliance-variable-create-history",
        ),
        path(
            "config-compliance/variables/delete-history",
            ComplianceVariablesDeleteHistoryListRevertAPIView.as_view(),
            name="config-compliance-variable-delete-history",
        ),
        # Retrieve Update Rule Variable
        path(
            "config-compliance/variables/<int:pk>",
            RuleVariableRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-retrieve-update-create",
        ),
        # Retrieve Update Rule Variable
        path(
            "config-compliance/variables/<int:pk>/change-history",
            RuleVariablesChangeHistoryListAPIView.as_view(),
            name="variable-history",
        ),
        # Rerun compliance calculation
        path(
            "config-compliance/rerun-compliance",
            RerunComplianceStatsTask.as_view(),
            name="rerun-compliance-calculation",
        ),
        # List Create Rule Variable for Customer
        path(
            "customers/<int:customer_id>/config-compliance/variables",
            RuleVariableListCreateAPIView.as_view(),
            name="config-compliance-variable-list-create-customer",
        ),
        # Retrieve Update Rule Variable for Customer
        path(
            "customers/<int:customer_id>/config-compliance/variables/<int:pk>",
            RuleVariableRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-variable-retrieve-update-customer",
        ),
        # Retrieve Update Rule Variable for Customer
        path(
            "customers/<int:customer_id>/config-compliance/variables/<int:pk>/change-history",
            RuleVariablesChangeHistoryListAPIView.as_view(),
            name="customer-variable-change-history",
        ),
        # SOW Dashboard Service Activity API
        path(
            "services-dashboards/service-activity",
            SOWServiceActivityDashboardAPIView.as_view(),
            name="sow-dashboards-service-activity",
        ),
        # SOW Dashboard Service History API
        path(
            "services-dashboards/service-history-created-won",
            SOWServiceHistoryCreatedWonDashboardAPIView.as_view(),
            name="sow-dashboards-service-activity",
        ),
        # SOW Dashboard Service History API
        path(
            "services-dashboards/service-history-expected-vs-won",
            SOWServiceHistoryExpectedWonDashboardAPIView.as_view(),
            name="sow-dashboards-expected-vs-won",
        ),
        path(
            "services-dashboards/raw-data",
            SOWDashboardRawDataAPIView.as_view(),
            name="sow-dashboards-raw-data",
        ),
        path(
            "services-dashboards/raw-data-export",
            SOWDashboardRawDataExportAPIView.as_view(),
            name="sow-dashboards-raw-data-export",
        ),
        # Collector Endpoints
        path(
            "customers/<int:customer_id>/collectors",
            CustomerCollectorListCreateAPIView.as_view(),
            name="collectors-list-create",
        ),
        path(
            "customers/<int:customer_id>/collectors/<int:pk>",
            CustomerCollectorRetrieveUpdateDeleteAPIView.as_view(),
            name="collectors-retrieve-update",
        ),
        path(
            "customers/<int:customer_id>/collectors/<int:collector_id>/integrations/<int:integration_id>",
            CollectorServiceIntegrationUpdateAPIView.as_view(),
            name="collector-integration-update",
        ),
        # Provider Collector settings endpoints
        path(
            "collector-settings",
            CollectorSettingsRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-collector-settings",
        ),
        path(
            "customers/<int:customer_id>/collectors/<int:collector_id>/queries",
            CollectorQueryListCreateAPIView.as_view(),
            name="collector-query-list-create",
        ),
        path(
            "customers/<int:customer_id>/collectors/<int:collector_id>/queries/<int:pk>",
            CollectorQueryRetrieveDeleteAPIView.as_view(),
            name="collector-query-retrieve-delete",
        ),
        path(
            "customers/types",
            CustomerTypesListAPIView.as_view(),
            name="list-customer-types",
        ),
        path(
            "customers/contact-types",
            CustomerContactTypesListAPIView.as_view(),
            name="list-customer-contact-types",
        ),
        path(
            "customers/vendors",
            VendorTypeCustomersListCreateAPIView.as_view(),
            name="list-create-vendor-type-customers",
        ),
        path(
            "customers/vendors/contact",
            VendorContactCreateAPIView.as_view(),
            name="create-vendor-contact",
        ),
        path(
            "customers/statuses",
            CustomerStatusesListAPIView.as_view(),
            name="list-customer-statuses",
        ),
        path(
            "customers/team-roles",
            TeamRolesListCreateAPIView.as_view(),
            name="list-create-team-roles",
        ),
        path(
            "customers/<int:customer_id>/purchase-orders",
            CustomerPurchaseOrderListAPIView.as_view(),
            name="list-customer-purchase-orders",
        ),
        path(
            "customers/<int:customer_id>/workflows/smartnet-receiving/validation",
            SmartnetReceivingImportFileValidationAPIView.as_view(),
            name="smartnet-receiving-validation",
        ),
        path(
            "workflows/smartnet-receiving/settings",
            SmartnetReceivingSettingRetrieveUpdateAPIView.as_view(),
            name="smartnet-receiving-settings",
        ),
        path(
            "workflows/smartnet-receiving/settings/attachments",
            SmartnetReceivingEmailAttachmentListCreateAPIView.as_view(),
            name="smartnet-receiving-setting-attachments",
        ),
        path(
            "workflows/smartnet-receiving/settings/ms-agent-provider-association",
            MSAgentProviderAssociationListCreateUpdateAPIView.as_view(),
            name="smartnet-receiving-setting-cco-id-mapping",
        ),
        path(
            "workflows/smartnet-receiving/settings/attachments/<int:pk>",
            SmartnetReceivingEmailAttachmentDestroyAPIView.as_view(),
            name="smartnet-receiving-setting-attachments-delete",
        ),
        path(
            "workflows/smartnet-receiving/shipmentmethods",
            ShipmentMethodsListAPIView.as_view(),
            name="shipment-methods-listing",
        ),
        # Sales Order status
        path(
            "workflows/smartnet-receiving/sales-order-statuses",
            SalesOrderStatusesListAPIView.as_view(),
            name="sales-order-statuses-listing",
        ),
        path(
            "workflows/smartnet-receiving/partial-receive-status",
            PartialReceiveStatusListAPIView.as_view(),
            name="partial-receive-status-listing",
        ),
        path(
            "workflows/smartnet-receiving/purchase-order-statuses",
            PurchaseOrderStatusesListAPIView.as_view(),
            name="purchase-order-statuses-listing",
        ),
        path(
            "customers/<int:customer_id>/workflows/smartnet-receiving/import",
            SmartnetReceivingImportAPIView.as_view(),
            name="smartnet-receiving-import",
        ),
        path(
            "customers/<int:customer_id>/workflows/smartnet-receiving/purchase-order-tickets",
            PurchaseOrderTicketListAPIView.as_view(),
            name="purchase-order-tickets-list",
        ),
        path(
            "purchase-order-tickets/<int:service_ticket_id>/notes",
            PurchaseOrderTicketNotesListAPIView.as_view(),
            name="purchase-order-ticket-notes",
        ),
        path(
            "customers/<int:customer_id>/workflows/smartnet-receiving/snt-email-recipient-list",
            SNTEMailNotificationRecipientListAPIView.as_view(),
            name="snt-email-recipient-list",
        ),
        path(
            "customers/<int:customer_id>/workflows/smartnet-receiving/opportunity",
            SmartnetWorkflowOpportunityCreateAPIView.as_view(),
            name="snt-workflow-opportunity-create",
        ),
        path(
            "workflows/smartnet-receiving/settings/check",
            ProviderSmartnetReceivingSettingsCheck.as_view(),
            name="smartnet-receiving-setting-check",
        ),
        path(
            "workflows/smartnet-receiving/test-snt-notification-email",
            TestSNTNotificationEmail.as_view(),
            name="test-snt-notification-email",
        ),
        path(
            "customers/<int:customer_id>/customer-notes",
            AdvancedCustomerNotesListCreateAPIView.as_view(),
            name="list-create-customer-notes",
        ),
        path(
            "customer-notes",
            AdvancedCustomerNotesListCreateAPIView.as_view(),
            name="list-create-all-customer-notes",
        ),
        path(
            "customers/<int:customer_id>/customer-notes/<int:pk>",
            AdvancedCustomerNotesRetrieveUpdateDestroyAPIView.as_view(),
            name="retrieve-update-destroy-customer-notes",
        ),
        path(
            "customer-notes/<int:pk>",
            AdvancedCustomerNotesRetrieveUpdateDestroyAPIView.as_view(),
            name="retrieve-update-destroy-customer-notes",
        ),
        path(
            "ingram-api/settings",
            IngramAPISettingRetrieveUpdateAPIView.as_view(),
            name="ingram-api-settings",
        ),
        path(
            "ingram-api/validate",
            ValidateIngramAPICredentials.as_view(),
            name="ingram-api-settings-validate",
        ),
        path(
            "email-templates",
            GenerateEmailTemplate.as_view(),
            name="generate-email-template",
        ),
        path(
            "operations/so-email-template/generation",
            SalesOrderEmailTemplateGenerationAPIView.as_view(),
            name="so-email-template-generation",
        ),
        path(
            "operations/service-tickets",
            ServiceTicketsListAPIView.as_view(),
            name="so-email-template-customer-service-tickets",
        ),
        path(
            "operations/customers/<int:customer_crm_id>/quotes",
            CustomerOpportunitiesListAPIView.as_view(),
            name="so-email-template-customer-quotes",
        ),
        path(
            "operations/email-template/purchase_orders",
            OperationEmailTemplatePurchaseOrderListAPIView.as_view(),
            name="list-purchase-orders",
        ),
        path(
            "operations/email-template/settings",
            OperationEmailTemplateSettingsAPIView.as_view(),
            name="operations-email-template-settings",
        ),
        path(
            "operations/receiving/purchase-order-details/<int:purchaseorder_id>",
            PurchaseOrderDetailsAPIView.as_view(),
            name="list-po-details",
        ),
        path(
            "operations/receiving/line-items",
            OrderReceivingLineItemListAPIView.as_view(),
            name="list-po-unreceived-receiving-line-items",
        ),
        path(
            "operations/receiving/all-line-items",
            PurchaseOrderLineItemsListAPIView.as_view(),
            name="list-po-all-line-items",
        ),
        path(
            "operations/receiving/line-items/statuses",
            POLineItemsStatuses.as_view(),
            name="po-line-items-statuses",
        ),
        path(
            "operations/receiving/ingram-data",
            PurchaseOrderIngramAPIDataView.as_view(),
            name="get-po-ingram-data",
        ),
        path(
            "operations/receiving/update-po",
            UpdatePurchaseOrderLineItemAPIView.as_view(),
            name="update-purchase-order",
        ),
        path(
            "operations/dashboard",
            OperationDashboardSettingsCreateRetrieveUpdateView.as_view(),
            name="operation-dashboard-settings",
        ),
        path(
            "order-tracking/dashboard/open-po-by-distributer",
            OrderTrackingOpenPOsByDistributorAPIView.as_view(),
            name="order-tracking-dashboard-first-chart",
        ),
        path(
            "order-tracking/dashboard/table-data",
            OrderTrackingTableDataAPIView.as_view(),
            name="order-tracking-dashboard-table-data",
        ),
        path(
            "order-tracking/dashboard/last-customer-updated-data",
            OrderTrackingLastCustomerUpdateAPIView.as_view(),
            name="order-tracking-dashboard-last-customer-updated-data",
        ),
        path(
            "order-tracking/dashboard/orders-by-est-shipped-date",
            OrderTrackingOrdersByEstimatedShippedDateAPIView.as_view(),
            name="order-tracking-dashboard-orders-by-est-shipped-date",
        ),
        path(
            "order-tracking/dashboard/orders-shipped-not-received",
            OrderTrackingOrdersShippedNotReceivedAPIView.as_view(),
            name="order-tracking-dashboard-orders-shipped-not-received",
        ),
        path(
            "order-tracking/dashboard/failed-service-tickets",
            SyncFailedTicketsListAPIView.as_view(),
            name="sync-failed-service-tickets",
        ),
        path(
            "order-tracking/dashboard/ignore-failed-service-tickets",
            IgnoreFailedTicketsUpdateAPIView.as_view(),
            name="ignore-failed-service-tickets",
        ),
        path(
            "order-tracking/dashboard/unlinked-purchase-orders",
            UnlinkedPurchaseOrdersListAPIView.as_view(),
            name="unlinked-purchase-orders",
        ),
        path(
            "order-tracking/weekly-recap/setting",
            AutomatedWeeklyRecapSettingCreateRetrieveUpdateView.as_view(),
            name="weekly-recap-setting-create-retrieve-update",
        ),
        path(
            "order-tracking/weekly-recap/email/trigger",
            TriggerWeeklyRecapEmailAPIView.as_view(),
            name="trigger-weekly-recap-emails",
        ),
        path(
            "order-tracking/weekly-recap/email/preview",
            WeeklyRecapEmailPreviewAPIView.as_view(),
            name="preview-weekly-recap-email",
        ),
        path(
            "operations/<int:customer_id>/purchase-orders-history",
            OperationPurchaseOrdersHistoryListAPIView.as_view(),
            name="list-purchase-orders-history",
        ),
        path(
            "operations/<int:purchaseorder_id>/purchase-order-history-details/<int:lineitem_id>",
            OperationPurchaseOrderDetailsListAPIView.as_view(),
            name="purchase-order-history-details",
        ),
        path(
            "operations/purchase-orders-history/export",
            OperationPurchaseOrdersHistoryExportAPIView.as_view(),
            name="export-purchase-orders-history",
        ),
        path(
            "operations/collections/automated-notices-setting",
            AutomatedCollectionNoticesSettingCreateRetrieveUpdateAPIView.as_view(),
            name="automated-collection-notices-setting",
        ),
        path(
            "operations/collections/billing-statuses",
            BillingStatusesListAPIView.as_view(),
            name="list-billing-statuses",
        ),
        path(
            "operations/collections/send-collection-notices",
            SendAutomatedCollectionNoticeEmailTaskTriggerAPIView.as_view(),
            name="send-collection-notices",
        ),
        path(
            "operations/collections/notice-email-preview",
            CollectionNoticeEmailPreviewAPIView.as_view(),
            name="preview-collection-notice",
        ),
        path(
            "work-roles", WorkRoleListAPIView.as_view(), name="list-work-roles"
        ),
        path(
            "work-types", WorkTypeListAPIView.as_view(), name="list-work-types"
        ),
        path(
            "communication-types",
            ListCWCommunicationTypes.as_view(),
            name="list-cw-communication-types",
        ),
        path(
            "phone-communication-types",
            ListCWPhoneCommunicationTypes.as_view(),
            name="list-cw-phone-communication-types",
        ),
        path(
            "communication-types/settings",
            ConnectwisePhoneNumberMappingSettingCreateRetrieveUpdateView.as_view(),
            name="update-cw-communication-types-setting",
        ),
        path(
            "document/sow-dashboard/sow-doc-update-stats",
            SOWDocumentLastUpdatedStatsAPIView.as_view(),
            name="sow-document-last-update-stats",
        ),
        path(
            "document/sow-dashboard/sow-doc-authors-list",
            SOWDocumentAuthorsListAPIView.as_view(),
            name="sow-document-authors-list",
        ),
        path(
            "document/sow-dashboard/sow-doc-count-by-type",
            SOWDocumentCountByTechnologyAPIView.as_view(),
            name="sow-document-count-by-tech",
        ),
        path(
            "document/sow-dashboard/sow-doc-table",
            SOWDashboardTableDataAPIView.as_view(),
            name="sow-document-table-data",
        ),
        # customer sales order status setting at provider level
        path(
            "sales-orders/status-settings",
            SalesOrdersStatusSettingsCreateRetrieveUpdateView.as_view(),
            name="customer-sales-orders-status-settings-at-provider-level",
        ),
        path(
            "document/sow-dashboard/sow-doc-margin-chart",
            SOWDashboardMarginChartAPIView.as_view(),
            name="sow-document-margin-chart",
        ),
        path(
            "custom-fields",
            CustomFieldsListAPIView.as_view(),
            name="custom-fields",
        ),
        path(
            "document/calculation/gross-profit",
            SOWDocumentGrossProfitCalculationAPIView.as_view(),
            name="gross-profit",
        ),
        path(
            "document/calculation/calculation-details",
            SOWDocumentCalculationDetailsAPIView.as_view(),
            name="calculation-details",
        ),
        path(
            "sales-order/po-link",
            SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view(),
            name="link-po-to-so",
        ),
        path(
            "sales-order/umapped-sos-from-tickets",
            UnmappedSosFromServiceTicketsListAPIView.as_view(),
            name="unmapped-so-from-service-tickets",
        ),
        path(
            "sales-order/opp-ticket-so-po-link",
            OpportunityTicketSOPOLinkingListAPIView.as_view(),
            name="list-opp-ticket-so-po-link",
        ),
        path(
            "sales-order/ticket-oppo-so-linking",
            TicketOpportunitySOLinkingAPIView.as_view(),
            name="link-ticket-oppo-so",
        ),
        path(
            "sales-order/remove-po-link",
            SalesOrderToPurchaseOrderLinkDeleteAPIView.as_view(),
            name="remove-po-to-so-link",
        ),
        path(
            "sales-order/unmapped-service-tickets-from-sos",
            UnmappedServiceTicketsFromSoListAPIView.as_view(),
            name="unmapped-service-tickets-from-so",
        ),
        path(
            "sales-order/mapped-pos-with-so/<int:sales_order_crm_id>",
            MappedPosWithSoListAPIView.as_view(),
            name="mapped-Pos-with-So",
        ),
        path(
            "sales-order/ignore-sos",
            IgnoreSalesOrdersAPIView.as_view(),
            name="ignore-sos-tickets",
        ),
        path(
            "sales-order/list-ignored-sos",
            IgnoreSalesOrdersListAPIView.as_view(),
            name="list-ignored-sos-tickets",
        ),
        path(
            "sales-order/revoke-ignored-sos",
            RevokeIgnoreSalesOrdersAPIView.as_view(),
            name="revoke-ignore-sos-tickets",
        ),
        path(
            "pax8-settings",
            PAX8IntegrationSettingsCreateRetrieveUpdateAPIView.as_view(),
            name="pax8-settings-create-retrieve-update",
        ),
        path(
            "pax8-settings/validate",
            PAX8IntegrationValidationAPIView.as_view(),
            name="validate-pax8-settings",
        ),
        path(
            "fire-report/partner-sites",
            FireReportPartnerSiteListCreateAPIView.as_view(),
            name="fire-report-partner-sites-list-create",
        ),
        path(
            "fire-report/partner-sites/<int:id>",
            FireReportPartnerSitesRetrieveUpdateDestroyAPIView.as_view(),
            name="fire-report-partner-sites-retrieve-update-destroy",
        ),
        path(
            "fire-report/setting",
            FireReportSettingCreateRetriveUpdateAPIView.as_view(),
            name="fire-report-setting-create-retrieve-update",
        ),
        path(
            "fire-report",
            FireReportGenerationAPIView.as_view(),
            name="fire-report-generation",
        ),
        path(
            "fire-report/partner-sites/sync-errors",
            PartnerSiteSyncErrorListAPIView.as_view(),
            name="partner-sites-sync-errors-list",
        ),
        path(
            "subscription-notices/setting",
            AutomatedSubscriptionNotificationSettingCreateRetrieveUpdateAPIView.as_view(),
            name="subscription-notices-setting-create-retrieve-update",
        ),
        path(
            "subscription-notices/config-mapping",
            ConfigurationMappingListCreateAPIView.as_view(),
            name="config-mapping-list-create",
        ),
        path(
            "subscription-notices/config-mapping/<int:pk>",
            ConfigurationMappingRetrieveUpdateDestroyAPIView.as_view(),
            name="config-mapping-retrive-update-destroy",
        ),
        path(
            "subscription-notices/send",
            SubscriptionNotificationTriggerAPIView.as_view(),
            name="send-subscription-notification",
        ),
        path(
            "subscription-notices/email-preview",
            SubscriptionNotificationEmailPreviewAPIView.as_view(),
            name="preview-subscription-notification-email",
        ),
        path(
            "client360/setting",
            Client360DashboardSettingCreateRetrieveUpdateView.as_view(),
            name="client360-setting-create-retrieve-update",
        ),
        path(
            "client360/open-opp-chart",
            OpenOpportunitiesChartAPIView.as_view(),
            name="client360-open-opp-chart",
        ),
        path(
            "client360/won-opp-chart",
            WonOpportuntiesChartAPIView.as_view(),
            name="client360-won-opp-chart",
        ),
        path(
            "client360/open-purchase-orders",
            OpenPurchaseOrdersChartAPIView.as_view(),
            name="client360-open-purchase-orders",
        ),
        path(
            "client360/open-service-tickets",
            OpenServiceTicketsChartAPIView.as_view(),
            name="client360-open-service-tickets",
        ),
    ],
    "providers",
)

user_report_url_patterns = [
    # List User Reporting Scheduling
    path("scheduled-report", ScheduleUserReportListCreateAPIView.as_view()),
    # Retrieve & Update User Reporting Scheduling
    path(
        "scheduled-report/<int:pk>",
        ScheduleUserReportRetrieveUpdateAPIView.as_view(),
    ),
]

customer_url_patterns = (
    [  # Customer Profile
        path(
            "profile",
            CustomerProfileRetrieveUpdateAPIView.as_view(),
            name="customer-profile",
        ),
        # Users list create endpoint
        # Get Customer Profile defaults with no authentication
        path(
            "profile-info",
            ProviderRetrieveAPIView.as_view(),
            name="profile-info",
        ),
        path(
            "users",
            ListCreateCustomerUserAPIView.as_view(),
            name="list-create-user",
        ),
        # Users retrieve, update endpoint
        re_path(
            r"^users/(?P<pk>[0-9A-Za-z_\-]+)$",
            CustomerUserRetrieveUpdateAPIView.as_view(),
            name="retrieve-update-delete-user",
        ),
        # Customer Users Activation Endpoint
        re_path(
            r"^users/(?P<pk>[0-9A-Za-z_\-]+)/activate$",
            UserActivationAPIView.as_view(),
            name="activate-customer-user",
        ),
        # Customer Devices List Create endpoint
        path(
            "devices",
            DeviceListCreateAPIView.as_view(),
            name="device-list-create",
        ),
        # get all customer devices
        path(
            "all-devices",
            AllCustomerDeviceListAPIView.as_view(),
            name="all-customer-devices",
        ),
        # Customer Subscription list endpoints
        path(
            "subscriptions",
            SubscriptionListAPIView.as_view(),
            name="subscriptions-list",
        ),
        path(
            "subscriptions/<int:subscription_id>",
            SubscriptionUpdateAPIView.as_view(),
            name="subscription-update",
        ),
        path(
            "subscription/product-mappings",
            SubscriptionProductMappingListAPIView.as_view(),
            name="list-subscription-product-mapping",
        ),
        # Customer Device Retrieve Update endpoint
        path(
            "devices/<int:device_id>",
            DeviceRetrieveUpdateAPIView.as_view(),
            name="device-retrieve-update",
        ),
        # Customer Devices contract update endpoint
        path(
            "devices/contract-update",
            DeviceContractUpdateAPIView.as_view(),
            name="device-contract-update",
        ),
        # Customer Devices status update endpoint
        path(
            "devices/status-update",
            DeviceStatusUpdateAPIView.as_view(),
            name="device-status-update",
        ),
        # Customer Devices category update endpoint
        path(
            "devices/category-update",
            DeviceCategoryUpdateAPIView.as_view(),
            name="device-category-update",
        ),
        # Customer Devices site update endpoint
        path(
            "devices/site-update",
            DeviceSiteUpdateAPIView.as_view(),
            name="device-site-update",
        ),
        # Customer Devices manufacturer update endpoint
        path(
            "devices/manufacturer-update",
            DeviceManufacturerUpdateAPIView.as_view(),
            name="device-manufacturer-update",
        ),
        # Customer Devices report endpoint
        path(
            "devices/report",
            ReportsAPIView.as_view(),
            name="report-device-data",
        ),
        # Device Renewal History API.
        path(
            "devices/<int:device_id>/renewal-history",
            RenewalHistoryListAPIView.as_view(),
            name="Device Renewal history",
        ),
        # Customer Sites endpoints
        path("sites", SiteListCreateAPIView.as_view(), name="list_sites"),
        # manufacturers list endpoint
        path(
            "manufacturers",
            ManufacturerListAPIView.as_view(),
            name="manufacturer-list",
        ),
        # List new renewals
        path(
            "new-renewals",
            CustomerNewRenewalsListAPIView.as_view(),
            name="new-renewals-list",
        ),
        # Get Pending Renewal Requests
        path(
            "pending-renewal-requests",
            RenewalRequestListAPIView.as_view(),
            name="list-pending-renewal-requests",
        ),
        # Get all Completed Renewal Requests by provider
        path(
            "completed-renewal-requests",
            CompletedRenewalListAPIView.as_view(),
            name="list-completed-renewal-requests",
        ),
        # Create New Renewal Request
        path(
            "renewal-requests",
            RenewalRequestCreateAPIView.as_view(),
            name="create-renewal-request",
        ),
        # Renewal History API.
        path(
            "renewal-history",
            RenewalHistoryListAPIView.as_view(),
            name="Renewal history",
        ),
        # Remove device from halted renewal state
        path(
            "devices/<int:device_id>/actions/continue-renewal",
            RenewalHaltedDevicesUpdateView.as_view(),
            name="remove-renewal-halt",
        ),
        # List Service Tickets
        path(
            "service-tickets",
            ServiceTicketListCreateAPIView.as_view(),
            name="list-service-tickets",
        ),
        # Get Service Ticket
        path(
            "service-tickets/<int:pk>",
            ServiceTicketRetrieveUpdateAPIView.as_view(),
            name="retrieve-service-tickets",
        ),
        # Create Service Ticket Note
        path(
            "service-tickets/<int:pk>/notes",
            ServiceTicketNoteCreateAPIView.as_view(),
            name="create-ticket-note",
        ),
        # Upload Service Ticket Documents
        path(
            "service-tickets/<int:pk>/documents",
            ServiceTicketDocumentCreateAPIView.as_view(),
            name="upload-ticket-attachments",
        ),
        # Download Service Ticket documents
        path(
            "service-tickets/<int:pk>/documents/<int:document_id>",
            ConnectwiseTicketDocumentsDownloadView.as_view(),
            name="download-ticket-attachments",
        ),
        # Service Ticket Note
        path(
            "service-ticket-note",
            ServiceTicketNoteRetrieveAPIView.as_view(),
            name="service-ticket-note",
        ),
        # Service Dashboard API
        path(
            "service-dashboard",
            ServiceDashboardAPIView.as_view(),
            name="service-dashboard",
        ),
        # Lifecycle Management Dashboard API
        path(
            "lifecycle-dashboard",
            LifecycleManagementDashboardAPIView.as_view(),
            name="lifecycle-dashboard",
        ),
        # Order dashboard API
        path(
            "orders-dashboard",
            OrdersDashboardAPIView.as_view(),
            name="orders-dashboard",
        ),
        # Quotes dashboard API
        path(
            "quotes-dashboard",
            OpenQuotesDashboardAPIView.as_view(),
            name="quotes-dashboard",
        ),
        # Quotes documents dashboard API
        path(
            "quotes/<int:quote_id>/documents",
            QuoteDocumentsAPIView.as_view(),
            name="quotes-documents-dashboard",
        ),
        # Lifecycle Management Report
        path(
            "customer-device-management-report",
            CustomerDeviceManagementReportAPIVIew.as_view(),
            name="customer-device-management-report",
        ),
        # Customer Escalation List-Create
        path(
            "customer-escalation",
            ListCreateCustomerEscalationAPIView.as_view(),
            name="list-create-customer-escalation",
        ),
        # Customer Escalation Retrieve-Update-Destroy
        path(
            "customer-escalation/<int:pk>",
            RetrieveUpdateDestroyCustomerEscalationAPIView.as_view(),
            name="retrieve-update-delete-customer-escalation",
        ),
        # Customer Circuit List-Create
        path(
            "circuit-info",
            CircuitInfoListCreateAPIView.as_view(),
            name="list-create-circuit-info",
        ),
        # Customer Circuit Retrieve-Update
        path(
            "circuit-info/<int:pk>",
            CircuitInfoRetrieveUpdateDestroyAPIView.as_view(),
            name="retrieve-update-circuit-info",
        ),
        # Customer circuit Info import
        path(
            "circuit-info/import",
            CircuitImportAPIView.as_view(),
            name="circuit-import",
        ),
        # Customer Circuit Report
        path(
            "circuit-info/report",
            CircuitInfoReportDownloadAPIView.as_view(),
            name="circuit-info-report",
        ),
        # Customer List Files
        path(
            "file-storage",
            FileStorageFileListingAPIView.as_view(),
            name="retrieve-customer-files",
        ),
        # Customer Download Files
        path(
            "file-storage/download",
            FileStorageFileRetrieveAPIView.as_view(),
            name="retrieve-download-file",
        ),
        # Retrieve available CircuitInfo available for Association
        path(
            "circuit-device-association/available",
            AvailableCircuitInfoForAssociationListAPIView.as_view(),
            name="circuit-device-association-available",
        ),
        # Create CircuitInfo-Device Association
        path(
            "circuit-device-association/create",
            DeviceCircuitInfoAssociationCreateAPIView.as_view(),
            name="create-circuit-device-association",
        ),
        # List CircuitInfo-Device Association for Device
        path(
            "circuit-device-association/device/<int:device_id>",
            DeviceCircuitInfoAssociationListAPIView.as_view(),
            name="list-circuit-device-association",
        ),
        path(
            "monitoring-configs",
            DeviceMonitoringConfigListAPIView.as_view(),
            name="monitoring-configs",
        ),
        # Customer Device Compliance Detail
        path(
            "devices/<int:device_id>/compliance",
            DeviceComplianceDetailAPIView.as_view(),
            name="device-config-compliance-detail",
        ),
        # Customer Devices Configs List endpoint
        path(
            "configurations",
            DeviceConfigurationSearchAPIView.as_view(),
            name="devdevice-configs-search",
        ),
        # Customer Devices Configs List endpoint
        path(
            "rules/dashboard",
            RulesDashboardListAPIView.as_view(),
            name="rules-monitoring-dashboard",
        ),
        # Customer Device Config Compliance Report
        path(
            "devices/<int:device_id>/compliance-report",
            DeviceRuleComplianceReportAPIView.as_view(),
            name="config-compliance-report",
        ),
        # Collector Endpoints
        path(
            "collectors",
            CustomerCollectorListCreateAPIView.as_view(),
            name="collectors-list-create",
        ),
        path(
            "collectors/<int:pk>",
            CustomerCollectorRetrieveUpdateDeleteAPIView.as_view(),
            name="collectors-retrieve-update",
        ),
        # List Create Compliance Rule
        path(
            "config-compliance/rules",
            RuleListCreateAPIView.as_view(),
            name="config-compliance-rule-list-create",
        ),
        # Retrieve Update Compliance Rule
        path(
            "config-compliance/rules/<int:pk>",
            RuleRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-rule-retrieve-update",
        ),
        # List Create Rule Variable
        path(
            "config-compliance/variables",
            RuleVariableListCreateAPIView.as_view(),
            name="config-compliance-variable-list-create",
        ),
        # Retrieve Update Rule Variable
        path(
            "config-compliance/variables/<int:pk>",
            RuleVariableRetrieveUpdateDestroyAPIView.as_view(),
            name="config-compliance-retrieve-update-create",
        ),
        # Retrieve Update Destroy Create Compliance Rule For Customer
        path(
            "config-compliance/rules/<int:rule_id>/<str:action>",
            CustomerRuleAssignmentUpdateAPIView.as_view(),
            name="config-compliance-rule-assigment-update-for-customer",
        ),
        # Customers Device Config Compliance Report
        path(
            "compliance-report",
            DeviceConfigComplianceReportAPIView.as_view(),
            name="config-compliance-report",
        ),
        # Customer Trigger Config sync task
        path(
            "config-sync",
            TriggerConfigSyncAPIView.as_view(),
            name="config-sync-task-trigger",
        ),
        # Rerun compliance calculation
        path(
            "config-compliance/rerun-compliance",
            RerunComplianceStatsTask.as_view(),
            name="rerun-compliance-calculation",
        ),
        path(
            "sales-orders",
            CustomerSalesOrderDataAPIView.as_view(),
            name="customer-sales-orders",
        ),
        path(
            "sales-orders/<int:sales_order_id>/product-items",
            CustomerProductItemsAPIView.as_view(),
            name="customer-sales-orders-product-items",
        ),
    ],
    "customer",
)
device_endpoints = (
    [
        # Device manufacturer api call retry count reset API
        path(
            "<slug:serial_number>/actions/reset-manufacturer-api-retry-count",
            DeviceManufacturerAPIRetryCountResetView.as_view(),
            name="device-manufacturer-api-retry-count-reset",
        )
    ],
    "device-endpoints",
)
collector_endpoints = (
    [
        path(
            "collector-download-url",
            CollectorDownloadURLAPIView.as_view(),
            name="collector-download-url",
        ),
        path(
            "collector-tokens",
            TokenView.as_view(),
            name="collector-token-get-validate",
        ),
        path(
            "collector-heartbeat",
            CollectorHeartBeatAPIView.as_view(),
            name="receive-collector-heartbeat",
        ),
        path(
            "collector-services",
            CollectorServiceIntegrationCreateAPIView.as_view(),
            name="create-collector-service",
        ),
        path(
            "collector-data",
            CollectorDataWebhook.as_view(),
            name="collector-data-webhook",
        ),
        path(
            "collector-service-types",
            CollectorServiceTypeListAPIView.as_view(),
            name="collector-service-type-list",
        ),
        path(
            "collector-queries",
            CollectorQueryListWebhook.as_view(),
            name="collector-queries-list",
        ),
        path(
            "collector-queries-update",
            CollectorQueryUpdateWebhook.as_view(),
            name="collector-queries-update",
        ),
        path(
            "collector-uninstall",
            UninstallCollectorAPIView.as_view(),
            name="collector-uninstall-webhook",
        ),
    ],
    "collector-endpoints",
)
api_v1_endpoints = (
    [
        # accounts endpoints
        path("", include(accounts_url_patterns)),
        # Integrations Categories List Endpoint
        path(
            "integration-categories", IntegrationCategoryListAPIView.as_view()
        ),
        # Integration details endpoint
        path(
            "integration-categories/<int:pk>",
            IntegrationCategoryRetrieveAPIView.as_view(),
        ),
        # Integration Types list endpoint
        path(
            "integration-categories/<int:category_id>/types",
            IntegrationTypeListAPIView.as_view(),
        ),
        # Integration Types detail endpoint
        path(
            "integration-categories/<int:category_id>/types/<int:pk>",
            IntegrationTypeRetrieveAPIView.as_view(),
        ),
        # Integrations Auth Verification endpoint
        path(
            "integration-categories/<int:category_id>/types/<int:type_id>/auth",
            ValidateAuthCredentials.as_view(),
        ),
        # Contract Status List API
        path("contract-statuses", ContractStatusListAPIView.as_view()),
        # Device types Lisiting
        path("device-types", DeviceTypeListAPIView.as_view()),
        # Renewal Types listing
        path("renewal-types", RenewalTypesListAPIView.as_view()),
        # Renewal Types listing
        path("service-level-types", ServiceLevelTypesListAPIView.as_view()),
        # Life Cycle Management report types
        path(
            "customer-reporting-types",
            CustomerReportingTypeRetrieveAPIView.as_view(),
        ),
        # Customer Escalation Fields
        path(
            "customer-escalation-fields",
            CustomerEscalationFieldsListAPIView.as_view(),
        ),
        # Retrieve Device Categories
        path("device-categories", DeviceCategoriesRetrieveAPIView.as_view()),
        # Circuit Info Types
        path(
            "circuit-info-types", CircuitInfoTypesListCreateAPIView.as_view()
        ),
        # Circuit Info Types Retrieve, Update and Delete
        path(
            "circuit-info-types/<int:pk>",
            CircuitInfoTypesRetrieveUpdateDeleteAPIView.as_view(),
        ),
        # Circuit Report Types
        path(
            "circuit-info/report/types",
            CircuitReportingTypeRetrieveAPIView.as_view(),
        ),
        # Scheduled Report Types
        path(
            "scheduled-report/types", ScheduledReportTypesListAPIView.as_view()
        ),
        # Scheduled Report Categories
        path(
            "scheduled-report/categories",
            ScheduledReportCategoriesRetrieveAPIView.as_view(),
        ),
        # Scheduled Report Frequencies
        path(
            "scheduled-report/frequencies",
            ScheduledReportFrequenciesRetrieveAPIView.as_view(),
        ),
        # Schedule report endpoint for user
        path("users/", include(user_report_url_patterns)),
        # Service Ticket status List API
        path("service-ticket-status", ServiceTicketStatus.as_view()),
        # To upload attachments
        path("image-upload", ImageUploadView.as_view()),
        # superuser endpoints
        path("superusers/", include(superuser_url_patterns)),
        # provider endpoints
        path("providers/", include(provider_url_patterns)),
        # customer endpoints
        path("customers/", include(customer_url_patterns)),
        # Device endpoints
        path("devices/", include(device_endpoints)),
        # Date Formats types
        path("date-formats", DateFormatListAPIView.as_view()),
        # Polling Task status
        path("task-status/<str:task_id>", PollingTaskStatusAPIView.as_view()),
        # Download ERP documents
        path(
            "documents/<int:document_id>/download",
            ConnectwiseTicketDocumentsDownloadView.as_view(),
            name="download-attachments",
        ),
        path("callback", TwoFactorAuthCallbackAPIView.as_view()),
        path(
            "cw-callbacks/providers/<int:provider_id>",
            ConnectwiseCallbackAPIView.as_view(),
            name="connectwise-callback-handler",
        ),
        path(
            "lm-callbacks/providers/<int:provider_id>",
            LogicMonitorCallbackAPIView.as_view(),
            name="logic-monitor-callback-handler",
        ),
        # collector endpoints
        path("", include(collector_endpoints)),
        path("timezones", TimezoneListAPIView.as_view()),
        # Logged email endpoints
        path(
            "email-logs",
            LoggedEmailDataListAPIView.as_view(),
            name="list-logged-emails",
        ),
        path(
            "email-logs/<int:pk>",
            LoggedEmailDataDestroyAPIView.as_view(),
            name="delete-logged-email",
        ),
        path(
            "email-logs/<int:pk>/resend",
            ResendLoggedEmailAPIView.as_view(),
            name="resend-logged-email",
        ),
    ],
    "api_v1_endpoints",
)
urlpatterns = [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=None),
        name="schema-json",
    ),
    re_path(
        r"^docs$",
        schema_view.with_ui("swagger", cache_timeout=None),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc$",
        schema_view.with_ui("redoc", cache_timeout=None),
        name="schema-redoc",
    ),
    # Django Admin
    path("monitoring/", include("django_prometheus.urls")),
    path("admin/", admin.site.urls),
    path("api/v1/", include(api_v1_endpoints)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
