from kombu import Exchange, Queue

default = "default"

CELERY_QUEUES = (
    Queue("high", Exchange("high"), routing_key="high"),
    Queue("default", Exchange("default"), routing_key="default"),
    Queue("slow", Exchange("slow"), routing_key="slow"),
)
CELERY_ROUTES = {
    "accounts.tasks.*": {"queue": "high"},
    "core.tasks.tasks.prepare_and_send_data_anomaly_report": {
        "queue": "high",
        "priority": 0,
    },
    "core.tasks.tasks.prepare_and_send_devices_reports_bundle": {
        "queue": "high",
        "priority": 0,
    },
    "core.tasks.connectwise.tasks.bulk_update_devices_by_value": {
        "queue": "high",
        "priority": 0,
    },
    "core.tasks.connectwise.tasks.bulk_update_devices_categories": {
        "queue": "high",
        "priority": 0,
    },
    "core.tasks.connectwise.tasks.batch_create_or_update_devices": {
        "queue": "high",
        "priority": 0,
    },
    "core.tasks.connectwise.tasks.batch_create_or_update_subscriptions": {
        "queue": "high",
        "priority": 0,
    },
    "document.tasks.document_tasks.expected_engineering_hours_metric_task": {
        "queue": "high",
        "priority": 0,
    },
    "document.tasks.operations_dashboard.sync_order_tracking_dashboard_data": {
        "queue": "high",
        "priority": 0,
    },
    "document.tasks.customer_order_tracking.sync_sales_order_tracking_data": {
        "queue": "high",
        "priority": 0,
    },
    "config_compliance.tasks.compliance.store_config_compliance_score": {
        "queue": "slow"
    },
    "core.tasks.logic_monitor.logic_monitor.sync_logic_monitor_config_data": {
        "queue": "slow"
    },
}
