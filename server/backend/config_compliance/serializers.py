import re

from box import Box
from django.core.validators import validate_email, validate_integer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from accounts.serializers import UserDropDownSerializer
from config_compliance.custom_validations import validate_hostname
from config_compliance.models import (
    Classification,
    Rule,
    RuleVariable,
    RuleVariableCustomerMapping,
)

RULE_TYPES_VALIDATORS = Box(
    EMAIL=validate_email, NUMERIC=validate_integer, HOSTNAME=validate_hostname
)


class ClassificationListCreateSerializer(serializers.ModelSerializer):
    def validate_name(self, name):
        request = self.context["request"]
        provider = request.provider
        try:
            if Classification.objects.get(name=name, provider=provider):
                raise ValidationError("Classification name already exists.")
        except Classification.DoesNotExist:
            return name

    class Meta:
        model = Classification
        exclude = ("provider",)


class RuleCreateSerializer(serializers.ModelSerializer):
    exceptions = serializers.ListField(
        allow_empty=True,
        child=serializers.CharField(max_length=1024, allow_blank=True),
    )

    def validate_detail(self, detail):
        request = self.context["request"]
        provider = request.provider
        if Rule.objects.filter(detail=detail, provider=provider):
            raise ValidationError("Rule detail already exists.")
        return detail

    def save(self, **kwargs):
        regex_validator = RuleVariable.regex_validator
        detail = self.validated_data.get("detail")
        if "\n" in detail:
            validators = set(
                re.findall(RuleVariable.inverse_regex_validator, detail)
            )
            if validators:
                raise ValidationError(
                    {
                        "detail": [
                            "Adding multiple variable codes in one word is not supported.\n "
                            "You can add spaces between variables codes."
                        ]
                    }
                )
        validators = set(re.findall(regex_validator, detail))
        variable_codes = set(
            RuleVariable.objects.filter(
                provider=kwargs["provider"]
            ).values_list("variable_code", flat=True)
        )
        diff = validators.difference(variable_codes)
        if diff:
            raise ValidationError(
                {
                    "detail": [
                        f"Variable code {','.join(diff)} doest not exists"
                    ]
                }
            )
        else:
            super(RuleCreateSerializer, self).save(**kwargs)

    class Meta:
        model = Rule
        fields = (
            "detail",
            "classification",
            "description",
            "name",
            "level",
            "applies_to",
            "applies_to_manufacturers",
            "is_enabled",
            "function",
            "created_by",
            "updated_by",
            "apply_not",
            "to_ignore_devices",
            "to_include_devices",
            "applies_to_ios_version",
            "exceptions",
        )
        read_only_fields = ("is_global", "created_by", "updated_by")


class RuleUpdateSerializer(RuleCreateSerializer):
    def validate_detail(self, detail):
        request = self.context["request"]
        rule_id = request.parser_context["kwargs"]["pk"]
        customer_id = self.context.get("customer_id")
        query_params = dict(detail=detail, provider=request.provider)
        if customer_id:
            query_params.update({"customer_id": customer_id})

        # No other global rule with the same detail should exist
        global_rule_check = (
            Rule.objects.exclude(id=rule_id)
            .filter(is_global=True, **query_params)
            .exists()
        )
        # At max one customer rule with the same details should exists
        customer_rule_check = 0
        if customer_id:
            customer_rule_check = (
                Rule.objects.exclude(id=rule_id)
                .filter(is_global=False, **query_params)
                .count()
            )
        if global_rule_check or customer_rule_check >= 1:
            raise ValidationError("Rule detail already exists.")
        return detail


class RuleRetrieveSerializer(serializers.ModelSerializer):
    created_by = UserDropDownSerializer()
    updated_by = UserDropDownSerializer()
    classification_name = serializers.CharField(
        source="classification.name", read_only=True
    )
    is_assigned = serializers.BooleanField(default=True)

    class Meta:
        model = Rule
        exclude = ("provider", "customer", "customers_not_assigned")


class RuleVariableListCreateSerializer(serializers.ModelSerializer):
    IP = "IP"
    HOSTNAME = "HOSTNAME"
    EMAIL = "EMAIL"
    TEXT = "TEXT"
    TYPE_CHOICES = (
        (IP, IP),
        (HOSTNAME, HOSTNAME),
        (EMAIL, EMAIL),
        (TEXT, TEXT),
    )
    type = serializers.ChoiceField(choices=TYPE_CHOICES)

    def validate(self, attrs):
        variable_code = attrs.get("variable_code")
        request = self.context["request"]
        provider = request.provider
        if RuleVariable.objects.filter(
            provider=provider, variable_code=variable_code
        ):
            raise ValidationError(
                {"variable_code": ["Variable Code already exists"]}
            )
        return attrs

    class Meta:
        model = RuleVariable
        exclude = ("provider", "customer")
        read_only_fields = ("is_global",)


class RuleVariableMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RuleVariableCustomerMapping
        fields = ["variable_values", "option"]


class RuleVariableRetrieveUpdateSerializer(serializers.ModelSerializer):
    customer_variables = serializers.SerializerMethodField()
    IP = "IP"
    HOSTNAME = "HOSTNAME"
    EMAIL = "EMAIL"
    TEXT = "TEXT"
    TYPE_CHOICES = (
        (IP, IP),
        (HOSTNAME, HOSTNAME),
        (EMAIL, EMAIL),
        (TEXT, TEXT),
    )
    type = serializers.ChoiceField(choices=TYPE_CHOICES)

    def get_customer_variables(self, obj):
        customer_id = self.context.get("customer_id")
        if customer_id:
            if obj.customer_variable_values:
                return {
                    "variable_values": obj.customer_variable_values,
                    "option": obj.customer_variable_option,
                }
        else:
            return {}

    class Meta:
        model = RuleVariable
        exclude = ("provider", "customer")
        read_only_fields = ("variable_code", "is_global", "customer_variables")


class RuleVariableDestroySerializer(RuleVariableRetrieveUpdateSerializer):
    associated_rules = serializers.SerializerMethodField()

    def get_associated_rules(self, instance):
        queryset = Rule.objects.filter(
            detail__contains=instance.variable_code
        ).select_related("classification")
        result = []
        for instance in queryset:
            result.append(RuleRetrieveSerializer(instance).data)

        return result
