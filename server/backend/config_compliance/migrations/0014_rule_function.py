# Generated by Django 2.0.5 on 2020-10-08 05:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config_compliance', '0013_configcompliancescore_compliance_statistics'),
    ]

    operations = [
        migrations.AddField(
            model_name='rule',
            name='function',
            field=models.CharField(blank=True, choices=[('ROUTED_INTERFACE', 'ROUTED INTERFACE')], max_length=100, null=True),
        ),
    ]
