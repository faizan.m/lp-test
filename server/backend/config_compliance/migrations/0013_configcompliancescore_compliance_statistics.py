# Generated by Django 2.0.5 on 2020-10-05 04:19

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('config_compliance', '0012_rule_applies_to_manufacturers'),
    ]

    operations = [
        migrations.AddField(
            model_name='configcompliancescore',
            name='compliance_statistics',
            field=django.contrib.postgres.fields.jsonb.JSONField(default={}),
        ),
    ]
