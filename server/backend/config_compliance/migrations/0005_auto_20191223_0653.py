# Generated by Django 2.0.5 on 2019-12-23 06:53

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0076_auto_20191118_0714'),
        ('config_compliance', '0004_rule_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='RuleVariableCustomerMapping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('variable_values', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), size=None)),
                ('option', models.CharField(choices=[('AND', 'AND'), ('OR', 'OR')], max_length=100)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Customer')),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Provider')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='rulevariable',
            name='is_global',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='rulevariablecustomermapping',
            name='variable',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='customer_variables', to='config_compliance.RuleVariable'),
        ),
    ]
