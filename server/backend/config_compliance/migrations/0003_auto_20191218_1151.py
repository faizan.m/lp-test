# Generated by Django 2.0.5 on 2019-12-18 11:51

from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0076_auto_20191118_0714'),
        ('config_compliance', '0002_auto_20191218_0906'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rule',
            name='customers_assigned',
        ),
        migrations.AddField(
            model_name='rule',
            name='applies_to',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), blank=True, default=[], size=None),
        ),
        migrations.AddField(
            model_name='rule',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='rule',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='compliance_rules', to='accounts.Customer'),
        ),
        migrations.AddField(
            model_name='rule',
            name='customers_not_assigned',
            field=models.ManyToManyField(blank=True, to='accounts.Customer'),
        ),
        migrations.AddField(
            model_name='rule',
            name='is_enabled',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='rule',
            name='is_global',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='rule',
            name='name',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
