from django.apps import AppConfig


class ConfigComplianceConfig(AppConfig):
    name = "config_compliance"
