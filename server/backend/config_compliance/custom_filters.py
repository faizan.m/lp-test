# import django_filters
# from django_filters.rest_framework import FilterSet
from django_filters import rest_framework as filters

from common.custom_filters import ListFilter, BooleanListFilter
from config_compliance.models import Rule, RuleVariable


class ComplianceRuleFilterSet(filters.FilterSet):
    classification = ListFilter()
    level = ListFilter()
    applies_to = ListFilter()
    is_global = BooleanListFilter()
    is_enabled = BooleanListFilter()

    class Meta:
        model = Rule
        fields = ["level", "classification", "is_global", "is_enabled"]


class RuleVariableFilter(filters.FilterSet):
    type = ListFilter()
    is_global = BooleanListFilter()

    class Meta:
        model = RuleVariable
        fields = ["type", "is_global"]
