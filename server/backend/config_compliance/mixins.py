from django.db.models import (
    BooleanField,
    Case,
    IntegerField,
    OuterRef,
    Subquery,
    Value,
    When,
    Q,
)
from rest_framework.exceptions import NotFound

from config_compliance.models import (
    Rule,
    RuleVariable,
    RuleVariableCustomerMapping,
)
from core.utils import get_customer_id


class RuleQuerySetMixin(object):
    def get_queryset(self):
        q1 = Rule.objects.filter(provider=self.request.provider).annotate(
            level_int=Case(
                When(level="LOW", then=Value(0)),
                When(level="MEDIUM", then=Value(1)),
                When(level="HIGH", then=Value(2)),
                output_field=IntegerField(),
            )
        )

        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
        except NotFound:
            customer_id = None
        if customer_id:
            q1 = q1.filter(
                Q(provider=self.request.provider, customer_id=customer_id)
                | Q(is_global=True)
            ).annotate(
                is_assigned=Case(
                    When(customers_not_assigned__in=[customer_id], then=False),
                    default=True,
                    output_field=BooleanField(),
                )
            )
        else:
            q1 = q1.filter(is_global=True, customer_id=None)
        return q1.order_by("-updated_on").select_related(
            "classification", "created_by"
        )


class RuleVariablesQuerySetMixin(object):
    def get_queryset(self):
        """
         This mixin prepares queryset to list variables.
         In case of customer variables the query gets complex.
         The customer variables includes global variables, customer variables and global variables
         edited for the customer.
         For the edited global variables, the query should return the customer specific values.

         Due to complexity of the query, the default django filter doesn't works for the "option" field.
         This is explicitly handled in the below query. The final SQL output with option="AND" is given below:

         SELECT "config_compliance_rulevariable"."id",
        "config_compliance_rulevariable"."created_on",
        "config_compliance_rulevariable"."updated_on",
        "config_compliance_rulevariable"."name",
        "config_compliance_rulevariable"."description",
        "config_compliance_rulevariable"."variable_code",
        "config_compliance_rulevariable"."type",
        "config_compliance_rulevariable"."option",
        "config_compliance_rulevariable"."provider_id",
        "config_compliance_rulevariable"."customer_id",
        "config_compliance_rulevariable"."variable_values",
        "config_compliance_rulevariable"."is_global",
        (SELECT U0."variable_values"
         FROM "config_compliance_rulevariablecustomermapping" U0
         WHERE (U0."customer_id" = 1 AND U0."option" IN ('OR') AND
                U0."variable_id" = ("config_compliance_rulevariable"."id"))
         LIMIT 1)          AS "customer_variable_values",
        (SELECT U0."option"
         FROM "config_compliance_rulevariablecustomermapping" U0
         WHERE (U0."customer_id" = 1 AND U0."option" IN ('OR') AND
                U0."variable_id" = ("config_compliance_rulevariable"."id"))
         LIMIT 1)          AS "customer_variable_option",
        CASE
            WHEN (SELECT U0."option"
                  FROM "config_compliance_rulevariablecustomermapping" U0
                  WHERE (U0."customer_id" = 1 AND U0."option" IN ('OR') AND
                         U0."variable_id" = ("config_compliance_rulevariable"."id"))
                  LIMIT 1) IS NOT NULL THEN True
            ELSE False END AS "is_edited"
         FROM "config_compliance_rulevariable"
         WHERE ("config_compliance_rulevariable"."provider_id" = 1 AND
        (("config_compliance_rulevariable"."customer_id" = 1 AND "config_compliance_rulevariable"."provider_id" = 1) OR
         "config_compliance_rulevariable"."is_global" = True) AND ((CASE
                                                                        WHEN ((SELECT U0."option"
                                                                               FROM "config_compliance_rulevariablecustomermapping" U0
                                                                               WHERE (U0."customer_id" = 1 AND
                                                                                      U0."option" IN ('OR') AND
                                                                                      U0."variable_id" = ("config_compliance_rulevariable"."id"))
                                                                               LIMIT 1) IS NOT NULL) THEN True
                                                                        ELSE False END = False AND
                                                                    "config_compliance_rulevariable"."option" IN ('OR')) OR
                                                                   CASE
                                                                       WHEN ((SELECT U0."option"
                                                                              FROM "config_compliance_rulevariablecustomermapping" U0
                                                                              WHERE (U0."customer_id" = 1 AND
                                                                                     U0."option" IN ('OR') AND
                                                                                     U0."variable_id" = ("config_compliance_rulevariable"."id"))
                                                                              LIMIT 1) IS NOT NULL) THEN True
                                                                       ELSE False END = True))


         Returns: RuleVariable queryset
        """
        q1 = RuleVariable.objects.filter(provider=self.request.provider)
        options_filter = self.request.query_params.get("option")
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
        except NotFound:
            customer_id = None
        if customer_id:
            customer_id = get_customer_id(self.request, **self.kwargs)
            q1 = q1.filter(
                Q(provider=self.request.provider, customer_id=customer_id)
                | Q(is_global=True)
            )
            conditions = dict(variable=OuterRef("pk"), customer_id=customer_id)
            # django filter will not work for option filter applied on customer variable
            # this is handled explicitly below
            if options_filter:
                conditions.update(
                    dict(option__in=list(options_filter.split(",")))
                )
            customer_variables_qs = RuleVariableCustomerMapping.objects.filter(
                **conditions
            )

            q1 = q1.annotate(
                customer_variable_values=Subquery(
                    customer_variables_qs.values("variable_values")[:1]
                ),
                customer_variable_option=Subquery(
                    customer_variables_qs.values("option")[:1]
                ),
                is_edited=Case(
                    When(customer_variable_option__isnull=False, then=True),
                    default=Value(False),
                    output_field=BooleanField(),
                ),
            )
            if options_filter:
                options_condition = Q(
                    Q(
                        Q(is_edited=False)
                        & Q(option__in=list(options_filter.split(",")))
                    )
                    | Q(is_edited=True)
                )
            else:
                options_condition = Q()
            q1 = q1.filter(options_condition)
        else:
            q1 = q1.filter(is_global=True)
            if options_filter:
                q1 = q1.filter(option__in=list(options_filter.split(",")))
        return q1.order_by("-updated_on")
