import re
from collections import defaultdict
from typing import Any, Dict, List, Optional

import numpy as np
import pandas as pd
from celery import Task
from celery.utils.log import get_task_logger
from django.db import connection
from django.db.models import Q, QuerySet
from django.utils import timezone

from accounts.models import Customer, Provider
from caching.service import CacheService
from config_compliance.models import ConfigComplianceScore, Rule, RuleVariable
from config_compliance.utilities.ciscoconfparser.rule_validator import (
    RuleValidatorViaCiscoConfParse,
)
from core.models import DeviceInfo
from core.services import DeviceService
from core.utils import group_list_of_dictionary_by_key

logger = get_task_logger(__name__)


class DeviceMonitoringService(object):
    @classmethod
    def get_compliance_stats(
        cls, provider_id: int, customer_id: int, device_crm_ids: List[int]
    ) -> Dict[int, Dict[str, Any]]:
        """
        Look for the compliance stats in the ConfigComplianceScore table and return the existing data
        """
        queryset: QuerySet = ConfigComplianceScore.objects.filter(
            provider_id=provider_id,
            customer_id=customer_id,
            device_crm_id__in=device_crm_ids,
        ).values(
            "device_crm_id",
            "compliance_statistics",
            "valid_compliance",
            "invalid_compliance",
            "updated_on",
        )
        results: Dict[int, Any] = {
            rec["device_crm_id"]: rec for rec in queryset
        }
        return results

    @classmethod
    def get_bundled_device_compliance_stats(
        cls, device_crm_id_list, compliance_stats_devices_ids, compliance_stats
    ):
        """Returns latest compliance score for bundled devices."""
        compliance_stats_data = {}
        found_device_crm_id = 0

        ids_with_compliance_stats_calculated = list(
            set(compliance_stats_devices_ids) & set(device_crm_id_list)
        )

        ids_with_compliance_stats_calculated_len = len(
            ids_with_compliance_stats_calculated
        )

        if ids_with_compliance_stats_calculated_len > 0:
            found_device_crm_id = ids_with_compliance_stats_calculated[0]
            compliance_stats_data = compliance_stats[found_device_crm_id]

        if ids_with_compliance_stats_calculated_len > 1:
            # need to sort stats and get the latest one.
            temp_compliance_stats_list = [
                compliance_stats[id]
                for id in ids_with_compliance_stats_calculated
            ]
            temp_compliance_stats_list.sort(
                key=lambda item: item["updated_on"], reverse=True
            )
            compliance_stats_data = temp_compliance_stats_list[0]
            found_device_crm_id = compliance_stats_data["device_crm_id"]

        # Adding is_global to all the rules inside compliance statistics
        if compliance_stats_data.get("compliance_statistics"):
            _compliance_stats = []
            for compliance_stat in compliance_stats_data.get("compliance_statistics"):
                rule_id = compliance_stat.get("id")
                rule_queryset = Rule.objects.get(id=rule_id)
                compliance_stat["is_global"] = rule_queryset.is_global
                _compliance_stats.append(compliance_stat)
            compliance_stats_data["compliance_statistics"] = _compliance_stats

        return compliance_stats_data, found_device_crm_id

    @classmethod
    def get_device_violations(cls, device_ids, provider_id, customer_id):
        violations_count_by_devices = {}

        intial_data = {
            "violations_count": 0,
            "valid_count": 0,
            "levels": {"HIGH": 0, "MEDIUM": 0, "LOW": 0},
        }

        device_crm_ids_str = ",".join(str(x) for x in device_ids)

        try:
            with connection.cursor() as cursor:
                query = """
                SELECT
                    device_crm_id,
                    arr.item_object ->> 'is_valid_rule' as is_valid_rule,
                    arr.item_object ->> 'level' as level
                FROM config_compliance_score,
                    jsonb_array_elements(compliance_statistics) with ordinality arr(item_object, position)
                WHERE compliance_statistics != '{{}}'
                AND device_crm_id IN ({device_ids}) AND provider_id = {provider_id}
                AND customer_id = {customer_id}
                GROUP BY device_crm_id, arr.item_object;
                """.format(
                    device_ids=device_crm_ids_str,
                    provider_id=provider_id,
                    customer_id=customer_id,
                )

                cursor.execute(query)
                data = cursor.fetchall()

            for device_data in data:
                device_id, is_valid_rule, level = device_data

                if device_id in violations_count_by_devices:
                    if not is_valid_rule:
                        violations_count_by_devices[device_id][
                            "violations_count"
                        ] += 1
                        violations_count_by_devices[device_id]["levels"][
                            level
                        ] += 1
                    else:
                        violations_count_by_devices[device_id][
                            "valid_count"
                        ] += 1
                else:
                    if not is_valid_rule:
                        violations_count_by_devices[device_id] = intial_data
                        violations_count_by_devices[device_id][
                            "violations_count"
                        ] = 1
                        violations_count_by_devices[device_id]["levels"][
                            level
                        ] = 1
                    else:
                        violations_count_by_devices[device_id] = intial_data
                        violations_count_by_devices[device_id][
                            "valid_count"
                        ] = 1

        except Exception as e:
            logger.info(f"Excpetion fetching violation count {e}")

        return violations_count_by_devices

    @classmethod
    def search_device_configuration(
        cls,
        provider_id,
        customer_crm_id,
        search_query,
        device_ids,
        erp_client,
        category_id,
    ):
        device_crm_ids_str = ",".join(str(x) for x in device_ids)

        try:
            with connection.cursor() as cursor:
                query = """
                SELECT device_crm_id
                FROM core_deviceinfo
                WHERE provider_id = {provider_id}
                AND device_crm_id IN ({device_ids})
                AND configuration -> 'logicmonitor'->>'config' ilike '%{search_query}%';
                """.format(
                    provider_id=provider_id,
                    device_ids=device_crm_ids_str,
                    search_query=search_query,
                )

                cursor.execute(query)
                data = cursor.fetchall()
            return [device_data[0] for device_data in data]

        except Exception as e:
            logger.info(f"Excpetion {e}")
            return []

    @classmethod
    def get_device_monitoring_config_data(
        cls,
        device_id,
        provider_id,
        customer_crm_id,
        erp_client,
        category_id,
    ):
        result = {}

        provider = Provider.objects.get(id=provider_id)
        cw_device_data = DeviceService.get_device(
            erp_client, device_id, provider_id
        )

        cw_all_devices = DeviceService.get_device_list(
            provider_id,
            customer_crm_id,
            erp_client,
            category_id,
            True,
            apply_device_display_filters=False,
        )

        device_info = DeviceInfo.objects.filter(
            (
                Q(config_meta_data__logicmonitor__has_key="config_id")
                | Q(config_meta_data__solarwinds__has_key="config_id")
            )
            & Q(monitoring_data__is_managed=True)
            & Q(provider=provider)
            & Q(device_crm_id=device_id)
        )

        bundle_to_device_crm_id = defaultdict(list)

        if cw_all_devices and cw_device_data and device_info:
            for device in cw_all_devices:
                device[
                    "monitoring_data"
                ] = DeviceService.get_combined_monitoring_data(
                    device.get("monitoring_data", {})
                )

                device_name = device.get("device_name")
                host_name = device.get("monitoring_data", {}).get("host_name")
                key = (device_name, host_name)

                _device_id = device.get("id")

                if device_name and host_name:
                    bundle_to_device_crm_id[key].append(_device_id)

            device_name = cw_device_data.get("device_name")
            _device_id = cw_device_data.get("id")
            host_name = (
                cw_device_data.get("monitoring_data", {})
                .get("logicmonitor", {})
                .get("host_name")
            )
            key = (device_name, host_name)

            device_crm_id_list = bundle_to_device_crm_id.get(key, [])

            device_config_data_by_crm_id = {
                "config_data": device_info[0].merged_config,
                "config_id": device_info[0].config_id,
            }

            result.update(device_config_data_by_crm_id)

            customer_id = (
                Customer.objects.only("id")
                .get(crm_id=customer_crm_id, provider=provider)
                .id
            )

            compliance_stats = cls.get_compliance_stats(
                provider.id, customer_id, device_crm_id_list
            )

            compliance_stats_devices_crm_ids = compliance_stats.keys()

            (
                compliance_stats_data,
                compliant_device_crm_id,
            ) = cls.get_bundled_device_compliance_stats(
                device_crm_id_list,
                compliance_stats_devices_crm_ids,
                compliance_stats,
            )

            valid_compliance_score = compliance_stats_data.get(
                "valid_compliance", []
            )

            valid_compliance_score = (
                valid_compliance_score if bool(valid_compliance_score) else []
            )

            invalid_compliance_score = compliance_stats_data.get(
                "invalid_compliance", []
            )

            invalid_compliance_score = (
                invalid_compliance_score
                if bool(invalid_compliance_score)
                else []
            )

            compliance_statistics = compliance_stats_data.get(
                "compliance_statistics", []
            )

            compliance_statistics = (
                compliance_statistics if bool(compliance_statistics) else []
            )

            compliance_last_calculated = compliance_stats_data.get(
                "updated_on"
            )

            result.update(
                {
                    "valid_compliance_statistics": valid_compliance_score,
                    "invalid_compliance_statistics": invalid_compliance_score,
                    "compliance_last_calculated": compliance_last_calculated,
                    "compliance_statistics": compliance_statistics,
                }
            )

        return result

    @classmethod
    def get_bundled_device_violations_using_sql(
        cls, device_id, device_crm_ids, provider_id, customer_id
    ):
        violations_data = {
            "violations_count": 0,
            "valid_count": 0,
            "violations_count_by_levels": {"HIGH": 0, "MEDIUM": 0, "LOW": 0},
            "levels": {"HIGH": 0, "MEDIUM": 0, "LOW": 0},
            "compliance_last_calculated": None,
        }

        device_crm_ids_str = ",".join(str(x) for x in device_crm_ids)

        try:
            with connection.cursor() as cursor:
                query = """
                    SELECT
                        device_crm_id,
                        arr.item_object ->> 'is_valid_rule' as is_valid_rule,
                        arr.item_object ->> 'level' as level,
                        updated_on as compliance_last_calculated
                    FROM
                        config_compliance_score,
                        jsonb_array_elements(compliance_statistics) with ordinality arr(item_object, position)
                    WHERE
                        id = (
                            SELECT id
                            FROM
                                config_compliance_score,
                                jsonb_array_elements(compliance_statistics) with ordinality arr(item_object, position)
                            WHERE
                                compliance_statistics != '{{}}'
                            AND
                                provider_id = {provider_id}
                            AND
                                customer_id = {customer_id}
                            AND
                                device_crm_id IN ({device_ids})
                            GROUP BY
                                id, device_crm_id, arr.item_object, updated_on
                            ORDER BY updated_on DESC
                            LIMIT 1
                        );
                """.format(
                    device_ids=device_crm_ids_str,
                    provider_id=provider_id,
                    customer_id=customer_id,
                )

                cursor.execute(query)
                data = cursor.fetchall()

            for device_data in data:
                (
                    device_id,
                    is_valid_rule,
                    level,
                    compliance_last_calculated,
                ) = device_data

                violations_data["levels"][level] += 1
                violations_data[
                    "compliance_last_calculated"
                ] = compliance_last_calculated

                if is_valid_rule == "true":
                    violations_data["valid_count"] += 1
                else:
                    violations_data["violations_count_by_levels"][level] += 1
                    violations_data["violations_count"] += 1

        except Exception as e:
            logger.info(f"Excpetion fetching violation count {e}")

        return violations_data

    @classmethod
    def get_monitoring_config_list(
        cls,
        provider_id,
        customer_crm_id,
        erp_client,
        category_id,
        show_active_only=True,
    ):
        provider = Provider.objects.get(id=provider_id)
        cw_devices = DeviceService.get_device_list(
            provider_id,
            customer_crm_id,
            erp_client,
            category_id,
            show_active_only,
            apply_device_display_filters=False,
        )
        if cw_devices:
            devices = []
            model_numbers = []
            device_crm_ids = [device.get("id") for device in cw_devices]
            device_ids_with_config = DeviceInfo.objects.filter(
                (
                    Q(config_meta_data__logicmonitor__has_key="config_id")
                    | Q(config_meta_data__solarwinds__has_key="config_id")
                )
                & Q(monitoring_data__is_managed=True)
                & Q(provider=provider)
                & Q(device_crm_id__in=device_crm_ids)
            )

            device_config_data_by_crm_id = {
                device_info.device_crm_id: {
                    "config_last_updated_on": device_info.merged_config.get(
                        "config_last_updated_on"
                    ),
                    "config_id": device_info.config_id,
                    "config_last_synced_date": device_info.merged_config.get(
                        "config_last_synced_date"
                    ),
                    "os_version": device_info.monitoring_data.get(
                        "os_version", None
                    ),
                }
                for device_info in device_ids_with_config.iterator()
            }

            customer_id = Customer.objects.get(
                crm_id=customer_crm_id, provider=provider
            ).id

            for device in cw_devices:
                if device.get("id") in device_config_data_by_crm_id:
                    devices.append(device)
                    model_number = device.get("model_number")
                    if model_number is not None:
                        model_numbers.append(model_number)
            if device_crm_ids:
                device_types_data = DeviceService.get_device_types(
                    model_numbers, provider.id
                )
                device_stacks = defaultdict(list)
                result_devices = []
                bundle_to_device_crm_id = defaultdict(list)

                for device in devices:
                    device[
                        "monitoring_data"
                    ] = DeviceService.get_combined_monitoring_data(
                        device.get("monitoring_data", {})
                    )
                    device_name = device.get("device_name")
                    host_name = device.get("monitoring_data", {}).get(
                        "host_name"
                    )
                    key = (device_name, host_name)

                    _device_id = device.get("id")

                    if device_name and host_name:
                        bundle_to_device_crm_id[key].append(_device_id)

                        bundle_device_data = {
                            "serial_number": device.get("serial_number"),
                            "device_type": device.get("device_type"),
                            "model_number": device.get("model_number"),
                        }
                        if len(device_stacks[key]) > 0:
                            device_stacks[key].append(bundle_device_data)
                            continue
                        device_stacks[key].append(bundle_device_data)
                    else:
                        device["stack_info"] = {}

                    model_number = device.get("model_number")
                    device_type_data = device_types_data.get(model_number)
                    if device_type_data:
                        device.update(dict(device_type=device_type_data))
                    else:
                        device.update(dict(device_type_id="", device_type=""))

                    device.update(device_config_data_by_crm_id.get(_device_id))

                    result_devices.append(device)

                for device in result_devices:
                    device_name = device.get("device_name")
                    _device_id = device.get("id")
                    host_name = device.get("monitoring_data", {}).get(
                        "host_name"
                    )
                    key = (device_name, host_name)
                    bundled_device_crm_id_list = bundle_to_device_crm_id[key]

                    count = len(device_stacks[key])
                    if count > 1:

                        device["stack_info"] = {
                            "count": count,
                            "bundled_devices_data": device_stacks[key],
                        }
                        for bundle_device in device_stacks[key]:
                            if bundle_device.get(
                                "device_type"
                            ) and bundle_device.get("model_number"):
                                device["model_number"] = bundle_device.get(
                                    "model_number"
                                )
                                device["device_type"] = bundle_device.get(
                                    "device_type"
                                )
                                break
                    else:
                        device["stack_info"] = {}

                    device_violations_count = (
                        cls.get_bundled_device_violations_using_sql(
                            _device_id,
                            bundled_device_crm_id_list,
                            provider_id,
                            customer_id,
                        )
                    )
                    device.update(device_violations_count)

                return result_devices
        return None

    @classmethod
    def get_monitoring_config_data(
        cls,
        provider_id,
        customer_crm_id,
        erp_client,
        category_id,
        show_active_only=True,
    ):
        provider = Provider.objects.get(id=provider_id)
        cw_devices = DeviceService.get_device_list(
            provider_id,
            customer_crm_id,
            erp_client,
            category_id,
            True,
            apply_device_display_filters=False,
        )

        customer_id = (
            Customer.objects.only("id")
            .get(crm_id=customer_crm_id, provider=provider)
            .id
        )

        if not cw_devices:
            return []

        device_crm_ids = [device.get("id") for device in cw_devices]
        devices_with_config = DeviceInfo.objects.filter(
            (
                Q(config_meta_data__logicmonitor__has_key="config_id")
                | Q(config_meta_data__solarwinds__has_key="config_id")
            )
            & Q(monitoring_data__is_managed=True)
            & Q(provider=provider)
            & Q(device_crm_id__in=device_crm_ids)
        )

        device_stacks = defaultdict(list)
        bundle_to_device_crm_id = defaultdict(list)

        device_config_data_by_crm_id = {
            device_info.device_crm_id: {
                "config_data": device_info.merged_config,
                "config_id": device_info.config_id,
                "os_version": device_info.monitoring_data.get(
                    "os_version", None
                ),
            }
            for device_info in devices_with_config.iterator()
        }

        cw_devices_with_config = []
        for device in cw_devices:
            _device_id = device.get("id")

            if _device_id not in device_config_data_by_crm_id:
                continue

            device[
                "monitoring_data"
            ] = DeviceService.get_combined_monitoring_data(
                device.get("monitoring_data", {})
            )

            device_name = device.get("device_name")
            host_name = device.get("monitoring_data", {}).get("host_name")
            key = (device_name, host_name)

            if device_name and host_name:
                bundle_to_device_crm_id[key].append(_device_id)
                bundle_device_data = {
                    "serial_number": device.get("serial_number"),
                    "device_type": device.get("device_type"),
                    "model_number": device.get("model_number"),
                }
                if len(device_stacks[key]) > 0:
                    device_stacks[key].append(bundle_device_data)
                    continue

                device_stacks[key].append(bundle_device_data)

            cw_devices_with_config.append(device)

        compliance_stats = DeviceMonitoringService.get_compliance_stats(
            provider_id, customer_id, device_config_data_by_crm_id.keys()
        )

        compliance_stats_devices_crm_ids = compliance_stats.keys()

        devices_data = []
        for device in cw_devices_with_config:
            device_id = device.get("id")
            device_name = device.get("device_name")
            host_name = device.get("monitoring_data", {}).get("host_name")
            key = (device_name, host_name)
            bundle_device_crm_ids = bundle_to_device_crm_id[key]

            (
                compliance_stats_data,
                compliant_device_crm_id,
            ) = DeviceMonitoringService.get_bundled_device_compliance_stats(
                bundle_device_crm_ids,
                compliance_stats_devices_crm_ids,
                compliance_stats,
            )

            if not compliance_stats_data:
                continue

            _device_info_meta_data = device_config_data_by_crm_id.get(
                compliant_device_crm_id
            )

            device["configuration_data"] = _device_info_meta_data

            data = {**device, **compliance_stats_data}
            devices_data.append(data)

        return devices_data


class ComplianceService:
    def __init__(self, provider, customer_crm_id, device_types_data):
        self.provider = provider
        self.customer_crm_id = customer_crm_id
        self.device_types_data = device_types_data

    @classmethod
    def refresh_compliance_stats_data(cls, provider_id, **kwargs) -> Task:
        """
        Trigger task to re calculate compliance stats
        """
        from config_compliance.tasks.compliance import (
            store_config_compliance_score,
        )
        from config_compliance.tasks import rerun_compliance_for_customer

        customer_id = kwargs.get("customer_id")
        if customer_id:
            device_ids = kwargs.get("device_ids", [])
            rule_ids = kwargs.get("rule_ids", [])

            task: Task = rerun_compliance_for_customer.apply_async(
                (provider_id, customer_id, rule_ids, device_ids), queue="high"
            )
        else:
            rule_ids = kwargs.get("rule_ids", [])
            task: Task = store_config_compliance_score.apply_async(
                (provider_id, rule_ids)
            )
        return task

    def calculate_compliance_score_for_devices(
        self, device_config_data, devices, rule_ids: Optional[List] = None
    ):
        if device_config_data:
            overall_rule_stats_data = defaultdict(list)
            customer = self.get_customer()
            if not customer:
                return overall_rule_stats_data

            rules = self.get_applicable_rules(customer, rule_ids)
            variable_values = self.get_variable_values(customer)
            devices_by_id = group_list_of_dictionary_by_key(devices, "id")
            for device_id, device_info_meta_data in device_config_data.items():
                config_id = device_info_meta_data.get("config_id")
                config = device_info_meta_data.get("config_data").get("config")
                device_os_version = device_info_meta_data.get(
                    "os_version", None
                )

                cache_key = CacheService.CACHE_KEYS.DEVICE_CONFIG_ID.format(
                    config_id
                )
                if CacheService.exists(cache_key):
                    formatted_data = CacheService.get_data(cache_key)
                else:
                    formatted_data = self.update_cache(cache_key, config)
                model_number, device_manufacturer_name, device_type = (
                    None,
                    None,
                    None,
                )
                if devices_by_id.get(device_id):
                    model_number = devices_by_id.get(device_id)[0].get(
                        "model_number", None
                    )
                    device_manufacturer_name = devices_by_id.get(device_id)[
                        0
                    ].get("manufacturer_name")
                if model_number:
                    device_type = self.device_types_data.get(model_number)
                for rule in rules:
                    compliance_score_for_device = self.apply_rule(
                        rule,
                        device_id,
                        formatted_data,
                        variable_values,
                        config,
                        device_type,
                        device_manufacturer_name,
                        device_os_version,
                    )
                    if compliance_score_for_device:
                        overall_rule_stats_data[device_id].append(
                            compliance_score_for_device
                        )

            return overall_rule_stats_data

    def get_customer(self) -> Optional[Customer]:
        """
        return Customer record using the customer crm id
        if no object is found, none is returned
        """
        customer = None
        try:
            customer = Customer.objects.only("id").get(
                provider_id=self.provider.id, crm_id=self.customer_crm_id
            )
        except Customer.DoesNotExist as e:
            logger.error(
                f"No Customer found {self.provider} and {self.customer_crm_id}. "
                f"Can not update compliance stats for the customer."
            )
        return customer

    def apply_rule(
        self,
        rule,
        device_id,
        formatted_data,
        variable_values,
        config,
        device_type,
        device_manufacturer_name,
        device_ios_version,
    ):
        # check if device need to ignore
        if device_id in rule.to_ignore_devices:
            return

        compliance_score_for_device = {
            "id": rule.id,
            "classification": rule.classification.id,
            "classification_name": rule.classification.name,
            "level": rule.level,
            "detail": rule.detail,
            "description": rule.description,
            "function": rule.function,
            "name": rule.name,
            "applies_to": rule.applies_to,
            "applies_to_manufacturers": rule.applies_to_manufacturers,
            "applies_to_ios_version": rule.applies_to_ios_version,
            "is_valid_rule": False,
            "updated_on": timezone.now().isoformat(),
        }

        if not bool(formatted_data):
            return compliance_score_for_device

        elif rule.applies_to:
            if (
                device_type not in rule.applies_to
                and device_id not in rule.to_include_devices
            ):
                return
            else:
                if (
                    rule.applies_to_manufacturers
                    and device_manufacturer_name
                    not in rule.applies_to_manufacturers
                ):
                    return

        elif device_ios_version and rule.applies_to_ios_version:
            if not self.is_os_version_valid(
                device_ios_version, rule.applies_to_ios_version
            ):
                return

        rule_stats_result = RuleValidatorViaCiscoConfParse(
            config, rule, variable_values
        )._verify_rule_for_config_using_ccp()

        compliance_score_for_device.update(rule_stats_result)

        return compliance_score_for_device

    def is_os_version_valid(
        self, device_ios_version, applies_to_ios_version_list
    ):
        applies_to_os_version = True
        # TODO: Logic for validation of version numbers with brackets and release

        return applies_to_os_version

    def update_cache(self, cache_key, config):
        parsed_data = self._parse_config_raw_text(config)
        formatted_data = self._format_config_data(parsed_data)
        CacheService.set_data(cache_key, formatted_data)
        return formatted_data

    def get_variable_values(
        self, customer: Customer
    ) -> Dict[str, Dict[str, str]]:
        """
        Gives a dictionary containing variable code as key and dict as a value.

        """
        rule_variables = RuleVariable.objects.filter(
            provider_id=self.provider.id
        ).prefetch_related("customer_variables")
        variable_values = {}
        for value in rule_variables:
            _variable_values = {
                "id": value.id,
                "variable_code": value.variable_code,
                "type": value.type,
                "variable_values": value.variable_values,
                "option": value.option,
                "is_global": value.is_global,
                "name": value.name,
                "description": value.description,
            }
            _customer_variables = value.customer_variables.filter(
                customer_id=customer.id
            ).first()
            if _customer_variables:
                _variable_values.update(
                    {
                        "customer_variables_variable_values": _customer_variables.variable_values,
                        "customer_variables_option": _customer_variables.option,
                    }
                )
            variable_values[value.variable_code] = _variable_values
        return variable_values

    def get_applicable_rules(
        self, customer: Customer, filtered_rule_ids: Optional[List[int]] = None
    ) -> "QuerySet[Rule]":
        """
        Gives a queryset of Rule which are applicable to the customer.
        Also takes a list of rule ids in `filtered_rule_ids`. If `filtered_rule_ids`
        is given only the rules with these ids will be returned.
        """
        query_params = dict(
            provider=self.provider, is_global=True, is_enabled=True
        )
        global_rules = (
            Rule.objects.exclude(customers_not_assigned__in=[customer.id])
            .filter(**query_params)
            .select_related()
        )
        customer_rules = customer.compliance_rules.filter(
            is_enabled=True
        ).select_related()
        if filtered_rule_ids:
            global_rules = global_rules.filter(id__in=filtered_rule_ids)
            customer_rules = customer_rules.filter(id__in=filtered_rule_ids)
        rules = global_rules.union(customer_rules)
        return rules

    @staticmethod
    def _verify_rule_for_config(rule, config, variable_values, raw_config):
        result = {}
        regex_validator = RuleVariable.regex_validator

        RuleValidatorViaCiscoConfParse()._verify_rule_for_config_using_ccp(
            rule, raw_config, variable_values
        )

        if config.get(rule.detail):
            return dict(
                valid_compliance={
                    "": [{"value": rule.detail, "status": "valid"}]
                },
                has_missing_section_lines=False,
                missing_variable_values=None,
            )
        elif "\n" in rule.detail:
            parsed_data = ComplianceService._parse_config_raw_text(rule.detail)
            formatted_data = ComplianceService._format_config_data(parsed_data)
            if not bool(formatted_data):
                _rule_detail = rule.detail.split("\n")
                rule_detail = {
                    "type": "Command",
                    "value": _rule_detail[0],
                    "sub_steps": [step.strip() for step in _rule_detail[1:]],
                }
                (
                    valid_compliance,
                    has_missing_section_lines,
                    missing_variable_values,
                ) = ComplianceService._validate_each_rule_line_for_variables(
                    rule_detail, config, variable_values
                )
                if valid_compliance:
                    result = dict(
                        valid_compliance=valid_compliance,
                        has_missing_section_lines=has_missing_section_lines,
                        missing_variable_values=missing_variable_values,
                    )
                    return result
            elif bool(formatted_data):
                res_valid_compliance = defaultdict(list)
                res_missing_variable_values = defaultdict(list)
                has_missing_section_lines = None
                for key, value in formatted_data.items():
                    if value.get("sub_steps"):
                        rule_detail = {
                            "type": "Command",
                            "value": value.get("value"),
                            "sub_steps": list(value.get("sub_steps").keys()),
                        }
                    else:
                        rule_detail = {
                            "type": "Command",
                            "value": value.get("value"),
                        }
                    (
                        valid_compliance,
                        missing_section_lines,
                        missing_variable_values,
                    ) = ComplianceService._validate_each_rule_line_for_variables(
                        rule_detail, config, variable_values
                    )
                    if valid_compliance:
                        for (
                            valid_compliance_key,
                            valid_compliance_lines,
                        ) in valid_compliance.items():
                            res_valid_compliance[valid_compliance_key].extend(
                                valid_compliance_lines
                            )
                    else:
                        has_missing_section_lines = True
                        res_valid_compliance[""].extend(
                            [
                                {
                                    "value": value.get("value"),
                                    "status": "not_valid",
                                }
                            ]
                        )
                    if missing_variable_values:
                        for (
                            _validator,
                            _validator_value,
                        ) in missing_variable_values.items():
                            res_missing_variable_values[_validator].extend(
                                _validator_value
                            )
                    if missing_section_lines:
                        has_missing_section_lines = True
                result = dict(
                    valid_compliance=res_valid_compliance,
                    has_missing_section_lines=has_missing_section_lines,
                    missing_variable_values=res_missing_variable_values,
                )
                return result
        elif re.findall(regex_validator, rule.detail):
            rule_detail = {"type": "Command", "value": rule.detail}
            (
                valid_compliance,
                has_missing_section_lines,
                missing_variable_values,
            ) = ComplianceService._validate_each_rule_line_for_variables(
                rule_detail, config, variable_values
            )
            result = dict(
                valid_compliance=valid_compliance,
                has_missing_section_lines=has_missing_section_lines,
                missing_variable_values=missing_variable_values,
            )
        return result

    @staticmethod
    def _validate_each_rule_line_for_variables(
        rule_detail, config, variable_values
    ):
        rows = list(config.keys())
        rows = [_.split(" ") for _ in rows]
        df = pd.DataFrame.from_records(rows)
        df.fillna("", inplace=True)
        has_missing_section_lines = None
        result_matching_lines = defaultdict(list)
        if rule_detail.get("sub_steps"):
            section_main_line = rule_detail["value"].split(" ")
            (
                matching_lines,
                missing_variable_values,
            ) = ComplianceService._verify_rule_for_validators(
                df, section_main_line, variable_values
            )
            if matching_lines:
                for matching_line in matching_lines:
                    _config = config.get(matching_line)["sub_steps"]
                    if _config:
                        rows = list(_config.keys())
                        rows = [_.split(" ") for _ in rows]
                        for sub_step in rule_detail.get("sub_steps"):
                            df = pd.DataFrame.from_records(rows)
                            df.fillna("", inplace=True)
                            (
                                _sub_steps_matching_lines,
                                _missing_variable_values,
                            ) = ComplianceService._verify_rule_for_validators(
                                df, sub_step.split(" "), variable_values
                            )
                            if _sub_steps_matching_lines:
                                result_matching_lines[matching_line].extend(
                                    [
                                        {
                                            "value": _sub_steps_matching_line,
                                            "status": "valid",
                                        }
                                        for _sub_steps_matching_line in _sub_steps_matching_lines
                                    ]
                                )
                            else:
                                # Sub section lines which are not present in the config.
                                has_missing_section_lines = True
                                result_matching_lines[matching_line].extend(
                                    [
                                        {
                                            "value": sub_step,
                                            "status": "not_valid",
                                        }
                                    ]
                                )
                            if _missing_variable_values:
                                missing_variable_values.update(
                                    _missing_variable_values
                                )
                    return (
                        result_matching_lines,
                        has_missing_section_lines,
                        missing_variable_values,
                    )
                else:
                    return (
                        None,
                        has_missing_section_lines,
                        missing_variable_values,
                    )
            else:
                return (
                    None,
                    has_missing_section_lines,
                    missing_variable_values,
                )
        else:
            rule_detail = rule_detail["value"].split(" ")
            (
                valid_compliance,
                missing_variable_values,
            ) = ComplianceService._verify_rule_for_validators(
                df, rule_detail, variable_values
            )
            if valid_compliance:
                result_matching_lines[""].extend(
                    [
                        {"value": valid_compliance_line, "status": "valid"}
                        for valid_compliance_line in valid_compliance
                    ]
                )
            return (result_matching_lines, None, missing_variable_values)

    @staticmethod
    def _verify_rule_for_validators(df, rule_detail, variable_values):
        df.replace("", np.nan, inplace=True)
        match_indexes = set(df.iloc[:, : len(rule_detail)].dropna().index)
        extra_columns_indexes = set(
            df.iloc[:, len(rule_detail) :].dropna(how="all").index
        )
        match_indexes = list(match_indexes.difference(extra_columns_indexes))
        if not match_indexes:
            logger.info("No matches found based on the length of th rule")
            return None, None
        df = df[df.index.isin(match_indexes)]

        df.fillna("", inplace=True)
        invalid_variable_values = defaultdict(list)
        result = []
        try:
            for index in range(len(rule_detail)):
                if index not in df.columns:
                    continue
                word = rule_detail[index]
                validators = set(
                    re.findall(RuleVariable.regex_validator, word)
                )
                if validators:
                    for validator in validators:
                        place_holder_values = []
                        if validator in variable_values:
                            variable_value = variable_values.get(validator)
                            values = (
                                variable_value.get(
                                    "customer_variables_variable_values"
                                )
                                if variable_value.get(
                                    "customer_variables_variable_values"
                                )
                                else variable_value.get("variable_values")
                            )
                            for value in values:
                                place_holder_values.append(
                                    word.replace(validator, value)
                                )
                            option = (
                                variable_value.get("customer_variables_option")
                                if variable_value.get(
                                    "customer_variables_option"
                                )
                                else variable_value.get("option")
                            )
                            if option and option.lower() == "and":
                                indexes = []
                                for (
                                    each_place_holder_value
                                ) in place_holder_values:
                                    place_holder_and_matching = list(
                                        df[
                                            df[index]
                                            == each_place_holder_value
                                        ].index
                                    )
                                    if place_holder_and_matching:
                                        indexes.extend(
                                            place_holder_and_matching
                                        )
                                    else:
                                        # 1
                                        # ha@{{version}}
                                        str_match = word.replace(validator, "")
                                        for each_variable in list(
                                            df[index].values
                                        ):
                                            if str_match in each_variable:
                                                _invalid_value = (
                                                    each_variable.replace(
                                                        str_match, ""
                                                    )
                                                )
                                                if (
                                                    _invalid_value
                                                    not in place_holder_values
                                                ):
                                                    invalid_variable_values[
                                                        validator
                                                    ].append(_invalid_value)
                                        continue
                                df = df[df.index.isin(indexes)]
                            elif option and option.lower() == "or":
                                if not df[
                                    df[index].isin(place_holder_values)
                                ].empty:
                                    df = df[
                                        df[index].isin(place_holder_values)
                                    ]
                                else:
                                    mask = np.logical_not(
                                        df[index].isin(place_holder_values)
                                    )
                                    word_split = word.split(validator)
                                    if len(word_split) > 2:
                                        invalid_variable_values[
                                            validator
                                        ].extend(
                                            list(set(df[mask][index].values))
                                        )
                                    else:
                                        for each_variable in list(
                                            set(df[mask][index].values)
                                        ):
                                            try:
                                                l_index = each_variable.find(
                                                    word_split[0],
                                                    0,
                                                    len(each_variable),
                                                )
                                                if l_index == -1:
                                                    continue
                                                r_index = each_variable.rindex(
                                                    word_split[1],
                                                    0,
                                                    len(each_variable),
                                                )
                                                _invalid_value = each_variable[
                                                    l_index
                                                    + len(
                                                        word_split[0]
                                                    ) : r_index
                                                ]
                                            except ValueError as e:
                                                continue
                                            if (
                                                _invalid_value
                                                not in place_holder_values
                                            ):
                                                invalid_variable_values[
                                                    validator
                                                ].append(_invalid_value)
                                    df = df[
                                        df[index].isin(place_holder_values)
                                    ]
                else:
                    if df.empty:
                        return result, invalid_variable_values
                    df = df[df[index] == word]
        except KeyError as e:
            pass

        df["compliance_lines"] = df.apply(" ".join, axis=1)

        result = [_.strip() for _ in df.compliance_lines.tolist()]
        return result, invalid_variable_values

    @staticmethod
    def _parse_config_raw_text(raw_text):
        """
        This function read the raw config text and transform it to a multi level list format.
        :param raw_text:
        :return:
        """
        raw_text = raw_text.strip()
        raw_text = raw_text.split("\n")
        result = []
        _result = []
        is_open = False
        for each in raw_text:
            if re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", each) and not is_open:
                is_open = True
                continue
            if re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", each) and is_open:
                is_open = False
                if _result:
                    result.append(_result)
                    _result = []
                continue
            if each:
                is_open = True
                if each.startswith(" !"):
                    continue
                else:
                    _result.append(each)
        if _result:
            result.append(_result)
        return result

    @staticmethod
    def _format_config_data(config):
        """
        This function
        :param config:
        :return:
        """
        parsed_data = {}
        for _ in config:
            _parsed_data = {}
            command = ""
            for each in _:
                if each:
                    try:
                        if not each.startswith(" "):
                            each = str(each).strip()
                            if _parsed_data.get(each):
                                count = _parsed_data.get(each).get("count") + 1
                                _parsed_data[each]["count"] = count
                            else:
                                command = each
                                _parsed_data[each] = {
                                    "type": "Command",
                                    "value": each,
                                    "count": 0,
                                    "sub_steps": {},
                                }
                        else:
                            each = each.strip()
                            _parsed_data[command]["sub_steps"][each] = {
                                "type": "Step",
                                "value": each,
                            }
                    except Exception as e:
                        continue
            parsed_data.update(_parsed_data)
        return parsed_data


class RulesService:
    @classmethod
    def prepare_dashboard_data(
        cls,
        provider_id,
        customer_crm_id,
        erp_client,
        category_id,
        show_active_only,
    ):
        """
        Process and prepare rules data across devices.
        """
        response = {
            "rules_violations_by_priority_obj": {
                "MEDIUM": 0,
                "HIGH": 0,
                "LOW": 0,
            },
            "top_rules_ids": [],
            "top_devices_ids": [],
            "rules": {},
            "devices": {},
        }

        main_rule_ids = []
        main_rule_data_obj = {}

        device_ids_list = []
        device_ids_obj = {}

        top_rules_list = []
        top_devices_list = []

        rules_violations_by_priority_obj = {"MEDIUM": 0, "HIGH": 0, "LOW": 0}

        devices_data = DeviceMonitoringService.get_monitoring_config_data(
            provider_id,
            customer_crm_id,
            erp_client,
            category_id,
            show_active_only,
        )

        for index in range(0, len(devices_data)):
            dev = devices_data[index]

            device_rules_violations_by_priority_obj = {
                "MEDIUM": 0,
                "HIGH": 0,
                "LOW": 0,
            }

            # device_id = str(dev.get("id", None))
            device_id = dev.get("id", None)

            comp_stats = dev.get("compliance_statistics", [])

            total_rules = len(comp_stats)

            dev["total_rules"] = total_rules

            if device_id not in device_ids_list:
                device_ids_list.append(device_id)
                device_ids_obj[device_id] = dev

            device_violations_count = 0

            for stat in comp_stats:
                # stat_id = str(stat["id"])
                stat_id = stat["id"]

                if stat_id not in main_rule_ids:
                    main_rule_ids.append(stat_id)
                    main_rule_data_obj[stat_id] = stat
                    main_rule_data_obj[stat_id]["deivces_list"] = [device_id]
                    main_rule_data_obj[stat_id]["violations_count"] = 0
                    if not stat["is_valid_rule"]:
                        main_rule_data_obj[stat_id]["violations_count"] = 1
                        rules_violations_by_priority_obj[stat["level"]] += 1
                        device_rules_violations_by_priority_obj[
                            stat["level"]
                        ] += 1
                        device_violations_count += 1
                else:
                    if not stat["is_valid_rule"]:
                        main_rule_data_obj[stat_id]["violations_count"] += 1
                        rules_violations_by_priority_obj[stat["level"]] += 1
                        device_rules_violations_by_priority_obj[
                            stat["level"]
                        ] += 1
                        device_violations_count += 1

                    if (
                        device_id
                        not in main_rule_data_obj[stat_id]["deivces_list"]
                    ):
                        main_rule_data_obj[stat_id]["deivces_list"].append(
                            device_id
                        )

            dev[
                "rules_violations_by_priority"
            ] = device_rules_violations_by_priority_obj
            dev["violations_count"] = device_violations_count

        top_rules_list = sorted(
            main_rule_data_obj,
            key=lambda x: main_rule_data_obj[x]["violations_count"],
            reverse=True,
        )
        # top_devices_list = sorted(device_ids_obj, key=lambda x:device_ids_obj[x]["total_rules"], reverse=True)
        top_devices_list = sorted(
            device_ids_obj,
            key=lambda x: device_ids_obj[x]["violations_count"],
            reverse=True,
        )

        response[
            "rules_violations_by_priority_obj"
        ] = rules_violations_by_priority_obj
        response["top_rules_ids"] = top_rules_list[0:5]
        response["top_devices_ids"] = top_devices_list[0:5]
        response["rules"] = main_rule_data_obj

        response["devices"] = device_ids_obj

        return response
