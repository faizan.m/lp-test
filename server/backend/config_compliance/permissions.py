from rest_framework.permissions import BasePermission


class IsConfigComplianceReadAllowed(BasePermission):
    def has_permission(self, request, view):
        return request.customer.config_compliance_service_allowed


class IsConfigComplianceWriteAllowed(BasePermission):
    def has_permission(self, request, view):
        return request.customer.config_compliance_service_write_allowed
