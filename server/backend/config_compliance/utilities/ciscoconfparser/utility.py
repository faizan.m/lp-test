# Queries:
def _parse_config_raw_text(raw_text):
    """
    This function read the raw config text and transform it to a multi level list format.
    :param raw_text:
    :return:
    """
    raw_text = raw_text.strip()
    raw_text = raw_text.split("\n")
    result = []
    _result = []
    is_open = False
    for each in raw_text:
        if re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", each) and not is_open:
            is_open = True
            continue
        if re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", each) and is_open:
            is_open = False
            if _result:
                result.append(_result)
                _result = []
            continue
        if each:
            is_open = True
            if each.startswith(" !"):
                continue
            else:
                _result.append(each)
    if _result:
        result.append(_result)
    return result


def calculate_compliance_score_for_devices(
    cls,
    provider_id,
    customer_crm_id,
    device_config_data_by_id,
    devices,
    device_types_data,
):
    devices_by_id = group_list_of_dictionary_by_key(devices, "id")
    if device_config_data_by_id:
        valid_compliance_score_for_devices = defaultdict(list)
        invalid_compliance_score_for_devices = defaultdict(list)
        customer = Customer.objects.get(
            provider_id=provider_id, crm_id=customer_crm_id
        )
        rules = (
            Rule.objects.exclude(customers_not_assigned__in=[customer.id])
            .filter(is_global=True, is_enabled=True)
            .select_related()
        )
        rules = rules.union(
            customer.compliance_rules.filter(is_enabled=True).select_related()
        )
        rule_variables = RuleVariable.objects.filter(
            provider_id=provider_id
        ).prefetch_related("customer_variables")
        variable_values = {}
        for value in rule_variables:
            _variable_values = {
                "variable_code": value.variable_code,
                "type": value.type,
                "variable_values": value.variable_values,
                "option": value.option,
            }
            _customer_variables = value.customer_variables.filter(
                customer_id=customer.id
            ).first()
            if _customer_variables:
                _variable_values.update(
                    {
                        "customer_variables_variable_values": _customer_variables.variable_values,
                        "customer_variables_option": _customer_variables.option,
                    }
                )
            variable_values[value.variable_code] = _variable_values

                    ####### Above code can be resused #######
##############################################################################################
        for device_id, device_info_meta_data in device_config_data_by_id.items():
            config_id = device_info_meta_data.get("config_id")
            cache_key = CacheService.CACHE_KEYS.DEVICE_CONFIG_ID.format(config_id)
            if CacheService.exists(cache_key):
                formatted_data = CacheService.get_data(cache_key)
            else:
                config = device_info_meta_data.get("config_data").get("config")
                parsed_data = cls._parse_config_raw_text(config)
                formatted_data = cls._format_config_data(parsed_data)
                CacheService.set_data(cache_key, formatted_data)
            model_number = devices_by_id[device_id][0].get("model_number", None)
            device_type = device_types_data.get(model_number)

                    #######   #######
##############################################################################################
            for rule in rules:
                compliance_score_for_device = {
                    "classification": rule.classification.id,
                    "classification_name": rule.classification.name,
                    "level": rule.level,
                    "detail": rule.detail,
                    "description": rule.description,
                    "name": rule.name,
                }
                if not bool(formatted_data):
                    invalid_compliance_score_for_devices[device_id].append(
                        compliance_score_for_device
                    )
                elif (e.applies_to
                ):
                    device_type
                    and rule.applies_to
                    and device_type in rul
                    if rule.applies_to_manufacturers:
                        device_manufacturer_name = devices_by_id.get(device_id)[
                            0
                        ].get("manufacturer_name")
                        if (
                            device_manufacturer_name
                            not in rule.applies_to_manufacturers
                        ):
                            continue

            ####### MAIN LOGIC WRITTEN HERE FOR VALIDATIION OF RULE #######
##############################################################################################
                    result = cls._verify_rule_for_config(
                        rule, formatted_data, variable_values
                    )
                    if (
                        result.get("has_missing_section_lines")
                        or result.get("missing_variable_values")
                        or not result
                    ):
                        result.pop("has_missing_section_lines", {})
                        compliance_score_for_device.update(result)
                        invalid_compliance_score_for_devices[device_id].append(
                            compliance_score_for_device
                        )
                    else:
                        compliance_score_for_device["valid_compliance"] = result[
                            "valid_compliance"
                        ]
                        valid_compliance_score_for_devices[device_id].append(
                            compliance_score_for_device
                        )
        return (
            valid_compliance_score_for_devices,
            invalid_compliance_score_for_devices,
        )


@staticmethod
def _verify_rule_for_config(rule, config, variable_values):
        ####### MAIN LOGIC WRITTEN HERE FOR VALIDATIION OF RULE #######
##############################################################################################
    result = {}
    regex_validator = RuleVariable.regex_validator
    if config.get(rule.detail):
        return dict(
            valid_compliance={"": [{"value": rule.detail, "status": "valid"}]},
            has_missing_section_lines=False,
            missing_variable_values=None,
        )
    elif "\n" in rule.detail:
        parsed_data = ComplianceService._parse_config_raw_text(rule.detail)
        formatted_data = ComplianceService._format_config_data(parsed_data)
        if not bool(formatted_data): # failed to format the config file syntax.
            _rule_detail = rule.detail.split("\n")
            rule_detail = {
                "type": "Command",
                "value": _rule_detail[0],
                "sub_steps": [step.strip() for step in _rule_detail[1:]],
            }
            valid_compliance, has_missing_section_lines, missing_variable_values = ComplianceService._validate_each_rule_line_for_variables(
                rule_detail, config, variable_values
            )
            if valid_compliance:
                result = dict(
                    valid_compliance=valid_compliance,
                    has_missing_section_lines=has_missing_section_lines,
                    
                    missing_variable_values=missing_variable_values,
                )
                return result
        elif bool(formatted_data):
            res_valid_compliance = defaultdict(list)
            res_missing_variable_values = defaultdict(list)
            has_missing_section_lines = None
            for key, value in formatted_data.items():
                if value.get("sub_steps"):
                    rule_detail = {
                        "type": "Command",
                        "value": value.get("value"),
                        "sub_steps": list(value.get("sub_steps").keys()),
                    }
                else:
                    rule_detail = {"type": "Command", "value": value.get("value")}
                valid_compliance, missing_section_lines, missing_variable_values = ComplianceService._validate_each_rule_line_for_variables(
                    rule_detail, config, variable_values
                )
                if valid_compliance:
                    for (
                        valid_compliance_key,
                        valid_compliance_lines,
                    ) in valid_compliance.items():
                        res_valid_compliance[valid_compliance_key].extend(
                            valid_compliance_lines
                        )
                else:
                    has_missing_section_lines = True
                    res_valid_compliance[""].extend(
                        [{"value": value.get("value"), "status": "not_valid"}]
                    )
                if missing_variable_values:
                    for (
                        _validator,
                        _validator_value,
                    ) in missing_variable_values.items():
                        res_missing_variable_values[_validator].extend(
                            _validator_value
                        )
                if missing_section_lines:
                    has_missing_section_lines = True
            result = dict(
                valid_compliance=res_valid_compliance,
                has_missing_section_lines=has_missing_section_lines,
                missing_variable_values=res_missing_variable_values,
            )
            return result
    elif re.findall(regex_validator, rule.detail):
        rule_detail = {"type": "Command", "value": rule.detail}
        valid_compliance, has_missing_section_lines, missing_variable_values = ComplianceService._validate_each_rule_line_for_variables(
            rule_detail, config, variable_values
        )
        result = dict(
            valid_compliance=valid_compliance,
            has_missing_section_lines=has_missing_section_lines,
            missing_variable_values=missing_variable_values,
        )

    return result



def _verify_rule_for_config_using_ccp(rule, config, variable_values)):
    """
    Parses the config and rule using the ciscoconfparser lib.
    Arguments:
        @rule: use rule which is a db obj, in that rule detail has the string value of the actual rule
        @config: not the entire file read string, its the parsed one with lists and nested for substeps.
        @varialble_values: need it as it is with values and option (AND/OR)

    Returns:
        @dict: {
            valid_compliance: valid_compliance,
            has_missing_section_lines: true/false,
            missing_variable_values: [

            ],
        }
    """
    # use rule which is a db obj, in that rule detail has the string value of the actual rule
    original_parsed = CiscoConfParse(original_config.splitlines())

    req_parsed = CiscoConfParse(required_config.splitlines())

    # print ("\n".join(original_parsed.sync_diff(required_config.splitlines(), "")))

    print ((original_parsed.req_cfgspec_all_diff(required_config.splitlines())))


def get_variable_replaced_rule(self, rule, variable_values):
        variable_replaced_commands = []
        option = None

        regex_validator = r"(@{{.*?}})"
        inverse_regex_validator = r"(}}@{{)"

        validators = set(re.findall(RuleVariable.regex_validator, rule))

        if validators:
            for validator in validators:
                place_holder_values = []
                if validator in variable_values:
                    variable_value = variable_values.get(validator)
                    values = (
                        variable_value.get("customer_variables_variable_values")
                        if variable_value.get(
                            "customer_variables_variable_values"
                        )
                        else variable_value.get("variable_values")
                    )

                    for value in values:
                        place_holder_values.append(
                            command.replace(validator, value)
                        )

                    option = (
                        variable_value.get("customer_variables_option")
                        if variable_value.get("customer_variables_option")
                        else variable_value.get("option")
                    )

                    variable_replaced_commands.append({
                        "command_possible_values": place_holder_values,
                        "option": option
                    })
        else:
            variable_replaced_commands.append({
                "command_possible_values": [command],
                "option": option
            })
