import time
import re
from regex_engine import generator
from difflib import SequenceMatcher, Differ

from celery.utils.log import get_task_logger
from ciscoconfparse import CiscoConfParse

from config_compliance.models import ConfigComplianceScore, Rule, RuleVariable

logger = get_task_logger(__name__)


class RuleValidatorViaCiscoConfParse:
    temp_parsed_config = None
    COMMAND_MATCHING_THRESHOLD = 0.75
    rule_multi_section_data = []

    def __init__(self, config, rule, variable_values):
        self.rule_specific_variable_values = {}
        self.original_parsed_config = CiscoConfParse(config.splitlines())
        self.rule = rule
        self.variable_values = variable_values

    def _verify_rule_for_config_using_ccp(self):
        rules_stats = {
            "variable_values": {},
            "is_valid_rule": True,
            "valid_commands": [],
            "invalid_commands": [],
            "multi_section_data": [],
        }

        try:
            self.rule_multi_section_data = []

            is_valid_rule = True

            logger.info("**** Compliance Validation ****")

            logger.info(f"Rule ID: {self.rule.id}")
            logger.info(f"Rule: {self.rule.detail}")
            rule_function_type = self.rule.function

            if "*" in self.rule.detail:
                logger.info("Wildcard Rule")
                (
                    rule_status,
                    valid_cmd_list,
                    invalid_cmd_list,
                ) = self.process_wildcard_rule()

                rules_stats["valid_commands"] = valid_cmd_list
                rules_stats["invalid_commands"] = invalid_cmd_list
                rules_stats["is_valid_rule"] = (
                    not rule_status if self.rule.apply_not else rule_status
                )

                for cmd_data in rules_stats["invalid_commands"][0]:
                    if cmd_data["command"] in self.rule.exceptions:
                        rules_stats["is_valid_rule"] = True

                logger.info("********* Compliance Stats *********")
                return rules_stats

            if (
                self.rule.function is not None
                and rule_function_type == Rule.ROUTED_INTERFACE
            ):
                logger.info("Routed Interface Rule")
                rules_stats_data = self.process_routed_interfrace_rule()

                rules_stats["valid_commands"] = rules_stats_data[
                    "valid_commands"
                ]
                rules_stats["invalid_commands"] = rules_stats_data[
                    "invalid_commands"
                ]

                logger.info("********* Compliance Stats *********")
                return rules_stats

            rule_line_separated = (
                self.separate_multiline_and_single_line_rules()
            )

            for rule_commands in rule_line_separated:
                is_current_command_valid = True
                if len(rule_commands) > 1:
                    logger.info("Multi line cmd validation")
                    # parse as a multi line rule
                    (
                        rule_status,
                        valid_cmd_list,
                        invalid_cmd_list,
                    ) = self.validate_multline_line_cmd(rule_commands)
                    if not rule_status:
                        # got a invalid rule.
                        is_valid_rule = False
                        rules_stats["invalid_commands"].append(
                            invalid_cmd_list
                        )
                    else:
                        rules_stats["invalid_commands"].append(
                            invalid_cmd_list
                        )
                else:
                    logger.info("Single line cmd validation")
                    # parse as a single line rule.
                    (
                        rule_status,
                        valid_cmd_list,
                        invalid_cmd_list,
                    ) = self.validate_single_line_cmd(rule_commands[0])
                    if not rule_status:
                        # got a invalid rule.
                        is_valid_rule = False
                        # diff_result = self.find_matching_commands(rule_commands[0])
                        rules_stats["invalid_commands"].append(
                            invalid_cmd_list
                        )
                    else:
                        rules_stats["valid_commands"].append(valid_cmd_list)

            rules_stats["is_valid_rule"] = (
                not is_valid_rule if self.rule.apply_not else is_valid_rule
            )
            rules_stats["variable_values"] = self.rule_specific_variable_values
            rules_stats["multi_section_data"] = self.rule_multi_section_data

            for cmd_data in rules_stats["invalid_commands"][0]:
                if cmd_data["command"] in self.rule.exceptions:
                    rules_stats["is_valid_rule"] = True

            logger.info("********* Compliance Stats *********")

            return rules_stats
        except Exception as e:
            logger.info(f"Failed rule: {self.rule.detail}")
            logger.info(f"Failed rule id: {self.rule.id}")
            logger.info(f"Error: {e}")
            return rules_stats

    def process_wildcard_rule(self):
        is_valid_command = False
        valid_commands = [[]]
        invalid_commands = []

        found_matching_cmd = []
        no_matching_cmd = []

        # rule_regex = self.rule.detail.replace('*', '.*')
        command_line_with_replaced_variable = self.get_variable_replaced_rule(
            self.rule.detail
        )
        commands_with_variable_values = command_line_with_replaced_variable[
            0
        ].get("command_possible_values", [])

        for cmd_regex in commands_with_variable_values:
            matching_commands = self.original_parsed_config.find_objects(
                rf"{cmd_regex}"
            )
            if len(matching_commands) < 1:
                no_matching_cmd.append(cmd_regex)
            else:
                found_matching_cmd.extend(matching_commands)

        cmd_diffs = self.original_parsed_config.req_cfgspec_all_diff(
            no_matching_cmd
        )

        no_matching_cmd_len = len(no_matching_cmd)

        for i in range(0, min(20, no_matching_cmd_len)):
            in_cmd = cmd_diffs[i]
            diff_result = self.find_matching_commands(
                in_cmd, found_matching_cmd
            )
            invalid_commands.append(diff_result)

        command_variable_condition_obj = command_line_with_replaced_variable[
            0
        ].get("option", None)
        cmd_option_list = list(command_variable_condition_obj.keys())

        command_variable_condition = (
            cmd_option_list[0] if len(cmd_option_list) > 0 else None
        )
        matching_cmds_len = len(found_matching_cmd)

        if len(found_matching_cmd) > 0:
            is_valid_command = True

            if (
                command_variable_condition == "AND"
                and matching_cmds_len != len(commands_with_variable_values)
            ):
                is_valid_command = False

            for cmd in found_matching_cmd:
                found_command_stats = {
                    "detail": cmd.text,
                    "line_number": cmd.linenum,
                }
                valid_commands[0].append(found_command_stats)

        return is_valid_command, valid_commands, invalid_commands

    def process_routed_interfrace_rule(self):
        valid_commands = [[]]
        invalid_commands = [[]]

        parse_interfaces = self.original_parsed_config.find_objects(
            r"^interface"
        )

        interface_line_number_map = {}

        interface_conf_lines = []

        for interface in parse_interfaces:
            child_lines = interface.re_search_children(r"^ ip\saddress")
            single_intf_lines = []

            if len(child_lines) > 0:
                single_intf_lines.append(interface.text)
                interface_line_number_map[interface.text] = interface.linenum

                for child in interface.children:
                    single_intf_lines.append(child.text)

            if len(single_intf_lines) > 0:
                interface_conf_lines.append(single_intf_lines)

        self.temp_parsed_config = self.original_parsed_config

        for routed_intf in interface_conf_lines:
            # process each and validate the rule
            interface_name = routed_intf[0]
            interface_line_number = interface_line_number_map.get(
                interface_name, 0
            )

            self.original_parsed_config = CiscoConfParse(routed_intf)
            rule_line_separated = (
                self.separate_multiline_and_single_line_rules()
            )

            for rule_commands in rule_line_separated:
                is_current_command_valid = True

                if len(rule_commands) > 1:
                    # parse as a multi line rule
                    rule_status = self.validate_multline_line_cmd(
                        rule_commands
                    )
                    if not rule_status:
                        # got a invalid rule.
                        is_valid_rule = False
                else:
                    # parse as a single line rule.
                    rule_status, _, _ = self.validate_single_line_cmd(
                        rule_commands[0]
                    )
                    if not rule_status:
                        # got a invalid rule.
                        is_valid_rule = False
                        interface_stats = {
                            "command": interface_name,
                            "line_number": interface_line_number,
                            "partial_matching_commands": [],
                        }
                        invalid_commands[0].append(interface_stats)
                    else:
                        interface_stats = {
                            "detail": interface_name,
                            "line_number": interface_line_number,
                        }

                        valid_commands[0].append(interface_stats)

        return {
            "valid_commands": valid_commands,
            "invalid_commands": invalid_commands,
        }

    def generate_combination_for_child_line(self, child_cmd_list):
        child_cmd_combination_list = [[]]

        temp_and_list = []
        temp_or_list = []

        for cmd in child_cmd_list:

            command_line_with_replaced_variable = (
                self.perform_variable_replacement(cmd, "multi_line")
            )

            # HANDLED FOR ONLY ONE VARIABLE FOR NOW.
            command_possible_values = command_line_with_replaced_variable.get(
                "command_possible_values", []
            )
            command_possible_values_len = len(command_possible_values)
            is_mutli_option = command_line_with_replaced_variable.get(
                "is_mutli_option", False
            )

            command_variable_condition_obj = (
                command_line_with_replaced_variable.get("option", None)
            )

            cmd_option_list = list(command_variable_condition_obj.keys())

            if command_possible_values_len < 1:
                continue

            if len(cmd_option_list) < 1 and command_possible_values_len < 1:
                continue

            command_variable_condition = (
                cmd_option_list[0] if len(cmd_option_list) > 0 else None
            )

            if (
                command_variable_condition == "AND"
                or command_variable_condition is None
            ):
                temp_and_list.extend(command_possible_values)
                for ch_res in child_cmd_combination_list:
                    ch_res.extend(command_possible_values)

            if (
                command_variable_condition == "OR"
                or command_variable_condition == "RANGE"
            ):
                start_comb = []

                for res in command_possible_values:
                    for child_res in child_cmd_combination_list:
                        new_or_list = []
                        new_or_list.extend(child_res)
                        new_or_list.append(res)

                        start_comb.append(new_or_list)

                child_cmd_combination_list = start_comb

        return child_cmd_combination_list

    def validate_multline_line_cmd(self, multi_commands_list):
        is_valid_command = False
        cmd_diffs_len = 0
        parent_cmd = multi_commands_list[0]

        # validators = set(re.findall(regex_validator, word))
        parent_command_line_with_replaced_variable = (
            self.perform_variable_replacement(parent_cmd, "multi_line")
        )

        parent_command_possible_values = (
            parent_command_line_with_replaced_variable.get(
                "command_possible_values", []
            )
        )
        parent_command_possible_values_len = len(
            parent_command_possible_values
        )
        is_mutli_option = parent_command_line_with_replaced_variable.get(
            "is_mutli_option", False
        )

        parent_command_variable_condition = (
            parent_command_line_with_replaced_variable.get("option", None)
        )

        if parent_command_possible_values_len < 1:
            self.rule_multi_section_data.append(
                {"command": f"- {parent_cmd}", "type": "no_match"}
            )
            return is_valid_command, [], []

        cmd_option_list = list(parent_command_variable_condition.keys())

        if len(cmd_option_list) < 1 and parent_command_possible_values_len < 1:
            self.rule_multi_section_data.append(
                {"command": f"- {parent_cmd}", "type": "no_match"}
            )
            return is_valid_command, [], []

        if is_mutli_option:
            self.rule_multi_section_data.append(
                {"command": f"- {parent_cmd}", "type": "no_match"}
            )
            return is_valid_command, [], []

        parent_command_variable_condition = (
            cmd_option_list[0] if len(cmd_option_list) > 0 else None
        )

        valid_parent_linespecs = []
        for parent_replaced_val in parent_command_possible_values:
            parents_matching = self.original_parsed_config.find_all_children(
                f"^{parent_replaced_val}"
            )
            if len(parents_matching) > 0:
                valid_parent_linespecs.append(parents_matching)

        if len(valid_parent_linespecs) < 1:
            self.rule_multi_section_data.append(
                {"command": f"- {parent_cmd}", "type": "no_match"}
            )
            return False, [], []

        child_cmd_combinations = self.generate_combination_for_child_line(
            multi_commands_list[1:]
        )

        invalid_cmd_list = []
        valid_cmd_list = []

        nearest_match_cmds_matching_count = -1

        temp_multi_section_data = []

        for parent_line_spec in valid_parent_linespecs:
            parent_spec = parent_line_spec[0]
            parent_line_spec_config = CiscoConfParse(parent_line_spec)

            for child_combo in child_cmd_combinations:
                child_line_specs = child_combo
                child_line_specs.insert(0, parent_spec)
                cmd_diffs = parent_line_spec_config.req_cfgspec_all_diff(
                    child_line_specs
                )

                cmd_diffs_len = len(cmd_diffs)
                matching_cmds = []

                matching_cmds = list(set(child_combo) - set(cmd_diffs))

                if len(matching_cmds) >= nearest_match_cmds_matching_count:
                    # closet_match_commands = child_line_specs
                    for in_cmd in cmd_diffs:
                        diff_result = self.find_matching_commands(
                            in_cmd, matching_cmds
                        )
                        invalid_cmd_list.append(diff_result)

                    for spec in child_line_specs:
                        if spec in matching_cmds:
                            temp_multi_section_data.append(
                                {"command": f"+ {spec}", "type": "match"}
                            )
                        else:
                            spec_diff_result = next(
                                (
                                    item
                                    for item in invalid_cmd_list
                                    if item["command"] == spec
                                ),
                                False,
                            )
                            match_type = "no_match"

                            if (
                                spec_diff_result
                                and len(
                                    spec_diff_result[
                                        "partial_matching_commands"
                                    ]
                                )
                                > 0
                            ):
                                match_type = "partial_match"

                            temp_multi_section_data.append(
                                {"command": f"- {spec}", "type": match_type}
                            )

                valid_cmd_list.extend(
                    self.get_line_num_for_matching_commands(matching_cmds)
                )

                if cmd_diffs_len == 0:
                    is_valid_command = True
                    break

        self.rule_multi_section_data.extend(temp_multi_section_data)

        return is_valid_command, valid_cmd_list, invalid_cmd_list

    def validate_single_line_cmd(self, command_line):
        """
        Handle each line verify its valid and if not find the exact diff
        between possible values and return for highlighting.
        """
        cmd_diffs = []
        is_valid_command = False

        exact_matches = []
        command_line_with_replaced_variable = (
            self.perform_variable_replacement(command_line)
        )

        # HANDLED FOR ONLY ONE VARIABLE FOR NOW.
        command_possible_values = command_line_with_replaced_variable.get(
            "command_possible_values", []
        )
        command_possible_values_len = len(command_possible_values)
        is_mutli_option = command_line_with_replaced_variable.get(
            "is_mutli_option", False
        )

        command_variable_condition_obj = (
            command_line_with_replaced_variable.get("option", None)
        )

        cmd_option_list = list(command_variable_condition_obj.keys())

        if command_possible_values_len < 1:
            return is_valid_command, [], []

        if len(cmd_option_list) < 1 and command_possible_values_len < 1:
            return is_valid_command, [], []

        if is_mutli_option:
            # parse for multi logic handling special case
            return is_valid_command, [], []

        command_variable_condition = (
            cmd_option_list[0] if len(cmd_option_list) > 0 else None
        )
        matching_cmds = []
        cmd_diffs = []
        result_valid_line_data = []
        if command_variable_condition == "RANGE":
            for cmd in command_possible_values:
                matching_cmd_list = self.original_parsed_config.find_objects(
                    rf"{cmd}"
                )
                if len(matching_cmd_list) > 0:
                    matching_cmds.extend(matching_cmd_list)
            cmd_diffs_temp = [command_line]
            for cmd in matching_cmds:
                cmd_line_num = cmd.linenum

                result_valid_line_data.append(
                    {"command": cmd.text, "line_number": cmd_line_num}
                )
        else:
            cmd_diffs = self.original_parsed_config.req_cfgspec_all_diff(
                command_possible_values
            )
            matching_cmds = list(set(command_possible_values) - set(cmd_diffs))
            cmd_diffs_temp = cmd_diffs
            result_valid_line_data = self.get_line_num_for_matching_commands(
                matching_cmds
            )

        # list of missing lines from the config
        # cmd_diffs = self.original_parsed_config.req_cfgspec_all_diff(command_possible_values)

        cmd_diffs_len = len(cmd_diffs)
        # if command_variable_condition != "RANGE":
        #     matching_cmds = list(set(command_possible_values) - set(cmd_diffs))

        # matching_cmds = list(set(command_possible_values) - set(cmd_diffs))

        # result_valid_line_data = self.get_line_num_for_matching_commands(matching_cmds)
        valid_cmd_list = result_valid_line_data

        invalid_cmd_list = []

        if command_variable_condition == "RANGE":
            cmd_diffs_temp = [command_line]

        for i in range(0, min(20, cmd_diffs_len)):
            in_cmd = cmd_diffs_temp[i]
            diff_result = self.find_matching_commands(in_cmd, matching_cmds)
            invalid_cmd_list.append(diff_result)

        if (
            command_variable_condition == "AND"
            or command_variable_condition is None
        ):
            """
            if the cmd_diffs len is 0 ie all the requuired cmds are present in the
            config
            """
            if cmd_diffs_len == 0:
                is_valid_command = True

        if (
            command_variable_condition == "OR"
            or command_variable_condition == "RANGE"
        ):
            """
            if the cmd_diffs len is less than all the requuired cmds list len,
            ie atleast one command line is present in the config from the required lines.
            """
            if cmd_diffs_len < command_possible_values_len:
                is_valid_command = True

        return is_valid_command, valid_cmd_list, invalid_cmd_list

    def get_line_num_for_matching_commands(self, matching_cmds):
        result_data = []

        for cmd in matching_cmds:
            cc_cmd_obj = self.original_parsed_config.find_objects(rf"{cmd}")
            cmd_line_num = cc_cmd_obj[0].linenum if len(cc_cmd_obj) else 0

            result_data.append({"command": cmd, "line_number": cmd_line_num})

        return result_data

    def perform_variable_replacement(self, command, rule_type="single_line"):
        variable_replaced_commands = {
            "command_possible_values": [],
            "option": {},
            "is_multi_option": False,
        }

        regex_validator = r"(@{{.*?}})"
        inverse_regex_validator = r"(}}@{{)"

        inline_variable_regex_expr = "(#\\w+\\(.*?\\))"

        variable_validators = set(re.findall(regex_validator, command))

        inline_variable_validators = set(
            re.findall(inline_variable_regex_expr, command)
        )

        command_list = [command]

        if (
            len(inline_variable_validators) > 0
            and len(variable_validators) > 0
        ):
            variable_replaced_commands["is_multi_option"] = True

        if len(inline_variable_validators) > 0:
            # call the inline replacing func
            inline_variable_cmds_obj = self.get_inline_variable_replaced_rule(
                command, rule_type
            )
            for in_res in inline_variable_cmds_obj:
                command_list = in_res.get("command_possible_values", [])
                command_option_obj = in_res.get("option", {})

                variable_replaced_commands["option"].update(command_option_obj)

        if variable_validators:
            for cmd in command_list:
                # call the variables dict replacing func with a list of commands.
                command_line_with_replaced_variable_obj = (
                    self.get_variable_replaced_rule(cmd)
                )

                for res in command_line_with_replaced_variable_obj:
                    command_poss_list = res.get("command_possible_values", [])
                    command_option_obj = res.get("option", {})

                    variable_replaced_commands[
                        "command_possible_values"
                    ].extend(command_poss_list)
                    variable_replaced_commands["option"].update(
                        command_option_obj
                    )
        else:
            variable_replaced_commands["command_possible_values"].extend(
                command_list
            )

        return variable_replaced_commands

    def get_variable_replaced_rule(self, command):
        variable_replaced_commands = []
        option = None

        regex_validator = r"(@{{.*?}})"
        inverse_regex_validator = r"(}}@{{)"

        inline_variable_regex_expr = "(@\\w+\\(.*?\\))"

        variable_validators = set(re.findall(regex_validator, command))

        if variable_validators:
            for validator in variable_validators:
                place_holder_values = []
                if validator in self.variable_values:
                    variable_value = self.variable_values.get(validator)
                    values = (
                        variable_value.get(
                            "customer_variables_variable_values"
                        )
                        if variable_value.get(
                            "customer_variables_variable_values"
                        )
                        else variable_value.get("variable_values")
                    )

                    option = (
                        variable_value.get("customer_variables_option")
                        if variable_value.get("customer_variables_option")
                        else variable_value.get("option")
                    )

                    if option == "RANGE":
                        option = "RANGE"
                        start, end = int(values[0]), int(values[1]) + 1
                        values = [value for value in range(start, end)]

                        for value in values:
                            place_holder_values.append(
                                command.replace(validator, str(value))
                            )
                    else:
                        for value in values:
                            place_holder_values.append(
                                command.replace(validator, value)
                            )

                    variable_replaced_commands.append(
                        {
                            "command_possible_values": place_holder_values,
                            "option": {
                                option: {
                                    "values": values,
                                    "variable": validator,
                                }
                            },
                        }
                    )
                    self.rule_specific_variable_values[
                        validator
                    ] = variable_value
        else:
            variable_replaced_commands.append(
                {"command_possible_values": [command], "option": {}}
            )

        return variable_replaced_commands

    def get_inline_variable_replaced_rule(
        self, command, rule_type="single_line"
    ):
        generate = generator()
        has_range_condition = False
        inline_variable_replaced_commands = []

        temp_combination_list = [command]

        inline_variable_regex_expr = "(#\\w+\\(.*?\\))"
        inline_variable_validators = set(
            re.findall(inline_variable_regex_expr, command)
        )

        if inline_variable_validators:
            for validator in inline_variable_validators:
                place_holder_values = []
                option = None
                variable_option_validator = set(
                    re.findall(r"\#([^{{].*?)\(", validator)
                )
                variable_option = variable_option_validator.pop()

                variable_value_validator = set(
                    re.findall(r"\((.*?)\)", validator)
                )
                variable_values = variable_value_validator.pop()

                if variable_option == "NUM_RANGE":
                    has_range_condition = True
                    option = "RANGE"
                    range_values = variable_values.strip().split("-")
                    try:
                        start, end = int(range_values[0]), int(
                            range_values[-1]
                        )

                        if rule_type == "multi_line":
                            for value in range(start, end + 1):
                                for comb_cmd in temp_combination_list:
                                    place_holder_values.append(
                                        comb_cmd.replace(validator, str(value))
                                    )
                        else:
                            regex_range = generate.numerical_range(start, end)

                            for comb_cmd in temp_combination_list:
                                regex_cmd = comb_cmd.replace(
                                    validator, regex_range[1:-1]
                                )
                                place_holder_values.append(regex_cmd)

                    except ValueError:
                        pass

                elif variable_option == "VAR_OPTION_OR":
                    option = "OR"
                    values = [
                        val.strip()
                        for val in variable_values.strip().split(",")
                    ]
                    for value in values:
                        for comb_cmd in temp_combination_list:
                            place_holder_values.append(
                                comb_cmd.replace(validator, str(value))
                            )

                elif variable_option == "VAR_OPTION_AND":
                    option = "AND"
                    values = " ".join(
                        [
                            val.strip()
                            for val in variable_values.strip().split(",")
                        ]
                    )

                    # for value in values:
                    for comb_cmd in temp_combination_list:
                        place_holder_values.append(
                            comb_cmd.replace(validator, str(values))
                        )
                else:
                    # this is an invalid rule
                    pass

                temp_combination_list = []

                temp_combination_list.extend(place_holder_values)

        inline_variable_replaced_commands.append(
            {
                "command_possible_values": temp_combination_list,
                "option": {"RANGE" if has_range_condition else "OR": []},
            }
        )

        return inline_variable_replaced_commands

    def separate_multiline_and_single_line_rules(self):
        raw_rule = self.rule.detail.strip()
        # raw_rule = self.rule.strip()
        raw_rule_commands_list = raw_rule.split("\n")
        raw_rule_commands_list_len = len(raw_rule_commands_list)
        result = []
        _result = []
        is_open = False

        for index in range(raw_rule_commands_list_len):
            command_line = raw_rule_commands_list[index]
            if (
                re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", command_line)
                and not is_open
            ):
                is_open = True
                continue
            if re.match(r"^[@_!#$%^&*()<>?/\|}{~:]", command_line) and is_open:
                is_open = False
                if _result:
                    result.append(_result)
                    _result = []
                continue
            if command_line:
                is_open = True
                if command_line.startswith(" !"):
                    continue
                else:
                    # if next is child then append to _result,
                    # else to the main list
                    is_open = True
                    if command_line.startswith(" "):
                        _result.append(command_line)
                        continue
                    else:
                        is_open = False
                        result.append(_result)
                        _result = []
                        _result.append(command_line)
        if _result:
            result.append(_result)

        # remove empty lists
        cleaned_rule_commands = []
        cleaned_rule_commands = [res for res in result if len(res) > 0]

        return cleaned_rule_commands

    def find_matching_commands(self, command, matched_cmds):
        regex_validator = r"(@{{.*?}})"
        variable_regex_values = []

        command_words_list = command.split(" ")
        variable_validators = []
        variable_validators = list(set(re.findall(regex_validator, command)))

        inline_variable_regex_expr = "(#\\w+\\(.*?\\))"
        inline_variable_validators = []
        inline_variable_validators = list(
            set(re.findall(inline_variable_regex_expr, command))
        )

        if len(inline_variable_validators) > 0:
            command_words_list = command.split(inline_variable_validators[0])
            command_words_list = command_words_list[0].strip().split()

        variable_regex_values.extend(variable_validators)

        command_words_list_len = len(command_words_list)

        if len(variable_regex_values) > 0:
            till_index = 99999999
            for index in range(command_words_list_len):
                word = command_words_list[index]

                for value in variable_regex_values:
                    if value in word:
                        if index <= till_index:
                            till_index = index

            to_search_val = " ".join(command_words_list[:till_index])
        else:
            to_search_val = (
                command_words_list[0] if command_words_list_len > 0 else ""
            )

        commands = self.original_parsed_config.find_objects(
            rf"{to_search_val}"
        )

        possible_commands = []
        for cmd in commands:
            if not cmd.text in matched_cmds:
                possible_commands.append(
                    {"cmd": cmd.text, "line_number": cmd.linenum}
                )

        diff_result = self.get_single_command_diff(command, possible_commands)

        return diff_result

    def get_single_command_diff(self, command, possible_commands):
        diff_result = {"command": command, "partial_matching_commands": []}

        for cmd_obj in possible_commands:
            cmd = cmd_obj["cmd"]
            cmd_line_number = cmd_obj["line_number"]

            s = SequenceMatcher(None, command, cmd)
            command_matching_ratio = s.ratio()

            if command_matching_ratio >= self.COMMAND_MATCHING_THRESHOLD:
                d = Differ()

                # calculate the difference between the two texts
                diff = d.compare(cmd.split(), command.split())
                # remove ? ^ ^
                diff = [res for res in diff if "?" not in res]

                diff_result["partial_matching_commands"].append(
                    {
                        "found_command": cmd,
                        "line_number": cmd_line_number,
                        "command_diff": list(diff),
                        "match_percentage": round(command_matching_ratio, 2),
                    }
                )

        return diff_result


# RuleValidatorViaCiscoConfParse("", "", "").get_inline_variable_replaced_rule("logging buffered #NUM_RANGE(5000-410000) #VAR_OPTION_OR(informational,6)")

# RuleValidatorViaCiscoConfParse("", "", "").validate_single_line_cmd("logging buffered #NUM_RANGE(5000-410000) #VAR_OPTION_OR(informational,6)")
