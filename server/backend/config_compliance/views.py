from enum import Enum

from django.db.models import Q, QuerySet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, status
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from django.db.models import Q, QuerySet
from accounts.custom_permissions import IsProviderUser
from accounts.models import UserType
from caching.service import CacheService
from config_compliance.custom_filters import (
    ComplianceRuleFilterSet,
    RuleVariableFilter,
)
from config_compliance.mixins import (
    RuleQuerySetMixin,
    RuleVariablesQuerySetMixin,
)
from config_compliance.models import (
    Classification,
    Rule,
    RuleVariableCustomerMapping,
)
from config_compliance.permissions import (
    IsConfigComplianceReadAllowed,
    IsConfigComplianceWriteAllowed,
)
from config_compliance.report import get_customer_device_compliance_report
from config_compliance.serializers import (
    ClassificationListCreateSerializer,
    RuleCreateSerializer,
    RuleRetrieveSerializer,
    RuleUpdateSerializer,
    RuleVariableDestroySerializer,
    RuleVariableListCreateSerializer,
    RuleVariableRetrieveUpdateSerializer,
)
from config_compliance.services import (
    ComplianceService,
    DeviceMonitoringService,
    RulesService,
)
from core.models import DeviceInfo
from core.services import DeviceService
from core import utils
from core.tasks.logic_monitor.logic_monitor import (
    sync_logic_monitor_config_data,
)
from core.utils import get_customer_id


class DeviceMonitoringConfigListAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        devices = DeviceMonitoringService.get_monitoring_config_list(
            request.provider.id,
            customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
        )
        if not devices:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data=devices, status=status.HTTP_200_OK)


class RuleLevelsListAPIView(APIView):
    """
    API View to list rule levels.
    """

    def get(self, request):
        level_types = [item[1] for item in Rule.LEVEL_CHOICES]
        return Response(data=level_types, status=status.HTTP_200_OK)


class RuleFunctionsListAPIView(APIView):
    """
    API View to list rule functions.
    """

    def get(self, request):
        function_types = [
            {"key": item[0], "name": item[1]} for item in Rule.FUNCTION_CHOICES
        ]
        return Response(data=function_types, status=status.HTTP_200_OK)


class RuleClassificationListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ClassificationListCreateSerializer
    pagination_class = None

    def get_queryset(self):
        return Classification.objects.filter(provider=self.request.provider)

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if (
            not self.request.user.is_anonymous
            and self.request.user.type == UserType.CUSTOMER
        ):
            if self.request.method == "GET":
                permission_classes.append(IsConfigComplianceReadAllowed)
            elif self.request.method == "POST":
                permission_classes.append(IsConfigComplianceWriteAllowed)
        else:
            permission_classes.append(IsProviderUser)
        return [permission() for permission in permission_classes]


class RuleListCreateAPIView(RuleQuerySetMixin, generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "detail",
        "created_by__first_name",
        "created_by__last_name",
    ]
    ordering_fields = ["level_int"]
    filterset_class = ComplianceRuleFilterSet

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if self.request.user.type == UserType.CUSTOMER:
            if self.request.method == "GET":
                permission_classes.append(IsConfigComplianceReadAllowed)
            elif self.request.method == "POST":
                permission_classes.append(IsConfigComplianceWriteAllowed)
        else:
            permission_classes.append(IsProviderUser)
        return [permission() for permission in permission_classes]

    def perform_create(self, serializer):
        customer_id = None
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
            is_global = False
        except NotFound:
            is_global = True
        serializer.save(
            provider=self.request.provider,
            customer_id=customer_id,
            created_by=self.request.user,
            is_global=is_global,
        )
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return RuleCreateSerializer
        else:
            return RuleRetrieveSerializer


class RuleRetrieveUpdateDestroyAPIView(
    RuleQuerySetMixin, generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = [IsAuthenticated]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
        except NotFound:
            customer_id = None
        context.update(dict(customer_id=customer_id))
        return context

    def perform_update(self, serializer):
        customer_id = serializer.context.get("customer_id")
        # In case a global rule is being edited for a customer
        # we need to clone the original rule for the customer
        # the logic below clones a rule, updates the values in it
        # sets the is_global flag as false
        # We also need to un-assign the the global rule for the customer
        if customer_id and serializer.instance.is_global:
            # un-assign global rule
            serializer.instance.customers_not_assigned.set([customer_id])

            # clone global rule and create new customer rule
            rule: Rule = serializer.instance
            rule.__dict__.update(serializer.validated_data)
            # clone rule
            rule.is_global = False
            rule.is_enabled = True
            rule.customer_id = customer_id
            rule.pk = None
            rule.updated_by = self.request.user
            rule.save()

        else:
            serializer.save(
                provider=self.request.provider,
                updated_by=self.request.user,
            )
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def perform_destroy(self, instance):
        instance.delete()
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def get_serializer_class(self):
        if self.request.method in ("PUT", "PATCH"):
            return RuleUpdateSerializer
        elif self.request.method == "GET":
            return RuleRetrieveSerializer

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if self.request.user.type == UserType.CUSTOMER:
            if self.request.method == "GET":
                permission_classes.append(IsConfigComplianceReadAllowed)
            elif self.request.method in ("PUT", "PATCH"):
                permission_classes.append(IsConfigComplianceWriteAllowed)
        else:
            permission_classes.append(IsProviderUser)
        return [permission() for permission in permission_classes]


class RuleAction(Enum):
    ASSIGN = "assign"
    UN_ASSIGN = "unassign"


class CustomerRuleAssignmentUpdateAPIView(APIView):
    def put(self, request, *args, **kwargs):
        action = kwargs.get("action")
        try:
            action = RuleAction(action)
            rule_id = int(kwargs["rule_id"])
            customer_id = get_customer_id(request, **kwargs)
            try:
                rule = Rule.objects.get(id=rule_id)
                if action is RuleAction.UN_ASSIGN:
                    rule.customers_not_assigned.set([customer_id])
                    if not rule.is_global:
                        rule.is_enabled = False
                elif action is RuleAction.ASSIGN:
                    rule.customers_not_assigned.remove(customer_id)
                    if not rule.is_global:
                        rule.is_enabled = True
                rule.save()
            except Rule.DoesNotExist:
                raise NotFound("Rule not found")
        except ValueError:
            raise ValidationError(
                {"error": "Please specify the action: unassign or assign"}
            )
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

        return Response(status=status.HTTP_200_OK)


class RuleVariableTypeListAPIView(APIView):
    """
    API View to list Rule Variables types.
    """

    def get(self, request):
        variable_types = [
            item[1] for item in RuleVariableListCreateSerializer.TYPE_CHOICES
        ]
        return Response(data=variable_types, status=status.HTTP_200_OK)


class RuleVariableListCreateAPIView(
    RuleVariablesQuerySetMixin, generics.ListCreateAPIView
):
    """
    API View to list create Rule Variables.
    """

    permission_classes = [IsAuthenticated]
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = ["name", "description", "variable_code"]
    filterset_class = RuleVariableFilter

    def perform_create(self, serializer):
        customer_id = None
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
            is_global = False
        except NotFound:
            is_global = True
        serializer.save(
            provider=self.request.provider,
            customer_id=customer_id,
            is_global=is_global,
        )
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def get_serializer_context(self):
        """
        pass request attribute to serializer
        """
        context = super(
            RuleVariableListCreateAPIView, self
        ).get_serializer_context()
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
        except NotFound:
            customer_id = None
        context.update({"customer_id": customer_id})
        return context

    def get_serializer_class(self):
        if self.request.method == "GET" and (
            "customer_id" in self.kwargs or self.request.customer
        ):
            return RuleVariableRetrieveUpdateSerializer
        else:
            return RuleVariableListCreateSerializer

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if self.request.user.type == UserType.CUSTOMER:
            if self.request.method == "GET":
                permission_classes.append(IsConfigComplianceReadAllowed)
            elif self.request.method == "POST":
                permission_classes.append(IsConfigComplianceWriteAllowed)
        else:
            permission_classes.append(IsProviderUser)
        return [permission() for permission in permission_classes]


class RuleVariableRetrieveUpdateDestroyAPIView(
    RuleVariablesQuerySetMixin, generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = [IsAuthenticated]
    serializer_class = RuleVariableRetrieveUpdateSerializer

    def get_permissions(self):
        permission_classes = [IsAuthenticated]
        if self.request.user.type == UserType.CUSTOMER:
            if self.request.method == "GET":
                permission_classes.append(IsConfigComplianceReadAllowed)
            elif self.request.method in ("PUT", "PATCH"):
                permission_classes.append(IsConfigComplianceWriteAllowed)
        else:
            permission_classes.append(IsProviderUser)
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        action = self.request.query_params.get("action", "false")
        if action == "delete":
            return RuleVariableDestroySerializer
        else:
            return RuleVariableRetrieveUpdateSerializer

    def perform_update(self, serializer):
        try:
            customer_id = get_customer_id(self.request, **self.kwargs)
        except NotFound:
            customer_id = None
        if customer_id:
            rule_variable = self.get_object()
            if rule_variable.is_global:
                option = serializer.validated_data["option"]
                variable_values = serializer.validated_data["variable_values"]
                RuleVariableCustomerMapping.objects.update_or_create(
                    variable=rule_variable,
                    provider=self.request.provider,
                    customer_id=customer_id,
                    defaults={
                        "option": option,
                        "variable_values": variable_values,
                    },
                )
            else:
                serializer.save()
        else:
            serializer.save()
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def perform_destroy(self, instance):
        Rule.objects.filter(detail__contains=instance.variable_code).delete()
        instance.delete()
        cache_key = CacheService.CACHE_KEYS.CONFIGURATION_LISTING.format(
            self.request.provider.id
        )
        CacheService.invalidate_data(cache_key)

    def get_serializer_context(self):
        """
        pass request attribute to serializer
        """
        context = super(
            RuleVariableRetrieveUpdateDestroyAPIView, self
        ).get_serializer_context()
        customer_id = self.kwargs.get("customer_id")
        context.update({"customer_id": customer_id})
        return context


class RerunComplianceStatsTask(APIView):

    """Rerun Compliance Task and update Database"""

    def post(self, request, *args, **kwargs):
        customer_id = request.data.get("customer_id")
        rule_ids = request.data.get("rule_ids", [])
        device_ids = request.data.get("devices_ids", [])
        task = ComplianceService.refresh_compliance_stats_data(
            request.provider.id,
            customer_id=customer_id,
            rule_ids=rule_ids,
            device_ids=device_ids,
        )
        data = {"task_id": task.task_id}
        return Response(data=data, status=status.HTTP_200_OK)


class DeviceConfigComplianceReportAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        report_meta = get_customer_device_compliance_report(
            request.provider.id,
            customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
        )

        if not report_meta:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data=report_meta, status=status.HTTP_200_OK)


class TriggerConfigSyncAPIView(APIView):
    def post(self, request, *args, **kwargs):
        task = sync_logic_monitor_config_data.apply_async(
            (request.provider.id,)
        )
        data = {"task_id": task.task_id}
        return Response(data=data, status=status.HTTP_200_OK)


class RulesDashboardListAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        result = RulesService.prepare_dashboard_data(
            request.provider.id,
            customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
        )

        return Response(data=result, status=status.HTTP_200_OK)


class DeviceRuleComplianceReportAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        show_inactive_devices = request.query_params.get(
            "show-inactive", "false"
        )
        show_active_only = False if show_inactive_devices == "true" else True
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        device_id = kwargs.get("device_id")
        report_meta = get_customer_device_compliance_report(
            request.provider.id,
            customer_id,
            erp_client=request.provider.erp_client,
            category_id=category_id,
            show_active_only=show_active_only,
            device_ids=[device_id],
        )

        if not report_meta:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data=report_meta, status=status.HTTP_200_OK)


class DeviceComplianceDetailAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        device_id = kwargs.get("device_id")
        device_monitoring_data = (
            DeviceMonitoringService.get_device_monitoring_config_data(
                device_id,
                request.provider.id,
                customer_id,
                erp_client=request.provider.erp_client,
                category_id=category_id,
            )
        )

        if not device_monitoring_data:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                data=device_monitoring_data, status=status.HTTP_200_OK
            )


class DeviceConfigurationSearchAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        search_query = request.query_params.get("q", "")
        category_id = request.provider.get_all_device_categories()
        customer_id = utils.get_customer_cw_id(request, **kwargs)
        device_ids = request.data.get("device_ids", [])

        devices = DeviceMonitoringService.search_device_configuration(
            request.provider.id,
            customer_id,
            search_query,
            device_ids,
            erp_client=request.provider.erp_client,
            category_id=category_id,
        )

        if not devices:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data=devices, status=status.HTTP_200_OK)


class ComplianceDevicesListAPIView(APIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        crm_id = utils.get_customer_cw_id(request, **kwargs)
        query_conditions = (
            (
                Q(config_meta_data__logicmonitor__has_key="config_id")
                | Q(config_meta_data__solarwinds__has_key="config_id")
            )
            & Q(monitoring_data__is_managed=True)
            & Q(provider_id=request.provider.id)
        )

        category_id = request.provider.get_all_device_categories()
        devices = request.provider.erp_client.get_active_devices(
            customer_id=crm_id, category_id=category_id, fields=["id"]
        )
        device_ids = [device.get("id") for device in devices]

        query_conditions.add(Q(device_crm_id__in=device_ids), Q.AND)

        devices_configs_queryset = DeviceInfo.objects.filter(query_conditions)

        device_crm_ids = devices_configs_queryset.values_list(
            "device_crm_id", flat=True
        )

        devices = []
        if device_crm_ids:
            # Fetch Devices from ERP Client
            devices = request.provider.erp_client.get_device_by_ids(
                list(device_crm_ids)
            )

        model_numbers = []
        for device in devices:
            if device.get("model_number") is not None:
                model_numbers.append(device.get("model_number"))

        device_types_data = DeviceService.get_device_types(
            model_numbers, request.provider.id
        )

        for device in devices:
            model_number = device.get("model_number")
            device_type_data = device_types_data.get(model_number)

            if device_type_data:
                device.update(dict(device_type=device_type_data))
            else:
                device.update(dict(device_type_id="", device_type=""))

        if not devices:
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data=devices, status=status.HTTP_200_OK)
