import datetime
import os
import tempfile
from typing import Any, Dict, List

from django.conf import settings
from django.template.loader import get_template
from rest_framework.generics import get_object_or_404
from weasyprint import CSS, HTML

from accounts.models import Customer
from config_compliance.services import DeviceMonitoringService
from core.utils import group_list_of_dictionary_by_key
from document.services import DocumentService, image_as_base64
from storage.file_upload import FileUploadService
from utils import get_valid_file_name


def _get_compliance_report_data(
    provider_id: int,
    customer_crm_id: int,
    erp_client,
    category_id: int,
    show_active_only=True,
) -> List[Dict[str, Any]]:
    devices = DeviceMonitoringService.get_monitoring_config_data(
        provider_id, customer_crm_id, erp_client, category_id, show_active_only
    )
    return devices


def get_customer_device_compliance_report(
    provider_id: int,
    customer_crm_id: int,
    erp_client,
    category_id: int,
    show_active_only=True,
    report_type: str = "pdf",
    device_ids=[],
) -> object:
    devices = _get_compliance_report_data(
        provider_id, customer_crm_id, erp_client, category_id, show_active_only
    )
    devices_with_compliance_fields = list(
        filter(
            lambda device: device.get("compliance_statistics") is not None,
            devices,
        )
    )

    if len(device_ids) > 0:
        # filter to return only device specific report
        devices_with_compliance_fields = list(
            filter(
                lambda d: d["id"] in device_ids, devices_with_compliance_fields
            )
        )

    compliance_data = []
    if not devices_with_compliance_fields:
        return None

    for device in devices_with_compliance_fields:
        compliance_statistics = device.get("compliance_statistics", [])

        for rule in compliance_statistics:
            rec = {
                "device_name": device.get("device_name"),
                "rule_name": rule.get("name"),
                "rule_description": rule.get("description"),
                "result": "PASS" if rule.get("is_valid_rule") else "FAIL",
            }
            compliance_data.append(rec)
    compliance_data = group_list_of_dictionary_by_key(
        compliance_data, "device_name"
    )
    customer = get_object_or_404(
        Customer, crm_id=customer_crm_id, provider=provider_id
    )
    report_title = f"{customer.name} - Device Compliance Report"
    report_date = datetime.datetime.today().date()
    template_vars = {
        "report_title": report_title,
        "report_date": report_date,
        "compliance_data": compliance_data,
        "device_count": len(compliance_data.keys()),
        "logo": image_as_base64(
            os.path.join(
                settings.STATIC_ROOT, "img", "lookingpoint-logo-new.png"
            ),
            "png",
        ),
        "name": report_title,
    }

    template_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "templates",
        "compliance_report.html",
    )
    template_html = get_template(template_path)
    rendered_template = template_html.render(template_vars)

    file_name = get_valid_file_name(f"{report_title}.pdf")
    TEMP_DIR = tempfile.gettempdir()
    file_path = os.path.join(TEMP_DIR, file_name)
    # header_html = get_template(
    #     os.path.join(
    #         os.path.dirname(os.path.abspath(__file__)),
    #         "templates",
    #         "report_header.html",
    #     )
    # )

    # header_html = header_html.render(template_vars)
    # DocumentService.render_pdf(rendered_template, header_html, output=file_path, options={})
    HTML(string=rendered_template).write_pdf(
        target=file_path,
        stylesheets=[
            CSS(
                os.path.join(settings.STATIC_ROOT, "css/compliance-report.css")
            )
        ],
    )
    if not settings.DEBUG:
        file_path = FileUploadService.upload_file(file_path)
    data = dict(file_path=file_path, file_name=file_name)
    return data
