from collections import defaultdict
from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional

from celery import Task, shared_task
from celery.utils.log import get_task_logger
from django.db.models import Q

from accounts.models import Customer, Provider
from config_compliance.models import ConfigComplianceScore
from config_compliance.services import ComplianceService
from core import utils
from core.models import DeviceInfo
from core.services import DeviceService

logger = get_task_logger(__name__)


@dataclass
class ComplianceTaskFilter:
    customer: Customer = None
    rule_ids: List[int] = field(default_factory=list)
    device_ids: List[int] = field(default_factory=list)


class ComputeConfigCompliance:
    def __init__(
        self,
        provider,
        filter_conditions: Optional[ComplianceTaskFilter] = None,
    ):
        self.provider = provider
        self.filter_conditions = filter_conditions

    def compute(self):
        """
        This is the driver method to execute the complete logic of calculating device compliance stats
        and updating the database.
        """
        if not self.provider.is_configured:
            logger.info(
                f"Provider {self.provider} is not configured. Skipping."
            )
            return
        cw_devices = self.get_devices()
        # Group the devices based on Customer CRM Id
        devices_per_customer: Dict[
            Any, List[Any]
        ] = utils.group_list_of_dictionary_by_key(cw_devices, "customer_id")

        customer_crm_id: int
        customer_devices: List[Any]
        for customer_crm_id, customer_devices in devices_per_customer.items():
            compliance_stats = self.calculate_devices_compliance(
                customer_crm_id, customer_devices
            )
            if not compliance_stats:
                logger.info(f"No stats calculated for {customer_crm_id}")
                continue
            self.write_stats_in_db(customer_crm_id, compliance_stats)
            logger.info(
                f"Compliance stats updated for customer {customer_crm_id}"
            )

    def write_stats_in_db(
        self, customer_crm_id: int, compliance_stats: Dict[int, List[Dict]]
    ) -> None:
        customer_id = None
        try:
            customer_id = (
                Customer.objects.only("id")
                .get(provider_id=self.provider.id, crm_id=customer_crm_id)
                .id
            )
        except Customer.DoesNotExist as e:
            logger.error(
                f"No Customer found {self.provider} and {customer_crm_id}. "
                f"Can not update compliance stats for the customer."
            )
        if customer_id:
            self.update_compliance_stats_in_db(customer_id, compliance_stats)
            self.delete_compliance_stats_from_db(customer_id, compliance_stats)

    def find_device_model_numbers(self, devices: List[Dict]) -> Dict[str, str]:
        model_numbers = []
        for device in devices:
            if device.get("model_number") is not None:
                model_numbers.append(device.get("model_number"))

        device_types_data: Dict[str, str] = DeviceService.get_device_types(
            model_numbers, self.provider.id
        )
        return device_types_data

    def get_devices(self) -> List[Dict]:
        """
        This will return the list of devices from connectwise, which have the config data in
        the DeviceInfo table and also satisfies the customer filter conditions from self.filter_conditions
        Example Response:
        [
        {'id': 29552,
          'device_name': 'LP-SAC-DMVPN01',
          'category_id': 68,
          'category_name': 'Contract - Cisco Subscription',
          'status_id': 1,
          'status': 'Active',
          'customer_id': 2,
          'customer_name': 'LookingPoint, Inc.',
          'location_id': 2,
          'serial_number': 'FLM2138W1QR',
          'model_number': 'ISR4331/K9',
          'notes': ''},
         {'id': 30209,
          'device_name': 'MBA-886-VGW',
          'category_id': 68,
          'category_name': 'Contract - Cisco Subscription',
          'status_id': 1,
          'status': 'Active',
          'customer_id': 2,
          'customer_name': 'Monterey Bay Aquarium',
          'location_id': 2,
          'serial_number': 'FLM1940W0NT',
          'model_number': 'ISR4331-V/K9',
          'notes': ''}
        ]
        """
        devices_configs_queryset = self.get_config_data(self.filter_conditions)
        device_crm_ids = devices_configs_queryset.values_list(
            "device_crm_id", flat=True
        )
        devices = []
        if device_crm_ids:
            # Fetch Devices from ERP Client
            devices: List[Any] = self.provider.erp_client.get_device_by_ids(
                list(device_crm_ids)
            )
        return devices

    def get_customer_device_ids(self, customer: Customer) -> List[int]:
        """
        Get the list of device crm ids for the customer
        """
        crm_id = customer.crm_id
        category_id = self.provider.get_all_device_categories()
        devices = self.provider.erp_client.get_active_devices(
            customer_id=crm_id, category_id=category_id, fields=["id"]
        )
        device_ids = [device.get("id") for device in devices]
        return device_ids

    def get_config_data(
        self, conditions: Optional[ComplianceTaskFilter] = None
    ) -> "Queryset[DeviceInfo]":
        """
        Fetch all the records in DeviceInfo that have config data
        Condition to filter config from device info
        """
        query_conditions = (
            (
                Q(config_meta_data__logicmonitor__has_key="config_id")
                | Q(config_meta_data__solarwinds__has_key="config_id")
            )
            & Q(monitoring_data__is_managed=True)
            & Q(provider_id=self.provider.id)
        )

        if conditions and conditions.customer:
            if conditions.device_ids:
                device_ids = conditions.device_ids
            else:
                device_ids = self.get_customer_device_ids(conditions.customer)
            query_conditions.add(Q(device_crm_id__in=device_ids), Q.AND)

        device_infos_with_config = DeviceInfo.objects.filter(query_conditions)
        return device_infos_with_config

    def calculate_devices_compliance(
        self, customer_crm_id: int, devices: List[Dict]
    ) -> Dict[int, List[Dict]]:
        customer_device_crm_ids: List[Any] = [
            device["id"] for device in devices
        ]
        configurations = DeviceInfo.objects.filter(
            device_crm_id__in=customer_device_crm_ids, provider=self.provider
        )

        # Prepare config data
        device_config_data: dict = {
            device_info.device_crm_id: {
                "config_data": device_info.merged_config,
                "config_id": device_info.config_id,
                "os_version": device_info.monitoring_data.get(
                    "os_version", None
                ),
            }
            for device_info in configurations.iterator()
        }

        valid_compliance_statistics: defaultdict[Any, list]
        invalid_compliance_statistics: defaultdict[Any, list]

        logger.debug(
            f"Calculating compliance score for Provider {self.provider}, "
            f"customer CRM ID {customer_crm_id}"
        )
        filtered_rule_ids = None
        if self.filter_conditions and self.filter_conditions.rule_ids:
            filtered_rule_ids = self.filter_conditions.rule_ids
        overall_compliance_statistics = ComplianceService(
            self.provider,
            customer_crm_id,
            self.find_device_model_numbers(devices),
        ).calculate_compliance_score_for_devices(
            device_config_data, devices, filtered_rule_ids
        )
        return overall_compliance_statistics

    def _update_full_stats(
        self, customer_id: int, compliance_stats: Dict[int, List[Dict]]
    ):
        """
        This method create or updates the compliance stats for a device. This is a full update which
        means all the existing stats for a device will be replaced by the new stats, hence it should be used
        caution. If you need to do partial update use `_update_specific_rule_stats` method.

        """

        for (
            device_id,
            compliance_stat,
        ) in compliance_stats.items():
            (
                config_compliance_score_obj,
                created,
            ) = ConfigComplianceScore.objects.update_or_create(
                provider_id=self.provider.id,
                customer_id=customer_id,
                device_crm_id=device_id,
                defaults=dict(compliance_statistics=compliance_stat),
            )
            if created:
                logger.info(
                    f"ConfigComplianceScore record created. Provider={self.provider}, "
                    f"{ customer_id}, {device_id}"
                )
            else:
                logger.info(
                    f"ConfigComplianceScore record updated. Provider={self.provider}, "
                    f"{ customer_id}, {device_id}"
                )

    def _update_specific_rule_stats(
        self, customer_id: int, compliance_stats: Dict[int, List[Dict]]
    ):
        """
        When only some of the rules stats are updated we can not overwrite the complete compliance_statistics
        instead we need to update the individual rules. This method handles this partial update.

        """
        for (
            device_id,
            compliance_stat,
        ) in compliance_stats.items():
            try:
                compliance_score = ConfigComplianceScore.objects.get(
                    provider_id=self.provider.id,
                    customer_id=customer_id,
                    device_crm_id=device_id,
                )
                # remove the existing stats for the rule
                rule_ids = [rule["id"] for rule in compliance_stat]
                existing_stats = [
                    stat
                    for stat in compliance_score.compliance_statistics
                    if stat["id"] not in rule_ids
                ]
                # update the new stats
                existing_stats.extend(compliance_stat)
                compliance_score.compliance_statistics = existing_stats
                compliance_score.save()
                logger.info(
                    f"ConfigComplianceScore record updated. Provider={self.provider}, "
                    f"{ customer_id}, {device_id} {rule_ids}"
                )

            except ConfigComplianceScore.DoesNotExist:
                ConfigComplianceScore.objects.get_or_create(
                    provider_id=self.provider.id,
                    customer_id=customer_id,
                    device_crm_id=device_id,
                    defaults=dict(compliance_statistics=compliance_stat),
                )
                logger.info(
                    f"ConfigComplianceScore record created. Provider={self.provider}, "
                    f"{ customer_id}, {device_id}"
                )

    def update_compliance_stats_in_db(
        self, customer_id: int, compliance_stats: Dict[int, List[Dict]]
    ) -> None:
        if self.filter_conditions and self.filter_conditions.rule_ids:
            self._update_specific_rule_stats(customer_id, compliance_stats)
        else:
            self._update_full_stats(customer_id, compliance_stats)

    def delete_compliance_stats_from_db(
        self, customer_id: int, compliance_stats: Dict[int, List[Dict]]
    ) -> None:
        if self.filter_conditions:
            logger.info(
                "Skipping delete as filter conditions applicable. "
                "Delete should only be run in case of full sync."
            )
            return
        device_ids = list(compliance_stats.keys())
        # Delete records not coming anymore
        existing_device_ids = set(
            ConfigComplianceScore.objects.filter(
                provider_id=self.provider.id, customer_id=customer_id
            ).values_list("device_crm_id", flat=True)
        )
        new_device_ids = set(device_ids)
        deleted_device_ids = existing_device_ids - new_device_ids
        ConfigComplianceScore.objects.filter(
            provider_id=self.provider.id,
            customer_id=customer_id,
            device_crm_id__in=deleted_device_ids,
        ).delete()
        logger.info(
            f"Deleted ConfigComplianceScore records {deleted_device_ids}"
        )


@shared_task
def store_config_compliance_score(provider_id=None, rule_ids=[]):
    """
    Calculate configuration compliance for all the existing configurations and store in
    """
    query_params = dict(is_active=True)
    filter_conditions = None
    if rule_ids:
        filter_conditions = ComplianceTaskFilter(rule_ids=rule_ids)
    if provider_id:
        query_params.update(dict(id=provider_id))
    for provider in Provider.objects.filter(**query_params):
        logger.info(f"Compliance compute started for {provider}")
        ComputeConfigCompliance(provider, filter_conditions).compute()
        logger.info(f"Compliance compute finished for {provider}")


@shared_task
def rerun_compliance_for_customer(
    provider_id, customer_id, rules_ids: None, device_ids: None
):
    rules_ids = rules_ids or []
    device_ids = device_ids or []
    try:
        provider = Provider.objects.get(id=provider_id, is_active=True)
        customer = Customer.objects.get(id=customer_id)
        filter_conditions = ComplianceTaskFilter(
            customer=customer, rule_ids=rules_ids, device_ids=device_ids
        )
        ComputeConfigCompliance(provider, filter_conditions).compute()
    except Provider.DoesNotExist as e:
        logger.error(f"No provider with {provider_id}")
    except Customer.DoesNotExist as e:
        logger.error(f"No customer found. {provider_id} {customer_id}")
