import reversion
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.db.models import JSONField

from accounts.model_utils import TimeStampedModel


@reversion.register()
class Classification(models.Model):
    name = models.CharField(max_length=150)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)

    class Meta:
        unique_together = ["provider", "name"]


@reversion.register()
class Rule(TimeStampedModel):
    LOW = "LOW"
    MEDIUM = "MEDIUM"
    HIGH = "HIGH"
    LEVEL_CHOICES = ((LOW, LOW), (MEDIUM, MEDIUM), (HIGH, HIGH))

    ROUTED_INTERFACE = "ROUTED_INTERFACE"
    FUNCTION_CHOICES = ((ROUTED_INTERFACE, "ROUTED INTERFACE"),)

    detail = models.TextField()
    description = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=100)
    classification = models.ForeignKey(
        "config_compliance.Classification",
        related_name="rules",
        on_delete=models.CASCADE,
    )
    level = models.CharField(max_length=100, choices=LEVEL_CHOICES)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    customers_not_assigned = models.ManyToManyField(
        "accounts.Customer", blank=True
    )
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="compliance_rules",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    applies_to = ArrayField(
        models.CharField(max_length=100), blank=True, default=list
    )
    applies_to_manufacturers = ArrayField(
        models.CharField(max_length=200), blank=True, default=list
    )
    created_by = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, blank=True, null=True
    )
    is_global = models.BooleanField(default=True)
    is_enabled = models.BooleanField(default=True)
    function = models.CharField(
        max_length=100, choices=FUNCTION_CHOICES, blank=True, null=True
    )
    to_ignore_devices = ArrayField(
        models.IntegerField(), blank=True, default=list
    )
    to_include_devices = ArrayField(
        models.IntegerField(), blank=True, default=list
    )
    apply_not = models.BooleanField(default=False)
    applies_to_ios_version = ArrayField(
        models.CharField(max_length=100), blank=True, default=list
    )
    updated_by = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="rules_udpdated",
    )
    exceptions = ArrayField(
        models.CharField(max_length=1024), blank=True, default=list
    )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        from config_compliance.services import ComplianceService

        params = {}
        params.update({"rule_ids": [self.id]})

        if not self.is_global and self.customer:
            params.update({"customer_id": self.customer.id})
        ComplianceService.refresh_compliance_stats_data(
            self.provider.id, **params
        )


@reversion.register()
class RuleVariable(TimeStampedModel):
    regex_validator = r"(@{{.*?}})"
    inverse_regex_validator = r"(}}@{{)"
    AND = "AND"
    OR = "OR"
    OPTIONS_CHOICES = ((AND, AND), (OR, OR))
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=250)
    variable_code = models.CharField(max_length=32)
    type = models.CharField(max_length=100)
    option = models.CharField(max_length=100, choices=OPTIONS_CHOICES)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    customer = models.ForeignKey(
        "accounts.Customer", blank=True, null=True, on_delete=models.CASCADE
    )
    variable_values = ArrayField(models.TextField(), blank=True, null=True)
    is_global = models.BooleanField(default=True)

    class Meta:
        unique_together = ["provider", "variable_code"]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        from config_compliance.services import ComplianceService

        ComplianceService.refresh_compliance_stats_data(self.provider.id)


@reversion.register()
class RuleVariableCustomerMapping(TimeStampedModel):
    variable_values = ArrayField(models.TextField())
    option = models.CharField(
        max_length=100, choices=RuleVariable.OPTIONS_CHOICES
    )
    variable = models.ForeignKey(
        "config_compliance.RuleVariable",
        related_name="customer_variables",
        on_delete=models.CASCADE,
    )
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    customer = models.ForeignKey("accounts.Customer", on_delete=models.CASCADE)

    class Meta:
        unique_together = ["provider", "customer", "variable"]
        index_together = ["customer", "variable"]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        from config_compliance.services import ComplianceService

        ComplianceService.refresh_compliance_stats_data(self.provider.id)


class ConfigComplianceScore(TimeStampedModel):
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    customer = models.ForeignKey("accounts.Customer", on_delete=models.CASCADE)
    device_crm_id = models.PositiveIntegerField()
    valid_compliance = JSONField(default=dict)
    invalid_compliance = JSONField(default=dict)
    compliance_statistics = JSONField(default=list)

    class Meta:
        unique_together = ["provider", "customer", "device_crm_id"]
        index_together = ["provider", "customer", "device_crm_id"]
        db_table = "config_compliance_score"
