#!/bin/sh

# wait for RabbitMQ server to start
sleep 20

flower -A celery_app --enable-events=False
