import os

import sentry_sdk
from configurations import values
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

from .common import Common

if os.getenv("SENTRY_DSN"):
    sentry_sdk.init(
        dsn=os.getenv("SENTRY_DSN"),
        traces_sample_rate=0.3,
        integrations=[
            DjangoIntegration(),
            CeleryIntegration(),
            RedisIntegration(),
        ],
    )


class Production(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    # GENERAL
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
    SECRET_KEY = os.getenv("DJANGO_SECRET_KEY")
    # Site
    # https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = ["*"]
    INSTALLED_APPS += ("gunicorn",)

    # EMAIL
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
    EMAIL_BACKEND = values.Value(
        "django.core.mail.backends.smtp.EmailBackend", environ_prefix=""
    )
    EMAIL_HOST = values.Value("smtp.office365.com")
    EMAIL_HOST_PASSWORD = values.Value()
    EMAIL_HOST_USER = values.Value()
    EMAIL_PORT = values.Value(587)
    EMAIL_USE_TLS = True
    # https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
    DEFAULT_FROM_EMAIL = values.Value("acela@lookingpoint.com")
    # https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
    EMAIL_SUBJECT_PREFIX = values.Value("[Acela]")
    EMAIL_IMAGE_DIR = "img"
    ADMINS = (("Author", "atul.mishra@velotio.com"),)

    # STORAGES
    # ------------------------------------------------------------------------------
    # https://django-storages.readthedocs.io/en/latest/#installation
    INSTALLED_APPS += ("storages",)  # noqa F405
    # https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
    AWS_ACCESS_KEY_ID = values.Value(environ_prefix="")
    # https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
    AWS_SECRET_ACCESS_KEY = values.Value(environ_prefix="")
    # https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
    AWS_STORAGE_BUCKET_NAME = values.Value(environ_prefix="")
    AWS_QUERYSTRING_AUTH = False

    # STATIC
    # ------------------------
    STATICFILES_STORAGE = (
        "whitenoise.storage.CompressedManifestStaticFilesStorage"
    )

    # MEDIA
    # ------------------------------------------------------------------------------
    DEFAULT_FILE_STORAGE = "custom_storages.MediaStorage"

    # WhiteNoise
    # ------------------------------------------------------------------------------
    # http://whitenoise.evans.io/en/latest/django.html#enable-whitenoise
    MIDDLEWARE = Common.MIDDLEWARE + (
        "whitenoise.middleware.WhiteNoiseMiddleware",
    )

    # SECURITY
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#secure-proxy-ssl-header
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    # https://docs.djangoproject.com/en/dev/ref/settings/#secure-ssl-redirect
    # Everything goes by https, except the /monitoring that goes on http
    SECURE_SSL_REDIRECT = values.BooleanValue(True)
    SECURE_REDIRECT_URLS = ["^"]
    SECURE_REDIRECT_EXEMPT = ["monitoring"]
    # https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-include-subdomains
    SECURE_HSTS_INCLUDE_SUBDOMAINS = values.BooleanValue(True)
    # https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-preload
    SECURE_HSTS_PRELOAD = values.BooleanValue(True)
    # https://docs.djangoproject.com/en/dev/ref/middleware/#x-content-type-options-nosniff
    SECURE_CONTENT_TYPE_NOSNIFF = values.BooleanValue(True)
    # https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
    SECURE_BROWSER_XSS_FILTER = values.BooleanValue(True)
    # https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options

    X_FRAME_OPTIONS = values.Value("DENY")

    # Logging
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "filters": {
            "require_debug_false": {
                "()": "django.utils.log.RequireDebugFalse"
            },
            "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
        },
        "formatters": {
            "simple": {
                "format": "[%(asctime)s] %(levelname)s %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
            "verbose": {
                "format": "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            "console": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "verbose",
            },
            "production_logfile": {
                "level": "INFO",
                "filters": ["require_debug_false"],
                "class": "custom_logging.RotatingFileHandlerWithCompression",
                "filename": "/var/log/django/django_production.log",
                "maxBytes": 1024 * 1024 * 100,  # 100MB
                "backupCount": 15,
                "formatter": "verbose",
            },
            "celery_logfile": {
                "level": "INFO",
                "filters": ["require_debug_false"],
                "class": "custom_logging.RotatingFileHandlerWithCompression",
                "filename": "/var/log/django/celery.log",
                "maxBytes": 1024 * 1024 * 100,  # 100MB
                "backupCount": 15,
                "formatter": "verbose",
            },
        },
        "root": {"level": "DEBUG", "handlers": ["console"]},
        "loggers": {
            "celery.worker": {
                "handlers": ["celery_logfile"],
                "propagate": True,
            },
            "celery": {"handlers": ["console"], "propagate": True},
            "backend": {"handlers": ["production_logfile"], "propagate": True},
            "django": {
                "handlers": ["production_logfile"],
                "level": "INFO",
                "propagate": True,
            },
            "gunicorn.access": {
                "handlers": ["production_logfile"],
                "propogate": True,
                "level": "DEBUG",
            },
        },
    }
