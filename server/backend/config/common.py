import datetime
import os
from distutils.util import strtobool
from os.path import join

import dj_database_url
from configurations import Configuration, values
from corsheaders.defaults import default_headers

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Common(Configuration):
    # APPS
    # ------------------------------------------------------------------------------
    DJANGO_APPS = (
        "django.contrib.admin",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "django.contrib.postgres",
        "django.contrib.humanize",
    )
    THIRD_PARTY_APPS = (
        "rest_framework",  # utilities for rest apis
        "rest_framework.authtoken",  # token authentication
        "django_filters",  # for filtering rest endpoints
        "guardian",  # for managing user groups and permissions
        "corsheaders",  # CORS Settings
        "drf_yasg",  # Swagger Docs
        "reversion",  # version control for model instances.
        "django_prometheus",  # prometheus for logging
    )
    LOCAL_APPS = (
        "accounts",  # manage user accounts
        "core",
        "renewal",  # Device renewal feature
        "service_requests",  # Service Request Feature
        "document",  # SOW Document Feature
        "rate_limiter",  # Rate Limiter
        "config_compliance",  # Config Compliance
        "collector",  # Collector
        "project_management",  # PMO Module
        "sales",  # sales
        "auditlog",  # Log model and object changes in app
    )

    INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

    # MIDDLEWARE
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#middleware
    MIDDLEWARE = (
        "django_prometheus.middleware.PrometheusBeforeMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "corsheaders.middleware.CorsMiddleware",
        "django.middleware.common.CommonMiddleware",
        "accounts.middlewares.provider_middleware",
        "django.middleware.security.SecurityMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
        "accounts.middlewares.JWTAuthenticationMiddleware",
        "accounts.middlewares.CustomerMiddleware",
        "reversion.middleware.RevisionMiddleware",
        "auditlog.middleware.AuditlogMiddleware",
        "django_prometheus.middleware.PrometheusAfterMiddleware",
    )

    # GENERAL
    # ------------------------------------------------------------------------------
    # Set DEBUG to False as a default for safety
    # https://docs.djangoproject.com/en/dev/ref/settings/#debug
    DEBUG = strtobool(os.getenv("DJANGO_DEBUG", "no"))
    # Set TWO_FA_ENABLED to False as a default for disabling the Two Factor Authentication
    TWO_FA_ENABLED = values.BooleanValue(False, environ_name="TWO_FA_ENABLED")
    # Auto Created primary key type
    DEFAULT_AUTO_FIELD = "django.db.models.AutoField"
    # CORS settings
    CORS_ORIGIN_ALLOW_ALL = True
    CORS_ALLOW_HEADERS = default_headers + ("Client-Url", "Multi-FA-Token")
    ALLOWED_HOSTS = ["*"]
    APPEND_SLASH = False
    # Added variable to run sync events method in async loop
    os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
    # Local time zone. Choices are
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # though not all of them may be available with every OS.
    # In Windows, this must be set to your system time zone.
    TIME_ZONE = "UTC"
    # https://docs.djangoproject.com/en/dev/ref/settings/#language-code
    LANGUAGE_CODE = "en-us"
    # If you set this to False, Django will make some optimizations so as not
    # to load the internationalization machinery.
    USE_I18N = False
    # https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
    USE_L10N = True
    # https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
    USE_TZ = True

    # URLS
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
    ROOT_URLCONF = "urls"
    # https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
    WSGI_APPLICATION = "wsgi.application"

    # AUTHENTICATION
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
    # Authentication Backends
    AUTHENTICATION_BACKENDS = (
        "accounts.authentication_backend.CustomAuthBackend",  # this is default
        "guardian.backends.ObjectPermissionBackend",
    )
    # https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
    AUTH_USER_MODEL = "accounts.User"
    # https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
    LOGIN_REDIRECT_URL = "/"

    EMAIL_IMAGE_DIR = "img"
    # DATABASES
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#databases
    DATABASES = {
        "default": dj_database_url.config(
            conn_max_age=int(os.getenv("POSTGRES_CONN_MAX_AGE", 600))
        )
    }
    DATABASES["default"]["ATOMIC_REQUESTS"] = True

    # STATIC
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#static-root
    STATIC_ROOT = os.path.normpath(join(os.path.dirname(BASE_DIR), "static"))
    # https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
    STATICFILES_DIRS = []

    # https://docs.djangoproject.com/en/dev/ref/settings/#static-url
    STATIC_URL = "/static/"
    # https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
    STATICFILES_FINDERS = (
        "django.contrib.staticfiles.finders.FileSystemFinder",
        "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    )

    # MEDIA
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#media-root
    MEDIA_ROOT = join(os.path.dirname(BASE_DIR), "media")
    # https://docs.djangoproject.com/en/dev/ref/settings/#media-url
    MEDIA_URL = "/media/"

    # TEMPLATES
    # ------------------------------------------------------------------------------
    TEMPLATE_DIR = os.path.join(BASE_DIR, "templates")
    # https://docs.djangoproject.com/en/dev/ref/settings/#templates
    TEMPLATES = [
        {  # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
            "DIRS": [TEMPLATE_DIR],
            "APP_DIRS": True,
            "OPTIONS": {
                # https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
                "debug": DEBUG,
                # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                ],
            },
        }
    ]

    # PASSWORDS
    # ------------------------------------------------------------------------------
    # https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
    PASSWORD_HASHERS = [
        # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
        "django.contrib.auth.hashers.Argon2PasswordHasher",
        "django.contrib.auth.hashers.PBKDF2PasswordHasher",
        "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
        "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
        "django.contrib.auth.hashers.BCryptPasswordHasher",
    ]
    # Password Validation
    # https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
    AUTH_PASSWORD_VALIDATORS = [
        {
            "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"
        },
        {
            "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"
        },
        {
            "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
        },
        {"NAME": "accounts.password_validation.NumberValidator"},
        {"NAME": "accounts.password_validation.UppercaseValidator"},
        {"NAME": "accounts.password_validation.LowercaseValidator"},
        {"NAME": "accounts.password_validation.SymbolValidator"},
    ]

    # THIRD PARTY APP SETTINGS
    # ------------------------------------------------------------------------------
    # DJANGO REST FRAMEWORK
    REST_FRAMEWORK = {
        "DEFAULT_PAGINATION_CLASS": "core.pagination.CustomPagination",
        "PAGE_SIZE": int(os.getenv("DJANGO_PAGINATION_LIMIT", 10)),
        "DATETIME_FORMAT": "%Y-%m-%dT%H:%M:%S%z",
        "DEFAULT_RENDERER_CLASSES": (
            "rest_framework.renderers.JSONRenderer",
            "rest_framework.renderers.BrowsableAPIRenderer",
        ),
        "DEFAULT_PERMISSION_CLASSES": [
            "rest_framework.permissions.IsAuthenticated"
        ],
        "DEFAULT_AUTHENTICATION_CLASSES": (
            "accounts.jwt_authentication.CustomJSONWebTokenAuthentication",
        ),
        "NON_FIELD_ERRORS_KEY": "message",
    }

    # DJANGO-GUARDIAN SETTINGS
    GUARDIAN_RAISE_403 = True  # raise django.core.exceptions.PermissionDenied
    ANONYMOUS_USER_NAME = None  # Disable Anonymous User object Permissions

    # DJANGO REST FRAMEWORK JWT SETTINGS
    JWT_AUTH = {
        "JWT_ENCODE_HANDLER": "rest_framework_jwt.utils.jwt_encode_handler",
        "JWT_DECODE_HANDLER": "rest_framework_jwt.utils.jwt_decode_handler",
        "JWT_PAYLOAD_HANDLER": "accounts.utils.custom_jwt_payload_handler",
        "JWT_PAYLOAD_GET_USER_ID_HANDLER": "rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler",
        "JWT_PAYLOAD_GET_USERNAME_HANDLER": "accounts.utils.jwt_get_username_from_payload_handler",
        "JWT_EXPIRATION_DELTA": datetime.timedelta(
            seconds=int(os.environ.get("DJANGO_JWT_EXPIRATION_DELTA", 60 * 15))
        ),
        "JWT_VERIFY": True,
        "JWT_VERIFY_EXPIRATION": values.BooleanValue(
            True, environ_name="JWT_VERIFY_EXPIRATION"
        ),
        "JWT_AUTH_HEADER_PREFIX": "Bearer",
        "JWT_ALLOW_REFRESH": True,
        "JWT_REFRESH_EXPIRATION_DELTA": datetime.timedelta(
            seconds=int(
                os.environ.get(
                    "DJANGO_JWT_REFRESH_EXPIRATION_DELTA", 3600 * 24 * 3
                )
            )
        ),
    }
    USER_INACTIVITY_DELTA = values.Value(
        3600 * 8, environ_name="USER_INACTIVITY_DELTA"
    )
    # LOCAL APP SETTINGS AND CONSTANTS
    # ------------------------------------------------------------------------------
    SUPER_USER_PORTAL_NAME = "accelavar"
    HTTP_PROTOCOL = values.Value("http")
    PASSWORD_RESET_CONFIRM_URL = "set-password/{uid}/{token}"
    UI_LOGIN_URL = "login"
    SALE_ACTIVITY_URL = "sales-activity?showAllActivities=false"
    MAX_ACTIVATION_MAILS_COUNT = values.Value(5)
    SITE_NAME = "lookingpoint.com"
    ADMIN_PORTAL_URL = values.Value(
        "admin.acela.io", environ_name="ADMIN_PORTAL_URL", environ_prefix=None
    )
    API_SERVER_URL = values.Value(
        "dev.acela.io", environ_name="API_SERVER_URL", environ_prefix=None
    )
    RESTRICTED_URLS = [ADMIN_PORTAL_URL, API_SERVER_URL, SITE_NAME]
    CISCO_MISSING_DATA_MAX_RETRY = values.Value(5)
    # Default country code for phone numbers
    DEFAULT_COUNTRY_CODE = "1"
    DEFAULT_REGION = "US"

    # Date Formats
    ISO_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
    MM_DD_YYYY_FORMAT = "%m/%d/%Y"

    # Collector Settings
    COLLECTOR_TOKEN_LENGTH = 32
    COLLECTOR_APP_NAME = "AcelaCollector.msi"

    # Service Names
    LOGICMONITOR = "logicmonitor"
    SOLARWINDS = "solarwinds"

    # Caching
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": os.environ.get(
                "CELERY_BROKER_URL", "redis://redis:6379"
            ),
            "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
            "TIMEOUT": 300,
        }
    }

    # SMARTNET RECEIVING NOTIFICATION
    DISABLE_CUSTOMER_SMARTNET_RECEIVING_NOTIFICATION = values.BooleanValue(
        False, environ_name="DISABLE_CUSTOMER_SMARTNET_RECEIVING_NOTIFICATION"
    )
    # Connectwise settings

    CONNECTWISE_CALLBACK_BASE_URL = values.Value(
        "https://dev.acela.io/api/v1/cw-callbacks/providers/",
        environ_name="CONNECTWISE_CALLBACK_BASE_URL",
        environ_prefix=None,
    )
    CONNECTWISE_DOCUMENT_TITLE_MAX_LENGTH = 100

    AUTHY_CALLBACK_URL = values.Value(
        "https://dev.acela.io/api/v1/callback",
        environ_name="AUTHY_CALLBACK_URL",
        environ_prefix=None,
    )

    # Project Management Module settings
    MILESTONE_DONE_STATUS_COLOR = "#176f07"
    ACTIVITY_STATUS_DONE_STATUS_COLOR = "#176f07"
    USER_DEFAULT_PROFILE_PIC = "https://acella-images.s3-us-west-2.amazonaws.com/media/user-filled-shape.svg"
    LOOKINGPOINT_LOGO = "https://acella-images.s3.us-west-2.amazonaws.com/media/lookingpoint-logo-new.png"

    TEST_EMAILS = ["rhushimahalle@gmail.com"]
    ENV_NAME = values.Value(environ_name="ENV_NAME", environ_prefix=None)
    TEST_ENV_NAME = "TestEnv"

    # PDFSHIFT
    PDFSHIFT_API_KEY = values.Value(
        environ_name="PDFSHIFT_API_KEY", environ_prefix=None
    )

    TEST_EMAILS = values.Value(
        "", environ_name="TEST_EMAILS", environ_prefix=""
    ).split(",")

    # MAX SIZE OF FILES IN BYTES TO ALLOW IN FILE UPLOAD
    # Currently 5 MB
    DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880
