from django_filters import Filter


class ListFilter(Filter):
    def filter(self, qs, value):
        if not value:
            return qs

        # For django-filter versions < 0.13, use lookup_type instead of lookup_expr
        self.lookup_expr = "in"
        self.distinct = True
        values = list(value.split(","))

        field_name: str = self.field_name
        qs_filtered_for_null_values = None
        qs_filtered_for_non_null_values = None

        filtered_values = []
        for value in values:
            if isinstance(value, str) and value.strip().lower() == "null":
                qs_filtered_for_null_values = qs.filter(**{field_name: None})
            else:
                filtered_values.append(value)

        qs_filtered_for_non_null_values = (
            super(ListFilter, self).filter(qs, filtered_values)
            if filtered_values
            else None
        )

        if qs_filtered_for_null_values and qs_filtered_for_non_null_values:
            return qs_filtered_for_non_null_values.union(
                qs_filtered_for_null_values
            )
        elif (
            qs_filtered_for_null_values and not qs_filtered_for_non_null_values
        ):
            return qs_filtered_for_null_values
        elif (
            qs_filtered_for_non_null_values and not qs_filtered_for_null_values
        ):
            return qs_filtered_for_non_null_values
        elif (
            not qs_filtered_for_non_null_values
            and not qs_filtered_for_null_values
        ):
            # Return empty queryset
            return qs.none()


class BooleanListFilter(Filter):
    def filter(self, qs, value):
        if not value:
            return qs

        # For django-filter versions < 0.13, use lookup_type instead of lookup_expr
        self.lookup_expr = "in"
        self.distinct = True
        values = []
        filters = value.split(",")
        filters = [value.lower() for value in filters]
        if "false" in filters:
            values.append(False)
        if "true" in filters:
            values.append(True)
        if not values:
            return qs
        return super(BooleanListFilter, self).filter(qs, values)


class ArrayFieldContainsFilter(Filter):
    def filter(self, qs, value):
        if not value:
            return qs

        # For django-filter versions < 0.13, use lookup_type instead of lookup_expr
        self.lookup_expr = "contains"
        self.distinct = True
        values = value.split(",")
        values = list(filter(None, values))
        return super(ArrayFieldContainsFilter, self).filter(qs, values)
