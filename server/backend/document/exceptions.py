from core.exceptions import InvalidConfigurations
from django.core.exceptions import ObjectDoesNotExist


class OperationDashboardSettingsNotConfigured(InvalidConfigurations):
    default_detail = (
        "Operation dashboard settings not configured for the provider"
    )


class SOWQuoteStatusSettingsNotConfiguredAPIException(InvalidConfigurations):
    default_detail = (
        "SOW Quote Status Settings not configured for the Provider."
    )


class SOWQuoteStatusSettingsNotConfigured(Exception):
    """Raise this exception when SOW quote status setting is not configured for Provider"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ServiceTicketBoardAndStatusMappingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Service ticket board and status mapping in "
        "Operation Email Template Settings "
        "not configured for the Provider!"
    )


class OpportunityStatusesNotConfigured(InvalidConfigurations):
    default_detail = (
        "Opportunity statuses in Operation Email Template Settings "
        "not configured for the Provider!"
    )


class OpportunityStagesNotConfigured(InvalidConfigurations):
    default_detail = (
        "Opportunity stages in Operation Email Template Settings "
        "not configured for the Provider!"
    )


class POCustomFieldNotConfigured(InvalidConfigurations):
    default_detail = (
        "Purchase order custom field in Operations Dashboard Settings "
        "not configured for the Provider!"
    )


class SOWVendorOnboardingSettingNotConfigured(InvalidConfigurations):
    default_detail = "SOW vendor onboarding not configured for the Provider!"


class SoWDocumentSettingNotConfigured(InvalidConfigurations):
    default_detail = "SoW document settings not configured for the provider!"


class DefaultDocumentCreatorRoleSettingNotConfigured(InvalidConfigurations):
    default_detail = "Default Document Creator Role Setting not configured for the Provider!"


class FireReportPartnerSiteNotConfigured(InvalidConfigurations):
    default_detail = (
        "Fire Report Site Setting not configured for the Provider!"
    )


class FireReportSettingNotConfigured(InvalidConfigurations):
    default_detail = "Fire Report Setting not configured for the Provider!"


class PartnerSiteNotFound(ObjectDoesNotExist):
    """
    Requested Partner Site does not exist!
    """


class AutomatedWeeklyRecapSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Automated Weekly Recap Setting not configured for the Provider!"
    )


class Client360DashboardSettingNotConfigured(InvalidConfigurations):
    default_detail = (
        "Client360 Dashboard Setting not configured for the Provider!"
    )
