from rest_framework_csv.renderers import CSVRenderer
import csv
from io import StringIO


class FireReportCSVRenderer(CSVRenderer):

    headers = [
        "opportunity_number",
        "opportunity_name",
        "customer_po",
        "po_number",
        "sales_order_crm_id",
        "manufacturer",
        "part_number",
        "description",
        "serial_number",
        "ship_date",
        "shipping_location",
        "carrier",
        "tracking_numbers",
        "received_status",
    ]

    labels = {
        "opportunity_number": "Opportunity Number",
        "opportunity_name": "Opportunity Name",
        "customer_po": "Customer PO",
        "po_number": "LP PO",
        "sales_order_crm_id": "Sales Order Number",
        "manufacturer": "Manufacturer",
        "part_number": "Part Number",
        "description": "Description",
        "serial_number": "Serial Number",
        "ship_date": "Ship Date",
        "shipping_location": "Shipping Location",
        "carrier": "Carrier",
        "tracking_numbers": "Tracking Numbers",
        "received_status": "Received Status",
    }
