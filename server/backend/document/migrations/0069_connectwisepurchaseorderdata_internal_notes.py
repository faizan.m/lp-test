# Generated by Django 2.0.5 on 2022-02-01 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0068_operationdashboardsettings_ticket_status_ids'),
    ]

    operations = [
        migrations.AddField(
            model_name='connectwisepurchaseorderdata',
            name='internal_notes',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
