# Generated by Django 2.0.5 on 2021-12-16 10:34

from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0101_auto_20211206_1046'),
        ('document', '0054_auto_20210818_1549'),
    ]

    operations = [
        migrations.CreateModel(
            name='OperationDashboardSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('business_unit_id', models.PositiveIntegerField()),
                ('service_boards', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), size=None)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('provider', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='accounts.Provider')),
            ],
        ),
        migrations.AlterField(
            model_name='sowdocument',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documents', to=settings.AUTH_USER_MODEL),
        ),
    ]
