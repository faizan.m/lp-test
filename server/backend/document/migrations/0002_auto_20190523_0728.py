# Generated by Django 2.0.5 on 2019-05-23 07:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sowdocument',
            old_name='opportunity_id',
            new_name='quote_id',
        ),
    ]
