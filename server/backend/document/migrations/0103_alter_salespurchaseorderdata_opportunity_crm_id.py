# Generated by Django 3.2.14 on 2023-02-10 06:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0102_add_cloud_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salespurchaseorderdata',
            name='opportunity_crm_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='serviceticketlinkedoppo', to='document.connectwiseserviceticket', to_field='opportunity_crm_id'),
        ),
    ]
