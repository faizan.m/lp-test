# Generated by Django 2.0.5 on 2019-06-03 12:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    def call_command_to_create_sow_template_categories(apps, schema_editor):
        from django.core.management import call_command
        call_command("createsowcategory")

    dependencies = [
        ('document', '0003_sowdocument_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='SOWCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='sowtemplate',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='document.SOWCategory'),
        ),
        migrations.RunPython(call_command_to_create_sow_template_categories),
    ]
