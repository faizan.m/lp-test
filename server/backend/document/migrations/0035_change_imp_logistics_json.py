# Generated by Django 2.0.5 on 2020-01-20 11:25

from django.db import migrations


class Migration(migrations.Migration):
    def update_implementation_logistics_section(apps, schema_editor):
        def _update_json_with_request_description(existing_data):
            implementation_logistics = {
                "section_label": "Implementation Logistics",
                "ordering": 3,
                "visible_in": ["Fixed Fee", "T & M"],
                "sections": [
                    {
                        "column1": {
                            "ordering": 1,
                            "label": "Column 1 Name",
                            "value": "Design",
                            "type": "TEXTBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                        "delivery_model": {
                            "section_label": "Delivery Mode Section",
                            "ordering": 2,
                            "label": "Delivery Mode",
                            "value": "",
                            "type": "RADIOBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "options": ["Onsite", "Remote"],
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                        "detail": {
                            "section_label": "Detail's Section",
                            "ordering": 3,
                            "label": "Detail",
                            "value": "",
                            "type": "TEXTAREA",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "RIGHT",
                        },
                    },
                    {
                        "delivery_model": {
                            "section_label": "Delivery Mode Section",
                            "ordering": 2,
                            "label": "Delivery Mode",
                            "value": "",
                            "type": "RADIOBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "options": ["Onsite", "Remote"],
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                        "detail": {
                            "section_label": "Detail's Section",
                            "ordering": 3,
                            "label": "Detail",
                            "value": "",
                            "type": "TEXTAREA",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "RIGHT",
                        },
                        "column2": {
                            "ordering": 1,
                            "label": "Column 2 Name",
                            "value": "Design",
                            "type": "TEXTBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                    },
                    {
                        "delivery_model": {
                            "section_label": "Delivery Mode Section",
                            "ordering": 2,
                            "label": "Delivery Mode",
                            "value": "",
                            "type": "RADIOBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "options": ["Onsite", "Remote"],
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                        "detail": {
                            "section_label": "Detail's Section",
                            "ordering": 3,
                            "label": "Detail",
                            "value": "",
                            "type": "TEXTAREA",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "RIGHT",
                        },
                        "column3": {
                            "ordering": 1,
                            "label": "Column 3 Name",
                            "value": "Design",
                            "type": "TEXTBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                    },
                    {
                        "delivery_model": {
                            "section_label": "Delivery Mode Section",
                            "ordering": 2,
                            "label": "Delivery Mode",
                            "value": "",
                            "type": "RADIOBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "options": ["Onsite", "Remote"],
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                        "detail": {
                            "section_label": "Detail's Section",
                            "ordering": 3,
                            "label": "Detail",
                            "value": "",
                            "type": "TEXTAREA",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "RIGHT",
                        },
                        "column4": {
                            "ordering": 1,
                            "label": "Column 4 Name",
                            "value": "Design",
                            "type": "TEXTBOX",
                            "input_type": "STRING",
                            "is_required": "true",
                            "is_label_editable": "false",
                            "position": "LEFT",
                        },
                    },
                ],
            }
            try:
                for idx, section in enumerate(implementation_logistics["sections"]):
                    section[f"column{idx + 1}"]["value"] = existing_data[
                        f"implementation_logistics_column{idx + 1}"
                    ][f"column{idx + 1}"]["value"]
                    section[f"detail"]["value"] = existing_data[
                        f"implementation_logistics_column{idx + 1}"
                    ][f"detail"]["value"]
                    section[f"delivery_model"]["value"] = existing_data[
                        f"implementation_logistics_column{idx + 1}"
                    ][f"delivery_model"]["value"]
                    existing_data.pop(f"implementation_logistics_column{idx + 1}", None)
            except KeyError:
                pass
            existing_data["implementation_logistics"] = implementation_logistics
            return existing_data

        SOWDocument = apps.get_model("document", "SOWDocument")
        SOWTemplate = apps.get_model("document", "SOWTemplate")

        for doc in SOWDocument.objects.all():
            json_config = _update_json_with_request_description(doc.json_config)
            doc.json_config = json_config
            doc.save()

        for template in SOWTemplate.objects.all():
            json_config = _update_json_with_request_description(template.json_config)
            template.json_config = json_config
            template.save()

    dependencies = [("document", "0034_auto_20200113_0533")]

    operations = [migrations.RunPython(update_implementation_logistics_section)]
