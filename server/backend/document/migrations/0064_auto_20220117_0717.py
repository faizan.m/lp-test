# Generated by Django 2.0.5 on 2022-01-17 07:17

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0063_auto_20220112_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='connectwisepurchaseorderdata',
            name='purchase_order_status_id',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name='connectwisepurchaseorderdata',
            name='purchase_order_status_name',
            field=models.CharField(default='', max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='operationdashboardsettings',
            name='purchase_order_status_ids',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.PositiveIntegerField(), blank=True, default=[], size=None),
        ),
        migrations.AlterField(
            model_name='ingrampurchaseorderdata',
            name='po_number',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ingram_line_items_data', to='document.ConnectwisePurchaseOrder'),
        ),
    ]
