import json
import os
from datetime import datetime, timedelta
from typing import List, Optional, Tuple

import reversion
from box import Box
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import JSONField
from django.utils import timezone
from django.utils.functional import cached_property
from model_utils import Choices

from accounts.model_utils import TimeStampedModel
from custom_storages import get_storage_class
from utils import get_app_logger

logger = get_app_logger(__name__)


class SOWBaseTemplate:
    base_dir = os.path.join("configs", "base_template", "providers", "default")

    @classmethod
    def json_config(cls):
        configs_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            cls.base_dir,
            f"sow_base_template.json",
        )
        try:
            with open(configs_file) as config:
                json_config = json.load(config)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        return json_config

    @classmethod
    def change_request_config(cls):
        configs_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            cls.base_dir,
            f"change_request_sow.json",
        )
        try:
            with open(configs_file) as config:
                change_request_config = json.load(config)
        except (FileNotFoundError, OSError) as e:
            logger.error(e)
            return {}
        return change_request_config


@reversion.register()
class SOWTemplate(TimeStampedModel):
    name = models.CharField(max_length=60)
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    json_config = JSONField(null=True, blank=True)
    category = models.ForeignKey(
        "document.SOWCategory", on_delete=models.CASCADE
    )
    service_catalog_category = models.ForeignKey(
        "document.ServiceCategory", on_delete=models.SET_NULL, null=True
    )
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="created_sow_templates",
    )
    updated_by = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="updated_sow_templates",
    )
    is_disabled = models.BooleanField(default=False)
    major_version = models.PositiveIntegerField(
        null=True, blank=True, default=1
    )
    minor_version = models.PositiveIntegerField(
        null=True, blank=True, default=0
    )

    class Meta:
        unique_together = ("name", "provider")
        ordering = ["-updated_on"]

    @cached_property
    def linked_service_catalog(self):
        try:
            return self.linked_catalog
        except ServiceCatalog.DoesNotExist:
            return None

    @cached_property
    def version(self):
        ver = f"{self.major_version}.{self.minor_version}"
        return ver


@reversion.register()
class SOWDocument(TimeStampedModel):
    base_dir = os.path.join("configs", "base_template", "providers", "default")
    GENERAL = "Fixed Fee"
    T_AND_M = "T & M"
    CHANGE_REQUEST = "Change Request"
    SOW_TEMPLATE_CHOICES = Choices(
        (GENERAL, GENERAL),
        (T_AND_M, T_AND_M),
        (CHANGE_REQUEST, CHANGE_REQUEST),
    )

    name = models.CharField(max_length=150)
    provider = models.ForeignKey(
        "accounts.Provider", related_name="documents", on_delete=models.CASCADE
    )
    json_config = JSONField(null=True, blank=True)
    customer = models.ForeignKey(
        "accounts.Customer", related_name="documents", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        "accounts.User", related_name="documents", on_delete=models.CASCADE
    )
    author = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="created_sow_docs",
    )
    updated_by = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        related_name="updated_sow_docs",
    )
    quote_id = models.IntegerField(unique=False)
    forecast_id = models.IntegerField(unique=False, null=True)
    category = models.ForeignKey(
        "document.SOWCategory", on_delete=models.CASCADE
    )
    doc_type = models.CharField(choices=SOW_TEMPLATE_CHOICES, max_length=30)
    major_version = models.PositiveIntegerField(
        null=True, blank=True, default=1
    )
    minor_version = models.PositiveIntegerField(
        null=True, blank=True, default=0
    )
    margin = models.FloatField(null=True)

    class Meta:
        ordering = ["-updated_on"]

    @classmethod
    def get_doc_template_path(cls, doc_type, file_type):
        _file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            cls.base_dir,
            f"{doc_type}-{file_type.lower()}.html",
        )
        return _file_path

    @classmethod
    def get_service_detail_template_path(cls):
        _file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            cls.base_dir,
            "service_detail_pdf.html",
        )
        return _file_path

    @cached_property
    def version(self):
        ver = f"{self.major_version}.{self.minor_version}"
        return ver


class SOWCategory(models.Model):
    CHANGE_REQUEST = "Change Request"
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


############################################
# Models for Service Catalog Feature
############################################


class ServiceTechnologyType(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="service_technology_types",
        on_delete=models.CASCADE,
    )
    service_type = models.ForeignKey(
        "document.SOWCategory",
        related_name="technology_types",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100)
    is_disabled = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class ServiceCategory(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="service_categories",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100)
    is_disabled = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class ServiceCatalog(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="service_catalogs",
        on_delete=models.CASCADE,
    )
    service_name = models.CharField(max_length=60)
    service_type = models.ForeignKey(
        "document.SOWCategory",
        related_name="service_catalogs",
        on_delete=models.CASCADE,
    )
    service_category = models.ForeignKey(
        "document.ServiceCategory",
        related_name="service_catalogs",
        on_delete=models.SET_NULL,
        null=True,
    )
    service_technology_types = models.ManyToManyField(
        "document.ServiceTechnologyType",
        related_name="service_catalogs",
        blank=True,
    )
    is_template_present = models.BooleanField(default=False)
    is_disabled = models.BooleanField(default=False)
    linked_template = models.OneToOneField(
        "document.SOWTemplate",
        related_name="linked_catalog",
        on_delete=models.SET_NULL,
        null=True,
    )
    notes = models.TextField(blank=True)

    def __str__(self):
        return self.service_name

    class Meta:
        unique_together = ("provider", "service_name")


@reversion.register()
class SOWDocumentSettings(TimeStampedModel):
    """
    SOW Document settings for each provider
    """

    provider = models.OneToOneField(
        "accounts.Provider",
        related_name="sow_doc_settings",
        on_delete=models.CASCADE,
    )
    fixed_fee_variable = models.FloatField(
        help_text=(
            "This variable is a percentage and will be added as an incremental charge for "
            "risk variance in fixed fee projects",
        )
    )
    engineering_hourly_cost = models.FloatField(
        help_text=(
            "This is a variable which defines hourly cost per engineering hour",
        )
    )
    pm_hourly_cost = models.FloatField(
        help_text=("This is a variable which defines hourly cost per PM hour",)
    )
    engineering_hourly_rate = models.FloatField(
        help_text=(
            "This is a variable which defines engineering hourly rate in $",
        )
    )
    after_hours_rate = models.FloatField(
        help_text=(
            "This is a variable which defines after hours hourly rate in $",
        )
    )
    after_hours_cost = models.FloatField(
        help_text=("This variable defines the after hours cost in $",)
    )
    project_management_hourly_rate = models.FloatField(
        help_text=(
            "This is a variable which defines project management hourly rate in $",
        )
    )
    project_management_t_and_m = models.TextField()
    project_management_fixed_fee = models.TextField()
    terms_fixed_fee = models.TextField()
    terms_t_and_m = models.TextField()
    total_no_of_sites_statement = models.TextField(
        null=True,
        blank=True,
        default="Up to ##{no_of_sites} sites will be included in this project.",
    )
    total_no_of_cutovers_statement = models.TextField(
        null=True,
        blank=True,
        default="Up to ##{no_of_cutovers} cutovers has been accounted for in this project. "
        "Additional cutover requests will require a change order and associated costs or hours.",
    )
    change_request_t_and_m_terms = models.TextField(null=True)
    change_request_fixed_fee_terms = models.TextField(null=True)
    change_request_t_and_m_terms_markdown = models.TextField(null=True)
    change_request_fixed_fee_terms_markdown = models.TextField(null=True)
    engineering_hours_description = models.CharField(
        max_length=500, null=True, default="Engineering hours"
    )
    after_hours_description = models.CharField(
        max_length=500, null=True, default="Engineering after hours"
    )
    project_management_hours_description = models.CharField(
        max_length=500, null=True, default="Project management hours"
    )
    integration_technician_hourly_cost = models.FloatField(null=True)
    integration_technician_hourly_rate = models.FloatField(null=True)
    integration_technician_description = models.CharField(
        max_length=500, null=True, default="Integration Technician"
    )

    def __str__(self):
        return f"{self.provider}"


class DashboardMetricData(models.Model):
    EXPECTED_ENGINEERING_HOURS = "EXPECTED_ENGINEERING_HOURS"
    METRIC_CHOICES = (
        (EXPECTED_ENGINEERING_HOURS, "Expected Engineering Hours"),
    )
    provider = models.ForeignKey("accounts.Provider", on_delete=models.CASCADE)
    metric = models.CharField(max_length=300, choices=METRIC_CHOICES)
    value = models.FloatField()
    timestamp = models.DateTimeField()

    class Meta:
        unique_together = ("metric", "provider", "timestamp")


class CustomerNotes(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="customer_notes",
    )
    customer = models.ForeignKey(
        "accounts.Customer", related_name="notes", on_delete=models.CASCADE
    )
    last_updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING
    )
    body = models.TextField()
    description = models.TextField()
    notes_object = JSONField()
    opportunity_ids = ArrayField(models.IntegerField(), null=True)
    contact_ids = ArrayField(models.IntegerField(), null=True)
    site_ids = ArrayField(models.IntegerField(), null=True)
    technology_ids = ArrayField(models.IntegerField(), null=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.body


class IngramAPISetting(TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="ingram_api_settings",
    )
    client_id = models.CharField(max_length=100)
    client_secret = models.CharField(max_length=100)
    ingram_customer_id = models.CharField(max_length=100)


class OperationEmailTemplateSettings(TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="operation_email_template_settings",
    )
    purchase_order_status_ids = ArrayField(models.IntegerField())
    extra_config = JSONField(default=dict)
    contact_type_connectwise_id = models.PositiveIntegerField(
        blank=True,
        null=True,
        help_text="Connectwise id for order tracking contact type.",
    )
    ticket_board_and_status_mapping = JSONField(default=list)
    opportunity_statuses = JSONField(default=list)
    opportunity_stages = JSONField(default=list)

    @property
    def default_extra_config(self):
        extra_config = {
            "email_settings": {
                "email_intro": "<p>Thank you for your recent product purchase. "
                "Items are on the way! Please see the shipping and "
                "tracking information below:</p>",
                "email_intro_markdown": "Thank you for your recent product purchase. "
                "Items are on the way! Please see the shipping and "
                "tracking information below:",
                "smartnet_intro": "<p>Below are Smart Net Total Care Contracts tied "
                "to the products above. Contracts will be generated after "
                "all products have shipped.</p>",
                "smartnet_intro_markdown": "Below are Smart Net Total Care Contracts tied "
                "to the products above. Contracts will be generated after "
                "all products have shipped.",
                "ending_paragraph": "<p><em>Please note: Shipping dates are estimated by "
                "manufacturer and subject to change.</em></p>",
                "ending_paragraph_markdown": "Please note: Shipping dates are estimated by "
                "manufacturer and subject to change.",
            },
            "product_category_mapping": {
                "licences": {
                    "carrier": "VIRTUAL",
                    "sub_strings": ["L-*", "*-5Y", "*-2Y"],
                },
                "contracts": {"carrier": "NA", "sub_strings": ["CON-*"]},
            },
            "carrier_tracking_url": {
                "UPS": {
                    "tracking_url": "http://wwwapps.ups.com/WebTracking/processInputRequest?"
                    "sort_by=status&tracknums_displ ayed=1&TypeOfInquiryNumber=T"
                    "&loc=en_US&InquiryNumb er1=%tracknum%&track.x=0&track.y=0",
                    "connectwise_shipper": "",
                }
            },
            "part_number_exclusion": ["shipping"],
            "received_status_mapping": {
                "received_status_id": 2,
                "mapped_status": "shipped",
            },
        }
        return extra_config

    @cached_property
    def settings(self):
        if self.extra_config:
            return Box(self.extra_config)
        else:
            return Box(self.default_extra_config)


class OperationDashboardSettings(TimeStampedModel):
    provider = models.OneToOneField(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    business_unit_id = models.PositiveIntegerField()
    service_boards = ArrayField(base_field=models.PositiveIntegerField())
    purchase_order_status_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, blank=True
    )
    ticket_status_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, blank=True
    )
    po_custom_field_id = models.PositiveIntegerField(null=True)
    service_ticket_create_date = models.DateField(null=True, default=None)


class ConnectwiseServiceTicketParseFailed(models.Model):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    ticket_crm_id = models.PositiveIntegerField()
    summary = models.CharField(max_length=200)
    is_ignored = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.ticket_crm_id}"


class ConnectwiseServiceTicket(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    ticket_crm_id = models.PositiveIntegerField()
    owner_crm_id = models.PositiveIntegerField(null=True)
    owner = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="+", null=True
    )
    summary = models.CharField(max_length=200)
    service_board_crm_id = models.PositiveIntegerField()
    status_crm_id = models.PositiveIntegerField()
    status_name = models.CharField(max_length=200)
    company_crm_id = models.PositiveIntegerField()
    company_name = models.CharField(max_length=500)
    connectwise_last_data_update = models.DateTimeField()
    additional_data = JSONField(default=dict)
    last_ticket_note = models.TextField(default="", null=True, blank=True)
    opportunity_crm_id = models.PositiveIntegerField(null=True, unique=True)
    opportunity_name = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
        indexes = [
            models.Index(fields=["provider", "owner"]),
        ]

    def __str__(self):
        return self.summary


class ConnectwisePurchaseOrder(models.Model):
    po_number = models.CharField(max_length=100, primary_key=True)
    ticket = models.ForeignKey(
        "document.ConnectwiseServiceTicket",
        related_name="purchase_orders",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.po_number


class ConnectwisePurchaseOrderData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    crm_id = models.PositiveIntegerField()
    po_number = models.OneToOneField(
        "document.ConnectwisePurchaseOrder",
        related_name="details",
        on_delete=models.CASCADE,
    )
    customer_company_name = models.CharField(
        max_length=500, null=True, blank=True
    )
    customer_company_id = models.PositiveIntegerField(null=True)
    vendor_company_name = models.CharField(max_length=500)
    vendor_company_id = models.PositiveIntegerField()
    additional_data = JSONField(default=dict)
    purchase_order_status_id = models.PositiveIntegerField(
        null=True, default=0
    )
    purchase_order_status_name = models.CharField(
        max_length=50, null=True, default=""
    )
    internal_notes = models.TextField(null=True, blank=True, default="")
    po_date = models.DateTimeField(null=True, default=None)
    site_name = models.CharField(
        max_length=500, blank=True, null=True, default=""
    )
    site_id = models.PositiveIntegerField(null=True, default=None)
    sales_order = models.ForeignKey(
        "document.SalesPurchaseOrderData",
        null=True,
        on_delete=models.SET_NULL,
        related_name="linked_pos",
    )
    location_id = models.PositiveIntegerField(null=True, default=None)
    business_unit = models.PositiveIntegerField(null=True, default=None)
    shipment_date = models.DateTimeField(null=True, default=None)
    total = models.FloatField(null=True)
    entered_by = models.CharField(max_length=200, blank=True, null=True)
    vendor_invoice_number = models.CharField(max_length=200, null=True)
    shipping_instruction = models.TextField(null=True, blank=True, default="")
    sales_tax = models.FloatField(null=True)
    custom_field_present = models.JSONField(default=dict)

    def __str__(self):
        return str(self.crm_id)


class IngramPurchaseOrderData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    po_number = models.ForeignKey(
        "document.ConnectwisePurchaseOrder",
        related_name="ingram_line_items_data",
        on_delete=models.CASCADE,
    )
    ingram_order_number = models.CharField(max_length=100)
    status = models.CharField(max_length=500)
    shipping_address = JSONField(default=dict)
    vendor_part_number = models.CharField(max_length=100)
    shipped_quantity = models.PositiveIntegerField()
    eta = models.DateField(blank=True, null=True)
    tracking_numbers = ArrayField(models.CharField(max_length=100))
    serial_numbers = ArrayField(models.CharField(max_length=100))
    carrier_name = models.CharField(max_length=200)
    shipped_date = models.DateField(blank=True, null=True)
    ingram_suborder_number = models.CharField(max_length=100)
    ingram_order_line_number = models.CharField(max_length=100)
    additional_data = JSONField(default=dict)


class ConnectwisePurchaseOrderLineitemsData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    po_number = models.ForeignKey(
        "document.ConnectwisePurchaseOrder",
        related_name="cw_line_items_data",
        on_delete=models.CASCADE,
    )
    crm_id = models.IntegerField()
    description = models.CharField(max_length=500)
    line_number = models.IntegerField()
    product_crm_id = models.IntegerField()
    product_id = models.CharField(max_length=100)
    purchase_order_crm_id = models.IntegerField()
    ordered_quantity = models.IntegerField()
    received_quantity = models.IntegerField(null=True)
    serial_numbers = ArrayField(models.CharField(max_length=100))
    ship_date = models.DateTimeField(null=True)
    shipping_id = models.IntegerField(null=True)
    shipping_method = models.CharField(max_length=100, null=True)
    received_date = models.DateTimeField(null=True)
    received_status = models.CharField(max_length=100, null=True)
    last_updated_in_cw = models.DateTimeField()
    additional_data = JSONField(default=dict)
    tracking_numbers = ArrayField(
        base_field=models.CharField(max_length=100), blank=True, default=list
    )
    is_closed = models.BooleanField(default=False)
    is_cancelled = models.BooleanField(default=False)
    unit_cost = models.FloatField(null=True)
    expected_ship_date = models.DateTimeField(null=True)
    vendor_order_number = models.CharField(max_length=200, null=True)
    unit_of_measure = models.CharField(max_length=50, null=True)
    internal_notes = models.TextField(null=True, blank=True, default="")
    warehouse_bin_name = models.CharField(max_length=200, null=True)


class SalesOrderStatusSettings(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    in_progress_status = models.PositiveIntegerField(null=True)
    waiting_on_smartnet_status = models.PositiveIntegerField(null=True)
    closed_statuses = ArrayField(
        base_field=models.PositiveIntegerField(), default=list
    )


class SalesPurchaseOrderData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    crm_id = models.PositiveIntegerField()
    customer_po_number = models.CharField(max_length=100, null=True)
    order_date = models.DateTimeField(null=True, default=None)
    opportunity_name = models.TextField(null=True)
    opportunity_crm_id = models.ForeignKey(
        "document.ConnectwiseServiceTicket",
        on_delete=models.SET_NULL,
        related_name="serviceticketlinkedoppo",
        to_field="opportunity_crm_id",
        null=True,
    )
    opportunity_won_date = models.DateTimeField(null=True, default=None)
    site_crm_id = models.PositiveIntegerField(null=True)
    site_name = models.CharField(max_length=500, null=True)
    customer = models.ForeignKey(
        "accounts.Customer",
        on_delete=models.SET_NULL,
        related_name="salesorders",
        null=True,
    )
    company_name = models.CharField(max_length=500)
    status_name = models.CharField(max_length=100)
    status_id = models.PositiveIntegerField(blank=True, null=True)
    product_ids = JSONField(default=list)
    bill_closed_flag = models.BooleanField()
    invoice_ids = JSONField(default=list)
    shipping_contact_crm_id = models.IntegerField(null=True)
    shipping_contact_name = models.CharField(
        null=True, blank=True, max_length=150
    )
    shipping_site_crm_id = models.IntegerField(null=True)
    so_linked_pos = JSONField(default=list)
    due_date = models.DateTimeField(null=True, default=None)
    total = models.FloatField(null=True)
    sub_total = models.FloatField(null=True)
    tax_total = models.FloatField(null=True)
    location = models.CharField(max_length=100, null=True)
    po_custom_field_present = models.JSONField(default=dict)
    is_ignored = models.BooleanField(default=False)
    last_crm_update = models.DateTimeField(null=True)


class ProductsItemData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    crm_id = models.PositiveIntegerField()
    product_id = models.CharField(max_length=500)
    description = models.TextField(null=True)
    quantity = models.IntegerField(null=True)
    sales_order = models.ForeignKey(
        "document.SalesPurchaseOrderData",
        on_delete=models.SET_NULL,
        related_name="productitems",
        null=True,
    )
    invoice_id = models.PositiveIntegerField(null=True)
    invoice_number = models.CharField(max_length=50, null=True, blank=True)
    cancelled_quantity = models.IntegerField(null=True)
    cost = models.FloatField(null=True)
    price = models.FloatField(null=True)
    ext_cost = models.FloatField(null=True)
    ext_price = models.FloatField(null=True)
    sequence_number = models.IntegerField(null=True)


class FinanceData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    received_status = models.CharField(max_length=100, null=True)
    po_number = models.CharField(max_length=100)
    purchase_order_status = models.CharField(max_length=100, null=True)
    product_id = models.CharField(max_length=500)
    description = models.TextField(null=True)
    received_quantity = models.IntegerField(null=True)
    unit_cost_in_receiving = models.FloatField(null=True)
    vendor_company_name = models.CharField(null=True, max_length=500)
    vendor_order_number = models.CharField(max_length=200, null=True)
    customer_company_name = models.CharField(
        max_length=500, null=True, blank=True
    )
    ordered_quantity = models.IntegerField()
    serial_numbers = models.TextField(null=True, default=None)
    shipping_method = models.CharField(max_length=100, null=True)
    tracking_numbers = models.TextField(null=True, default=None)
    ext_cost_in_receiving = models.FloatField(null=True)
    location = models.CharField(max_length=100, null=True)
    expected_ship_date = models.DateTimeField(null=True)
    unit_cost_in_so = models.FloatField(null=True)
    unit_price = models.FloatField(null=True)
    ext_price = models.FloatField(null=True)
    ext_cost_in_so = models.FloatField(null=True)
    opportunity_name = models.TextField(null=True)
    sales_order_crm_id = models.PositiveIntegerField(null=True)
    po_date = models.DateTimeField(null=True, default=None)
    so_linked_pos = models.TextField(null=True, default=None)
    customer_po_number = models.CharField(max_length=100, null=True)
    quantity = models.IntegerField(null=True)
    is_closed = models.BooleanField(default=False)
    is_cancelled = models.BooleanField(default=False)
    shipment_date = models.DateTimeField(null=True, default=None)
    total = models.FloatField(null=True)
    entered_by = models.CharField(max_length=200, blank=True, null=True)
    shipping_instruction = models.TextField(null=True, blank=True, default="")
    sales_tax = models.FloatField(null=True)
    unit_of_measure = models.CharField(max_length=50, null=True)
    internal_notes_po = models.TextField(null=True, blank=True, default="")
    due_date = models.DateTimeField(null=True, default=None)
    total_in_so = models.FloatField(null=True)
    sub_total = models.FloatField(null=True)
    tax_total = models.FloatField(null=True)
    purchase_detail_recid = models.PositiveIntegerField(
        null=True, default=None
    )
    line_number = models.PositiveIntegerField(null=True, default=None)
    received_date = models.DateTimeField(null=True, default=None)
    Purchase_Order_RecID = models.PositiveIntegerField(null=True, default=None)
    order_date = models.DateTimeField(null=True, default=None)
    invoice_ids = JSONField(null=True, default=list)
    invoice_numbers = JSONField(null=True, default=list)
    opportunity_crm_id = models.PositiveIntegerField(null=True)
    status_name = models.CharField(max_length=100, null=True)
    opportunity_won_date = models.DateTimeField(null=True, default=None)
    invoice_number = models.CharField(max_length=50, null=True, blank=False)


class InvoiceData(TimeStampedModel):
    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    id = models.PositiveIntegerField(primary_key=True)
    number = models.CharField(max_length=100)
    data = JSONField(default=list)
    created_date = models.DateTimeField(null=True, default=None)
    payment_date = models.DateTimeField(null=True, default=None)


def get_vendor_onboarding_attachments_upload_url(
    instance, filename: str
) -> str:
    return (
        f"providers/{instance.provider.id}/sow-vendor-onboarding-setting/"
        f"{instance.sow_vendor_onboarding_setting.id}/{filename}"
    )


class SOWVendorOnboardingSetting(TimeStampedModel):
    """
    Model to store sow vendor onboarding settings.
    """

    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="sow_vendor_onboarding_setting",
    )
    territories = JSONField(default=list)
    customer_type = JSONField(default=dict)
    vendor_onboarding_email_alias = models.EmailField()
    vendor_onboarding_email_subject = models.CharField(
        max_length=200, blank=False, null=False
    )
    vendor_onboarding_email_body = models.TextField(null=False, blank=False)
    vendor_create_status = JSONField(default=dict)
    vendor_list_statuses = JSONField(default=list)

    def get_cw_customer_type_id_for_vendor_creation(self) -> int:
        return self.customer_type.get("customer_type_crm_id")

    def get_cw_status_ids_for_vendors_listing(self) -> List[int]:
        status_ids: List[int] = [
            status.get("customer_status_crm_id")
            for status in self.vendor_list_statuses
        ]
        return status_ids

    def get_cw_status_id_for_vendor_creation(self) -> int:
        return self.vendor_create_status.get("customer_status_crm_id")


class SOWVendorOnboardingAttachments(models.Model):
    """
    Model to store vendor onboarding setting attachments.
    """

    provider = models.ForeignKey(
        "accounts.Provider", on_delete=models.CASCADE, related_name="+"
    )
    sow_vendor_onboarding_setting = models.ForeignKey(
        "document.SOWVendorOnboardingSetting",
        on_delete=models.CASCADE,
        related_name="onboarding_attachments",
    )
    name = models.CharField(max_length=500, null=False, blank=False)
    attachment = models.FileField(
        null=True,
        blank=True,
        upload_to=get_vendor_onboarding_attachments_upload_url,
        storage=get_storage_class(),
    )


class DefaultDocumentCreatorRoleSetting(models.Model):
    """
    Model to store the default CW role ID of team roles.
    The SoW, CR author will be added as member to customer team with these roles.
    """

    provider = models.OneToOneField(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="default_document_creator_role_setting",
    )
    default_sow_creator_role_id = models.IntegerField(null=False)
    default_change_request_creator_role_id = models.IntegerField(null=False)


class DocumentAuthorTeamMemberMapping(models.Model):
    """
    The model stores SoW document and it's author's team member CRM ID.

    Created when a document is created and the author is added as member to customer team.
    If author is changed during document update, team_member_crm_id stored for the document is used to remove the
    existing member from customer team on CW. The mapping is updated with new author's team_member_crm_id.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        on_delete=models.CASCADE,
        related_name="+",
    )
    sow_document = models.OneToOneField(
        "document.SOWDocument",
        on_delete=models.CASCADE,
        related_name="author_team_member_mapping",
    )
    team_member_crm_id = models.IntegerField(null=False)


class SoWHourlyResourcesDescriptionMapping(models.Model):
    """
    Model to map description of hourly resources (vendors).
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="hourly_resources_description_mapping",
        on_delete=models.CASCADE,
    )
    vendor_name = models.CharField(max_length=200, blank=False, null=False)
    vendor_crm_id = models.PositiveIntegerField(null=False)
    resource_description = models.CharField(
        max_length=500, blank=False, null=False
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["provider", "vendor_crm_id"],
                name="unique_vendor_description_mapping",
            )
        ]


class FireReportPartnerSite(TimeStampedModel):
    """
    Model to store partner sites.
    """

    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="fire_report_partner_sites",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=50, null=False, blank=False)
    address_line_1 = models.CharField(max_length=50, null=True, blank=False)
    address_line_2 = models.CharField(max_length=50, null=True, blank=False)
    city = models.CharField(max_length=50, null=True, blank=False)
    state = models.CharField(max_length=100, null=True, blank=False)
    state_crm_id = models.PositiveIntegerField(null=True)
    country = models.CharField(max_length=100, null=True, blank=False)
    country_crm_id = models.PositiveIntegerField(null=True)
    zip = models.CharField(max_length=10, null=True, blank=False)
    phone_number = models.CharField(max_length=15, null=True, blank=False)


class FireReportSetting(TimeStampedModel):
    """
    Store FIRE Report setting for a Provider.
    """

    DAILY = "DAILY"
    WEEKLY = "WEEKLY"
    MONTHLY = "MONTHLY"
    YEARLY = "YEARLY"
    NEVER = "NEVER"
    FREQUENCY_CHOICES = Choices(
        (DAILY, "DAILY"),
        (WEEKLY, "WEEKLY"),
        (MONTHLY, "MONTHLY"),
        (YEARLY, "YEARLY"),
        (NEVER, "NEVER"),
    )
    LAST_30_DAYS = "LAST_30_DAYS"
    LAST_60_DAYS = "LAST_60_DAYS"
    LAST_90_DAYS = "LAST_90_DAYS"
    LAST_180_DAYS = "LAST_180_DAYS"
    date_range_choices = Choices(
        (LAST_30_DAYS, "LAST_30_DAYS"),
        (LAST_60_DAYS, "LAST_60_DAYS"),
        (LAST_90_DAYS, "LAST_90_DAYS"),
        (LAST_180_DAYS, "LAST_180_DAYS"),
    )

    provider = models.OneToOneField(
        "accounts.Provider",
        related_name="fire_report_setting",
        on_delete=models.CASCADE,
    )
    receiver_email_ids = ArrayField(
        base_field=models.EmailField(), null=False, default=list
    )
    subject = models.CharField(max_length=250, null=True, blank=False)
    frequency = models.CharField(choices=FREQUENCY_CHOICES, max_length=15)
    next_run = models.DateTimeField(null=True)
    past_days = models.IntegerField(null=True)

    @classmethod
    def get_next_run(cls, frequency: str) -> datetime:
        """
        Get the next run datetime for frequency.

        Args:
            frequency: Frequency

        Returns:
            Next run datetime.
        """
        now: datetime = timezone.now()
        if frequency == cls.DAILY:
            next_run = now + timedelta(days=1)
        elif frequency == cls.WEEKLY:
            next_run = now + timedelta(days=7)
        elif frequency == cls.MONTHLY:
            next_run = now + timedelta(days=30)
        elif frequency == cls.YEARLY:
            next_run = now + timedelta(days=365)
        else:
            next_run = None
        return next_run

    def get_date_range_date_values(
        self,
    ) -> Tuple[Optional[datetime.date], datetime.date]:
        """
        Get date values for given date range.

        Returns:
            Start date, end date
        """
        end_date = timezone.now().date()
        start_date = end_date - timedelta(days=self.past_days)
        return start_date, end_date


class PartnerSiteSyncError(TimeStampedModel):
    """
    Model to store partner site sync errors for create, update & delete operations.
    """

    CREATE = "CREATE"
    UPDATE = "UPDATE"
    DELETE = "DELETE"
    sync_operation_choices = Choices(
        (CREATE, "CREATE"),
        (UPDATE, "UPDATE"),
        (DELETE, "DELETE"),
    )
    provider = models.ForeignKey(
        "accounts.Provider",
        related_name="partner_site_sync_errors",
        on_delete=models.CASCADE,
    )
    operation = models.CharField(max_length=10, choices=sync_operation_choices)
    customer = models.ForeignKey(
        "accounts.Customer",
        related_name="+",
        on_delete=models.SET_NULL,
        null=True,
    )
    customer_crm_id = models.PositiveIntegerField(null=False)
    site_crm_id = models.PositiveIntegerField(null=True)
    site_name = models.CharField(max_length=100, null=True)
    site_payload = JSONField(default=dict)
    errors = JSONField(default=dict)


class SoWOpportunityTeamMapping(models.Model):
    """
    Model to store SoWDocument to opportunity team CRM ID mapping.
    """

    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    sow_document = models.OneToOneField(
        "document.SOWDocument",
        related_name="opportunity_team",
        on_delete=models.CASCADE,
    )
    author = models.ForeignKey(
        "accounts.User", related_name="+", on_delete=models.CASCADE
    )
    team_crm_id = models.PositiveIntegerField(null=False)


class AutomatedWeeklyRecapSetting(models.Model):
    """
    Model to store automated order tracking weekly recap setting.
    """

    MONDAY = "Monday"
    TUESDAY = "Tuesday"
    WEDNESDAY = "Wednesday"
    THURSDAY = "Thursday"
    FRIDAY = "Friday"
    SATURDAY = "Saturday"
    SUNDAY = "Sunday"
    WEEKLY_SCHEDULE_CHOICES = [
        (MONDAY, MONDAY),
        (TUESDAY, TUESDAY),
        (WEDNESDAY, WEDNESDAY),
        (THURSDAY, THURSDAY),
        (FRIDAY, FRIDAY),
        (SATURDAY, SATURDAY),
        (SUNDAY, SUNDAY),
    ]
    provider = models.OneToOneField(
        "accounts.Provider",
        related_name="weekly_recap_setting",
        on_delete=models.CASCADE,
    )
    receiver_email_ids = ArrayField(
        base_field=models.EmailField(), null=False, blank=False
    )
    sender = models.ForeignKey(
        "accounts.User", null=True, related_name="+", on_delete=models.SET_NULL
    )
    weekly_schedule = ArrayField(
        base_field=models.CharField(
            max_length=15, choices=WEEKLY_SCHEDULE_CHOICES
        ),
        null=False,
    )
    contact_type_crm_id = models.PositiveIntegerField(null=False)
    email_subject = models.CharField(max_length=200, null=False, blank=False)
    email_body_text = models.TextField(null=False)
    is_enabled = models.BooleanField(default=True)

    @classmethod
    def is_configured_for_provider(cls, provider) -> bool:
        """
        Check if automated recap setting is configured and enabled for the provider.

        Args:
            provider: Provider

        Returns:
            True if configured and enabled else False
        """
        try:
            return (
                provider.weekly_recap_setting
                and provider.weekly_recap_setting.is_enabled
            )
        except AutomatedWeeklyRecapSetting.DoesNotExist:
            return False

    def is_scheduled_for_today(self) -> bool:
        """
        Check if today is in weekly run schedule.

        Returns:
            True if configured for today else False
        """
        today: str = timezone.now().date().strftime("%A").upper()
        scheduled_days: Set[str] = {
            day.upper() for day in self.weekly_schedule
        }
        return today in scheduled_days

    @classmethod
    def get_weekly_recap_email_template_directory_path(cls) -> str:
        """
        Get weekly recap email templates directory path.

        Returns:
            Path
        """
        path: str = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates",
            os.path.join("weekly_recap"),
        )
        return path

    @classmethod
    def get_weekly_recap_email_body_template_path(cls) -> str:
        """
        Get weekly recap email body template path.

        Returns:
            Path
        """
        dir_path: str = cls.get_weekly_recap_email_template_directory_path()
        template_path: str = os.path.join(
            dir_path, "weekly_recap_email_body.html"
        )
        return template_path


class ConnectwiseOpportunityData(TimeStampedModel):
    """
    Model to store Connectwise opportunity data
    """

    provider = models.ForeignKey(
        "accounts.Provider", related_name="+", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=250, null=False)
    crm_id = models.PositiveIntegerField(null=False)
    status_crm_id = models.PositiveIntegerField(null=True)
    status_name = models.CharField(max_length=50, null=True)
    stage_crm_id = models.PositiveIntegerField(null=True)
    stage_name = models.CharField(max_length=50, null=True)
    customer_po = models.CharField(max_length=50, null=True, blank=True)
    customer_name = models.CharField(max_length=150, null=False, blank=False)
    customer_crm_id = models.PositiveIntegerField(null=False)
    closed_date = models.DateTimeField(null=True)
    last_crm_update = models.DateTimeField(null=True)


class Client360DashboardSetting(TimeStampedModel):
    """
    Model to store setting for various Client360 dashboard.
    """

    provider = models.OneToOneField(
        "accounts.Provider",
        related_name="client360_setting",
        on_delete=models.CASCADE,
    )
    # won_opp_status_crm_ids are used in Won opportunity in last 90 days chart
    won_opp_status_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, null=False
    )
    # open_po_status_crm_ids are used in Open POs chart
    open_po_status_crm_ids = ArrayField(
        base_field=models.PositiveIntegerField(), default=list, null=False
    )

    @classmethod
    def configured_for_provider(cls, provider) -> bool:
        """
        Check if Client360DashboardSetting configured for Provider or not.

        Args:
            provider: Provider

        Returns:
            True if configured else False
        """
        try:
            provider.client360_setting
            return True
        except Client360DashboardSetting.DoesNotExist:
            return False
