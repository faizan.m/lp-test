import datetime
from typing import Dict, Set, List, Union, Optional
from unittest import mock
from unittest.mock import patch

from document.services.fire_report import FireReportGenerator
from test_utils import BaseProviderTestCase
from document.tests.factories import (
    AutomatedWeeklyRecapSettingFactory,
    ConnectwisePurchaseOrderFactory,
    ConnectwisePurchaseOrderDataFactory,
    ConnectwisePurchaseOrderLineitemsDataFactory,
    SalesPurchaseOrderDataFactory,
    ConnectwiseServiceTicketFactory,
)
from document.services.order_tracking import WeeklyRecapService
from document.tests.factories import CustomerFactory


class TestWeeklyRecapService(BaseProviderTestCase):
    """
    Test class: WeeklyRecapService
    """

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.weekly_recap_setting: AutomatedWeeklyRecapSettingFactory = (
            AutomatedWeeklyRecapSettingFactory(
                provider=cls.provider,
                sender=cls.user,
                receiver_email_ids=["abc@test.com"],
                weekly_schedule=[
                    "Sunday",
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                ],
            )
        )
        cls.service_class = WeeklyRecapService(
            cls.provider, cls.weekly_recap_setting
        )

    def test_get_order_tracking_tracking_contact_details(self):
        self.provider.erp_client = mock.MagicMock()
        # Test the case when contact exists
        self.provider.erp_client.get_customer_users.return_value = [
            dict(
                first_name="Bruce",
                last_name="Wayne",
                email="bruce@wayne.com",
                title="CEO",
            )
        ]
        result: Dict = (
            self.service_class.get_order_tracking_tracking_contact_details(100)
        )
        expected_result: Dict = dict(
            first_name="Bruce",
            last_name="Wayne",
            email="bruce@wayne.com",
            title="CEO",
        )
        self.assertEqual(expected_result, result)
        # Test case when contact does not exists
        self.provider.erp_client.get_customer_users.return_value = []
        result: Dict = (
            self.service_class.get_order_tracking_tracking_contact_details(100)
        )
        self.assertEqual({}, result)

    @patch(
        "document.services.order_tracking.WeeklyRecapService."
        "get_order_tracking_tracking_contact_details",
        return_value=dict(
            first_name="Bruce",
            last_name="Wayne",
            email="bruce@wayne.com",
            title="CEO",
        ),
    )
    @patch(
        "document.services.order_tracking.convert_utc_datetime_to_provider_timezone",
        return_value=datetime.datetime(day=15, month=6, year=2023),
    )
    def test__get_render_context(
        self,
        convert_utc_datetime_to_provider_timezone,
        get_order_tracking_tracking_contact_details,
    ):
        customer: Customer = CustomerFactory(
            provider=self.provider, name="Telcom"
        )
        expected_result: Dict = dict(
            sender_details=dict(
                name=self.user.name,
                email=self.user.email,
                email_signature=self.user.profile.email_signature.get(
                    "email_signature", None
                ),
            ),
            subject=f"Telcom - {self.weekly_recap_setting.email_subject}",
            email_body=self.weekly_recap_setting.email_body_text,
            email_sent_datetime="Thursday, June 15, 2023 12:00 AM",
            customer_contact=dict(
                first_name="Bruce",
                last_name="Wayne",
                email="bruce@wayne.com",
                title="CEO",
            ),
        )
        result: Dict = self.service_class._get_render_context(customer)
        self.assertEqual(expected_result, result)

    def test_render_weekly_recap_email_body(self):
        render_context: Dict = dict(
            open_sales_orders=[
                dict(
                    crm_id=1,
                    customer_po="PO-123",
                    shipping_contact="John Wick",
                    opportunity_summary="Muscle car delivery",
                    site="101 Matrix",
                    last_product_ship_date="01/01/2023",
                )
            ],
            open_purchase_orders=[
                dict(
                    crm_id=1,
                    po_number="LP123",
                    last_product_ship_date="1/1/2021",
                    customer_site="San Francisco",
                ),
                dict(
                    crm_id=2,
                    po_number="LP456",
                    last_product_ship_date="10/2/2021",
                    customer_site="San Jose",
                ),
            ],
        )
        result: str = self.service_class.render_weekly_recap_email_body(
            render_context
        )
        self.assertTrue(result.startswith("<"))

    def test_generate_email_preview(self):
        result: str = self.service_class.generate_email_preview()
        self.assertTrue(result.startswith("<"))

    @patch("document.services.order_tracking.WeeklyRecapEmail.send")
    @patch(
        "document.services.order_tracking.WeeklyRecapService.render_weekly_recap_email_body"
    )
    @patch(
        "document.services.order_tracking.WeeklyRecapService._get_render_context"
    )
    @patch(
        "document.services.order_tracking.WeeklyRecapService.get_open_purchase_order_data_for_customer"
    )
    @patch(
        "document.services.order_tracking.WeeklyRecapService.get_open_sales_orders_data_for_customer"
    )
    def test_send_order_tracking_weekly_recap_email(
        self,
        get_open_sales_orders_data_for_customer,
        get_open_purchase_order_data_for_customer,
        _get_render_context,
        render_weekly_recap_email_body,
        send_email,
    ):
        customer: Customer = CustomerFactory(provider=self.provider)
        # Test the case when open sales orders does not exist
        get_open_sales_orders_data_for_customer.return_value = []
        get_open_purchase_order_data_for_customer.return_value = []
        (
            sent,
            error,
        ) = self.service_class.send_order_tracking_weekly_recap_email(customer)
        self.assertEqual(False, sent)
        self.assertEqual(
            error,
            "Open sales orders, open purchase orders not found for the customer.",
        )
        # Test the case when open sales orders exist
        get_open_sales_orders_data_for_customer.return_value = [
            dict(
                crm_id=1,
                customer_po="PO-123",
                shipping_contact="John Wick",
                opportunity_summary="Muscle car delivery",
                site="101 Matrix",
                last_product_ship_date="01/01/2023",
            )
        ]
        _get_render_context.return_value = {}
        render_weekly_recap_email_body.return_value = (
            "<p>Email body ready!</p>"
        )
        (
            sent,
            error,
        ) = self.service_class.send_order_tracking_weekly_recap_email(customer)
        self.assertEqual(True, sent)
        send_email.assert_called()

    def test_get_open_sales_orders_data_for_customer(self):
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_customer_complete_site_addresses_with_name.return_value = {
            2: "Gotham"
        }
        # Test when open sales orders exist. Mock the required DB objects
        customer_1 = CustomerFactory(provider=self.provider, crm_id=10)
        service_ticket = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            opportunity_crm_id=10,
        )
        purchase_order = ConnectwisePurchaseOrderFactory(
            po_number="LP-1234", ticket=service_ticket
        )
        sales_order = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            opportunity_crm_id=service_ticket,
            shipping_site_crm_id=2,
            customer_po_number="1234",
            crm_id=123,
            customer=customer_1,
            shipping_contact_name="Bruce Wayne",
            opportunity_name="Test opp 10",
        )
        ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            sales_order=sales_order,
            crm_id=101,
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=150,
            received_status="FullyReceived",
            expected_ship_date=datetime.datetime(day=10, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=0,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=1, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=10,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=20, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=5,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=5, month=1, year=2023),
        )
        result: List[
            Dict
        ] = self.service_class.get_open_sales_orders_data_for_customer(
            customer_1.crm_id
        )
        expected_result: List[Dict] = [
            dict(
                crm_id=123,
                customer_po="1234",
                shipping_contact="Bruce Wayne",
                opportunity_summary="Test opp 10",
                site="Gotham",
                last_product_ship_date="01/20/2023",
            )
        ]
        self.assertEqual(expected_result, result)
        # Test when open sales orders does not exist
        customer_2: Customer = CustomerFactory(
            provider=self.provider, crm_id=20
        )
        result: List[
            Dict
        ] = self.service_class.get_open_sales_orders_data_for_customer(
            customer_2.crm_id
        )
        self.assertEqual([], result)

    def test_get_open_purchase_order_data_for_customer(self):
        # Test when open purchase orders exist for customer
        customer_1 = CustomerFactory(provider=self.provider, crm_id=10)
        service_ticket = ConnectwiseServiceTicketFactory(
            provider=self.provider,
        )
        purchase_order = ConnectwisePurchaseOrderFactory(
            po_number="LP-1234", ticket=service_ticket
        )
        ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            crm_id=101,
            site_name="LA",
            customer_company_id=customer_1.crm_id,
            sales_order=None,
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=150,
            received_status="FullyReceived",
            expected_ship_date=datetime.datetime(day=10, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=0,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=1, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=10,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=20, month=1, year=2023),
        )
        ConnectwisePurchaseOrderLineitemsDataFactory(
            provider=self.provider,
            po_number=purchase_order,
            unit_cost=5,
            received_status="Waiting",
            expected_ship_date=datetime.datetime(day=5, month=1, year=2023),
        )
        result: List[
            Dict
        ] = self.service_class.get_open_purchase_order_data_for_customer(
            customer_1.crm_id
        )
        expected_result: List[Dict] = [
            dict(
                crm_id=101,
                po_number="LP-1234",
                customer_site="LA",
                last_product_ship_date="01/20/2023",
            )
        ]
        self.assertEqual(expected_result, result)
        # Test when open purchase orders do not exist
        customer_2 = CustomerFactory(provider=self.provider)
        result: List = (
            self.service_class.get_open_purchase_order_data_for_customer(
                customer_2.crm_id
            )
        )
        self.assertEqual([], result)
