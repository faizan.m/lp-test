import mock
from faker import Faker
from typing import Dict
from document.models import (
    SOWDocument,
    SoWOpportunityTeamMapping,
)
from document.services.opportunity_team_operations import (
    SoWOpportunityTeamOperationService,
)
from document.tests.factories import (
    SOWDocumentFactory,
    SoWOpportunityTeamMappingFactory,
)
from accounts.tests.factories import ProviderUserFactory
from test_utils import BaseProviderTestCase
from mock import patch


class TestSoWOpportunityTeamOperationService(BaseProviderTestCase):
    """
    Test class: SoWOpportunityTeamOperationService
    """

    @classmethod
    def setUpTestData(cls):
        cls.service_class = SoWOpportunityTeamOperationService()
        cls.fake = Faker()
        cls.setup_provider_and_user()

    def test_create_opportunity_team_mapping(self):
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        team_crm_id: int = 1
        mapping: SoWOpportunityTeamMapping = (
            self.service_class.create_opportunity_team_mapping(
                sow_document, team_crm_id
            )
        )
        self.assertEqual(mapping.team_crm_id, team_crm_id)
        self.assertEqual(mapping.author_id, sow_document.author_id)

    def test_update_opportunity_team_mapping(self):
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        SoWOpportunityTeamMappingFactory(
            sow_document=sow_document,
            provider=self.provider,
            author_id=sow_document.author_id,
            team_crm_id=1,
        )
        new_author: User = ProviderUserFactory(provider=self.provider)
        updated_mapping: SoWOpportunityTeamMapping = (
            self.service_class.update_opportunity_team_mapping(
                sow_document, new_author.id
            )
        )
        self.assertEqual(updated_mapping.author_id, new_author.id)

    @patch(
        "document.services.opportunity_team_operations.SoWOpportunityTeamOperationService."
        "create_opportunity_team_mapping",
    )
    def test_create_opportunity_team_and_mapping(
        self, create_opportunity_team_mapping
    ):
        self.provider.erp_client = mock.MagicMock()
        opportunity_team: Dict = dict(team_crm_id=10, team_type="Individual")
        self.provider.erp_client.create_opportunity_team_of_individual_type.return_value = (
            opportunity_team
        )
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, quote_id=101, author=self.user
        )
        result: Dict = self.service_class.create_opportunity_team_and_mapping(
            sow_document
        )
        self.assertEqual(result, opportunity_team)
        create_opportunity_team_mapping.assert_called()
        self.provider.erp_client.create_opportunity_team_of_individual_type.assert_called()

    @patch(
        "document.services.opportunity_team_operations.SoWOpportunityTeamOperationService."
        "update_opportunity_team_mapping",
    )
    def test_update_opportunity_team_and_mappping(
        self, update_opportunity_team_mapping
    ):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, quote_id=101, author=self.user
        )
        SoWOpportunityTeamMappingFactory(
            provider=self.provider,
            sow_document=sow_document,
            author=sow_document.author,
            team_crm_id=10,
        )
        self.provider.erp_client = mock.MagicMock()
        opportunity_team: Dict = dict(team_crm_id=10, team_type="Individual")
        self.provider.erp_client.update_opportunity_team.return_value = (
            opportunity_team
        )
        result: Dict = self.service_class.update_opportunity_team_and_mappping(
            sow_document
        )
        self.assertEqual(result, opportunity_team)
        update_opportunity_team_mapping.assert_called()
        self.provider.erp_client.update_opportunity_team.assert_called()

    def test__get_authors_system_member_crm_id(self):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, quote_id=101, author=self.user
        )
        system_member_crm_id: int = (
            self.service_class._get_authors_system_member_crm_id(sow_document)
        )
        self.assertEqual(
            system_member_crm_id,
            sow_document.author.profile.system_member_crm_id,
        )

    @patch(
        "document.services.opportunity_team_operations.SoWOpportunityTeamOperationService."
        "create_opportunity_team_and_mapping",
    )
    def test_create_or_update_opportunity_team__in_case_of_create(
        self, create_opportunity_team_and_mapping
    ):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, quote_id=101, author=self.user
        )
        self.service_class.create_or_update_opportunity_team(sow_document)
        create_opportunity_team_and_mapping.assert_called()

    @patch(
        "document.services.opportunity_team_operations.SoWOpportunityTeamOperationService."
        "update_opportunity_team_and_mappping",
    )
    def test_create_or_update_opportunity_team__in_case_of_update(
        self, update_opportunity_team_and_mappping
    ):
        old_author: User = ProviderUserFactory(provider=self.provider)
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, quote_id=101, author=self.user
        )
        SoWOpportunityTeamMappingFactory(
            provider=self.provider,
            sow_document=sow_document,
            author=old_author,
            team_crm_id=10,
        )
        self.service_class.create_or_update_opportunity_team(sow_document)
        update_opportunity_team_and_mappping.assert_called()
