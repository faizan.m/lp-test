from document.services.operations_dashboard import OperationsDashboardService
from unittest.mock import patch
from unittest import mock
from test_utils import BaseProviderTestCase
from typing import List, Dict, Tuple
from document.models import (
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrder,
)
from document.tests.factories import (
    SalesPurchaseOrderDataFactory,
    ConnectwisePurchaseOrderDataFactory,
    ConnectwisePurchaseOrderFactory,
)
from factory.fuzzy import FuzzyText
from faker import Faker


class TestOperationsDashboardService(BaseProviderTestCase):
    """
    Test the service: OperationsDashboardService
    """

    service_class = OperationsDashboardService

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_get_purchase_orders_data_from_erp(self):
        """
        Test the method: get_purchase_orders_data_from_erp()
        """
        fake = Faker()
        provider: Provider = mock.Mock()
        purchase_order_crm_ids: Tuple[int] = (fake.pyint(),)
        mock_cw_po_data: List[Dict] = [
            dict(
                id=fake.pyint(),
                poNumber=FuzzyText().fuzz(),
                status=fake.name(),
                customFields=[
                    dict(
                        id=100,
                        caption=fake.text(max_nb_chars=5),
                        type=fake.text(max_nb_chars=5),
                        entryMethod=fake.text(max_nb_chars=5),
                        numberOfDecimals=fake.pyint(),
                        value=str(fake.pyint()),
                    )
                ],
            )
        ]
        provider.erp_client.get_purchase_order_list.return_value = (
            mock_cw_po_data
        )
        dashboard_service: OperationsDashboardService = self.service_class(
            provider
        )
        result: List[
            Dict
        ] = dashboard_service.get_purchase_orders_data_from_erp(
            purchase_order_crm_ids
        )
        self.assertEqual(mock_cw_po_data, result)

    @patch(
        "document.tests.test_services.test_operations_dashboard.OperationsDashboardService."
        "link_sales_order_to_purchase_orders_in_cw",
        return_value=[],
    )
    @patch(
        "document.tests.test_services.test_operations_dashboard.OperationsDashboardService."
        "get_purchase_orders_data_from_erp",
        return_value=[
            dict(
                id=100,
                poNumber="PO-123",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
            dict(
                id=200,
                poNumber="PO-345",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
        ],
    )
    def test_link_purchase_orders_to_sales_order(
        self,
        get_purchase_orders_data_from_erp_1,
        link_sales_order_to_purchase_orders_in_cw,
    ):
        """
        Test the method: link_purchase_orders_to_sales_order()
        """
        fake = Faker()
        provider: Provider = self.provider
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=provider)
        )
        po_1: ConnectwisePurchaseOrder = ConnectwisePurchaseOrderFactory(
            po_number="PO-123"
        )
        po_2: ConnectwisePurchaseOrder = ConnectwisePurchaseOrderFactory(
            po_number="PO-345"
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=provider, crm_id=100, po_number=po_1
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=provider, crm_id=200, po_number=po_2
        )
        po_custom_field_id: int = 5
        purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            ConnectwisePurchaseOrderData.objects.filter(provider=provider)
        )
        mock_updated_cw_po_data: List[Dict] = [
            dict(
                id=100,
                poNumber="PO-123",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                        value=str(sales_order.crm_id),
                    )
                ],
            ),
            dict(
                id=200,
                poNumber="PO-345",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                        value=str(sales_order.crm_id),
                    )
                ],
            ),
        ]
        dashboard_service: OperationsDashboardService = self.service_class(
            provider
        )
        provider.erp_client = mock.Mock()
        provider.erp_client.update_purchase_order_custom_field.side_effect = [
            mock_updated_cw_po_data[0],
            mock_updated_cw_po_data[1],
        ]
        expected_result: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            linked_purchase_orders=[
                dict(
                    purchase_order_crm_id=mock_updated_cw_po_data[0]["id"],
                    po_number=mock_updated_cw_po_data[0]["poNumber"],
                ),
                dict(
                    purchase_order_crm_id=mock_updated_cw_po_data[1]["id"],
                    po_number=mock_updated_cw_po_data[1]["poNumber"],
                ),
            ],
            linking_errors=[],
        )
        actual_result: Dict = (
            dashboard_service.link_purchase_orders_to_sales_order(
                sales_order,
                purchase_orders,
                po_custom_field_id,
                purchase_order_crm_ids=(100, 200),
            )
        )
        self.assertEqual(expected_result, actual_result)

    @patch(
        "document.tests.test_services.test_operations_dashboard.OperationsDashboardService."
        "link_sales_order_to_purchase_orders_in_cw",
        return_value=[],
    )
    @patch(
        "document.tests.test_services.test_operations_dashboard.OperationsDashboardService."
        "get_purchase_orders_data_from_erp",
        return_value=[
            dict(
                id=100,
                poNumber="PO-123",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
            dict(
                id=200,
                poNumber="PO-345",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
        ],
    )
    def test_remove_purchase_order_link_from_sales_order(
        self,
        get_purchase_orders_data_from_erp_1,
        link_sales_order_to_purchase_orders_in_cw,
    ):
        """
        Test the method: remove_purchase_order_link_from_sales_order()
        """
        fake = Faker()
        provider: Provider = self.provider
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=provider)
        )
        po_1: ConnectwisePurchaseOrder = ConnectwisePurchaseOrderFactory(
            po_number="PO-123"
        )
        po_2: ConnectwisePurchaseOrder = ConnectwisePurchaseOrderFactory(
            po_number="PO-345"
        )
        purchase_order_1: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=provider,
                crm_id=100,
                sales_order=sales_order,
                po_number=po_1,
            )
        )
        purchase_order_2: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=provider,
                crm_id=200,
                sales_order=sales_order,
                po_number=po_2,
            )
        )
        po_custom_field_id: int = 5
        mock_updated_cw_po_data: List[Dict] = [
            dict(
                id=100,
                poNumber="PO-123",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
            dict(
                id=200,
                poNumber="PO-345",
                status="Waiting",
                customFields=[
                    dict(
                        id=5,
                        caption="SO#",
                        type="Type",
                        entryMethod="EntryMethod",
                        numberOfDecimals=2,
                    )
                ],
            ),
        ]
        dashboard_service: OperationsDashboardService = self.service_class(
            provider
        )
        provider.erp_client = mock.Mock()
        provider.erp_client.update_purchase_order_custom_field.side_effect = [
            mock_updated_cw_po_data[0],
            mock_updated_cw_po_data[1],
        ]
        expected_result: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            removed_purchase_order_link=[
                dict(
                    purchase_order_crm_id=mock_updated_cw_po_data[0]["id"],
                    po_number=mock_updated_cw_po_data[0]["poNumber"],
                ),
                dict(
                    purchase_order_crm_id=mock_updated_cw_po_data[1]["id"],
                    po_number=mock_updated_cw_po_data[1]["poNumber"],
                ),
            ],
            purchase_order_link_remove_errors=[],
        )
        actual_result: Dict = (
            dashboard_service.remove_purchase_order_link_from_sales_order(
                (purchase_order_1.crm_id, purchase_order_2.crm_id),
                sales_order,
                (purchase_order_1.crm_id),
                po_custom_field_id,
            )
        )
        purchase_order_1.refresh_from_db()
        purchase_order_2.refresh_from_db()
        link_removed: bool = (
            purchase_order_1.sales_order is None
            and purchase_order_2.sales_order is None
        )
        self.assertEqual(expected_result, actual_result)
        self.assertEqual(link_removed, True)
