import copy
from typing import Dict
from unittest.mock import patch

import mock

from accounts.models import Customer
from accounts.tests.factories import CustomerFactory, ProfileFactory
from document.models import SOWDocument, SOWCategory
from document.services.document_service import DocumentService
from document.tests.factories import (
    SOWDocumentFactory,
    SOWCategoryFactory,
    SOWDocumentSettingsFactory,
)
from document.tests.utils import (
    get_mock_json_config_for_sow_document,
    get_mock_company_quotes,
)
from test_utils import BaseProviderTestCase
from faker import Faker
from typing import List, Dict


class TestDocumentService(BaseProviderTestCase):
    """
    Test service: DocumentService
    """

    service_class = DocumentService()

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "document.services.document_service.SoWOpportunityTeamOperationService.create_or_update_opportunity_team",
    )
    @patch(
        "document.services.document_service.SoWCustomerTeamOperationService.add_sow_document_author_to_customer_team"
    )
    @patch(
        "document.services.document_service.DocumentService.get_document_margin",
        return_value=50,
    )
    @patch(
        "document.services.document_service.QuoteService.create_or_update_forecast",
        return_value=dict(id=200),
    )
    @patch(
        "document.services.document_service.QuoteService.get_quote",
        return_value=dict(quote_id=100),
    )
    def test_create_document(
        self,
        get_quote_call,
        create_or_update_forecast_call,
        get_document_margin_call,
        add_sow_document_author_to_customer_team,
        create_or_update_opportunity_team,
    ):
        """
        Test method: create_document
        """
        customer: Customer = CustomerFactory(provider=self.provider)
        category: SOWCategory = SOWCategoryFactory()
        sow_document: SOWDocument = SOWDocumentFactory.build()
        provider_id: int = self.provider.id
        quote_id: int = 100
        document_create_payload: Dict = dict(
            name=sow_document.name,
            quote_id=quote_id,
            doc_type=sow_document.doc_type,
            json_config=get_mock_json_config_for_sow_document(),
            customer=customer,
            user=self.user,
            category=category,
        )
        result = self.service_class.create_document(
            provider_id, self.user.id, document_create_payload
        )
        get_quote_call.assert_called_with(
            provider_id, quote_id, customer.crm_id
        )
        create_or_update_forecast_call.assert_called_with(
            self.provider.id, quote_id, document_create_payload
        )
        get_document_margin_call.assert_called()
        add_sow_document_author_to_customer_team.assert_called()
        create_or_update_opportunity_team.assert_called()
        self.assertIsInstance(result, SOWDocument)

    @patch(
        "document.services.document_service.SoWOpportunityTeamOperationService.create_or_update_opportunity_team",
    )
    @patch(
        "document.services.document_service.SoWCustomerTeamOperationService.add_updated_author_to_customer_team"
    )
    @patch(
        "document.services.document_service.DocumentService.get_document_margin",
        return_value=50,
    )
    @patch(
        "document.services.document_service.QuoteService.create_or_update_forecast",
        return_value=dict(id=200),
    )
    @patch(
        "document.services.document_service.QuoteService.get_quote",
        return_value=dict(quote_id=100),
    )
    def test_update_document(
        self,
        get_quote_call,
        create_or_update_forecast_call,
        get_document_margin_call,
        add_updated_author_to_customer_team_call,
        create_or_update_opportunity_team,
    ):
        fake = Faker()
        provider_id: int = self.provider.id
        sow_document: SOWDocument = SOWDocumentFactory.create(
            provider=self.provider
        )
        quote_id: int = 100
        json_config: Dict = get_mock_json_config_for_sow_document()
        sow_document_update_payload: Dict = dict(
            update_author=True,
            update_version=True,
            version_description=fake.text(max_nb_chars=5),
            name=fake.name(),
            quote_id=quote_id,
            json_config=copy.deepcopy(json_config),
        )
        self.service_class.update_document(
            provider_id,
            sow_document.id,
            self.user.id,
            sow_document_update_payload,
        )
        get_quote_call.assert_called_with(provider_id, quote_id)
        create_or_update_forecast_call.assert_called_with(
            provider_id, quote_id, sow_document_update_payload
        )
        get_document_margin_call.assert_called()
        add_updated_author_to_customer_team_call.assert_called()
        sow_document.refresh_from_db()
        self.assertEqual(
            sow_document.name, sow_document_update_payload["name"]
        )

    def test_get_document(self):
        """
        Test method: get_document
        """
        sow_document: SOWDocument = SOWDocumentFactory.create(
            provider=self.provider
        )
        result = self.service_class.get_document(
            self.provider.id, sow_document.id
        )
        self.assertEqual(result, sow_document)

    @patch(
        "document.services.document_service.image_as_base64",
        return_value="Encoded string",
    )
    def test_get_logo(self, image_as_base64_call):
        """
        Test method: get_logo
        """
        result = self.service_class.get_logo()
        self.assertIsInstance(result, str)
        image_as_base64_call.assert_called()

    @patch(
        "document.services.document_service.image_as_base64",
        return_value="Encoded string",
    )
    def test_get_banner(self, image_as_base64_call):
        """
        Test method: get_banner
        """
        result = self.service_class.get_banner()
        self.assertIsInstance(result, str)
        image_as_base64_call.assert_called()

    @patch(
        "document.services.document_service.image_as_base64",
        return_value="Encoded string",
    )
    def test_get_footer(self, image_as_base64_call):
        """
        Test method: get_footer
        """
        result = self.service_class.get_footer()
        self.assertIsInstance(result, str)
        image_as_base64_call.assert_called()

    def test_get_header_html(self):
        """
        Test method: get_header_html
        """
        result = self.service_class.get_header_html(dict())
        self.assertIsInstance(result, str)
        self.assertEqual(len(result) > 0, True)

    def test_get_footer_html(self):
        """
        Test method: get_footer_html
        """
        result = self.service_class.get_footer_html(dict())
        self.assertIsInstance(result, str)
        self.assertEqual(len(result) > 0, True)

    def test_convert_html_to_str(self):
        """
        Test method: convert_html_to_str
        """
        pass

    @patch(
        "document.services.document_service.generate_random_id",
        return_value="1234567890",
    )
    @patch(
        "document.services.document_service.tempfile.gettempdir",
        return_value="TEMP_DIR",
    )
    def test_get_temp_file_path(
        self, gettempdir_call, generate_random_id_call
    ):
        """
        Test method: _get_temp_file_path
        """

        file_name, file_path = self.service_class._get_temp_file_path()
        self.assertEqual(file_name, "1234567890.pdf")
        self.assertEqual(file_path, "TEMP_DIR/1234567890.pdf")

    def test_get_sow_setting(self):
        """
        Test method: _get_sow_setting
        """
        settings = SOWDocumentSettingsFactory.create(provider=self.provider)
        result = self.service_class._get_sow_setting(self.provider)
        self.assertEqual(result, settings)

    def test_get_project_assumption_variable_values(self):
        """
        Test method: get_project_assumption_variable_values
        """
        fake = Faker()
        engineering_hours_breakup = dict(
            total_sites=fake.pyint(),
            total_sites_override=fake.pybool(),
            total_cutovers=fake.pyint(),
            total_cutovers_override=fake.pybool(),
        )
        mock_service_cost: Dict = dict(
            engineering_hours_breakup=engineering_hours_breakup
        )
        result = self.service_class.get_project_assumption_variable_values(
            mock_service_cost
        )
        self.assertTupleEqual(
            result,
            (
                engineering_hours_breakup["total_sites"],
                engineering_hours_breakup["total_sites_override"],
                engineering_hours_breakup["total_cutovers"],
                engineering_hours_breakup["total_cutovers_override"],
            ),
        )

    def test_get_additional_project_assumptions(self):
        """
        Test method: get_additional_project_assumptions
        """
        SOWDocumentSettingsFactory.create(
            provider=self.provider,
            total_no_of_sites_statement=(
                "Up to ##{no_of_sites} sites will be included in this project."
            ),
            total_no_of_cutovers_statement=(
                "Up to ##{no_of_cutovers} cutovers has been accounted for in this project. "
                "Additional cutover requests will require a change order and associated costs or hours."
            ),
        )
        engineering_hours_breakup = dict(
            total_sites=10,
            total_sites_override=False,
            total_cutovers=20,
            total_cutovers_override=False,
        )
        mock_service_cost: Dict = dict(
            engineering_hours_breakup=engineering_hours_breakup
        )
        expected_result: List[str] = [
            "Up to 10 sites will be included in this project.",
            "Up to 20 cutovers has been accounted for in this project. Additional cutover requests will require a change order and associated costs or hours.",
        ]
        result = self.service_class.get_additional_project_assumptions(
            self.provider, mock_service_cost
        )
        self.assertListEqual(result, expected_result)

    def test_download_document(self):
        """
        Test method: download_document
        """
        pass

    def test_render_pdf(self):
        """
        Test method: render_pdf
        """
        pass

    @patch(
        "document.services.document_service.get_template",
        return_value=mock.Mock(),
    )
    @patch(
        "document.services.document_service.SOWDocument.get_doc_template_path",
        return_value=mock.Mock(),
    )
    def test_get_rendered_html_document(
        self, get_doc_template_path_call, get_template_call
    ):
        """
        Test method: get_rendered_html_document
        """
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        template_vars: Dict = dict()
        file_type: str = "PDF"
        self.service_class.get_rendered_html_document(
            sow_document, template_vars, file_type
        )
        get_doc_template_path_call.assert_called_with(
            sow_document.doc_type, file_type
        )
        get_template_call.assert_called()

    @patch(
        "document.services.document_service.DocumentService.get_project_assumption_variable_values",
        return_value=(1, False, 2, False),
    )
    @patch(
        "document.services.document_service.DocumentService.get_additional_project_assumptions"
    )
    @patch("document.services.document_service.get_formatted_phone_number")
    def test_get_template_context(
        self,
        get_formatted_phone_number_call,
        get_additional_project_assumptions_call,
        get_project_assumption_variable_values_call,
    ):
        """
        Test method: get_template_context
        """
        fake = Faker()
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_customer.return_value = dict(
            territory_manager_name=fake.name(),
            name=fake.name(),
            email=fake.email(),
        )
        sow_document: SOWDocument = SOWDocumentFactory.create(
            provider=self.provider, user=self.user
        )
        result = self.service_class.get_template_context(
            self.user, sow_document, self.provider.erp_client
        )
        self.assertIsInstance(result, dict)
        self.provider.erp_client.get_customer.assert_called_with(
            sow_document.customer.crm_id
        )
        get_formatted_phone_number_call.assert_called_with(
            sow_document.user.profile.full_cell_phone_number
        )
        get_additional_project_assumptions_call.assert_called_with(
            sow_document.provider,
            sow_document.json_config.get("service_cost", {}),
        )
        get_project_assumption_variable_values_call.assert_called_with(
            sow_document.json_config.get("service_cost", {})
        )

    # @patch(
    #     "document.services.document_service.settings.DEBUG",
    #     return_value=True,
    # )
    # @patch(
    #     "document.services.document_service.DocumentService.render_pdf",
    #     return_value="header_html",
    # )
    # @patch(
    #     "document.services.document_service.render_to_string",
    #     return_value="header_html",
    # )
    # @patch(
    #     "document.services.document_service.image_as_base64",
    #     return_value="encoded_logo",
    # )
    # @patch(
    #     "document.services.document_service.tempfile.gettempdir",
    #     return_value="TEMP_DIR",
    # )
    # @patch(
    #     "document.services.document_service.get_sow_document_file_name",
    #     return_value="FILE_NAME.PDF",
    # )
    # @patch(
    #     "document.services.document_service.DocumentService.get_rendered_html_service_detail_document",
    #     return_value="<p>HTML string</p>",
    # )
    # def test_service_details_download(
    #     self,
    #     get_rendered_html_service_detail_document_call,
    #     get_valid_file_name_call,
    #     gettempdir_call,
    #     image_as_base64_call,
    #     render_to_string_call,
    #     render_pdf_call,
    #     patch_debug_settings_as_true,
    # ):
    #     """
    #     Test method: service_details_download
    #     """
    #     sow_document: SOWDocument = SOWDocumentFactory.create(
    #         provider=self.provider
    #     )
    #     quote: Dict = dict(name="Mock quote name", id=1)
    #     result = self.service_class.service_details_download(
    #         self.provider.id, sow_document, quote
    #     )
    #     expected_result: Dict = dict(
    #         file_name="FILE_NAME.PDF", file_path="TEMP_DIR/FILE_NAME.PDF"
    #     )
    #     self.assertDictEqual(result, expected_result)
    #     get_rendered_html_service_detail_document_call.assert_called_with(
    #         sow_document, quote
    #     )
    #     get_valid_file_name_call.assert_called()
    #     gettempdir_call.assert_called()
    #     image_as_base64_call.assert_called()
    #     render_to_string_call.assert_called()
    #     render_pdf_call.assert_called()

    # @patch(
    #     "document.services.document_service.get_template",
    #     return_value=mock.MagicMock(),
    # )
    # @patch(
    #     "document.services.document_service.SOWDocument.get_service_detail_template_path",
    #     return_value="TEMPLATE_PATH",
    # )
    # @patch(
    #     "document.services.document_service.DocumentService.get_project_assumption_variable_values",
    #     return_value=(1, False, 2, False),
    # )
    # @patch(
    #     "document.services.document_service.QuoteService.get_forecast_from_template",
    #     return_value=dict(revenue=100, cost=50),
    # )
    # def test_get_rendered_html_service_detail_document(
    #     self,
    #     get_forecast_from_template_call,
    #     get_project_assumption_variable_values_call,
    #     get_service_detail_template_path_call,
    #     get_template_call,
    # ):
    #     """
    #     Test method: get_rendered_html_service_detail_document
    #     """
    #     sow_document: SOWDocument = SOWDocumentFactory.create(
    #         provider=self.provider, doc_type=SOWDocument.T_AND_M
    #     )
    #     quote: Dict = dict(name="Mock quote", id=1)
    #     self.service_class.get_rendered_html_service_detail_document(
    #         sow_document, quote
    #     )
    #     get_forecast_from_template_call.assert_called()
    #     get_project_assumption_variable_values_call.assert_called()
    #     get_service_detail_template_path_call.assert_called()
    #     get_template_call.assert_called()

    def test_create_sow_document_object(self):
        """
        Test method: create_sow_document_object
        """
        sow_doc: SOWDocument = SOWDocumentFactory.build(
            provider=self.provider,
        )
        mock_sow_object_payload: Dict = dict(
            name=sow_doc.name,
            customer=sow_doc.customer.id,
            user=self.user,
            category=sow_doc.id,
            json_config=sow_doc.json_config,
            updated_by=self.user,
            quote_id=sow_doc.quote_id,
            forecast_id=sow_doc.forecast_id,
            major_version=sow_doc.major_version,
            minor_version=sow_doc.minor_version,
            doc_type=sow_doc.doc_type,
            author=self.user,
        )
        result = self.service_class.create_sow_document_object(
            mock_sow_object_payload
        )
        self.assertIsInstance(result, SOWDocument)

    @patch(
        "document.services.document_service.FileUploadService.upload_file",
        return_value="FILE_PATH",
    )
    @patch(
        "document.services.document_service.generate_pdf_using_pdf_shift",
        return_value=dict(file_name="FILE_NAME", file_path="FILE_PATH"),
    )
    @patch(
        "document.services.document_service.DocumentService.get_footer_html",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.DocumentService.get_header_html",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.DocumentService.convert_html_to_str",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.DocumentService.get_rendered_html_document",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.DocumentService.get_template_context",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.DocumentService.create_sow_document_object",
        return_value=mock.MagicMock(),
    )
    def test_generate_document_preview(
        self,
        create_sow_document_object_call,
        get_template_context_call,
        get_rendered_html_document,
        convert_html_to_str_call,
        get_header_html_call,
        get_footer_html_call,
        generate_pdf_using_pdf_shift,
        upload_file_call,
    ):
        self.provider.erp_client = mock.MagicMock()
        result = self.service_class.generate_document_preview(
            self.provider, self.user, dict()
        )
        self.assertIsInstance(result, dict)
        create_sow_document_object_call.assert_called()
        get_template_context_call.assert_called()
        get_rendered_html_document.assert_called()
        convert_html_to_str_call.assert_called()
        get_header_html_call.assert_called()
        get_footer_html_call.assert_called()
        generate_pdf_using_pdf_shift.assert_called()
        upload_file_call.assert_called()

    @patch(
        "document.services.document_service.FileUploadService.upload_file",
        return_value="FILE_PATH",
    )
    @patch(
        "document.services.document_service.generate_pdf_using_pdf_shift",
        return_value=dict(file_name="FILE_NAME", file_path="FILE_PATH"),
    )
    @patch(
        "document.services.document_service.DocumentService._get_temp_file_path",
        return_value=dict(file_name="FILE_NAME", file_path="FILE_PATH"),
    )
    @patch(
        "document.services.document_service.DocumentService.get_footer_html",
        return_value="<p>FOOTER HTML string</p>",
    )
    @patch(
        "document.services.document_service.DocumentService.get_header_html",
        return_value="<p>HEADER HTML string</p>",
    )
    @patch(
        "document.services.document_service.DocumentService.convert_html_to_str",
        return_value="<p>HTML string</p>",
    )
    @patch(
        "document.services.document_service.get_template",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.services.document_service.SOWDocument.get_doc_template_path",
        return_value="TEMPLATE_PATH",
    )
    @patch(
        "document.services.document_service.DocumentService.get_footer",
        return_value="FOOTER",
    )
    @patch(
        "document.services.document_service.DocumentService.get_banner",
        return_value="BANNER",
    )
    @patch(
        "document.services.document_service.DocumentService.get_logo",
        return_value="LOGO",
    )
    @patch(
        "document.services.document_service.DocumentService.get_additional_project_assumptions",
        return_value=list(),
    )
    def test_generate_template_preview(
        self,
        get_additional_project_assumptions_call,
        get_logo_call,
        get_banner_call,
        get_footer_call,
        get_doc_template_path_call,
        get_template_call,
        convert_html_to_str_call,
        get_header_html_call,
        get_footer_html_call,
        _get_temp_file_path_call,
        generate_pdf_using_pdf_shift,
        upload_file_call,
    ):
        result = self.service_class.generate_template_preview(
            self.provider, dict(), SOWDocument.GENERAL
        )
        self.assertIsInstance(result, dict)
        get_additional_project_assumptions_call.assert_called()
        get_logo_call.assert_called()
        get_banner_call.assert_called()
        get_footer_call.assert_called()
        get_doc_template_path_call.assert_called()
        get_template_call.assert_called()
        convert_html_to_str_call.assert_called()
        get_header_html_call.assert_called()
        get_footer_html_call.assert_called()
        _get_temp_file_path_call.assert_called()
        generate_pdf_using_pdf_shift.assert_called()
        upload_file_call.assert_called()

    def test_list_document(self):
        """
        Test method: list_document
        """
        SOWDocumentFactory.create_batch(2, provider=self.provider)
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider=self.provider
        )
        mock_quotes: List[Dict] = get_mock_company_quotes(sow_documents)
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mock_quotes
        )
        result = self.service_class.list_document(self.provider)
        self.assertEqual(len(result), 2)

    def test_calculate_base_internal_cost(self):
        """
        Test method: calculate_base_internal_cost
        """
        service_cost_json: Dict = dict(
            engineering_hours=10, project_management_hours=10, after_hours=10
        )
        actual_internal_cost: int = (
            self.service_class.calculate_base_internal_cost(
                service_cost_json,
                engineering_hourly_cost=1,
                pm_hourly_cost=2,
                after_hours_cost=3,
            )
        )
        self.assertEqual(actual_internal_cost, 60)

    @patch(
        "document.services.document_service.DocumentService.calculate_base_internal_cost",
        return_value=10,
    )
    def test_calculate_fixed_fee_internal_cost(
        self, calculate_base_internal_cost_call
    ):
        """
        Test method: calculate_fixed_fee_internal_cost
        """
        service_cost_json: Dict = dict(
            travels=[dict(cost=10), dict(cost=20)],
            contractors=[dict(customer_cost=10), dict(customer_cost=20)],
        )
        fixed_fee_internal_cost: int = (
            self.service_class.calculate_fixed_fee_internal_cost(
                service_cost_json
            )
        )
        self.assertEqual(fixed_fee_internal_cost, 70)

    @patch(
        "document.services.document_service.DocumentService.calculate_base_internal_cost",
        return_value=10,
    )
    def test_calculate_t_and_m_internal_cost(
        self, calculate_base_internal_cost_call
    ):
        """
        Test method: calculate_t_and_m_internal_cost
        """
        t_and_m_internal_cost: int = (
            self.service_class.calculate_t_and_m_internal_cost(dict())
        )
        self.assertEqual(t_and_m_internal_cost, 10)

    @patch(
        "document.services.document_service.SoWDocumentCalculation.get_total_margin",
        return_value=1500,
    )
    def test_get_document_margin(self, get_total_margin):
        """
        Test method: get_document_margin
        """
        sow_document: SOWDocument = SOWDocumentFactory.create(
            provider=self.provider
        )
        margin: int = self.service_class.get_document_margin(sow_document)
        self.assertEqual(margin, 1500)
        get_total_margin.assert_called()

    @patch(
        "document.services.document_service.DocumentService.calculate_risk_budget",
        return_value=10,
    )
    def test_get_internal_cost_risk(self, calculate_risk_budget_call):
        """
        Test method: get_internal_cost_risk
        """
        sow_settings = SOWDocumentSettingsFactory(
            provider=self.provider, engineering_hourly_cost=10
        )
        service_cost_data: Dict = dict(
            engineering_hours=5, engineering_hourly_rate=20
        )
        internal_cost_risk: int = self.service_class.get_internal_cost_risk(
            service_cost_data, sow_settings
        )
        self.assertEqual(internal_cost_risk, 5)

    def test_calculate_risk_budget(self):
        """
        Test method: calculate_risk_budget
        """
        sow_settings = SOWDocumentSettingsFactory(
            provider=self.provider, fixed_fee_variable=0.5
        )
        service_cost_data: Dict = dict(
            engineering_hours=1,
            engineering_hourly_rate=10,
            project_management_hours=2,
            project_management_hourly_rate=10,
            after_hours=3,
            after_hours_rate=10,
        )
        risk_budget: int = self.service_class.calculate_risk_budget(
            service_cost_data, sow_settings
        )
        self.assertEqual(risk_budget, 30)
