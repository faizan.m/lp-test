from typing import Dict, List, Optional

from faker import Faker
from mock import patch

from document.services.sow_calculations import ServiceCostCalculation
from document.tests.factories import SOWDocumentSettingsFactory
from test_utils import BaseProviderTestCase


class TestServiceCostCalculation(BaseProviderTestCase):
    """
    Test class: ServiceCostCalculation
    """

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        # Note the values used in sow_doc_settings. These are used in calculatins.
        # Changing these values would cause test cases to fail. This setting is used in most of test cases
        # but may not apply to all.
        cls.sow_doc_settings = SOWDocumentSettingsFactory(
            provider=cls.provider,
            engineering_hourly_cost=5,
            pm_hourly_cost=10,
            after_hours_cost=15,
            fixed_fee_variable=0.5,
            integration_technician_hourly_cost=5,
        )
        cls.fake = Faker()

    def test_calculate_engineering_hour_values(self):
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            engineering_hours=10,
            engineering_hourly_rate=10,
        ).calculate_engineering_hour_values()
        expected_values: Dict = dict(
            engineering_hour_customer_cost=100,
            engineering_hour_internal_cost=50,
            engineering_hour_margin=50,
            engineering_hour_margin_percent=50.0,
        )
        self.assertDictEqual(expected_values, result)

    def test_calculate_engineering_after_hour_values(self):
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            after_hours=10,
            after_hours_rate=200,
        ).calculate_engineering_after_hour_values()
        expected_values: Dict = dict(
            after_hours_customer_cost=2000,
            after_hours_internal_cost=150,
            after_hours_margin=1850,
            after_hours_margin_percent=92.5,
        )
        self.assertDictEqual(expected_values, result)

    def test_calculate_integration_technician_hour_values(self):
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            integration_technician_hours=10,
            integration_technician_hourly_rate=150,
        ).calculate_integration_technician_hour_values()
        expected_values: Dict = dict(
            integration_technician_customer_cost=1500,
            integration_technician_internal_cost=50,
            integration_technician_hours_margin=1450,
            integration_technician_hours_margin_percent=96.7,
        )
        self.assertDictEqual(expected_values, result)

    def test_calculate_project_management_hour_values(self):
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            project_management_hours=300,
            project_management_hourly_rate=10,
        ).calculate_project_management_hour_values()
        expected_values: Dict = dict(
            project_management_customer_cost=3000,
            project_management_internal_cost=3000,
            project_management_hours_margin=0,
            project_management_hours_margin_percent=0.0,
        )
        self.assertDictEqual(expected_values, result)

    @patch(
        "document.services.sow_calculations.ServiceCostCalculation.get_hourly_resources_internal_cost",
        return_value=200,
    )
    @patch(
        "document.services.sow_calculations.ServiceCostCalculation.get_hourly_resources_customer_cost",
        return_value=500,
    )
    def test_calculate_hourly_resources_values(
        self,
        get_hourly_resources_customer_cost,
        get_hourly_resources_internal_cost,
    ):
        hourly_resources: List[Dict] = [
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=300,
                internal_cost=100,
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=200,
                internal_cost=100,
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
        ]
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            hourly_resources=hourly_resources,
        ).calculate_hourly_resources_values()
        expected_values: Dict = dict(
            hourly_resources_customer_cost=500,
            hourly_resources_internal_cost=200,
            hourly_resources_margin=300,
            hourly_resources_margin_percent=60.0,
        )
        self.assertDictEqual(expected_values, result)
        get_hourly_resources_customer_cost.assert_called()
        get_hourly_resources_internal_cost.assert_called()

    def test_calculate_total_travel_cost(self):
        travels: List[Dict] = [
            dict(cost=100, description=self.fake.bs()),
            dict(cost=200, description=self.fake.bs()),
        ]
        result: int = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            travels=travels,
        ).calculate_total_travel_cost()
        self.assertEqual(result, 300)

    @patch(
        "document.services.sow_calculations.ServiceCostCalculation.get_total_contractor_internal_cost",
        return_value=300,
    )
    @patch(
        "document.services.sow_calculations.ServiceCostCalculation.get_total_contractor_customer_cost",
        return_value=400,
    )
    def test_calculate_contractor_values(
        self,
        get_total_contractor_customer_cost,
        get_total_contractor_internal_cost,
    ):
        contractors: List[Dict] = [
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=100,
                customer_cost=200,
                margin_percentage=50,
            ),
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=200,
                customer_cost=200,
                margin_percentage=0,
            ),
        ]
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            contractors=contractors,
        ).calculate_contractor_values()
        expected_values: Dict = dict(
            contractors_customer_cost=400,
            contractors_internal_cost=300,
            contractors_margin=100,
            contractors_margin_percent=25.0,
        )
        self.assertDictEqual(expected_values, result)
        get_total_contractor_customer_cost.assert_called()
        get_total_contractor_internal_cost.assert_called()

    def test_calculate_risk_budget_values(self):
        engineering_hour_calculations: Dict = dict(
            engineering_hour_customer_cost=500,
            engineering_hour_internal_cost=200,
        )
        after_hours_calculations: Dict = dict(
            after_hours_customer_cost=1000, after_hours_internal_cost=400
        )
        project_management_hours_calculations: Dict = dict(
            project_management_customer_cost=700,
            project_management_internal_cost=200,
        )
        hourly_resources_calculations: Dict = dict(
            hourly_resources_customer_cost=1000,
            hourly_resources_internal_cost=300,
        )
        integration_technician_calculations: Dict = dict(
            integration_technician_customer_cost=600,
            integration_technician_internal_cost=200,
        )
        result: Dict = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            engineering_hours=self.fake.pyint(),
            engineering_hourly_rate=self.fake.pyint(),
            after_hours=self.fake.pyint(),
            after_hours_rate=self.fake.pyint(),
            project_management_hours=self.fake.pyint(),
            project_management_hourly_rate=self.fake.pyint(),
        ).calculate_risk_budget_values(
            engineering_hour_calculations,
            after_hours_calculations,
            project_management_hours_calculations,
            hourly_resources_calculations,
            integration_technician_calculations,
        )
        expected_values: Dict = dict(
            risk_budget_customer_cost=1900,
            risk_budget_internal_cost=650,
            risk_budget_margin=1250,
            risk_budget_margin_percent=65.8,
        )
        self.assertDictEqual(expected_values, result)

    def test__calculate_margin_percent(self):
        margin: int = 40
        customer_cost: int = 90
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        )._calculate_margin_percent(margin, customer_cost)
        self.assertEqual(44.4, result)

    def test_get_hourly_resources_customer_cost(self):
        hourly_resources: List[Dict] = [
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=300,
                internal_cost=self.fake.pyint(),
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=200,
                internal_cost=self.fake.pyint(),
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
        ]
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            hourly_resources=hourly_resources,
        ).get_hourly_resources_customer_cost()
        self.assertEqual(500, result)

    def test_get_hourly_resources_internal_cost(self):
        hourly_resources: List[Dict] = [
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=self.fake.pyint(),
                internal_cost=150,
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
            dict(
                hours=self.fake.pyint(),
                margin=self.fake.pyint(),
                override=True,
                hourly_cost=self.fake.pyint(),
                hourly_rate=self.fake.pyint(),
                resource_id=self.fake.pyint(),
                customer_cost=self.fake.pyint(),
                internal_cost=150,
                resource_name=self.fake.company(),
                margin_percentage=self.fake.pyfloat(),
            ),
        ]
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            hourly_resources=hourly_resources,
        ).get_hourly_resources_internal_cost()
        self.assertEqual(300, result)

    def test_get_total_contractor_customer_cost(self):
        contractors: List[Dict] = [
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=100,
                customer_cost=200,
                margin_percentage=50,
            ),
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=200,
                customer_cost=200,
                margin_percentage=0,
            ),
        ]
        result: int = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            contractors=contractors,
        ).get_total_contractor_customer_cost()
        self.assertEqual(400, result)

    def test_get_total_contractor_internal_cost(self):
        contractors: List[Dict] = [
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=100,
                customer_cost=200,
                margin_percentage=50,
            ),
            dict(
                name=self.fake.name(),
                type=self.fake.word(),
                partner_cost=200,
                customer_cost=200,
                margin_percentage=0,
            ),
        ]
        result: int = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
            contractors=contractors,
        ).get_total_contractor_internal_cost()
        self.assertEqual(300, result)

    def test_get_risk_budget_customer_cost(self):
        engineering_hour_customer_cost: int = 1000
        after_hours_customer_cost: int = 1500
        project_management_customer_cost: int = 1500
        hourly_resources_customer_cost: Optional[int] = None
        integration_technician_customer_cost: int = 2000
        result: int = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        ).get_risk_budget_customer_cost(
            engineering_hour_customer_cost,
            after_hours_customer_cost,
            project_management_customer_cost,
            hourly_resources_customer_cost,
            integration_technician_customer_cost,
        )
        self.assertEqual(3000, result)

    def test_get_risk_budget_margin(self):
        risk_budget_customer_cost: int = 150
        engineering_hour_margin_percent: float = 75.0
        result: int = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        ).get_risk_budget_margin(
            risk_budget_customer_cost, engineering_hour_margin_percent
        )
        self.assertEqual(112, result)

    def test_get_risk_budget_margin_percent(self):
        risk_budget_margin: int = 250
        risk_budget_cutsomer_cost: int = 1000
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        ).get_risk_budget_margin_percent(
            risk_budget_margin, risk_budget_cutsomer_cost
        )
        self.assertEqual(25.0, result)

    def test_calculate_total_customer_cost_for_fixed_fee_docs(self):
        engineering_hour_customer_cost: int = 100
        after_hours_customer_cost: int = 200
        project_management_customer_cost: int = 300
        hourly_resources_customer_cost: Optional[int] = 400
        total_travel_cost: Optional[int] = 100
        contractors_customer_cost: Optional[int] = 200
        risk_budget_customer_cost: Optional[int] = 300
        integration_technician_customer_cost: int = 150
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        ).calculate_total_customer_cost(
            engineering_hour_customer_cost,
            after_hours_customer_cost,
            project_management_customer_cost,
            hourly_resources_customer_cost,
            total_travel_cost,
            contractors_customer_cost,
            risk_budget_customer_cost,
            integration_technician_customer_cost,
        )
        self.assertEqual(1750, result)

    def test_calculate_total_customer_cost_for_non_fixed_fee_docs(self):
        engineering_hour_customer_cost: int = 100
        after_hours_customer_cost: int = 200
        project_management_customer_cost: int = 300
        hourly_resources_customer_cost: Optional[int] = 400
        total_travel_cost: Optional[int] = 100
        contractors_customer_cost: Optional[int] = 200
        risk_budget_customer_cost: Optional[int] = 300
        integration_technician_customer_cost: int = 150
        result: float = ServiceCostCalculation(
            "T & M",
            self.sow_doc_settings,
        ).calculate_total_customer_cost(
            engineering_hour_customer_cost,
            after_hours_customer_cost,
            project_management_customer_cost,
            hourly_resources_customer_cost,
            total_travel_cost,
            contractors_customer_cost,
            risk_budget_customer_cost,
            integration_technician_customer_cost,
        )
        self.assertEqual(1150, result)

    def test_calculate_total_internal_cost_for_fixed_fee_document(self):
        engineering_hour_internal_cost: int = 100
        after_hours_internal_cost: int = 200
        project_management_internal_cost: int = 300
        hourly_resources_internal_cost: Optional[int] = 400
        contractors_internal_cost: Optional[int] = 500
        risk_budget_internal_cost: Optional[int] = 600
        integration_technician_internal_cost: int = 200
        result: float = ServiceCostCalculation(
            "Fixed Fee",
            self.sow_doc_settings,
        ).calculate_total_internal_cost(
            engineering_hour_internal_cost,
            after_hours_internal_cost,
            project_management_internal_cost,
            hourly_resources_internal_cost,
            contractors_internal_cost,
            risk_budget_internal_cost,
            integration_technician_internal_cost,
        )
        self.assertEqual(2300, result)

    def test_calculate_total_internal_cost_for_non_fixed_fee_document(self):
        engineering_hour_internal_cost: int = 100
        after_hours_internal_cost: int = 200
        project_management_internal_cost: int = 300
        hourly_resources_internal_cost: Optional[int] = 400
        contractors_internal_cost: Optional[int] = 500
        risk_budget_internal_cost: Optional[int] = 600
        integration_technician_internal_cost: int = 200
        result: float = ServiceCostCalculation(
            "T & M",
            self.sow_doc_settings,
        ).calculate_total_internal_cost(
            engineering_hour_internal_cost,
            after_hours_internal_cost,
            project_management_internal_cost,
            hourly_resources_internal_cost,
            contractors_internal_cost,
            risk_budget_internal_cost,
            integration_technician_internal_cost,
        )
        self.assertEqual(1200, result)

    def test_get_total_margin_percent(self):
        total_margin: int = 25
        total_customer_cost: int = 150
        result: float = ServiceCostCalculation(
            "T & M",
            self.sow_doc_settings,
        ).get_total_margin_percent(total_margin, total_customer_cost)
        self.assertEqual(16.7, result)

    def test_calculate_professional_service_values(self):
        engineering_hour_customer_cost: int = 200
        engineering_hour_internal_cost: int = 100
        after_hours_customer_cost: int = 400
        after_hours_internal_cost: int = 300
        project_management_customer_cost: int = 600
        project_management_internal_cost: int = 500
        integration_technician_customer_cost: int = 250
        integration_technician_internal_cost: int = 150
        result: Dict = ServiceCostCalculation(
            "T & M",
            self.sow_doc_settings,
        ).calculate_professional_service_values(
            engineering_hour_customer_cost,
            engineering_hour_internal_cost,
            after_hours_customer_cost,
            after_hours_internal_cost,
            project_management_customer_cost,
            project_management_internal_cost,
            integration_technician_customer_cost,
            integration_technician_internal_cost,
        )
        expected_result: Dict = dict(
            professional_services_customer_cost=1450,
            professional_services_internal_cost=1050,
            professional_services_margin=400,
            professional_services_margin_percent=27.6,
        )
        self.assertEqual(expected_result, result)

    def test_calculate_all_values_for_fixed_fee_document(self):
        # Delete the setting initialized in setUpTestData
        # Use new setting with convenient values
        self.provider.sow_doc_settings.delete()
        sow_doc_setting: SOWDocumentSettingsFactory = (
            SOWDocumentSettingsFactory.create(
                provider=self.provider,
                engineering_hourly_cost=150,
                pm_hourly_cost=85,
                after_hours_cost=165,
                integration_technician_hourly_cost=100,
                fixed_fee_variable=0.25,
            )
        )
        test_class = ServiceCostCalculation(
            "Fixed Fee",
            sow_doc_setting,
            engineering_hours=100,
            engineering_hourly_rate=275,
            after_hours=200,
            after_hours_rate=375,
            project_management_hours=300,
            project_management_hourly_rate=185,
            integration_technician_hours=150,
            integration_technician_hourly_rate=150,
            travels=[
                dict(cost=1000, description=self.fake.bs()),
                dict(cost=200, description=self.fake.bs()),
            ],
            contractors=[
                dict(
                    name=self.fake.name(),
                    type=self.fake.word(),
                    partner_cost=10000,
                    customer_cost=20000,
                    margin_percentage=50,
                ),
                dict(
                    name=self.fake.name(),
                    type=self.fake.word(),
                    partner_cost=5000,
                    customer_cost=25000,
                    margin_percentage=80,
                ),
            ],
            hourly_resources=[
                dict(
                    hours=self.fake.pyint(),
                    margin=self.fake.pyint(),
                    override=True,
                    hourly_cost=self.fake.pyint(),
                    hourly_rate=self.fake.pyint(),
                    resource_id=self.fake.pyint(),
                    customer_cost=80000,
                    internal_cost=40000,
                    resource_name=self.fake.company(),
                    margin_percentage=self.fake.pyfloat(),
                ),
                dict(
                    hours=self.fake.pyint(),
                    margin=self.fake.pyint(),
                    override=True,
                    hourly_cost=self.fake.pyint(),
                    hourly_rate=self.fake.pyint(),
                    resource_id=self.fake.pyint(),
                    customer_cost=150000,
                    internal_cost=75000,
                    resource_name=self.fake.company(),
                    margin_percentage=self.fake.pyfloat(),
                ),
            ],
        )
        self.assertEqual(
            test_class.calculated_values.get("total_margin"), 289950
        )
        self.assertEqual(
            test_class.calculated_values.get("total_internal_cost"), 269375
        )
        self.assertEqual(
            test_class.calculated_values.get("total_customer_cost"), 559325
        )
        self.assertEqual(
            test_class.calculated_values.get("total_margin_percent"), 51.8
        )

    def test_calculate_all_values_for_t_and_m_documents(self):
        # Delete the setting initialized in setUpTestData
        # Use new setting with convenient values
        self.provider.sow_doc_settings.delete()
        sow_doc_setting: SOWDocumentSettingsFactory = (
            SOWDocumentSettingsFactory.create(
                provider=self.provider,
                engineering_hourly_cost=150,
                pm_hourly_cost=85,
                after_hours_cost=165,
                fixed_fee_variable=0.25,
            )
        )
        test_class = ServiceCostCalculation(
            "T & M",
            sow_doc_setting,
            engineering_hours=100,
            engineering_hourly_rate=275,
            after_hours=200,
            after_hours_rate=375,
            project_management_hours=300,
            project_management_hourly_rate=185,
            travels=[],
            contractors=[],
            hourly_resources=[
                dict(
                    hours=self.fake.pyint(),
                    margin=self.fake.pyint(),
                    override=True,
                    hourly_cost=self.fake.pyint(),
                    hourly_rate=self.fake.pyint(),
                    resource_id=self.fake.pyint(),
                    customer_cost=80000,
                    internal_cost=40000,
                    resource_name=self.fake.company(),
                    margin_percentage=self.fake.pyfloat(),
                ),
                dict(
                    hours=self.fake.pyint(),
                    margin=self.fake.pyint(),
                    override=True,
                    hourly_cost=self.fake.pyint(),
                    hourly_rate=self.fake.pyint(),
                    resource_id=self.fake.pyint(),
                    customer_cost=150000,
                    internal_cost=75000,
                    resource_name=self.fake.company(),
                    margin_percentage=self.fake.pyfloat(),
                ),
            ],
        )
        self.assertEqual(
            test_class.calculated_values.get("total_margin"), 199500
        )
        self.assertEqual(
            test_class.calculated_values.get("total_internal_cost"), 188500
        )
        self.assertEqual(
            test_class.calculated_values.get("total_customer_cost"), 388000
        )
        self.assertEqual(
            test_class.calculated_values.get("total_margin_percent"), 51.4
        )
