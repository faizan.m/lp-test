from typing import Dict

import mock
from faker import Faker
from mock import patch

from document.models import (
    SOWDocument,
    DocumentAuthorTeamMemberMapping,
)
from document.services.sow_customer_team_operations import (
    SoWCustomerTeamOperationService,
)
from document.tests.factories import (
    DefaultDocumentCreatorRoleSettingFactory,
    SOWDocumentFactory,
    DocumentAuthorTeamMemberMappingFactory,
)
from test_utils import BaseProviderTestCase


class TestSoWCustomerTeamOperationService(BaseProviderTestCase):
    """
    Test class: SoWCustomerTeamOperationService
    """

    @classmethod
    def setUpTestData(cls):
        cls.service_class = SoWCustomerTeamOperationService()
        cls.fake = Faker()
        cls.setup_provider_and_user()

    def test_create_or_update_document_author_team_member_mapping(self):
        # Test creation
        team_member_crm_id: int = 10
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        mapping_exists: bool = DocumentAuthorTeamMemberMapping.objects.filter(
            provider=self.provider, sow_document=sow_document
        ).exists()
        self.assertEqual(mapping_exists, False)
        self.service_class.create_or_update_document_author_team_member_mapping(
            sow_document, team_member_crm_id
        )
        mapping: DocumentAuthorTeamMemberMapping = (
            sow_document.author_team_member_mapping
        )
        self.assertEqual(mapping.team_member_crm_id, team_member_crm_id)
        # Test update
        updated_team_member_crm_id: int = 20
        self.service_class.create_or_update_document_author_team_member_mapping(
            sow_document, updated_team_member_crm_id
        )
        self.assertEqual(
            mapping.team_member_crm_id, updated_team_member_crm_id
        )

    def test_get_default_role_crm_id_from_setting__for_FixedFee_and_T_and_M(
        self,
    ):
        DefaultDocumentCreatorRoleSettingFactory(
            provider=self.provider,
            default_sow_creator_role_id=10,
            default_change_request_creator_role_id=20,
        )
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, doc_type=SOWDocument.GENERAL
        )
        role_crm_id: int = (
            self.service_class.get_default_role_crm_id_from_setting(
                sow_document
            )
        )
        self.assertEqual(role_crm_id, 10)

    def test_get_default_role_crm_id_from_setting__for_change_request(self):
        DefaultDocumentCreatorRoleSettingFactory(
            provider=self.provider,
            default_sow_creator_role_id=10,
            default_change_request_creator_role_id=20,
        )
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, doc_type=SOWDocument.CHANGE_REQUEST
        )
        role_crm_id: int = (
            self.service_class.get_default_role_crm_id_from_setting(
                sow_document
            )
        )
        self.assertEqual(role_crm_id, 20)

    @patch(
        "document.services.sow_customer_team_operations.SoWCustomerTeamOperationService."
        "get_default_role_crm_id_from_setting",
        return_value=10,
    )
    def test_get_author_details(self, get_default_role_crm_id_from_setting):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            doc_type=SOWDocument.CHANGE_REQUEST,
            author=self.user,
        )
        expected_author_details: Dict = dict(
            role_crm_id=10,
            system_member_crm_id=self.user.profile.system_member_crm_id,
        )
        acutal_author_details: Dict = self.service_class.get_author_details(
            sow_document
        )
        self.assertEqual(expected_author_details, acutal_author_details)
        get_default_role_crm_id_from_setting.assert_called()

    def test_get_author_details_when_no_system_member_crm_id(self):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            doc_type=SOWDocument.CHANGE_REQUEST,
            author=self.user,
        )
        self.user.profile.system_member_crm_id = None
        acutal_author_details: Dict = self.service_class.get_author_details(
            sow_document
        )
        self.assertEqual(acutal_author_details, {})

    @patch(
        "document.services.sow_customer_team_operations.SoWCustomerTeamOperationService."
        "add_sow_document_author_to_customer_team",
        return_value=dict(
            team_member_crm_id=10,
            customer_crm_id=100,
            customer_name="Company Inc.",
            role_crm_id=2,
            role_name="Manager",
            system_member_crm_id=200,
            team_member_name="Michael Jackson",
        ),
    )
    def test_add_updated_author_to_customer_team(
        self, add_sow_document_author_to_customer_team
    ):
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.remove_member_from_company_team.return_value = (
            mock.MagicMock()
        )
        DefaultDocumentCreatorRoleSettingFactory(
            provider=self.provider,
        )
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        DocumentAuthorTeamMemberMappingFactory(
            sow_document=sow_document, provider=self.provider
        )
        member_details: Dict = (
            self.service_class.add_updated_author_to_customer_team(
                sow_document
            )
        )
        self.provider.erp_client.remove_member_from_company_team.assert_called()
        add_sow_document_author_to_customer_team.assert_called()
        self.assertEqual(member_details.get("team_member_crm_id"), 10)

    def test_add_updated_author_to_customer_team_when_setting_not_configured(
        self,
    ):
        sow_document: SOWDocument = SOWDocumentFactory(provider=self.provider)
        member_details: Dict = (
            self.service_class.add_updated_author_to_customer_team(
                sow_document
            )
        )
        self.assertEqual(member_details, {})

    @patch(
        "document.services.sow_customer_team_operations.SoWCustomerTeamOperationService."
        "create_or_update_document_author_team_member_mapping",
    )
    @patch(
        "document.services.sow_customer_team_operations.SoWCustomerTeamOperationService."
        "get_author_details",
        return_value=dict(role_crm_id=10, system_member_crm_id=200),
    )
    def test_add_sow_document_author_to_customer_team(
        self,
        get_author_details,
        create_or_update_document_author_team_member_mapping,
    ):
        DefaultDocumentCreatorRoleSettingFactory(
            provider=self.provider,
        )
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.add_new_member_to_company_team.return_value = dict(
            team_member_crm_id=10,
            customer_crm_id=100,
            customer_name="Company Inc.",
            role_crm_id=2,
            role_name="Manager",
            system_member_crm_id=200,
            team_member_name="Michael Jackson",
        )
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, author=self.user
        )
        member: Dict = (
            self.service_class.add_sow_document_author_to_customer_team(
                sow_document
            )
        )
        self.assertEqual(member.get("team_member_crm_id"), 10)
        get_author_details.assert_called()
        self.provider.erp_client.add_new_member_to_company_team.assert_called()
        create_or_update_document_author_team_member_mapping.assert_called()

    def test_add_sow_document_author_to_customer_team_when_setting_not_configured(
        self,
    ):
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, author=self.user
        )
        member: Dict = (
            self.service_class.add_sow_document_author_to_customer_team(
                sow_document
            )
        )
        self.assertEqual(member, {})
