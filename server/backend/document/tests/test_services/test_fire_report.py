from typing import Dict, Set
from unittest import mock

from document.services.fire_report import FireReportGenerator
from test_utils import BaseProviderTestCase
from document.tests.factories import (
    ConnectwiseServiceTicketFactory,
    SalesPurchaseOrderDataFactory,
    ConnectwisePurchaseOrderDataFactory,
    ConnectwisePurchaseOrderLineitemsDataFactory,
    ConnectwisePurchaseOrderFactory,
)


class TestFireReportGenerator(BaseProviderTestCase):
    """
    Test class: FireReportGenerator
    """

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.service_class = FireReportGenerator(cls.provider)

    def test__get_product_id_manufacturer_name_map(self):
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.get_product_catalogs.return_value = [
            dict(catalog_crm_id=1, manufacturer_name="Tyrell"),
            dict(catalog_crm_id=2, manufacturer_name="Lannister"),
        ]
        catalog_crm_ids: Set[int] = {1, 2, 3}
        result: Dict = (
            self.service_class._get_product_id_manufacturer_name_map(
                catalog_crm_ids
            )
        )
        self.assertEqual({1: "Tyrell", 2: "Lannister"}, result)

    @mock.patch(
        "document.services.fire_report.FireReportGenerator._get_product_id_manufacturer_name_map",
        return_value={11: "Tyrell", 12: "Lannister"},
    )
    def test_get_fire_report_data(self, _get_product_id_manufacturer_name_map):
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory(
                provider=self.provider,
                opportunity_crm_id=10,
                opportunity_name="Test 1",
            )
        )
        purchase_order: ConnectwisePurchaseOrder = (
            ConnectwisePurchaseOrderFactory(
                po_number="LP-1234", ticket=service_ticket
            )
        )
        sales_order: SalesPurchaseOrderData = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            opportunity_crm_id=service_ticket,
            shipping_site_crm_id=2,
            customer_po_number="LP123",
            crm_id=123,
        )
        purchase_order_data: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory(
                provider=self.provider,
                po_number=purchase_order,
                purchase_order_status_id=3,
                site_name="Fremont",
                sales_order=sales_order,
                crm_id=101,
            )
        )
        line_item_1: ConnectwisePurchaseOrderLineitemsData = (
            ConnectwisePurchaseOrderLineitemsDataFactory(
                provider=self.provider,
                po_number=purchase_order,
                unit_cost=150,
                product_id="PART-123",
                description="Service product",
                serial_numbers=["SR1", "SR2"],
                tracking_numbers=["TR1", "TR2"],
                shipping_method="FedZ",
                product_crm_id=11,
                purchase_order_crm_id=101,
            )
        )
        line_item_2: ConnectwisePurchaseOrderLineitemsData = (
            ConnectwisePurchaseOrderLineitemsDataFactory(
                provider=self.provider,
                po_number=purchase_order,
                unit_cost=100,
                product_id="PART-345",
                description="Software product",
                serial_numbers=["SR1", "SR2"],
                tracking_numbers=["TR1", "TR2"],
                shipping_method="OTA",
                product_crm_id=12,
                purchase_order_crm_id=101,
            )
        )
        line_item_3: ConnectwisePurchaseOrderLineitemsData = (
            ConnectwisePurchaseOrderLineitemsDataFactory(
                provider=self.provider, po_number=purchase_order, unit_cost=0
            )
        )
        result: List[Dict] = self.service_class.get_fire_report_data({})
        # 4 because each row has to have a serial nuber
        self.assertEqual(4, len(result))
