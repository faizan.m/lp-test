import factory
import uuid
from typing import List, Dict
from factory.fuzzy import FuzzyChoice, FuzzyText
from document.models import (
    ConnectwiseServiceTicket,
    ConnectwisePurchaseOrder,
    ConnectwisePurchaseOrderData,
    SalesPurchaseOrderData,
    ProductsItemData,
    ConnectwisePurchaseOrderLineitemsData,
    OperationEmailTemplateSettings,
    OperationDashboardSettings,
    SOWDocument,
    SOWCategory,
    SOWTemplate,
    SOWDocumentSettings,
    ServiceCategory,
    SOWBaseTemplate,
    ServiceCatalog,
    ConnectwiseServiceTicketParseFailed,
    SOWVendorOnboardingSetting,
    SOWVendorOnboardingAttachments,
    DefaultDocumentCreatorRoleSetting,
    DocumentAuthorTeamMemberMapping,
    SoWHourlyResourcesDescriptionMapping,
    FireReportPartnerSite,
    FireReportSetting,
    PartnerSiteSyncError,
    SoWOpportunityTeamMapping,
    AutomatedWeeklyRecapSetting,
    ConnectwiseOpportunityData,
    Client360DashboardSetting,
)
import datetime
from faker import Faker
from accounts.tests.factories import (
    ProviderFactory,
    CustomerFactory,
    ProviderUserFactory,
)
import random

fake = Faker()


def get_random_purchase_order_number() -> str:
    """
    Generated string is alphanumeric and can be used as po number
    E.g. 2429A11558
    """
    return FuzzyText().fuzz()


SAMPLE_PRODUCT_IDS: List[str] = [
    "BSURGBEBHMH",
    "MCLDGBCBMLH",
    "PGXZGBDB81G",
    "ICHCGBTW1LD",
    "QOBTGBGZMQW",
    "XPSFGBNDKKV",
    "QIDTGB2LA40",
    "SQJUGBWYQJ9",
    "WAOXGBFVC8R",
    "EHYPGBWSWIT",
    "MXNUGBFAMVK",
    "HHUOGBY0CZ0",
    "YVWBGBK2EQK",
    "OQPRGBAC5UT",
    "OBZZGB0M9FI",
]

CW_PO_STATUSES: List[str] = [
    "New",
    "Closed",
    "Order Placed",
    "Waiting on SMNT",
    "Problems",
]
CW_PO_STATUS_IDS: List[int] = [1, 3, 4, 5, 6]

CW_SALES_ORDER_STATUSES: List[str] = [
    "New",
    "Order Placed",
    "Shipped (Invoice Order)",
    "Waiting on SMARTnet",
]
CW_SALES_ORDER_STATUS_IDS: List[int] = [
    1,
    3,
    7,
    12,
]


MOCK_CUSTOMER_STATUSES: List[str] = ["Active", "Not-Active"]


def get_mock_territories_for_sow_vendor_onboarding_settings() -> List[Dict]:
    return [
        dict(territory_crm_id=fake.pyint(), territory_name=fake.city()),
        dict(territory_crm_id=fake.pyint(), territory_name=fake.city()),
    ]


def get_mock_customer_type_for_sow_vendor_onboarding_settings() -> Dict:
    return dict(
        customer_type_crm_id=fake.pyint(), customer_type_name=fake.job()
    )


def get_mock_cw_statuses_for_sow_vendor_listing() -> List[Dict]:
    return [
        dict(
            customer_status_name=random.choice(MOCK_CUSTOMER_STATUSES),
            customer_status_crm_id=fake.pyint(),
        ),
        dict(
            customer_status_name=random.choice(MOCK_CUSTOMER_STATUSES),
            customer_status_crm_id=fake.pyint(),
        ),
    ]


def get_mock_cw_status_for_vendor_creation() -> Dict:
    return dict(
        customer_status_crm_id=fake.pyint(),
        customer_status_name=random.choice(MOCK_CUSTOMER_STATUSES),
    )


def get_mock_service_board_ids_list() -> List[int]:
    return [fake.pyint() for i in range(3)]


class OperationEmailTemplateSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OperationEmailTemplateSettings

    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    provider = factory.SubFactory(ProviderFactory)
    purchase_order_status_ids = []
    extra_config = {}
    contact_type_connectwise_id = fake.pyint()
    ticket_board_and_status_mapping = []
    opportunity_statuses = []
    opportunity_stages = []


class ConnectwiseServiceTicketFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConnectwiseServiceTicket

    provider = factory.SubFactory(ProviderFactory)
    owner = factory.SubFactory(ProviderUserFactory)
    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    # Using Sequence() to keep CRM IDs unique
    ticket_crm_id = factory.Sequence(lambda n: n)
    owner_crm_id = factory.Sequence(lambda n: n)
    summary = fake.text(max_nb_chars=10)
    service_board_crm_id = factory.Sequence(lambda n: n)
    status_crm_id = factory.Sequence(lambda n: n)
    status_name = fake.text(max_nb_chars=10)
    company_crm_id = factory.Sequence(lambda n: n)
    company_name = fake.company()
    connectwise_last_data_update = fake.date_this_year()
    additional_data = {}
    last_ticket_note = fake.text(max_nb_chars=10)
    opportunity_crm_id = factory.Sequence(lambda n: n)
    opportunity_name = fake.text(max_nb_chars=10)


class ConnectwisePurchaseOrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConnectwisePurchaseOrder
        django_get_or_create = ("po_number",)

    po_number = factory.LazyFunction(get_random_purchase_order_number)
    ticket = factory.SubFactory(ConnectwiseServiceTicketFactory)


class SalesPurchaseOrderDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SalesPurchaseOrderData

    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    provider = factory.SubFactory(ProviderFactory)
    crm_id = factory.Sequence(lambda n: n)
    customer_po_number = factory.LazyFunction(get_random_purchase_order_number)
    order_date = fake.date_this_year()
    opportunity_name = fake.name()
    opportunity_crm_id = factory.SubFactory(ConnectwiseServiceTicketFactory)
    site_crm_id = factory.Sequence(lambda n: n)
    site_name = fake.street_name()
    customer = factory.SubFactory(CustomerFactory)
    company_name = fake.company()
    status_name = FuzzyChoice(choices=CW_SALES_ORDER_STATUSES)
    status_id = FuzzyChoice(choices=CW_SALES_ORDER_STATUS_IDS)
    product_ids = {}
    bill_closed_flag = fake.pybool()
    invoice_ids = []
    shipping_contact_crm_id = factory.Sequence(lambda n: n)
    shipping_contact_name = fake.name()
    shipping_site_crm_id = factory.Sequence(lambda n: n)


class ConnectwisePurchaseOrderDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConnectwisePurchaseOrderData
        django_get_or_create = ("po_number",)

    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    provider = factory.SubFactory(ProviderFactory)
    po_number = factory.SubFactory(ConnectwisePurchaseOrderFactory)
    crm_id = factory.Sequence(lambda n: n)
    vendor_company_name = fake.company()
    vendor_company_id = factory.Sequence(lambda n: n)
    additional_data = {}
    purchase_order_status_id = FuzzyChoice(choices=CW_PO_STATUS_IDS)
    purchase_order_status_name = FuzzyChoice(choices=CW_PO_STATUSES)
    internal_notes = fake.text(max_nb_chars=10)
    po_date = fake.date_time_this_decade()
    site_name = fake.street_name()
    site_id = factory.Sequence(lambda n: n)
    sales_order = factory.SubFactory(SalesPurchaseOrderDataFactory)


class ProductsItemDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductsItemData

    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    provider = factory.SubFactory(ProviderFactory)
    crm_id = factory.Sequence(lambda n: n)
    product_id = FuzzyChoice(choices=SAMPLE_PRODUCT_IDS)
    description = fake.text(max_nb_chars=200)
    quantity = fake.pyint()
    sales_order = factory.SubFactory(SalesPurchaseOrderDataFactory)
    invoice_id = factory.Sequence(lambda n: n)
    cancelled_quantity = fake.pyint()
    cost = fake.pyfloat(right_digits=2, positive=True)
    sequence_number = factory.Sequence(lambda n: n)


class ConnectwisePurchaseOrderLineitemsDataFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = ConnectwisePurchaseOrderLineitemsData

    created_on = factory.LazyFunction(datetime.datetime.now)
    updated_on = factory.LazyFunction(datetime.datetime.now)
    provider = factory.SubFactory(ProviderFactory)
    po_number = None
    crm_id = factory.Sequence(lambda n: n)
    description = fake.text(max_nb_chars=500)
    line_number = fake.pyint()
    product_crm_id = factory.Sequence(lambda n: n)
    product_id = FuzzyChoice(choices=SAMPLE_PRODUCT_IDS)
    purchase_order_crm_id = factory.Sequence(lambda n: n)
    ordered_quantity = fake.pyint()
    received_quantity = fake.pyint()
    serial_numbers = []
    ship_date = fake.date_time_this_month()
    shipping_method = fake.text(max_nb_chars=50)
    received_date = fake.date_time_this_month()
    received_status = None
    last_updated_in_cw = fake.date_time_this_month()
    additional_data = {}
    tracking_numbers = []
    is_closed = fake.pybool()
    unit_cost = fake.pyfloat(right_digits=2, positive=True)
    expected_ship_date = fake.future_datetime()


class OperationDashboardSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OperationDashboardSettings

    provider = factory.SubFactory(ProviderFactory)
    business_unit_id = fake.pyint()
    service_boards = factory.LazyFunction(get_mock_service_board_ids_list)
    purchase_order_status_ids = factory.LazyFunction(
        get_mock_service_board_ids_list
    )
    ticket_status_ids = factory.LazyFunction(get_mock_service_board_ids_list)
    po_custom_field_id = fake.pyint()


class SOWCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SOWCategory
        django_get_or_create = [
            "name",
        ]

    name = fake.text(max_nb_chars=10)


class SOWDocumentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SOWDocument

    created_on = None
    updated_on = None
    name = fake.name()
    provider = factory.SubFactory(ProviderFactory)
    json_config = {}
    customer = factory.SubFactory(CustomerFactory)
    user = factory.SubFactory(ProviderUserFactory)
    author = factory.SubFactory(ProviderUserFactory)
    updated_by = factory.SubFactory(ProviderUserFactory)
    quote_id = factory.Sequence(lambda n: n)
    forecast_id = factory.Sequence(lambda n: n)
    category = factory.SubFactory(SOWCategoryFactory)
    doc_type = FuzzyChoice(
        choices=[
            SOWDocument.T_AND_M,
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]
    ).fuzz()
    major_version = 1
    minor_version = 0
    margin = fake.pyfloat()

    @classmethod
    def _create(cls, target_class, *args, **kwargs):
        updated_on = kwargs.pop("updated_on", None)
        created_on = kwargs.pop("created_on", None)
        obj = super(SOWDocumentFactory, cls)._create(
            target_class, *args, **kwargs
        )
        # Django model overrides the updated_on and created_on values.
        # This updates the object with actual passed values to factory.
        if updated_on is not None:
            SOWDocument.objects.filter(
                id=obj.id, provider_id=obj.provider.id
            ).update(updated_on=updated_on)
        if created_on is not None:
            SOWDocument.objects.filter(
                id=obj.id, provider_id=obj.provider.id
            ).update(created_on=created_on)
        return obj


class SOWDocumentSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SOWDocumentSettings

    provider = factory.SubFactory(ProviderFactory)
    fixed_fee_variable = fake.pyfloat()
    engineering_hourly_cost = fake.pyfloat()
    pm_hourly_cost = fake.pyfloat()
    engineering_hourly_rate = fake.pyfloat()
    after_hours_rate = fake.pyfloat()
    after_hours_cost = fake.pyfloat()
    project_management_hourly_rate = fake.pyfloat()
    project_management_t_and_m = fake.text(max_nb_chars=5)
    project_management_fixed_fee = fake.text(max_nb_chars=5)
    terms_fixed_fee = fake.text(max_nb_chars=5)
    terms_t_and_m = fake.text(max_nb_chars=5)
    total_no_of_sites_statement = (
        "Up to ##{no_of_sites} sites will be included in this project."
    )
    total_no_of_cutovers_statement = (
        "Up to ##{no_of_cutovers} cutovers has been accounted for in this project. "
        "Additional cutover requests will require a change order and associated costs or hours."
    )
    change_request_t_and_m_terms = fake.text(max_nb_chars=5)
    change_request_fixed_fee_terms = fake.text(max_nb_chars=5)
    change_request_t_and_m_terms_markdown = fake.text(max_nb_chars=5)
    change_request_fixed_fee_terms_markdown = fake.text(max_nb_chars=5)
    engineering_hours_description = fake.text(max_nb_chars=5)
    after_hours_description = fake.text(max_nb_chars=5)
    project_management_hours_description = fake.text(max_nb_chars=5)
    integration_technician_hourly_cost = fake.pyfloat()
    integration_technician_hourly_rate = fake.pyfloat()
    integration_technician_description = fake.text(max_nb_chars=5)


class ConnectwiseServiceTicketParseFailedFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = ConnectwiseServiceTicketParseFailed

    provider = factory.SubFactory(ProviderFactory)
    ticket_crm_id = factory.Sequence(lambda n: n)
    summary = fake.text(max_nb_chars=5)
    is_ignored = False


class SOWVendorOnboardingSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SOWVendorOnboardingSetting

    provider = factory.SubFactory(ProviderFactory)
    territories = factory.LazyFunction(
        get_mock_territories_for_sow_vendor_onboarding_settings
    )
    customer_type = factory.LazyFunction(
        get_mock_customer_type_for_sow_vendor_onboarding_settings
    )
    vendor_onboarding_email_alias = fake.email()
    vendor_onboarding_email_subject = fake.text(max_nb_chars=10)
    vendor_onboarding_email_body = fake.text(max_nb_chars=10)
    vendor_list_statuses = factory.LazyFunction(
        get_mock_cw_statuses_for_sow_vendor_listing
    )
    vendor_create_status = factory.LazyFunction(
        get_mock_cw_status_for_vendor_creation
    )


class SOWVendorOnboardingAttachmentsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SOWVendorOnboardingAttachments

    provider = factory.SubFactory(ProviderFactory)
    sow_vendor_onboarding_setting = factory.SubFactory(
        SOWVendorOnboardingSettingFactory
    )
    name = fake.file_name()
    attachment = factory.django.FileField()


class DefaultDocumentCreatorRoleSettingFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = DefaultDocumentCreatorRoleSetting

    provider = factory.SubFactory(ProviderFactory)
    default_sow_creator_role_id = fake.pyint()
    default_change_request_creator_role_id = fake.pyint()


class DocumentAuthorTeamMemberMappingFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = DocumentAuthorTeamMemberMapping

    provider = factory.SubFactory(ProviderFactory)
    sow_document = factory.SubFactory(SOWDocumentFactory)
    team_member_crm_id = fake.pyint()


class SoWHourlyResourcesDescriptionMappingFactory(
    factory.django.DjangoModelFactory
):
    class Meta:
        model = SoWHourlyResourcesDescriptionMapping

    provider = factory.SubFactory(ProviderFactory)
    vendor_name = fake.company()
    vendor_crm_id = factory.Sequence(lambda n: n)
    resource_description = fake.text(max_nb_chars=5)


class FireReportPartnerSiteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FireReportPartnerSite

    provider = factory.SubFactory(ProviderUserFactory)
    name = fake.building_number()
    address_line_1 = fake.street_address()
    address_line_2 = fake.street_name()
    city = fake.city()
    state = fake.state()
    state_crm_id = fake.pyint()
    country = fake.country()
    country_crm_id = fake.pyint()
    zip = fake.postcode()
    phone_number = fake.msisdn()


class FireReportSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FireReportSetting

    provider = factory.SubFactory(ProviderUserFactory)
    receiver_email_ids = ["v5test@yopmail.com", "lptestuser@yopmail.com"]
    subject = "Fire report generation"
    frequency = fake.random_element(
        [
            FireReportSetting.DAILY,
            FireReportSetting.MONTHLY,
            FireReportSetting.WEEKLY,
            FireReportSetting.YEARLY,
            FireReportSetting.NEVER,
        ]
    )
    next_run = None
    past_days = fake.pyint()


class PartnerSiteSyncErrorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PartnerSiteSyncError

    provider = factory.SubFactory(ProviderUserFactory)
    operation = FuzzyChoice(
        choices=[
            PartnerSiteSyncError.CREATE,
            PartnerSiteSyncError.UPDATE,
            PartnerSiteSyncError.DELETE,
        ]
    )
    customer = factory.SubFactory(CustomerFactory)
    customer_crm_id = fake.pyint()
    site_crm_id = fake.pyint()
    site_name = fake.name()
    site_payload = {}
    errors = {}


class SoWOpportunityTeamMappingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SoWOpportunityTeamMapping

    provider = factory.SubFactory(ProviderFactory)
    sow_document = factory.SubFactory(SOWDocumentFactory)
    author = factory.SubFactory(ProviderUserFactory)
    team_crm_id = fake.pyint(min_value=1)


class AutomatedWeeklyRecapSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AutomatedWeeklyRecapSetting

    provider = factory.SubFactory(ProviderFactory)
    receiver_email_ids = [fake.email(), fake.email(), fake.email()]
    sender = factory.SubFactory(ProviderUserFactory)
    weekly_schedule = random.choices(
        [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ],
        k=2,
    )
    contact_type_crm_id = fake.pyint(min_value=1)
    email_subject = fake.text(max_nb_chars=10)
    email_body_text = fake.text(max_nb_chars=10)
    is_enabled = True


class ConnectwiseOpportunityDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConnectwiseOpportunityData

    provider = factory.SubFactory(ProviderFactory)
    name = fake.text(max_nb_chars=10)
    crm_id = factory.Sequence(lambda n: n)
    status_crm_id = fake.pyint(min_value=1, max_value=5)
    status_name = random.choice(
        [
            "Open",
            "Won",
            "Lost",
            "No decision",
            "Won - Errors corrected",
            "Rejected - See Notes Tab",
            "Order Approved",
        ]
    )
    customer_po = fake.ean(length=8)
    customer_name = fake.company()
    customer_crm_id = factory.Sequence(lambda n: n)
    closed_date = fake.date_time_this_month()


class Client360DashboardSettingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Client360DashboardSetting

    provider = factory.SubFactory(ProviderFactory)
    won_opp_status_crm_ids = [
        random.choice([1, 2, 3]),
        random.choice([4, 5, 6]),
    ]
    open_po_status_crm_ids = [
        random.choice([1, 2, 3]),
        random.choice([4, 5, 6]),
    ]
