from typing import Dict
from unittest.mock import MagicMock
from unittest.mock import patch

from accounts.models import Provider
from accounts.tests.factories import ProviderFactory
from document.tasks.customer_order_tracking import (
    send_order_tracking_weekly_recap_email,
    send_automated_order_tracking_weekly_recap_emails,
)
from document.tests.factories import (
    AutomatedWeeklyRecapSettingFactory,
    CustomerFactory,
)
from test_utils import BaseProviderTestCase


class TestWeeklyRecapEmailTasks(BaseProviderTestCase):
    @patch(
        "document.services.order_tracking.WeeklyRecapService.send_order_tracking_weekly_recap_email"
    )
    @patch(
        "document.tasks.customer_order_tracking.logger",
        return_value=MagicMock(),
    )
    def test_send_automated_order_tracking_weekly_recap_emails(
        self, logger, send_weekly_recap_email
    ):
        provider: Provider = ProviderFactory(
            name="Circuit",
            is_active=True,
            integration_statuses=dict(
                crm_authentication_configured=True,
                device_manufacturer_api_configured=True,
            ),
        )
        AutomatedWeeklyRecapSettingFactory(
            provider=provider,
            weekly_schedule=[
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday",
            ],
        )
        CustomerFactory.create_batch(
            2, provider=provider, is_active=True, is_deleted=False
        )
        send_weekly_recap_email.side_effect = [
            (True, None),
            (
                False,
                "Open sales orders, open purchase orders not found for the customer.",
            ),
        ]
        # Trigger the task as normal function
        send_automated_order_tracking_weekly_recap_emails()
        send_weekly_recap_email.assert_called()

    @patch(
        "document.services.order_tracking.WeeklyRecapService.send_order_tracking_weekly_recap_email"
    )
    @patch(
        "document.tasks.customer_order_tracking.logger",
        return_value=MagicMock(),
    )
    def test_send_order_tracking_weekly_recap_email(
        self, logger, send_weekly_recap_email
    ):
        # Test the case when Weekly Recap Setting not configured for the provider
        # Trigger the task as a normal function
        provider: Provider = ProviderFactory(
            name="Circuit",
            is_active=True,
            integration_statuses=dict(
                crm_authentication_configured=True,
                device_manufacturer_api_configured=True,
            ),
        )
        result: Dict = send_order_tracking_weekly_recap_email(
            provider.id, True, []
        )
        expected_result: Dict = dict(
            operation_status="FAILED",
            reason=f"Order tracking weekly recap setting not configured for the Provider: {provider.name}! "
            f"Weekly Recap emails will not be sent.",
            payload={},
        )
        self.assertEqual(expected_result, result)
        # Test when setting is configured
        AutomatedWeeklyRecapSettingFactory(provider=provider)
        customer_1 = CustomerFactory(provider=provider, crm_id=1)
        customer_2 = CustomerFactory(provider=provider, crm_id=2)
        send_weekly_recap_email.side_effect = [
            (True, None),
            (
                False,
                "Open sales orders, open purchase orders not found for the customer.",
            ),
        ]
        result = send_order_tracking_weekly_recap_email(
            provider.id, False, [1, 2]
        )
        expected_result: Dict = dict(
            operation_status="SUCCESS",
            reason="",
            payload=dict(
                skipped_customer_details=[
                    dict(
                        customer_name=customer_2.name,
                        customer_crm_id=customer_2.crm_id,
                        reason="Open sales orders, open purchase orders not found for the customer.",
                    )
                ]
            ),
        )
        self.assertEqual(expected_result, result)
