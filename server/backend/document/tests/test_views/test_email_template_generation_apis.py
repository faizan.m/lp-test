from django.urls import reverse
from faker import Faker
from rest_framework.request import Request
from rest_framework.response import Response
from unittest.mock import patch
from accounts.tests.factories import CustomerFactory
from document.models import ConnectwiseServiceTicket
from document.views.email_generation import (
    SalesOrderEmailTemplateGenerationAPIView,
    ServiceTicketsListAPIView,
    CustomerOpportunitiesListAPIView,
)
from accounts.models import Provider
from test_utils import BaseProviderTestCase, TestClientMixin
from document.tests.factories import (
    SalesPurchaseOrderDataFactory,
    OperationEmailTemplateSettingsFactory,
    ConnectwiseServiceTicketFactory,
)
from document.tests.utils import (
    get_valid_ticket_board_and_status_mapping,
    get_valid_service_tickets,
    get_valid_opportunity_stages_mapping,
    get_valid_opportunity_statuses_mapping,
    get_valid_customer_quotes,
    create_email_template_generation_scenario,
    get_sample_template_data,
    get_sample_csv_tables,
)

# Be careful while modifying certain imports as it may cause namespace issues
# during mocking and cause tests to fail

fake = Faker()


class TestSOEmailTemplateGenerationAPI(BaseProviderTestCase):
    """
    Test email template generation endpoint
    """

    so_email_template_generation_url = reverse(
        "api_v1_endpoints:providers:so-email-template-generation"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        OperationEmailTemplateSettingsFactory.create(
            provider=cls.provider,
            ticket_board_and_status_mapping=get_valid_ticket_board_and_status_mapping(),
            opportunity_statuses=get_valid_opportunity_statuses_mapping(),
            opportunity_stages=get_valid_opportunity_stages_mapping(),
        )

    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "get_sales_order_email_template_data",
        return_value=(get_sample_template_data(), get_sample_csv_tables()),
    )
    def test_email_template_generation_when_settings_are_configured(
        self, mock_service_call
    ):
        """
        API should return email template data and csv tables when settings are configured
        and data exists for line items
        """
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory.create(provider=self.provider, opportunity_crm_id=100)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.so_email_template_generation_url,
            self.provider,
            self.user,
            data=dict(opportunity_crm_id=service_ticket.opportunity_crm_id),
        )
        view = SalesOrderEmailTemplateGenerationAPIView().as_view()
        create_email_template_generation_scenario(
            self.provider, opportunity_crm_id=service_ticket.opportunity_crm_id
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_falilure_of_email_template_generation_when_no_opportunity_crm_id(
        self,
    ):
        """
        API should raise 400 error when opportunity_crm_id is not in request body
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.so_email_template_generation_url,
            self.provider,
            self.user,
            data=dict(),
        )
        view = SalesOrderEmailTemplateGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_falilure_of_email_template_generation_when_no_sales_order_exists(
        self,
    ):
        """
        Test should raise 404 error when Sales Order does not exist for given opportunty
        """
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory.create(provider=self.provider)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.so_email_template_generation_url,
            self.provider,
            self.user,
            data=dict(opportunity_crm_id=service_ticket.opportunity_crm_id),
        )
        view = SalesOrderEmailTemplateGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 404)

    def test_falilure_of_email_template_generation_when_no_purchase_order_exists(
        self,
    ):
        """
        Test should raise 404 error when Purchase Orders for the Sales Order does not exist or is not linked
        """
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory.create(provider=self.provider)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.so_email_template_generation_url,
            self.provider,
            self.user,
            data=dict(opportunity_crm_id=service_ticket.opportunity_crm_id),
        )
        SalesPurchaseOrderDataFactory.create(
            provider=self.provider, opportunity_crm_id_id=service_ticket.opportunity_crm_id
        )
        view = SalesOrderEmailTemplateGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 404)


class TestCustomerServiceTicketListingAPI(BaseProviderTestCase):
    """
    Test customer service ticket listing endpoint
    """

    service_ticket_listing_url: str = (
        "api_v1_endpoints:providers:so-email-template-customer-service-tickets"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        OperationEmailTemplateSettingsFactory.create(
            provider=cls.provider,
            ticket_board_and_status_mapping=get_valid_ticket_board_and_status_mapping(),
            opportunity_statuses=get_valid_opportunity_statuses_mapping(),
            opportunity_stages=get_valid_opportunity_stages_mapping(),
        )

    def test_service_ticket_listing(self):
        """
        API should list service tickets when settings are properly configured
        """
        with patch(
            "document.tests.test_views.test_email_template_generation_apis."
            "Provider.erp_client"
        ) as mock_erp_client:
            customer = CustomerFactory(
                provider=self.provider, crm_id=fake.pyint()
            )
            # Mock erp client method calls
            self.provider.erp_client.return_value = mock_erp_client
            mock_erp_client.get_service_tickets.return_value = (
                get_valid_service_tickets()
            )

            request: Request = TestClientMixin.api_factory_client_request(
                "GET",
                self.service_ticket_listing_url,
                self.provider,
                self.user,
            )
            view = ServiceTicketsListAPIView().as_view()
            response: Response = view(request, customer_id=customer.id)
            self.assertEqual(response.status_code, 200)

    def test_failure_of_service_ticket_listing_when_ticket_board_mapping_is_not_configured(
        self,
    ):
        """
        API should raise appropriate erros when board and service ticket status ids
        are not configured for the provider
        """
        customer = CustomerFactory(provider=self.provider, crm_id=fake.pyint())
        # Delete board and service ticket status mapping for test
        self.provider.operation_email_template_settings.ticket_board_and_status_mapping = (
            []
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.service_ticket_listing_url,
            self.provider,
            self.user,
        )
        view = ServiceTicketsListAPIView().as_view()
        response: Response = view(request, customer_id=customer.id)
        self.assertEqual(response.status_code, 412)


class TestCustomerOpportunitiesListAPI(BaseProviderTestCase):
    """
    Test customer quote listing endpoint
    """

    customer_quote_list_endpoint_name: str = (
        "api_v1_endpoints:providers:so-email-template-customer-quotes"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        OperationEmailTemplateSettingsFactory.create(
            provider=cls.provider,
            opportunity_statuses=get_valid_opportunity_statuses_mapping(),
            opportunity_stages=get_valid_opportunity_stages_mapping(),
        )

    def test_customer_quote_listing(self):
        """
        API should list customer quotes settings are properly configured
        """
        with patch(
            "document.tests.test_views.test_email_template_generation_apis."
            "Provider.erp_client"
        ) as mock_erp_client:
            customer = CustomerFactory(
                provider=self.provider, crm_id=fake.pyint()
            )
            self.provider.erp_client.return_value = mock_erp_client
            mock_erp_client.get_company_quotes.return_value = (
                get_valid_customer_quotes()
            )
            request_url: str = reverse(
                self.customer_quote_list_endpoint_name,
                kwargs={"customer_crm_id": customer.id},
            )
            request: Request = TestClientMixin.api_factory_client_request(
                "GET",
                request_url,
                self.provider,
                self.user,
            )
            view = CustomerOpportunitiesListAPIView().as_view()
            response: Response = view(request, customer_id=customer.id)
            self.assertEqual(response.status_code, 200)

    def test_failure_of_customer_quote_listing_when_setting_not_configured(
        self,
    ):
        """
        API should raise errors when opportunity status and statge mapping
        is not configured for the Provider
        """
        customer = CustomerFactory(provider=self.provider, crm_id=fake.pyint())
        # Delete opportunity statuses mapping for test
        self.provider.operation_email_template_settings.opportunity_statuses = (
            []
        )
        request_url: str = reverse(
            self.customer_quote_list_endpoint_name,
            kwargs={"customer_crm_id": customer.id},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            request_url,
            self.provider,
            self.user,
        )
        view = CustomerOpportunitiesListAPIView().as_view()
        response: Response = view(request, customer_id=customer.id)
        self.assertEqual(response.status_code, 412)
