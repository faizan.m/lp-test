from typing import Dict

from django.urls import reverse
from rest_framework.request import Request
from rest_framework.response import Response

from document.tests.factories import SOWDocumentSettingsFactory
from document.views.sow_settings import SOWSettingsRetrieveUpdateAPIView
from test_utils import BaseProviderTestCase, TestClientMixin
from faker import Faker


class TestSOWSettingsRetrieveUpdateAPIView(BaseProviderTestCase):
    """
    Test view: SOWSettingsRetrieveUpdateAPIView
    """

    sow_doc_setting_url: str = reverse(
        "api_v1_endpoints:providers:retrieve-update-sow-doc-settings"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_update_to_sow_doc_settings(self):
        fake = Faker()
        setting = SOWDocumentSettingsFactory(provider=self.provider)
        request_body: Dict = dict(
            fixed_fee_variable=fake.pyint(),
            engineering_hourly_cost=100,
            pm_hourly_cost=fake.pyfloat(),
            engineering_hourly_rate=fake.pyfloat(),
            after_hours_rate=fake.pyfloat(),
            after_hours_cost=fake.pyfloat(),
            project_management_hourly_rate=fake.pyfloat(),
            project_management_t_and_m=fake.text(max_nb_chars=5),
            project_management_fixed_fee=fake.text(max_nb_chars=5),
            terms_fixed_fee=fake.text(max_nb_chars=5),
            terms_t_and_m=fake.text(max_nb_chars=5),
            change_request_t_and_m_terms=fake.text(max_nb_chars=5),
            change_request_fixed_fee_terms=fake.text(max_nb_chars=5),
            change_request_t_and_m_terms_markdown=fake.text(max_nb_chars=5),
            change_request_fixed_fee_terms_markdown=fake.text(max_nb_chars=5),
            engineering_hours_description=fake.text(max_nb_chars=5),
            after_hours_description=fake.text(max_nb_chars=5),
            project_management_hours_description=fake.text(max_nb_chars=5),
            integration_technician_hourly_cost=fake.pyfloat(),
            integration_technician_hourly_rate=fake.pyfloat(),
            integration_technician_description=fake.text(max_nb_chars=5),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.sow_doc_setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWSettingsRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(100, setting.engineering_hourly_cost)

    def test_sow_doc_setting_retrieve(self):
        setting = SOWDocumentSettingsFactory(provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_doc_setting_url,
            self.provider,
            self.user,
        )
        view = SOWSettingsRetrieveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(setting.id, response.data.get("id"))
