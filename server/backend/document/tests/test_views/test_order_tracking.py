from document.views.order_tracking import (
    AutomatedWeeklyRecapSettingCreateRetrieveUpdateView,
    WeeklyRecapEmailPreviewAPIView,
    TriggerWeeklyRecapEmailAPIView,
)
from document.models import AutomatedWeeklyRecapSetting
from accounts.models import Provider, User
from test_utils import TestClientMixin, BaseProviderTestCase
from django.urls import reverse
from typing import List, Dict
from document.tests.factories import AutomatedWeeklyRecapSettingFactory
from accounts.tests.factories import ProviderUserFactory
from unittest.mock import patch


class TestAutomatedWeeklyRecapSettingCreateRetrieveUpdateView(
    BaseProviderTestCase
):
    """
    Test view: AutomatedWeeklyRecapSettingCreateRetrieveUpdateView
    """

    setting_url: str = reverse(
        "api_v1_endpoints:providers:weekly-recap-setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_creation_of_setting(self):
        request_body: Dict = dict(
            receiver_email_ids=["abc@yopmail.com", "def@yopmail.com"],
            sender=self.user.id,
            weekly_schedule=["Monday", "Sunday"],
            contact_type_crm_id=1,
            email_subject="Email subject",
            email_body_text="Email body text",
            is_enabled=True,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual(
            True,
            AutomatedWeeklyRecapSetting.objects.filter(
                provider=self.provider
            ).exists(),
        )

    def test_setting_creation_error_if_already_exists(self):
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, sender=self.user
        )
        request_body: Dict = dict(
            receiver_email_ids=["abc@yopmail.com", "def@yopmail.com"],
            sender=self.user.id,
            weekly_schedule=["Monday", "Sunday"],
            contact_type_crm_id=1,
            email_subject="Email subject",
            email_body_text="Email body text",
            is_enabled=True,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_creation_of_setting_when_invalid_request_body(self):
        request_body: Dict = dict(
            sender=self.user.id,
            weekly_schedule=[],
            contact_type_crm_id=1,
            is_enabled=True,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_retrieval_of_weekly_recap_setting(self):
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider,
            sender=self.user,
            receiver_email_ids=["abc@yopmail.com"],
            weekly_schedule=["Monday"],
            contact_type_crm_id=1,
            email_subject="Email subject",
            email_body_text="Email body text",
            is_enabled=True,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.setting_url,
            self.provider,
            self.user,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        expected_response: Dict = dict(
            provider=self.provider.id,
            receiver_email_ids=["abc@yopmail.com"],
            sender=self.user.id,
            weekly_schedule=["Monday"],
            is_enabled=True,
            contact_type_crm_id=1,
            email_subject="Email subject",
            email_body_text="Email body text",
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_error_retrieving_setting_if_does_not_exist(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.setting_url,
            self.provider,
            self.user,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(404, response.status_code)

    def test_update_to_setting(self):
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, sender=self.user
        )
        user: User = ProviderUserFactory(provider=self.provider)
        request_body: Dict = dict(
            receiver_email_ids=["bruce@wayne.com"],
            weekly_schedule=["Sunday"],
            contact_type_crm_id=2,
            is_enabled=False,
            sender=user.id,
            email_subject="Updated email subject",
            email_body_text="Updated email body text",
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        expected_response: Dict = dict(
            sender=user.id,
            provider=self.provider.id,
            receiver_email_ids=["bruce@wayne.com"],
            weekly_schedule=["Sunday"],
            contact_type_crm_id=2,
            is_enabled=False,
            email_subject="Updated email subject",
            email_body_text="Updated email body text",
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_error_updating_setting_if_does_not_exist(self):
        request_body: Dict = dict(
            receiver_email_ids=["bruce@wayne.com"],
            weekly_schedule=["Sunday"],
            contact_type_crm_id=2,
            is_enabled=False,
            sender=self.user.id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = AutomatedWeeklyRecapSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(404, response.status_code)


class TestTriggerWeeklyRecapEmailAPIView(BaseProviderTestCase):
    """
    Test view: TriggerWeeklyRecapEmailAPIView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:trigger-weekly-recap-emails"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "document.views.order_tracking.send_order_tracking_weekly_recap_email.apply_async"
    )
    def test_weekly_recap_emails_trigger(
        self, send_order_tracking_weekly_recap_email
    ):
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, is_enabled=True
        )
        view = TriggerWeeklyRecapEmailAPIView().as_view()
        # Test for all customers
        request_body: Dict = dict(all_customers=True, customer_crm_ids=[])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=request_body,
        )
        response: Response = view(request)
        self.assertEqual(202, response.status_code)
        send_order_tracking_weekly_recap_email.assert_called_with(
            (self.provider.id, True, []), queue="high"
        )
        # Test for selected customers
        request_body: Dict = dict(
            all_customers=False, customer_crm_ids=[1, 2, 3]
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=request_body,
        )
        response: Response = view(request)
        self.assertEqual(202, response.status_code)
        send_order_tracking_weekly_recap_email.assert_called_with(
            (self.provider.id, False, [1, 2, 3]), queue="high"
        )

    def test_error_triggering_emails_if_setting_not_configured(self):
        request_body: Dict = dict(all_customers=True, customer_crm_ids=[])
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = TriggerWeeklyRecapEmailAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)


class TestWeeklyRecapEmailPreviewAPIView(BaseProviderTestCase):
    """
    Test view: WeeklyRecapEmailPreviewAPIView
    """

    url: str = reverse("api_v1_endpoints:providers:preview-weekly-recap-email")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "document.views.order_tracking.WeeklyRecapService.generate_email_preview",
        return_value="<p>Hello, World!</p>",
    )
    def test_weekly_recap_email_preview(self, generate_email_preview):
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, is_enabled=True
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = WeeklyRecapEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual("<p>Hello, World!</p>", response.data.get("html"))
        generate_email_preview.assert_called()

    def test_error_previewing_recap_email_if_setting_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = WeeklyRecapEmailPreviewAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)
