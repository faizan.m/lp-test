from faker import Faker
from typing import List, Dict
from document.services.email_generation import EmailTemplateGenerationService
from unittest.mock import patch
from unittest import mock
from test_utils import BaseProviderTestCase
from accounts.tests.factories import CustomerFactory, UserFactory
from document.models import (
    SalesPurchaseOrderData,
    OperationEmailTemplateSettings,
    ConnectwisePurchaseOrderData,
    ConnectwiseServiceTicket,
)
from core.services import ERPClientIntegrationService
from ..factories import (
    SalesPurchaseOrderDataFactory,
    OperationEmailTemplateSettingsFactory,
    ConnectwiseServiceTicketFactory,
)
from ..utils import (
    get_sample_hardware_products_data,
    get_sample_table,
    create_email_template_generation_scenario,
)

# Be careful while modifying certain imports as it may cause namespace issues
# during mocking and cause tests to fail

fake = Faker()


class TestEmailTemplateGenerationServiceClass(BaseProviderTestCase):
    """
    Test methods of EmailTemplateGenerationService class
    """

    service_class = EmailTemplateGenerationService()

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_get_contact_emails(self):
        user = self.user
        mock_provider = mock.Mock()
        customer_id: int = fake.pyint()
        contact_type_connectwise_id = fake.pyint()
        mock_provider.erp_client.get_customer_users.return_value = (
            [user.email],
            [fake.email()],
        )
        to_contacts, cc_contacts = self.service_class.get_contact_emails(
            mock_provider,
            customer_id,
            user.crm_id,
            contact_type_connectwise_id,
        )
        self.assertIsNotNone(to_contacts)
        self.assertIsNotNone(cc_contacts)

    def test_extract_subject_info_from_opportunity_name(self):
        """
        Test method extract_subject_info_from_opportunity_name()
        """
        opportunity_name: str = "WR - Tegile Support Renewal 1 Year"
        exptected_subject: str = "Tegile Support Renewal 1 Year"
        result_subject: str = (
            self.service_class.extract_subject_info_from_opportunity_name(
                opportunity_name
            )
        )
        self.assertEqual(exptected_subject, result_subject)

    def test_get_customer_po_from_opportunity(self):
        """
        Test method get_customer_po_from_opportunity()
        """
        expected_customer_po: str = "123456789"
        mock_provider = mock.Mock()
        mock_provider.erp_client.client.get_company_opportunity.return_value = dict(
            customer_po="123456789"
        )
        opportunity_id: int = fake.pyint()
        customer_id: int = fake.pyint()
        actual_customer_po: str = (
            self.service_class.get_customer_po_from_opportunity(
                mock_provider, opportunity_id, customer_id
            )
        )
        self.assertEqual(expected_customer_po, actual_customer_po)

    @patch(
        "document.tests.test_views.test_email_template_generation_service."
        "ERPClientIntegrationService.get_customer_complete_site_addresses",
        return_value={
            345: "Address Line 1, Address Line 2, City, State, Postalcode, Country"
        },
    )
    def test_get_shipping_site_information(self, mock_service_method_call):
        """
        Test method get_shipping_site_information()
        """
        provider = mock.Mock()
        provider.id = fake.pyint()
        provider.erp_client = mock.Mock()
        customer_id: int = fake.pyint()
        shipping_site_id: int = 345
        exptected_addres_info: Dict = dict(
            addressline1="Address Line 1",
            addressline2="Address Line 2",
            city="City",
            state="State",
            postalcode="Postalcode",
            country="Country",
        )
        actual_shipping_address = (
            self.service_class.get_shipping_site_information(
                provider, customer_id, shipping_site_id
            )
        )
        self.assertDictEqual(exptected_addres_info, actual_shipping_address)

    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "get_shipping_site_information",
        return_value=dict(
            addressline1="Address Line 1",
            addressline2="Address Line 2",
            city="City",
            state="State",
            postalcode="Postalcode",
            country="Country",
        ),
    )
    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "get_customer_po_from_opportunity",
        return_value="1234567890",
    )
    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "extract_subject_info_from_opportunity_name",
        return_value="Tegile Support Renewal 1 Year",
    )
    def test_get_sales_order_details_payload(
        self, mock_call_1, mock_call_2, mock_call_3
    ):
        provider = mock.Mock()
        customer = CustomerFactory(crm_id=100)
        customer_id: int = customer.id
        sales_order = SalesPurchaseOrderDataFactory(
            opportunity_name="WR - Tegile Support Renewal 1 Year",
            shipping_site_crm_id=345,
            customer=customer,
        )
        expected_payload: Dict = dict(
            subject_opportunity_name="Tegile Support Renewal 1 Year",
            customer_po="1234567890",
            shipping_details=dict(
                addressline1="Address Line 1",
                addressline2="Address Line 2",
                city="City",
                state="State",
                postalcode="Postalcode",
                country="Country",
            ),
            customer_crm_id=customer.crm_id,
        )
        actual_payload: Dict = (
            self.service_class.get_sales_order_details_payload(
                provider,
                sales_order,
            )
        )
        self.assertDictEqual(expected_payload, actual_payload)

    def test_extract_table_from_product_data(self):
        """
        Test method extract_table_from_product_data()
        """
        product_data: List[Dict] = get_sample_hardware_products_data()
        expected_result: List[List] = get_sample_table()
        actual_result = self.service_class.extract_table_from_product_data(
            product_data
        )
        self.assertListEqual(expected_result, actual_result)

    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "extract_table_from_product_data",
        return_value=get_sample_table(),
    )
    def test_get_product_data_tables(self, mock_service_class_call):
        """
        Test method test_get_product_data_tables()
        """
        hardware_products = get_sample_hardware_products_data()
        template_data: Dict = dict(hardware_products=hardware_products)
        actual_result = self.service_class.get_product_data_tables(
            template_data
        )
        # hardware_products has 2 dicts, so table should contain 3 rows (1 header row + 2 data rows)
        self.assertEqual(len(actual_result[0]), 3)

    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "get_contact_emails",
        return_value=(
            [fake.email()],
            [fake.email()],
        ),
    )
    @patch(
        "document.services.email_generation.EmailTemplateGenerationService."
        "get_sales_order_details_payload",
        return_value=dict(
            subject_opportunity_name="Tegile Support Renewal 1 Year",
            customer_po="1234567890",
            shipping_details=dict(
                addressline1="Address Line 1",
                addressline2="Address Line 2",
                city="City",
                state="State",
                postalcode="Postalcode",
                country="Country",
            ),
            customer_crm_id=100,
        ),
    )
    def test_get_template_context(
        self, get_sales_order_details_payload, get_contact_emails
    ):
        """
        Test method get_template_context()
        """
        provider: Provider = self.provider
        operation_email_template_settings: OperationEmailTemplateSettings = (
            OperationEmailTemplateSettingsFactory(provider=provider)
        )
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory.create(provider=self.provider, opportunity_crm_id=100)
        )
        create_email_template_generation_scenario(provider, 100)
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderData.objects.get(
                provider=provider, opportunity_crm_id=service_ticket.opportunity_crm_id
            )
        )
        purchase_orders: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderData.objects.filter(
                provider=provider, sales_order_id=sales_order.id
            )
        )
        actual_context: Dict = self.service_class.get_template_context(
            sales_order, purchase_orders, operation_email_template_settings
        )
        expected_keys: List[str] = [
            "contracts",
            "hardware_products",
            "licences",
            "name",
            "cc_contacts",
            "to_contacts_emails",
            "settings",
            "subject_opportunity_name",
            "customer_po",
            "shipping_details",
        ]
        # Check if context contains all keys
        all_keys_present: bool = all(
            [
                True if key in expected_keys else False
                for key in actual_context.keys()
            ]
        )
        self.assertEqual(all_keys_present, True)
