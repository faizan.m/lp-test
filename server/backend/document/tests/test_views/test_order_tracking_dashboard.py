from datetime import datetime, timedelta

from django.urls import reverse
from typing import List, Dict
from rest_framework.request import Request
from rest_framework.response import Response
from factory.fuzzy import FuzzyChoice
from accounts.tests.factories import CustomerFactory
from document.models import (
    ConnectwisePurchaseOrderData,
    OperationDashboardSettings,
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrder,
    ConnectwiseServiceTicket,
)
from document.tests.factories import (
    ConnectwisePurchaseOrderDataFactory,
    OperationDashboardSettingsFactory,
    ConnectwisePurchaseOrderFactory,
    ConnectwiseServiceTicketFactory,
    SalesPurchaseOrderDataFactory,
    ConnectwisePurchaseOrder,
    ConnectwiseServiceTicketParseFailedFactory,
)
from document.views.order_tracking_dashboard import (
    UnlinkedPurchaseOrdersListAPIView,
    SalesOrderToPurchaseOrderLinkingListCreateAPIView,
    SalesOrderToPurchaseOrderLinkDeleteAPIView,
    SyncFailedTicketsListAPIView,
)
from test_utils import BaseProviderTestCase, TestClientMixin
from unittest.mock import patch


def add_query_param_to_url(url: str, query_params: Dict) -> str:
    url += "?"
    for param, value in query_params.items():
        value = str(value) if isinstance(value, int) else value
        url = url + param + "=" + value + "&"
    return url


class TestUnlinkedPurchaseOrdersListAPIView(BaseProviderTestCase):
    """
    Test view: UnlinkedPurchaseOrdersListAPIView
    """

    url = reverse("api_v1_endpoints:providers:unlinked-purchase-orders")

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    def test_unlinked_purchase_orders_listing(self):
        """
        Test listing of unlinked purchase orders.
        """
        ConnectwisePurchaseOrderDataFactory.create_batch(
            2,
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        expected_purchase_orders_count: int = 2
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(
            response.data.get("count"), expected_purchase_orders_count
        )

    def test_ordering_by_po_number_in_alphabetical_order(self):
        """
        Test listing of unlinked purchase orders in alphabetical order
        """
        purchase_order_A: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                po_number=ConnectwisePurchaseOrderFactory(
                    po_number="Purchase Order AA"
                ),
                provider=self.provider,
                sales_order=None,
            )
        )
        purchase_order_B: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                po_number=ConnectwisePurchaseOrderFactory(
                    po_number="Purchase Order AB"
                ),
                provider=self.provider,
                sales_order=None,
            )
        )
        expected_ordered_ids: List[int] = [
            purchase_order_A.id,
            purchase_order_B.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="po_number")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_ordering_by_po_number_in_reverse_alphabetical_order(self):
        """
        Test listing of unlinked purchase orders in reverse alphabetical order
        """
        purchase_order_A: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                po_number=ConnectwisePurchaseOrderFactory(
                    po_number="Purchase Order AA"
                ),
                provider=self.provider,
                sales_order=None,
            )
        )
        purchase_order_B: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                po_number=ConnectwisePurchaseOrderFactory(
                    po_number="Purchase Order AB"
                ),
                provider=self.provider,
                sales_order=None,
            )
        )
        expected_ordered_ids: List[int] = [
            purchase_order_B.id,
            purchase_order_A.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-po_number")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_ordering_by_latest_updated_purchase_order(self):
        """
        Test listing of unlinked purchase orders by latest update
        """
        old_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                updated_on=datetime.now() - timedelta(days=1),
            )
        )
        new_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                updated_on=datetime.now(),
            )
        )
        expected_ordered_ids: List[int] = [
            new_purchase_order.id,
            old_purchase_order.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-updated_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_ordering_by_oldest_updated_purchase_order(self):
        """
        Test listing of unlinked purchase orders by oldest update
        """
        old_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                updated_on=datetime.now() - timedelta(days=1),
            )
        )
        new_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                updated_on=datetime.now(),
            )
        )
        expected_ordered_ids: List[int] = [
            old_purchase_order.id,
            new_purchase_order.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="updated_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_ordering_by_oldest_created_purchase_order(self):
        """
        Test listing of unlinked purchase orders by oldest created
        """
        old_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                created_on=datetime.now() - timedelta(days=1),
            )
        )
        new_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                created_on=datetime.now(),
            )
        )
        expected_ordered_ids: List[int] = [
            old_purchase_order.id,
            new_purchase_order.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="created_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_ordering_by_latest_created_purchase_order(self):
        """
        Test listing of unlinked purchase orders by latest created
        """
        old_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                created_on=datetime.now() - timedelta(days=1),
            )
        )
        new_purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider,
                sales_order=None,
                created_on=datetime.now(),
            )
        )
        expected_ordered_ids: List[int] = [
            new_purchase_order.id,
            old_purchase_order.id,
        ]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-created_on")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_ordered_ids: List[int] = [
            order.get("id") for order in response.data.get("results")
        ]
        self.assertEqual(expected_ordered_ids, actual_ordered_ids)

    def test_filtering_by_po_number_on_unlinked_purchase_orders_listing(self):
        """
        Test filtering by po number on unlinked purchase orders listing
        """
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                po_number="Purchase Order AA"
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                po_number="Purchase Order AB"
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        expected_purchase_orders_count: int = 1
        url: str = add_query_param_to_url(
            self.url, query_params=dict(po_number="Purchase Order AA")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_purchase_orders_count: int = response.data.get("count")
        self.assertEqual(
            expected_purchase_orders_count, actual_purchase_orders_count
        )

    def test_filtering_by_customer_name_on_unlinked_purchase_orders_listing(
        self,
    ):
        """
        Test filtering by customer name on unlinked purchase orders listing
        """
        ticket_customer_A = ConnectwiseServiceTicketFactory(
            company_name="Company AA"
        )
        ticket_customer_B = ConnectwiseServiceTicketFactory(
            company_name="Company AB"
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                po_number="PO1", ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                po_number="PO2", ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_B
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        expected_purchase_orders_count: int = 2
        url: str = add_query_param_to_url(
            self.url, query_params=dict(customer="Company AA")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_purchase_orders_count: int = response.data.get("count")
        self.assertEqual(
            expected_purchase_orders_count, actual_purchase_orders_count
        )

    def test_search_by_purchase_order_number_on_unlinked_purchase_orders_listing(
        self,
    ):
        """
        Test search by purchase order number on unlinked purchase orders listing
        """
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(po_number="PO1"),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(po_number="PO2"),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        expected_purchase_orders_count: int = 1
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search="PO1")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_purchase_orders_count: int = response.data.get("count")
        self.assertEqual(
            expected_purchase_orders_count, actual_purchase_orders_count
        )

    def test_search_by_customer_name_on_unlinked_purchase_orders_listing(self):
        """
        Test search by customer name on unlinked purchase orders listing
        """
        # DRF search is case insensitive partial match
        # E.g. If searched for "company A" then both "Company A" and "Company B" will be
        # included in search results. Hence include vastly different names in test cases.
        mock_customer_name: str = "mock Company Pvt. Ltd."
        ticket_customer_A = ConnectwiseServiceTicketFactory(
            company_name=mock_customer_name
        )
        ticket_customer_B = ConnectwiseServiceTicketFactory(
            company_name="mock Corporation Inc."
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_B
            ),
            provider=self.provider,
            sales_order=None,
        )
        expected_purchase_orders_count: int = 2
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search=mock_customer_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_purchase_orders_count: int = response.data.get("count")
        self.assertEqual(
            expected_purchase_orders_count, actual_purchase_orders_count
        )

    def test_search_by_ticket_summary_on_unlinked_purchase_orders_listing(
        self,
    ):
        # DRF search works by case insensitive partial matching.
        # E.g. If searched for "company A", then both "Company A" and "Company B" will be
        # included in search results. Hence include vastly different names in test cases.
        ticket_customer_A = ConnectwiseServiceTicketFactory(
            summary="LP purchase order"
        )
        ticket_customer_B = ConnectwiseServiceTicketFactory(
            summary="Vel order status"
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_B
            ),
            provider=self.provider,
            sales_order=None,
        )
        ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        expected_purchase_orders_count: int = 2
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search="LP purchase order")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        actual_purchase_orders_count: int = response.data.get("count")
        self.assertEqual(
            expected_purchase_orders_count, actual_purchase_orders_count
        )

    def test_ordering_by_customer_name_on_unlinked_purchase_orders_listing(
        self,
    ):
        ticket_customer_A = ConnectwiseServiceTicketFactory(company_name="A")
        ticket_customer_B = ConnectwiseServiceTicketFactory(company_name="B")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_B
            ),
            provider=self.provider,
            sales_order=None,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        # Test alphabetical ordering first
        expected_order: List[int] = [purchase_order_1.id, purchase_order_2.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="customer_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test reverse alphabetical order
        expected_order: List[int] = [purchase_order_2.id, purchase_order_1.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-customer_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_summary_on_unlinked_purchase_orders_listing(
        self,
    ):
        ticket_customer_A = ConnectwiseServiceTicketFactory(summary="A")
        ticket_customer_B = ConnectwiseServiceTicketFactory(summary="B")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_A
            ),
            provider=self.provider,
            sales_order=None,
        )
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory.create(
            po_number=ConnectwisePurchaseOrderFactory(
                ticket=ticket_customer_B
            ),
            provider=self.provider,
            sales_order=None,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        # Test ordering by alphabetical order first
        expected_order: List[int] = [purchase_order_1.id, purchase_order_2.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="service_ticket_summary")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test ordering by reverse alphabetical order first
        expected_order: List[int] = [purchase_order_2.id, purchase_order_1.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-service_ticket_summary")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_unauthorized_user_API_listing_failure(self):
        """
        API should raise error for unauthorized user
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
        )
        view = UnlinkedPurchaseOrdersListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestSalesOrderToPurchaseOrderLinkingListCreateAPIView(
    BaseProviderTestCase
):
    """
    Test view: SalesOrderToPurchaseOrderLinkingListCreateAPIView
    """

    url = reverse("api_v1_endpoints:providers:link-po-to-so")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.dashboard_settings: OperationDashboardSettings = (
            OperationDashboardSettingsFactory.create(provider=cls.provider)
        )

    def test_sales_order_to_purchase_orders_linking_listing(self):
        """
        Test listing of sales order to purchase order link listing.
        """
        sales_order: SalesPurchaseOrderData = SalesPurchaseOrderDataFactory(
            provider=self.provider
        )
        purchase_order_1: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory(
                provider=self.provider, sales_order=sales_order
            )
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        expected_result: Dict = dict(
            id=sales_order.id,
            sales_order_crm_id=sales_order.crm_id,
            customer_name=sales_order.company_name,
            opportunity_crm_id=sales_order.opportunity_crm_id_id,
            opportunity_name=sales_order.opportunity_name,
            purchase_order_numbers=[
                purchase_order_1.po_number_id,
            ],
            purchase_order_crm_ids=[
                purchase_order_1.crm_id,
            ],
        )
        actual_result: Dict = response.data.get("results")[0]
        self.assertDictEqual(expected_result, actual_result)

    @patch(
        "document.views.order_tracking_dashboard.OperationsDashboardService."
        "link_purchase_orders_to_sales_order",
        return_value={
            "sales_order_crm_id": 5028,
            "linked_purchase_orders": [
                {"purchase_order_crm_id": 5500, "po_number": "LP10711-CAD"}
            ],
            "linking_errors": [],
        },
    )
    @patch(
        "document.views.order_tracking_dashboard.SalesOrderToPurchaseOrderLinkingMixin."
        "get_purchase_order_custom_field_id",
        return_value=10,
    )
    def test_creation_of_sales_order_to_purchase_order_link(
        self,
        mock_get_purchase_order_custom_field_id,
        mock_link_purchase_orders_to_sales_order,
    ):
        """
        Test the case when link creation occurs without any errors.
        """
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            opportunity_crm_id=sales_order.opportunity_crm_id_id,
            purchase_order_crm_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        # As long as the output of method link_purchase_orders_to_sales_order does not
        # contain any linking errors, it means link creation is successful.
        response: Response = view(request)
        self.assertEqual(201, response.status_code)

    @patch(
        "document.views.order_tracking_dashboard.OperationsDashboardService."
        "link_purchase_orders_to_sales_order",
        return_value={
            "sales_order_crm_id": 5028,
            "linked_purchase_orders": [],
            "linking_errors": [
                {
                    "purchase_order_crm_id": 5500,
                    "po_number": "LP10711-CAD",
                    "error": "CW error",
                }
            ],
        },
    )
    @patch(
        "document.views.order_tracking_dashboard.SalesOrderToPurchaseOrderLinkingMixin."
        "get_purchase_order_custom_field_id",
        return_value=10,
    )
    def test_creation_of_sales_order_to_purchase_order_link_in_case_of_any_errors(
        self,
        mock_get_purchase_order_custom_field_id,
        mock_link_purchase_orders_to_sales_order,
    ):
        """
        Test the case when any errors occur during link creation.
        """
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(provider=self.provider)
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            opportunity_crm_id=sales_order.opportunity_crm_id_id,
            purchase_order_crm_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        # Case when there are any possible errors during linking
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_api_error_when_operation_dashboard_settings_not_configured(self):
        """
        API should raise validation error when Operation Dashboard Setting not'
        configured for the provider.
        """
        OperationDashboardSettings.objects.get(
            provider_id=self.provider.id
        ).delete()
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider, sales_order=sales_order
            )
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            opportunity_crm_id=sales_order.opportunity_crm_id_id,
            purchase_order_crm_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_api_error_when_po_custom_field_not_configured(self):
        """
        API should raise validation error when purchase order custom field not
        configured in Operation Dashboard Setting for the provider.
        """
        OperationDashboardSettings.objects.filter(
            provider_id=self.provider.id
        ).update(po_custom_field_id=None)
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider, sales_order=sales_order
            )
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            opportunity_crm_id=sales_order.opportunity_crm_id_id,
            purchase_order_crm_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_api_error_when_invalid_request_body_payload(self):
        """
        API should raise error for invalid request body payload
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data={}
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_search_on_sales_order_CRM_ID(self):
        """
        Test search on sales order CRM ID
        """
        CRM_ID_1: int = 100
        CRM_ID_2: int = 200
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=CRM_ID_1
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=CRM_ID_2
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search=CRM_ID_1)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(
            CRM_ID_1,
            response.data.get("results")[0].get("sales_order_crm_id"),
        )

    def test_search_on_linked_purchase_order_number(self):
        """
        Test search on linked purchase order number
        """
        po_number_1: str = "PO-123"
        po_number_2: str = "PO-345"
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order_1: ConnectwisePurchaseOrder = (
            ConnectwisePurchaseOrderFactory.create(po_number=po_number_1)
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider,
            sales_order=sales_order_1,
            po_number=purchase_order_1,
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(provider=self.provider)
        )
        purchase_order_2: ConnectwisePurchaseOrder = (
            ConnectwisePurchaseOrderFactory.create(po_number=po_number_2)
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider,
            sales_order=sales_order_2,
            po_number=purchase_order_2,
        )
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search=po_number_1)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(
            po_number_1,
            response.data.get("results")[0].get("purchase_order_numbers")[0],
        )

    def test_search_on_sales_order_customer_name(self):
        """
        Test search on sales order customer name
        """
        company_name_1: str = "Company Corporation Inc."
        company_name_2: str = "Random Company Inc."
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name=company_name_1
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name=company_name_2
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        url: str = add_query_param_to_url(
            self.url, query_params=dict(search=company_name_1)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(
            company_name_1,
            response.data.get("results")[0].get("customer_name"),
        )

    def test_ordering_by_sales_order_crm_id_in_ascending_order(self):
        """
        Test ascending ordering by sales order CRM ID.
        """
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=100
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=200
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        expected_order: List[int] = [sales_order_1.id, sales_order_2.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="crm_id")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            sales_order.get("id")
            for sales_order in response.data.get("results")
        ]
        self.assertEqual(
            expected_order,
            actual_order,
        )

    def test_ordering_by_sales_order_crm_id_in_descending_order(self):
        """
        Test descending ordering by sales order CRM ID.
        """
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=100
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=200
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        expected_order: List[int] = [sales_order_2.id, sales_order_1.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-crm_id")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            sales_order.get("id")
            for sales_order in response.data.get("results")
        ]
        self.assertEqual(
            expected_order,
            actual_order,
        )

    def test_ordering_by_sales_order_customer_name_in_alphabetical_order(self):
        """
        Test alphabetical ordering by sales order customer name.
        """
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name="Company A"
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name="Company B"
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        expected_order: List[int] = [sales_order_1.id, sales_order_2.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="company_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            sales_order.get("id")
            for sales_order in response.data.get("results")
        ]
        self.assertEqual(
            expected_order,
            actual_order,
        )

    def test_ordering_by_sales_order_customer_name_in_reverse_alphabetical_order(
        self,
    ):
        """
        Test reverse alphabetical ordering by sales order customer name.
        """
        sales_order_1: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name="Company A"
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_1
        )
        sales_order_2: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, company_name="Company B"
            )
        )
        ConnectwisePurchaseOrderDataFactory.create(
            provider=self.provider, sales_order=sales_order_2
        )
        expected_order: List[int] = [sales_order_2.id, sales_order_1.id]
        url: str = add_query_param_to_url(
            self.url, query_params=dict(ordering="-company_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SalesOrderToPurchaseOrderLinkingListCreateAPIView.as_view()
        response: Response = view(request)
        actual_order: List[int] = [
            sales_order.get("id")
            for sales_order in response.data.get("results")
        ]
        self.assertEqual(
            expected_order,
            actual_order,
        )


class TestSalesOrderToPurchaseOrderLinkDeleteAPIView(BaseProviderTestCase):
    """
    Test view: SalesOrderToPurchaseOrderLinkDeleteAPIView
    """

    url = reverse("api_v1_endpoints:providers:remove-po-to-so-link")

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.dashboard_settings: OperationDashboardSettings = (
            OperationDashboardSettingsFactory.create(provider=cls.provider)
        )

    @patch(
        "document.views.order_tracking_dashboard.OperationsDashboardService."
        "remove_purchase_order_link_from_sales_order",
        return_value=dict(
            sales_order_crm_id=100,
            removed_purchase_order_link=[
                dict(purchase_order_crm_id=123, po_number="PO-123")
            ],
            purchase_order_link_remove_errors=[],
        ),
    )
    def test_sales_order_purchase_order_link_deletion(
        self, mock_remove_purchase_order_link_from_sales_order
    ):
        """
        Test deletion of sales order to purchase orders link
        """
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=100
            )
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider, sales_order=sales_order
            )
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            remove_link_for_po_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkDeleteAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)

    @patch(
        "document.views.order_tracking_dashboard.OperationsDashboardService."
        "remove_purchase_order_link_from_sales_order",
        return_value=dict(
            sales_order_crm_id=100,
            removed_purchase_order_link=[],
            purchase_order_link_remove_errors=[
                dict(
                    purchase_order_crm_id=123,
                    po_number="PO-123",
                    error="Error from CW",
                )
            ],
        ),
    )
    def test_sales_order_purchase_order_link_deletion_in_case_of_errors(
        self, mock_remove_purchase_order_link_from_sales_order
    ):
        """
        Test deletion of sales order to purchase orders link when there are errors
        """
        sales_order: SalesPurchaseOrderData = (
            SalesPurchaseOrderDataFactory.create(
                provider=self.provider, crm_id=100
            )
        )
        purchase_order: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory.create(
                provider=self.provider, sales_order=sales_order
            )
        )
        request_payload: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            remove_link_for_po_ids=[purchase_order.crm_id],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data=request_payload
        )
        view = SalesOrderToPurchaseOrderLinkDeleteAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_api_error_for_invalid_request_body(self):
        """
        API should validation raise error for invalid request body
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", self.url, self.provider, self.user, data={}
        )
        view = SalesOrderToPurchaseOrderLinkDeleteAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)


class TestSyncFailedTicketsListAPIView(BaseProviderTestCase):
    """
    Test view: SyncFailedTicketsListAPIView
    """

    sync_failed_tickets_list_url: str = reverse(
        "api_v1_endpoints:providers:sync-failed-service-tickets"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_list_sync_failed_service_tickets(self):
        ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, is_ignored=False
        )
        ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, is_ignored=True
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.sync_failed_tickets_list_url, self.provider, self.user
        )
        view = SyncFailedTicketsListAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data.get("results")), 1)

    def test_ordering_by_ticket_crm_id_on_sync_failed_ticket_listing(
        self,
    ):
        ticket_1 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, ticket_crm_id=1, is_ignored=False
        )
        ticket_2 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, ticket_crm_id=2, is_ignored=False
        )
        view = SyncFailedTicketsListAPIView.as_view()
        # Test ascending ordering
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(ordering="ticket_crm_id"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        expected_order: List[int] = [ticket_1.id, ticket_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(ordering="-ticket_crm_id"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        expected_order: List[int] = [ticket_2.id, ticket_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_summary_on_sync_failed_ticket_listing(
        self,
    ):
        ticket_1 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, summary="A", is_ignored=False
        )
        ticket_2 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, summary="B", is_ignored=False
        )
        view = SyncFailedTicketsListAPIView.as_view()
        # Test alphabetical ordering
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(ordering="summary"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        expected_order: List[int] = [ticket_1.id, ticket_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test reverse alphabetical ordering
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(ordering="-summary"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        expected_order: List[int] = [ticket_2.id, ticket_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_search_by_summary_on_sync_failed_ticket_listing(
        self,
    ):
        summary_to_search: str = "mock Summary"
        ticket_1 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, summary=summary_to_search, is_ignored=False
        )
        ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, is_ignored=False
        )
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(search=summary_to_search),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SyncFailedTicketsListAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data.get("results")[0].get("id"), ticket_1.id
        )

    def test_search_by_ticket_crm_id_on_sync_failed_ticket_listing(
        self,
    ):
        ticket_1 = ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, ticket_crm_id=10, is_ignored=False
        )
        ConnectwiseServiceTicketParseFailedFactory.create(
            provider=self.provider, ticket_crm_id=20, is_ignored=False
        )
        url: str = add_query_param_to_url(
            self.sync_failed_tickets_list_url,
            query_params=dict(search=str(10)),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SyncFailedTicketsListAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data.get("results")[0].get("id"), ticket_1.id
        )
