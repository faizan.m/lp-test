from test_utils import BaseProviderTestCase, TestClientMixin

from document.views.client360 import (
    Client360DashboardSettingCreateRetrieveUpdateView,
    OpenOpportunitiesChartAPIView,
    WonOpportuntiesChartAPIView,
    OpenPurchaseOrdersChartAPIView,
    OpenServiceTicketsChartAPIView,
)
from document.tests.factories import (
    Client360DashboardSettingFactory,
    ConnectwiseOpportunityDataFactory,
    ConnectwisePurchaseOrderDataFactory,
    ConnectwiseServiceTicketFactory,
    ConnectwisePurchaseOrderFactory,
    SalesPurchaseOrderDataFactory,
)
from django.urls import reverse
from typing import List, Dict, Union, Set
from document.tests.utils import add_query_params_to_url
from datetime import datetime, timedelta
from rest_framework.response import Response
from rest_framework.request import Request
from mock import Mock, MagicMock, PropertyMock
from unittest.mock import patch


class TestClient360DashboardSettingCreateRetrieveUpdateView(
    BaseProviderTestCase
):
    """
    Test view: Client360DashboardSettingCreateRetrieveUpdateView
    """

    setting_url: str = reverse(
        "api_v1_endpoints:providers:client360-setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_create_client360_setting(self):
        request_body: Dict = dict(
            won_opp_status_crm_ids=[1, 2], open_po_status_crm_ids=[10, 20]
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = Client360DashboardSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        expected_payload: Dict = dict(
            provider=self.provider.id,
            won_opp_status_crm_ids=[1, 2],
            open_po_status_crm_ids=[10, 20],
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(expected_payload, response.data)

    def test_error_creating_client360_setting_if_already_exists(self):
        Client360DashboardSettingFactory(provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=dict(
                won_opp_status_crm_ids=[1, 2], open_po_status_crm_ids=[10, 20]
            ),
        )
        view = Client360DashboardSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_retrieve_client360_setting(self):
        Client360DashboardSettingFactory(
            provider=self.provider,
            won_opp_status_crm_ids=[1, 2],
            open_po_status_crm_ids=[10, 20],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.setting_url, self.provider, self.user
        )
        view = Client360DashboardSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        expected_payload: Dict = dict(
            provider=self.provider.id,
            won_opp_status_crm_ids=[1, 2],
            open_po_status_crm_ids=[10, 20],
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_payload, response.data)

    def test_error_retrieving_client360_setting_if_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.setting_url, self.provider, self.user
        )
        view = Client360DashboardSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        self.assertEqual(404, response.status_code)

    def test_update_client360_setting(self):
        Client360DashboardSettingFactory(
            provider=self.provider,
            won_opp_status_crm_ids=[1, 2],
            open_po_status_crm_ids=[10, 20],
        )
        request_body: Dict = dict(
            won_opp_status_crm_ids=[300],
            open_po_status_crm_ids=[10],
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = Client360DashboardSettingCreateRetrieveUpdateView().as_view()
        response: Response = view(request)
        expected_payload: Dict = dict(
            provider=self.provider.id,
            won_opp_status_crm_ids=[300],
            open_po_status_crm_ids=[10],
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_payload, response.data)


class TestOpenOpportunitiesChartAPIView(BaseProviderTestCase):
    """
    Test view: OpenOpportunitiesChartAPIView
    """

    chart_url: str = reverse(
        "api_v1_endpoints:providers:client360-open-opp-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_error_listing_opp_when_board_mapping_not_configured(self):
        setattr(
            self.provider,
            "integration_statuses",
            dict(crm_board_mapping_configured=False),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        view = OpenOpportunitiesChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_listing_of_open_opportunities(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Opportunities": {
                    "status": [1],
                    "stages": [3, 4],
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseOpportunityDataFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=3,
            stage_name="Contract",
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=4,
            stage_name="Quote",
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=5,
            stage_name="Done",
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        view = OpenOpportunitiesChartAPIView().as_view()
        response: Response = view(request)
        expected_response: List[Dict] = [
            dict(stage_name="Contract", count=2),
            dict(stage_name="Quote", count=1),
        ]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_customer_name_filter_on_open_opp_chart(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Opportunities": {
                    "status": [1],
                    "stages": [3, 4],
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseOpportunityDataFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=3,
            stage_name="Contract",
            customer_name="Tyrell Corporation",
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=4,
            stage_name="Quote",
            customer_name="Cyberdyne",
        )
        url: str = add_query_params_to_url(
            self.chart_url,
            query_params=dict(customer_name="tyrell corporation"),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = OpenOpportunitiesChartAPIView().as_view()
        response: Response = view(request)
        expected_response: List[Dict] = [dict(stage_name="Contract", count=2)]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_customer_crm_id_filter_on_open_opp_chart(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Opportunities": {
                    "status": [1],
                    "stages": [3, 4],
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseOpportunityDataFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=3,
            stage_name="Contract",
            customer_name="Tyrell Corporation",
            customer_crm_id=101,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=4,
            stage_name="Quote",
            customer_name="Tyrell Corporation",
            customer_crm_id=101,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=3,
            stage_name="Contract",
            customer_name="Cyberdyne",
            customer_crm_id=102,
        )
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_crm_id=101)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = OpenOpportunitiesChartAPIView().as_view()
        response: Response = view(request)
        expected_response: List[Dict] = [
            dict(stage_name="Contract", count=2),
            dict(stage_name="Quote", count=1),
        ]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)

    def test_stage_crm_id_filter_on_open_opp_chart(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Opportunities": {
                    "status": [1],
                    "stages": [3, 4],
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseOpportunityDataFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=3,
            stage_name="Contract",
            customer_name="Tyrell Corporation",
            customer_crm_id=101,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=1,
            stage_crm_id=5,
            stage_name="Listed",
            customer_name="Cyberdyne",
            customer_crm_id=102,
        )
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(stage_crm_id=3)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = OpenOpportunitiesChartAPIView().as_view()
        response: Response = view(request)
        expected_response: List[Dict] = [dict(stage_name="Contract", count=2)]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data)


class TestWonOpportuntiesChartAPIView(BaseProviderTestCase):
    """
    Test view: WonOpportuntiesChartAPIView
    """

    chart_url: str = reverse(
        "api_v1_endpoints:providers:client360-won-opp-chart"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_error_listing_won_opp_if_client360_setting_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        view = WonOpportuntiesChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_listing_of_won_opps_within_last_90_days(self):
        now = datetime.now()
        datetime_in_future = now + timedelta(days=10)
        datetime_within_last_90_days = now - timedelta(days=40)
        Client360DashboardSettingFactory(
            provider=self.provider, won_opp_status_crm_ids=[2]
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            name="Desktops 101",
            status_crm_id=2,
            closed_date=datetime_within_last_90_days,
            customer_name="Apple Comps",
            customer_crm_id=99,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            name="Tablets 101",
            status_crm_id=2,
            closed_date=datetime_in_future,
            customer_name="Levono Comps",
            customer_crm_id=101,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        view = WonOpportuntiesChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, response.data.get("count"))

    def test_customer_crm_id_filter_no_won_opps_within_last_90_days(self):
        now = datetime.now()
        datetime_within_last_90_days = now - timedelta(days=40)
        Client360DashboardSettingFactory(
            provider=self.provider, won_opp_status_crm_ids=[2]
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            name="Desktops 101",
            status_crm_id=2,
            closed_date=datetime_within_last_90_days,
            customer_name="Apple Comps",
            customer_crm_id=99,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            name="Desktops 101",
            status_crm_id=3,
            closed_date=datetime_within_last_90_days,
            customer_name="Atom Comps",
            customer_crm_id=100,
        )
        ConnectwiseOpportunityDataFactory.create_batch(
            2,
            provider=self.provider,
            name="Tablets 101",
            status_crm_id=2,
            closed_date=datetime_within_last_90_days,
            customer_name="Levono Comps",
            customer_crm_id=101,
        )
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_crm_id=101)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = WonOpportuntiesChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, response.data.get("count"))

    def test_customer_name_filter_on_won_opps_within_last_90_days(self):
        now = datetime.now()
        datetime_within_last_90_days = now - timedelta(days=20)
        Client360DashboardSettingFactory(
            provider=self.provider, won_opp_status_crm_ids=[2]
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            customer_crm_id=1,
            customer_name="Apple Computers",
            status_crm_id=2,
            closed_date=datetime_within_last_90_days,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            customer_crm_id=1,
            customer_name="Apple Computers",
            status_crm_id=5,
            closed_date=datetime_within_last_90_days,
        )
        ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            customer_crm_id=2,
            customer_name="Maple Computers",
            status_crm_id=2,
            closed_date=datetime_within_last_90_days,
        )
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_name="Apple computers")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = WonOpportuntiesChartAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, response.data.get("count"))

    def test_ordering_by_closed_date_on_won_opps_within_last_90_days(self):
        now = datetime.now()
        Client360DashboardSettingFactory(
            provider=self.provider, won_opp_status_crm_ids=[2]
        )
        opp1 = ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            customer_crm_id=1,
            customer_name="Apple Computers",
            status_crm_id=2,
            closed_date=now - timedelta(days=10),
        )
        opp2 = ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            customer_crm_id=1,
            customer_name="Apple Computers",
            status_crm_id=2,
            closed_date=now - timedelta(days=20),
        )
        view = WonOpportuntiesChartAPIView().as_view()
        # Test for ascending ordering by date
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="closed_date")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [opp2.id, opp1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test for descending ordering by date
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-closed_date")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [opp1.id, opp2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_name_on_won_opps_within_last_90_days(self):
        now = datetime.now()
        Client360DashboardSettingFactory(
            provider=self.provider, won_opp_status_crm_ids=[2]
        )
        opp1 = ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=2,
            name="Apple",
            closed_date=now - timedelta(days=10),
        )
        opp2 = ConnectwiseOpportunityDataFactory(
            provider=self.provider,
            status_crm_id=2,
            name="Banana",
            closed_date=now - timedelta(days=20),
        )
        view = WonOpportuntiesChartAPIView().as_view()
        # Test for ascending ordering by name
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [opp2.id, opp1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test for descending ordering by name
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [opp1.id, opp2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)


class TestOpenPurchaseOrdersChartAPIView(BaseProviderTestCase):
    """
    Test view: OpenPurchaseOrdersChartAPIView
    """

    chart_url: str = reverse(
        "api_v1_endpoints:providers:client360-open-purchase-orders"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_error_listing_open_pos_if_client360_setting_not_configured(self):
        view = OpenPurchaseOrdersChartAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_listing_of_open_purchase_orders(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        sales_order = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO-1234",
            opportunity_name="Hardware buyback",
        )
        po = ConnectwisePurchaseOrderFactory(po_number="LP12345-ABC")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order,
            po_number=po,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            purchase_order_status_name="Closed",
            sales_order=None,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        expected_response: List[Dict] = [
            dict(
                id=purchase_order_1.id,
                po_number_id=purchase_order_1.po_number_id,
                vendor_company_name=purchase_order_1.vendor_company_name,
                purchase_order_status_name=purchase_order_1.purchase_order_status_name,
                customer_po=sales_order.customer_po_number,
                opportunity_name=sales_order.opportunity_name,
                customer_name=purchase_order_1.customer_company_name,
                customer_crm_id=purchase_order_1.customer_company_id,
            )
        ]
        self.assertEqual(expected_response, response.data.get("results"))

    def test_customer_crm_id_filter_on_open_purchase_orders_listing(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        sales_order_1 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO-1234",
            opportunity_name="Hardware buyback",
        )
        po_1 = ConnectwisePurchaseOrderFactory(po_number="LP12345-ABC")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_1,
            po_number=po_1,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        sales_order_2 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO-5678",
            opportunity_name="Hardware selling",
        )
        po_2 = ConnectwisePurchaseOrderFactory(po_number="LP678910-ABC")
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_2,
            po_number=po_2,
            vendor_company_name="MadFuture",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            purchase_order_status_name="Waiting",
            customer_company_name="Manhattan Project",
            customer_company_id=70,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_crm_id=69)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(response.data.get("results")))

    def test_customer_name_filter_on_open_purchase_orders_listing(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        sales_order_1 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO-1234",
            opportunity_name="Hardware buyback",
        )
        po_1 = ConnectwisePurchaseOrderFactory(po_number="LP12345-ABC")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_1,
            po_number=po_1,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        sales_order_2 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO-5678",
            opportunity_name="Hardware selling",
        )
        po_2 = ConnectwisePurchaseOrderFactory(po_number="LP678910-ABC")
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_2,
            po_number=po_2,
            vendor_company_name="MadFuture",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            purchase_order_status_name="Order Placed",
            customer_company_name="Manhattan Project",
            customer_company_id=70,
            purchase_order_status_id=1,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_name="ExSeparate")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(response.data.get("results")))

    def test_ordering_by_vendor_on_open_po_listing(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
        )
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            vendor_company_name="MadFuture",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        # Test ascending ordering by vendor name
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="vendor_company_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_1.id, purchase_order_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering by vendor name
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-vendor_company_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_2.id, purchase_order_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_customer_po_on_open_po_listing(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        sales_order_1 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO1234",
            opportunity_name="Hardware buyback",
        )
        po_1 = ConnectwisePurchaseOrderFactory(po_number="LP12345-ABC")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_1,
            po_number=po_1,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        sales_order_2 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO5678",
            opportunity_name="Hardware selling",
        )
        po_2 = ConnectwisePurchaseOrderFactory(po_number="LP678910-ABC")
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_2,
            po_number=po_2,
            vendor_company_name="MadFuture",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        # Test ascending ordering by customer PO
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="customer_po")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_1.id, purchase_order_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering by customer PO
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-customer_po")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_2.id, purchase_order_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_opp_name_on_open_po_listing(self):
        Client360DashboardSettingFactory(
            provider=self.provider, open_po_status_crm_ids=[1]
        )
        sales_order_1 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO1234",
            opportunity_name="Hardware buyback",
        )
        po_1 = ConnectwisePurchaseOrderFactory(po_number="LP12345-ABC")
        purchase_order_1 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_1,
            po_number=po_1,
            vendor_company_name="MadEx",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        sales_order_2 = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            customer_po_number="PO5678",
            opportunity_name="Hardware selling",
        )
        po_2 = ConnectwisePurchaseOrderFactory(po_number="LP678910-ABC")
        purchase_order_2 = ConnectwisePurchaseOrderDataFactory(
            provider=self.provider,
            sales_order=sales_order_2,
            po_number=po_2,
            vendor_company_name="MadFuture",
            purchase_order_status_name="Order Placed",
            purchase_order_status_id=1,
            customer_company_name="ExSeparate",
            customer_company_id=69,
        )
        view = OpenPurchaseOrdersChartAPIView().as_view()
        # Test ascending ordering by customer PO
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="opportunity_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_1.id, purchase_order_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering by customer PO
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-opportunity_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [purchase_order_2.id, purchase_order_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)


class TestOpenServiceTicketsChartAPIView(BaseProviderTestCase):
    """
    Test view: OpenServiceTicketsChartAPIView
    """

    chart_url: str = reverse(
        "api_v1_endpoints:providers:client360-open-service-tickets"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_error_listing_open_tickets_if_board_mapping_not_configured(self):
        setattr(
            self.provider,
            "integration_statuses",
            dict(crm_board_mapping_configured=False),
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_listing_of_open_service_tickets(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Service Board Mapping": {
                    "Managed Services": {"open_status": [1]},
                    "Professional Services": {"open_status": [2]},
                }
            }
        }
        # Monkey patch Provider's property & attributes.
        # Easy to understand, modify than mocking erp integration
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ticket_1 = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=1,
            status_name="Open",
            connectwise_last_data_update=datetime(day=5, month=7, year=2023),
        )
        ticket_2 = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=2,
            status_name="Order Placed",
            connectwise_last_data_update=datetime(day=2, month=7, year=2023),
        )
        ticket_3 = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=3,
            status_name="New",
            connectwise_last_data_update=datetime(day=10, month=7, year=2023),
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.chart_url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        expected_response: List[Dict] = [
            dict(
                id=ticket_1.id,
                ticket_crm_id=ticket_1.ticket_crm_id,
                summary=ticket_1.summary,
                customer_crm_id=ticket_1.company_crm_id,
                customer_name=ticket_1.company_name,
                status_name=ticket_1.status_name,
                status_crm_id=ticket_1.status_crm_id,
            ),
            dict(
                id=ticket_2.id,
                ticket_crm_id=ticket_2.ticket_crm_id,
                summary=ticket_2.summary,
                customer_crm_id=ticket_2.company_crm_id,
                customer_name=ticket_2.company_name,
                status_name=ticket_2.status_name,
                status_crm_id=ticket_2.status_crm_id,
            ),
        ]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.data.get("results"))

    def test_customer_crm_id_filter_on_open_service_tickets_listing(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Service Board Mapping": {
                    "Managed Services": {"open_status": [1]},
                    "Professional Services": {"open_status": [2]},
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseServiceTicketFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            status_name="Open",
            company_crm_id=20,
            company_name="Appleton",
        )
        ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=2,
            status_name="Order Placed",
            company_crm_id=21,
            company_name="Bananaland",
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_crm_id=20)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(2, len(response.data.get("results")))

    def test_customer_name_filter_on_open_service_tickets_listing(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Service Board Mapping": {
                    "Managed Services": {"open_status": [1]},
                    "Professional Services": {"open_status": [2]},
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ConnectwiseServiceTicketFactory.create_batch(
            2,
            provider=self.provider,
            status_crm_id=1,
            status_name="Open",
            company_crm_id=20,
            company_name="Appleton",
        )
        ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=2,
            status_name="Order Placed",
            company_crm_id=21,
            company_name="Bananaland",
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(customer_name="Bananaland")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        response: Response = view(request)
        self.assertEqual(1, len(response.data.get("results")))

    def test_ordering_by_ticket_crm_id_on_open_service_tickets_listing(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Service Board Mapping": {
                    "Managed Services": {"open_status": [1]},
                    "Professional Services": {"open_status": [2]},
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ticket_1 = ConnectwiseServiceTicketFactory(
            provider=self.provider, status_crm_id=1, ticket_crm_id=10
        )
        ticket_2 = ConnectwiseServiceTicketFactory(
            provider=self.provider, status_crm_id=1, ticket_crm_id=20
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        # Test ascending ordering by ticket CRM ID
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="ticket_crm_id")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [ticket_1.id, ticket_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering by ticket CRM ID
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-ticket_crm_id")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [ticket_2.id, ticket_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_status_on_open_service_tickets_listing(self):
        mock_other_config: Dict = {
            "board_mapping": {
                "Service Board Mapping": {
                    "Managed Services": {"open_status": [1]},
                    "Professional Services": {"open_status": [2]},
                }
            }
        }
        mock_erp_integration = PropertyMock(return_value=Mock())
        setattr(mock_erp_integration, "other_config", mock_other_config)
        setattr(self.provider, "erp_integration", mock_erp_integration)
        ticket_1 = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=1,
            status_name="Closed",
            ticket_crm_id=10,
        )
        ticket_2 = ConnectwiseServiceTicketFactory(
            provider=self.provider,
            status_crm_id=1,
            status_name="Open",
            ticket_crm_id=20,
        )
        view = OpenServiceTicketsChartAPIView().as_view()
        # Test ascending ordering by ticket CRM ID
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="status_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [ticket_1.id, ticket_2.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test descending ordering by ticket CRM ID
        url: str = add_query_params_to_url(
            self.chart_url, query_params=dict(ordering="-status_name")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        expected_order: List[int] = [ticket_2.id, ticket_1.id]
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
