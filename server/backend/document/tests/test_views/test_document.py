# Test views from document.py file here

import random
import shutil
from unittest import mock
from unittest.mock import patch
import os
from django.http import HttpResponse
from django.urls import reverse
from factory.fuzzy import FuzzyChoice
from faker import Faker
from rest_framework.request import Request

from accounts.models import Customer
from accounts.tests.factories import CustomerFactory
from accounts.tests.factories import ProviderUserFactory
from document.tests.factories import (
    SOWDocumentFactory,
    SOWCategoryFactory,
    SOWVendorOnboardingSettingFactory,
    SOWVendorOnboardingAttachmentsFactory,
    DefaultDocumentCreatorRoleSettingFactory,
    SoWHourlyResourcesDescriptionMappingFactory,
)
from document.tests.utils import (
    add_query_params_to_url,
    get_mock_company_quotes,
    mock_filter_documents_by_doc_status,
)
from document.views.document import *
from test_utils import (
    BaseProviderTestCase,
    TestClientMixin,
    get_a_random_value_from_given_choices,
)
from rest_framework.test import APIRequestFactory, force_authenticate
from django.core.files.uploadedfile import SimpleUploadedFile

SAMPLE_HTML_STRING: str = "<p>rendered html string</p>"
SAMPLE_FILE_PATH: str = "www.sample_path.com/file/path"
SAMPLE_FILE_NAME: str = "sample-file-name.format"


class TestSOWDocumentTypesListAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentTypesListAPIView
    """

    document_types_list_url: str = reverse(
        "api_v1_endpoints:providers:document-types"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    def test_document_types_listing(self):
        """
        Test SoW document types listing
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.document_types_list_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentTypesListAPIView().as_view()
        response: Response = view(request)
        expected_result: List[str] = [
            SOWDocument.GENERAL,
            SOWDocument.T_AND_M,
            SOWDocument.CHANGE_REQUEST,
        ]
        self.assertEqual(response.data, expected_result)


class TestSOWDocumentCreateListAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentCreateListAPIView
    """

    document_create_list_url: str = reverse(
        "api_v1_endpoints:providers:list-create-document"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        cls.sow_document = SOWDocumentFactory(
            provider=cls.provider,
            customer=cls.customer,
            author=cls.user,
            user=cls.user,
            updated_by=cls.user,
        )

    def test_sow_document_listing(self):
        """
        Test SoW document listing
        """
        with patch(
            "document.views.document.DocumentService.list_document"
        ) as mock_document_listing:
            fake = Faker()
            sow_listing_details: Dict = dict(
                **SOWDocumentSerializer(self.sow_document).data,
                quote_exist=True,
                quote=dict(
                    id=fake.pyint(),
                    name=fake.company(),
                    stage_id=fake.pyint(),
                    stage_name=fake.name(),
                ),
            )
            mock_listing_result: List[Dict] = [sow_listing_details]
            mock_document_listing.return_value = mock_listing_result
            request: Request = TestClientMixin.api_factory_client_request(
                "GET",
                self.document_create_list_url,
                self.provider,
                self.user,
            )
            view = SOWDocumentCreateListAPIView().as_view()
            response: Response = view(request)
            self.assertEqual(response.data, mock_listing_result)

    @patch("document.views.document.transaction.on_commit", return_value=None)
    @patch("document.views.document.DocumentService.create_document")
    def test_sow_document_creation(
        self,
        mock_create_document_call,
        mock_upload_sow_and_service_detail_in_cw_call_block,
    ):
        """
        Test SOW document creation for valid request payload
        """
        mock_create_document_call.return_value = self.sow_document
        fake = Faker()
        request_body: Dict = dict(
            name=fake.name(),
            json_config={},
            quote_id=self.sow_document.quote_id,
            forecast_id=self.sow_document.forecast_id,
            category=self.sow_document.category.id,
            doc_type=self.sow_document.doc_type,
            customer=self.sow_document.customer.id,
            user=self.sow_document.user.id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_create_list_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWDocumentCreateListAPIView().as_view()
        response: Response = view(request)
        mock_create_document_call.assert_called()
        mock_upload_sow_and_service_detail_in_cw_call_block.assert_called()
        self.assertEqual(response.status_code, 201)

    def test_api_error_creating_document_if_name_contains_forbidden_characters(
        self,
    ):
        """
        API should raise validation error when name contains invalid characters.
        """
        request_body: Dict = dict(
            name="Mock name <?*/",
            json_config={},
            quote_id=self.sow_document.quote_id,
            forecast_id=self.sow_document.forecast_id,
            category=self.sow_document.category.id,
            doc_type=self.sow_document.doc_type,
            customer=self.sow_document.customer.id,
            user=self.sow_document.user.id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_create_list_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWDocumentCreateListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_failure_of_sow_document_listing_for_unauthorized_user(self):
        """
        API should raise error if user is unauthorized
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.document_create_list_url,
            self.provider,
        )
        view = SOWDocumentCreateListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)

    def test_failure_of_sow_doc_creation_for_invalid_request_body(self):
        """
        API should raise validation errors if request body is invalid
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_create_list_url,
            self.provider,
            self.user,
            data={},
        )
        view = SOWDocumentCreateListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_failure_of_sow_doc_creation_for_unauthorized_user(self):
        """
        API should raise error if user is unauthorized
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_create_list_url,
            self.provider,
            data={},
        )
        view = SOWDocumentCreateListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestSOWDocumentRetrieveUpdateDestroyAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentRetrieveUpdateDestroyAPIView
    """

    document_retrieve_update_destroy_endpoint: str = (
        "api_v1_endpoints:providers:document"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        cls.sow_document = SOWDocumentFactory(
            provider=cls.provider,
            customer=cls.customer,
            author=cls.user,
            user=cls.user,
            updated_by=cls.user,
        )

    def test_sow_document_retrieval(self):
        """
        Method to test SOW document retrieval.
        """
        provider: Provider = self.provider
        provider.erp_client = mock.Mock()
        provider.erp_client.get_company_quote.return_value = {}
        document_pk: int = self.sow_document.id
        url: str = reverse(
            self.document_retrieve_update_destroy_endpoint,
            kwargs={"pk": document_pk},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            provider,
            self.user,
        )
        view = SOWDocumentRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=document_pk)
        self.assertEqual(response.data.get("id"), document_pk)

    def test_retrieval_of_non_existent_sow_document(self):
        """
        API should raise error for non existent sow document
        """
        fake = Faker()
        invalid_document_pk: int = fake.pyint()
        # Make sure the document does not exist
        invalid_document_pk = (
            invalid_document_pk
            if invalid_document_pk != self.sow_document.id
            else invalid_document_pk + 100000
        )
        url: str = reverse(
            self.document_retrieve_update_destroy_endpoint,
            kwargs={"pk": invalid_document_pk},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=invalid_document_pk)
        self.assertEqual(response.status_code, 404)

    @patch("document.views.document.transaction.on_commit", return_value=None)
    @patch(
        "document.views.document.DocumentService.update_document",
        return_value=None,
    )
    def test_sow_document_update(
        self,
        mock_document_update_call,
        mock_upload_sow_and_service_detail_in_cw,
    ):
        """
        Method to test SOW document update
        """
        fake = Faker()
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, user=self.user, author=self.user
        )
        document_id: int = self.sow_document.id
        url: str = reverse(
            self.document_retrieve_update_destroy_endpoint,
            kwargs={"pk": document_id},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            url,
            self.provider,
            self.user,
            data=dict(
                name=fake.name(),
                quote_id=sow_document.quote_id,
                doc_type=sow_document.doc_type,
                category=sow_document.category_id,
                user=sow_document.user_id,
            ),
        )
        view = SOWDocumentRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=document_id)
        mock_document_update_call.assert_called()
        mock_upload_sow_and_service_detail_in_cw.assert_called()
        self.assertEqual(response.status_code, 200)

    def test_sow_document_delete(self):
        """
        Method to test SOW document deletion
        """
        fake = Faker()
        url: str = reverse(
            self.document_retrieve_update_destroy_endpoint,
            kwargs={"pk": self.sow_document.id},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, pk=self.sow_document.id)
        doc_exists: bool = SOWDocument.objects.filter(
            provider=self.provider, id=self.sow_document.id
        ).exists()
        self.assertFalse(doc_exists)


class TestSOWDocumentDownloadAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentDownloadAPIView
    """

    document_download_endpoint: str = (
        "api_v1_endpoints:providers:document-download"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        cls.sow_document = SOWDocumentFactory(
            provider=cls.provider,
            customer=cls.customer,
            author=cls.user,
            user=cls.user,
            updated_by=cls.user,
        )

    @patch(
        "document.views.document.ChangeRequestSOWService.download_document",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_change_request_sow_document_download(
        self, mock_download_document_call
    ):
        """
        Method to test change request sow document download
        """
        self.provider.erp_client = mock.Mock()
        change_request_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=SOWDocument.CHANGE_REQUEST,
        )
        url: str = reverse(
            self.document_download_endpoint,
            kwargs={"id": change_request_document.id},
        )
        url = add_query_params_to_url(
            url, query_params=dict(type="pdf", name="")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentDownloadAPIView.as_view()
        response: Response = view(request, id=change_request_document.id)
        expected_result: Dict = dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        )
        self.assertEqual(response.data, expected_result)

    @patch(
        "document.views.document.DocumentService.download_document",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_sow_document_pdf_download(self, mock_download_document_call):
        """
        Test sow document download
        """
        self.provider.erp_client = mock.Mock()
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=FuzzyChoice(
                choices=[SOWDocument.GENERAL, SOWDocument.T_AND_M]
            ).fuzz(),
        )
        url: str = reverse(
            self.document_download_endpoint,
            kwargs={"id": sow_document.id},
        )
        url = add_query_params_to_url(
            url, query_params=dict(type="pdf", name="")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentDownloadAPIView.as_view()
        response: Response = view(request, id=sow_document.id)
        expected_result: Dict = dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        )
        self.assertEqual(response.data, expected_result)

    @patch(
        "document.views.document.DocumentService.download_document",
        return_value=SAMPLE_HTML_STRING,
    )
    def test_sow_document_download(self, mock_download_document_call):
        """
        Test sow document (used as Word document) download
        """
        self.provider.erp_client = mock.Mock()
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=FuzzyChoice(
                choices=[SOWDocument.GENERAL, SOWDocument.T_AND_M]
            ).fuzz(),
        )
        url: str = reverse(
            self.document_download_endpoint,
            kwargs={"id": sow_document.id},
        )
        url = add_query_params_to_url(
            url, query_params=dict(type="doc", name="")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentDownloadAPIView.as_view()
        response: Response = view(request, id=sow_document.id)
        self.assertIsInstance(response, HttpResponse)


class TestSOWDocumentPreviewAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentPreviewAPIView
    """

    document_preview_url: str = reverse(
        "api_v1_endpoints:providers:document-preview"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        cls.sow_document = SOWDocumentFactory(
            provider=cls.provider,
            customer=cls.customer,
            author=cls.user,
            user=cls.user,
            updated_by=cls.user,
        )

    @patch(
        "document.views.document.ChangeRequestSOWService.generate_document_preview",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_change_request_document_preview(self, mock_generate_preview_call):
        """
        Test change request document preview
        """
        self.provider.erp_client = mock.Mock()
        change_request_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=SOWDocument.CHANGE_REQUEST,
        )
        fake = Faker()
        preview_payload: Dict = dict(
            author_id=change_request_document.author_id,
            category=change_request_document.category_id,
            category_id=change_request_document.category_id,
            created_on=change_request_document.created_on,
            customer=change_request_document.customer_id,
            customer_id=change_request_document.customer_id,
            doc_type=change_request_document.doc_type,
            forecast_id=change_request_document.forecast_id,
            id=change_request_document.id,
            json_config={},
            major_version=1,
            minor_version=0,
            margin=fake.pyint(),
            name=fake.name(),
            provider_id=change_request_document.provider_id,
            quote_id=change_request_document.quote_id,
            updated_by_id=change_request_document.updated_by_id,
            updated_on=change_request_document.updated_on,
            user=change_request_document.user_id,
            user_id=change_request_document.user_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_preview_url,
            self.provider,
            self.user,
            data=preview_payload,
        )
        view = SOWDocumentPreviewAPIView.as_view()
        response: Response = view(request)
        expected_result: Dict = dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        )
        self.assertEqual(response.data, expected_result)

    @patch(
        "document.views.document.DocumentService.generate_document_preview",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_sow_document_preview(self, mock_document_preview_call):
        """
        Test preview of SOW document
        """
        self.provider.erp_client = mock.Mock()
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=get_a_random_value_from_given_choices(
                [SOWDocument.GENERAL, SOWDocument.T_AND_M]
            ),
        )
        fake = Faker()
        preview_payload: Dict = dict(
            author_id=sow_document.author_id,
            category=sow_document.category_id,
            category_id=sow_document.category_id,
            created_on=sow_document.created_on,
            customer=sow_document.customer_id,
            customer_id=sow_document.customer_id,
            doc_type=sow_document.doc_type,
            forecast_id=sow_document.forecast_id,
            id=sow_document.id,
            json_config={},
            major_version=1,
            minor_version=0,
            margin=fake.pyint(),
            name=fake.name(),
            provider_id=sow_document.provider_id,
            quote_id=sow_document.quote_id,
            updated_by_id=sow_document.updated_by_id,
            updated_on=sow_document.updated_on,
            user=sow_document.user_id,
            user_id=sow_document.user_id,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.document_preview_url,
            self.provider,
            self.user,
            data=preview_payload,
        )
        view = SOWDocumentPreviewAPIView.as_view()
        response: Response = view(request)
        expected_result: Dict = dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        )
        self.assertEqual(response.data, expected_result)


class TestSendAccountManagerEmail(BaseProviderTestCase):
    """
    Test view: SendAccountManagerEmail
    """

    account_manager_email_endpoint: str = (
        "api_v1_endpoints:providers:document-am-email"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()
        cls.sow_document = SOWDocumentFactory(
            provider=cls.provider,
            customer=cls.customer,
            author=cls.user,
            user=cls.user,
            updated_by=cls.user,
        )

    @patch(
        "document.views.document.email_sow_document_to_account_manager.apply_async",
        return_value=mock.Mock(),
    )
    @patch("document.views.document.QuoteService.get_quote", return_value={})
    @patch(
        "document.views.document.ChangeRequestSOWService.download_document",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_account_manager_email_for_change_request_email(
        self,
        mock_download_document_call,
        mock_get_quote_call,
        mock_email_sow_document_to_account_manager_call,
    ):
        """
        Test account manager email sending for change request document
        """
        fake = Faker()
        self.provider.erp_client = mock.Mock()
        # Mock only the bare minimum data.
        # E.g. The response of get_customer() method. Only territory_manager_id is required for
        # this test case, hence the return value.
        self.provider.erp_client.get_customer.return_value = dict(
            territory_manager_id=fake.pyint()
        )
        self.provider.erp_client.get_member.return_value = dict()
        change_request_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider,
            author=self.user,
            updated_by=self.user,
            user=self.user,
            doc_type=SOWDocument.CHANGE_REQUEST,
        )
        url: str = reverse(
            self.account_manager_email_endpoint,
            kwargs={"pk": change_request_document.id},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            url,
            self.provider,
            self.user,
        )
        view = SendAccountManagerEmail.as_view()
        response: Response = view(request, pk=change_request_document.id)
        # Make sure that task is triggered.
        mock_email_sow_document_to_account_manager_call.assert_called()
        self.assertEqual(response.status_code, 202)

    @patch(
        "document.views.document.email_sow_document_to_account_manager.apply_async",
        return_value=mock.MagicMock(),
    )
    @patch(
        "document.views.document.QuoteService.get_quote",
        return_value={},
    )
    @patch(
        "document.views.document.DocumentService.download_document",
        return_value=dict(
            file_path=SAMPLE_FILE_PATH, file_name=SAMPLE_FILE_NAME
        ),
    )
    def test_account_manager_email_for_sow_document(
        self,
        download_document,
        get_quote,
        email_sow_document_to_account_manager,
    ):
        """
        Test account manager email sending for sow document
        """
        sow_document: SOWDocument = SOWDocumentFactory(
            provider=self.provider, doc_type="Fixed Fee"
        )
        self.provider.erp_client = mock.Mock()
        self.provider.erp_client.get_customer.return_value = dict(
            territory_manager_id=10
        )
        self.provider.erp_client.get_member.return_value = mock.Mock()
        url: str = reverse(
            self.account_manager_email_endpoint,
            kwargs={"pk": sow_document.id},
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            url,
            self.provider,
            self.user,
        )
        view = SendAccountManagerEmail.as_view()
        response: Response = view(request, pk=sow_document.id)
        self.assertEqual(response.status_code, 202)


class TestSOWDocumentLastUpdatedStatsAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentLastUpdatedStatsAPIView
    """

    last_updated_stats_url: str = reverse(
        "api_v1_endpoints:providers:sow-document-last-update-stats"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_sow_document_last_updated_stats(
        self, mock_sow_quote_setting_configured
    ):
        now = datetime.now()
        last_7_day = now - timedelta(days=7)
        last_15_day = now - timedelta(days=15)
        last_45_day = now - timedelta(days=45)
        # Create mock sow documents within the date ranges.
        datetime_within_last_7_days = last_7_day + timedelta(days=3)
        datetime_between_last_15_and_7_days = last_15_day + timedelta(days=3)
        datetime_between_last_45_and_15_days = last_45_day + timedelta(days=5)
        datetime_before_last_45_days = last_45_day - timedelta(days=5)
        SOWDocumentFactory.create(
            provider=self.provider,
            user=self.user,
            updated_on=datetime_within_last_7_days,
        )
        SOWDocumentFactory.create(
            provider=self.provider,
            user=self.user,
            updated_on=datetime_between_last_15_and_7_days,
        )
        SOWDocumentFactory.create(
            provider=self.provider,
            user=self.user,
            updated_on=datetime_between_last_45_and_15_days,
        )
        SOWDocumentFactory.create(
            provider=self.provider,
            user=self.user,
            updated_on=datetime_before_last_45_days,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.last_updated_stats_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        # Response should be four dicts for each time period range.
        self.assertEqual(len(response.data), 4)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_technology_type_filter_on_sow_last_updated_stats(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test technology_type filter on sow last updated stats API.
        """
        fake = Faker()
        no_of_docs: int = 2
        sow_documents = SOWDocumentFactory.create_batch(
            no_of_docs,
            provider=self.provider,
            author=self.user,
            category=SOWCategoryFactory(name="Mock category name"),
            updated_on=fake.date_this_year(),
        )
        technology_type: str = sow_documents[0].category.name
        SOWDocumentFactory.create_batch(
            3,
            provider=self.provider,
            author=self.user,
            updated_on=fake.date_this_year(),
        )
        url: str = add_query_params_to_url(
            self.last_updated_stats_url,
            query_params=dict(technology_type=technology_type),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, no_of_docs)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_date_range_filter_on_sow_last_updated_stats(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test date range filter on sow documents last updated stats API
        """
        fake = Faker()
        now = datetime.now()
        last_7_days = datetime.now() - timedelta(days=7)
        last_15_days = datetime.now() - timedelta(days=15)
        datetime_within_last_7_days = last_7_days - timedelta(days=3)
        datetime_between_last_15_to_7_days = last_7_days + timedelta(days=4)
        expected_docs_count: int = 1
        SOWDocumentFactory.create_batch(
            expected_docs_count,
            provider=self.provider,
            updated_on=datetime_within_last_7_days,
        )
        SOWDocumentFactory.create_batch(
            1,
            provider=self.provider,
            updated_on=datetime_between_last_15_to_7_days,
        )
        filter_range_start = last_7_days.date()
        filter_range_end = now.date()
        # Filter documents updated within last 7 days
        url: str = add_query_params_to_url(
            self.last_updated_stats_url,
            query_params=dict(
                updated_after=filter_range_start.strftime("%Y-%m-%d"),
                updated_before=filter_range_end.strftime("%Y-%m-%d"),
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, expected_docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_customer_name_filter_on_sow_last_updated_stats(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test customer name filter on sow documents last updated stats API
        """
        customer_name: str = "Mock company name"
        customer_docs_count: int = 2
        SOWDocumentFactory.create_batch(
            customer_docs_count,
            provider=self.provider,
            customer=CustomerFactory(name=customer_name),
        )
        SOWDocumentFactory.create_batch(
            3,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.last_updated_stats_url,
            query_params=dict(
                customer_name=customer_name,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, customer_docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_author_filter_on_sow_last_updated_stats(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test filtering by author on sow last updated stats API.
        """
        author_docs_count: int = 1
        author: User = ProviderUserFactory(provider=self.provider)
        SOWDocumentFactory.create_batch(
            author_docs_count,
            provider=self.provider,
            author=author,
        )
        SOWDocumentFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.last_updated_stats_url,
            query_params=dict(
                author_id=author.id,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, author_docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=False,
    )
    def test_failure_of_last_update_stats_when_sow_setting_not_configure(
        self, mock_sow_quote_setting_configured
    ):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.last_updated_stats_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentLastUpdatedStatsAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)


class TestSOWDocumentAuthorsListAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentAuthorsListAPIView
    """

    document_authors_list_url: str = reverse(
        "api_v1_endpoints:providers:sow-document-authors-list"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_authors_document_count_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test authors document count listing API
        """
        author_1 = ProviderUserFactory(provider=self.provider)
        authors_doc_count: int = 2
        SOWDocumentFactory.create_batch(
            authors_doc_count,
            provider=self.provider,
            author=author_1,
            user=self.user,
        )
        SOWDocumentFactory.create_batch(
            1, provider=self.provider, author=self.user, user=self.user
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.document_authors_list_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for author_data in response.data:
            if author_data["author_id"] == str(author_1.id):
                actual_docs_count = author_data["count"]
        self.assertEqual(actual_docs_count, authors_doc_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=False,
    )
    def test_failure_of_author_document_count_listing_when_setting_not_configured(
        self, mock_sow_quote_setting_configured
    ):
        """
        API should raise errors when sow quote setting is not configured
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.document_authors_list_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_author_id_validation_if_used_in_filter(
        self, mock_sow_quote_setting_configured
    ):
        """
        API should raise error if author ID is not UUID.
        """
        fake = Faker()
        url: str = add_query_params_to_url(
            self.document_authors_list_url,
            query_params=dict(author_id=fake.pyint()),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_technology_type_filter_on_authors_document_count_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test technology_type filter on authors document count listing API
        """
        fake = Faker()
        count_of_category_docs: int = 2
        category_name: str = "Mock category name"
        SOWDocumentFactory.create_batch(
            count_of_category_docs,
            provider=self.provider,
            author=self.user,
            category=SOWCategoryFactory(name=category_name),
        )
        SOWDocumentFactory.create_batch(
            1,
            provider=self.provider,
            author=self.user,
        )
        url: str = add_query_params_to_url(
            self.document_authors_list_url,
            query_params=dict(technology_type=category_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, count_of_category_docs)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_date_range_filter_on_authors_document_count_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test date range filter on authors document count listing API
        """
        fake = Faker()
        now = datetime.now()
        last_7_days = now - timedelta(days=7)
        last_15_days = now - timedelta(days=15)

        datetime_within_last_7_days = last_7_days + timedelta(days=3)
        datetime_between_last_15_and_7_days = last_15_days + timedelta(days=3)
        # Create 2 docs with updated datetime within last 7 days time range for different users
        docs_count: int = 2
        SOWDocumentFactory.create(
            provider=self.provider,
            updated_on=datetime_within_last_7_days,
            author=ProviderUserFactory(provider=self.provider),
        )
        SOWDocumentFactory.create(
            provider=self.provider,
            updated_on=datetime_within_last_7_days,
            author=self.user,
        )
        # Create a doc with updated datetime between last 15 and 7 days.
        SOWDocumentFactory.create(
            provider=self.provider,
            updated_on=datetime_between_last_15_and_7_days,
        )
        filter_range_start = last_7_days.date().strftime("%Y-%m-%d")
        filter_range_end = now.date().strftime("%Y-%m-%d")
        url: str = add_query_params_to_url(
            self.document_authors_list_url,
            query_params=dict(
                updated_after=filter_range_start,
                updated_before=filter_range_end,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_customer_name_filter(self, mock_sow_quote_setting_configured):
        """
        Test customer name filter on authors document count listing API
        """
        customer_name: str = "Mock company name"
        customer_docs_count: int = 2
        SOWDocumentFactory.create_batch(
            customer_docs_count,
            provider=self.provider,
            customer=CustomerFactory(name=customer_name),
        )
        SOWDocumentFactory.create_batch(
            2,
            provider=self.provider,
        )
        url: str = add_query_params_to_url(
            self.document_authors_list_url,
            query_params=dict(
                customer_name=customer_name,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDocumentAuthorsListAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        self.assertEqual(actual_docs_count, customer_docs_count)


class TestSOWDocumentCountByTechnologyAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentCountByTechnologyAPIView
    """

    sow_document_count_by_tech_type_url: str = reverse(
        "api_v1_endpoints:providers:sow-document-count-by-tech"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    @patch(
        "document.views.document.SOWDocumentCountByTechnologyAPIView.calculate_margin_for_documents",
        return_value=100,
    )
    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_margin_of_sow_docs_by_technology_type(
        self,
        mock_sow_quote_setting_configured,
        mock_calculate_margin_for_documents_call,
    ):
        """
        Test margin of sow documents by technology type
        """
        docs_count: int = 2
        SOWDocumentFactory.create_batch(
            docs_count,
            provider=self.provider,
        )
        document_catgories_count: int = (
            SOWCategory.objects.distinct("name").order_by().count()
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_document_count_by_tech_type_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentCountByTechnologyAPIView.as_view()
        response: Response = view(request)
        actual_docs_count: int = 0
        for item in response.data:
            actual_docs_count += item.get("count", 0)
        # Response should contain data for all categories
        self.assertEqual(len(response.data), document_catgories_count)
        self.assertEqual(actual_docs_count, docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=False,
    )
    def test_api_error_when_sow_quote_settings_not_configured(
        self, mock_sow_quote_setting_configured_call
    ):
        """
        API should raise error when SOW quote status settings is not configured.
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_document_count_by_tech_type_url,
            self.provider,
            self.user,
        )
        view = SOWDocumentCountByTechnologyAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_failure_to_get_margin_of_sow_docs_by_tech_type_for_unauthorized_user(
        self,
    ):
        """
        API should forbid access to unauthorized users
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_document_count_by_tech_type_url,
            self.provider,
        )
        view = SOWDocumentCountByTechnologyAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 401)


class TestSOWDashboardTableDataAPIView(BaseProviderTestCase):
    """
    Test SOWDashboardTableDataAPIView
    """

    table_data_url: str = reverse(
        "api_v1_endpoints:providers:sow-document-table-data"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_sow_table_data_listing(self, mock_sow_quote_setting_configured):
        """
        Test SOW table data listing
        """
        docs_count: int = 2
        SOWDocumentFactory.create_batch(docs_count, provider=self.provider)
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider=self.provider
        )
        # This creates mock quotes data for sow documents which is needed for documents to
        # be part of response
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", self.table_data_url, self.provider, self.user
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_if_sow_table_data_listing_pagination_is_working(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test SOW table data listing pagination
        """
        docs_count: int = 3
        SOWDocumentFactory.create_batch(docs_count, provider=self.provider)
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider=self.provider
        )
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        required_page_size: int = 1
        url: str = add_query_params_to_url(
            self.table_data_url,
            query_params=dict(page_size=required_page_size),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), docs_count)
        self.assertEqual(len(response.data.get("results")), required_page_size)
        self.assertEqual(response.data.get("links").get("next_page_number"), 2)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_technology_type_filter_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test technology type filter on sow table data listing
        """
        fake = Faker()
        technology_type_doc_count: int = 1
        category_name: str = "Mock category name"
        SOWDocumentFactory.create_batch(
            technology_type_doc_count,
            provider=self.provider,
            category=SOWCategoryFactory(name=category_name),
        )
        SOWDocumentFactory.create_batch(
            2,
            provider=self.provider,
        )
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url: str = add_query_params_to_url(
            self.table_data_url,
            query_params=dict(technology_type=category_name),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET", url, self.provider, self.user
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), technology_type_doc_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_date_range_filter_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test date range filter on sow dashboard table data listing
        """
        now = datetime.now()
        datetime_within_last_7_days = now - timedelta(days=3)
        datetime_between_last_15_and_7_days = now - timedelta(days=10)
        sow_docs_count_within_time_range: int = 1
        SOWDocumentFactory.create_batch(
            sow_docs_count_within_time_range,
            provider=self.provider,
            updated_on=datetime_within_last_7_days,
        )
        SOWDocumentFactory.create(
            provider=self.provider,
            updated_on=datetime_between_last_15_and_7_days,
        )
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client = mock.MagicMock()
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        filter_range_start = (
            (now - timedelta(days=7)).date().strftime("%Y-%m-%d")
        )
        filter_range_end = now.date().strftime("%Y-%m-%d")
        url: str = add_query_params_to_url(
            self.table_data_url,
            query_params=dict(
                updated_after=filter_range_start,
                updated_before=filter_range_end,
            ),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(
            response.data.get("count"), sow_docs_count_within_time_range
        )

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_customer_id_filter_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test customer ID filter on sow dashboard table data listing
        """
        customer_docs_count: int = 1
        customer: Customer = CustomerFactory(provider=self.provider)
        SOWDocumentFactory.create_batch(
            customer_docs_count, provider=self.provider, customer=customer
        )
        SOWDocumentFactory.create_batch(2, provider=self.provider)
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(customer_id=customer.id)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), customer_docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_customer_name_filter_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test customer name filter on sow dashboard table data listing
        """
        customer_docs_count: int = 1
        customer: Customer = CustomerFactory(provider=self.provider)
        SOWDocumentFactory.create_batch(
            customer_docs_count, provider=self.provider, customer=customer
        )
        SOWDocumentFactory.create_batch(2, provider=self.provider)
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(customer_name=customer.name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), customer_docs_count)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_author_id_filter_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test customer name filter on sow dashboard table data listing
        """
        author_doc_count: int = 1
        author: Customer = ProviderUserFactory(provider=self.provider)
        SOWDocumentFactory.create_batch(
            author_doc_count, provider=self.provider, author=author
        )
        SOWDocumentFactory.create_batch(2, provider=self.provider)
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(author_id=str(author.id))
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), author_doc_count)

    @patch(
        "document.views.document.filter_documents_by_doc_status",
    )
    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_doc_quote_status_filter_on_sow_dashboard_table_data_listing(
        self,
        mock_sow_quote_setting_configured,
        mock_filter_documents_by_doc_status_call,
    ):
        """
        Test customer name filter on sow dashboard table data listing
        """
        doc_quote_statuses = ["won", "open", "lost"]
        doc_status = FuzzyChoice(choices=doc_quote_statuses).fuzz()
        SOWDocumentFactory.create_batch(3, provider=self.provider)
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        # Mimicking the behaviour of the method: filter_documents_by_doc_status
        # This will return sliced queryset with only first object, as it only 1 doc has
        # the required status
        mock_filter_documents_by_doc_status_call.side_effect = (
            mock_filter_documents_by_doc_status
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params={"doc-status": doc_status}
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=False,
    )
    def test_failure_of_sow_dashboard_table_data_listing_when_settings_not_configured(
        self, mock_sow_quote_setting_configured
    ):
        """
        API should raise error if SOW dashboard setting not configured
        """
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.table_data_url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    @patch("document.views.document.is_valid_uuid", return_value=False)
    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=False,
    )
    def test_failure_of_sow_dashboard_table_data_listing_for_invalid_author_id(
        self, mock_sow_quote_setting_configured, mock_is_valid_uuid
    ):
        """
        API should raise error if invalid author id is passed in query params
        """
        fake = Faker()
        invalid_author_id: int = fake.pyint()
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(author_id=invalid_author_id)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_latest_updated_on_date_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by latest updated on date on SOW dashboard table data listing
        """
        now = datetime.now()
        past_datetime = now - timedelta(days=7)
        latest_document = SOWDocumentFactory.create(
            provider=self.provider, updated_on=now
        )
        old_document = SOWDocumentFactory.create(
            provider=self.provider, updated_on=past_datetime
        )
        expected_doc_order: List[int] = [latest_document.id, old_document.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="-updated_on")
        )
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_oldest_updated_on_date_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by oldest updated on date on SOW dashboard table data listing
        """
        now = datetime.now()
        past_datetime = now - timedelta(days=7)
        latest_document = SOWDocumentFactory.create(
            provider=self.provider, updated_on=now
        )
        old_document = SOWDocumentFactory.create(
            provider=self.provider, updated_on=past_datetime
        )
        expected_doc_order: List[int] = [old_document.id, latest_document.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="updated_on")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_latest_created_on_date_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by latest created on date on SOW dashboard table data listing
        """
        now = datetime.now()
        past_datetime = now - timedelta(days=7)
        latest_document = SOWDocumentFactory.create(
            provider=self.provider, created_on=now
        )
        old_document = SOWDocumentFactory.create(
            provider=self.provider, created_on=past_datetime
        )
        expected_doc_order: List[int] = [latest_document.id, old_document.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="-created_on")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_oldest_created_on_date_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by oldest created on date on SOW dashboard table data listing
        """
        now = datetime.now()
        past_datetime = now - timedelta(days=7)
        latest_document = SOWDocumentFactory.create(
            provider=self.provider, created_on=now
        )
        old_document = SOWDocumentFactory.create(
            provider=self.provider, created_on=past_datetime
        )
        expected_doc_order: List[int] = [old_document.id, latest_document.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="created_on")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_alphabetical_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by alphabetical name on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            name="AA", provider=self.provider
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            name="AB", provider=self.provider
        )
        expected_doc_order: List[int] = [sow_document_1.id, sow_document_2.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="name")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_reverse_alphabetical_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by reverse alphabetical name on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            name="AA", provider=self.provider
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            name="AB", provider=self.provider
        )
        expected_doc_order: List[int] = [sow_document_2.id, sow_document_1.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="-name")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_lowest_margin_first_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by lowest margin first on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            margin=10, provider=self.provider
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            margin=20, provider=self.provider
        )
        expected_doc_order: List[int] = [sow_document_1.id, sow_document_2.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="margin")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_highest_margin_first_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by highest margin first on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            margin=10, provider=self.provider
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            margin=20, provider=self.provider
        )
        expected_doc_order: List[int] = [sow_document_2.id, sow_document_1.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="-margin")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_lowest_budget_hours_first_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by lowest budget hours first on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            json_config=dict(service_cost=dict(project_management_hours=10)),
            provider=self.provider,
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            json_config=dict(service_cost=dict(project_management_hours=20)),
            provider=self.provider,
        )
        expected_doc_order: List[int] = [sow_document_1.id, sow_document_2.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="budget_hours")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_ordering_by_highest_budget_hours_first_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test ordering by highest budget hours first on SOW dashboard table data listing
        """
        sow_document_1: SOWDocument = SOWDocumentFactory.create(
            json_config=dict(service_cost=dict(total_hours=10)),
            provider=self.provider,
        )
        sow_document_2: SOWDocument = SOWDocumentFactory.create(
            json_config=dict(service_cost=dict(total_hours=20)),
            provider=self.provider,
        )
        expected_doc_order: List[int] = [sow_document_2.id, sow_document_1.id]
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(ordering="-budget_hours")
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        actual_doc_order: List[int] = [
            doc.get("id") for doc in response.data.get("results")
        ]
        self.assertEqual(expected_doc_order, actual_doc_order)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_search_by_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test search by name on SOW dashboard table data listing
        """
        search_name: str = "Document 1"
        SOWDocumentFactory.create(name=search_name, provider=self.provider)
        SOWDocumentFactory.create(name="Document 2", provider=self.provider)
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(search=search_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_search_by_customer_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test search by customer name on SOW dashboard table data listing
        """
        search_customer_name: str = "Customer 1"
        SOWDocumentFactory.create(
            customer=CustomerFactory(name=search_customer_name),
            provider=self.provider,
        )
        SOWDocumentFactory.create(
            customer=CustomerFactory(name="Customer 2"),
            provider=self.provider,
        )
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(search=search_customer_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_search_by_category_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test search by category name on SOW dashboard table data listing
        """
        category_name_to_search: str = "Mock category name 1"
        SOWDocumentFactory.create(
            category=SOWCategoryFactory(name=category_name_to_search),
            provider=self.provider,
        )
        SOWDocumentFactory.create(
            category=SOWCategoryFactory(name="Mock category name 2"),
            provider=self.provider,
        )
        sow_documents = SOWDocument.objects.filter(
            provider_id=self.provider.id
        )
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url,
            query_params=dict(search=category_name_to_search),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_search_by_author_first_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test search by author first name on SOW dashboard table data listing
        """
        author_first_name: str = "First name 1"
        SOWDocumentFactory.create(
            author=ProviderUserFactory(
                provider=self.provider, first_name=author_first_name
            ),
            provider=self.provider,
        )
        SOWDocumentFactory.create(
            author=ProviderUserFactory(
                provider=self.provider, first_name="First name 2"
            ),
            provider=self.provider,
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(search=author_first_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)

    @patch(
        "document.views.document.sow_quote_setting_configured",
        return_value=True,
    )
    def test_search_by_author_last_name_on_sow_dashboard_table_data_listing(
        self, mock_sow_quote_setting_configured
    ):
        """
        Test search by author last name on SOW dashboard table data listing
        """
        author_last_name: str = "Last name 1"
        SOWDocumentFactory.create(
            author=ProviderUserFactory(
                provider=self.provider, last_name=author_last_name
            ),
            provider=self.provider,
        )
        SOWDocumentFactory.create(
            author=ProviderUserFactory(
                provider=self.provider, last_name="Last name 2"
            ),
            provider=self.provider,
        )
        sow_documents = SOWDocument.objects.filter(provider=self.provider)
        self.provider.erp_client = mock.MagicMock()
        mocked_quotes = get_mock_company_quotes(sow_documents)
        self.provider.erp_client.get_company_quote_statuses.return_value = (
            mocked_quotes
        )
        url = add_query_params_to_url(
            self.table_data_url, query_params=dict(search=author_last_name)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = SOWDashboardTableDataAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(len(response.data.get("results")), 1)


class TestSOWDocumentGrossProfitCalculationAPIView(BaseProviderTestCase):
    """
    Test view: SOWDocumentGrossProfitCalculationAPIView
    """

    gross_profit_url: str = reverse("api_v1_endpoints:providers:gross-profit")

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    @patch(
        "document.views.document.QuoteService.get_forecast_from_template",
        return_value=dict(revenue=100, cost=50),
    )
    def test_gross_profit_calculation(
        self, mock_get_forecast_from_template_call
    ):
        request_body: Dict = dict(
            doc_type=random.choice([SOWDocument.T_AND_M, SOWDocument.GENERAL]),
            json_config=dict(),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.gross_profit_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWDocumentGrossProfitCalculationAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.data.get("gross_profit_percent"), 50)

    def test_api_error_calculating_gross_profit_for_invalid_request_body(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.gross_profit_url,
            self.provider,
            self.user,
            data=dict(),
        )
        view = SOWDocumentGrossProfitCalculationAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)


class TestSOWVendorOnboardingSettingCreateRetrieveUpdateAPIView(
    BaseProviderTestCase
):
    """
    Test view: SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView
    """

    sow_vendor_setting_create_retrieve_url: str = reverse(
        "api_v1_endpoints:providers:sow-vendor-onboarding-"
        "setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.customer = CustomerFactory()
        cls.setup_provider_and_user()

    def test_creation_of_sow_vendor_onboarding_setting(self):
        fake = Faker()
        request_body: Dict = dict(
            territories=[
                dict(territory_crm_id=fake.pyint(), territory_name=fake.name())
            ],
            customer_type=dict(
                customer_type_crm_id=fake.pyint(),
                customer_type_name=fake.bs(),
            ),
            vendor_list_statuses=[
                dict(
                    customer_status_name=fake.bs(),
                    customer_status_crm_id=fake.pyint(),
                )
            ],
            vendor_create_status=dict(
                customer_status_crm_id=fake.pyint(),
                customer_status_name=fake.bs(),
            ),
            vendor_onboarding_email_alias=fake.email(),
            vendor_onboarding_email_subject=fake.text(max_nb_chars=10),
            vendor_onboarding_email_body=fake.text(max_nb_chars=10),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.sow_vendor_setting_create_retrieve_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 201)

    def test_retrieval_of_sow_vendor_onboarding_setting(self):
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            SOWVendorOnboardingSettingFactory(provider=self.provider)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_vendor_setting_create_retrieve_url,
            self.provider,
            self.user,
        )
        view = SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data.get("id"), sow_vendor_onboarding_setting.id
        )

    def test_error_retrieving_sow_vendor_settings_if_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sow_vendor_setting_create_retrieve_url,
            self.provider,
            self.user,
        )
        view = SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_error_creating_sow_vendor_settings_if_it_already_exists(self):
        fake = Faker()
        SOWVendorOnboardingSettingFactory.create(provider=self.provider)
        request_body: Dict = dict(
            territories=[
                dict(territory_crm_id=fake.pyint(), territory_name=fake.name())
            ],
            customer_type=dict(
                customer_type_crm_id=fake.pyint(),
                customer_type_name=fake.company(),
            ),
            vendor_statuses=dict(
                customer_status_name=fake.bs(), customer_status_id=fake.pyint()
            ),
            vendor_onboarding_email_alias=fake.email(),
            vendor_onboarding_email_subject=fake.text(max_nb_chars=10),
            vendor_onboarding_email_body=fake.text(max_nb_chars=10),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.sow_vendor_setting_create_retrieve_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)


def delete_sow_vendor_attachment_created_during_test(
    sow_vendor_onboarding_attachment: SOWVendorOnboardingAttachments,
    provider_id: int,
) -> None:
    # Delete attachment file created during factory object creation.
    # Refer to file path format in model SOWVendorOnboardingAttachments
    attachment_path: str = sow_vendor_onboarding_attachment.attachment.path
    dir_path: str = attachment_path.split(f"{provider_id}/")[0]
    dir_to_delete: str = dir_path + f"{provider_id}"
    shutil.rmtree(dir_to_delete)
    return


class TestSOWVendorOnboardingAttachmentsListCreateAPIView(
    BaseProviderTestCase
):
    """
    Test view: SOWVendorOnboardingAttachmentsListCreateAPIView
    """

    attachment_list_create_url: str = reverse(
        "api_v1_endpoints:providers:vendor-onboarding-setting-attachment-list-create"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @mock.patch(
        "document.views.document.SOWVendorOnboardingAttachmentsListCreateAPIView.perform_create"
    )
    def test_sow_vendor_attachment_creation(self, perform_create_call):
        SOWVendorOnboardingSettingFactory.create(provider=self.provider)
        file_name: str = "File.txt"
        mock_file = SimpleUploadedFile(
            file_name, b"ABC", content_type="text/plain"
        )
        client = APIRequestFactory()
        request: Request = client.post(
            self.attachment_list_create_url,
            data=dict(attachment=mock_file, name=file_name),
            format="multipart",
        )
        force_authenticate(request, user=self.user)
        setattr(request, "provider", self.provider)
        view = SOWVendorOnboardingAttachmentsListCreateAPIView.as_view()
        response: Response = view(request)
        perform_create_call.assert_called()
        self.assertEqual(response.status_code, 201)

    def test_error_creating_attachment_if_sow_vendor_setting_not_configured(
        self,
    ):
        file_name: str = "File.txt"
        mock_file = SimpleUploadedFile(
            file_name, b"ABC", content_type="text/plain"
        )
        client = APIRequestFactory()
        request: Request = client.post(
            self.attachment_list_create_url,
            data=dict(attachment=mock_file, name=file_name),
            format="multipart",
        )
        force_authenticate(request, user=self.user)
        setattr(request, "provider", self.provider)
        view = SOWVendorOnboardingAttachmentsListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 412)

    def test_listing_of_sow_vendor_attachments(self):
        # Creating SOWVendorOnboardingAttachmentsFactory object will actually create
        # file in os. So don't create actual object in DB.
        SOWVendorOnboardingSettingFactory.create(provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.attachment_list_create_url,
            self.provider,
            self.user,
        )
        view = SOWVendorOnboardingAttachmentsListCreateAPIView.as_view()
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)


class TestSOWVendorOnboardingAttachmentsDestroyAPIView(BaseProviderTestCase):
    """
    Test view: SOWVendorOnboardingAttachmentsDestroyAPIView
    """

    vendor_onboarding_attachment_delete_endpoint_name: str = (
        "api_v1_endpoints:providers:"
        "vendor-onboarding-setting-attachment-delete"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @mock.patch(
        "document.views.document.SOWVendorOnboardingAttachmentsDestroyAPIView.get_object"
    )
    def test_delete_vendor_onboarding_attachment(self, get_object_call):
        # Creating SOWVendorOnboardingAttachmentsFactory object will actually create
        # file in os. Mock the get_object method so it API doesn't throw 404
        attachment_id = 10
        url: str = reverse(
            self.vendor_onboarding_attachment_delete_endpoint_name,
            kwargs=dict(id=attachment_id),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = SOWVendorOnboardingAttachmentsDestroyAPIView.as_view()
        response: Response = view(request, id=attachment_id)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            SOWVendorOnboardingAttachments.objects.filter(
                provider=self.provider, id=attachment_id
            ).exists(),
            False,
        )

    def test_error_deleting_vendor_onboarding_attachment_if_it_doesnt_exist(
        self,
    ):
        url: str = reverse(
            self.vendor_onboarding_attachment_delete_endpoint_name,
            kwargs=dict(id=10),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = SOWVendorOnboardingAttachmentsDestroyAPIView.as_view()
        response: Response = view(request, id=10)
        self.assertEqual(response.status_code, 404)


class TestDefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView(
    BaseProviderTestCase
):
    """
    Test view: DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView
    """

    setting_url: str = reverse(
        "api_v1_endpoints:providers:document-creator-role-setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_retrieval_of_default_document_creator_role_setting(self):
        setting: DefaultDocumentCreatorRoleSetting = (
            DefaultDocumentCreatorRoleSettingFactory.create(
                provider=self.provider
            )
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.setting_url,
            self.provider,
            self.user,
        )
        expected_result: Dict = dict(
            provider=self.provider.id,
            default_sow_creator_role_id=setting.default_sow_creator_role_id,
            default_change_request_creator_role_id=setting.default_change_request_creator_role_id,
        )
        view = (
            DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.data, expected_result)

    def test_creation_of_default_document_creator_role_setting(self):
        request_body: Dict = dict(
            default_sow_creator_role_id=1,
            default_change_request_creator_role_id=2,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        expected_result: Dict = dict(provider=self.provider.id, **request_body)
        view = (
            DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.data, expected_result)

    def test_error_creating_default_document_creator_role_setting_if_already_exists(
        self,
    ):
        DefaultDocumentCreatorRoleSettingFactory.create(provider=self.provider)
        request_body: Dict = dict(
            default_sow_creator_role_id=1,
            default_change_request_creator_role_id=2,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = (
            DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_update_of_default_document_creator_role_setting(self):
        DefaultDocumentCreatorRoleSettingFactory.create(provider=self.provider)
        request_body: Dict = dict(
            default_sow_creator_role_id=10,
            default_change_request_creator_role_id=20,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        expected_result: Dict = dict(provider=self.provider.id, **request_body)
        view = (
            DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.data, expected_result)


class TestSoWHourlyResourcesDescriptionMappingListCreateAPIView(
    BaseProviderTestCase
):
    """
    Test view: SoWHourlyResourcesDescriptionMappingListCreateAPIView
    """

    url: str = reverse(
        "api_v1_endpoints:providers:resource-description-list-create"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()
        cls.fake = Faker()

    def test_listing_of_hourly_resources_description_mapping(self):
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data.get("results")), 1)

    def test_creation_of_vendor_description_mapping(self):
        request_body: Dict = dict(
            vendor_name=self.fake.company(),
            vendor_crm_id=self.fake.pyint(),
            resource_description=self.fake.text(max_nb_chars=5),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data.get("vendor_crm_id"),
            request_body.get("vendor_crm_id"),
        )

    def test_error_creating_duplicate_description_for_same_resource(self):
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_crm_id=10
        )
        request_body: Dict = dict(
            vendor_name=self.fake.company(),
            vendor_crm_id=10,
            resource_description=self.fake.text(max_nb_chars=5),
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST", self.url, self.provider, self.user, data=request_body
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.status_code, 400)

    def test_ordering_by_vendor_name_in_listing_of_hourly_resources_description(
        self,
    ):
        # First test alphabetical ordering by vendor name
        mapping_1 = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="A"
        )
        mapping_2 = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="B"
        )
        url: str = add_query_params_to_url(
            self.url, query_params=dict(ordering="vendor_name")
        )
        expected_order: List[int] = [mapping_1.id, mapping_2.id]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test reverse alphabetical ordering by vendor name
        url: str = add_query_params_to_url(
            self.url, query_params=dict(ordering="-vendor_name")
        )
        expected_order: List[int] = [mapping_2.id, mapping_1.id]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_ordering_by_resource_description_in_listing_of_hourly_resources_description(
        self,
    ):
        # First test alphabetical ordering
        mapping_1 = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, resource_description="A"
        )
        mapping_2 = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, resource_description="B"
        )
        url: str = add_query_params_to_url(
            self.url, query_params=dict(ordering="resource_description")
        )
        expected_order: List[int] = [mapping_1.id, mapping_2.id]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)
        # Test reverse alphabetical ordering
        url: str = add_query_params_to_url(
            self.url, query_params=dict(ordering="-resource_description")
        )
        expected_order: List[int] = [mapping_2.id, mapping_1.id]
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        actual_order: List[int] = [
            item.get("id") for item in response.data.get("results")
        ]
        self.assertEqual(expected_order, actual_order)

    def test_search_on_vendor_name_in_listing_of_hourly_resources_description(
        self,
    ):
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="Apple"
        )
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="Beats"
        )
        url: str = add_query_params_to_url(
            self.url, query_params=dict(search="apple")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)

    def test_search_on_resource_description_in_listing_of_hourly_resources_description(
        self,
    ):
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, resource_description="Apple"
        )
        SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, resource_description="Beats"
        )
        url: str = add_query_params_to_url(
            self.url, query_params=dict(search="apple")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingListCreateAPIView().as_view()
        )
        response: Response = view(request)
        self.assertEqual(response.data.get("count"), 1)


class TestSoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView(
    BaseProviderTestCase
):
    """
    Test view: SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView
    """

    endpoint_name: str = "api_v1_endpoints:providers:resource-description-retrieve-update-destroy"

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_retrieval_of_resource_description_mapping(self):
        mapping = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="Apple"
        )
        url: str = reverse(self.endpoint_name, kwargs=dict(id=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = (
            SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView().as_view()
        )
        response: Response = view(request, id=mapping.id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get("vendor_name"), "Apple")

    def test_update_to_resource_description_mapping(self):
        mapping = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="Apple", vendor_crm_id=100
        )
        update_payload: Dict = dict(
            resource_description="Updated description",
            vendor_name="Apple",
            vendor_crm_id=100,
        )
        url: str = reverse(self.endpoint_name, kwargs=dict(id=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=update_payload
        )
        view = (
            SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView().as_view()
        )
        response: Response = view(request, id=mapping.id)
        self.assertEqual(response.status_code, 200)
        mapping.refresh_from_db()
        self.assertEqual(
            mapping.resource_description,
            update_payload.get("resource_description"),
        )

    def test_delete_resource_description_mapping(self):
        mapping = SoWHourlyResourcesDescriptionMappingFactory.create(
            provider=self.provider, vendor_name="Apple", vendor_crm_id=100
        )
        url: str = reverse(self.endpoint_name, kwargs=dict(id=mapping.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE", url, self.provider, self.user
        )
        view = (
            SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView().as_view()
        )
        response: Response = view(request, id=mapping.id)
        self.assertEqual(response.status_code, 204)
        mapping_exists: bool = (
            SoWHourlyResourcesDescriptionMapping.objects.filter(
                provider_id=self.provider.id, id=mapping.id
            ).exists()
        )
        self.assertEqual(mapping_exists, False)
