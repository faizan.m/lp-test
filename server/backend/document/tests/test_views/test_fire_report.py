from datetime import datetime, timedelta

from django.urls import reverse
from mock import MagicMock
from mock import patch
from rest_framework.request import Request

from document.tests.factories import (
    FireReportPartnerSiteFactory,
    SalesPurchaseOrderDataFactory,
    ConnectwisePurchaseOrderFactory,
    ConnectwisePurchaseOrderLineitemsDataFactory,
    ConnectwiseServiceTicketFactory,
    ConnectwisePurchaseOrderDataFactory,
    FireReportSettingFactory,
    PartnerSiteSyncErrorFactory,
)
from accounts.tests.factories import CustomerFactory
from document.views.fire_report import (
    FireReportPartnerSiteListCreateAPIView,
    FireReportGenerationAPIView,
    FireReportSettingCreateRetriveUpdateAPIView,
    FireReportPartnerSitesRetrieveUpdateDestroyAPIView,
    PartnerSiteSyncErrorListAPIView,
)
from test_utils import (
    BaseProviderTestCase,
    TestClientMixin,
)
from sales.tests.utils import add_query_params_to_url


class TestFireReportPartnerSiteListCreateAPIView(BaseProviderTestCase):
    """
    Test view: FireReportPartnerSiteListCreateAPIView
    """

    sites_url: str = reverse(
        "api_v1_endpoints:providers:fire-report-partner-sites-list-create"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    @patch(
        "document.views.fire_report.create_partner_site_for_all_customers.apply_async",
        return_value=MagicMock(),
    )
    def test_creation_of_partner_site(
        self, create_partner_site_for_all_customers
    ):
        request_body: Dict = dict(
            name="ABC",
            address_line_1="Dance Street",
            address_line_2="YMCK Lane",
            city="New York",
            state="Washington DC",
            state_crm_id=1,
            country="USA",
            country_crm_id=2,
            zip="12345",
            phone_number="1234567890",
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.sites_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportPartnerSiteListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual("ABC", response.data.get("name"))
        self.assertIn("task_id", response.data)
        create_partner_site_for_all_customers.assert_called()

    def test_listing_of_partner_sites(self):
        FireReportPartnerSiteFactory.create(provider=self.provider)
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.sites_url,
            self.provider,
            self.user,
        )
        view = FireReportPartnerSiteListCreateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.data.get("results")))


class TestFireReportPartnerSitesRetrieveUpdateDestroyAPIView(
    BaseProviderTestCase
):
    """
    Test view: FireReportPartnerSitesRetrieveUpdateDestroyAPIView
    """

    url: str = "api_v1_endpoints:providers:fire-report-partner-sites-retrieve-update-destroy"

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_retrieval_of_partner_site(self):
        site: FireReportPartnerSite = FireReportPartnerSiteFactory.create(
            provider=self.provider, name="ABC"
        )
        url: str = reverse(self.url, kwargs=dict(id=site.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = FireReportPartnerSitesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, id=site.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual("ABC", response.data.get("name"))

    def test_error_retrieving_partner_site_if_does_not_exist(self):
        url: str = reverse(self.url, kwargs=dict(id=1))
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = FireReportPartnerSitesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, id=1)
        self.assertEqual(404, response.status_code)

    @patch(
        "document.views.fire_report.update_partner_site_for_cw_customers.apply_async",
        return_value=MagicMock(),
    )
    def test_update_to_partner_site(
        self, update_partner_site_for_cw_customers
    ):
        site: FireReportPartnerSite = FireReportPartnerSiteFactory.create(
            provider=self.provider, name="ABC"
        )
        url: str = reverse(self.url, kwargs=dict(id=site.id))
        request_body: Dict[str, Any] = dict(
            name="DEF",
            address_line_1="Freedom Lane",
            address_line_2="Liberty Street",
            city="Metropolis",
            state="California",
            state_crm_id=5,
            country="United States",
            country_crm_id=1,
            zip="12345",
            phone_number="1234432112",
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT", url, self.provider, self.user, data=request_body
        )
        view = FireReportPartnerSitesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, id=site.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual("DEF", response.data.get("name"))
        update_partner_site_for_cw_customers.assert_called_with(
            (self.provider.id, site.id, "ABC"), queue="high"
        )

    @patch(
        "document.views.fire_report.delete_partner_site_for_cw_customers.apply_async",
        return_value=MagicMock(),
    )
    def test_deletion_of_partner_site(
        self, delete_partner_site_for_cw_customers
    ):
        site: FireReportPartnerSite = FireReportPartnerSiteFactory.create(
            provider=self.provider, name="ABC"
        )
        url: str = reverse(self.url, kwargs=dict(id=site.id))
        request: Request = TestClientMixin.api_factory_client_request(
            "DELETE",
            url,
            self.provider,
            self.user,
        )
        view = FireReportPartnerSitesRetrieveUpdateDestroyAPIView().as_view()
        response: Response = view(request, id=site.id)
        self.assertEqual(202, response.status_code)
        delete_partner_site_for_cw_customers.assert_called_with(
            (self.provider.id, "ABC"), queue="high"
        )


class TestFireReportSettingCreateRetriveUpdateAPIView(BaseProviderTestCase):
    """
    Test view: FireReportSettingCreateRetriveUpdateAPIView
    """

    fire_report_setting_url: str = reverse(
        "api_v1_endpoints:providers:fire-report-setting-create-retrieve-update"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_creation_of_fire_report_setting(self):
        request_body: Dict = dict(
            receiver_email_ids=["v5test@yopmail.com"],
            subject="FIRE report",
            frequency="WEEKLY",
            past_days=100,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(201, response.status_code)
        self.assertEqual(
            request_body["past_days"],
            self.provider.fire_report_setting.past_days,
        )

    def test_next_run_value(self):
        request_body: Dict = dict(
            receiver_email_ids=["v5test@yopmail.com"],
            subject="FIRE report",
            frequency="DAILY",
            past_days=100,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertIn("next_run", response.data)
        self.assertEqual(
            (datetime.now() + timedelta(days=1)).date(),
            self.provider.fire_report_setting.next_run.date(),
        )

    def test_error_creating_setting_if_already_configured(self):
        FireReportSettingFactory(provider=self.provider)
        request_body: Dict = dict(
            receiver_email_ids=["v5test@yopmail.com"],
            subject="FIRE report",
            frequency="WEEKLY",
            past_days=100,
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(400, response.status_code)

    def test_retrieval_of_fire_report_setting(self):
        setting = FireReportSettingFactory(
            provider=self.provider, past_days=10
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.fire_report_setting_url,
            self.provider,
            self.user,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(setting.past_days, response.data.get("past_days"))

    def test_error_retrieving_setting_if_not_configured(self):
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.fire_report_setting_url,
            self.provider,
            self.user,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(404, response.status_code)

    def test_fire_report_setting_update(self):
        FireReportSettingFactory(provider=self.provider, past_days=10)
        request_body: Dict = dict(
            past_days=20,
            receiver_email_ids=["v5test@yopmail.com"],
            subject="FIRE report",
            frequency="WEEKLY",
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "PUT",
            self.fire_report_setting_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportSettingCreateRetriveUpdateAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(20, self.provider.fire_report_setting.past_days)


class TestFireReportGenerationAPIView(BaseProviderTestCase):
    """
    Test view: FireReportGenerationAPIView
    """

    fire_report_generation_url: str = reverse(
        "api_v1_endpoints:providers:fire-report-generation"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_error_generating_fire_report_if_partner_site_not_configured(self):
        request_body: Dict = dict(
            start_date="2021-01-01", end_date="2023-01-01", site_crm_ids=[1, 2]
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_generation_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    def test_error_generating_fire_report_if_fire_report_setting_not_configured(
        self,
    ):
        FireReportPartnerSiteFactory(
            provider=self.provider,
            name="ABC",
            address_line_1="Dance Street",
            address_line_2="YMCK Lane",
            city="New York",
            state="Washington DC",
            state_crm_id=1,
            country="USA",
            country_crm_id=1,
            zip="12345",
            phone_number="1234567890",
        )
        request_body: Dict = dict(
            start_date=None, end_date=None, site_crm_ids=None
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_generation_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(412, response.status_code)

    @patch(
        "document.services.fire_report.FireReportGenerator._get_product_id_manufacturer_name_map",
        return_value={11: "Tyrell Corporation"},
    )
    def test_fire_report_generation(
        self, _get_product_id_manufacturer_name_map
    ):
        # Instead of mocking FireReportGenerator.get_fire_report_data() return value,
        # actually call the service method.
        FireReportPartnerSiteFactory(
            provider=self.provider,
            name="ABC",
            address_line_1="Dance Street",
            address_line_2="YMCK Lane",
            city="New York",
            state="Washington DC",
            state_crm_id=1,
            country="USA",
            country_crm_id=1,
            zip="12345",
            phone_number="1234567890",
        )
        FireReportSettingFactory(provider=self.provider)
        service_ticket: ConnectwiseServiceTicket = (
            ConnectwiseServiceTicketFactory(
                provider=self.provider,
                opportunity_crm_id=10,
                opportunity_name="Test 1",
            )
        )
        purchase_order: ConnectwisePurchaseOrder = (
            ConnectwisePurchaseOrderFactory(
                po_number="LP-1234", ticket=service_ticket
            )
        )
        sales_order: SalesPurchaseOrderData = SalesPurchaseOrderDataFactory(
            provider=self.provider,
            opportunity_crm_id=service_ticket,
            shipping_site_crm_id=2,
            customer_po_number="LP123",
            crm_id=123,
        )
        purchase_order_data: ConnectwisePurchaseOrderData = (
            ConnectwisePurchaseOrderDataFactory(
                provider=self.provider,
                po_number=purchase_order,
                purchase_order_status_id=3,
                site_name="Fremont",
                sales_order=sales_order,
                crm_id=101,
            )
        )
        line_item_1: ConnectwisePurchaseOrderLineitemsData = (
            ConnectwisePurchaseOrderLineitemsDataFactory(
                provider=self.provider,
                po_number=purchase_order,
                unit_cost=150,
                product_id="PART-123",
                description="Service product",
                serial_numbers=["SR1", "SR2"],
                tracking_numbers=["TR1", "TR2"],
                shipping_method="FedZ",
                product_crm_id=11,
                purchase_order_crm_id=101,
            )
        )
        line_item_2: ConnectwisePurchaseOrderLineitemsData = (
            ConnectwisePurchaseOrderLineitemsDataFactory(
                provider=self.provider, po_number=purchase_order, unit_cost=0
            )
        )
        request_body: Dict = dict(
            start_date=None, end_date=None, site_crm_ids=None
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "POST",
            self.fire_report_generation_url,
            self.provider,
            self.user,
            data=request_body,
        )
        view = FireReportGenerationAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)


class TestPartnerSiteSyncErrorListAPIView(BaseProviderTestCase):
    """
    Test view: PartnerSiteSyncErrorListAPIView
    """

    errors_list_url: str = reverse(
        "api_v1_endpoints:providers:partner-sites-sync-errors-list"
    )

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_listing_of_partner_sites_sync_error_listing(self):
        customer = CustomerFactory(provider=self.provider)
        PartnerSiteSyncErrorFactory.create_batch(
            2, provider=self.provider, customer=customer
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            self.errors_list_url,
            self.provider,
            self.user,
        )
        view = PartnerSiteSyncErrorListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(response.data.get("results")))

    def test_customer_crm_id_filter_on_partner_sites_sync_error_listing(self):
        PartnerSiteSyncErrorFactory.create(
            provider=self.provider, customer_crm_id=1
        )
        PartnerSiteSyncErrorFactory.create(
            provider=self.provider, customer_crm_id=2
        )
        url: str = add_query_params_to_url(
            self.errors_list_url, query_params=dict(customer_crm_id=1)
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PartnerSiteSyncErrorListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.data.get("results")))

    def test_customer_name_filter_partner_sites_sync_error_listing(self):
        customer: Customer = CustomerFactory(
            provider=self.provider, name="Apple"
        )
        PartnerSiteSyncErrorFactory.create(
            provider=self.provider, customer=customer
        )
        PartnerSiteSyncErrorFactory.create(provider=self.provider)
        url: str = add_query_params_to_url(
            self.errors_list_url, query_params=dict(customer_name="Apple")
        )
        request: Request = TestClientMixin.api_factory_client_request(
            "GET",
            url,
            self.provider,
            self.user,
        )
        view = PartnerSiteSyncErrorListAPIView().as_view()
        response: Response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.data.get("results")))
