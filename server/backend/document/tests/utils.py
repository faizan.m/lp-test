from faker import Faker
import uuid
from typing import List, Dict, Optional, Any
from core.integrations.erp.connectwise.client import Client
from core.integrations.erp.connectwise.connectwise import Connectwise
from unittest import mock
from datetime import datetime, timedelta
from document.tests.factories import (
    SalesPurchaseOrderDataFactory,
    ProductsItemDataFactory,
    ConnectwisePurchaseOrderDataFactory,
    ConnectwisePurchaseOrderLineitemsDataFactory,
    OperationEmailTemplateSettingsFactory,
    ConnectwiseServiceTicketFactory,
    ConnectwisePurchaseOrderFactory,
)
from document.models import (
    SalesPurchaseOrderData,
    ProductsItemData,
    ConnectwisePurchaseOrder,
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrderLineitemsData,
    ConnectwiseServiceTicket,
)
from accounts.models import Provider, Customer
from factory.fuzzy import FuzzyChoice

fake = Faker()


def get_sample_product_ids() -> List[str]:
    """
    Get sample product ids

    Returns:
        Product ids list
    """
    return [
        "UCSC-C220-M5SX",
        "UCS-SD19TB121X-EV",
        "CON-SNT-C220M5SX",
    ]


# Don't change the returned dictionaries in below functions
def get_sample_hardware_products_data() -> List[Dict]:
    """
    Get sample hardware products data

    Returns:
        List of hardware products
    """
    hardware_products: List[Dict] = [
        {
            "part_number": "UCSC-C220-M5SX",
            "shipped_quantity": 5,
            "purchased_quantity": 1,
            "tracking_numbers": "N.A.",
            "carriers": "GRVL",
            "ship_date": "Est. Ship Date: 01/13/2020",
            "status": "Processing",
        },
        {
            "part_number": "UCSC-PSU1-770W",
            "shipped_quantity": 10,
            "purchased_quantity": 10,
            "tracking_numbers": "N.A.",
            "carriers": "GRVL",
            "ship_date": "Est. Ship Date: 01/13/2020",
            "status": "Processing",
        },
    ]
    return hardware_products


def get_sample_table() -> List[List]:
    """
    Get a sample table

    Returns:
        Table
    """
    table: List[List] = [
        [
            "part_number",
            "shipped_quantity",
            "purchased_quantity",
            "tracking_numbers",
            "carriers",
            "ship_date",
            "status",
        ],
        [
            "UCSC-C220-M5SX",
            5,
            1,
            "N.A.",
            "GRVL",
            "Est. Ship Date: 01/13/2020",
            "Processing",
        ],
        [
            "UCSC-PSU1-770W",
            10,
            10,
            "N.A.",
            "GRVL",
            "Est. Ship Date: 01/13/2020",
            "Processing",
        ],
    ]
    return table


def create_email_template_generation_scenario(
    provider: Provider, opportunity_crm_id: int
):
    """
    Method to create a scenario for email template generation

    Args:
        provider: Provider
        opportunity_crm_id: opportunity CRM ID
    """
    service_ticket: ConnectwiseServiceTicket = (
        ConnectwiseServiceTicket.objects.get(
            provider_id=provider.id, opportunity_crm_id=opportunity_crm_id
        )
    )
    linked_sales_order = SalesPurchaseOrderDataFactory.create(
        provider=provider, opportunity_crm_id_id=service_ticket.opportunity_crm_id
    )
    # Ensure at least 1 product each of hardware product, license and contract exists
    ProductsItemDataFactory.create(
        provider=provider,
        sales_order=linked_sales_order,
        cost=fake.pyint(),
        product_id="UCSC-C220-M5SX",
    )
    ProductsItemDataFactory.create(
        provider=provider,
        sales_order=linked_sales_order,
        cost=fake.pyint(),
        product_id="UCS-SD19TB121X-EV",
    )
    purchase_order: ConnectwisePurchaseOrder = ConnectwisePurchaseOrderFactory(
        ticket=service_ticket
    )
    ConnectwisePurchaseOrderDataFactory.create(
        provider=provider,
        sales_order=linked_sales_order,
        po_number=purchase_order,
    )
    ConnectwisePurchaseOrderLineitemsDataFactory.create_batch(
        2,
        po_number=purchase_order,
        product_id=FuzzyChoice(choices=get_sample_product_ids()),
    )


def get_valid_ticket_board_and_status_mapping() -> List[Dict]:
    """
    Get board and service ticket status mapping

    Returns:
        board and service ticket status mapping
    """
    return [
        {
            "board_id": 21,
            "board_name": "Order Processing",
            "ticket_statuses": [
                {
                    "ticket_status_id": 194,
                    "ticket_status_name": "Order Confirmed",
                },
                {
                    "ticket_status_id": 197,
                    "ticket_status_name": "Order Shipped - Invoiced",
                },
            ],
        }
    ]


def get_valid_service_tickets() -> List[Dict]:
    """
    Get mock service tickets

    Returns:
        Mock Service tickets
    """
    return [
        {
            "id": 38953,
            "summary": "LP8705: WR - License Renewal C3560X   Co-Termed ",
            "record_type": "ServiceTicket",
            "board_id": 21,
            "board": "Order Processing",
            "status_id": 199,
            "status": "Closed",
            "company_id": 1641,
            "company_name": "Hanna Brophy",
            "opportunity_id": 6029,
            "opportunity_name": "WR - License Renewal C3560X   Co-Termed ",
        }
    ]


def get_valid_opportunity_statuses_mapping() -> List[Dict]:
    """
    Get opportunity status mapping

    Returns:
        Opportunity status mapping
    """
    return [
        {"status_id": 1, "status_name": "Open"},
        {"status_id": 2, "status_name": "Won"},
    ]


def get_valid_opportunity_stages_mapping() -> List[Dict]:
    """
    Get opportunity stage mapping

    Returns:
        Opportunity stage mapping
    """
    return [
        {"stage_id": 1, "stage_name": "Optional Quote"},
        {"stage_id": 10, "stage_name": "Commitment"},
    ]


def get_valid_customer_quotes() -> List[Dict]:
    """
    Get customer quote

    Returns:
        Customer quote
    """
    return [
        {
            "id": 382,
            "name": "SolarWinds Licensing",
            "type_id": 9,
            "type_name": "Hardware - Network",
            "stage_id": 10,
            "stage_name": "Commitment",
            "status_id": 2,
            "status_name": "Won",
            "company_id": 1641,
            "company_name": "Hanna Brophy",
            "created_on": "2011-08-18T04:23:56Z",
            "closed_date": "2011-08-18T22:01:36Z",
            "shipping_contact_id": 2311,
            "shipping_contact_name": "Sean Kennedy",
        }
    ]


def get_sample_template_data():
    template_data = {
        "contracts": [
            {
                "part_number": "CON-SSSNT-CSROOMK9",
                "status": "Contracts Pending",
            },
            {
                "part_number": "CON-SSSNT-CSROOMK9",
                "status": "Contracts Pending",
            },
        ],
        "hardware_products": [
            {
                "part_number": "CS-ROOM55-K9",
                "shipped_quantity": 5,
                "purchased_quantity": 5,
                "carriers": "Electronic",
                "ship_date": "07/15/2022",
                "tracking_numbers": "33112",
                "status": "07/15/2022",
            },
            {
                "part_number": "CS-ROOM55-K9",
                "shipped_quantity": 5,
                "purchased_quantity": 5,
                "carriers": "Electronic",
                "ship_date": "08/09/2022",
                "tracking_numbers": "10417883390",
                "status": "07/15/2022",
            },
        ],
        "licences": [
            {
                "part_number": "UCS-SD19TB121X-EV",
                "shipped_quantity": 2,
                "purchased_quantity": 2,
                "carriers": "VIRTUAL",
                "ship_date": "Est. Ship Date: 01/13/2020",
                "tracking_numbers": "N.A.",
                "status": "07/15/2022",
            },
            {
                "part_number": "UCS-SD960G121X-EV",
                "shipped_quantity": 8,
                "purchased_quantity": 4,
                "carriers": "VIRTUAL",
                "ship_date": "Est. Ship Date: 01/14/2020",
                "tracking_numbers": "N.A.",
                "status": "07/15/2022",
            },
        ],
        "name": "Jun",
        "cc_contacts": ["user@email.com"],
        "to_contacts_emails": ["placidov@cadence.com"],
        "subject_opportunity_name": "Cadence Room 55 Project",
        "customer_po": "490125981",
        "shipping_details": {
            "addressline1": "2655 Seely Avenue",
            "addressline2": "",
            "city": "San Jose",
            "state": "California",
            "postalcode": "95134",
            "country": "United States",
        },
    }
    return template_data


def get_sample_csv_tables() -> List[Dict]:
    return [
        {
            "file_path": "www.file_path.com/file",
            "file_name": "sample file name.csv",
        }
    ]


def add_query_params_to_url(url: str, query_params: Dict):
    """
    Add query params to an URL

    Args:
        url: URL
        query_params: query params

    Returns:
        URL with query params
    """
    url: str = url + f"?"
    for key, value in query_params.items():
        url += f"{key}={value}&"
    return url


def get_sow_document_categories() -> List[str]:
    """
    Get SOW document categories

    Returns:
        List of SOW document categories
    """
    return [
        "Collaboration",
        "Data Center",
        "Networking",
        "Security",
        "Change Request",
    ]


def get_mock_company_quotes(
    sow_documents,
) -> List[Dict]:
    # Return quotes with quote ids same as that of sow documents
    return [
        dict(
            id=quote_id,
            status_id=fake.pyint(),
            status_name=fake.name(),
            stage_id=fake.pyint(),
            stage_name=fake.name(),
        )
        for quote_id in list(sow_documents.values_list("quote_id", flat=True))
    ]


def get_random_company_quotes(quantity: int = 5) -> List[Dict]:
    return [
        dict(
            id=fake.pyint(),
            status_id=fake.pyint(),
            status_name=fake.name(),
            stage_id=fake.pyint(),
            stage_name=fake.name(),
        )
        for num in range(quantity)
    ]


def mock_filter_documents_by_doc_status(provider, queryset, doc_status_name):
    """
    Mock the behaviour of filter_documents_by_doc_status().
    Method signature is same as that of original method.
    Slices the queryset which mimicks the behaviour of above method.

    Args:
        provider: Provider
        queryset: Queryset
        doc_status_name: Document quote status name

    Returns:
        Returns queryset with one object
    """
    return queryset[:1]


def get_mock_service_cost_data_for_sow_document() -> Dict:
    """
    Get mock service cost data

    Returns:
        Service cost data
    """
    return {
        "travels": [
            {"cost": fake.pyint(), "description": fake.text(max_nb_chars=10)},
            {"cost": fake.pyint(), "description": fake.text(max_nb_chars=10)},
        ],
        "after_hours": fake.pyint(),
        "contractors": [
            {
                "name": fake.name(),
                "partner_cost": fake.pyint(),
                "customer_cost": fake.pyint(),
                "margin_percentage": fake.pyint(),
            },
            {
                "name": fake.name(),
                "partner_cost": fake.pyint(),
                "customer_cost": fake.pyint(),
                "margin_percentage": fake.pyint(),
            },
        ],
        "customer_cost": str(fake.pyint()),
        "internal_cost": str(fake.pyint()),
        "static_fields": False,
        "after_hours_rate": fake.pyint(),
        "engineering_hours": fake.pyint(),
        "zero_dollar_change": fake.pybool(),
        "customer_cost_fixed_fee": str(fake.pyint()),
        "engineering_hourly_rate": fake.pyint(),
        "internal_cost_fixed_fee": str(fake.pyint()),
        "project_management_hours": fake.pyint(),
        "customer_cost_t_and_m_fee": str(fake.pyint()),
        "internal_cost_t_and_m_fee": str(fake.pyint()),
        "project_management_hourly_rate": fake.pyint(),
    }


def get_mock_json_config_for_sow_document() -> Dict:
    """
    Get mock json config data for sow document

    Returns:
        Json config data
    """
    return dict(service_cost=get_mock_service_cost_data_for_sow_document())
