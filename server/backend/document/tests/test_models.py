from document.models import AutomatedWeeklyRecapSetting
from document.tests.factories import (
    AutomatedWeeklyRecapSettingFactory,
)
from test_utils import BaseProviderTestCase
from unittest.mock import patch
from datetime import datetime


class TestAutomatedWeeklyRecapModel(BaseProviderTestCase):
    """
    Test model: AutomatedWeeklyRecapSetting
    """

    @classmethod
    def setUpTestData(cls):
        cls.setup_provider_and_user()

    def test_is_configured_for_provider(self):
        # Test when setting not configured for provider
        self.assertEqual(
            False,
            AutomatedWeeklyRecapSetting.is_configured_for_provider(
                self.provider
            ),
        )
        # Test when setting is configured for provider
        AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, is_enabled=True
        )
        self.assertEqual(
            True,
            AutomatedWeeklyRecapSetting.is_configured_for_provider(
                self.provider
            ),
        )

    @patch("document.models.timezone.now")
    def test_is_scheduled_for_today(self, timezone_now):
        # Test when schedule for today
        # Select a datetime that falls on Monday
        timezone_now.return_value = datetime(day=12, month=6, year=2023)
        setting = AutomatedWeeklyRecapSettingFactory(
            provider=self.provider, weekly_schedule=["Monday", "Tuesday"]
        )
        self.assertEqual(True, setting.is_scheduled_for_today())
        # Test when not scheduled for today
        timezone_now.return_value = datetime(day=16, month=6, year=2023)
        self.assertEqual(False, setting.is_scheduled_for_today())

    def test_get_weekly_recap_email_template_directory_path(self):
        path: str = (
            AutomatedWeeklyRecapSetting.get_weekly_recap_email_template_directory_path()
        )
        self.assertTrue(path.endswith("document/templates/weekly_recap"))

    def test_get_weekly_recap_email_body_template_path(self):
        path: str = (
            AutomatedWeeklyRecapSetting.get_weekly_recap_email_body_template_path()
        )
        self.assertTrue(
            path.endswith(
                "document/templates/weekly_recap/weekly_recap_email_body.html"
            )
        )
