import os
from datetime import datetime, timedelta
from typing import List, Dict, Union, Any

from rest_framework.exceptions import NotFound, ValidationError

from accounts.models import Customer, Provider, User
from caching.service import CacheService, caching
from core.exceptions import InvalidConfigurations
from document.exceptions import (
    SOWQuoteStatusSettingsNotConfiguredAPIException,
    SOWQuoteStatusSettingsNotConfigured,
)
from document.models import SOWDocument
from exceptions.exceptions import APIError
from sales.models import Agreement
from sales.services import pax8_calculations, product_service
from utils import get_app_logger
from .sow_calculations import ServiceCostCalculation

# Get an instance of a logger

logger = get_app_logger(__name__)


class ForecastItemDescriptionSuffixes:
    PAX8_PRODUCT_FORECAST_DESCRIPTION_SUFFIX: str = "PAX8 product"
    PRODUCT_FORECAST_DESCRIPTION_SUFFIX: str = "Product"


def group_pax8_products_by_billing_term(
    computed_pax8_products: "List[PAX8ProductCalculator]",
) -> Dict[str, "List[PAX8ProductCalculator]"]:
    """
    Group computed pax8 products by billing term.

    Args:
        computed_pax8_products: List of PAX8ProductCalculator instances

    Returns:
        Computed pax8 products grouped by billing term.
    """
    pax8_products_grouped_by_billing_term: Dict[
        str, List[PAX8ProductCalculator]
    ] = dict()

    for product in computed_pax8_products:
        if product.billing_term in set(
            pax8_products_grouped_by_billing_term.keys()
        ):
            pax8_products_grouped_by_billing_term[product.billing_term].append(
                product
            )
        else:
            pax8_products_grouped_by_billing_term[product.billing_term] = [
                product
            ]

    return pax8_products_grouped_by_billing_term


def group_pax8_products_by_vendor(
    computed_pax8_proudcts: "List[PAX8ProductCalculator]",
) -> Dict[str, "List[PAX8ProductCalculator]"]:
    """
    Group computed PAX8 products by vendor.

    Args:
        computed_pax8_proudcts: List of PAX8ProductCalculator instances

    Returns:
        Computed PAX8 product grouped by vendor.
    """
    products_grouped_by_vendor: Dict[
        str, "List[PAX8ProductCalculator]"
    ] = dict()

    for product in computed_pax8_proudcts:
        if product.vendor_name in set(products_grouped_by_vendor.keys()):
            products_grouped_by_vendor[product.vendor_name].append(product)
        else:
            products_grouped_by_vendor[product.vendor_name] = [product]
    return products_grouped_by_vendor


def get_vendor_pax8_product_details_by_billing_term(
    pax8_products_grouped_by_billing_term: Dict[
        str, "List[PAX8ProductCalculator]"
    ]
) -> List[Dict]:
    """
    Get vendor PAX8 product details by billing term.
    E.g. Terms like Monthly Microsoft, Annual AMD, etc.

    Args:
        pax8_products_grouped_by_billing_term: PAX8ProductCalculator instances grouped by billing term

    Returns:
        Vendor PAX8 product details by billing term.
    """
    pax8_data: List[Dict] = list()

    for (
        billing_term,
        computed_products,
    ) in pax8_products_grouped_by_billing_term.items():
        pax8_products_grouped_by_vendor: Dict[
            str, List["PAX8ProductCalculator"]
        ] = group_pax8_products_by_vendor(computed_products)

        for (
            vendor_name,
            products,
        ) in pax8_products_grouped_by_vendor.items():
            # PAX8 products forecast is created on annual basis
            extended_cost: float = round(
                sum([product.annual_extended_cost for product in products]), 2
            )
            quantity: int = sum([product.quantity for product in products])
            internal_cost: float = round(
                sum([product.annual_internal_cost for product in products]), 2
            )
            pax8_data.append(
                dict(
                    billing_term=billing_term,
                    vendor_name=vendor_name,
                    extended_cost=extended_cost,
                    quantity=quantity,
                    internal_cost=internal_cost,
                )
            )
    return pax8_data


def prepare_forecast_payload_for_pax8_products(
    pax8_products: List[Dict],
) -> List[Dict]:
    """
    Prepare forecast payload for pax8 products.

    Args:
        pax8_products: List of PAX8 product details

    Returns:
        Forecast payload for PAX8 products.
    """
    forecast_payload: List[Dict] = list()
    for product in pax8_products:
        description: str = (
            f"{product.get('billing_term')} {product.get('vendor_name')}"
        )
        forecast_payload.append(
            dict(
                cost=product.get("internal_cost"),
                revenue=product.get("extended_cost"),
                description=description,
                type="Product",
                quantity=product.get("quantity"),
            )
        )
    return forecast_payload


def generate_forecast_payload_for_pax8_products(
    computed_pax8_products: "List[PAX8ProductCalculator]",
) -> List[Dict]:
    """
    Generate PAX8 product forecast payload

    Args:
        computed_pax8_products: List of PAX8ProductCalculator instances

    Returns:
        Forecast payload
    """
    pax8_products_grouped_by_billing_term: Dict[
        str, List[PAX8ProductCalculator]
    ] = group_pax8_products_by_billing_term(computed_pax8_products)
    pax8_products: List[
        Dict
    ] = get_vendor_pax8_product_details_by_billing_term(
        pax8_products_grouped_by_billing_term
    )
    forecast_payload: List[Dict] = prepare_forecast_payload_for_pax8_products(
        pax8_products
    )
    return forecast_payload


class QuoteService:
    MAX_FORECAST_ITEM_DESCRIPTION_LENGTH: int = 50

    @classmethod
    def get_quote_types(cls, provider_id):
        provider = Provider.objects.get(id=provider_id)
        return provider.erp_client.get_opportunity_types()

    @classmethod
    def get_quote_business_units(cls, provider):
        return provider.erp_client.get_system_departments()

    @classmethod
    def get_quote_statuses(cls, provider):
        return provider.erp_client.get_opportunity_statuses()

    @classmethod
    def get_quote_stages(cls, provider_id):
        provider = Provider.objects.get(id=provider_id)
        quotes_config = cls.get_provider_quotes_config(provider)
        stages = quotes_config.get("stages")
        return provider.erp_client.get_opportunity_stages(stages)

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.OPPORTUNITY_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
        invalidate=True,
    )
    def create_quote(cls, provider_id, data):
        provider = Provider.objects.get(id=provider_id)
        customer_id = data.pop("customer_id")
        user_id = data.get("user_id")
        quotes_config = cls.get_provider_sow_quotes_config(provider)
        open_status = quotes_config.get("open_status")
        try:
            if not open_status and not isinstance(open_status, list):
                raise InvalidConfigurations("Orders Open states not set.")
            open_status = open_status[0]
            customer = Customer.objects.get(id=customer_id)
            user = customer.users.get(id=user_id)
            territory_manager_id = QuoteService.get_customer_territory_manager(
                provider, customer_id
            )
            close_date = datetime.strftime(
                datetime.today() + timedelta(days=30), "%Y-%m-%dT%H:%M:%SZ"
            )
            config = cls.get_provider_sow_quotes_config(provider)
            business_unit_id = config.get("business_unit")
            if business_unit_id is None:
                raise InvalidConfigurations(
                    "SOW Opportunity Mapping not completed."
                )
            data.update(
                {
                    "user_id": user.crm_id,
                    "territory_manager_id": territory_manager_id,
                    "close_date": close_date,
                    "business_unit_id": business_unit_id[0],
                }
            )
            return provider.erp_client.create_company_quote(
                customer.crm_id, open_status, data
            )
        except Customer.DoesNotExist:
            raise NotFound("Customer does not exist")
        except User.DoesNotExist:
            raise NotFound("User does not exist for the specified Customer")

    @classmethod
    def get_quote(cls, provider_id, quote_id, customer_id=None):
        provider = Provider.objects.get(id=provider_id)
        return provider.erp_client.get_company_quote(quote_id, customer_id)

    @classmethod
    def update_quote(cls, provider_id, customer_id, quote_id, data):
        provider = Provider.objects.get(id=provider_id)
        return provider.erp_client.update_company_quote(
            customer_id, quote_id, data
        )

    @classmethod
    def update_quote_stage(cls, provider, quote_id, data):
        try:
            return provider.erp_client.patch_quote(quote_id, data)
        except APIError as e:
            if e.status_code == 400:
                raise ValidationError(
                    {
                        "detail": "This opportunity is closed and cannot be edited."
                    }
                )
            else:
                logger.error(
                    f"Update quote stage error: {quote_id} \n Error: {e.errors}"
                )

    @classmethod
    def create_forecast_item(
        cls, provider: Provider, quote_id: int, data: Dict
    ):
        config = cls.get_provider_sow_quotes_config(provider)
        open_status = config.get("open_status")[0]
        try:
            return provider.erp_client.create_opportunity_forecast_item(
                quote_id, data, open_status
            )
        except APIError as e:
            raise ValidationError({"quote_id": e.detail})

    @classmethod
    def _create_main_forecast(
        cls, provider: Provider, quote_id: int, data: Dict[str, Any]
    ) -> Dict:
        params: Dict = dict(provider=provider)
        payload: Dict[str, int] = cls.get_cost_and_revenue_from_service_cost(
            data.get("doc_type"),
            data.get("json_config", {}).get("service_cost", {}),
            **params,
        )
        return cls.create_forecast_item(provider, quote_id, payload)

    @classmethod
    def get_cost_and_revenue_from_service_cost(
        cls, sow_doc_type: str, service_cost: Dict[str, Any], **kwargs
    ) -> Dict[str, int]:
        """
        Get cost and revenue from service cost data.

        Args:
            sow_doc_type: SoW Document type
            service_cost: Service cost data
            **kwargs: Keyword arguments

        Returns:
            Cost and revenue
        """
        provider: Provider = kwargs.get("provider")
        sow_doc_setting: SOWDocumentSettings = provider.sow_doc_settings
        calculation_payload: Dict = dict(
            sow_doc_type=sow_doc_type,
            sow_doc_setting=sow_doc_setting,
            engineering_hours=service_cost.get("engineering_hours"),
            engineering_hourly_rate=service_cost.get(
                "engineering_hourly_rate"
            ),
            after_hours=service_cost.get("after_hours"),
            after_hours_rate=service_cost.get("after_hours_rate"),
            project_management_hours=service_cost.get(
                "project_management_hours"
            ),
            project_management_hourly_rate=service_cost.get(
                "project_management_hourly_rate"
            ),
            integration_technician_hours=service_cost.get(
                "integration_technician_hours"
            ),
            integration_technician_hourly_rate=service_cost.get(
                "integration_technician_hourly_rate"
            ),
            hourly_resources=service_cost.get("hourly_resources"),
        )
        # Contractors exist for FixedFee and CR documents.
        # But a seperate forecast is created for contractors.
        # Pass contractors = [] to exlcude them from main forecast.
        # The main forecast & contractor forecast add up to document forecast.
        if sow_doc_type in [SOWDocument.GENERAL, SOWDocument.CHANGE_REQUEST]:
            calculation_payload.update(
                contractors=[],
                travels=service_cost.get("travels"),
            )
        else:
            calculation_payload.update(
                contractors=[],
                travels=[],
            )
        service_cost_calculator: ServiceCostCalculation = (
            ServiceCostCalculation(**calculation_payload)
        )
        revenue: int = service_cost_calculator.calculated_values.get(
            "total_customer_cost"
        )
        cost: int = service_cost_calculator.calculated_values.get(
            "total_internal_cost"
        )
        return dict(cost=cost, revenue=revenue)

    @classmethod
    def _create_contractor_forecast_items(
        cls, provider: Provider, quote_id: int, contractors: List[Dict]
    ):
        for contractor in contractors:
            payload = cls._get_contractor_forecast_payload(contractor)
            if payload.get("revenue", 0) > 0 or payload.get("cost", 0) > 0:
                cls.create_forecast_item(provider, quote_id, payload)

    @classmethod
    def _create_forecast(
        cls, provider: Provider, quote_id: int, data: Dict
    ) -> Dict:
        forecast: Dict = cls._create_main_forecast(provider, quote_id, data)
        contractors: List[Dict] = (
            data.get("json_config", {})
            .get("service_cost", {})
            .get("contractors")
        )
        # Fee based contractors exist in FixedFee & CR docs
        if data.get("doc_type") in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            cls._create_contractor_forecast_items(
                provider, quote_id, contractors
            )
        return forecast

    @classmethod
    def create_or_update_forecast(
        cls, provider_id: int, quote_id: int, data: Dict
    ):

        provider: Provider = Provider.objects.get(id=provider_id)
        # delete existing forecasts
        cls.delete_existing_forecasts(provider, quote_id)
        # create new forecasts
        forecast: Dict = cls._create_forecast(provider, quote_id, data)
        return forecast

    @classmethod
    def get_forecast(cls, provider_id, quote_id, forecast_id):
        provider = Provider.objects.get(id=provider_id)
        return provider.erp_client.get_opportunity_forecast(
            quote_id, forecast_id
        )

    @classmethod
    def create_change_request(cls, provider_id, data):
        provider = Provider.objects.get(id=provider_id)
        customer_id = data.pop("customer_id")
        user_id = data.get("user_id")
        change_request_config = cls.get_provider_change_request_config(
            provider
        )
        config = cls.get_provider_sow_quotes_config(provider)
        open_status = config.get("open_status")[0]
        won_status = change_request_config.get("won_status")[0]
        won_stage = change_request_config.get("won_stage")[0]
        business_unit_id = config.get("business_unit")
        if business_unit_id is None:
            raise InvalidConfigurations(
                "SOW Opportunity Mapping not completed."
            )
        try:
            customer = Customer.objects.get(id=customer_id)
            user = customer.users.get(id=user_id)
            territory_manager_id = QuoteService.get_customer_territory_manager(
                provider, customer_id
            )
            close_date = datetime.strftime(
                datetime.today(), "%Y-%m-%dT%H:%M:%SZ"
            )
            data.update(
                {
                    "user_id": user.crm_id,
                    "territory_manager_id": territory_manager_id,
                    "close_date": close_date,
                    "stage_id": won_stage,
                    "business_unit_id": business_unit_id[0],
                }
            )
            opportunity = provider.erp_client.create_company_quote(
                customer.crm_id, open_status, data
            )
            QuoteService.create_forecast_item(
                provider, opportunity.get("id"), data
            )
            opportunity.update(data)
            opportunity.update({"status_id": won_status})
            QuoteService.update_quote(
                provider.id,
                customer.crm_id,
                opportunity.get("id"),
                opportunity,
            )
            return opportunity

        except Customer.DoesNotExist:
            raise NotFound("Customer does not exist")
        except User.DoesNotExist:
            raise NotFound("User does not exist for the specified Customer")

    @classmethod
    def get_customer_territory_manager(cls, provider, customer_id):
        customer = Customer.objects.get(id=customer_id)
        cw_customer = provider.erp_client.get_customer(customer.crm_id)
        if cw_customer.get("territory_manager_id") is None:
            raise ValidationError(
                {
                    "customer_id": "Territory Manager not assigned for the selected Customer"
                }
            )
        territory_manager_id = cw_customer.get("territory_manager_id")
        return territory_manager_id

    @classmethod
    def get_forecast_from_template(cls, data, template_type, **kwargs):
        try:
            service_cost = data.get("json_config", {}).get("service_cost", {})
            if template_type == SOWDocument.CHANGE_REQUEST:
                revenue = float(service_cost.get("customer_cost", {}))
                cost = float(service_cost.get("internal_cost", {}))
            elif template_type.lower() == SOWDocument.T_AND_M.lower():
                revenue = float(
                    service_cost.get("customer_cost_t_and_m_fee", {})
                )
                cost = float(service_cost.get("internal_cost_t_and_m_fee", {}))
            else:
                revenue = float(
                    service_cost.get("customer_cost_fixed_fee", {})
                )
                cost = float(service_cost.get("internal_cost_fixed_fee", {}))
                sow_doc_settings = Provider.objects.get(
                    id=kwargs.get("provider_id")
                ).sow_doc_settings
                from document.services.document_service import DocumentService

                internal_cost_risk = DocumentService.get_internal_cost_risk(
                    service_cost, sow_doc_settings
                )
                cost += internal_cost_risk
            contractors = service_cost.get("contractors", [])
            contractor_partner_cost = 0
            contractor_customer_cost = 0
            for contractor in contractors:
                contractor_partner_cost += contractor.get("partner_cost", 0)
                contractor_customer_cost += contractor.get("customer_cost", 0)
            # Remove contractor costs
            cost -= contractor_partner_cost
            revenue -= contractor_customer_cost
        except ValueError:
            raise ValidationError({"json_config": "Cost should be numeric."})
        if data.get("is_zero_dollar_change_request", False):
            revenue, cost = 0, 0
        _data = {"revenue": round(revenue), "cost": round(cost)}
        return _data

    @classmethod
    def _get_contractor_forecast_payload(cls, contractor_service_cost_payload):
        revenue = contractor_service_cost_payload.get("customer_cost", 0)
        cost = contractor_service_cost_payload.get("partner_cost", 0)
        name = f"{contractor_service_cost_payload.get('name')} Costs"
        # By default, the type should be Service for contractors
        type = (
            "Product"
            if contractor_service_cost_payload.get("type", "").lower()
            == "product"
            else "Service"
        )
        payload = dict(revenue=revenue, cost=cost, name=name, type=type)
        return payload

    @classmethod
    def get_provider_quotes_config(cls, provider):
        if not provider.integration_statuses["crm_board_mapping_configured"]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        quotes_config = provider.erp_integration.other_config.get(
            "board_mapping"
        ).get("Opportunities")
        if not quotes_config:
            raise InvalidConfigurations(
                "Opportunities field mapping not completed."
            )
        return quotes_config

    @classmethod
    def get_provider_sow_quotes_config(cls, provider):
        if not provider.integration_statuses["crm_board_mapping_configured"]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        quotes_config = provider.erp_integration.other_config.get(
            "board_mapping"
        ).get("SOW Opportunity Mapping")
        if not quotes_config:
            raise InvalidConfigurations(
                "SOW Opportunity Mapping not completed."
            )
        return quotes_config

    @classmethod
    def get_provider_change_request_config(cls, provider):
        if not provider.integration_statuses["crm_board_mapping_configured"]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        change_request_config = provider.erp_integration.other_config.get(
            "board_mapping"
        ).get("Change Request Mapping")
        if change_request_config:
            won_status = change_request_config.get("won_status")
            won_stage = change_request_config.get("won_stage")
            if all([change_request_config, won_status, won_stage]):
                return change_request_config
        raise InvalidConfigurations("Change Request Mapping not completed.")

    @classmethod
    def get_open_orders_count(cls, provider, customer_id):
        order_config = cls.get_provider_order_config(provider)
        open_status = order_config.get("open_status")
        if not open_status:
            raise InvalidConfigurations("Orders Open states not set.")
        open_orders_count = provider.erp_client.get_orders_count(
            open_status, customer_id
        )
        return open_orders_count

    @classmethod
    def get_closed_orders_count(cls, provider, customer_id):
        order_config = cls.get_provider_order_config(provider)
        closed_status = order_config.get("closed_status")
        if not closed_status:
            raise InvalidConfigurations("Orders Closed states not set.")
        closed_orders_count = provider.erp_client.get_orders_count(
            closed_status, customer_id
        )
        return closed_orders_count

    @classmethod
    def get_provider_order_config(cls, provider):
        if not provider.integration_statuses["crm_board_mapping_configured"]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        order_config = provider.erp_integration.other_config.get(
            "board_mapping"
        ).get("Orders")
        if not order_config:
            raise InvalidConfigurations("Order field mapping not completed.")
        return order_config

    @classmethod
    def get_open_quotes_count(cls, provider, customer_id):
        quotes_config = cls.get_provider_quotes_config(provider)
        open_statuses = quotes_config.get("status")
        stages = quotes_config.get("stages")
        return provider.erp_client.get_company_quotes_count(
            customer_id, open_statuses, stages
        )

    @classmethod
    def get_orders_dashboard(cls, provider, customer_id=None):
        cache_key = CacheService.CACHE_KEYS.ORDERS_DASHBOARD.format(
            provider.id, customer_id
        )
        if CacheService.exists(cache_key) and isinstance(customer_id, int):
            return CacheService.get_data(cache_key)
        dashboard = {
            "open": cls.get_open_orders_count(provider, customer_id),
            "closed": cls.get_closed_orders_count(provider, customer_id),
        }
        if customer_id:
            dashboard.update(
                {
                    "open_quotes": cls.get_open_quotes_count(
                        provider, customer_id
                    )
                }
            )

        if isinstance(customer_id, int):
            CacheService.set_data(cache_key, dashboard)
        return dashboard

    @classmethod
    def get_quotes(cls, provider_id, customer_id, open_only=None, **kwargs):
        provider = Provider.objects.get(id=provider_id)
        customer = Customer.objects.get(id=customer_id)
        won_only = kwargs.get("won_only", False)
        params = dict()
        if open_only == "true":
            config = cls.get_provider_sow_quotes_config(provider)
            open_status = config.get("open_status")
            business_unit_id = config.get("business_unit")
            if business_unit_id is None:
                raise InvalidConfigurations(
                    "SOW Opportunity Mapping not completed."
                )
            stages = []
            if open_status:
                open_statuses = list(open_status)
            else:
                raise InvalidConfigurations(
                    "Field mappings for Opportunity Board not Configured Properly."
                )
        elif won_only:
            try:
                won_status_ids = provider.erp_integration.won_status_ids
            except SOWQuoteStatusSettingsNotConfigured:
                raise SOWQuoteStatusSettingsNotConfiguredAPIException()
            open_statuses = won_status_ids
            business_unit_id = None
            stages = []
            params.update(dict(won_only=won_only))
        else:
            config = cls.get_provider_quotes_config(provider)
            open_statuses = config.get("status")
            stages = config.get("stages")
            business_unit_id = None
        return provider.erp_client.get_company_quotes(
            customer.crm_id, open_statuses, stages, business_unit_id, **params
        )

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.OPPORTUNITY_LIST,
        cache_type=CacheService.CACHE_TYPE.CUSTOMER,
    )
    def get_quote_document(cls, provider_id, customer_id, quote_id):
        provider = Provider.objects.get(id=provider_id)
        quote_documents = provider.erp_client.get_quote_documents(quote_id)
        pdf_documents = []
        for quote_document in quote_documents:
            server_file_name = quote_document.get("server_file_name", "")
            file_name, file_extension = os.path.splitext(server_file_name)
            from document.services import DocumentService

            if (
                ".pdf" == file_extension
                and DocumentService.SERVICE_DETAIL not in file_name
            ):
                pdf_documents.append(quote_document)
        if pdf_documents:
            latest_doc = None
            latest_doc_date = None
            for _document in pdf_documents:
                _date = datetime.strptime(
                    _document.get("last_updated_on"), "%Y-%m-%dT%H:%M:%SZ"
                )
                if latest_doc_date is None:
                    latest_doc_date = _date
                    latest_doc = _document
                if _date > latest_doc_date:
                    latest_doc = _document
                    latest_doc_date = _date
            return latest_doc
        return pdf_documents

    @classmethod
    def delete_existing_forecasts(cls, provider: Provider, quote_id: int):
        """
        Delete CW forecasts of given opportunity.

        Args:
            provider: Provider
            quote_id: Quote ID
        """
        forecasts: List[
            Dict
        ] = provider.erp_client.get_opportunity_forecast_items(quote_id)
        existing_forecast_ids: List[int] = [
            forecast["id"] for forecast in forecasts
        ]
        for forecast_id in existing_forecast_ids:
            provider.erp_client.delete_forecast(quote_id, forecast_id)

    @classmethod
    def _get_valid_forecast_item_description(
        cls, description: str, suffix_text: str, substitute_value: str
    ) -> str:
        """
        Get valid forecast item description. If the legth is less than CW max. description length, description is used,
        else custom description using suffix_text and substitute value.

        Args:
            description: Forecast item description
            suffix_text: Suffix text
            substitute_value: Substitute value

        Returns:
            Forecast item description.
        """
        if len(description) < cls.MAX_FORECAST_ITEM_DESCRIPTION_LENGTH:
            return description
        else:
            custom_description: str = f"{suffix_text}: {substitute_value}"
            if (
                len(custom_description)
                < cls.MAX_FORECAST_ITEM_DESCRIPTION_LENGTH
            ):
                return custom_description
            else:
                return suffix_text

    @classmethod
    def _get_foreacast_payload_for_pax8_products(
        cls, pax8_products: List[Dict]
    ) -> List[Dict]:
        """
        Get forecast payload for PAX8 products.

        Args:
            pax8_products: Agreement PAX8 products

        Returns:
            Forecast paylod
        """
        computed_pax8_products: List[PAX8ProductCalculator] = list()
        for product in pax8_products:
            if product.get("is_comment"):
                continue
            pax8_product: PAX8ProductCalculator = (
                pax8_calculations.PAX8ProductCalculator(
                    billing_term=product.get("billing_term"),
                    margin_percent=product.get("margin"),
                    partner_buy_rate=product.get("partner_buy_rate"),
                    product_name=product.get("product_name"),
                    quantity=product.get("quantity"),
                    sku=product.get("sku"),
                    vendor_name=product.get("vendor_name"),
                    short_description=product.get("short_description"),
                    commitment_term=product.get("commitment_term"),
                )
            )
            computed_pax8_products.append(pax8_product)

        pax8_products_forecast_payload: List[
            Dict
        ] = generate_forecast_payload_for_pax8_products(computed_pax8_products)
        return pax8_products_forecast_payload

    @classmethod
    def _get_forecast_payload_for_products(
        cls, products: List[Dict], discount_percent: int
    ) -> List[Dict]:
        """
        Get forecast payload for agreement products.

        Args:
            products: Agreement products
            discount_percent: Discount percent

        Returns:
            Forecast payload
        """
        forecast_payload: List[Dict] = list()

        for product in products:
            selected_product_index: int = product.get("selected_product")
            product_type: str = product.get("product_type")
            if (
                selected_product_index is None
                or product_type == Agreement.COMMENT
            ):
                continue
            products_list: List[Dict] = product.get("products")
            selected_product: Dict = products_list[selected_product_index]
            # Product dataclass handles calculations for individual product
            computed_product: Product = product_service.Product(
                name=selected_product.get("name"),
                description=selected_product.get("description"),
                original_internal_cost_per_unit=round(
                    selected_product.get("internal_cost")
                ),
                original_customer_cost_per_unit=round(
                    selected_product.get("customer_cost")
                ),
                margin_percent=round(selected_product.get("margin")),
                quantity=selected_product.get("quantity"),
                billing_option=product.get("billing_option"),
                discount_percentage=discount_percent,
            )
            description: str = cls._get_valid_forecast_item_description(
                selected_product.get("description"),
                ForecastItemDescriptionSuffixes.PRODUCT_FORECAST_DESCRIPTION_SUFFIX,
                selected_product.get("name"),
            )
            forecast_payload.append(
                dict(
                    cost=round(
                        computed_product.total_internal_cost_for_all_units
                    ),
                    revenue=round(computed_product.extended_cost),
                    description=description,
                    type="Product",
                    quantity=selected_product.get("quantity"),
                )
            )
        return forecast_payload

    @classmethod
    def create_forecast_for_agreement_products(
        cls,
        provider: Provider,
        quote_id: int,
        pax8_products: List[Dict],
        products: List[Dict],
        **kwargs,
    ):
        """
        Create forecast for agreement products.

        Args:
            provider: Provider
            quote_id: Quote ID
            pax8_products: PAX8 products
            products: Products
            kwargs: Keyword arguments

        Returns:
            Created forecast data.
        """
        cls.delete_existing_forecasts(provider, quote_id)

        forecast_items: List[Dict] = list()
        if pax8_products:
            # The forecast for PAX8 product is calculated on annual basis for each vendor
            pax8_products_forecast_payload: List[
                Dict
            ] = cls._get_foreacast_payload_for_pax8_products(pax8_products)
            forecast_items.extend(pax8_products_forecast_payload)
        if products:
            products_forecast_payload: List[
                Dict
            ] = cls._get_forecast_payload_for_products(
                products, kwargs.get("discount_percentage", 0)
            )
            forecast_items.extend(products_forecast_payload)

        config: Dict = cls.get_provider_sow_quotes_config(provider)
        open_status_id: int = config.get("open_status")[0]
        try:
            return provider.erp_client.create_opportunity_forecast_items(
                quote_id, forecast_items, open_status_id
            )
        except APIError as err:
            raise ValidationError(
                f"Error creating forecast for quote_id: {quote_id}. Error: {err}"
            )

    @classmethod
    def partially_update_quote(
        cls, provider: Provider, quote_id: int, data: Dict
    ) -> Dict:
        """
        Update quote.

        Args:
            provider: Provider
            quote_id: Quote CRM ID
            data: Update payload

        Returns:
            Updated quote
        """
        updated_quote: Dict = provider.erp_client.patch_quote(quote_id, data)
        return updated_quote
