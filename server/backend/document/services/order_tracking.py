from copy import deepcopy
from itertools import groupby
from typing import List, Dict, Optional, Union, Set, Tuple
from rest_framework.exceptions import ValidationError

from accounts.models import Provider, Customer
from core.integrations import Connectwise
from core.integrations.erp.connectwise.connectwise import (
    CWFieldsFilter,
    partial_received_status,
    FULLY_RECEIVED,
    WAITING,
    PARTIALLY_RECEIVED_CANCEL_REST,
    PARTIALLY_RECEIVED_CLONE_REST,
)
from core.tasks.tasks import create_devices_task, create_opportunity_task
from core.workflow.smartnet_receiving.services import (
    SmartnetReceivingDataValidationService,
)
from core.workflow.smartnet_receiving.services import SmartnetReceivingService
from document.services.email_generation import EmailTemplateGenerationService
from service_requests.mixins import ServiceRequestModelMixin
from service_requests.services import ServiceRequestService
from document.models import (
    AutomatedWeeklyRecapSetting,
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrderLineitemsData,
    ConnectwisePurchaseOrder,
)
from django.db.models import (
    Q,
    QuerySet,
    Subquery,
    OuterRef,
    Exists,
    Max,
    Case,
    When,
    Value,
    DateTimeField,
    F,
)
from celery.utils.log import get_task_logger
from document.emails import WeeklyRecapEmail
from utils import convert_utc_datetime_to_provider_timezone
from django.template.loader import render_to_string

task_logger = get_task_logger(__name__)
from django.utils import timezone


class OrderTrackingReceivingService:
    """
    Ability to update the products
    on the purchase order with the information gathered from the Ingram API.
    This will allow the user to push information like serial number, ship date,
    tracking number, etc. to the purchase order items.
    """

    @classmethod
    def get_purchase_order_line_items(
        cls, provider, purchase_order_ids, **kwargs
    ):
        line_items = SmartnetReceivingDataValidationService.get_purchase_order_line_items(
            provider.erp_client, purchase_order_ids, **kwargs
        )
        return line_items

    @classmethod
    def get_ingram_api_data(
        cls, provider, po_numbers, line_item_identifiers, po_id
    ):
        result = dict()
        purchase_order_ingram_data = (
            EmailTemplateGenerationService.get_ingram_api_data(
                provider, po_numbers
            )
        )
        for purchase_order_data in purchase_order_ingram_data:
            for line_data in purchase_order_data.get("lines", []):
                carrier, ship_date, tracking_number, serial_numbers = (
                    None,
                    None,
                    None,
                    [],
                )
                shipped_quantity = int(line_data.get("confirmedquantity", 0))
                part_number = line_data.get("manufacturerpartnumber")
                estimated_delivery_date = line_data.get(
                    "promiseddeliverydate", ""
                )
                if part_number in line_item_identifiers:
                    if part_number in result:
                        result[part_number][
                            "shipped_quantity"
                        ] += shipped_quantity
                        continue
                    shipment = line_data.get("shipmentdetails", [])
                    if shipment:
                        carrier = shipment[0].get("carriername")
                        ship_date = shipment[0].get("shipmentdate")
                        package = shipment[0].get("packagedetails", [])
                        tracking_number_list = []
                        if package:
                            tracking_number = package[0].get("trackingnumber")
                            if tracking_number:
                                tracking_number_list.append(tracking_number)
                            serial_number_details = package[0].get(
                                "serialnumberdetails", []
                            )
                            if serial_number_details:
                                serial_numbers = [
                                    item.get("serialNumber")
                                    for item in serial_number_details
                                    if item.get("serialNumber")
                                ]
                    result[part_number] = dict(
                        carrier=carrier,
                        ship_date=ship_date,
                        tracking_number=tracking_number_list,
                        serial_numbers=serial_numbers,
                        shipped_quantity=shipped_quantity,
                        estimated_delivery_date=estimated_delivery_date,
                    )
        if po_id:
            result = cls.tracking_number_filter(po_id, provider, result)
        return result

    @classmethod
    def tracking_number_filter(cls, po_id, provider, result):
        required_fields = [
            "id",
            "product/identifier",
            "trackingNumber",
            "purchaseOrderId",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        po_line_items = (
            OrderTrackingReceivingService.get_purchase_order_line_items(
                provider, po_id, fields_filter=[fields_filter]
            )
        )
        for line_item in po_line_items:
            product_id = line_item.get("product", {}).get("identifier")
            tracking_number = line_item.get("trackingNumber", "")
            if tracking_number:
                for ingram_data in result:
                    if ingram_data == product_id:
                        ingram_tracking_number = ingram_data.get(
                            "tracking_number"
                        )
                        updated_tracking_number = [
                            i
                            for i in ingram_tracking_number
                            if i not in tracking_number
                        ]
                        ingram_data[
                            "tracking_number"
                        ] = updated_tracking_number
            return result

    @classmethod
    def receive_line_items(cls, customer_id, provider, user, payload):
        client: Connectwise = provider.erp_client
        # Update line Items
        cls.update_purchase_order_line_items(
            provider, user, client, customer_id, payload
        )

        # Create Opportunity
        opportunity_payload = list()
        for line_item in payload.get("line_items", []):
            if line_item.get("opportunity"):
                opportunity_payload.append(line_item.get("opportunity"))
        if opportunity_payload:
            cls.create_opportunity(provider, customer_id, opportunity_payload)

        # Create Device
        # Making payload to create devices which have configuration key in line items
        create_device_payload = list()
        # for config in payload["line_items"]:
        for line_item in payload.get("line_items", []):
            if "configuration" in line_item:
                create_device_payload.append(line_item)

        # Call create device method
        if create_device_payload:
            cls.create_devices(provider.id, customer_id, create_device_payload)

    @classmethod
    def update_purchase_order_line_items(
        cls, provider, user, client, customer_id, payload
    ):
        smartnet_receiving_settings = (
            SmartnetReceivingService.get_smartnet_receiving_settings(provider)
        )
        partial_receive_status_id = (
            smartnet_receiving_settings.partial_receive_status
        )
        if partial_receive_status_id:
            try:
                for status in partial_received_status:
                    if status.get("id") == partial_receive_status_id:
                        received_status = status.get("status")
            except Exception as e:
                received_status = None
        else:
            raise ValidationError(
                "Partial receiving setting is not set. Please set it"
            )
        for line_item in payload.get("line_items", []):
            line_item_id = line_item.pop("line_item_id")
            if line_item.get("received_status") != FULLY_RECEIVED:
                line_item["received_status"] = received_status
            if int(line_item.get("received_qty")) == 0:
                line_item["received_status"] = WAITING
            client.update_purchase_order_line_item(
                provider,
                payload.get("purchase_order_id"),
                line_item_id,
                **line_item,
            )

        # Create Service Ticket Notes
        service_ticket_created_by = (
            ServiceRequestModelMixin().get_created_by_for_service_ticket(user)
        )
        if payload.get("ticket_id") and payload.get("ticket_note"):
            ServiceRequestService.create_service_ticket_note(
                client,
                payload.get("ticket_id"),
                service_ticket_created_by,
                {"text": payload.get("ticket_note")},
            )

    @classmethod
    def create_opportunity(cls, provider, customer_id, opportunity_payload):
        create_opportunity_task.apply_async(
            (
                provider.id,
                customer_id,
                opportunity_payload,
            ),
            queue="high",
        )

    @classmethod
    def create_devices(cls, provider_id, customer_id, payload):
        provider = Provider.objects.get(id=provider_id)
        customer = provider.customer_set.get(id=customer_id)
        field_mapping_list = [
            "product_id",
            "serial_number",
            "product_type",
            "service_contract_number",
            "expiration_date",
            "installation_date",
        ]
        field_mapping_dict = {field: field for field in field_mapping_list}
        field_mapping_payload = {"field_mappings": field_mapping_dict}
        data = list()
        for config in payload:
            if not config.get("serial_numbers"):
                device_category_ids = (
                    provider.erp_integration.other_config.get(
                        "empty_serial_mapping", {}
                    ).get("device_category_ids", [])
                )
                if (
                    config.get("configuration").get("category_id")
                    in device_category_ids
                ):
                    config_data = config.get("configuration", [])
                    config_data["device_name"] = config.get(
                        "configuration"
                    ).get("product_id")
                    data.append(deepcopy(config_data))
                else:
                    return None
            else:
                for serial_num in config.get("serial_numbers", []):
                    config_data = config.get("configuration", [])
                    config_data["device_name"] = config.get(
                        "configuration"
                    ).get("product_id")
                    config_data["serial_number"] = serial_num
                    data.append(deepcopy(config_data))
        field_mapping = field_mapping_payload.get("field_mappings")
        if data:
            validated_data = (
                SmartnetReceivingService.validate_create_device_input_data(
                    data, field_mapping
                )
            )
            for manufacturer_id, validated_payload in groupby(
                validated_data, lambda x: x.get("manufacturer_id")
            ):
                # category_id = provider.get_device_category_id_by_manufacturer(
                #     manufacturer_id
                # )
                category_id = None
                create_devices_task.apply_async(
                    (
                        provider_id,
                        category_id,
                        customer.crm_id,
                        manufacturer_id,
                        list(validated_payload),
                    ),
                    queue="high",
                )


class WeeklyRecapService:
    """
    Handles order tracking weekly recap emails.
    """

    def __init__(
        self,
        provider: Provider,
        weekly_recap_setting: AutomatedWeeklyRecapSetting,
    ):
        self.provider = provider
        self.weekly_recap_setting = weekly_recap_setting

    def _get_open_sales_orders_for_customer(
        self, customer_crm_id: int
    ) -> "QuerySet[SalesPurchaseOrderData]":
        """
        Get open sales orders for customer. A sales order is considered open if it has
        atleast one PO line item with unit cost greater than zero and yet to be received.

        Args:
            customer_crm_id: Customer CRM ID
        """
        # Subquery to filter PO line item with unit_cost > 0 and excluding received_status = "FullyReceived"
        non_received_line_items = Subquery(
            ConnectwisePurchaseOrderLineitemsData.objects.filter(
                provider_id=self.provider.id,
                po_number=OuterRef("po_number"),
                unit_cost__gt=0,
            )
            .exclude(Q(received_status__in=[FULLY_RECEIVED]))
            .values("id")
        )
        # Subquery for POs with non received line items
        pos_with_non_received_line_items = (
            ConnectwisePurchaseOrderData.objects.select_related(
                "po_number", "po_number__cw_line_items_data"
            )
            .filter(
                provider_id=self.provider.id,
                sales_order_id=OuterRef("id"),
                po_number__cw_line_items_data__in=non_received_line_items,
            )
            .values("id")
        )
        # Subquery to get last product ship date
        last_product_ship_date = Subquery(
            ConnectwisePurchaseOrderData.objects.select_related(
                "po_number", "po_number__cw_line_items_data"
            )
            .filter(
                provider_id=self.provider.id,
                sales_order_id=OuterRef("id"),
                po_number__cw_line_items_data__in=non_received_line_items,
            )
            .order_by("-po_number__cw_line_items_data__expected_ship_date")
            .values("po_number__cw_line_items_data__expected_ship_date")[:1]
        )
        sales_orders: "QuerySet[SalesPurchaseOrderData]" = (
            SalesPurchaseOrderData.objects.select_related("customer")
            .filter(
                customer__crm_id=customer_crm_id,
                provider_id=self.provider.id,
                linked_pos__isnull=False,
                linked_pos__in=pos_with_non_received_line_items,
            )
            .annotate(last_product_ship_date=last_product_ship_date)
        )
        return sales_orders

    def _get_unlinked_open_purchase_orders_for_customer(
        self, customer_crm_id: int
    ) -> "QuerySet[ConnectwisePurchaseOrderData]":
        """
        Get unlinked purchase orders having at least one unreceived line item with
        unit cost > 0

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Purchase orders
        """
        # Subquery to get the latest expected ship date
        last_product_ship_date = Subquery(
            ConnectwisePurchaseOrderLineitemsData.objects.filter(
                provider=OuterRef("provider"),
                po_number=OuterRef("po_number"),
                unit_cost__gt=0,
            )
            .exclude(received_status__in=[FULLY_RECEIVED])
            .values("expected_ship_date")
            .order_by("-expected_ship_date")[:1]
        )
        open_purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            ConnectwisePurchaseOrderData.objects.filter(
                provider=self.provider,
                sales_order__isnull=True,
                customer_company_id=customer_crm_id,
                po_number__cw_line_items_data__isnull=False,
            )
            .annotate(last_product_ship_date=last_product_ship_date)
            .distinct()
        )
        return open_purchase_orders

    def get_open_sales_orders_data_for_customer(
        self, customer_crm_id: int
    ) -> List[Dict]:
        """
        Get open sales order data for a customer.

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Open sales orders
        """
        open_sales_orders: "QuerySet[SalesPurchaseOrderData]" = (
            self._get_open_sales_orders_for_customer(customer_crm_id)
        )
        if not open_sales_orders:
            return []

        sites: Dict[
            int, str
        ] = self.provider.erp_client.get_customer_complete_site_addresses_with_name(
            customer_crm_id,
            set(
                open_sales_orders.values_list(
                    "shipping_site_crm_id", flat=True
                )
            ),
        )
        sales_orders_data: List[Dict] = [
            dict(
                crm_id=sales_order.crm_id,
                customer_po=sales_order.customer_po_number,
                shipping_contact=sales_order.shipping_contact_name,
                opportunity_summary=sales_order.opportunity_name,
                site=sites.get(sales_order.shipping_site_crm_id, ""),
                last_product_ship_date=sales_order.last_product_ship_date.strftime(
                    "%m/%d/%Y"
                )
                if sales_order.last_product_ship_date
                else "TBD",
            )
            for sales_order in open_sales_orders
        ]
        return sales_orders_data

    def get_open_purchase_order_data_for_customer(
        self, customer_crm_id: int
    ) -> List[Dict]:
        """
        Get unlinked open purchase orders for customer.

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Open purchase orders.
        """
        open_purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            self._get_unlinked_open_purchase_orders_for_customer(
                customer_crm_id
            )
        )
        if not open_purchase_orders:
            return []
        purchase_order_data: List[Dict] = [
            dict(
                crm_id=purchase_order.crm_id,
                po_number=purchase_order.po_number_id,
                customer_site=purchase_order.site_name,
                last_product_ship_date=purchase_order.last_product_ship_date.strftime(
                    "%m/%d/%Y"
                )
                if purchase_order.last_product_ship_date
                else "TBD",
            )
            for purchase_order in open_purchase_orders
        ]
        return purchase_order_data

    def get_order_tracking_tracking_contact_details(
        self, customer_crm_id: int
    ) -> Dict:
        """
        Get customer order tracking contact details.

        Args:
            customer_crm_id: Customer CRM ID

        Returns:
            Contact details
        """
        contacts: List[Dict] = self.provider.erp_client.get_customer_users(
            customer_id=customer_crm_id,
            active_users_only=True,
            type_filter=self.weekly_recap_setting.contact_type_crm_id,
        )
        if contacts:
            return dict(
                first_name=contacts[0].get("first_name"),
                last_name=contacts[0].get("last_name"),
                title=contacts[0].get("title"),
                email=contacts[0].get("email"),
            )
        else:
            return {}

    def _get_render_context(self, customer: Customer) -> Dict:
        """
        Get weekly recap email body render context.

        Args:
            customer: Customer

        Returns:
            Render context
        """
        current_datetime = timezone.now()
        provider_datetime = convert_utc_datetime_to_provider_timezone(
            current_datetime, self.provider.timezone
        )
        email_sent_datetime: str = provider_datetime.strftime(
            "%A, %B %-d, %Y %-I:%M %p"
        )
        customer_contact: Dict = (
            self.get_order_tracking_tracking_contact_details(customer.crm_id)
        )
        render_context: Dict = dict(
            sender_details=dict(
                name=self.weekly_recap_setting.sender.name,
                email=self.weekly_recap_setting.sender.email,
                email_signature=self.weekly_recap_setting.sender.profile.email_signature.get(
                    "email_signature", None
                ),
            ),
            subject=f"{customer.name} - {self.weekly_recap_setting.email_subject}",
            email_body=self.weekly_recap_setting.email_body_text,
            email_sent_datetime=email_sent_datetime,
            customer_contact=customer_contact,
        )
        return render_context

    @staticmethod
    def render_weekly_recap_email_body(render_context: Dict) -> str:
        """
        Render weekly recap email body.

        Args:
            render_context: Render context

        Returns:
            Weekly recap email body
        """
        template_path: str = (
            AutomatedWeeklyRecapSetting.get_weekly_recap_email_body_template_path()
        )
        rendered_email_body: str = render_to_string(
            template_path, render_context
        )
        return rendered_email_body

    def send_order_tracking_weekly_recap_email(
        self, customer: Customer
    ) -> Tuple[bool, Optional[str]]:
        """
        Send order tracking weekly recap email for customer.
        Email will not be sent if no open sales orders are found for the customer.

        Args:
            customer: Customer

        Returns:
            Boolean indicating if email sent, reason for not sending email
        """
        open_sales_orders: List[
            Dict
        ] = self.get_open_sales_orders_data_for_customer(customer.crm_id)
        open_purchase_orders: List[
            Dict
        ] = self.get_open_purchase_order_data_for_customer(customer.crm_id)
        if not open_sales_orders and not open_purchase_orders:
            return (
                False,
                "Open sales orders, open purchase orders not found for the customer.",
            )

        render_context: Dict = self._get_render_context(customer)
        render_context.update(
            open_sales_orders=open_sales_orders,
            open_purchase_orders=open_purchase_orders,
        )
        email_body: str = self.render_weekly_recap_email_body(render_context)
        WeeklyRecapEmail(
            context=dict(
                email_body=email_body,
                provider=self.provider,
                subject=render_context.get("subject", ""),
            ),
            from_email=render_context.get("sender_details", {}).get(
                "email", ""
            ),
            subject=render_context.get("subject", ""),
        ).send(
            to=self.weekly_recap_setting.receiver_email_ids, log_error=False
        )
        return True, None

    def generate_email_preview(self) -> str:
        """
        Generate weekly recap email preview.

        Returns:
            Email preview
        """
        render_context: Dict = dict(
            open_sales_orders=[
                dict(
                    crm_id=1,
                    customer_po="PO-123",
                    shipping_contact="Bruce Wayne",
                    opportunity_summary="HW opportunity 101",
                    site="WH 99, Mountain Drive, San Andreas, California, 1234",
                    last_product_ship_date="11/12/2023",
                ),
                dict(
                    crm_id=2,
                    customer_po="PO-456",
                    shipping_contact="Ray Palmer",
                    opportunity_summary="SW opportunity 201",
                    site="Main 101, Stairway Lane, Los Angeles, California, 5678",
                    last_product_ship_date="20/12/2023",
                ),
            ],
            open_purchase_orders=[
                dict(
                    crm_id=101,
                    po_number="LP12345-ABC",
                    customer_site="Main",
                    last_product_ship_date="01/20/2023",
                ),
                dict(
                    crm_id=102,
                    po_number="LP67891-DEF",
                    customer_site="Headquarters",
                    last_product_ship_date="01/20/2023",
                ),
            ],
            sender_details=dict(
                name=self.weekly_recap_setting.sender.name,
                email=self.weekly_recap_setting.sender.email,
                email_signature=self.weekly_recap_setting.sender.profile.email_signature.get(
                    "email_signature", None
                ),
            ),
            subject=f"CUSTOMER_NAME - {self.weekly_recap_setting.email_subject}",
            email_body=self.weekly_recap_setting.email_body_text,
            email_sent_datetime="Sunday, January 1, 2023 11:00 PM",
            customer_contact=dict(
                first_name="FIRST_NAME",
                last_name="LAST_NAME",
                email="customer@customer.com",
            ),
        )
        preview: str = self.render_weekly_recap_email_body(render_context)
        return preview
