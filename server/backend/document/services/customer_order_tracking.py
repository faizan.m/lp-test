import copy
from typing import Dict, Generator, List, Optional, Tuple, Union

from django.db import IntegrityError
from django.utils import timezone
from requests import Response
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from accounts.models import Provider, Customer
from core.integrations import Connectwise
from core.integrations.erp.connectwise import field_mappings
from core.integrations.erp.connectwise.field_mappings import InvoiceById
from core.integrations.utils import prepare_response
from document.models import (
    SalesPurchaseOrderData,
    ProductsItemData,
    ConnectwisePurchaseOrderLineitemsData,
    FinanceData,
    ConnectwisePurchaseOrderData,
    OperationDashboardSettings,
    InvoiceData,
)
from document.services import QuoteService
from utils import DbOperation, get_app_logger

logger = get_app_logger(__name__)


class SalesOrderCreateSerializer(serializers.Serializer):
    crm_id = serializers.IntegerField()
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    customer_po_number = serializers.CharField(
        allow_null=True, allow_blank=True
    )
    order_date = serializers.DateTimeField(allow_null=True)
    opportunity_name = serializers.CharField(allow_null=True, allow_blank=True)
    opportunity_crm_id_id = serializers.IntegerField(allow_null=True)
    opportunity_won_date = serializers.DateTimeField(
        allow_null=True, required=False
    )
    site_crm_id = serializers.IntegerField(allow_null=True)
    site_name = serializers.CharField(allow_null=True, allow_blank=True)
    customer = serializers.PrimaryKeyRelatedField(
        allow_null=True, queryset=Customer.objects.all(), required=False
    )
    company_name = serializers.CharField(allow_null=True)
    status_name = serializers.CharField(allow_null=True)
    status_id = serializers.IntegerField(allow_null=True)
    product_ids = serializers.ListField(allow_empty=True)
    bill_closed_flag = serializers.BooleanField()
    invoice_ids = serializers.ListField(allow_empty=True)
    shipping_site_crm_id = serializers.IntegerField(allow_null=True)
    shipping_contact_crm_id = serializers.IntegerField(allow_null=True)
    shipping_contact_name = serializers.CharField(
        allow_null=True, allow_blank=True
    )
    due_date = serializers.DateTimeField(allow_null=True, required=False)
    total = serializers.FloatField(allow_null=True, required=False)
    sub_total = serializers.FloatField(allow_null=True, required=False)
    tax_total = serializers.FloatField(allow_null=True, required=False)
    location = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    so_linked_pos = serializers.ListField(allow_empty=True)
    po_custom_field_present = serializers.JSONField(required=False)
    last_crm_update = serializers.DateTimeField(allow_null=True)

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=SalesPurchaseOrderData.objects.all(),
                fields=("provider", "crm_id"),
            )
        ]


class SalesOrderUpdateSerializer(SalesOrderCreateSerializer):
    crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("provider", "crm_id")


class ProductitemCreateSerializer(serializers.Serializer):
    crm_id = serializers.IntegerField()
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    product_id = serializers.CharField(allow_null=True, allow_blank=True)
    description = serializers.CharField(allow_null=True, allow_blank=True)
    quantity = serializers.IntegerField(allow_null=True)
    sales_order = serializers.PrimaryKeyRelatedField(
        allow_null=True, queryset=SalesPurchaseOrderData.objects.all()
    )
    invoice_id = serializers.IntegerField(allow_null=True)
    invoice_number = serializers.CharField(allow_null=True, allow_blank=True)
    cancelled_quantity = serializers.IntegerField(allow_null=True)
    cost = serializers.FloatField(allow_null=True)
    sequence_number = serializers.IntegerField(allow_null=True)
    price = serializers.FloatField(required=False, allow_null=True)
    ext_cost = serializers.FloatField(required=False, allow_null=True)
    ext_price = serializers.FloatField(required=False, allow_null=True)

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=ProductsItemData.objects.all(),
                fields=("provider", "crm_id"),
            )
        ]


class ProductItemUpdateSerializer(ProductitemCreateSerializer):
    crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("provider", "crm_id")


class SalesOrderQueryService:
    def __init__(self, provider: Provider):
        self.provider = provider

    def update_sales_order_by_crm_id(
        self, crm_id: int, payload: Dict
    ) -> object:
        """
        Updates the sales order instance where crm_id is crm_id.
        Sets the new attributes using payload.
        Args:
            crm_id: CRM id of the sales order
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        sales_order_query = SalesPurchaseOrderData.objects.filter(
            provider=self.provider, crm_id=crm_id
        )
        sales_order_query.update(**payload)
        return sales_order_query

    def create_sales_order(self, payload: Dict):
        try:
            sales_order = SalesPurchaseOrderData.objects.create(**payload)
        except IntegrityError as e:
            logger.info(e)
            return None
        return sales_order

    @staticmethod
    def delete_sales_order(filter_conditions: Dict) -> object:
        """
        Delete sales order filtered using the filter conditions.
        Args:
            filter_conditions: A dict of SalesPurchaseOrderData model fields as key and
            value of the key.

        """
        result = SalesPurchaseOrderData.objects.filter(
            **filter_conditions
        ).delete()
        return result

    def get_customer_by_crm_id(self, crm_id: int) -> Optional[int]:
        """
        Searches the customer on the crm_id field and returns the DB Id if found, else
        returns None.
        Args:
            crm_id: CRM Id of the customer

        """
        try:
            customer = Customer.objects.get(
                provider=self.provider, crm_id=crm_id
            ).id
        except Customer.DoesNotExist:
            return None
        return customer

    def get_sales_order_id_by_crm_id(self, crm_id: int) -> Optional[int]:
        """
        Searches the sales order on the crm_id field and returns the DB Id if found, else
        returns None.
        Args:
            crm_id: CRM Id of the sales order

        """
        try:
            sales_order_id = SalesPurchaseOrderData.objects.get(
                provider_id=self.provider.id, crm_id=crm_id
            ).id
        except SalesPurchaseOrderData.DoesNotExist:
            return None
        return sales_order_id

    def update_product_item_by_crm_id(
        self, crm_id: int, payload: Dict
    ) -> object:
        """
        Updates the product item instance where crm_id is crm_id.
        Sets the new attributes using payload.
        Args:
            crm_id: CRM id of the sales order
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        product_item_query = ProductsItemData.objects.filter(
            provider=self.provider, crm_id=crm_id
        )
        product_item_query.update(**payload)
        return product_item_query

    def create_product_item(self, bulk_payload: List):
        try:
            product_item = ProductsItemData.objects.bulk_create(bulk_payload)
        except IntegrityError as e:
            logger.info(e)
            return None
        return product_item

    @staticmethod
    def delete_product_item(filter_conditions: Dict) -> object:
        """
        Delete product Item filtered using the filter conditions.
        Args:
            filter_conditions: A dict of SalesPurchaseOrderData model fields as key and
            value of the key.

        """
        result = ProductsItemData.objects.filter(**filter_conditions).delete()
        return result


class ConnectwiseSalesOrderPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on Project
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        If the actions is deleted, payload accepts a set of crm Id's of the deleted
        projects.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = SalesOrderQueryService(provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        # update custom field value from list to dict
        self.payload["customFields"] = self.payload.get("customFields")[0]

        self.payload = prepare_response(
            self.payload, field_mappings.SalesOrder
        )

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        company_crm_id = self.payload.pop("company_crm_id", None)
        so_linked_pos = self.payload.get("so_linked_pos", [])
        if so_linked_pos:
            so_linked_pos = so_linked_pos.split(", ")
            is_pos_exists = ConnectwisePurchaseOrderData.objects.filter(
                po_number__in=so_linked_pos
            )
            if is_pos_exists.count() == len(so_linked_pos):
                po_custom_field_present = {
                    "po_custom_field_present": {
                        "po_custom_field_present": True,
                        "reason": f"Custom Field has value of purchase order in CW and "
                        f"purchase orders {so_linked_pos} are present in Acela",
                    }
                }
            else:
                po_custom_field_present = {
                    "po_custom_field_present": {
                        "po_custom_field_present": True,
                        "reason": f"Custom Field has value of purchase order in CW but "
                        f"purchase orders {so_linked_pos} are not synced in Acela",
                    }
                }
        else:
            po_custom_field_present = {
                "po_custom_field_present": {
                    "po_custom_field_present": False,
                    "reason": f"Custom Field has no value of purchase order in CW",
                }
            }
        # fetch opportunity won date
        opportunity_won_date = None
        if self.payload.get("opportunity_crm_id"):
            quote_data = QuoteService.get_quote(
                self.provider.id, self.payload.get("opportunity_crm_id")
            )
            opportunity_won_date = quote_data.get("closed_date", "")
        transformed_payload = dict(
            crm_id=self.payload.get("crm_id"),
            customer_po_number=self.payload.get("customer_po_number"),
            order_date=self.payload.get("order_date"),
            opportunity_name=self.payload.get("opportunity_name"),
            opportunity_crm_id_id=self.payload.get("opportunity_crm_id"),
            opportunity_won_date=opportunity_won_date,
            site_crm_id=self.payload.get("site_crm_id"),
            site_name=self.payload.get("site_name"),
            company_name=self.payload.get("company_name"),
            status_id=self.payload.get("status_id"),
            status_name=self.payload.get("status_name"),
            product_ids=self.payload.get("product_ids", []),
            bill_closed_flag=self.payload.get("bill_closed_flag"),
            invoice_ids=self.payload.get("invoice_ids", []),
            provider=self.provider.id,
            shipping_contact_name=self.payload.get(
                "ship_to_contact_name", None
            ),
            shipping_contact_crm_id=self.payload.get(
                "ship_to_contact_crm_id", None
            ),
            shipping_site_crm_id=self.payload.get(
                "shipping_site_crm_id", None
            ),
            due_date=self.payload.get("due_date"),
            total=self.payload.get("total"),
            sub_total=self.payload.get("sub_total"),
            tax_total=self.payload.get("tax_total"),
            location=self.payload.get("location"),
            so_linked_pos=so_linked_pos,
            po_custom_field_present=po_custom_field_present,
            last_crm_update=self.payload.get("last_crm_update", None),
        )
        if company_crm_id:
            customer_id = self.query_service.get_customer_by_crm_id(
                crm_id=company_crm_id
            )
            transformed_payload["customer"] = customer_id
        else:
            transformed_payload["customer"] = None

        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = SalesOrderCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = SalesOrderUpdateSerializer
        sales_order_serializer = serializer(data=self.payload)
        is_valid = sales_order_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = sales_order_serializer.errors
            return is_valid, errors
        self.payload = sales_order_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        if self.action == DbOperation.UPDATE:
            sales_order_present = SalesPurchaseOrderData.objects.filter(
                provider=self.provider, crm_id=crm_id
            ).exists()
            if sales_order_present:
                try:
                    self.query_service.update_sales_order_by_crm_id(
                        crm_id, self.payload
                    )
                except IntegrityError as e:
                    logger.info(e)
            else:
                # This handles the case when the update action is called but the sales order is
                # present. It creates the sales orders. Use in cases when the create action failed for the
                # object.
                self.payload.update(
                    dict(provider_id=self.provider.id, crm_id=crm_id)
                )
                self.query_service.create_sales_order(self.payload)
        elif self.action == DbOperation.CREATE:
            self.query_service.create_sales_order(self.payload)
        else:
            filter_conditions = dict(
                provider=self.provider,
                crm_id__in=self.payload.get("crm_id", []),
            )
            self.query_service.delete_sales_order(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("crm_id")
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class ConnectwiseProductItemsPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on Project
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        If the actions is deleted, payload accepts a set of crm Id's of the deleted
        projects.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = SalesOrderQueryService(provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        self.payload = prepare_response(
            self.payload, field_mappings.ProductItem
        )

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        if self.payload.get("sales_order_crm_id"):
            sales_order_id = self.query_service.get_sales_order_id_by_crm_id(
                self.payload.get("sales_order_crm_id")
            )
        else:
            sales_order_id = ""

        transformed_payload = dict(
            crm_id=self.payload.get("crm_id"),
            product_id=self.payload.get("product_id"),
            description=self.payload.get("description"),
            quantity=self.payload.get("quantity"),
            sales_order=sales_order_id,
            invoice_id=self.payload.get("invoice_id"),
            invoice_number=self.payload.get("invoice_number"),
            cancelled_quantity=self.payload.get("cancelled_quantity"),
            provider=self.provider.id,
            cost=self.payload.get("cost"),
            price=self.payload.get("price"),
            sequence_number=self.payload.get("sequence_number"),
        )

        if self.payload.get("quantity"):
            ext_cost = self.payload.get("quantity") * self.payload.get("cost")
            ext_price = self.payload.get("quantity") * self.payload.get(
                "price"
            )
            transformed_payload.update(
                {ext_price: ext_price, ext_cost: ext_cost}
            )

        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = ProductitemCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = ProductItemUpdateSerializer
        product_item_serializer = serializer(data=self.payload)
        is_valid = product_item_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = product_item_serializer.errors
            return is_valid, errors
        self.payload = product_item_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        if self.action == DbOperation.UPDATE:
            product_item_present = ProductsItemData.objects.filter(
                provider=self.provider, crm_id=crm_id
            ).exists()
            if product_item_present:
                self.query_service.update_product_item_by_crm_id(
                    crm_id, self.payload
                )
            else:
                # This handles the case when the update action is called but the product item is
                # present. It creates the product item. Use in cases when the create action failed for the
                # object.
                self.payload.update(
                    dict(provider_id=self.provider.id, crm_id=crm_id)
                )
                product_item_model_data = [ProductsItemData(**self.payload)]
                self.query_service.create_product_item(product_item_model_data)
        elif self.action == DbOperation.CREATE:
            self.query_service.create_product_item(
                [ProductsItemData(**self.payload)]
            )
        else:
            filter_conditions = dict(
                provider=self.provider,
                crm_id__in=self.payload.get("crm_ids_to_delete", []),
            )
            self.query_service.delete_product_item(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("crm_id")
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class SalesOrderService:
    def __init__(self, provider):
        self.provider = provider
        self.query_service = SalesOrderQueryService(provider)

    def get_provider_sales_orders_from_erp(
        self,
    ) -> Optional[Generator[Response, None, None]]:
        erp_client: Connectwise = self.provider.erp_client

        # fetch sales orders from service ticket created date after settings
        try:
            dashboard_settings: OperationDashboardSettings = (
                OperationDashboardSettings.objects.get(provider=self.provider)
            )
            created_date = dashboard_settings.service_ticket_create_date
        except OperationDashboardSettings.DoesNotExist as e:
            logger.info(
                f"Provider required Operations Dashboard settings not configured.{e}"
            )
            created_date = None
        sales_orders = erp_client.get_sales_orders(created_date=created_date)
        return sales_orders

    def get_products_for_sales_orders_from_erp(
        self, sales_order_list
    ) -> Optional[Generator[Response, None, None]]:
        erp_client: Connectwise = self.provider.erp_client
        sales_orders_products = (
            erp_client.get_sales_order_products_list_with_generator(
                sales_order_list
            )
        )
        return sales_orders_products


class FinanceReportService:
    def __init__(self, provider):
        self.provider = provider

    @staticmethod
    def export_finance_report(provider, filter_params):
        status = filter_params.get("status")
        filter_conditions: Dict = {}
        if status:
            line_items_status = provider.erp_client.line_items_status()
            for _status in line_items_status:
                if int(status) == _status.get("id"):
                    filter_conditions.update(
                        {
                            "is_closed": _status.get("is_closed", False),
                            "is_cancelled": _status.get("is_cancelled"),
                        }
                    )
                    break

        # date range filters for finance data
        date_ranges: List[Tuple[str, str, str]] = [
            ("po_date", "po_date_after", "po_date_before"),
            (
                "expected_ship_date",
                "expected_ship_date_after",
                "expected_ship_date_before",
            ),
            ("received_date", "received_date_after", "received_date_before"),
            (
                "opportunity_won_date",
                "opportunity_won_date_after",
                "opportunity_won_date_before",
            ),
        ]

        for field, after_param, before_param in date_ranges:
            after_date = filter_params.get(after_param)
            before_date = filter_params.get(before_param)
            if after_date and before_date:
                filter_conditions[field + "__range"] = [
                    after_date,
                    before_date,
                ]

        # filter for invoice data
        invoice_filter_conditions: Dict = {}
        invoice_date_ranges = [
            ("created_date", "created_date_after", "created_date_before"),
            ("payment_date", "payment_date_after", "payment_date_before"),
        ]

        for field, after_param, before_param in invoice_date_ranges:
            after_date = filter_params.get(after_param)
            before_date = filter_params.get(before_param)
            if after_date and before_date:
                invoice_filter_conditions[field + "__range"] = [
                    after_date,
                    before_date,
                ]

        # fetch invoice data
        invoices: "QuerySet[InvoiceData]" = InvoiceData.objects.filter(
            provider=provider, **invoice_filter_conditions
        ).only("number", "data")

        invoice_data: Dict[str, Dict] = {}
        for invoice in invoices:
            invoice_data[invoice.number] = invoice.data

        # fetch shipping cost of shipping type product
        products_items: "QuerySet[ProductsItemData]" = (
            ProductsItemData.objects.filter(
                provider=provider, product_id="Shipping"
            ).only("invoice_id", "cost")
        )
        shipping_cost: Dict[str, Union[float, int]] = {}
        for item in products_items:
            shipping_cost[item.invoice_id] = item.cost

        finance_data = FinanceData.objects.filter(
            provider=provider, **filter_conditions
        ).only(
            "received_status",
            "po_number",
            "purchase_order_status",
            "product_id",
            "description",
            "received_quantity",
            "unit_cost_in_receiving",
            "vendor_company_name",
            "vendor_order_number",
            "customer_company_name",
            "ordered_quantity",
            "serial_numbers",
            "shipping_method",
            "tracking_numbers",
            "ext_cost_in_receiving",
            "location",
            "expected_ship_date",
            "unit_cost_in_so",
            "unit_price",
            "ext_cost_in_so",
            "ext_price",
            "opportunity_name",
            "sales_order_crm_id",
            "po_date",
            "so_linked_pos",
            "customer_po_number",
            "quantity",
            "shipment_date",
            "total",
            "entered_by",
            "invoice_numbers",
            "shipping_instruction",
            "sales_tax",
            "unit_of_measure",
            "internal_notes_po",
            "due_date",
            "total_in_so",
            "sub_total",
            "tax_total",
            "line_number",
            "received_date",
            "order_date",
            "invoice_ids",
            "opportunity_crm_id",
            "status_name",
            "invoice_number",
        )

        # if invoice payment date filter apply then filter finance data on that invoices
        if invoice_filter_conditions.get("payment_date__range"):
            invoices_list = list(invoice_data.keys())
            finance_data = finance_data.filter(
                invoice_number__contained_by=invoices_list
            )

        result: List[Dict] = []
        for data in finance_data:
            if data.invoice_number:
                # invoice_numbers = data.invoice_number
                # for invoice_number in invoice_numbers:
                #     _invoice_data = invoice_data.get(invoice_number, {})
                #     _shipping_cost = shipping_cost.get(
                #         _invoice_data.get("invoice_crm_id")
                #     )
                #     if _invoice_data:
                #         invoice_data.pop(invoice_number)
                #     query_data: Dict = {
                #         **data.__dict__,
                #         **_invoice_data,
                #         "vendor_invoice_number": invoice_number,
                #         "invoice_shipping_price": _shipping_cost,
                #     }
                #     result.append(query_data)
                _invoice_data: Dict = invoice_data.get(data.invoice_number, {})
                _shipping_cost: Union[int, float] = shipping_cost.get(
                    _invoice_data.get("invoice_crm_id")
                )
                if _invoice_data:
                    invoice_data.pop(data.invoice_number)
                query_data: Dict = {
                    **data.__dict__,
                    **_invoice_data,
                    "vendor_invoice_number": data.invoice_number,
                    "invoice_shipping_price": _shipping_cost,
                }
                result.append(query_data)
            else:
                result.append(data.__dict__)

        # include all invoices for create date filter either associated with SO-PO or not
        if invoice_filter_conditions.get("created_date__range"):
            for (
                unmapped_invoice_data,
                unmapped_invoice_values,
            ) in invoice_data.items():
                unmapped_invoice_values[
                    "vendor_invoice_number"
                ] = unmapped_invoice_values.get("invoice_number")
                result.append(unmapped_invoice_values)
        return result

    def get_invoices_data_from_erp(
        self,
    ) -> Optional[Generator[Response, None, None]]:
        try:
            dashboard_settings: OperationDashboardSettings = (
                OperationDashboardSettings.objects.get(provider=self.provider)
            )
            created_date = dashboard_settings.service_ticket_create_date
        except OperationDashboardSettings.DoesNotExist as e:
            logger.info(
                f"Provider required Operations Dashboard settings not configured.{e}"
            )
            created_date = None
        erp_client: Connectwise = self.provider.erp_client
        get_invoices_data = erp_client.get_invoices_with_generator(
            created_date=created_date
        )
        return get_invoices_data

    def get_payment_details_from_erp(self, invoice_crm_id):
        erp_client: Connectwise = self.provider.erp_client
        get_invoices_data = erp_client.get_invoice_payment_details(
            invoice_crm_id
        )
        return get_invoices_data
