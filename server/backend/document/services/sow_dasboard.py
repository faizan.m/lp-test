import datetime as dt
from collections import OrderedDict
from datetime import datetime, timedelta

import dateutil.parser
import pandas as pd
from django.conf import settings

from caching.service import CacheService, caching
from core.utils import (
    convert_date_string_to_date_object,
    group_list_of_dictionary_by_key,
)
from document.models import DashboardMetricData
from document.services import DocumentService


class SOWDashboardService:
    """
    SOW Dashboard Service Layer
    """

    @classmethod
    def _get_hours_from_document(cls, document):
        service_cost = document.get("json_config", {}).get("service_cost", {})
        hours = (
            int((service_cost.get("engineering_hours", 0)))
            + int(service_cost.get("project_management_hours", 0))
            + int(service_cost.get("after_hours", 0))
        )
        return hours

    @classmethod
    def _get_document_response(cls, documents, get_hours=True):
        instances = []
        hours = None
        for doc in documents:
            quote = doc.get("quote", {})
            if quote:
                quote_name = quote.get("name")
            else:
                quote_name = ""
            if get_hours:
                hours = cls._get_hours_from_document(doc)
            d = {
                "customer": doc.get("customer", {}).get("name"),
                "name": doc.get("name"),
                "quote": quote_name,
                "author_name": doc.get("author_name"),
                "hours": hours,
                "id": doc.get("id"),
                "created_on": doc.get("created_on"),
            }
            instances.append(d)
        return instances

    @classmethod
    def get_dashboard_data(cls, provider, params=None):
        """
        Get list of documents along with quote data
        Args:
            provider: Provider object
            params: parameters to be passed to Document query

        Returns: List of documents

        """
        params = params or {}
        documents = DocumentService.list_document(
            provider, params, show_closed=True
        )
        return documents

    @classmethod
    def get_last_week_documents(cls, provider):
        """
        Get list of documents created last week (Mon-Sun)
        Args:
            provider: Provider Object

        Returns: List of documents

        """
        today = datetime.today()
        last_sunday = today - dt.timedelta(days=today.isoweekday())
        last_week_monday = last_sunday - dt.timedelta(days=6)
        params = {
            "created_on__date__gte": last_week_monday,
            "created_on__date__lte": last_sunday,
        }

        documents = cls.get_dashboard_data(provider, params)
        return documents

    @classmethod
    def get_service_win_rate(cls, provider, n_days=90):
        """
        Get the last n_days days, pull SoW’s created and then pull SoW’s WON during that same time period
        Args:
            n_days:
            provider: Provider Object

        Returns: dict of the form
        {
            "value": 14.29,
            "hover_text": "Calculated based on SoW’s created vs SoW’s Won during the last 60 Days.",
            "label": "Services WIN Rate",
            "unit": "%",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          }

        """

        documents = cls.get_dashboard_data(provider)
        if documents:
            today = datetime.today()
            last_n_days = today - dt.timedelta(days=n_days)
            updated_last_n_days = list(
                filter(
                    lambda doc: last_n_days.date()
                    <= convert_date_string_to_date_object(
                        doc["updated_on"], "%Y-%m-%dT%H:%M:%S+0000"
                    ),
                    documents,
                )
            )
            total_created = len(updated_last_n_days)
            total_won = cls.get_sow_won(documents).get("value", 0)
            if total_created:
                percent = round((total_won * 100) / total_created, 2)
            else:
                percent = 0
        else:
            percent = 0
        result = dict(
            value=percent,
            hover_text=f"Calculated based on SoW’s created/updated vs SoW’s Won during the last {n_days} Days.",
            label="Services WIN Rate",
            unit="%",
            bg_color="#c6efce",
            text_color="#006000",
        )
        return result

    @classmethod
    def get_document_created(cls, documents, n_days=90):
        """
        For the previous week count the amount of SoW’s created
        Args:
            provider: Provider Object

        Returns: dict of the form
        {
            "value": 14.29,
            "hover_text": "Calculated based on SoW’s created vs SoW’s Won during the last 60 Days.",
            "label": "Services WIN Rate",
            "unit": "%",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          }

        """
        documents_count = 0
        instances = []
        if documents:
            today = datetime.today()
            last_n_days = today - dt.timedelta(days=n_days)
            created_last_n_days = list(
                filter(
                    lambda doc: last_n_days.date()
                    <= convert_date_string_to_date_object(
                        doc["created_on"], "%Y-%m-%dT%H:%M:%S+0000"
                    ),
                    documents,
                )
            )
            documents_count = len(created_last_n_days)
            instances = cls._get_document_response(created_last_n_days)
        result = dict(
            value=documents_count,
            hover_text=f"SoW’s Created in last {n_days} days.",
            label="SoW’s Created",
            bg_color="#c6efce",
            text_color="#006000",
            documents=instances,
        )
        return result

    @classmethod
    def get_sow_won(cls, documents, n_days=90):
        """
         For the previous week count the amount of SoW’s WON
        Args:
            provider: Provider Object

        Returns: dict of the form
        {
            "value": 14.29,
            "hover_text": "Calculated based on SoW’s created vs SoW’s Won during the last 60 Days.",
            "label": "Services WIN Rate",
            "unit": "%",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          }

        """
        total_won = 0
        instances = []
        if documents:
            today = datetime.today()
            last_n_days = today - dt.timedelta(days=n_days)
            won_last_n_days = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"].lower()
                    in [DocumentService.WON.lower()]
                    and last_n_days.date()
                    <= convert_date_string_to_date_object(
                        doc["quote"]["closed_date"], settings.ISO_FORMAT
                    ),
                    documents,
                )
            )
            total_won = len(won_last_n_days)
            instances = cls._get_document_response(won_last_n_days)
        result = dict(
            value=total_won,
            hover_text=f"SoW’s Won in last {n_days} days.",
            label="SoW’s Won",
            bg_color="#c6efce",
            text_color="#006000",
            documents=instances,
        )
        return result

    @classmethod
    def get_sow_lost(cls, documents, n_days=90):
        """
        For the previous week count the amount of SoW’s Closed, no decision, lost
        Args:
            provider: Provider Object

        Returns: dict of the form
        {
            "value": 14.29,
            "hover_text": "Calculated based on SoW’s created vs SoW’s Won during the last 60 Days.",
            "label": "Services WIN Rate",
            "unit": "%",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          }

        """
        total_lost = 0
        instances = []
        if documents:
            today = datetime.today()
            last_n_days = today - dt.timedelta(days=n_days)
            lost_documents = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"].lower()
                    in DocumentService.CLOSED_STATUSES,
                    documents,
                )
            )
            for doc in lost_documents:
                if not doc.get("quote", {}).get("closed_date"):
                    doc["quote"]["closed_date"] = doc["quote"].get(
                        "expected_close_date"
                    )
            lost_documents = list(
                filter(
                    lambda doc: last_n_days.date()
                    <= convert_date_string_to_date_object(
                        doc["quote"]["closed_date"], settings.ISO_FORMAT
                    ),
                    lost_documents,
                )
            )
            total_lost = len(lost_documents)
            instances = cls._get_document_response(lost_documents)
        result = dict(
            value=total_lost,
            hover_text=f"SoW’s Lost in last {n_days} days.",
            label="SoW’s Lost",
            bg_color="#c6efce",
            text_color="#006000",
            documents=instances,
        )
        return result

    @classmethod
    def calculate_engineering_hours_open(cls, documents):
        """
        Pull all open document quotes where the close date is either past due or close date is within the
        next 60 days. If the document is T&M pull the estimated hours form T&M hours area
        if the document is fixed fee pull the hours from fixed fee.
        Args:
            documents: list of all documents

        Returns: Integer

        """
        next_60_days_date = datetime.today() + timedelta(days=60)
        total_hours = 0
        instances = []
        if documents:
            documents = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"] in [DocumentService.OPEN]
                    and convert_date_string_to_date_object(
                        doc["quote"]["expected_close_date"],
                        settings.ISO_FORMAT,
                    )
                    < next_60_days_date.date(),
                    documents,
                )
            )
            for document in documents:
                total_hours += cls._get_hours_from_document(document)
            instances = cls._get_document_response(documents)
        return round(total_hours, 2), instances

    @classmethod
    def get_engineering_hours_open(cls, documents):
        """
        Get Engineering hours open dashboard
        Args:
            documents: list of documents

        Returns: Dict of the form
        {
          "value": 124.3,
          "hover_text": "Open SOW Hours delivered to customers.",
          "label": "Engineering Hours Open",
          "bg_color": "#ffeb9c",
          "text_color": "#995200"
        }

        """
        total_hours, instances = cls.calculate_engineering_hours_open(
            documents
        )
        result = dict(
            value=total_hours,
            hover_text="Open SOW Hours delivered to customers.",
            label="Engineering Hours Open",
            bg_color="#ffeb9c",
            text_color="#995200",
            documents=instances,
        )
        return result

    @classmethod
    def calculate_avg_project_hours(cls, documents, reference_date):
        """
        Pull all open document quotes where the close date is either past due or close date is within the
        next 60 days. If the document is T&M pull the estimated hours form T&M hours area if the document is
        fixed fee pull the hours from fixed fee. These should be added up then divided by the Open OPP count.
        So Average Project Hours = (TOTAL-PROJECT-HOURS/OPEN-PROJECT-COUNT)
        Args:
            documents: list of all documents

        Returns: Numeric value

        """
        next_60_days_date = reference_date + timedelta(days=60)
        value = 0
        if documents:
            total_hours = 0
            documents = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"] in [DocumentService.OPEN]
                    and convert_date_string_to_date_object(
                        doc["quote"]["expected_close_date"],
                        settings.ISO_FORMAT,
                    )
                    < next_60_days_date.date(),
                    documents,
                )
            )
            for document in documents:
                total_hours += cls._get_hours_from_document(document)
            if documents:
                value = total_hours / len(documents)
        return round(value, 2)

    @classmethod
    def get_average_project_hours_dashboard(cls, documents):
        """
        Get Average project hours dashboard
        Args:
            documents: list of documents

        Returns: Dict of the form
        {
          "value": 31.075,
          "hover_text": "Average project hours are calculated based on total open hours / open projects count",
          "label": "Average Project Hours",
          "bg_color": "#ffeb9c",
          "text_color": "#995200"
        }

        """
        current_date = datetime.today()
        value = cls.calculate_avg_project_hours(documents, current_date)
        result = dict(
            value=value,
            hover_text="Average project hours are calculated based on total open hours / open projects count",
            label="Average Project Hours",
            bg_color="#ffeb9c",
            text_color="#995200",
        )
        return result

    @classmethod
    def _calculate_engineering_hours_won(
        cls, documents, reference_date, by_week=False
    ):
        total_hours = 0
        instances = []
        if documents:
            current_date = reference_date
            if by_week:
                first_date = reference_date - dt.timedelta(days=6)
            else:
                current_quarter = round((current_date.month - 1) // 3 + 1)
                first_date = datetime(
                    current_date.year, 3 * current_quarter - 2, 1
                )
            won_this_quarter = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"] in [DocumentService.WON]
                    and first_date.date()
                    <= convert_date_string_to_date_object(
                        doc["quote"]["closed_date"], settings.ISO_FORMAT
                    )
                    <= reference_date.date(),
                    documents,
                )
            )
            for document in won_this_quarter:
                total_hours += cls._get_hours_from_document(document)
            instances = cls._get_document_response(won_this_quarter)
        return round(total_hours, 2), instances

    @classmethod
    def get_engineering_hours_won(cls, documents):
        """
        Pull the list of WON Document Opportunities for the current quarter and add up the engineering hours.

        Args:
            documents:

        Returns:

        """
        total_hours = 0
        instances = []
        if documents:
            current_date = datetime.now()
            total_hours, instances = cls._calculate_engineering_hours_won(
                documents, current_date
            )

        result = dict(
            value=total_hours,
            hover_text="Won Service hours this quarter.",
            label="Engineering Hours Won",
            bg_color="#c6efce",
            text_color="#006000",
            documents=instances,
        )
        return result

    @classmethod
    def get_engineering_hours_won_per_category(cls, documents):
        """
        Get engineering hours won per category
        Args:
            documents: list of documents

        Returns:

        """
        result_by_category = {}
        if documents:
            current_date = datetime.now()
            current_quarter = round((current_date.month - 1) // 3 + 1)
            first_date = datetime(
                current_date.year, 3 * current_quarter - 2, 1
            )
            won_this_quarter = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"] in [DocumentService.WON]
                    and first_date.date()
                    <= convert_date_string_to_date_object(
                        doc["quote"]["closed_date"], settings.ISO_FORMAT
                    )
                    <= current_date.date(),
                    documents,
                )
            )
            won_this_quarter = group_list_of_dictionary_by_key(
                won_this_quarter, "category_name"
            )
            result_by_category = {}
            for category, documents in won_this_quarter.items():
                total_hours = 0
                for document in documents:
                    total_hours += cls._get_hours_from_document(document)
                instances = cls._get_document_response(documents)
                result_by_category[category] = dict(
                    total_hours=total_hours, instances=instances
                )
        result = []
        for category, value in result_by_category.items():
            _res = dict(
                value=value.get("total_hours"),
                hover_text=f"Won {category} Service hours this quarter.",
                label=f"{category} Hours Won",
                bg_color="#c6efce",
                text_color="#006000",
                documents=value.get("instances"),
            )
            result.append(_res)
        return result

    @classmethod
    def calculate_win_rate(cls, all_documents, provider, reference_date):
        """
        Average the last 8 weeks of SoW’s Delivered and Average the last 8 weeks of SoW’s WON. Then based on
        those two averages
        WIN RATE = (AVG-SOW-WON)/(AVG-SOW-DELIVERED)
        Args:
            all_documents: list of documents
            provider: Provider Object

        Returns:

        """
        # last 8 weeks
        created_on = reference_date - timedelta(days=56)
        params = dict(created_on__gte=created_on)
        documents_created = cls.get_dashboard_data(provider, params)
        if not documents_created:
            return 0
        documents_won = list(
            filter(
                lambda doc: doc["quote"]
                and doc["quote"]["status_name"] in [DocumentService.WON]
                and created_on.date()
                <= convert_date_string_to_date_object(
                    doc["quote"]["closed_date"], settings.ISO_FORMAT
                ),
                all_documents,
            )
        )
        win_rate = len(documents_won) / len(documents_created)
        return win_rate

    @classmethod
    def calculate_expected_engineering_hours(
        cls, all_documents, reference_date, provider
    ):
        next_60_days_date = reference_date + timedelta(days=60)
        value = 0
        if all_documents:
            win_rate = cls.calculate_win_rate(
                all_documents, provider, reference_date
            )
            avg_project_hours = cls.calculate_avg_project_hours(
                all_documents, reference_date
            )
            all_documents = list(
                filter(
                    lambda doc: doc["quote"]
                    and doc["quote"]["status_name"] in [DocumentService.OPEN]
                    and convert_date_string_to_date_object(
                        doc["quote"]["expected_close_date"],
                        settings.ISO_FORMAT,
                    )
                    < next_60_days_date.date(),
                    all_documents,
                )
            )
            open_opp_count = len(all_documents)
            value = round(open_opp_count * win_rate * avg_project_hours, 2)
        return value

    @classmethod
    def get_historical_expected_engineering_hours_data(
        cls, timestamp, provider
    ):
        """
        Get historical expected engineering hours data from DB.
        """

        value = 0
        record = DashboardMetricData.objects.filter(
            provider=provider,
            metric=DashboardMetricData.EXPECTED_ENGINEERING_HOURS,
            timestamp__date=timestamp.date(),
        ).first()
        if record:
            value = record.value
        return value

    @classmethod
    def get_expected_hours(cls, documents, provider):
        current_date = datetime.today()
        value = cls.calculate_expected_engineering_hours(
            documents, current_date, provider
        )
        result = dict(
            value=value,
            hover_text="Expected hours calculated from open opportunities * WIN rate * Average project hours."
            " We are pulling opportunities 60 days out",
            label="Expected Hours",
            bg_color="#ffeb9c",
            text_color="#995200",
        )
        return result

    @classmethod
    def get_services_activity_dashboard(cls, provider):
        """
        Get the service activity dashboard
        Args:
            provider: Provider Object

        Returns: list of dict of the form
        [
          {
            "value": 14.29,
            "hover_text": "Calculated based on SoW’s created vs SoW’s Won during the last 90 Days.",
            "label": "Services WIN Rate",
            "unit": "%",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          },
          {
            "value": 1,
            "hover_text": "SoW’s Created last week.",
            "label": "SoW’s Created",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          },
          {
            "value": 0.0,
            "hover_text": "SoW’s Won last week.",
            "label": "SoW’s Won",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          },
          {
            "value": 0.0,
            "hover_text": "SoW’s Lost last week.",
            "label": "SoW’s Lost",
            "bg_color": "#c6efce",
            "text_color": "#006000"
          }
        ]

        """
        documents = cls.get_dashboard_data(provider, {})
        service_activity_dashboard = [
            cls.get_service_win_rate(provider),
            cls.get_document_created(documents),
            cls.get_sow_won(documents),
            cls.get_sow_lost(documents),
        ]
        pipeline_dashboard = [
            cls.get_engineering_hours_open(documents),
            cls.get_expected_hours(documents, provider),
            cls.get_average_project_hours_dashboard(documents),
        ]
        booked_service_mix = [cls.get_engineering_hours_won(documents)]
        booked_service_mix.extend(
            cls.get_engineering_hours_won_per_category(documents)
        )
        dashboard = OrderedDict(
            {
                "Service Activity": service_activity_dashboard,
                "Pipeline": pipeline_dashboard,
                "Booked Service Mix": booked_service_mix,
            }
        )
        return dashboard

    @classmethod
    def _calculate_document_count_by_week(cls, documents, date_key):
        if not documents:
            return pd.DataFrame()
        df = pd.DataFrame(documents, columns=["id", date_key])
        df[date_key] = pd.to_datetime(df[date_key])
        df.set_index(df[date_key], inplace=True)
        df.drop(date_key, axis=1, inplace=True)
        # documents created by week
        res_df = df.resample("W").count()
        res_df.reset_index(inplace=True)
        res_df.rename(
            columns={"id": "value", date_key: "timeperiod"}, inplace=True
        )
        res_df.timeperiod = res_df.timeperiod.dt.date
        res_df.sort_values(["timeperiod"])
        return res_df

    @classmethod
    def _calculate_created_and_won_service_history(cls, provider, no_of_weeks):
        all_documents = cls.get_dashboard_data(provider, {})
        today = datetime.today()
        last_sunday = today - dt.timedelta(days=today.isoweekday())
        # documents created before current week
        documents_created_before_current_week = list(
            filter(
                lambda doc: dateutil.parser.parse(doc["created_on"]).date()
                <= last_sunday.date(),
                all_documents,
            )
        )
        won_before_current_week_documents = list(
            filter(
                lambda doc: doc["quote"]
                and doc["quote"]["status_name"] in [DocumentService.WON]
                and convert_date_string_to_date_object(
                    doc["quote"]["closed_date"], settings.ISO_FORMAT
                )
                <= last_sunday.date(),
                all_documents,
            )
        )
        for doc in won_before_current_week_documents:
            doc["closed_date"] = doc["quote"]["closed_date"]
        created_documents_summary_by_week_df = (
            cls._calculate_document_count_by_week(
                documents_created_before_current_week, "created_on"
            )
        )
        created_documents_summary_by_week_df.rename(
            columns={"value": "created_documents_summary_by_week"},
            inplace=True,
        )
        won_documents_summary_by_week_df = (
            cls._calculate_document_count_by_week(
                won_before_current_week_documents, "closed_date"
            )
        )
        won_documents_summary_by_week_df.rename(
            columns={"value": "won_documents_summary_by_week"}, inplace=True
        )
        try:
            result_df = pd.merge(
                created_documents_summary_by_week_df,
                won_documents_summary_by_week_df,
                how="outer",
            )
        except pd.errors.MergeError:
            if not created_documents_summary_by_week_df.empty:
                result_df = created_documents_summary_by_week_df
                result_df["won_documents_summary_by_week"] = 0
            elif not won_documents_summary_by_week_df.empty:
                result_df = won_documents_summary_by_week_df
                result_df["created_documents_summary_by_week_df"] = 0
            else:
                labels = [
                    last_sunday - dt.timedelta(days=d)
                    for d in range(no_of_weeks * 7)
                ]
                created_sows = [0] * len(labels)
                won_sows = [0] * len(labels)
                _data = dict(
                    created_documents_summary_by_week=created_sows,
                    won_documents_summary_by_week=won_sows,
                )
                result_df = pd.DataFrame(_data, index=labels)
                result_df = result_df.resample("W").sum()
                result_df["timeperiod"] = list(result_df.index)
        result_df.fillna(0, inplace=True)
        result_df = result_df.tail(no_of_weeks)
        result = dict(
            label=result_df.timeperiod.apply(
                lambda x: x.strftime(settings.MM_DD_YYYY_FORMAT)
            ),
            created_documents_summary_by_week=result_df.created_documents_summary_by_week,
            won_documents_summary_by_week=result_df.won_documents_summary_by_week,
        )
        return result

    @classmethod
    def get_created_and_won_service_history(cls, provider):
        result = cls._calculate_created_and_won_service_history(provider, 8)
        dashboard = {
            "labels": result.get("label"),
            "datasets": [
                {
                    "label": "SoW's Created",
                    "fill": False,
                    "lineTension": 0,
                    "backgroundColor": "#ed7c31",
                    "borderColor": "#ed7c31",
                    "borderCapStyle": "round",
                    "borderDash": [],
                    "borderDashOffset": 0.0,
                    "borderJoinStyle": "round",
                    "pointBorderColor": "#ed7c31",
                    "pointBackgroundColor": "#fff",
                    "pointBorderWidth": 8,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#ed7c31",
                    "pointHoverBorderColor": "#ed7c31",
                    "pointHoverBorderWidth": 3,
                    "pointRadius": 3,
                    "pointHitRadius": 1,
                    "data": result.get("created_documents_summary_by_week"),
                    "borderWidth": 4,
                },
                {
                    "label": "SoW's Won",
                    "fill": False,
                    "lineTension": 0,
                    "backgroundColor": "#4471c4",
                    "borderColor": "#4471c4",
                    "borderCapStyle": "round",
                    "borderDash": [],
                    "borderDashOffset": 0.0,
                    "borderJoinStyle": "round",
                    "pointBorderColor": "#4471c4",
                    "pointBackgroundColor": "#fff",
                    "pointBorderWidth": 8,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#4471c4",
                    "pointHoverBorderColor": "#4471c4",
                    "pointHoverBorderWidth": 3,
                    "pointRadius": 3,
                    "pointHitRadius": 1,
                    "data": result.get("won_documents_summary_by_week"),
                    "borderWidth": 4,
                },
            ],
        }
        return dashboard

    @classmethod
    def _calculate_expected_vs_won_hours_historical(
        cls, provider, no_of_weeks
    ):
        today = datetime.today()
        last_sunday = today - dt.timedelta(days=today.isoweekday())
        last_n_week_dates = [
            last_sunday - dt.timedelta(days=d) for d in range(no_of_weeks * 7)
        ]
        df = pd.DataFrame(index=last_n_week_dates)
        tdf = df.resample("W").count()
        week_dates = list(tdf.index)
        all_documents = cls.get_dashboard_data(provider, {})
        expected_hours, won_hours = [], []
        for week_date in week_dates:
            expected_hour = cls.get_historical_expected_engineering_hours_data(
                week_date, provider
            )
            won_hour, _ = cls._calculate_engineering_hours_won(
                all_documents, week_date, by_week=True
            )
            expected_hours.append(expected_hour)
            won_hours.append(won_hour)
        result = dict(
            label=list(tdf.index.strftime(settings.MM_DD_YYYY_FORMAT)),
            expected_hours=expected_hours,
            won_hours=won_hours,
        )
        return result

    @classmethod
    def get_expected_vs_won_hours_historical(cls, provider):
        result = cls._calculate_expected_vs_won_hours_historical(provider, 8)
        dashboard = {
            "labels": result["label"],
            "datasets": [
                {
                    "label": "Expected Hours",
                    "fill": False,
                    "lineTension": 0,
                    "backgroundColor": "#ffc000",
                    "borderColor": "#ffc000",
                    "borderCapStyle": "round",
                    "borderDash": [],
                    "borderDashOffset": 0.0,
                    "borderJoinStyle": "round",
                    "pointBorderColor": "#ffc000",
                    "pointBackgroundColor": "#fff",
                    "pointBorderWidth": 8,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#ffc000",
                    "pointHoverBorderColor": "#ffc000",
                    "pointHoverBorderWidth": 3,
                    "pointRadius": 3,
                    "pointHitRadius": 1,
                    "data": result["expected_hours"],
                    "borderWidth": 4,
                },
                {
                    "label": "Won Hours",
                    "fill": False,
                    "lineTension": 0,
                    "backgroundColor": "#a5a5a5",
                    "borderColor": "#a5a5a5",
                    "borderCapStyle": "round",
                    "borderDash": [],
                    "borderDashOffset": 0.0,
                    "borderJoinStyle": "round",
                    "pointBorderColor": "#a5a5a5",
                    "pointBackgroundColor": "#fff",
                    "pointBorderWidth": 8,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#a5a5a5",
                    "pointHoverBorderColor": "#a5a5a5",
                    "pointHoverBorderWidth": 3,
                    "pointRadius": 3,
                    "pointHitRadius": 1,
                    "data": result["won_hours"],
                    "borderWidth": 4,
                },
            ],
        }
        return dashboard

    @classmethod
    @caching(
        cache_key=CacheService.CACHE_KEYS.SOW_DASHBOARD_RAW_DATA_TABLE,
        cache_type=CacheService.CACHE_TYPE.PROVIDER,
    )
    def get_raw_data(cls, provider):
        no_of_weeks = 52
        expected_vs_won_df = pd.DataFrame(
            cls._calculate_expected_vs_won_hours_historical(
                provider, no_of_weeks
            )
        )
        created_vs_won_df = pd.DataFrame(
            cls._calculate_created_and_won_service_history(
                provider, no_of_weeks
            )
        )
        expected_vs_won_df["label"] = pd.to_datetime(
            expected_vs_won_df["label"]
        ).dt.date
        created_vs_won_df["label"] = pd.to_datetime(
            created_vs_won_df["label"]
        ).dt.date
        result_df = pd.merge(
            expected_vs_won_df, created_vs_won_df, how="outer", on="label"
        )
        result_df.fillna(0, inplace=True)
        result_df.sort_values(by=["label"], inplace=True, ascending=False)
        dates = list(
            result_df.label.apply(
                lambda x: x.strftime(settings.MM_DD_YYYY_FORMAT)
            )
        )
        sow_created = list(result_df.created_documents_summary_by_week)
        sow_won = list(result_df.won_documents_summary_by_week)
        expected_hours = list(result_df.expected_hours)
        won_hours = list(result_df.won_hours)
        result = []
        for pos, _date in enumerate(dates):
            _sow_created = sow_created[pos]
            _sow_won = sow_won[pos]
            _expected_hours = expected_hours[pos]
            _won_hours = won_hours[pos]
            if any([_sow_created, _sow_won, _expected_hours, _won_hours]):
                _res = dict(
                    date=_date,
                    sow_created=_sow_created,
                    sow_won=_sow_won,
                    expected_hours=_expected_hours,
                    won_hours=_won_hours,
                )
                result.append(_res)
        return result

    @classmethod
    def export_raw_data(cls, provider):
        data = cls.get_raw_data(provider)
        df = pd.DataFrame(data)
        df.rename(
            columns={
                "date": "Date",
                "sow_created": "SOW Created",
                "sow_won": "SOW Won",
                "expected_hours": "Expected Hours",
                "won_hours": "Won Hours",
            },
            inplace=True,
        )
        records = df.to_dict("records")
        return records
