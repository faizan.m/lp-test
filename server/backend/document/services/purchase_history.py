from core.integrations import Connectwise
from core.integrations.erp.connectwise.connectwise import CWFieldsFilter
import asyncio
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from core.integrations.utils import prepare_response
from core.integrations.device_info.cisco.field_mappings import (
    PurchaseOrderHistoryData,
)
from datetime import datetime
from document.models import ConnectwisePurchaseOrderData, FinanceData, InvoiceData


class PurchaseOrderHistoryService:
    """
    Get all the purchase order for a customer
    with the required fields.
    Fetch line items for each PO number in async manner
    At the end adding purchase order date for each PO.
    """

    @classmethod
    async def get_purchase_orders(cls, provider, customer_crm_id, **kwargs):
        client: Connectwise = provider.erp_client
        required_fields = [
            "id",
            "customerCompany",
            "poDate",
            "poNumber",
            "vendorInvoiceNumber",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        purchase_orders = client.get_open_purchase_order_list_for_customer(
            customer_crm_id, filters=[fields_filter]
        )
        with ThreadPoolExecutor(max_workers=10) as executor:
            required_fields = [
                "id",
                "product/identifier",
                "quantity",
                "description",
                "receivedStatus",
                "purchaseOrderId",
            ]
            if kwargs.get("export"):
                export_required_fields = [
                    "receivedQuantity",
                    "dateReceived",
                    "serialNumbers",
                    "shipDate",
                    "shipmentMethod/name",
                    "trackingNumber",
                ]
                required_fields.extend(export_required_fields)
            fields_filter = CWFieldsFilter(required_fields)
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    partial(
                        client.get_purchase_order_line_items,
                        purchase_order.get("id"),
                        None,
                        include_closed=True,
                        fields_filter=[fields_filter],
                        **kwargs
                    ),
                )
                for purchase_order in purchase_orders
            ]
            response = await asyncio.gather(*tasks)
        line_items = []
        for purchase_order in purchase_orders:
            for line_item in response:
                for i in range(len(line_item)):
                    if purchase_order.get("id") == line_item[i].get(
                        "purchaseOrderId"
                    ):
                        line_item[i][
                            "purchaseOrderNumber"
                        ] = purchase_order.get("poNumber")
                        # fetch customer po
                        # customer_po_number = (
                        #     ConnectwisePurchaseOrderData.objects.filter(
                        #         po_number=purchase_order.get("poNumber")
                        #     )
                        #     .select_related("sales_order")
                        #     .values_list(
                        #         "sales_order__customer_po_number", flat=True
                        #     )
                        # )
                        so_details_po_number = (
                            ConnectwisePurchaseOrderData.objects.filter(
                                po_number=purchase_order.get("poNumber")
                            )
                            .select_related("sales_order")
                            .values(
                                "sales_order__customer_po_number",
                                "sales_order__crm_id",
                                "sales_order__opportunity_name",
                                "sales_order__opportunity_crm_id_id",
                                "sales_order__opportunity_crm_id__ticket_crm_id",
                                "sales_order__opportunity_crm_id__summary",
                                "sales_order__invoice_ids"
                            )
                        )
                        if so_details_po_number:
                            line_item[i][
                                "customer_po_number"
                            ] = so_details_po_number[0].get(
                                "sales_order__customer_po_number"
                            )
                            line_item[i][
                                "sales_order_crm_id"
                            ] = so_details_po_number[0].get(
                                "sales_order__crm_id"
                            )
                            line_item[i][
                                "opportunity_name"
                            ] = so_details_po_number[0].get(
                                "sales_order__opportunity_name"
                            )
                            line_item[i][
                                "opportunity_crm_id"
                            ] = so_details_po_number[0].get(
                                "sales_order__opportunity_crm_id_id"
                            )
                            line_item[i][
                                "ticket_crm_id"
                            ] = so_details_po_number[0].get(
                                "sales_order__opportunity_crm_id__ticket_crm_id"
                            )
                            line_item[i][
                                "ticket_summary"
                            ] = so_details_po_number[0].get(
                                "sales_order__opportunity_crm_id__summary"
                            )
                            # fetch invoice details from sales orders
                            if so_details_po_number[0].get(
                                "sales_order__invoice_ids"
                            ):
                                invoice_ids = so_details_po_number[0].get(
                                "sales_order__invoice_ids"
                                )
                                invoice_number = InvoiceData.objects.filter(
                                    id__in=invoice_ids).values_list("number", flat=True)
                                line_item[i][
                                    "vendorInvoiceNumber"
                                ] = invoice_number
                        else:
                            line_item[i]["customer_po_number"] = None
                            line_item[i]["sales_order_crm_id"] = None
                            line_item[i]["opportunity_name"] = None
                            line_item[i]["opportunity_crm_id"] = None
                            line_item[i]["ticket_crm_id"] = None
                            line_item[i]["ticket_summary"] = None
                            line_item[i]["vendorInvoiceNumber"] = None

                        line_item[i]["purchaseOrderDate"] = purchase_order.get(
                            "poDate"
                        )
                        # line_item[i][
                        #     "vendorInvoiceNumber"
                        # ] = purchase_order.get("vendorInvoiceNumber")
                        if (
                            line_item[i].get("receivedStatus")
                            == "FullyReceived"
                        ):
                            line_item[i]["received"] = "Yes"
                        else:
                            line_item[i]["received"] = "No"
                        line_item[i].pop("receivedStatus")
                        line_items.append(line_item[i])
        return line_items

    @classmethod
    def get_purchase_order_details(
        cls, provider, purchase_order_id, line_item_id, po_number
    ):
        required_fields = [
            "id",
            "product/identifier",
            "quantity",
            "receivedQuantity",
            "description",
            "receivedStatus",
            "dateReceived",
            "purchaseOrderId",
            "serialNumbers",
            "shipDate",
            "shipmentMethod",
            "trackingNumber",
        ]
        fields_filter = CWFieldsFilter(required_fields)
        client: Connectwise = provider.erp_client
        line_item = client.get_purchase_order_line_item(
            purchase_order_id, line_item_id, filters=[fields_filter]
        )
        # fetch customer po
        customer_po_number = (
            ConnectwisePurchaseOrderData.objects.filter(po_number=po_number)
            .select_related("sales_order")
            .values_list("sales_order__customer_po_number", flat=True)
        )
        if customer_po_number:
            line_item["customer_po_number"] = customer_po_number[0]
        else:
            line_item["customer_po_number"] = None
        if line_item.get("receivedStatus") == "FullyReceived":
            line_item["received"] = "Yes"
        else:
            line_item["received"] = "No"
        line_item.pop("receivedStatus", "")
        return line_item

    @classmethod
    def export_purchase_orders(cls, provider, customer_crm_id, **kwargs):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            cls.get_purchase_orders(
                provider, customer_crm_id, export=True, **kwargs
            )
        )
        line_items = loop.run_until_complete(future)
        for line_item in line_items:
            if line_item.get("purchaseOrderDate"):
                purchase_order_date = datetime.strptime(
                    line_item.get("purchaseOrderDate"), "%Y-%m-%dT%H:%M:%SZ"
                )
                line_item["purchaseOrderDate"] = datetime.strftime(
                    purchase_order_date, "%m/%d/%Y"
                )
            if line_item.get("dateReceived"):
                received_date = datetime.strptime(
                    line_item.get("dateReceived"), "%Y-%m-%dT%H:%M:%SZ"
                )
                line_item["dateReceived"] = datetime.strftime(
                    received_date, "%m/%d/%Y"
                )
            if line_item.get("shipDate"):
                shipped_date = datetime.strptime(
                    line_item.get("shipDate"), "%Y-%m-%dT%H:%M:%SZ"
                )
                line_item["shipDate"] = datetime.strftime(
                    shipped_date, "%m/%d/%Y"
                )
        line_items = prepare_response(line_items, PurchaseOrderHistoryData)
        return line_items
