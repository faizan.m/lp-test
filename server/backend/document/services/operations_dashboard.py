import re
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from typing import Any, Dict, Generator, List, Optional, Tuple

from django.db import IntegrityError
from django.utils import timezone
from requests import Response
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from accounts.models import Provider, User
from core.integrations import Connectwise, Ingram
from core.integrations.erp.connectwise import field_mappings
from core.integrations.erp.connectwise.connectwise import CWFieldsFilter
from core.integrations.utils import prepare_response
from core.utils import chunks_of_list_with_max_item_count
from document.models import (
    ConnectwisePurchaseOrder,
    ConnectwisePurchaseOrderData,
    ConnectwiseServiceTicket,
    IngramPurchaseOrderData,
    OperationDashboardSettings,
    ConnectwisePurchaseOrderLineitemsData,
    SalesPurchaseOrderData,
    ConnectwiseOpportunityData,
)
from document.utils import add_custom_field_to_payload
from utils import DbOperation, get_app_logger
from service_requests.services import ServiceRequestService
import asyncio
from core.integrations.erp.connectwise.connectwise import (
    CWFieldsFilter,
    CWConditions,
    Operators,
    Condition,
    PatchGroup,
)

logger = get_app_logger(__name__)

# customer_po_status = {
#     "cw_status": [
#         "Order Placed",
#         "Waiting on Smartnet",
#         "closed",
#         "Not Billed",
#     ],
#     "acela_status": ["In Progress", "Ship Complete", "Delivered", "Order Placed"],
# }


class ServiceTicketCreateSeriaizer(serializers.Serializer):
    ticket_crm_id = serializers.IntegerField()
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    summary = serializers.CharField(max_length=500)
    po_numbers = serializers.ListField(
        child=serializers.CharField(max_length=200), min_length=1
    )
    status_crm_id = serializers.IntegerField(required=True)
    status_name = serializers.CharField(max_length=300)
    company_crm_id = serializers.IntegerField(required=True)
    company_name = serializers.CharField(max_length=500)
    owner_crm_id = serializers.IntegerField(allow_null=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), allow_null=True, required=False
    )
    service_board_crm_id = serializers.IntegerField(required=True)
    connectwise_last_data_update = serializers.DateTimeField()
    last_ticket_note = serializers.CharField(allow_null=True, allow_blank=True)
    opportunity_name = serializers.CharField(
        max_length=500, required=False, allow_null=True, allow_blank=True
    )
    opportunity_crm_id = serializers.IntegerField(
        required=False, allow_null=True
    )

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=ConnectwiseServiceTicket.objects.all(),
                fields=("provider", "ticket_crm_id"),
            )
        ]


class ServiceTicketUpdateSerializer(ServiceTicketCreateSeriaizer):
    ticket_crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("provider", "ticket_crm_id")


class PurchaseOrderCreateSeriaizer(serializers.Serializer):
    crm_id = serializers.IntegerField()
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    po_number = serializers.PrimaryKeyRelatedField(
        queryset=ConnectwisePurchaseOrder.objects.all()
    )
    customer_company_id = serializers.IntegerField(
        required=False, allow_null=True
    )
    customer_company_name = serializers.CharField(
        max_length=500, required=False, allow_blank=True, allow_null=True
    )
    vendor_company_id = serializers.IntegerField(required=True)
    vendor_company_name = serializers.CharField(max_length=500)
    purchase_order_status_id = serializers.IntegerField(
        required=False, allow_null=True
    )
    purchase_order_status_name = serializers.CharField(
        max_length=50, allow_null=True, allow_blank=True
    )
    internal_notes = serializers.CharField(
        required=False, allow_null=True, allow_blank=True
    )
    po_date = serializers.DateTimeField(allow_null=True)
    site_id = serializers.IntegerField(required=False, allow_null=True)
    site_name = serializers.CharField(
        max_length=500, required=False, allow_blank=True, allow_null=True
    )
    sales_order = serializers.PrimaryKeyRelatedField(
        queryset=SalesPurchaseOrderData.objects.all(),
        allow_null=True,
        required=False,
    )
    location_id = serializers.IntegerField(required=False, allow_null=True)
    business_unit = serializers.IntegerField(required=False, allow_null=True)
    shipment_date = serializers.DateTimeField(allow_null=True, required=False)
    total = serializers.FloatField(allow_null=True, required=False)
    entered_by = serializers.CharField(
        max_length=200, required=False, allow_blank=True, allow_null=True
    )
    vendor_invoice_number = serializers.CharField(
        max_length=200, required=False, allow_blank=True, allow_null=True
    )
    shipping_instruction = serializers.CharField(
        max_length=1000, required=False, allow_null=True, allow_blank=True
    )
    custom_field_present = serializers.JSONField(required=False)

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=ConnectwisePurchaseOrderData.objects.all(),
                fields=("provider", "crm_id"),
            )
        ]


class PurchaseOrderUpdateSerializer(PurchaseOrderCreateSeriaizer):
    crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("provider", "crm_id")


class PurchaseOrderLineItemCreateSeriaizer(serializers.Serializer):
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    po_number = serializers.PrimaryKeyRelatedField(
        queryset=ConnectwisePurchaseOrder.objects.all()
    )
    crm_id = serializers.IntegerField()
    description = serializers.CharField(max_length=500)
    line_number = serializers.IntegerField()
    product_crm_id = serializers.IntegerField()
    product_id = serializers.CharField(max_length=100)
    purchase_order_crm_id = serializers.IntegerField()
    ordered_quantity = serializers.IntegerField()
    received_quantity = serializers.IntegerField(
        allow_null=True, required=False
    )
    serial_numbers = serializers.ListField(
        child=serializers.CharField(max_length=100), allow_empty=True
    )
    ship_date = serializers.DateTimeField(allow_null=True, required=False)
    shipping_id = serializers.IntegerField(allow_null=True, required=False)
    shipping_method = serializers.CharField(allow_null=True, required=False)
    received_date = serializers.DateTimeField(allow_null=True, required=False)
    received_status = serializers.CharField(allow_null=True, required=False)
    last_updated_in_cw = serializers.DateTimeField()
    tracking_numbers = serializers.ListField(
        child=serializers.CharField(max_length=100, allow_blank=True),
        allow_null=True,
        allow_empty=True,
    )
    is_closed = serializers.BooleanField()
    unit_cost = serializers.FloatField(allow_null=True, required=False)
    expected_ship_date = serializers.DateTimeField(
        allow_null=True, required=False
    )
    vendor_order_number = serializers.CharField(
        allow_null=True, required=False, allow_blank=True
    )
    is_cancelled = serializers.BooleanField(required=False)
    unit_of_measure = serializers.CharField(allow_null=True, required=False)
    internal_notes = serializers.CharField(
        allow_blank=True, allow_null=True, required=False
    )
    warehouse_bin_name = serializers.CharField(allow_null=True, required=False)


class PurchaseOrderLineItemUpdateSerializer(
    PurchaseOrderLineItemCreateSeriaizer
):
    crm_id = None
    provider = None

    class Meta:
        read_only_fields = ("provider", "crm_id")


class IngramPurchaseOrderCreateSerializer(serializers.Serializer):
    provider = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all()
    )
    po_number = serializers.PrimaryKeyRelatedField(
        queryset=ConnectwisePurchaseOrder.objects.all()
    )
    ingram_order_number = serializers.CharField(max_length=100)
    status = serializers.CharField(max_length=500)
    shipping_address = serializers.JSONField()
    vendor_part_number = serializers.CharField(max_length=100)
    shipped_quantity = serializers.IntegerField(
        required=False, allow_null=True
    )
    eta = serializers.DateField(allow_null=True)
    tracking_numbers = serializers.ListField(allow_empty=True)
    serial_numbers = serializers.ListField(
        child=serializers.CharField(max_length=100), allow_empty=True
    )
    carrier_name = serializers.CharField(allow_null=True, max_length=200)
    shipped_date = serializers.DateField(allow_null=True)
    ingram_suborder_number = serializers.CharField(max_length=100)
    ingram_order_line_number = serializers.CharField(max_length=100)
    additional_data = serializers.JSONField(default={})

    def validate_tracking_numbers(self, tracking_numbers):
        TRACKING_NUMBERS_MAX_LENGTH = 100
        new_tracking_numbers = []
        for tracking_number in tracking_numbers:
            if len(tracking_number) > TRACKING_NUMBERS_MAX_LENGTH:
                try:
                    _space_separated_tracking_numbers = tracking_number.split()
                    new_tracking_numbers.extend(
                        _space_separated_tracking_numbers
                    )
                except Exception:
                    pass
            else:
                new_tracking_numbers.append(tracking_number)
        return new_tracking_numbers


class IngramPurchaseOrderUpdateSerializer(IngramPurchaseOrderCreateSerializer):
    provider = None
    po_number = None
    ingram_order_number = None
    ingram_suborder_number = None
    ingram_order_line_number = None

    class Meta:
        read_only_fields = (
            "provider",
            "po_number",
            "ingram_order_number",
            "ingram_suborder_number",
            "ingram_order_line_number",
        )


class ConnectwiseOpportunityDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConnectwiseOpportunityData
        fields = (
            "provider",
            "name",
            "crm_id",
            "status_crm_id",
            "status_name",
            "stage_crm_id",
            "stage_name",
            "customer_po",
            "customer_name",
            "customer_crm_id",
            "closed_date",
            "last_crm_update",
        )
        read_only_fields = ("provider",)


class OperationsDashboardQueryService:
    def __init__(self, provider: Provider):
        self.provider = provider

    def check_operations_dashboard_settings(self):
        """
        This method checks if all the required settings under OperationDashboardSettings are completed.

        """
        message = (
            "Provider required Operations Dashboard settings not configured."
        )
        configured = True
        try:
            dashboard_settings: OperationDashboardSettings = (
                OperationDashboardSettings.objects.get(provider=self.provider)
            )
        except OperationDashboardSettings.DoesNotExist as e:
            logger.info(e)
            configured = False
            return configured, message
        if not dashboard_settings.business_unit_id:
            configured = False
            message = "Business Unit Id not set"
            return configured, message
        if not dashboard_settings.service_boards:
            configured = False
            message = "Service Boards not selected."
            return configured, message
        if not dashboard_settings.ticket_status_ids:
            configured = False
            message = "Ticket statuses not selected."
            return configured, message
        message = "Operations Dashboard settings completed."
        return configured, message

    def get_operations_dashboard_settings(
        self,
    ) -> Optional[OperationDashboardSettings]:
        try:
            dashboard_settings: OperationDashboardSettings = (
                OperationDashboardSettings.objects.get(provider=self.provider)
            )
        except OperationDashboardSettings.DoesNotExist as e:
            logger.info(
                f"Provider required Operations Dashboard settings not configured.{e}"
            )
            return None
        return dashboard_settings

    def update_ticket_by_crm_id(
        self, ticket_crm_id: int, payload: Dict
    ) -> object:
        """
        Updates the ticket instance where crm_id is ticket_crm_id.
        Sets the new attributes using payload.
        Args:
            ticket_crm_id: CRM id of the ticket
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        ticket_query = ConnectwiseServiceTicket.objects.filter(
            provider=self.provider, ticket_crm_id=ticket_crm_id
        )
        ticket_query.update(**payload)
        return ticket_query

    def create_ticket(self, payload: Dict):
        try:
            ticket = ConnectwiseServiceTicket.objects.create(**payload)
        except IntegrityError as e:
            logger.info(e)
            return None
        return ticket

    def create_purchase_order(
        self, ticket: ConnectwiseServiceTicket, po_number: str
    ) -> ConnectwisePurchaseOrder:
        """
        Create a ConnectwisePurchaseOrder entry.
        :param ticket_id: PK of `ConnectwiseServiceTicket`
        :param po_number: Purchase order PoNumber
        """
        try:
            purchase_order = ConnectwisePurchaseOrder.objects.create(
                po_number=po_number, ticket=ticket
            )
        except IntegrityError as e:
            logger.info(e)
            return None
        return purchase_order

    @staticmethod
    def delete_tickets(filter_conditions: Dict) -> object:
        """
        Delete tickets filtered using the filter conditions.
        Args:
            filter_conditions: A dict of ConnectwiseServiceTicket model fields as key and
            value of the key.

        """
        result = ConnectwiseServiceTicket.objects.filter(
            **filter_conditions
        ).delete()
        return result

    def create_purchase_order_data(self, purchase_order_payload: Dict):
        try:
            purchase_order = ConnectwisePurchaseOrderData.objects.create(
                **purchase_order_payload
            )
        except IntegrityError as e:
            logger.error(f"Error creating PO: {e}")
            return
        return purchase_order

    def update_purchase_order_by_crm_id(
        self, po_crm_id: int, payload: Dict
    ) -> object:
        """
        Updates the purchase order data instance where crm_id is po crm_id.
        Sets the new attributes using payload.
        Args:
            po_crm_id: CRM id of the Purchase Order
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        purchase_order = ConnectwisePurchaseOrderData.objects.filter(
            provider=self.provider, crm_id=po_crm_id
        )
        purchase_order.update(**payload)
        return purchase_order

    @staticmethod
    def delete_purchase_orders(filter_conditions: Dict) -> object:
        """
        Delete purchase order data filtered using the filter conditions.
        Args:
            filter_conditions: A dict of ConnectwisePurchaseOrderData model fields as key and
            value of the key.

        """
        result = ConnectwisePurchaseOrderData.objects.filter(
            **filter_conditions
        ).delete()
        return result

    def create_po_line_item_data(self, bulk_payload: List):
        purchase_order = (
            ConnectwisePurchaseOrderLineitemsData.objects.bulk_create(
                bulk_payload
            )
        )
        return purchase_order

    def update_po_line_item_data(
        self, po_line_item_crm_id: int, line_number: int, payload: Dict
    ) -> object:
        """
        Updates the purchase order data instance where crm_id is po crm_id.
        Sets the new attributes using payload.
        Args:
            po_line_item_crm_id: CRM id of the Purchase Order Line Item
            line_number: line number of line item
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        purchase_order = ConnectwisePurchaseOrderLineitemsData.objects.filter(
            provider=self.provider,
            crm_id=po_line_item_crm_id,
            line_number=line_number,
        )
        purchase_order.update(**payload)
        return purchase_order

    @staticmethod
    def delete_po_line_item_data(filter_conditions: Dict) -> object:
        """
        Delete purchase order line item data filtered using the filter conditions.
        Args:
            filter_conditions: A dict of ConnectwisePurchaseOrderLineitemsData model fields as key and
            value of the key.

        """
        result = ConnectwisePurchaseOrderLineitemsData.objects.filter(
            **filter_conditions
        ).delete()
        return result

    def create_ingram_purchase_order_data(self, bulk_payload: List):
        ingram_purchase_order = IngramPurchaseOrderData.objects.bulk_create(
            bulk_payload
        )
        return ingram_purchase_order

    def update_ingram_purchase_order_data(
        self,
        po_number: str,
        ingram_suborder_number: str,
        ingram_order_line_number: str,
        payload: Dict,
    ) -> object:
        """
        Updates the purchase order data instance where po_number is Ingram customer number,
        ingram_suborder_number is Ingram suborder number and
        ingram_order_line_number is the line number of suborder
        Sets the new attributes using payload.
        Args:
            po_number: Customer Purchase Order Number
            ingram_suborder_number: Ingram suborder number
            ingram_order_line_number:  Ingram order line number
            payload: Updated payload

        """
        # We are using .update() method so the auto_now property
        # set for updated_on field will not work and needs to be handled
        # explicitly.
        payload.update({"updated_on": timezone.now()})
        purchase_order = IngramPurchaseOrderData.objects.filter(
            provider=self.provider,
            po_number=po_number,
            ingram_suborder_number=ingram_suborder_number,
            ingram_order_line_number=ingram_order_line_number,
        )
        purchase_order.update(**payload)
        return purchase_order

    @staticmethod
    def delete_ingram_purchase_orders(filter_conditions: Dict) -> object:
        """
        Delete purchase order data filtered using the filter conditions.
        Args:
            filter_conditions: A dict of IngramPurchaseOrderData model fields as key and
            value of the key.

        """
        result = IngramPurchaseOrderData.objects.filter(
            **filter_conditions
        ).delete()
        return result

    @staticmethod
    def get_purchase_orders_not_present_in_db(
        purchase_order_numbers: List[str],
    ) -> List[str]:
        """
        Get purchase orders from given purchase orders that are yet to be created in DB.

        Args:
            purchase_order_numbers: List of purchase order numbers

        Returns:
            Purchase orders
        """
        purchase_orders_in_db: Set[str] = set(
            ConnectwisePurchaseOrder.objects.filter(
                po_number__in=purchase_order_numbers,
            ).values_list("po_number", flat=True)
        )
        purchase_orders_not_in_db: Set[str] = (
            set(purchase_order_numbers) - purchase_orders_in_db
        )
        return list(purchase_orders_not_in_db)

    def create_service_ticket_to_purchase_order_mapping(
        self,
        ticket: ConnectwiseServiceTicket,
        purchase_order_numbers: List[str],
    ) -> None:
        """
        Maps multiple purchase orders to a service ticket in ConnectwisePurchaseOrder model.

        Args:
            ticket: ConnectwiseServiceTicket object
            purchase_order_numbers: List of purchase order numbers

        Returns:
            None
        """
        for purchase_order_number in purchase_order_numbers:
            self.create_purchase_order(ticket, purchase_order_number)
        return


class ConnectwiseServiceTicketPayloadHandler:
    MIN_PO_NUMBER_LENGTH = 11
    MAX_PO_NUMBER_LENGTH = 15

    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on Project
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        If the actions is deleted, payload accepts a set of crm Id's of the deleted
        projects.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = OperationsDashboardQueryService(provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        self.payload = prepare_response(
            self.payload, field_mappings.ServiceTicket
        )

    @staticmethod
    def derive_po_number(summary: str) -> List[str]:
        """
        Method extracts PO Numbers from ticket summary. A regex is being used to match the PO Number pattern.
        Works even if there are multiple PO Numbers in the summary.
        The regex has been tested for the following cases.

        LP10600-FTC: SW - FLEX Calling
        LP10653-INP- HW - Cisco UCS Seed
        LP10696-KLE- HW - Surface Laptop 3s
        LP10700-DRS, LP10700-DRS-B- - HW - Driscolls Cat9200 EOY Request
        LP10712-CAD, LP10711-CAD- HW - Cadence UCS Non Standard 12/9/19 Updated
        LP10716-RST-RPL, LP10716-RST- HW - 9200
        LP10757-CRC- SW - Crocs New Campus vmWare
        LP10032-BTM: WR -  Meraki Licensing Renewal
        LP10037-MCG: WR - SMARTnet Renewal HW
        LP12622B-LMR - SW - ThousandEyes Modification (+250 Units)
        LP13801A-RVN LP13801B-RVN LP13801C-RVN: HW - Rivian Network Infra Rohini 1.24.23
        LP13681A-PAR LP13681B-PAR: HW - Network Cables for UCS

        Used Regex Explanation `r"^[A-Z\d].*[A-Z]{0,1}-[A-Z]{3}-*[A-Z]*"`

        ^ asserts position at start of a line
        Match a single character present in the list below [A-Z\d]
        A-Z matches a single character in the range between A (index 65) and Z (index 90) (case sensitive)
        \d matches a digit (equivalent to [0-9])
        .
        matches any character (except for line terminators)
        * matches the previous token between zero and unlimited times, as many times as possible,
        giving back as needed (greedy)
        [A-Z]Match a single character present in the list below [A-Z]
        {0,1} matches the previous token between zero and one times
        - matches the character - with index 4510 (2D16 or 558) literally (case sensitive)
        Match a single character present in the list below [A-Z]
        {3} matches the previous token exactly 3 times
        A-Z matches a single character in the range between A (index 65) and Z (index 90) (case sensitive)
        -
        matches the character - with index 4510 (2D16 or 558) literally (case sensitive)
        Match a single character present in the list below [A-Z]
        * matches the previous token between zero and unlimited times, as many times as possible,
        giving back as needed (greedy)
        A-Z matches a single character in the range between A (index 65) and Z (index 90) (case sensitive)


        """
        if not summary:
            return []
        # pattern = r"^[A-Z\d].*-[A-Z]{3}-*[A-Z]*"
        validated_po_numbers: List = []
        # This captures any PO number
        po_regex: str = r"([A-Z]+\d+[A-Z]{0,1}-[A-Z]{3}(-[A-Z]+)*)"

        # This captures purchase order in summary.
        # The summary must start with PO which are seperated by ", " and PO are
        # seperated from description by "- "
        po_regex_for_summary: str = (
            r"\b(([A-Z]+\d+[A-Z]{0,1}-[A-Z]{3}(-[A-Z]*)*,?\s?)*)\b"
        )

        match = re.search(po_regex_for_summary, summary)
        if match:
            po_numbers = re.findall(po_regex, match.group())
            po_numbers: List[str] = [po[0] for po in po_numbers]
            for po in po_numbers:
                po_len = len(po)
                if (
                    po_len
                    <= ConnectwiseServiceTicketPayloadHandler.MAX_PO_NUMBER_LENGTH
                ):
                    validated_po_numbers.append(po)
        return validated_po_numbers

    def get_provider_user_by_system_member_crm_id(
        self, crm_id: int
    ) -> Optional[User]:
        """
        Searches the Provider users on the customer_user_instance_id field and returns the
        UUID of the found user. If no User is found returns None.
        Args:
            crm_id: User CRM Id
        """
        try:
            user = User.providers.get(
                provider=self.provider,
                profile__system_member_crm_id=crm_id,
            ).id
        except User.DoesNotExist:
            return None
        return user

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        po_numbers = self.derive_po_number(self.payload.get("summary"))
        owner_crm_id = self.payload.get("owner_id")
        transformed_payload = dict(
            ticket_crm_id=self.payload.get("id"),
            summary=self.payload.get("summary"),
            po_numbers=po_numbers,
            status_crm_id=self.payload.get("status_id"),
            status_name=self.payload.get("status"),
            company_crm_id=self.payload.get("company_id"),
            company_name=self.payload.get("company_name"),
            owner_crm_id=owner_crm_id,
            service_board_crm_id=self.payload.get("board_id"),
            connectwise_last_data_update=self.payload.get("last_updated_on"),
            provider=self.provider.id,
            last_ticket_note=self.payload.get("ticket_note"),
            opportunity_name=self.payload.get("opportunity_name"),
            opportunity_crm_id=self.payload.get("opportunity_id"),
        )
        if owner_crm_id:
            owner = self.get_provider_user_by_system_member_crm_id(
                owner_crm_id
            )
            if owner:
                transformed_payload["owner"] = owner
        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = ServiceTicketCreateSeriaizer
        if self.action == DbOperation.UPDATE:
            serializer = ServiceTicketUpdateSerializer
        ticket_serializer = serializer(data=self.payload)
        is_valid = ticket_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = ticket_serializer.errors
            return is_valid, errors
        self.payload = ticket_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        purchase_orders: List[str] = self.payload.pop("po_numbers", [])
        if self.action == DbOperation.UPDATE:
            ticket_present = ConnectwiseServiceTicket.objects.filter(
                provider=self.provider, ticket_crm_id=crm_id
            ).exists()
            if ticket_present:
                try:
                    updated_ticket: "QuerySet[ConnectwiseServiceTicket]" = (
                        self.query_service.update_ticket_by_crm_id(
                            crm_id, self.payload
                        )
                    )
                    # This handles the edge case when some PO are created during ticket creation, but
                    # not all PO are successfully captured from summary and synced in DB.
                    purchase_orders_to_create: List[
                        str
                    ] = self.query_service.get_purchase_orders_not_present_in_db(
                        purchase_orders
                    )
                    if purchase_orders_to_create:
                        self.query_service.create_service_ticket_to_purchase_order_mapping(
                            updated_ticket[0], purchase_orders_to_create
                        )
                except IntegrityError as e:
                    logger.info(e)
            else:
                # This handles the case when the update action is called but the project is
                # present. It creates the project. Use in cases when the create action failed for the
                # object.
                self.payload.update(
                    dict(provider_id=self.provider.id, ticket_crm_id=crm_id)
                )
                self.query_service.create_ticket(self.payload)
        elif self.action == DbOperation.CREATE:
            ticket = self.query_service.create_ticket(self.payload)
            if ticket:
                self.query_service.create_service_ticket_to_purchase_order_mapping(
                    ticket, purchase_orders
                )
        else:
            filter_conditions = dict(
                provider=self.provider,
                ticket_crm_id__in=self.payload.get("id", []),
            )
            self.query_service.delete_tickets(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("id")
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class ConnectwisePurchaseOrdersDataPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on ConnectwisePurchaseOrderData
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = OperationsDashboardQueryService(provider)

    def extract_payload(self, **params):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        po_custom_field_id = params.get("po_custom_field_setting", None)
        self.payload = add_custom_field_to_payload(
            self.payload, po_custom_field_id
        )
        self.payload = prepare_response(
            self.payload, field_mappings.PurchaseOrder
        )

    def transform_payload(self, **params):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        self.payload.update({"provider": self.provider.id})
        # If PO custom field setting is configured, add id of SalesPurchaseOrderData to payload
        settings_id = params.get("po_custom_field_setting", None)
        po_custom_field_id = self.payload.get("custom_field_id", None)
        sales_order_crm_id = self.payload.get("sales_order_id", None)
        # sales_order_crm_id value should be numeric
        if sales_order_crm_id:
            if (
                isinstance(sales_order_crm_id, str)
                and sales_order_crm_id.isnumeric()
            ):
                sales_order_crm_id = int(sales_order_crm_id)
            elif isinstance(sales_order_crm_id, int):
                sales_order_crm_id = sales_order_crm_id
            else:
                sales_order_crm_id = None
        else:
            self.payload.update(
                {
                    "custom_field_present": {
                        "custom_field_present": False,
                        "reason": "Custom Field has no value of sales order in CW",
                    }
                }
            )
        sales_order = None
        if (
            settings_id
            and po_custom_field_id
            and sales_order_crm_id
            and (settings_id == po_custom_field_id)
        ):
            try:
                sales_order = SalesPurchaseOrderData.objects.get(
                    provider_id=self.provider.id, crm_id=sales_order_crm_id
                ).id
                self.payload.update(
                    {
                        "custom_field_present": {
                            "custom_field_present": True,
                            "reason": "Custom Field has value of sales order in CW and sales order is present in Acela",
                        }
                    }
                )
            except SalesPurchaseOrderData.DoesNotExist:
                sales_order = None
                self.payload.update(
                    {
                        "custom_field_present": {
                            "custom_field_present": True,
                            "reason": "Custom Field has value of sales order in CW but sales order is not present in Acela",
                        }
                    }
                )
        self.payload.update({"sales_order": sales_order})
        self.payload.pop("custom_field_id", None)
        self.payload.pop("sales_order_id", None)
        return self.payload

    def validate_payload(self, **params) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = PurchaseOrderCreateSeriaizer
        if self.action == DbOperation.UPDATE:
            serializer = PurchaseOrderUpdateSerializer
        purchase_orders_serializer = serializer(data=self.payload)
        is_valid = purchase_orders_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = purchase_orders_serializer.errors
            return is_valid, errors
        self.payload = purchase_orders_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None):
        if self.action == DbOperation.UPDATE:
            po_present = ConnectwisePurchaseOrderData.objects.filter(
                provider=self.provider, crm_id=crm_id
            ).exists()
            if po_present:
                self.query_service.update_purchase_order_by_crm_id(
                    crm_id, self.payload
                )
            else:
                # This handles the case when the update action is called but the project is
                # present. It creates the project. Use in cases when the create action failed for the
                # object.
                self.query_service.create_purchase_order_data(self.payload)
        elif self.action == DbOperation.CREATE:
            self.query_service.create_purchase_order_data(self.payload)
        else:
            filter_conditions = dict(
                provider=self.provider,
                crm_id__in=self.payload.get("crm_ids", []),
            )
            self.query_service.delete_purchase_orders(filter_conditions)

    def process(self, **params):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload(**params)
            crm_id = self.payload.get("crm_id")
            self.payload = self.transform_payload(**params)
            is_valid, errors = self.validate_payload(**params)
            if is_valid:
                self.perform_action(crm_id=crm_id)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class OperationsDashboardService:
    def __init__(self, provider):
        self.provider = provider
        self.query_service = OperationsDashboardQueryService(provider)

    def get_provider_service_tickets_from_erp(
        self,
    ) -> Optional[Generator[Response, None, None]]:
        erp_client: Connectwise = self.provider.erp_client
        dashboard_settings: Optional[
            OperationDashboardSettings
        ] = self.query_service.get_operations_dashboard_settings()
        if not dashboard_settings:
            return None
        tickets_generator = erp_client.get_service_tickets_with_generator(
            boards=dashboard_settings.service_boards,
            created_date=dashboard_settings.service_ticket_create_date
            # statuses=dashboard_settings.ticket_status_ids,
        )
        return tickets_generator

    def get_all_provider_opportunities_from_erp(
        self,
    ) -> Generator[Response, None, None]:
        dashboard_settings: Optional[
            OperationDashboardSettings
        ] = self.query_service.get_operations_dashboard_settings()
        if not dashboard_settings:
            return None
        opportunities: Generator[
            Response, None, None
        ] = self.provider.erp_client.get_all_opportunities(
            created_date=dashboard_settings.service_ticket_create_date
        )
        return opportunities

    def get_provider_purchase_orders_from_erp(
        self, po_numbers_list=None
    ) -> Optional[Generator[Response, None, None]]:
        erp_client: Connectwise = self.provider.erp_client

        purchase_orders_generator = (
            erp_client.get_purchase_order_list_with_generator(po_numbers_list)
        )

        return purchase_orders_generator

    async def get_purchase_orders_line_items_from_erp(
        self, purchase_order_ids=None
    ) -> Optional[Generator[Response, None, None]]:
        with ThreadPoolExecutor(max_workers=10) as executor:
            erp_client: Connectwise = self.provider.erp_client
            required_fields = [
                "id",
                "description",
                "lineNumber",
                "packingSlip",
                "product/id",
                "product/identifier",
                "quantity",
                "purchaseOrderId",
                "receivedQuantity",
                "serialNumbers",
                "shipDate",
                "shipmentMethod/id",
                "shipmentMethod/name",
                "dateReceived",
                "receivedStatus",
                "_info/lastUpdated",
                "trackingNumber",
                "closedFlag",
                "unitCost",
                "expectedShipDate",
                "vendorOrderNumber",
                "unitOfMeasure/name",
                "internalNotes",
                "warehouse/name",
            ]
            fields_filter = CWFieldsFilter(required_fields)
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    partial(
                        erp_client.get_purchase_order_line_items,
                        purchase_order_id,
                        include_closed=True,
                        fields_filter=[fields_filter],
                    ),
                )
                for purchase_order_id in purchase_order_ids
            ]
            purchase_orders_line_item = await asyncio.gather(*tasks)
        return purchase_orders_line_item

    async def fetch_ticket_notes_for_tickets(self, ticket_ids_list):
        """
        Async method to fetch the ticket note for given ticket ids.
        Args:
            ticket_ids_list:

        Returns: Dictionary containing ticket_id mapped to its latest ticket note
        """
        erp_client = self.provider.erp_client
        ticket_notes_dict = {}
        with ThreadPoolExecutor(max_workers=10) as executor:
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    partial(
                        ServiceRequestService.get_ticket_notes,
                        erp_client,
                        ticket_id,
                        external_flag_filter=False,
                        internal_flag_filter=True,
                    ),
                )
                for ticket_id in ticket_ids_list
            ]
            ticket_notes = await asyncio.gather(*tasks)
        # Store the ticket_id along with it's latest updated note in ticket_notes_dict
        for ticket_note in ticket_notes:
            if ticket_note:
                ticket_notes_dict.update(
                    {
                        ticket_note[0]
                        .get("ticketId"): ticket_note[0]
                        .get("text", "")
                    }
                )
            else:
                continue
        return ticket_notes_dict

    def fetch_ticket_notes(self, ticket_ids_list):
        """
        Method to fetch ticket note for given ticket ids.
        Args:
            ticket_ids_list: List of ticket ids

        Returns:
            Dictionary containing ticket_id along with its latest note
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            self.fetch_ticket_notes_for_tickets(ticket_ids_list)
        )
        ticket_notes = loop.run_until_complete(future)
        return ticket_notes

    def get_sales_order_data_from_erp(
        self,
        sales_order_crm_id: int,
        required_fields: Optional[List[Dict]] = None,
    ) -> Dict:
        """
        Get the sales orders data from Connectwise

        Args:
            sales_order_crm_id: Tuple containing sales order CRM ID
            required_fields: Required fields

        Returns:
            Connectwise sales order data for given CRM IDs.
        """
        filters = []
        _required_fields: List[str] = [
            "id",
            "customFields",
        ]
        required_fields: List[str] = (
            required_fields if required_fields else _required_fields
        )
        sales_order_ids_filter = CWConditions(
            int_conditions=[
                Condition(
                    "id",
                    Operators.IN,
                    sales_order_crm_id,
                ),
            ]
        )
        filters.append(sales_order_ids_filter)
        sales_order_cw_data: Dict = (
            self.provider.erp_client.get_sales_order_by_id(
                sales_order_crm_id=sales_order_crm_id,
                fields=required_fields,
                filters=filters,
            )
        )
        return sales_order_cw_data

    def get_purchase_orders_data_from_erp(
        self,
        purchase_order_crm_ids: Tuple[int],
        required_fields: Optional[List[Dict]] = None,
    ):
        """
        Get only the required purchase order data from Connectwise

        Args:
            purchase_order_crm_ids: Tuple containing purchase order CRM ID
            required_fields: Required fields

        Returns:
            Connectwise purchase order data for given CRM IDs.
        """
        filters = []
        _required_fields: List[str] = [
            "id",
            "poNumber",
            "status",
            "customFields",
        ]
        required_fields: List[str] = (
            required_fields if required_fields else _required_fields
        )
        required_fields_filter = CWFieldsFilter(required_fields)
        filters.append(required_fields_filter)
        purchase_order_ids_filter = CWConditions(
            int_conditions=[
                Condition(
                    "id",
                    Operators.IN,
                    purchase_order_crm_ids,
                ),
            ]
        )
        filters.append(purchase_order_ids_filter)
        purchase_orders_CW_data: List[
            Dict
        ] = self.provider.erp_client.get_purchase_order_list(filters=filters)
        return purchase_orders_CW_data

    def link_sales_order_to_purchase_orders_in_cw(
        self,
        sales_order,
        purchase_order_crm_ids,
        purchase_order_linking_errors,
    ):
        sales_orders_cw_data: Dict = self.get_sales_order_data_from_erp(
            sales_order.crm_id
        )
        po_numbers: List[str] = ConnectwisePurchaseOrderData.objects.filter(
            crm_id__in=purchase_order_crm_ids,
            provider_id=sales_order.provider_id,
        ).values_list("po_number_id", flat=True)
        custom_fields: List[Dict] = sales_orders_cw_data.get(
            "customFields", []
        )
        for field in custom_fields:
            field["value"] = ", ".join(po_numbers)
            break

        try:
            sales_order_crm_id: int = sales_orders_cw_data.get("id")
            updated_cw_sales_order: Dict = (
                self.provider.erp_client.update_sales_order_custom_field(
                    sales_order_crm_id, custom_fields
                )
            )
            assert sales_order_crm_id == updated_cw_sales_order.get("id")
        except Exception as exc:
            logger.error(exc)
            purchase_order_linking_errors.append(
                dict(
                    sales_order_crm_id=sales_order.crm_id,
                    po_numbers=po_numbers,
                    error=repr(exc),
                )
            )
        return purchase_order_linking_errors

    def link_purchase_orders_to_sales_order(
        self,
        sales_order: SalesPurchaseOrderData,
        purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]",
        po_custom_field_id: int,
        purchase_order_crm_ids: Tuple[int],
    ):
        """
        Links purchase orders to sales order by first updating the sales order CRM ID value for in
        purchase orders custom field in CW and then links in DB through foreign key.

        Args:
            sales_order: SalesPurchaseOrderData object
            purchase_orders: ConnectwisePurchaseOrderData queryset
            po_custom_field_id: Purchase order custom field ID as configured in Operation Dashboard Setting
            purchase_order_crm_ids: Purchase order CRM IDs

        Returns:
            Details about linked sales order and purchase orders.
        """
        _purchase_order_crm_ids: Tuple[str] = tuple(
            purchase_orders.values_list("crm_id", flat=True)
        )
        purchase_orders_CW_data: List[
            Dict
        ] = self.get_purchase_orders_data_from_erp(_purchase_order_crm_ids)

        linked_purchase_orders: List[Dict] = []
        purchase_order_linking_errors: List[Dict] = []
        for purchase_order in purchase_orders_CW_data:
            purchase_order_crm_id: int = purchase_order["id"]
            po_number: str = purchase_order["poNumber"]
            custom_fields: List[Dict] = purchase_order.get("customFields", [])

            for field in custom_fields:
                if field["id"] == po_custom_field_id:
                    field["value"] = str(sales_order.crm_id)
                    break

            # update custom field value of SO in Purchase order
            try:
                updated_cw_purchase_order: Dict = self.provider.erp_client.update_purchase_order_custom_field(
                    purchase_order_crm_id, custom_fields
                )
                assert purchase_order_crm_id == updated_cw_purchase_order.get(
                    "id"
                )
                assert po_number == updated_cw_purchase_order.get("poNumber")

                ConnectwisePurchaseOrderData.objects.filter(
                    crm_id=updated_cw_purchase_order.get("id"),
                    po_number=updated_cw_purchase_order.get("poNumber"),
                    provider_id=self.provider.id,
                ).update(sales_order=sales_order)
            except Exception as exc:
                logger.error(exc)
                purchase_order_linking_errors.append(
                    dict(
                        purchase_order_crm_id=purchase_order_crm_id,
                        po_number=po_number,
                        error=repr(exc),
                    )
                )
            linked_purchase_orders.append(
                dict(
                    purchase_order_crm_id=purchase_order_crm_id,
                    po_number=po_number,
                )
            )

        # update custom field value of PO in Sales order
        purchase_order_linking_errors = (
            self.link_sales_order_to_purchase_orders_in_cw(
                sales_order,
                purchase_order_crm_ids,
                purchase_order_linking_errors,
            )
        )
        result: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            linked_purchase_orders=linked_purchase_orders,
            linking_errors=purchase_order_linking_errors,
        )
        return result

    def remove_purchase_order_link_from_sales_order(
        self,
        purchase_order_crm_ids: Tuple[int],
        sales_order: SalesPurchaseOrderData,
        purchase_order_crm_ids_to_update_so: Tuple[int],
        po_custom_field_id: int,
    ):
        """
        Deletes the purchase order link to sales order by first deleting the sales order CRM ID
        in purchase order custom field value in CW and then removes the foreign key in DB.

        Args:
            purchase_order_crm_ids: CRM IDs of purchase orders
            sales_order: SalesPurchaseOrderData object
            purchase_order_crm_ids_to_update_so: to update sales order for the PO
            po_custom_field_id: Purchase order custom field ID as configured in Operation Dashboard Setting

        Returns:
            Details about deleted link between sales order and purchase orders.
        """
        purchase_orders_CW_data: List[
            Dict
        ] = self.get_purchase_orders_data_from_erp(purchase_order_crm_ids)

        removed_purchase_order_link: List[Dict] = []
        purchase_order_link_remove_errors: List[Dict] = []
        for purchase_order in purchase_orders_CW_data:
            purchase_order_crm_id: int = purchase_order["id"]
            po_number: str = purchase_order["poNumber"]
            custom_fields: List[Dict] = purchase_order.get("customFields", [])

            for field in custom_fields:
                if field["id"] == po_custom_field_id:
                    field.pop("value", None)
                    break

            try:
                updated_cw_purchase_order: Dict = self.provider.erp_client.update_purchase_order_custom_field(
                    purchase_order_crm_id, custom_fields
                )
                assert purchase_order_crm_id == updated_cw_purchase_order.get(
                    "id"
                )
                ConnectwisePurchaseOrderData.objects.filter(
                    crm_id=purchase_order_crm_id,
                    po_number=po_number,
                    provider_id=self.provider.id,
                ).update(sales_order=None)
            except Exception as exc:
                logger.error(exc)
                purchase_order_link_remove_errors.append(
                    dict(
                        purchase_order_crm_id=purchase_order_crm_id,
                        po_number=po_number,
                        error=repr(exc),
                    )
                )
            removed_purchase_order_link.append(
                dict(
                    purchase_order_crm_id=purchase_order_crm_id,
                    po_number=po_number,
                )
            )

        # update custom field value of PO in Sales order
        ConnectwisePurchaseOrderData.objects.filter()
        purchase_order_link_remove_errors = self.link_sales_order_to_purchase_orders_in_cw(
            sales_order,
            purchase_order_crm_ids_to_update_so,
            purchase_order_linking_errors=purchase_order_link_remove_errors,
        )
        result: Dict = dict(
            sales_order_crm_id=sales_order.crm_id,
            removed_purchase_order_link=removed_purchase_order_link,
            purchase_order_link_remove_errors=purchase_order_link_remove_errors,
        )
        return result


class ConnectwisePurchaseOrdersLineItemsDataPayloadHandler:
    TRACKING_NUMBER_COMMA_SPLITTER: str = ","
    TRACKING_NUMBER_SPACE_SPLITTER: str = " "
    EMPTY_STRING: str = ""

    def __init__(self, provider: Provider, action: DbOperation, payload=None):
        """
        Service class to perform Create, Update and Delete action on ConnectwisePurchaseLineItemOrderData
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        Args:
            action: create/update/delete action
            payload: In case of create/update action, payload is a dict of project data
            In case of delete action, payload should be a set of id's
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.query_service = OperationsDashboardQueryService(provider)

    def extract_payload(self):
        """
        Take out the required fields from the entire payload and return the
        reduced payload.
        Returns: dict

        """
        self.payload = prepare_response(
            self.payload, field_mappings.PurchaseOrderLineItem
        )

    def extract_tracking_numbers_from_string(
        self, tracking_numbers_string: str
    ) -> List[str]:
        """
        Extract tracking number from string by splitting the tracking number string at comma, then
        split the tracking numbers again at space for those tracking numbers that contain spaces.
        E.g. If tracking_numbers_string = "1,2,3 4, 5   6", returns ["1","2","3","4","5","6"]

        Args:
            tracking_numbers_string: String containing tracking numbers separated by commas and spaces

        Returns:
            Tracking numbers list
        """
        final_tracking_numbers_list: List = []
        tracking_numbers_split_at_comma: List = tracking_numbers_string.split(
            self.TRACKING_NUMBER_COMMA_SPLITTER
        )
        for tracking_number in tracking_numbers_split_at_comma:
            if self.TRACKING_NUMBER_SPACE_SPLITTER in tracking_number:
                tracking_numbers_split_at_space: List[
                    str
                ] = tracking_number.split(self.TRACKING_NUMBER_SPACE_SPLITTER)
                for _tracking_number in tracking_numbers_split_at_space:
                    if _tracking_number != self.EMPTY_STRING:
                        final_tracking_numbers_list.append(_tracking_number)
            else:
                final_tracking_numbers_list.append(tracking_number)
        return final_tracking_numbers_list

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        self.payload.update({"provider": self.provider.id})
        if self.payload.get("serial_numbers"):
            serial_numbers_list = self.payload.get("serial_numbers").split(",")
            self.payload.update({"serial_numbers": serial_numbers_list})
        else:
            self.payload.update({"serial_numbers": []})

        if not self.payload.get("po_number"):
            po_numbers_list = ConnectwisePurchaseOrderData.objects.filter(
                provider=self.provider,
                crm_id=self.payload.get("purchase_order_crm_id"),
            ).values_list("po_number", flat=True)
            if po_numbers_list:
                self.payload.update({"po_number": po_numbers_list[0]})

        tracking_numbers: str = self.payload.get("tracking_numbers", None)
        if (not tracking_numbers) or (not tracking_numbers.strip()):
            self.payload.update({"tracking_numbers": []})
        else:
            tracking_numbers_list: List[
                str
            ] = self.extract_tracking_numbers_from_string(tracking_numbers)
            self.payload.update({"tracking_numbers": tracking_numbers_list})
        return self.payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = PurchaseOrderLineItemCreateSeriaizer
        if self.action == DbOperation.UPDATE:
            serializer = PurchaseOrderLineItemUpdateSerializer
        po_line_items_serializer = serializer(data=self.payload)
        is_valid = po_line_items_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = po_line_items_serializer.errors
            return is_valid, errors
        self.payload = po_line_items_serializer.validated_data
        return is_valid, []

    def perform_action(self, crm_id=None, line_number=None):
        if self.action == DbOperation.UPDATE:
            po_line_item_present = (
                ConnectwisePurchaseOrderLineitemsData.objects.filter(
                    provider=self.provider,
                    crm_id=crm_id,
                    line_number=line_number,
                ).exists()
            )
            if po_line_item_present:
                self.query_service.update_po_line_item_data(
                    crm_id, line_number, self.payload
                )
            else:
                # This handles the case when the update action is called but the lineitem is
                # present. It creates the lineitem. Use in cases when the create action failed for the
                # object.
                po_line_item_model_data = [
                    ConnectwisePurchaseOrderLineitemsData(**self.payload)
                ]
                self.query_service.create_po_line_item_data(
                    po_line_item_model_data
                )
        elif self.action == DbOperation.CREATE:
            self.query_service.create_po_line_item_data(
                [ConnectwisePurchaseOrderLineitemsData(**self.payload)]
            )
        else:
            filter_conditions = dict(
                provider=self.provider,
                crm_id__in=self.payload.get("crm_ids", []),
            )
            self.query_service.delete_po_line_item_data(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload()
            crm_id = self.payload.get("crm_id")
            line_number = self.payload.get("line_number")
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(crm_id=crm_id, line_number=line_number)
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return success, errors


class IngramPurchaseOrderHandler:
    def __init__(
        self,
        provider: Provider,
        action: DbOperation,
        payload=None,
        ingrampurchaseorderdata=None,
    ):
        """
        Service class to perform Create, Update and Delete action on IngramPurchaseOrderData
        model. Take different actions based on the type of action.
        If the action is created or updated, the first step is to clean and prepare the
        payload and validate it. If the payload is valid, actual create or update
        action is called. If payload is invalid, no further actions is performed and
        error are returned.
        Args:
            action: create/update action
            payload: In case of create/update action, payload is a dict of project data
        """
        self.provider = provider
        self.action = action
        self.payload = payload
        self.ingrampurchaseorderdata = ingrampurchaseorderdata
        self.query_service = OperationsDashboardQueryService(provider)

    async def get_ingram_purchase_order_from_ingram(
        self, provider, po_numbers_list=None
    ) -> Optional[List[Any]]:
        """
        This method will take POs list and provider then fetch the data from Ingram for each po
        and bind them in a list then return all orders data
        Args:
            provider : provider
            po_numbers_list : list of POs
        """
        ingram_credentials = provider.ingram_api_settings
        ingram_client: Ingram = Ingram(
            ingram_credentials.client_id,
            ingram_credentials.client_secret,
            ingram_credentials.ingram_customer_id,
        )
        if not ingram_client:
            return None

        po_chunk_size = 50
        po_number_chunks = chunks_of_list_with_max_item_count(
            po_numbers_list, po_chunk_size
        )
        orders = []
        for po_number_chunk in po_number_chunks:
            with ThreadPoolExecutor(max_workers=10) as executor:
                loop = asyncio.get_event_loop()
                tasks = [
                    loop.run_in_executor(
                        executor,
                        partial(
                            ingram_client.get_all_orders_by_po_number,
                            po_number,
                        ),
                    )
                    for po_number in po_number_chunk
                ]
                order = await asyncio.gather(*tasks)
                orders.extend(order)
        return orders

    def transform_payload(self):
        """
        Change the payload to the form required to create the object.
        This includes renaming the keys, changing the reference Id's
        Returns:

        """
        shipment = self.payload.get("shipmentdetails", [])
        if shipment:
            carrier = shipment[0].get("carriername")
            ship_date = shipment[0].get("shipmentdate")
            package = shipment[0].get("packagedetails", [])
            tracking_number_list = []
            serial_numbers = []
            if package:
                tracking_number = package[0].get("trackingnumber")
                if tracking_number:
                    tracking_number_list.append(tracking_number)
                serial_number_details = package[0].get(
                    "serialnumberdetails", []
                )
                if serial_number_details:
                    serial_numbers = [
                        item.get("serialNumber")
                        for item in serial_number_details
                        if item.get("serialNumber")
                    ]
                else:
                    serial_numbers = []
        else:
            tracking_number_list = []
            serial_numbers = []
            carrier = "N.A."
            ship_date = "N.A."

        transformed_payload = dict(
            ingram_order_number=self.ingrampurchaseorderdata.get(
                "ingramordernnumber"
            ),
            status=self.payload.get("orderstatus"),
            shipping_address=self.ingrampurchaseorderdata.get("shiptoaddress"),
            po_number=self.ingrampurchaseorderdata.get("customerordernumber"),
            vendor_part_number=self.payload.get("manufacturerpartnumber"),
            shipped_quantity=self.payload.get("confirmedquantity"),
            eta=self.payload.get("promiseddeliverydate"),
            ingram_suborder_number=self.payload.get("subordernumber"),
            ingram_order_line_number=self.payload.get("orderlinenumber"),
            tracking_numbers=tracking_number_list,
            serial_numbers=serial_numbers,
            carrier_name=carrier,
            shipped_date=ship_date,
            provider=self.provider.id,
        )
        return transformed_payload

    def validate_payload(self) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors

        """
        serializer = IngramPurchaseOrderCreateSerializer
        if self.action == DbOperation.UPDATE:
            serializer = IngramPurchaseOrderUpdateSerializer
        ingram_po_data_serializer = serializer(data=self.payload)
        is_valid = ingram_po_data_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors = ingram_po_data_serializer.errors
            return is_valid, errors
        self.payload = ingram_po_data_serializer.validated_data
        return is_valid, []

    def perform_action(
        self,
        po_number=None,
        ingram_suborder_number=None,
        ingram_order_line_number=None,
    ):
        if self.action == DbOperation.UPDATE:
            ingram_order_present = IngramPurchaseOrderData.objects.filter(
                provider=self.provider,
                po_number=po_number,
                ingram_suborder_number=ingram_suborder_number,
                ingram_order_line_number=ingram_order_line_number,
            ).exists()
            if ingram_order_present:
                self.query_service.update_ingram_purchase_order_data(
                    po_number,
                    ingram_suborder_number,
                    ingram_order_line_number,
                    self.payload,
                )
        elif self.action == DbOperation.CREATE:
            self.query_service.create_ingram_purchase_order_data(
                [IngramPurchaseOrderData(**self.payload)]
            )
        else:
            filter_conditions = dict(
                provider=self.provider,
                ingram_suborder_number__in=self.payload.get(
                    "ingram_suborder_number", []
                ),
            )
            self.query_service.delete_ingram_purchase_orders(filter_conditions)

    def process(self):
        success = True
        errors = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            po_number = self.ingrampurchaseorderdata.get("customerordernumber")
            ingram_suborder_number = self.payload.get("subordernumber")
            ingram_order_line_number = self.payload.get("orderlinenumber")
            self.payload = self.transform_payload()
            is_valid, errors = self.validate_payload()
            if is_valid:
                self.perform_action(
                    po_number=po_number,
                    ingram_suborder_number=ingram_suborder_number,
                    ingram_order_line_number=ingram_order_line_number,
                )
            else:
                return is_valid, errors
        else:
            self.perform_action()

        return success, errors


class ConnectwiseOpportunityDataPayloadHandler:
    def __init__(self, provider: Provider, action: DbOperation, payload: Dict):
        self.provider = provider
        self.action = action
        self.payload = payload

    def extract_payload(self, **params) -> None:
        self.payload = prepare_response(
            self.payload, field_mappings.Opportunity
        )

    def transform_payload(self, **params):
        updated_payload: Dict = dict(
            name=self.payload.get("name"),
            crm_id=self.payload.get("id"),
            status_crm_id=self.payload.get("status_id", None),
            status_name=self.payload.get("status_name", None),
            stage_crm_id=self.payload.get("stage_id", None),
            stage_name=self.payload.get("stage_name", None),
            customer_po=self.payload.get("customer_po", None),
            customer_name=self.payload.get("company_name"),
            customer_crm_id=self.payload.get("company_id"),
            closed_date=self.payload.get("closed_date", None),
            last_crm_update=self.payload.get("last_updated", None),
        )
        setattr(self, "payload", updated_payload)
        return self.payload

    def validate_payload(self, **params) -> Tuple[bool, List]:
        """
        Validates the payload. Returns the validation result and errors
        Returns: Tuple of validations result and list of errors
        """
        opportunity_serializer = ConnectwiseOpportunityDataSerializer(
            data=self.payload
        )
        is_valid: bool = opportunity_serializer.is_valid(raise_exception=False)
        if not is_valid:
            errors: List = opportunity_serializer.errors
            return is_valid, errors
        self.payload = opportunity_serializer.validated_data
        return is_valid, []

    def perform_action(self):
        if self.action == DbOperation.UPDATE:
            crm_id: int = self.payload["crm_id"]
            if ConnectwiseOpportunityData.objects.filter(
                provider=self.provider, crm_id=crm_id
            ).exists():
                self.update_opportunity(crm_id, self.payload)
        elif self.action == DbOperation.CREATE:
            self.create_opportunity(self.payload)
        else:
            opp_crm_ids: List[int] = self.payload.get("crm_ids")
            self.delete_opportunities(crm_ids=opp_crm_ids)

    def process(self, **params) -> Tuple[bool, List]:
        is_succcessful: bool = True
        errors: List = []
        if self.action in [DbOperation.CREATE, DbOperation.UPDATE]:
            self.extract_payload(**params)
            self.payload = self.transform_payload(**params)
            is_valid, errors = self.validate_payload(**params)
            if is_valid:
                self.perform_action()
            else:
                return is_valid, errors
        else:
            self.perform_action()
        return is_succcessful, errors

    def create_opportunity(self, payload: Dict) -> ConnectwiseOpportunityData:
        opportunity: ConnectwiseOpportunityData = (
            ConnectwiseOpportunityData.objects.create(
                provider=self.provider, **payload
            )
        )
        return opportunity

    def update_opportunity(
        self, crm_id: int, payload: Dict
    ) -> ConnectwiseOpportunityData:
        payload.update({"updated_on": timezone.now()})
        opportunity = ConnectwiseOpportunityData.objects.filter(
            provider=self.provider, crm_id=crm_id
        )
        opportunity.update(**payload)
        return opportunity

    def delete_opportunities(self, crm_ids: List[int]) -> None:
        ConnectwiseOpportunityData.objects.filter(
            provider=self.provider, crm_id__in=crm_ids
        ).delete()
        return
