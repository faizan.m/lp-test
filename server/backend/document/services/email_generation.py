import datetime as dt
import os
from collections import defaultdict
from typing import Dict, List, Tuple, Optional, Set, Any

import dateutil.parser
import pandas as pd
from bs4 import BeautifulSoup
from django.conf import settings
from django.template.loader import render_to_string

from accounts.models import Provider, User
from core.integrations import Ingram
from core.utils import convert_date_object_to_date_string
from document.models import OperationEmailTemplateSettings
from storage.file_upload import FileUploadService
from core.integrations.erp.connectwise.connectwise import (
    CWFieldsFilter,
    CWConditions,
    Condition,
    Operators,
    partial_received_status,
    FULLY_RECEIVED,
    WAITING,
)
from core.services import ERPClientIntegrationService
from accounts.models import Customer, Provider
from document.models import (
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrderLineitemsData,
    ProductsItemData,
)

NA = "N.A."
STANDARD_CARRIER = "Standard"
OPEN = "Open"
RECEIVED = "Received"
CANCELLED = "Cancelled"

LINE_ITEM_RECEIVED_STATUS_MAPPING = {FULLY_RECEIVED: RECEIVED, WAITING: OPEN}


class EmailTemplateGenerationService:
    OPPORTUNITY_NAME_SEPERATOR = "- "
    SO_EMAIL_GENERATION_TEMPLATE = "so_email_template_generation.html"

    @classmethod
    def _render_email_template(cls, template_name, context):
        TEMPLATE_PATH = os.path.join("order_tracking_emails", template_name)
        html_str = render_to_string(TEMPLATE_PATH, context)
        return html_str

    @classmethod
    def _get_shipment_date_text(cls, shipment_date):
        current_date = dt.datetime.now().date()
        shipment_date = dateutil.parser.parse(shipment_date).date()
        shipment_date_str = convert_date_object_to_date_string(
            shipment_date, "%m/%d/%Y"
        )
        if shipment_date > current_date:
            shipment_date_text = f"Est. Ship Date: {shipment_date_str}"
        elif shipment_date == current_date:
            shipment_date_text = f"Ship Date: {shipment_date_str}"
        # elif shipment_date < current_date:
        #     shipment_date_text = f"Shipped Date: {shipment_date_str}"
        elif shipment_date < current_date:
            shipment_date_text = f"Est. Ship Date: {shipment_date_str}"
        return shipment_date_text

    @classmethod
    def _generate_tracking_url(
        cls, tracking_number, carrier, carrier_tracking_url_mapping
    ):
        carrier_mapping = carrier_tracking_url_mapping.get(carrier)
        if carrier_mapping:
            tracking_url_template = carrier_mapping.get("tracking_url")
            tracking_url_template = tracking_url_template.replace(
                "%tracknum%", tracking_number
            )
            tracking_link = (
                f"<a href={tracking_url_template}>{tracking_number}</a>"
            )
            return tracking_link
        else:
            return tracking_number

    @classmethod
    def _generate_cw_tracking_url(
        cls, tracking_number, shipping_id, carrier_tracking_url_mapping
    ):
        """
        Method to map tracking url with connectwise shipment method
        """
        for carrier_mapping in carrier_tracking_url_mapping:
            if carrier_mapping.get("connectwise_shipper") == shipping_id:
                tracking_url_template = carrier_mapping.get("tracking_url")
                tracking_url_template = tracking_url_template.replace(
                    "%tracknum%", tracking_number
                )
                tracking_link = f'<a target="_blank" href="{tracking_url_template}">{tracking_number}</a>'
                return tracking_link
        return tracking_number

    @classmethod
    def generate_order_tracking_information_email(
        cls,
        provider: Provider,
        po_ids: List,
        customer_contact_first_name: str,
        operation_email_template_settings: OperationEmailTemplateSettings,
        ingram_data: List[Dict],
        customer_id,
        to_contact_id,
    ) -> object:
        from document.services import OrderTrackingReceivingService

        po_line_items = (
            OrderTrackingReceivingService.get_purchase_order_line_items(
                provider, po_ids, include_closed=True
            )
        )
        line_identifiers = defaultdict(int)
        for line_item in po_line_items:
            product = line_item.get("product", {}).get("identifier")
            purchase_qty = int(line_item.get("quantity"))
            line_identifiers[product] += purchase_qty

        (
            contracts,
            subscriptions,
            licences,
            hardware_products,
            snt_template_orders,
            ship_to_address,
        ) = (
            {},
            {},
            {},
            {},
            {},
            {},
        )
        product_category_mapping = (
            operation_email_template_settings.settings.get(
                "product_category_mapping"
            )
        )
        carrier_tracking_url_mapping = (
            operation_email_template_settings.settings.carrier_tracking_url
        )
        part_number_exclusion_mapping = (
            operation_email_template_settings.settings.part_number_exclusion
        )
        contact_type_connectwise_id = (
            operation_email_template_settings.contact_type_connectwise_id
        )
        to_contacts_emails, cc_contacts = cls.get_contact_emails(
            provider,
            customer_id,
            to_contact_id=to_contact_id,
            contact_type_connectwise_id=contact_type_connectwise_id,
        )

        for order in ingram_data:
            ship_to_address = order.get("shiptoaddress", {})
            ship_to_address["addressline1"] = ship_to_address.pop(
                "addressLine1", None
            )
            ship_to_address["addressline2"] = ship_to_address.pop(
                "addressLine2", None
            )
            ship_to_address["postalcode"] = ship_to_address.pop(
                "postalCode", None
            )

            for line in order.get("lines", []):
                part_number: str = line.get("manufacturerpartnumber", "")
                if (
                    part_number in line_identifiers.keys()
                    and part_number.lower()
                    not in part_number_exclusion_mapping
                ):
                    shipped_quantity: int = int(
                        line.get("confirmedquantity", 0)
                    )
                    if any(
                        cls.validate_patterns(
                            part_number, product_category_mapping, "contracts"
                        )
                    ):
                        status = "Contracts Pending"
                        _contract = dict(
                            part_number=part_number, status=status
                        )
                        contracts[part_number] = _contract
                    elif any(
                        cls.validate_patterns(
                            part_number, product_category_mapping, "licences"
                        )
                    ):
                        is_licence, carrier = cls.validate_patterns(
                            part_number, product_category_mapping, "licences"
                        )
                        hw_product = cls._process_hardware(
                            carrier_tracking_url_mapping,
                            line,
                            line_identifiers,
                            part_number,
                            shipped_quantity,
                        )
                        _licence = dict(
                            part_number=part_number,
                            shipped_quantity=shipped_quantity,
                            purchased_quantity=line_identifiers[part_number],
                            carriers=carrier,
                            ship_date=hw_product.get("ship_date"),
                            tracking_numbers=hw_product.get(
                                "tracking_numbers"
                            ),
                            status=hw_product.get("status"),
                        )
                        if part_number in licences:
                            licences[part_number][
                                "shipped_quantity"
                            ] += shipped_quantity
                        else:
                            licences[part_number] = _licence
                    else:
                        hw_product = cls._process_hardware(
                            carrier_tracking_url_mapping,
                            line,
                            line_identifiers,
                            part_number,
                            shipped_quantity,
                        )
                        if part_number in hardware_products:
                            hardware_products[part_number][
                                "shipped_quantity"
                            ] += shipped_quantity
                            if hw_product.get("carriers") == NA:
                                hardware_products[part_number]["carriers"] = NA
                        else:
                            hardware_products[part_number] = hw_product
        context = {
            "hardware_products": [
                {
                    "ship_to_address": ship_to_address,
                    "line_items": list(hardware_products.values()),
                }
            ],
            "licences": [
                {
                    "licences": list(licences.values()),
                }
            ],
            "name": customer_contact_first_name,
            "settings": operation_email_template_settings.settings,
            "cc_contacts": cc_contacts,
            "to_contacts_emails": to_contacts_emails,
        }
        if licences:
            contracts = {"contracts": list(contracts.values())}
            context.get("licences")[0].update(contracts)
        elif hardware_products:
            contracts = {"contracts": list(contracts.values())}
            context.get("hardware_products")[0].update(contracts)
        template_name = "order_tracking_info.html"
        template = cls._render_email_template(template_name, context)
        return template, context

    @classmethod
    def validate_patterns(
        cls, part_number, product_category_mapping, product_type
    ):
        """
        This method matches the pattern of licenses or contracts
        part_number: Product identifier
        product_category_mapping: Category mapping from the settings
        product_type: either licences or contracts
        """
        is_matched = False
        carieer = None
        product_type_mapping = product_category_mapping.get(
            product_type, []
        ).to_dict()
        for pattern in product_type_mapping.get("sub_strings"):
            if part_number.startswith(
                pattern.replace("*", "")
            ) or part_number.endswith(pattern.replace("*", "")):
                is_matched = True
                carieer = product_type_mapping.get("carrier", "")
                break
        return is_matched, carieer

    @classmethod
    def _process_hardware(
        cls,
        carrier_tracking_url_mapping,
        line,
        line_identifiers,
        part_number,
        shipped_quantity,
    ):
        tracking_numbers = []
        carriers = []
        ship_date = ""
        for shipment in line.get("shipmentdetails", []):
            carrier = shipment.get("carriername")
            carriers.append(carrier)
            ship_date = shipment.get("shipmentdate")
            status = shipment.get("statusdescription", "")
            for package in shipment.get("packagedetails", []):
                tracking_number = package.get("trackingnumber")
                if tracking_number not in tracking_numbers:
                    if carrier and tracking_number:
                        tracking_number = cls._generate_tracking_url(
                            tracking_number,
                            carrier,
                            carrier_tracking_url_mapping,
                        )
                    tracking_number = tracking_number or NA
                    if tracking_number not in tracking_numbers:
                        tracking_numbers.append(tracking_number)
        tracking_numbers = ", ".join(tracking_numbers)
        tracking_numbers = tracking_numbers or NA
        carriers = ", ".join(carriers)
        if not ship_date and not carriers:
            carriers = NA
        if ship_date:
            ship_date = cls._get_shipment_date_text(ship_date)
        ship_date = ship_date or NA
        if not status:
            status = "Processing"
        if not tracking_numbers.startswith("<a"):
            if tracking_numbers != NA:
                tracking_numbers = " ".join(set(tracking_numbers.split()))
        hw_product = dict(
            part_number=part_number,
            shipped_quantity=shipped_quantity,
            purchased_quantity=line_identifiers[part_number],
            tracking_numbers=tracking_numbers,
            carriers=carriers,
            ship_date=ship_date,
            status=status,
        )
        return hw_product

    @classmethod
    def extract_tables_data(cls, html_template):
        soup = BeautifulSoup(html_template, features="html5lib")
        tables = soup.find_all("table")
        tables_data = []
        for i, table in enumerate(tables):
            output_rows = []
            for table_row in table.findAll("tr"):
                columns = table_row.findAll("td")
                output_row = []
                for column in columns:
                    output_row.append(column.text.strip())
                output_rows.append(output_row)
            tables_data.append(output_rows)
        return tables_data

    @classmethod
    def extract_tables_to_csv(
        cls, html_template: Optional[str] = None, **kwargs
    ):
        csv_files = []
        if kwargs.get("tables", None):
            tables: List[List] = kwargs.get("tables")
        else:
            tables = cls.extract_tables_data(html_template)
        for table in tables:
            columns = table[0]
            rows = table[1:]
            df = pd.DataFrame(rows, columns=columns)
            records = df.to_dict("record")
            # create csv files
            from reporting.services import CSVCreationService

            file_path, file_name = CSVCreationService.create_csv(
                records, columns
            )
            if file_path is not None:
                if not settings.DEBUG:
                    file_path = FileUploadService.upload_file(file_path)
                csv_files.append(
                    dict(file_path=file_path, file_name=file_name)
                )
            else:
                continue
        return csv_files

    @classmethod
    def get_ingram_api_data(
        cls, provider: Provider, po_numbers: List[str]
    ) -> List[Dict]:
        ingram_credentials = provider.ingram_api_settings
        ingram_client: Ingram = Ingram(
            ingram_credentials.client_id,
            ingram_credentials.client_secret,
            ingram_credentials.ingram_customer_id,
        )
        if not ingram_client:
            return None

        orders = []
        for po_number in po_numbers:
            _orders = ingram_client.get_all_orders_by_po_number(po_number)
            orders.extend(_orders)
        return orders

    @classmethod
    def process(
        cls,
        provider: Provider,
        po_numbers: List[str],
        po_ids: List[int],
        customer_contact_first_name: str,
        customer_id,
        to_contact_id,
    ):

        operation_email_template_settings = (
            provider.operation_email_template_settings
        )
        orders = cls.get_ingram_api_data(provider, po_numbers)
        if not orders:
            return None, None
        (
            template,
            template_data,
        ) = cls.generate_order_tracking_information_email(
            provider,
            po_ids,
            customer_contact_first_name,
            operation_email_template_settings,
            orders,
            customer_id,
            to_contact_id,
        )
        return template, template_data

    @classmethod
    def extract_subject_info_from_opportunity_name(
        cls, opportunity_name: str
    ) -> str:
        """
        Extract subject info from opportunity name

        Args:
            opportunity_name: Opportunity name

        Returns:
            Subject information as required in Email template generation
            E.g. If opportunity name is "HW - Boston DC RMA order" returns "Boston DC RMA order"
        """
        opportunity_name_seperator: str = cls.OPPORTUNITY_NAME_SEPERATOR
        if opportunity_name_seperator in opportunity_name:
            seperator_start_index: int = opportunity_name.find(
                opportunity_name_seperator
            )
            subject_start_index: int = seperator_start_index + len(
                opportunity_name_seperator
            )
            subject_info: str = opportunity_name[subject_start_index:]
            return subject_info
        return opportunity_name

    @staticmethod
    def get_customer_po_from_opportunity(
        provider: Provider, opportunity_id: int, customer_id: int
    ) -> str:
        """
        Get customer po number from opportunity

        Args:
            provider: Provider
            opportunity_id: Opportunity CRM ID
            customer_id: Customer CRM ID

        Returns:
            Customer po number
        """
        filters = []
        required_fields: List[str] = ["id", "name", "company", "customerPO"]
        fields_filter = CWFieldsFilter(required_fields)
        filters.append(fields_filter)
        quote_filters = CWConditions(
            int_conditions=[
                Condition("company/id", Operators.EQUALS, customer_id)
            ]
        )
        filters.append(quote_filters)
        opportunity_data: Dict = (
            provider.erp_client.client.get_company_opportunity(
                opportunity_id, filters=filters
            )
        )
        return opportunity_data.get("customer_po", "-")

    @staticmethod
    def get_shipping_site_information(
        provider: Provider, customer_id: int, shipping_site_id: int, **kwargs
    ) -> Dict:
        """
        Get Sales Order shipping site information

        Args:
            provider: Provider
            customer_id: Customer CRM ID
            shipping_site_id: Sales order shipping site CRM ID

        Returns:
            Shipping details for sales order
        """
        shipping_site: Dict = (
            ERPClientIntegrationService.get_customer_complete_site_addresses(
                provider.id,
                customer_id,
                provider.erp_client,
                [shipping_site_id],
            )
        )
        shipping_details: str = shipping_site.get(shipping_site_id)
        shipping_address: List[str] = shipping_details.split(",")
        # Refer to get_customer_complete_site_addresses() method above for address format
        address_fields: List = [
            "addressline1",
            "addressline2",
            "city",
            "state",
            "postalcode",
            "country",
        ]
        shipping_address: Dict = {
            field: shipping_address[index].strip()
            for index, field in enumerate(address_fields)
        }
        return shipping_address

    @classmethod
    def get_sales_order_details_payload(
        cls, provider: Provider, sales_order: SalesPurchaseOrderData, **kwargs
    ) -> Dict:
        """
        Get Sales order details payload

        Args:
            provider: Provider
            sales_order: SalesPurchaseOrderData object

        Returns:
            Sales order details payload
        """
        email_template_subject: str = (
            cls.extract_subject_info_from_opportunity_name(
                sales_order.opportunity_name
            )
            if sales_order.opportunity_name
            else ""
        )
        customer_crm_id: int = Customer.objects.get(
            id=sales_order.customer_id
        ).crm_id
        customer_po: str = cls.get_customer_po_from_opportunity(
            provider, sales_order.opportunity_crm_id_id, customer_crm_id
        )
        shipping_details: Dict = (
            cls.get_shipping_site_information(
                provider, customer_crm_id, sales_order.shipping_site_crm_id
            )
            if sales_order.shipping_site_crm_id
            else {}
        )
        sales_order_details_payload: Dict = dict(
            subject_opportunity_name=email_template_subject,
            customer_po=customer_po,
            shipping_details=shipping_details,
            customer_crm_id=customer_crm_id,
        )
        return sales_order_details_payload

    @classmethod
    def get_contact_emails(
        cls,
        provider: Provider,
        customer_id: int,
        to_contact_id: Optional[int] = None,
        contact_type_connectwise_id: Optional[int] = None,
        **kwargs,
    ) -> Tuple[List, List]:
        """
        Get to contact and cc contact emails
        Args:
            provider: Provider
            customer_id: Customer CRM ID
            to_contact_id: Contact CRM ID
            contact_type_connectwise_id: CW contact type ID
            **kwargs: keyword arguments

        Returns:
            To contact emails, cc contact emails
        """
        to_contacts_emails: List[str] = (
            list(
                User.objects.filter(crm_id=to_contact_id)
                .order_by("email")
                .values_list("email", flat=True)
                .distinct()
            )
            if to_contact_id
            else []
        )
        cc_contact_emails: List[str] = (
            provider.erp_client.get_customer_users(
                customer_id=customer_id,
                required_fields=[
                    "id",
                    "firstName",
                    "communicationItems",
                    "types",
                ],
                type_filter=contact_type_connectwise_id,
            )
            if contact_type_connectwise_id
            else []
        )
        return to_contacts_emails, cc_contact_emails

    @staticmethod
    def get_carrier_of_hardware_product(
        line_item: ConnectwisePurchaseOrderLineitemsData,
    ) -> str:
        """
        If carrier is Standard, show NA otherwise the shipping method.

        Args:
            line_item: ConnectwisePurchaseOrderLineitemsData object

        Returns:
            Carrier name.
        """
        carrier: str = line_item.shipping_method or ""
        tracking_numbers: List[str] = line_item.tracking_numbers
        if (not tracking_numbers) or (carrier == STANDARD_CARRIER):
            carrier = NA
        return carrier

    @staticmethod
    def get_mapped_line_item_status(received_status: str) -> str:
        """
        Get mapped status of line item received status if mapped else
        return the original status.

        Args:
            received_status: Line item received status

        Returns:
            Line item status
        """
        mapped_status: str = LINE_ITEM_RECEIVED_STATUS_MAPPING.get(
            received_status, received_status
        )
        return mapped_status

    @classmethod
    def get_ship_date_and_status(
        cls,
        line_item: ConnectwisePurchaseOrderLineitemsData,
    ) -> Tuple[str, str]:
        """
        Get ship date and status of line item.

        Args:
            line_item: ConnectwisePurchaseOrderLineitemsData object

        Returns:
            Ship date and status
        """
        if line_item.ship_date is None:
            ship_date: str = "-"
            expected_ship_date: str = (
                convert_date_object_to_date_string(
                    line_item.expected_ship_date, "%m/%d/%Y"
                )
                if line_item.expected_ship_date
                else None
            )
            expected_ship_date: str = (
                "Est. ship date " + expected_ship_date
                if expected_ship_date
                else "-"
            )
            status = expected_ship_date
            return ship_date, status
        else:
            ship_date: str = convert_date_object_to_date_string(
                line_item.ship_date, "%m/%d/%Y"
            )
            status: str = cls.get_mapped_line_item_status(
                line_item.received_status
            )
            return ship_date, status

    @classmethod
    def get_template_context(
        cls,
        sales_order: SalesPurchaseOrderData,
        purchase_orders: "Queryset[ConnectwisePurchaseOrderData]",
        operation_email_template_settings: OperationEmailTemplateSettings,
        **kwargs,
    ):
        """
        Get template context such as hardware products, licences, shipping details, etc.

        Args:
            sales_order: SalesPurchaseOrderData
            purchase_orders: ConnectwisePurchaseOrderData queryset
            operation_email_template_settings: OperationEmailTemplateSettings
            **kwargs: kwyword arguments

        Returns:
            Template context
        """
        provider: Provider = sales_order.provider
        provider_id: int = provider.id
        purchase_order_crm_ids: List[int] = list(
            purchase_orders.values_list("crm_id", flat=True)
        )
        product_ids: List[str] = list(
            ProductsItemData.objects.filter(
                provider_id=provider_id,
                sales_order_id=sales_order.id,
                cost__gt=0,
            ).values_list("product_id", flat=True)
        )
        po_line_items: "QuerySet[ConnectwisePurchaseOrderLineitemsData]" = (
            ConnectwisePurchaseOrderLineitemsData.objects.filter(
                provider_id=provider_id,
                product_id__in=product_ids,
                purchase_order_crm_id__in=purchase_order_crm_ids,
                unit_cost__gt=0,
            )
        )
        product_category_mapping = (
            operation_email_template_settings.settings.get(
                "product_category_mapping"
            )
        )
        (
            contracts,
            licences,
            hardware_products,
        ) = ([], [], [])

        for line_item in po_line_items:
            part_number: str = line_item.product_id
            if (
                part_number.lower()
                not in operation_email_template_settings.settings.get(
                    "part_number_exclusion", []
                )
            ):
                shipped_quantity: int = line_item.received_quantity
                # # update fully received status with mapped settings
                # received_status = line_item.received_status
                # get all the values of carrier tracking url to get cw mapped carrier
                carrier_tracking_url_mapping = (
                    operation_email_template_settings.extra_config.get(
                        "carrier_tracking_url"
                    ).values()
                )
                if any(
                    cls.validate_patterns(
                        part_number, product_category_mapping, "contracts"
                    )
                ):
                    status = "Contracts Pending"
                    _contract = dict(part_number=part_number, status=status)
                    contracts.append(_contract)
                elif any(
                    cls.validate_patterns(
                        part_number, product_category_mapping, "licences"
                    )
                ):
                    is_licence, carrier = cls.validate_patterns(
                        part_number, product_category_mapping, "licences"
                    )
                    _tracking_numbers = []
                    for tracking_number in line_item.tracking_numbers:
                        tracking_number = cls._generate_cw_tracking_url(
                            tracking_number,
                            line_item.shipping_id,
                            carrier_tracking_url_mapping,
                        )
                        _tracking_numbers.append(tracking_number)
                    _ship_date, _mapped_status = cls.get_ship_date_and_status(
                        line_item
                    )
                    _licence = dict(
                        part_number=part_number,
                        shipped_quantity=shipped_quantity,
                        purchased_quantity=line_item.ordered_quantity,
                        carriers=carrier,
                        ship_date=_ship_date,
                        tracking_numbers=", ".join(_tracking_numbers),
                        status=_mapped_status,
                    )
                    licences.append(_licence)
                else:
                    _tracking_numbers = []
                    for tracking_number in line_item.tracking_numbers:
                        tracking_number = cls._generate_cw_tracking_url(
                            tracking_number,
                            line_item.shipping_id,
                            carrier_tracking_url_mapping,
                        )
                        _tracking_numbers.append(tracking_number)
                    _ship_date, _mapped_status = cls.get_ship_date_and_status(
                        line_item
                    )
                    hw_product = dict(
                        part_number=part_number,
                        shipped_quantity=shipped_quantity,
                        purchased_quantity=line_item.ordered_quantity,
                        carriers=cls.get_carrier_of_hardware_product(
                            line_item
                        ),
                        ship_date=_ship_date,
                        tracking_numbers=", ".join(_tracking_numbers),
                        status=_mapped_status,
                    )
                    hardware_products.append(hw_product)
        customer_contact_first_name: str = (
            sales_order.shipping_contact_name.split(" ")[0]
            if sales_order.shipping_contact_name
            else ""
        )
        contact_type_connectwise_id: int = (
            operation_email_template_settings.contact_type_connectwise_id
        )
        sales_order_details_payload: Dict = (
            cls.get_sales_order_details_payload(provider, sales_order)
        )
        customer_id: int = sales_order_details_payload.pop(
            "customer_crm_id", None
        )
        to_contact_id: int = sales_order.shipping_contact_crm_id
        to_contact_emails, cc_contacts_emails = cls.get_contact_emails(
            provider, customer_id, to_contact_id, contact_type_connectwise_id
        )
        context: Dict[str, Any] = dict(
            contracts=contracts,
            hardware_products=hardware_products,
            licences=licences,
            name=customer_contact_first_name,
            cc_contacts=cc_contacts_emails,
            to_contacts_emails=to_contact_emails,
            settings=operation_email_template_settings.settings,
            **sales_order_details_payload,
        )
        return context

    @classmethod
    def get_sales_order_email_template_data(
        cls,
        sales_order: SalesPurchaseOrderData,
        purchase_orders: "Queryset[ConnectwisePurchaseOrderData]",
        operation_email_template_settings: OperationEmailTemplateSettings,
        **kwargs,
    ) -> Tuple[Dict, List[Dict]]:
        """
        Get rendered template, template data and csv tables

        Args:
            sales_order: SalesPurchaseOrderData
            purchase_orders: ConnectwisePurchaseOrderData queryset
            operation_email_template_settings: OperationEmailTemplateSettings
            **kwargs: keyword arguments

        Returns:
            Rendered template HTML, template data and csv tables
        """
        template_data: Dict = cls.get_template_context(
            sales_order, purchase_orders, operation_email_template_settings
        )
        tables: List[List[List]] = cls.get_product_data_tables(template_data)
        table_csvs: List[Optional[Dict]] = (
            cls.extract_tables_to_csv(tables=tables) if tables else []
        )
        return template_data, table_csvs

    @classmethod
    def get_product_data_tables(cls, template_data: Dict) -> List[List[List]]:
        """
        Get tables for different product types

        Args:
            template_data: Template data

        Returns:
            Tables for differenent product types
        """
        tables_list: List[List[List]] = []
        hardware_products: List[Dict] = template_data.get(
            "hardware_products", []
        )
        contracts: List[Dict] = template_data.get("contracts", [])
        licences: List[Dict] = template_data.get("licences", [])
        if hardware_products:
            hardware_products_table: List[
                List
            ] = cls.extract_table_from_product_data(hardware_products)
            tables_list.append(hardware_products_table)
        if contracts:
            contracts_table: List[List] = cls.extract_table_from_product_data(
                contracts
            )
            tables_list.append(contracts_table)
        # We don't show licenses data from UI side, but if needed just uncomment this code
        # Licenses data is still a part of API response
        # if licences:
        #     licences_table: List[List] = cls.extract_table_from_product_data(
        #         licences
        #     )
        #     tables_list.append(licences_table)
        return tables_list

    @staticmethod
    def extract_table_from_product_data(
        products_data: List[Dict],
    ) -> List[List]:
        """
        Extract a table from product data

        Args:
            products_data: Product data

        Returns:
            Table
        """
        table: List[List] = []
        columns: List[str] = list(products_data[0].keys())
        rows: List[List] = []
        for product in products_data:
            rows.append(list(product.values()))
        table.append(columns)
        table.extend(rows)
        return table
