import copy
import csv
import os
import tempfile
from typing import Any, List, Dict, Set, Optional

from accounts.models import Provider
from core.utils import chunks_of_list_with_max_item_count
from document.models import (
    ConnectwisePurchaseOrderLineitemsData,
)
from utils import get_app_logger

logger = get_app_logger(__name__)


class FireReportGenerator:
    """
    Generate FIRE Report.
    """

    def __init__(self, provider: Provider):
        self.provider = provider

    def _get_product_id_manufacturer_name_map(
        self, catalog_crm_ids: Set[int]
    ) -> Dict[int, str]:
        """
        Get product ID to manufacturer name map.

        Args:
            catalog_crm_ids: List of catalog CRM IDs

        Returns:
            Product ID to manufacturer name map
        """
        catalog_crm_ids: List[int] = list(catalog_crm_ids)
        product_catalogs: List[Dict] = list()
        for catalog_crm_ids_list in chunks_of_list_with_max_item_count(
            catalog_crm_ids, 100
        ):
            catalogs: List[
                Dict
            ] = self.provider.erp_client.get_product_catalogs(
                catalog_crm_ids_list
            )
            product_catalogs.extend(catalogs)
        product_id_to_manufacturer_name_map: Dict[int, str] = {
            item.get("catalog_crm_id"): item.get("manufacturer_name")
            for item in product_catalogs
        }
        return product_id_to_manufacturer_name_map

    @staticmethod
    def _process_fire_report_query_filters(
        filter_conditions: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        Process FIRE report query filters. Map filter conditions to query filters.

        Args:
            filter_conditions: Filter conditions

        Returns:
            Query filters
        """
        # NOTE: The change in query may require change in the query filters.
        query_filters: Dict[str, Any] = dict()
        if filter_conditions.get("shipping_site_crm_ids_list", []):
            query_filters.update(
                po_number__details__sales_order__shipping_site_crm_id__in=filter_conditions.get(
                    "shipping_site_crm_ids_list"
                )
            )
        if filter_conditions.get("sales_order_updated_after_date", None):
            query_filters.update(
                po_number__details__sales_order__last_crm_update__date__gte=filter_conditions.get(
                    "sales_order_updated_after_date"
                )
            )
        if filter_conditions.get("sales_order_updated_before_date", None):
            query_filters.update(
                po_number__details__sales_order__last_crm_update__date__lte=filter_conditions.get(
                    "sales_order_updated_before_date"
                )
            )
        if filter_conditions.get("po_site_names", []):
            query_filters.update(
                po_number__details__site_name__in=filter_conditions.get(
                    "po_site_names"
                )
            )
        return query_filters

    def get_fire_report_data(
        self, filter_conditions: Dict[str, Any]
    ) -> List[Dict[str, Any]]:
        """
        Get FIRE report data.

        Args:
            filter_conditions: Filter conditions

        Returns:
            FIRE report data
        """
        fire_report_data: List[Dict[str, Any]] = list()

        filter_params: Dict[str, Any] = dict(
            provider_id=self.provider.id,
            po_number__details__sales_order__isnull=False,
            po_number__details__sales_order__opportunity_crm_id_id__isnull=False,
            unit_cost__gt=0,
        )
        fire_report_filters: Dict[
            str, Any
        ] = self._process_fire_report_query_filters(filter_conditions)
        filter_params.update(**fire_report_filters)

        po_line_items: "QuerySet[ConnectwisePurchaseOrderLineitemsData]" = (
            ConnectwisePurchaseOrderLineitemsData.objects.select_related(
                "po_number",
                "po_number__ticket",
                "po_number__details",
                "po_number__details__sales_order",
            ).filter(
                **filter_params,
            )
        )
        if not po_line_items:
            return fire_report_data

        product_catalog_crm_ids: Set[int] = set(
            po_line_items.values_list("product_crm_id", flat=True)
        )
        product_id_manufacturer_name_map: Dict[
            int, str
        ] = self._get_product_id_manufacturer_name_map(product_catalog_crm_ids)

        for line_item in po_line_items:
            line_item_data: Dict[str, Any] = {
                "opportunity_number": line_item.po_number.ticket.opportunity_crm_id,
                "opportunity_name": line_item.po_number.ticket.opportunity_name,
                "customer_po": line_item.po_number.details.sales_order.customer_po_number,
                "po_number": line_item.po_number_id,
                "sales_order_crm_id": line_item.po_number.details.sales_order.crm_id,
                "manufacturer": product_id_manufacturer_name_map.get(
                    line_item.product_crm_id, "-"
                ),
                "part_number": line_item.product_id,
                "description": line_item.description,
                "serial_number": None,
                "ship_date": line_item.ship_date.strftime("%m/%d/%Y")
                if line_item.ship_date
                else "-",
                "shipping_location": line_item.po_number.details.site_name,
                "carrier": line_item.shipping_method,
                "tracking_numbers": ", ".join(line_item.tracking_numbers)
                if line_item.tracking_numbers
                else "-",
                "received_status": line_item.received_status,
            }
            # Each row must have a single serial number
            if not line_item.serial_numbers:
                fire_report_data.append(line_item_data)
            else:
                serial_numbers: List[str] = [
                    serial_number for serial_number in line_item.serial_numbers
                ]
                _line_items: List[Dict] = list()
                for number in serial_numbers:
                    _line_item = copy.copy(line_item_data)
                    _line_item.update(serial_number=number)
                    _line_items.append(_line_item)
                fire_report_data.extend(_line_items)
        return fire_report_data

    def generate_fire_report_csv(
        self, filter_conditions: Dict[str, Any]
    ) -> Optional[Dict[str, str]]:
        """
        Generate FIRE report CSV file.

        Args:
            filter_conditions: Filter conditions

        Returns:
            FIRE report details.
        """
        headers: List[str] = [
            "Opportunity Number",
            "Opportunity Name",
            "Customer PO",
            "LP PO",
            "Sales Order Number",
            "Manufacturer",
            "Part Number",
            "Description",
            "Serial Number",
            "Ship Date",
            "Shipping Location",
            "Carrier",
            "Tracking Numbers",
            "Received Status",
        ]
        fire_report_data: List[Dict] = self.get_fire_report_data(
            filter_conditions
        )
        if not fire_report_data:
            return None
        file_data: List[Dict] = list()
        for data in fire_report_data:
            file_data.append(
                {
                    "Opportunity Number": data.get("opportunity_number"),
                    "Opportunity Name": data.get("opportunity_name"),
                    "Customer PO": data.get("customer_po"),
                    "LP PO": data.get("po_number"),
                    "Sales Order Number": data.get("sales_order_crm_id"),
                    "Manufacturer": data.get("manufacturer"),
                    "Part Number": data.get("part_number"),
                    "Description": data.get("description"),
                    "Serial Number": data.get("serial_number"),
                    "Ship Date": data.get("ship_date"),
                    "Shipping Location": data.get("shipping_location"),
                    "Carrier": data.get("carrier"),
                    "Tracking Numbers": data.get("tracking_numbers"),
                    "Received Status": data.get("received_status"),
                }
            )

        TEMP_DIR = tempfile.gettempdir()
        file_name = f"FIRE report.csv"
        file_path = os.path.join(TEMP_DIR, file_name)
        try:
            with open(file_path, "w") as output_file:
                dict_writer = csv.DictWriter(output_file, headers)
                dict_writer.writeheader()
                dict_writer.writerows(file_data)
        except OSError as err:
            logger.error(
                f"OS error while generating FIRE report CSV. Error: {err}"
            )
            return None
        return dict(file_path=file_path, file_name=file_name)
