from typing import Dict, Optional

from document.models import (
    SOWDocument,
    DefaultDocumentCreatorRoleSetting,
    DocumentAuthorTeamMemberMapping,
)
from exceptions.exceptions import APIError
from utils import get_app_logger

logger = get_app_logger(__name__)


class SoWCustomerTeamOperationService:
    """
    This class handles all operations related to customer team operations after SoWDocument is created or updated.
    """

    @staticmethod
    def create_or_update_document_author_team_member_mapping(
        document: SOWDocument, team_member_crm_id: int
    ) -> DocumentAuthorTeamMemberMapping:
        """
        Create or update document to author's team member CRM ID mapping.

        Args:
            document: SOWDocument
            team_member_crm_id: Author's team member CRM ID

        Returns:
            DocumentAuthorTeamMemberMapping
        """
        try:
            existing_mapping: Optional[
                DocumentAuthorTeamMemberMapping
            ] = document.author_team_member_mapping
        except DocumentAuthorTeamMemberMapping.DoesNotExist:
            existing_mapping = None

        if not existing_mapping:
            mapping: DocumentAuthorTeamMemberMapping = (
                DocumentAuthorTeamMemberMapping.objects.create(
                    provider=document.provider,
                    sow_document=document,
                    team_member_crm_id=team_member_crm_id,
                )
            )
            return mapping
        else:
            existing_mapping.team_member_crm_id = team_member_crm_id
            existing_mapping.save()
            return existing_mapping

    @classmethod
    def add_sow_document_author_to_customer_team(
        cls,
        document: SOWDocument,
    ) -> Dict:
        """
        Adds the SoW document author as member to customer team. The default role of author is configured in
        DefaultDocumentCreatorRoleSetting. When a new document is created, the author is added to customer team with
        default role configured in DefaultDocumentCreatorRoleSetting. The member CRM ID is then mapped with sow document
        in DocumentAuthorTeamMemberMapping.

        Args:
            document: SOWDocument

        Returns:
            Member details
        """
        provider: Provider = document.provider
        try:
            default_document_creator_role_setting: DefaultDocumentCreatorRoleSetting = (
                provider.default_document_creator_role_setting
            )
        except DefaultDocumentCreatorRoleSetting.DoesNotExist:
            logger.info(
                f"Default Document Creator Role Setting not configured for the provider: {provider.name}, "
                f"ID: {provider.id}. Cannot add document author to customer team. Document ID: {document.id}. "
            )
            return {}

        author_details: Dict[str, int] = cls.get_author_details(document)
        if not author_details:
            return {}

        try:
            member: Dict = provider.erp_client.add_new_member_to_company_team(
                document.customer.crm_id, author_details
            )
        except APIError as err:
            logger.error(
                f"CW API error while adding sow author to customer team. "
                f"Document ID: {document.id}, author ID: {document.author_id}, Customer CRM ID: "
                f"{document.customer.crm_id}. Error: {err}"
            )
            member: Dict = {}

        if member.get("team_member_crm_id", None):
            cls.create_or_update_document_author_team_member_mapping(
                document, member.get("team_member_crm_id")
            )
        return member

    @classmethod
    def add_updated_author_to_customer_team(
        cls,
        document: SOWDocument,
    ) -> Dict:
        """
        Add the updated sow document author to customer team. First the previous author is removed from customer team
        on CW. Then new author is added to customer team on CW, member CRM ID to sow document mapping is updated in
        DocumentAuthorTeamMemberMapping.

        Args:
            document: SOWDocument

        Returns:
            Team member details.
        """
        provider: Provider = document.provider
        customer_crm_id: int = document.customer.crm_id

        try:
            default_document_creator_role_setting: DefaultDocumentCreatorRoleSetting = (
                provider.default_document_creator_role_setting
            )
        except DefaultDocumentCreatorRoleSetting.DoesNotExist:
            logger.info(
                f"Default Document Creator Role Setting not configured for the provider: {provider.name}, "
                f"ID: {provider.id}. Cannot add updated sow author to customer team. Document ID: {document.id}. "
            )
            return {}

        try:
            existing_author_team_member_mapping: Optional[
                DocumentAuthorTeamMemberMapping
            ] = document.author_team_member_mapping
        except DocumentAuthorTeamMemberMapping.DoesNotExist:
            existing_author_team_member_mapping = None

        if existing_author_team_member_mapping:
            # Remove author from customer team on CW
            try:
                provider.erp_client.remove_member_from_company_team(
                    customer_crm_id,
                    existing_author_team_member_mapping.team_member_crm_id,
                )
            except APIError as err:
                logger.error(
                    f"CW API error while removing existing author from customer team. "
                    f"Document ID: {document.id}, existing author CRM ID: "
                    f"{existing_author_team_member_mapping.team_member_crm_id}, Customer CRM ID: "
                    f"{document.customer.crm_id}. Error: {err}"
                )
        member_details: Dict = cls.add_sow_document_author_to_customer_team(
            document
        )
        return member_details

    @classmethod
    def get_author_details(cls, document: SOWDocument) -> Dict[str, int]:
        """
        Get author details.

        Details include:
            system_member_crm_id
            default_team_role_crm_id

        Args:
            document: SOWDocument

        Returns:
            Author details.
        """
        system_member_crm_id: Optional[
            int
        ] = document.author.profile.system_member_crm_id
        if system_member_crm_id is None:
            logger.info(
                f"System member CRM ID not found for SoW author: {document.author.name}, ID: {document.author_id}. "
                f"Document ID: {document.id}, Provider ID: {document.provider.id}. Cannot add author as team member to"
                f"customer team."
            )
            return {}

        default_role_crm_id: Optional[
            int
        ] = cls.get_default_role_crm_id_from_setting(document)
        if default_role_crm_id is None:
            return {}

        author_details: Dict[str, int] = dict(
            role_crm_id=default_role_crm_id,
            system_member_crm_id=system_member_crm_id,
        )
        return author_details

    @staticmethod
    def get_default_role_crm_id_from_setting(
        sow_document: SOWDocument,
    ) -> Optional[int]:
        """
        Get default team member role CRM ID as configured in DefaultDocumentCreatorRoleSetting.
        The role depends on type of SoWDocument.

        Args:
            sow_document: SOWDocument

        Returns:
            (int) Role CRM ID
        """
        document_creator_role_setting: DefaultDocumentCreatorRoleSetting = (
            sow_document.provider.default_document_creator_role_setting
        )

        if sow_document.doc_type in (SOWDocument.GENERAL, SOWDocument.T_AND_M):
            return document_creator_role_setting.default_sow_creator_role_id
        elif sow_document.doc_type == SOWDocument.CHANGE_REQUEST:
            return (
                document_creator_role_setting.default_change_request_creator_role_id
            )
        else:
            return None
