import base64
import os

import requests


def image_as_base64(image_file, format="png"):
    """
    :param `image_file` for the complete path of image.
    :param `format` is format for image, eg: `png` or `jpg`.
    """
    if not os.path.isfile(image_file):
        return None

    with open(image_file, "rb") as img_f:
        encoded_string = base64.b64encode(img_f.read()).decode("utf-8")
    return "data:image/%s;base64,%s" % (format, encoded_string)


def get_as_base64(url):
    """
    Convert a web-based image to base64.
    """
    if url:
        return base64.b64encode(requests.get(url).content)
    else:
        return None
