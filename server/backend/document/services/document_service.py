import copy
import json
import math
import os
import re
import tempfile
from datetime import date, datetime
from typing import Dict, Optional, List, Union, Any, Set, Tuple
from uuid import UUID

import pdfkit
import reversion
from bs4 import BeautifulSoup
from django.conf import settings
from django.forms import model_to_dict
from django.template.loader import get_template, render_to_string
from django.utils import timezone
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import get_object_or_404

from accounts.models import Provider, User
from accounts.utils import get_formatted_phone_number
from core.utils import generate_random_id
from document.models import (
    SOWDocument,
    SOWDocumentSettings,
    SoWHourlyResourcesDescriptionMapping,
)
from document.serializers import (
    SOWDocumentSerializer,
)
from document.services import QuoteService, get_as_base64, image_as_base64
from document.services.sow_customer_team_operations import (
    SoWCustomerTeamOperationService,
)
from document.services.opportunity_team_operations import (
    SoWOpportunityTeamOperationService,
)
from document.utils import (
    get_sow_document_file_name,
    substitute_page_break_placeholder_in_sow_json_config,
)
from exceptions.exceptions import InvalidRequestError, NotFoundError, APIError
from storage.file_upload import FileUploadService
from utils import (
    get_valid_file_name,
    get_app_logger,
    format_phone_number,
    add_comma_to_number,
    generate_pdf_using_pdf_shift,
)
from .sow_calculations import SoWDocumentCalculation

logger = get_app_logger(__name__)


class DocumentService:
    # Quote Status
    WON = "Won"
    OPEN = "Open"
    NO_DECISION = "No Decision"
    CLOSED = "Closed"
    LOST = "Lost"
    REJECTED = "Rejected"
    REJECTED_NOTES = "Rejected - See Notes Tab"
    CLOSED_STATUSES = [
        LOST.lower(),
        REJECTED.lower(),
        REJECTED_NOTES.lower(),
        NO_DECISION.lower(),
    ]
    SERVICE_DETAIL = "Service Detail"
    SOW = "SoW"
    TEMPLATE_BASE_DIR = os.path.join(
        "configs", "base_template", "providers", "default"
    )
    PDF_FORMAT = "pdf"

    @classmethod
    def create_document(cls, provider_id: int, author_id: UUID, data: Dict):
        quote_id: int = data.get("quote_id")
        if QuoteService.get_quote(
            provider_id, quote_id, data.get("customer").crm_id
        ):
            try:
                forecast: Dict = QuoteService.create_or_update_forecast(
                    provider_id, quote_id, data
                )
                forecast_id: int = forecast.get("id")
            except Exception as error_message:
                logger.error(
                    f"Exception while creating forecast for oppo: {error_message}"
                )
                forecast_exception = dict(
                    exception_in_forecast_creation=True,
                    message=f"Exception while creating forecast for oppo: {error_message}",
                )
                forecast_id = None
                data.get("json_config").update(
                    dict(forecast_exception=forecast_exception)
                )
            data.update(dict(provider_id=provider_id))
            sow_document_payload: Dict = dict(
                author_id=author_id,
                forecast_id=forecast_id,
                updated_by_id=author_id,
                **data,
            )
            sow_document_object: SOWDocument = SOWDocument(
                **sow_document_payload
            )
            document_margin: float = cls.get_document_margin(
                sow_document_object
            )
            sow_document_payload.update(margin=document_margin)
            sow_document: SOWDocument = SOWDocument.objects.create(
                **sow_document_payload
            )
            SoWCustomerTeamOperationService.add_sow_document_author_to_customer_team(
                sow_document
            )
            SoWOpportunityTeamOperationService().create_or_update_opportunity_team(
                sow_document
            )
            return sow_document
        else:
            raise NotFound("Opportunity does not exist. Please create one.")

    @classmethod
    def update_document(cls, provider_id, document_id, updated_by_id, data):
        quote_id = data.get("quote_id")
        if QuoteService.get_quote(provider_id, quote_id):
            try:
                sow_document: SOWDocument = get_object_or_404(
                    SOWDocument.objects.all(),
                    pk=document_id,
                    provider_id=provider_id,
                )
                try:
                    forecast: Dict = QuoteService.create_or_update_forecast(
                        provider_id, quote_id, data
                    )
                    forecast_id: int = forecast.get("id")
                except Exception as error_message:
                    logger.error(
                        f"Exception while updating forecast for oppo: {error_message}"
                    )
                    forecast_exception = dict(
                        exception_in_forecast_creation=True,
                        message=f"Exception while updating forecast for oppo: {error_message}",
                    )
                    forecast_id = None
                    data.get("json_config").update(
                        dict(forecast_exception=forecast_exception)
                    )
                # forecast = QuoteService.create_or_update_forecast(
                #     provider_id, quote_id, data
                # )
                # forecast_id = forecast.get("id")

                major_version = sow_document.major_version or 1
                minor_version = sow_document.minor_version or 0
                minor_version += 1
                if data.get("update_version", False):
                    major_version += 1
                    minor_version = 0
                data.update(
                    updated_by_id=updated_by_id,
                    forecast_id=forecast_id,
                )
                sow_document_payload: Dict = copy.deepcopy(data)
                sow_document_payload.update(provider_id=provider_id)
                sow_document_payload.pop("update_author", None)
                sow_document_payload.pop("update_version", None)
                sow_document_payload.pop("version_description", None)
                sow_document_object: SOWDocument = SOWDocument(
                    **sow_document_payload
                )
                document_margin: float = cls.get_document_margin(
                    sow_document_object
                )
                data.update(
                    updated_on=datetime.now(),
                    margin=document_margin,
                )
                for attr, val in data.items():
                    setattr(sow_document, attr, val)

                sow_document.major_version = major_version
                sow_document.minor_version = minor_version
                update_author: bool = data.get("update_author", False)
                if update_author:
                    sow_document.author_id = updated_by_id
                with reversion.create_revision():
                    sow_document.save()
                    sow_document.refresh_from_db()
                    # Store some meta-information.
                    meta_data = {
                        "version": sow_document.version,
                        "description": data.get("version_description"),
                    }
                    reversion.set_user(User.objects.get(id=updated_by_id))
                    reversion.set_comment(json.dumps(meta_data))
                if update_author:
                    SoWCustomerTeamOperationService.add_updated_author_to_customer_team(
                        sow_document
                    )
                SoWOpportunityTeamOperationService().create_or_update_opportunity_team(
                    sow_document
                )
            except NotFoundError as e:
                raise ValidationError(
                    {
                        "quote_id": "Current Opportunity Status is closed. "
                        "Please connect to your administrator."
                    }
                )
            except InvalidRequestError as e:
                raise ValidationError({"quote_id": e.errors})
        else:
            raise NotFound("Opportunity does not exist. Please create one.")

    @classmethod
    def get_document(cls, provider_id, document_id):
        try:
            document = SOWDocument.objects.select_related().get(
                id=document_id, provider=provider_id
            )
            return document
        except SOWDocument.DoesNotExist:
            raise NotFound("SOW Document does not exist.")

    @classmethod
    def get_logo(cls):
        logo = image_as_base64(
            os.path.join(settings.STATIC_ROOT, "img", "lookingpoint-logo.png"),
            "png",
        )
        return logo

    @classmethod
    def get_banner(cls):
        banner = image_as_base64(
            os.path.join(
                settings.STATIC_ROOT, "img", "lookingpoint-banner.png"
            ),
            "png",
        )
        return banner

    @classmethod
    def get_footer(cls):
        footer = image_as_base64(
            os.path.join(
                settings.STATIC_ROOT, "img", "lookingpoint-footer.png"
            ),
            "png",
        )
        return footer

    @classmethod
    def get_header_html(cls, template_vars) -> str:
        HEADER_HTML_PATH = os.path.join(cls.TEMPLATE_BASE_DIR, "header.html")
        header_html = render_to_string(HEADER_HTML_PATH, template_vars)
        return header_html

    @classmethod
    def get_footer_html(cls, template_vars) -> str:
        FOOTER_HTML_PATH = os.path.join(cls.TEMPLATE_BASE_DIR, "footer.html")
        footer_html = render_to_string(FOOTER_HTML_PATH, template_vars)
        return footer_html

    @classmethod
    def convert_html_to_str(cls, html_out):
        soup = BeautifulSoup(html_out, "html.parser")
        # convert images to base64 str
        for img in soup.findAll("img"):
            if not re.match(r"^data:image", img["src"]):
                extension = img["src"].split(".")[-1]
                img["src"] = "data:image/%s;base64,%s" % (
                    extension,
                    get_as_base64(img["src"]).decode("utf-8"),
                )
        html_str = str(soup)
        return html_str

    @classmethod
    def _get_temp_file_path(cls):
        file_name = f"{generate_random_id(15)}.pdf"
        TEMP_DIR = tempfile.gettempdir()
        file_path = os.path.join(TEMP_DIR, file_name)
        return file_name, file_path

    @classmethod
    def _get_sow_setting(cls, provider):
        try:
            return provider.sow_doc_settings
        except SOWDocumentSettings.DoesNotExist:
            return None

    @classmethod
    def get_project_assumption_variable_values(
        cls, service_cost: Dict[str, Any]
    ) -> Tuple:
        """
        Get project assumption variable values.

        Args:
            service_cost: Service cost data

        Returns:
            Project assumption variable values.
        """
        engineering_hours_breakup = service_cost.get(
            "engineering_hours_breakup", {}
        )
        total_sites = engineering_hours_breakup.get("total_sites", 1)
        total_sites_override = engineering_hours_breakup.get(
            "total_sites_override", 1
        )
        total_cutovers = engineering_hours_breakup.get("total_cutovers", False)
        total_cutovers_override = engineering_hours_breakup.get(
            "total_cutovers_override", False
        )
        return (
            total_sites,
            total_sites_override,
            total_cutovers,
            total_cutovers_override,
        )

    @classmethod
    def get_additional_project_assumptions(
        cls, provider: Provider, service_cost: Dict
    ) -> List[str]:
        """
        Gives a list of additional project assumptions to be added to the document.

        """
        sow_settings: SOWDocumentSettings = cls._get_sow_setting(provider)
        additional_assumptions = []
        if not sow_settings:
            return additional_assumptions
        (
            total_sites,
            total_sites_override,
            total_cutovers,
            total_cutovers_override,
        ) = cls.get_project_assumption_variable_values(service_cost)
        if not total_sites_override:
            try:
                additional_assumptions.append(
                    (
                        sow_settings.total_no_of_sites_statement.replace(
                            "##{no_of_sites}", str(total_sites)
                        )
                    )
                )
            except AttributeError:
                return additional_assumptions
        if not total_cutovers_override:
            try:
                additional_assumptions.append(
                    (
                        sow_settings.total_no_of_cutovers_statement.replace(
                            "##{no_of_cutovers}", str(total_cutovers)
                        )
                    )
                )
            except AttributeError:
                return additional_assumptions

        return additional_assumptions

    @staticmethod
    def parse_special_variables_in_sow_document(
        context: Dict[str, Any]
    ) -> None:
        """
        Replace placeholders with values in json config.

        Args:
            context: Render context

        Returns:
            None
        """
        json_config: Dict[str, Any] = context.pop("json_config", {})
        parsed_json_config: Dict[
            str, Any
        ] = substitute_page_break_placeholder_in_sow_json_config(json_config)
        context.update(json_config=parsed_json_config)
        return

    @classmethod
    def download_document(
        cls,
        provider_id: int,
        erp_client,
        author: User,
        document_id: int,
        file_type: str,
        file_name: Optional[str] = None,
        upload: bool = True,
    ) -> Union[str, Dict]:
        document: SOWDocument = DocumentService.get_document(
            provider_id, document_id
        )
        template_vars: Dict = cls.get_template_context(
            document.author, document, erp_client
        )
        cls.parse_special_variables_in_sow_document(template_vars)
        html_out: str = cls.get_rendered_html_document(
            document, template_vars, file_type
        )
        file_name: str = (
            file_name
            if file_name
            else get_sow_document_file_name(document, file_type)
        )
        file_name: str = get_valid_file_name(file_name)
        soup = BeautifulSoup(html_out, "html.parser")
        for img in soup.findAll("img"):
            if not re.match(r"^data:image", img["src"]):
                extension = img["src"].split(".")[-1]
                img["src"] = "data:image/%s;base64,%s" % (
                    extension,
                    get_as_base64(img["src"]).decode("utf-8"),
                )
                replace_with = f"""       
                <!--[if mso]>
                 <center>
                 <table><tr><td width="630">
                 <div style="max-width:630px; margin:0 auto;">
                 <![endif]-->
                 {img}
                 <!--[if mso]>
                 </div>
                 </td></tr></table>
                 </center>
                <![endif]--> 
                        """
                img.replaceWith(BeautifulSoup(replace_with, "html.parser"))
        html_str: str = str(soup)
        if file_type.lower() == cls.PDF_FORMAT:
            header: Dict = dict(source=cls.get_header_html(template_vars))
            footer: Dict = dict(source=cls.get_footer_html(template_vars))
            data: Dict[str, str] = generate_pdf_using_pdf_shift(
                html_str,
                file_name,
                local_path=True,
                header=header,
                footer=footer,
                options=dict(
                    margin="80px 10px 80px 10px",
                ),
            )
            if not settings.DEBUG:
                s3_path: str = FileUploadService.upload_file(
                    data.get("file_path")
                )
                data.update(file_path=s3_path)
            return data
        else:
            return html_str

    @classmethod
    def render_pdf(
        cls,
        html,
        header_html=None,
        footer_html=None,
        output=None,
        options=None,
    ):
        """
        Function for easy printing of pdfs from django templates

        Header template can also be set
        """
        if options is None:
            options = {
                "--load-error-handling": "skip",
                "page-size": "Letter",
                "margin-top": "1.25in",
                "margin-right": "0.75in",
                "margin-bottom": "0.75in",
                "margin-left": "0.95in",
                "encoding": "UTF-8",
                "no-outline": None,
                "enable-local-file-access": "",
            }
        try:
            if header_html:
                with tempfile.NamedTemporaryFile(
                    suffix=".html", delete=False
                ) as header:
                    options["header-html"] = header.name
                    header.write(header_html.encode("utf-8"))
            if footer_html:
                with tempfile.NamedTemporaryFile(
                    suffix=".html", delete=False
                ) as footer:
                    options["footer-html"] = footer.name
                    footer.write(footer_html.encode("utf-8"))

            return pdfkit.from_string(html, output, options=options)
        finally:
            # Ensure temporary file is deleted after finishing work
            if header_html:
                os.remove(options["header-html"])
            if footer_html:
                os.remove(options["footer-html"])

    @classmethod
    def get_rendered_html_document(cls, document, template_vars, file_type):
        html_page = get_template(
            SOWDocument.get_doc_template_path(document.doc_type, file_type)
        )
        html_out = html_page.render(template_vars)
        return html_out

    @staticmethod
    def add_description_to_hourly_resources(
        template_vars: Dict[str, Any]
    ) -> None:
        """
        Add resource description to hourly resources. The description is mapped in SoWHourlyResourcesDescriptionMapping.

        Args:
            template_vars: Template render context

        Returns:
            None
        """
        hourly_resources: List[Dict] = (
            template_vars.get("json_config", {})
            .get("service_cost", {})
            .get("hourly_resources", [])
        )
        if not hourly_resources:
            return

        hourly_resource_crm_ids: List[int] = [
            resource.get("resource_id") for resource in hourly_resources
        ]
        resource_description_mapping: "QuerySet[SoWHourlyResourcesDescriptionMapping]" = SoWHourlyResourcesDescriptionMapping.objects.filter(
            provider_id=template_vars.get("provider"),
            vendor_crm_id__in=hourly_resource_crm_ids,
        )
        resource_crm_id_to_description_mapping: Dict[int, str] = {
            mapping.vendor_crm_id: mapping.resource_description
            for mapping in resource_description_mapping
        }
        mapped_resource_crm_ids: Set[int] = set(
            resource_crm_id_to_description_mapping.keys()
        )
        for hourly_resource in hourly_resources:
            resource_id: int = hourly_resource.get("resource_id")
            if resource_id in mapped_resource_crm_ids:
                hourly_resource.update(
                    resource_description=resource_crm_id_to_description_mapping[
                        resource_id
                    ]
                )

    @staticmethod
    def add_default_description_to_t_and_m_resource_hours(
        template_vars: Dict[str, Any]
    ) -> None:
        """
        Add default description for T&M resource hours like engineering hours, engineering after hours,
        project management hours. These default descriptions are configured in TandMResourcesDefaultDescriptionSetting

        Args:
            template_vars: Template render context

        Returns:
            None
        """
        provider: Provider = Provider.objects.get(
            id=template_vars.get("provider")
        )
        try:
            sow_document_setting: Optional[
                SOWDocumentSettings
            ] = provider.sow_doc_settings
        except SOWDocumentSettings.DoesNotExist:
            sow_document_setting = None
        if sow_document_setting:
            template_vars.update(
                engineering_hours_default_description=sow_document_setting.engineering_hours_description,
                after_hours_default_description=sow_document_setting.after_hours_description,
                project_management_hours_default_description=sow_document_setting.project_management_hours_description,
                integration_technician_hours_default_description=sow_document_setting.integration_technician_description,
            )
        else:
            template_vars.update(
                engineering_hours_default_description="Engineering hours",
                after_hours_default_description="After hours",
                project_management_hours_default_description="Project management hours",
                integration_technician_hours_default_description="Integration technician",
            )
        return

    @classmethod
    def get_template_context(
        cls, author: User, document: SOWDocument, erp_client, **kwargs
    ) -> Dict:
        cw_customer: Dict = erp_client.get_customer(document.customer.crm_id)
        account_manager: Dict = dict(
            territory_manager_name=cw_customer.get("territory_manager_name"),
            name=cw_customer.get("name"),
            email=cw_customer.get("email"),
        )
        template_vars: Dict = model_to_dict(document)
        author_details: Dict = dict(name=author.name, email=author.email)
        # When previewing sow document that is already created, original author name is used.
        if kwargs.get("original_author_name", None):
            author_details.update(name=kwargs.get("original_author_name"))
        current_date = date.today().strftime("%m/%d/%Y")
        template_vars.update(
            {
                "customer": model_to_dict(document.customer),
                "user_name": document.user.name,
                "contact_details": document.user.profile.full_cell_phone_number
                if document.user.profile.cell_phone_number
                else document.customer.phone,
                "account_manager": account_manager,
                "author": author_details,
                "version": document.major_version,
            }
        )
        template_vars["contact_details"] = (
            get_formatted_phone_number(template_vars["contact_details"])
            if template_vars["contact_details"]
            else None
        )
        template_vars.update(
            {
                "current_date": current_date,
                "logo": cls.get_logo(),
                "banner": cls.get_banner(),
                "footer": cls.get_footer(),
            }
        )
        additional_project_assumptions = (
            cls.get_additional_project_assumptions(
                document.provider, document.json_config.get("service_cost", {})
            )
        )
        template_vars[
            "additional_project_assumptions"
        ] = additional_project_assumptions
        (
            total_sites,
            total_sites_override,
            total_cutovers,
            total_cutovers_override,
        ) = cls.get_project_assumption_variable_values(
            document.json_config.get("service_cost", {})
        )
        template_vars["total_sites"] = total_sites
        template_vars["total_cutovers"] = total_cutovers
        template_vars["total_sites_override"] = total_sites_override
        template_vars["total_cutovers_override"] = total_cutovers_override
        if template_vars.get("doc_type") == SOWDocument.T_AND_M:
            cls.add_description_to_hourly_resources(template_vars)
            cls.add_default_description_to_t_and_m_resource_hours(
                template_vars
            )
        return template_vars

    @classmethod
    def _generate_service_detail_pdf_file_name(
        cls, document: SOWDocument
    ) -> str:
        file_name: str = (
            f"{cls.SERVICE_DETAIL} - {document.name}.{cls.PDF_FORMAT}"
        )
        return file_name

    @classmethod
    def download_service_detail_document_pdf(
        cls, document: SOWDocument, quote: Dict
    ) -> Dict[str, str]:
        """
        Download service detail document PDF.

        Args:
            document: SOWDocument
            quote: Quote data

        Returns:
            File path and file name of service detail document.
        """
        file_name: str = cls._generate_service_detail_pdf_file_name(document)
        service_detail_document_html: str = (
            cls.get_rendered_service_detail_document_html(document, quote)
        )
        service_detail_header: Dict[
            str, str
        ] = cls.get_rendered_html_service_detail_header()

        if settings.DEBUG:
            local_path: bool = True
        else:
            local_path: bool = False
        service_detail_pdf: Dict[str, str] = generate_pdf_using_pdf_shift(
            service_detail_document_html,
            file_name,
            local_path=local_path,
            header=service_detail_header,
        )
        return service_detail_pdf

    @classmethod
    def get_rendered_html_service_detail_header(cls) -> Dict[str, str]:
        """
        Get service detail document header HTML.

        Returns:
            Rendered service detail document header HTML.
        """
        render_context: Dict = dict(
            logo=settings.LOOKINGPOINT_LOGO,
        )
        service_detail_header_path: str = os.path.join(
            cls.TEMPLATE_BASE_DIR, "service_detail_header.html"
        )
        header_html: str = render_to_string(
            service_detail_header_path, render_context
        )
        return dict(source=header_html)

    @staticmethod
    def get_total_of_hourly_resource_hours(
        hourly_resources: List[Dict[str, Any]]
    ) -> int:
        """
        Get total of hours for all hourly resources.

        Args:
            hourly_resources: Hourly resources

        Returns:
            Total of hours for all hourly resources.
        """
        total_of_hourly_resource_hours: int = 0
        total_of_hourly_resource_hours += sum(
            [
                hourly_resource.get("hours", 0)
                for hourly_resource in hourly_resources
            ]
        )
        return total_of_hourly_resource_hours

    @classmethod
    def get_rate_and_hours_section_data_for_service_detail_doc(
        cls, service_cost: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        Get data for rate and hours section in service detail doc.

        Args:
            service_cost: Service cost data.

        Returns:
            Data for rate and hours section in service detail doc.
        """
        engineering_hours: int = service_cost.get("engineering_hours")
        after_hours: int = service_cost.get("after_hours")
        project_management_hours: int = service_cost.get(
            "project_management_hours"
        )
        integration_technician_hours: int = service_cost.get(
            "integration_technician_hours"
        )

        hourly_resources: List[Dict[str, Any]] = service_cost.get(
            "hourly_resources", list()
        )
        total_hourly_resource_hours: int = (
            cls.get_total_of_hourly_resource_hours(hourly_resources)
        )

        total_hours_in_rate_and_hours_section: int = 0
        total_hours_in_rate_and_hours_section += (
            engineering_hours
            + after_hours
            + project_management_hours
            + total_hourly_resource_hours
            + integration_technician_hours
        )
        return dict(
            total_hours_in_rate_and_hours=total_hours_in_rate_and_hours_section
        )

    @staticmethod
    def get_phase_section_data_for_service_detail_doc(
        service_cost: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        Get data for phase section in service detail document.

        Args:
            service_cost: Service cost

        Returns:
            Phase section data.
        """
        phases: List[Dict[str, Any]] = service_cost.get(
            "engineering_hours_breakup", dict()
        ).get("phases", list())

        total_hours_in_phases: int = 0
        total_hours_in_phases += sum(
            [phase.get("hours", 0) for phase in phases]
        )
        project_management_hours: int = service_cost.get(
            "project_management_hours"
        )
        total_hours_in_phases += project_management_hours
        return dict(total_hours_in_phases=total_hours_in_phases)

    @classmethod
    def get_hourly_resources_data_for_service_detail_doc(
        cls, provider: Provider, service_cost: Dict[str, Any]
    ) -> List[Dict[str, Any]]:
        """
        Get data for hourly resources section in service detail doc.

        Args:
            provider: Provider
            service_cost: Service cost data

        Returns:
            Data for hourly resources section in service detail doc.
        """
        if not service_cost.get("hourly_resources", list()):
            return list()

        hourly_resources: List[Dict[str, Any]] = service_cost.get(
            "hourly_resources"
        )
        vendor_crm_ids: List[int] = [
            hourly_resource.get("resource_id")
            for hourly_resource in hourly_resources
            if hourly_resource.get("resource_id", None)
        ]
        vendor_contact_details: List[
            Dict
        ] = provider.erp_client.get_default_customer_contacts(vendor_crm_ids)
        vendor_crm_id_to_contact_details_mapping: Dict[int, Dict] = {
            contact_info.get("customer_id"): contact_info
            for contact_info in vendor_contact_details
        }

        for hourly_resource in hourly_resources:
            margin: Union[int, float] = hourly_resource.get("margin")
            customer_cost: Union[int, float] = hourly_resource.get(
                "customer_cost"
            )
            internal_cost: Union[int, float] = hourly_resource.get(
                "internal_cost"
            )
            contact_details: Dict = (
                vendor_crm_id_to_contact_details_mapping.get(
                    hourly_resource.get("resource_id"), dict()
                )
            )
            hourly_resource.update(
                contact_name=f"{contact_details.get('first_name')} {contact_details.get('last_name')}",
                email=contact_details.get("email", ""),
                phone_number=format_phone_number(
                    contact_details.get("default_phone_number")
                )
                if contact_details.get("default_phone_number", None)
                else "",
                margin=margin,
                customer_cost=customer_cost,
                internal_cost=internal_cost,
            )
        return hourly_resources

    @classmethod
    def get_financial_summary_data_for_service_detail_doc(
        cls, document: SOWDocument
    ) -> Dict[str, Any]:
        """
        Get financial summary data for service detail doc.

        Args:
            document: SOWDocument

        Returns:
            Financial summary data for service detail doc.
        """
        sow_calculator: SoWDocumentCalculation = SoWDocumentCalculation(
            document.provider, document
        )
        revenue: int = sow_calculator.get_total_revenue()
        cost: int = sow_calculator.get_total_cost()
        margin: int = sow_calculator.get_total_margin()
        gross_margin_percent: float = (
            sow_calculator.get_total_gross_profit_percent()
        )

        professional_services_summary: Dict[
            str, Any
        ] = sow_calculator.get_professional_services_summary()

        risk_budget_summary: Dict[
            str, Any
        ] = sow_calculator.get_risk_budget_summary()

        contractors_summary: Dict[
            str, Any
        ] = sow_calculator.get_contractors_summary()

        hourly_resources_summary: Dict[
            str, Any
        ] = sow_calculator.get_hourly_resources_summary()

        total_travel_cost: Optional[
            int
        ] = sow_calculator.get_total_travel_cost()

        return dict(
            professional_services_summary=professional_services_summary,
            risk_budget_summary=risk_budget_summary,
            contractors_summary=contractors_summary,
            hourly_resources_summary=hourly_resources_summary,
            revenue=revenue,
            cost=cost,
            margin=margin,
            margin_percent=gross_margin_percent,
            total_travel_cost=total_travel_cost,
        )

    @classmethod
    def get_contractors_data_for_service_detail_doc(
        cls, provider: Provider, service_cost: Dict[str, Any]
    ) -> List[Dict[str, Any]]:
        """
        Get contractors data for service detail document.
        Includes more info such as contact information of contractors.

        Args:
            provider: Provider
            service_cost: Service cost dictionary

        Returns:
            Contractors data
        """
        contractors: List[Dict[str, Any]] = service_cost.get(
            "contractors", list()
        )
        if not contractors:
            return list()

        contractor_crm_id_to_contact_details_mapping: Dict = dict()
        contractor_crm_ids: List[int] = [
            contractor.get("vendor_id")
            for contractor in contractors
            if contractor.get("vendor_id", None)
        ]
        if contractor_crm_ids:
            contractor_contact_details: List[
                Dict
            ] = provider.erp_client.get_default_customer_contacts(
                contractor_crm_ids
            )
            contractor_crm_id_to_contact_details_mapping: Dict[int, Dict] = {
                contact_detail.get("customer_id"): contact_detail
                for contact_detail in contractor_contact_details
            }
        for contractor in contractors:
            contractor_crm_id: int = contractor.get("vendor_id", None)
            contact_details: Dict[str, Any] = (
                contractor_crm_id_to_contact_details_mapping.get(
                    contractor_crm_id, dict()
                )
                if contractor_crm_id
                else dict()
            )
            customer_cost: Union[int, float] = contractor.get("customer_cost")
            partner_cost: Union[int, float] = contractor.get("partner_cost")
            margin: Union[int, float] = customer_cost - partner_cost
            contractor.update(
                margin=margin,
                customer_cost=customer_cost,
                partner_cost=partner_cost,
                contact_name=f"{contact_details.get('first_name', '')} {contact_details.get('last_name', '')}",
                email=contact_details.get("email", ""),
                phone_number=format_phone_number(
                    contact_details.get("default_phone_number")
                )
                if contact_details.get("default_phone_number", None)
                else "",
            )
        return contractors

    @classmethod
    def get_rendered_service_detail_document_html(
        cls, document: SOWDocument, quote: Dict
    ) -> str:
        """
        Get rendered service detail document HTML.

        Args:
            document: SOWDocument
            quote: Quote

        Returns:
            Service detail document HTML.
        """
        render_context: Dict[
            str, Any
        ] = cls.get_render_context_for_service_detail_document(document, quote)
        html_page = get_template(
            SOWDocument.get_service_detail_template_path()
        )
        html_out: str = html_page.render(render_context)
        return html_out

    @classmethod
    def get_customer_contact_details_for_service_detail_doc(
        cls, sow_document: SOWDocument
    ) -> Dict[str, str]:
        """
        Get customer contact details such as email, phone number of customers for service detail docs.

        Args:
            sow_document: SOWDocument

        Returns:
            Contact details.
        """
        provider: Provider = sow_document.provider
        customer_contact_details: Dict = dict(
            customer_name=sow_document.customer.name
        )
        customer_contact: Dict = (
            provider.erp_client.get_default_customer_contacts(
                [sow_document.customer.crm_id]
            )
        )
        if customer_contact:
            customer_contact = customer_contact[0]
            first_name: str = customer_contact.get("first_name", "")
            last_name: str = customer_contact.get("last_name", "")
            customer_contact_details.update(
                contact_name=f"{first_name} {last_name}"
                if (first_name and last_name)
                else "",
                phone_number=format_phone_number(
                    customer_contact.get("default_phone_number")
                )
                if customer_contact.get("default_phone_number", "")
                else "",
            )
        return customer_contact_details

    @classmethod
    def get_internal_contact_details_for_service_detail_doc(
        cls, sow_document: SOWDocument
    ) -> Dict[str, str]:
        """
        Get contact details such as territory manage name, sow author name of internal users for service detail docs.

        Args:
            sow_document: SOWDocument

        Returns:
            Contact details
        """
        provider: Provider = sow_document.provider
        customer: Dict = provider.erp_client.get_customer(
            sow_document.customer.crm_id
        )
        territory_manager: Dict = provider.erp_client.get_member(
            customer.get("territory_manager_id")
        )
        internal_contact_details: Dict[str, str] = dict(
            territory_manager_name=f"{territory_manager.get('first_name')} {territory_manager.get('last_name')}",
            sow_author_name=sow_document.author.name,
        )
        return internal_contact_details

    @classmethod
    def get_render_context_for_service_detail_document(
        cls, document: SOWDocument, quote: Dict
    ) -> Dict[str, Any]:
        """
        Get render context for service detail document.

        Args:
            document: SOWDocument
            quote: Quote

        Returns:
            Render context for service detail document
        """
        provider: Provider = document.provider
        template_render_context: Dict[str, Any] = dict()
        service_cost: Dict[str, Any] = document.json_config.get(
            "service_cost", dict()
        )

        rates_and_hours_section_data: Dict[
            str, Any
        ] = cls.get_rate_and_hours_section_data_for_service_detail_doc(
            service_cost
        )
        phase_section_data: Dict[
            str, Any
        ] = cls.get_phase_section_data_for_service_detail_doc(service_cost)
        financial_summary_data: Dict[
            str, Any
        ] = cls.get_financial_summary_data_for_service_detail_doc(document)
        (
            total_sites,
            total_sites_override,
            total_cutovers,
            total_cutovers_override,
        ) = cls.get_project_assumption_variable_values(service_cost)
        contractors_data: List[
            Dict[str, Any]
        ] = cls.get_contractors_data_for_service_detail_doc(
            provider, service_cost
        )
        hourly_resources_data: List[
            Dict[str, Any]
        ] = cls.get_hourly_resources_data_for_service_detail_doc(
            provider, service_cost
        )
        current_date: str = date.today().strftime("%m/%d/%Y")
        customer_contact_details: Dict[
            str, Any
        ] = cls.get_customer_contact_details_for_service_detail_doc(document)
        internal_contact_details: Dict[
            str, Any
        ] = cls.get_internal_contact_details_for_service_detail_doc(document)

        template_render_context.update(
            current_date=current_date,
            quote_name=quote.get("name", ""),
            document_name=document.name,
            doc_type=document.doc_type,
            service_cost=service_cost,
            rates_and_hours_section_data=rates_and_hours_section_data,
            phase_section_data=phase_section_data,
            hourly_resources_data=hourly_resources_data,
            total_sites=total_sites,
            total_sites_override=total_sites_override,
            total_cutovers=total_cutovers,
            total_cutovers_override=total_cutovers_override,
            contractors_data=contractors_data,
            financial_summary_data=financial_summary_data,
            customer_contact_details=customer_contact_details,
            internal_contact_details=internal_contact_details,
        )
        return template_render_context

    @classmethod
    def create_sow_document_object(cls, payload: Dict) -> SOWDocument:
        """
        Creation SOWDocument object.

        Args:
            payload: SOWDocument data payload

        Returns:
            SOWDocument object
        """
        sow_payload: Dict[str, Any] = dict(
            customer_id=payload.get("customer"),
            user_id=payload.get("user"),
            category_id=payload.get("category"),
            json_config=payload.get("json_config"),
            doc_type=payload.get("doc_type"),
            name=payload.get("name"),
            quote_id=payload.get("quote_id"),
            major_version=payload.get("major_version"),
            minor_version=payload.get("minor_version"),
            updated_by_id=payload.get("user"),
            provider_id=payload.get("provider_id"),
            updated_on=timezone.now(),
        )
        sow_document_object: SOWDocument = SOWDocument(**sow_payload)
        return sow_document_object

    @classmethod
    def generate_document_preview(
        cls, provider: Provider, author: User, payload: Dict[str, Any]
    ) -> Dict[str, str]:
        # Original author name will be used when previewing SOW document that already exists in DB(preview during edit).
        # In case of preview before document creation, request users will be treated as author.
        original_author_name: str = payload.get("author_name", None)
        payload.update(provider_id=provider.id)
        doc: SOWDocument = cls.create_sow_document_object(payload)
        template_vars: Dict[str, Any] = cls.get_template_context(
            author,
            doc,
            provider.erp_client,
            original_author_name=original_author_name,
        )
        cls.parse_special_variables_in_sow_document(template_vars)
        html_out = cls.get_rendered_html_document(
            doc, template_vars, cls.PDF_FORMAT
        )
        html_str: str = cls.convert_html_to_str(html_out)
        file_name: str = get_sow_document_file_name(doc, cls.PDF_FORMAT)
        header: Dict = dict(source=cls.get_header_html(template_vars))
        footer: Dict = dict(source=cls.get_footer_html(template_vars))
        # Render the document locally. Upload it to S3 on Prod
        preview: Dict[str, str] = generate_pdf_using_pdf_shift(
            html_str,
            file_name,
            local_path=True,
            header=header,
            footer=footer,
            options=dict(
                margin="80px 10px 80px 10px",
            ),
        )
        if not settings.DEBUG:
            s3_path: str = FileUploadService.upload_file(
                preview.get("file_path")
            )
            preview.update(file_path=s3_path)
        return preview

    @staticmethod
    def parse_special_variables_in_sow_template(
        template_vars: Dict[str, Any]
    ) -> None:
        """
        Substitute various placeholders with values.

        Args:
            template_vars: Template render context

        Returns:
            None
        """
        json_config: Dict[str, Any] = template_vars.pop("json_config", {})
        parsed_json_config: Dict[
            str, Any
        ] = substitute_page_break_placeholder_in_sow_json_config(json_config)
        template_vars.update(json_config=parsed_json_config)
        return

    @classmethod
    def generate_template_preview(
        cls, provider: Provider, template_vars: Dict, template_type: str
    ) -> Dict[str, str]:
        """
        Generate template preview.

        Args:
            provider: Provider
            template_vars: Template vars
            template_type: Template type

        Returns:
            Template PDF preview.
        """
        current_date = date.today().strftime("%m/%d/%Y")
        additional_project_assumptions: List[
            str
        ] = cls.get_additional_project_assumptions(
            provider,
            template_vars.get("json_config", {}).get("service_cost", {}),
        )
        template_vars[
            "additional_project_assumptions"
        ] = additional_project_assumptions
        template_vars.update(
            {
                "current_date": current_date,
                "logo": cls.get_logo(),
                "banner": cls.get_banner(),
                "footer": cls.get_footer(),
            }
        )
        if template_type == SOWDocument.T_AND_M:
            cls.add_description_to_hourly_resources(template_vars)
            cls.add_default_description_to_t_and_m_resource_hours(
                template_vars
            )
        cls.parse_special_variables_in_sow_template(template_vars)
        html_page = get_template(
            SOWDocument.get_doc_template_path(template_type, cls.PDF_FORMAT)
        )
        html_out = html_page.render(template_vars)
        html_str: str = cls.convert_html_to_str(html_out)
        file_name, _ = cls._get_temp_file_path()
        header: Dict = dict(source=cls.get_header_html(template_vars))
        footer: Dict = dict(source=cls.get_footer_html(template_vars))
        preview: Dict[str, str] = generate_pdf_using_pdf_shift(
            html_str,
            file_name,
            local_path=True,
            header=header,
            footer=footer,
            options=dict(margin="80px 10px 80px 10px"),
        )
        if not settings.DEBUG:
            s3_path: str = FileUploadService.upload_file(
                preview.get("file_path")
            )
            preview.update(file_path=s3_path)
        return preview

    @classmethod
    def list_document(cls, provider, query_params=None, show_closed=False):
        """
        Returns list of SOW documents
        """
        query_params = query_params or {}
        query_params.update(dict(provider=provider))
        documents = (
            SOWDocument.objects.filter(**query_params)
            .select_related(
                "provider",
                "customer",
                "customer__address",
                "user",
                "author",
                "updated_by",
                "category",
            )
            .prefetch_related()
        )
        quotes_ids = documents.values_list("quote_id", flat=True)
        if quotes_ids:
            # Fetch and update quote details to the documents
            quotes = provider.erp_client.get_company_quote_statuses(
                list(quotes_ids)
            )
            quotes = {quote["id"]: quote for quote in quotes}
        documents = SOWDocumentSerializer(documents, many=True).data
        for document in documents:
            document["quote"] = quotes.get(document.get("quote_id"))

        result = documents
        # By Default do not show SoW’s where the quote status is No decision or closed.
        if not show_closed:
            result = []
            for doc in documents:
                if (
                    doc["quote"]
                    and doc["quote"]["status_name"].lower()
                    not in cls.CLOSED_STATUSES
                ):
                    result.append(doc)
        for doc in result:
            if doc["quote"] is not None:
                doc["quote_exist"] = True
            else:
                doc["quote_exist"] = False
                doc["quote_id"] = None
        return result

    @classmethod
    def calculate_base_internal_cost(
        cls,
        service_cost_json,
        engineering_hourly_cost=0,
        pm_hourly_cost=0,
        after_hours_cost=0,
    ):
        engineering_hours = service_cost_json.get("engineering_hours", 0)
        pm_hours = service_cost_json.get("project_management_hours", 0)
        after_hours = service_cost_json.get("after_hours", 0)
        internal_cost = (
            engineering_hours * engineering_hourly_cost
            + pm_hours * pm_hourly_cost
            + after_hours * after_hours_cost
        )
        return math.ceil(internal_cost / 10) * 10

    @classmethod
    def calculate_fixed_fee_internal_cost(
        cls,
        service_cost_json,
        engineering_hourly_cost=0,
        pm_hourly_cost=0,
        after_hours_cost=0,
    ):
        travel_cost = service_cost_json.get("travels", [{}, {}])[0].get(
            "cost", 0
        ) + service_cost_json.get("travels", [{}, {}])[1].get("cost", 0)
        contractor_cost = service_cost_json.get("contractors", [{}, {}])[
            0
        ].get("customer_cost", 0) + service_cost_json.get(
            "contractors", [{}, {}]
        )[
            1
        ].get(
            "customer_cost", 0
        )
        internal_cost = (
            travel_cost
            + contractor_cost
            + cls.calculate_base_internal_cost(
                service_cost_json,
                engineering_hourly_cost,
                pm_hourly_cost,
                after_hours_cost,
            )
        )
        return math.ceil(internal_cost / 10) * 10

    @classmethod
    def calculate_t_and_m_internal_cost(
        cls,
        service_cost_json,
        engineering_hourly_cost=0,
        pm_hourly_cost=0,
        after_hours_cost=0,
    ):
        internal_cost = cls.calculate_base_internal_cost(
            service_cost_json,
            engineering_hourly_cost,
            pm_hourly_cost,
            after_hours_cost,
        )
        return internal_cost

    @classmethod
    def get_document_margin(cls, sow_document: SOWDocument) -> int:
        sow_calculator: SoWDocumentCalculation = SoWDocumentCalculation(
            sow_document.provider, sow_document
        )
        margin: int = sow_calculator.get_total_margin()
        return margin

    @staticmethod
    def get_internal_cost_risk(service_cost_data, sow_settings):
        """
        Method to calculate internal cost risk for service cost data of a document
        Args:
            service_cost_data: Document service cost data
            sow_settings: Provider sow document settings

        Returns:
            Internal cost risk
        """
        engineering_internal_cost = (
            service_cost_data.get("engineering_hours", 0)
            * sow_settings.engineering_hourly_cost
        )
        engineering_hours_customer_cost = service_cost_data.get(
            "engineering_hours", 0
        ) * service_cost_data.get("engineering_hourly_rate", 0)

        try:
            engineering_cost_margin_percent = abs(
                round(
                    (
                        (
                            engineering_hours_customer_cost
                            - float(engineering_internal_cost)
                        )
                        / engineering_hours_customer_cost
                    ),
                    2,
                )
            )
        except ZeroDivisionError:
            engineering_cost_margin_percent = 0

        risk_budget = DocumentService.calculate_risk_budget(
            service_cost_data, sow_settings
        )
        risk_budget_margin = round(
            (risk_budget * engineering_cost_margin_percent), 2
        )
        internal_cost_risk = round((risk_budget - risk_budget_margin), 2)
        return internal_cost_risk

    @staticmethod
    def calculate_risk_budget(service_cost_data, sow_settings):
        risk_budget = (
            (
                service_cost_data.get("engineering_hours", 0)
                * service_cost_data.get("engineering_hourly_rate", 0)
            )
            + (
                service_cost_data.get("project_management_hours", 0)
                * service_cost_data.get("project_management_hourly_rate", 0)
            )
            + (
                service_cost_data.get("after_hours", 0)
                * service_cost_data.get("after_hours_rate", 0)
            )
        ) * sow_settings.fixed_fee_variable
        return risk_budget
