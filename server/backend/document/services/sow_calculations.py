from accounts.models import Provider, Customer
from document.models import SOWDocument, SOWDocumentSettings
from document.exceptions import SoWDocumentSettingNotConfigured
from typing import List, Dict, Optional, Set, Union, Any
from dataclasses import dataclass, field, asdict


# As of now for normal Sow docs (either T&M & Fixed Fee), contractors & travel apply for
# only Fixed Fee documents. Risk budget is also calculated for only Fixed Fee documents.

# Same applies to CR SoW documents (CR type i.e. FixedFee & T&M does not matter).
# Requirement not provided yet.
@dataclass
class ServiceCostCalculation:
    """
    A dataclass that peforms all SoW document service cost calculations.
    """

    sow_doc_type: str
    sow_doc_setting: SOWDocumentSettings
    engineering_hours: int = 0
    engineering_hourly_rate: int = 0
    after_hours: int = 0
    after_hours_rate: int = 0
    project_management_hours: int = 0
    project_management_hourly_rate: int = 0
    integration_technician_hours: int = 0
    integration_technician_hourly_rate: int = 0
    travels: Optional[List[Dict]] = None
    contractors: Optional[List[Dict]] = None
    hourly_resources: Optional[List[Dict]] = None
    calculated_values: Dict[str, Any] = field(init=False)

    def __post_init__(self):
        self.calculated_values: Dict[
            str, Union[int, float]
        ] = self.calculate_all_values()

    def calculate_engineering_hour_values(
        self,
    ) -> Dict[str, Union[int, float]]:
        """
        Calculates the engineering hour values based on the hourly rate, internal cost and the margin percent.

        Returns:
            Dict: The dictionary with keys 'engineering_hour_customer_cost', 'engineering_hour_internal_cost',
            'engineering_hour_margin', 'engineering_hour_margin_percent' and their respective values.

        """
        engineering_hour_customer_cost: int = round(
            self.engineering_hours * self.engineering_hourly_rate
        )
        engineering_hour_internal_cost: int = round(
            self.engineering_hours
            * self.sow_doc_setting.engineering_hourly_cost
        )
        engineering_hour_margin: int = (
            engineering_hour_customer_cost - engineering_hour_internal_cost
        )
        engineering_hour_margin_percent: float = (
            self._calculate_margin_percent(
                engineering_hour_margin, engineering_hour_customer_cost
            )
        )
        return dict(
            engineering_hour_customer_cost=engineering_hour_customer_cost,
            engineering_hour_internal_cost=engineering_hour_internal_cost,
            engineering_hour_margin=engineering_hour_margin,
            engineering_hour_margin_percent=engineering_hour_margin_percent,
        )

    def calculate_engineering_after_hour_values(
        self,
    ) -> Dict[str, Union[int, float]]:
        """
        Calculates the customer cost, internal cost, margin, and margin percentage for after-hours engineering work.

        Returns:
            Dict: A dictionary containing the customer cost, internal cost, margin, and margin percentage
            for after-hours engineering work.
        """
        after_hours_customer_cost: int = round(
            self.after_hours * self.after_hours_rate
        )
        after_hours_internal_cost: int = round(
            self.after_hours * self.sow_doc_setting.after_hours_cost
        )
        after_hours_margin: int = (
            after_hours_customer_cost
        ) - after_hours_internal_cost
        after_hours_margin_percent: float = self._calculate_margin_percent(
            after_hours_margin, after_hours_customer_cost
        )
        return dict(
            after_hours_customer_cost=after_hours_customer_cost,
            after_hours_internal_cost=after_hours_internal_cost,
            after_hours_margin=after_hours_margin,
            after_hours_margin_percent=after_hours_margin_percent,
        )

    def calculate_integration_technician_hour_values(
        self,
    ) -> Dict[str, Union[int, float]]:
        """
        Calculate the customer cost, internal cost, margin, and margin percentage for integration technician hours.

        Returns:
            Dict: A dictionary containing the customer cost, internal cost, margin, and margin percentage for
            integration technician hours.
        """
        integration_technician_customer_cost: int = round(
            self.integration_technician_hours
            * self.integration_technician_hourly_rate
        )
        integration_technician_internal_cost: int = round(
            self.integration_technician_hours
            * self.sow_doc_setting.integration_technician_hourly_cost
        )
        integration_technician_hours_margin: int = (
            integration_technician_customer_cost
            - integration_technician_internal_cost
        )
        integration_technician_hours_margin_percent: float = (
            self._calculate_margin_percent(
                integration_technician_hours_margin,
                integration_technician_customer_cost,
            )
        )
        return dict(
            integration_technician_customer_cost=integration_technician_customer_cost,
            integration_technician_internal_cost=integration_technician_internal_cost,
            integration_technician_hours_margin=integration_technician_hours_margin,
            integration_technician_hours_margin_percent=integration_technician_hours_margin_percent,
        )

    def calculate_project_management_hour_values(
        self,
    ) -> Dict[str, Union[int, float]]:
        """
        Calculate the customer cost, internal cost, margin, and margin percentage for project management hours.

        Returns:
            Dict: A dictionary containing the customer cost, internal cost, margin, and margin percentage for
            project management hours.
        """
        project_management_customer_cost: int = round(
            self.project_management_hours * self.project_management_hourly_rate
        )
        project_management_internal_cost: int = round(
            self.project_management_hours * self.sow_doc_setting.pm_hourly_cost
        )
        project_management_hours_margin: int = (
            project_management_customer_cost - project_management_internal_cost
        )
        project_management_hours_margin_percent: float = (
            self._calculate_margin_percent(
                project_management_hours_margin,
                project_management_customer_cost,
            )
        )
        return dict(
            project_management_customer_cost=project_management_customer_cost,
            project_management_internal_cost=project_management_internal_cost,
            project_management_hours_margin=project_management_hours_margin,
            project_management_hours_margin_percent=project_management_hours_margin_percent,
        )

    def calculate_professional_service_values(
        self,
        engineering_hour_customer_cost: int,
        engineering_hour_internal_cost: int,
        after_hours_customer_cost: int,
        after_hours_internal_cost: int,
        project_management_customer_cost: int,
        project_management_internal_cost: int,
        integration_technician_customer_cost: int,
        integration_technician_internal_cost: int,
    ) -> Dict[str, Union[int, float]]:
        """
        Calculates the professional services values based on the input costs.

        Args:
            engineering_hour_customer_cost (int): Cost of engineering hours to the customer.
            engineering_hour_internal_cost (int): Cost of engineering hours internally.
            after_hours_customer_cost (int): Cost of after hours services to the customer.
            after_hours_internal_cost (int): Cost of after hours services internally.
            project_management_customer_cost (int): Cost of project management to the customer.
            project_management_internal_cost (int): Cost of project management internally.

        Returns:
            Dict[str, Union[int, float]]: A dictionary containing the following keys:
                professional_services_customer_cost (int): Total cost of professional services to the customer.
                professional_services_internal_cost (int): Total cost of professional services internally.
                professional_services_margin (int): Difference between the customer cost and internal cost.
                professional_services_margin_percent (float): Margin percentage calculated from
                    professional_services_margin and professional_services_customer_cost.
        """
        professional_services_customer_cost: int = 0
        professional_services_customer_cost += (
            engineering_hour_customer_cost
            + after_hours_customer_cost
            + project_management_customer_cost
            + integration_technician_customer_cost
        )

        professional_services_internal_cost: int = 0
        professional_services_internal_cost += (
            engineering_hour_internal_cost
            + after_hours_internal_cost
            + project_management_internal_cost
            + integration_technician_internal_cost
        )

        professional_services_margin: int = (
            professional_services_customer_cost
            - professional_services_internal_cost
        )
        professional_services_margin_percent: float = (
            self._calculate_margin_percent(
                professional_services_margin,
                professional_services_customer_cost,
            )
        )
        return dict(
            professional_services_customer_cost=professional_services_customer_cost,
            professional_services_internal_cost=professional_services_internal_cost,
            professional_services_margin=professional_services_margin,
            professional_services_margin_percent=professional_services_margin_percent,
        )

    def calculate_hourly_resources_values(
        self,
    ) -> Dict[str, Optional[Union[int, float]]]:
        """
        Calculates the customer cost, internal cost, margin, and margin percentage for hourly resources.

        Returns:
            Dict[str, Optional[Union[int, float]]]: A dictionary containing the customer cost, internal cost,
            margin, and margin percentage for hourly resources. The values may be None if `hourly_resources` is not set.
        """
        hourly_resources_customer_cost: Optional[int] = None
        hourly_resources_internal_cost: Optional[int] = None
        hourly_resources_margin: Optional[int] = None
        hourly_resources_margin_percent: Optional[float] = None
        if self.hourly_resources:
            hourly_resources_customer_cost: int = (
                self.get_hourly_resources_customer_cost()
            )
            hourly_resources_internal_cost: int = (
                self.get_hourly_resources_internal_cost()
            )
            hourly_resources_margin: int = (
                hourly_resources_customer_cost - hourly_resources_internal_cost
            )
            hourly_resources_margin_percent: float = (
                self._calculate_margin_percent(
                    hourly_resources_margin, hourly_resources_customer_cost
                )
            )
        return dict(
            hourly_resources_customer_cost=hourly_resources_customer_cost,
            hourly_resources_internal_cost=hourly_resources_internal_cost,
            hourly_resources_margin=hourly_resources_margin,
            hourly_resources_margin_percent=hourly_resources_margin_percent,
        )

    def calculate_total_travel_cost(self) -> Optional[int]:
        """
        Calculates the total cost of travel for a SOW document of type 'GENERAL'.

        Returns:
            Optional[int]: Total travel cost, if SOW document is of type 'GENERAL' and there are travels.
            Otherwise, returns None.
        """
        # Travel cost only applies for Fixed Fee docs
        total_travel_cost: Optional[int] = None
        if self.travels and (
            self.sow_doc_type
            in [SOWDocument.GENERAL, SOWDocument.CHANGE_REQUEST]
        ):
            total_travel_cost: float = 0
            for travel in self.travels:
                total_travel_cost += travel.get("cost", 0)
            return round(total_travel_cost)
        return total_travel_cost

    def calculate_contractor_values(
        self,
    ) -> Dict[str, Optional[Union[int, float]]]:
        """
        Calculate the customer cost, internal cost, margin and margin percent for contractors.

        Returns:
            dict: A dictionary containing the contractors customer cost, internal cost, margin and margin percent values.
                  The values will be None if contractors don't exist or the SOW document type is not `GENERAL`.
        """
        contractors_customer_cost: Optional[int] = None
        contractors_internal_cost: Optional[int] = None
        contractors_margin: Optional[int] = None
        contractors_margin_percent: Optional[float] = None
        # Contractors only apply for Fixed Fee docs and Change Request docs.
        if self.contractors and (
            self.sow_doc_type
            in [SOWDocument.GENERAL, SOWDocument.CHANGE_REQUEST]
        ):
            contractors_customer_cost: int = (
                self.get_total_contractor_customer_cost()
            )
            contractors_internal_cost: int = (
                self.get_total_contractor_internal_cost()
            )
            contractors_margin: int = (
                contractors_customer_cost - contractors_internal_cost
            )
            contractors_margin_percent: float = self._calculate_margin_percent(
                contractors_margin, contractors_customer_cost
            )
        return dict(
            contractors_customer_cost=contractors_customer_cost,
            contractors_internal_cost=contractors_internal_cost,
            contractors_margin=contractors_margin,
            contractors_margin_percent=contractors_margin_percent,
        )

    def calculate_risk_budget_values(
        self,
        engineering_hour_calculations: Dict[str, Union[int, float]],
        after_hours_calculations: Dict[str, Union[int, float]],
        project_management_hours_calculations: Dict[str, Union[int, float]],
        hourly_resources_calculations: Dict[str, Union[int, float]],
        integration_technician_calculations: Dict[str, Union[int, float]],
    ) -> Dict[str, Optional[Union[int, float]]]:
        """
        Calculates the risk budget values including customer cost, internal cost, margin, and margin percent.

        Risk budget calculations only apply for SOW documents of type `GENERAL`.

        Args:
        - engineering_hour_calculations (int): Engineering hour calculations
        - after_hours_calculations (float): After hour calculations
        - project_management_hours_calculations (int): Project management hour calculations
        - hourly_resources_calculations (int): Hourly resource calculations
        - integration_technician_calculations (Optional[int]): Integration technician calculations

        Returns:
        - Dict[str, Optional[Union[int, float]]]: Dictionary containing risk budget customer cost, internal cost,
          margin, and margin percent
        """
        risk_budget_customer_cost: Optional[int] = None
        risk_budget_internal_cost: Optional[int] = None
        risk_budget_margin: Optional[int] = None
        risk_budget_margin_percent: Optional[float] = None
        # Risk budget only applies for Fixed Fee docs
        if self.sow_doc_type in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            risk_budget_customer_cost: int = (
                self.get_risk_budget_customer_cost(
                    engineering_hour_calculations.get(
                        "engineering_hour_customer_cost", 0
                    ),
                    after_hours_calculations.get(
                        "after_hours_customer_cost", 0
                    ),
                    project_management_hours_calculations.get(
                        "project_management_customer_cost", 0
                    ),
                    hourly_resources_calculations.get(
                        "hourly_resources_customer_cost", 0
                    ),
                    integration_technician_calculations.get(
                        "integration_technician_customer_cost", 0
                    ),
                )
            )
            risk_budget_internal_cost: int = (
                self.get_risk_budget_internal_cost(
                    engineering_hour_calculations.get(
                        "engineering_hour_internal_cost", 0
                    ),
                    after_hours_calculations.get(
                        "after_hours_internal_cost", 0
                    ),
                    project_management_hours_calculations.get(
                        "project_management_internal_cost", 0
                    ),
                    hourly_resources_calculations.get(
                        "hourly_resources_internal_cost", 0
                    ),
                    integration_technician_calculations.get(
                        "integration_technician_internal_cost", 0
                    ),
                )
            )
            risk_budget_margin: int = (
                risk_budget_customer_cost - risk_budget_internal_cost
            )
            risk_budget_margin_percent: float = self._calculate_margin_percent(
                risk_budget_margin, risk_budget_customer_cost
            )
        return dict(
            risk_budget_customer_cost=risk_budget_customer_cost,
            risk_budget_internal_cost=risk_budget_internal_cost,
            risk_budget_margin=risk_budget_margin,
            risk_budget_margin_percent=risk_budget_margin_percent,
        )

    @staticmethod
    def _calculate_margin_percent(
        margin: Union[int, float], customer_cost: Union[int, float]
    ) -> float:
        """
        Calculate margin percent.

        Args:
            margin: Margin
            customer_cost: Customer cost

        Returns:
            Margin percent
        """
        try:
            margin_percent: float = (margin / customer_cost) * 100
        except ZeroDivisionError:
            margin_percent = 0
        return round(margin_percent, 1)

    def get_hourly_resources_customer_cost(self) -> int:
        """
        Return the rounded down total customer cost of all hourly resources.

        Args:
            self (object): The instance of the object being processed.

        Returns:
            int: The rounded down total customer cost of all hourly resources.
        """
        total_hourly_resources_customer_cost: float = 0
        for hourly_contractor in self.hourly_resources:
            total_hourly_resources_customer_cost += hourly_contractor.get(
                "customer_cost", 0
            )
        return round(total_hourly_resources_customer_cost)

    def get_hourly_resources_internal_cost(self) -> int:
        """
        Return the rounded down total internal cost of all hourly resources.

        Args:
            self (object): The instance of the object being processed.

        Returns:
            int: The rounded down total internal cost of all hourly resources.
        """
        total_hourly_contractor_internal_cost: float = 0
        for hourly_contractor in self.hourly_resources:
            total_hourly_contractor_internal_cost += hourly_contractor.get(
                "internal_cost", 0
            )
        return round(total_hourly_contractor_internal_cost)

    def get_total_contractor_customer_cost(self) -> int:
        """
        Calculate the total customer cost of all contractors.

        Returns:
            int: Total customer cost of all contractors.
        """
        total_contractor_customer_cost: float = 0
        for contractor in self.contractors:
            total_contractor_customer_cost += contractor.get(
                "customer_cost", 0
            )
        return round(total_contractor_customer_cost)

    def get_total_contractor_internal_cost(self) -> int:
        """
        Calculate the total internal cost of all contractors.

        Returns:
            int: Total internal cost of all contractors.
        """
        total_contractor_internal_cost: float = 0
        for contractor in self.contractors:
            total_contractor_internal_cost += contractor.get("partner_cost", 0)
        return round(total_contractor_internal_cost)

    def get_risk_budget_customer_cost(
        self,
        engineering_hour_customer_cost: Optional[int],
        after_hours_customer_cost: Optional[int],
        project_management_customer_cost: Optional[int],
        hourly_resources_customer_cost: Optional[int],
        integration_technician_customer_cost: Optional[int],
    ) -> int:
        """
        Calculate the risk budget customer cost.

        Args:
            engineering_hour_customer_cost (int): Customer cost for engineering hours.
            after_hours_customer_cost (int): Customer cost for after hours.
            project_management_customer_cost (int): Customer cost for project management.
            hourly_resources_customer_cost (Optional[int]): Customer cost for hourly resources.
            integration_technician_customer_cost (int): Customer cost for integration technician

        Returns:
            int: Risk budget customer cost rounded to the nearest integer.
        """
        risk_budget_customer_cost: int = 0
        if engineering_hour_customer_cost:
            risk_budget_customer_cost += engineering_hour_customer_cost
        if after_hours_customer_cost:
            risk_budget_customer_cost += after_hours_customer_cost
        if project_management_customer_cost:
            risk_budget_customer_cost += project_management_customer_cost
        if hourly_resources_customer_cost:
            risk_budget_customer_cost += hourly_resources_customer_cost
        if integration_technician_customer_cost:
            risk_budget_customer_cost += integration_technician_customer_cost
        return round(
            risk_budget_customer_cost * self.sow_doc_setting.fixed_fee_variable
        )

    def get_risk_budget_internal_cost(
        self,
        engineering_hour_internal_cost: Optional[int],
        after_hours_internal_cost: Optional[int],
        project_management_internal_cost: Optional[int],
        hourly_resources_internal_cost: Optional[int],
        integration_technician_internal_cost: Optional[int],
    ) -> int:
        """
        Calculate the risk budget customer cost.

        Args:
            engineering_hour_internal_cost (int): Internal cost for engineering hours.
            after_hours_internal_cost (int): Internal cost for after hours.
            project_management_internal_cost (int): Internal cost for project management.
            hourly_resources_internal_cost (Optional[int]): Internal cost for hourly resources.
            integration_technician_internal_cost (int): Internal cost for integration technician

        Returns:
            int: Risk budget internal cost rounded to the nearest integer.
        """
        risk_budget_internal_cost: int = 0
        if engineering_hour_internal_cost:
            risk_budget_internal_cost += engineering_hour_internal_cost
        if after_hours_internal_cost:
            risk_budget_internal_cost += after_hours_internal_cost
        if project_management_internal_cost:
            risk_budget_internal_cost += project_management_internal_cost
        if hourly_resources_internal_cost:
            risk_budget_internal_cost += hourly_resources_internal_cost
        if integration_technician_internal_cost:
            risk_budget_internal_cost += integration_technician_internal_cost
        return round(
            risk_budget_internal_cost * self.sow_doc_setting.fixed_fee_variable
        )

    @staticmethod
    def get_risk_budget_margin(
        risk_budget_customer_cost: int,
        engineering_hour_margin_percent: float,
    ) -> int:
        """
        Calculate the risk budget margin.

        Args:
            risk_budget_customer_cost (int): Customer cost for the risk budget.
            engineering_hour_margin_percent (float): Margin percent for engineering hours.

        Returns:
            int: Risk budget margin rounded to the nearest integer.
        """
        return round(
            risk_budget_customer_cost
            * round(engineering_hour_margin_percent / 100, 3)
        )

    @staticmethod
    def get_risk_budget_margin_percent(
        risk_budget_margin: int, risk_budget_cutsomer_cost: int
    ) -> float:
        """
        Calculate the risk budget margin percent.

        Args:
            risk_budget_margin (int): Risk budget margin.
            risk_budget_cutsomer_cost (int): Customer cost for the risk budget.

        Returns:
            float: Risk budget margin percent rounded to 1 decimal place.
        """
        try:
            risk_budget_margin_percent: float = (
                risk_budget_margin / risk_budget_cutsomer_cost
            ) * 100
        except ZeroDivisionError:
            risk_budget_margin_percent = 0
        return round(risk_budget_margin_percent, 1)

    def calculate_total_customer_cost(
        self,
        engineering_hour_customer_cost: int,
        after_hours_customer_cost: int,
        project_management_customer_cost: int,
        hourly_resources_customer_cost: Optional[int],
        total_travel_cost: Optional[int],
        contractors_customer_cost: Optional[int],
        risk_budget_customer_cost: Optional[int],
        integration_technician_customer_cost: int,
    ) -> int:
        """
        Calculate the total customer cost.

        Args:
            engineering_hour_customer_cost (int): The customer cost for engineering hours.
            after_hours_customer_cost (int): The customer cost for after hours work.
            project_management_customer_cost (int): The customer cost for project management.
            hourly_resources_customer_cost (Optional[int]): The customer cost for hourly resources, if any.
            total_travel_cost (Optional[int]): The customer cost for total travel, if any.
            contractors_customer_cost (Optional[int]): The customer cost for contractors, if any.
            risk_budget_customer_cost (Optional[int]): The customer cost for risk budget, if any.
            integration_technician_customer_cost (int): The customer cost for integration technician.

        Returns:
            int: The total customer cost.
        """
        total_customer_cost: int = (
            engineering_hour_customer_cost
            + after_hours_customer_cost
            + project_management_customer_cost
            + integration_technician_customer_cost
        )
        if hourly_resources_customer_cost:
            total_customer_cost += hourly_resources_customer_cost

        if self.sow_doc_type in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            if total_travel_cost:
                total_customer_cost += total_travel_cost
            if contractors_customer_cost:
                total_customer_cost += contractors_customer_cost
            if risk_budget_customer_cost:
                total_customer_cost += risk_budget_customer_cost
        return total_customer_cost

    def calculate_total_internal_cost(
        self,
        engineering_hour_internal_cost: int,
        after_hours_internal_cost: int,
        project_management_internal_cost: int,
        hourly_resources_internal_cost: Optional[int],
        contractors_internal_cost: Optional[int],
        risk_budget_internal_cost: Optional[int],
        integration_technician_internal_cost: int,
    ) -> int:
        """
        Calculate the total internal cost.

        Args:
            engineering_hour_internal_cost (int): Internal cost of engineering hours.
            after_hours_internal_cost (int): Internal cost of after hours.
            project_management_internal_cost (int): Internal cost of project management.
            hourly_resources_internal_cost (Optional[int]): Internal cost of hourly resources.
            contractors_internal_cost (Optional[int]): Internal cost of contractors.
            risk_budget_internal_cost (Optional[int]): Internal cost of risk budget.
            integration_technician_internal_cost (int): Internal cost of integration technician

        Returns:
            int: Total internal cost.
        """
        total_internal_cost: int = (
            engineering_hour_internal_cost
            + after_hours_internal_cost
            + project_management_internal_cost
            + integration_technician_internal_cost
        )
        if hourly_resources_internal_cost:
            total_internal_cost += hourly_resources_internal_cost

        if self.sow_doc_type in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            if contractors_internal_cost:
                total_internal_cost += contractors_internal_cost
            if risk_budget_internal_cost:
                total_internal_cost += risk_budget_internal_cost
        return total_internal_cost

    @staticmethod
    def get_total_margin_percent(
        total_margin: int, total_customer_cost: int
    ) -> float:
        """
        Calculate the total margin percentage.

        Args:
            total_margin (int): The total margin.
            total_customer_cost (int): The total customer cost.

        Returns:
            float: The total margin percentage rounded to 1 decimal place.

        Raises:
            ZeroDivisionError: If total_customer_cost is 0.
        """
        try:
            total_margin_percent: float = (
                total_margin / total_customer_cost
            ) * 100
        except ZeroDivisionError:
            total_margin_percent = 0
        return round(total_margin_percent, 1)

    def calculate_all_values(self) -> Dict:
        """
        Calculates various costs, such as engineering hours, after hours, project management hours, hourly resources,
        contractors, risk budget, and totals.
        The calculated values include customer cost, internal cost, margin, and margin percent.

        Returns:
            A dictionary with all calculated values.
        """
        engineering_hour_calculations: Dict[
            str, Union[int, float]
        ] = self.calculate_engineering_hour_values()
        engineering_hour_customer_cost: int = (
            engineering_hour_calculations.get("engineering_hour_customer_cost")
        )
        engineering_hour_internal_cost: int = (
            engineering_hour_calculations.get("engineering_hour_internal_cost")
        )

        after_hours_calculations: Dict[
            str, Union[int, float]
        ] = self.calculate_engineering_after_hour_values()
        after_hours_customer_cost: int = after_hours_calculations.get(
            "after_hours_customer_cost", 0
        )
        after_hours_internal_cost: int = after_hours_calculations.get(
            "after_hours_internal_cost", 0
        )

        integration_technician_calculations: Dict[
            str, Union[int, float]
        ] = self.calculate_integration_technician_hour_values()
        integration_technician_customer_cost: int = (
            integration_technician_calculations.get(
                "integration_technician_customer_cost"
            )
        )
        integration_technician_internal_cost: int = (
            integration_technician_calculations.get(
                "integration_technician_internal_cost"
            )
        )

        project_management_hours_calculations: Dict[
            str, Union[int, float]
        ] = self.calculate_project_management_hour_values()
        project_management_customer_cost: int = (
            project_management_hours_calculations.get(
                "project_management_customer_cost", 0
            )
        )
        project_management_internal_cost: int = (
            project_management_hours_calculations.get(
                "project_management_internal_cost", 0
            )
        )

        professional_service_calculations: Dict[
            str, Union[int, float]
        ] = self.calculate_professional_service_values(
            engineering_hour_customer_cost,
            engineering_hour_internal_cost,
            after_hours_customer_cost,
            after_hours_internal_cost,
            project_management_customer_cost,
            project_management_internal_cost,
            integration_technician_customer_cost,
            integration_technician_internal_cost,
        )

        hourly_resources_calculations: Dict[
            str, Optional[Union[int, float]]
        ] = self.calculate_hourly_resources_values()
        hourly_resources_customer_cost: Optional[
            int
        ] = hourly_resources_calculations.get(
            "hourly_resources_customer_cost", None
        )
        hourly_resources_internal_cost: Optional[
            int
        ] = hourly_resources_calculations.get(
            "hourly_resources_internal_cost", None
        )

        contractors_calculations: Dict[
            str, Optional[Union[int, float]]
        ] = self.calculate_contractor_values()
        contractors_customer_cost: Optional[
            int
        ] = contractors_calculations.get("contractors_customer_cost")
        contractors_internal_cost: Optional[
            int
        ] = contractors_calculations.get("contractors_internal_cost")

        total_travel_cost: Optional[int] = self.calculate_total_travel_cost()

        risk_budget_calculations: Dict[
            str, Optional[Union[int, float]]
        ] = self.calculate_risk_budget_values(
            engineering_hour_calculations,
            after_hours_calculations,
            project_management_hours_calculations,
            hourly_resources_calculations,
            integration_technician_calculations,
        )
        risk_budget_customer_cost: Optional[
            int
        ] = risk_budget_calculations.get("risk_budget_customer_cost")
        risk_budget_internal_cost: Optional[
            int
        ] = risk_budget_calculations.get("risk_budget_internal_cost")

        total_customer_cost: int = self.calculate_total_customer_cost(
            engineering_hour_customer_cost,
            after_hours_customer_cost,
            project_management_customer_cost,
            hourly_resources_customer_cost,
            total_travel_cost,
            contractors_customer_cost,
            risk_budget_customer_cost,
            integration_technician_customer_cost,
        )
        total_internal_cost: int = self.calculate_total_internal_cost(
            engineering_hour_internal_cost,
            after_hours_internal_cost,
            project_management_internal_cost,
            hourly_resources_internal_cost,
            contractors_internal_cost,
            risk_budget_internal_cost,
            integration_technician_internal_cost,
        )
        total_margin: int = total_customer_cost - total_internal_cost
        total_margin_percent: float = self._calculate_margin_percent(
            total_margin, total_customer_cost
        )

        all_calculated_values: Dict = dict(
            **engineering_hour_calculations,
            **after_hours_calculations,
            **project_management_hours_calculations,
            **hourly_resources_calculations,
            **contractors_calculations,
            **risk_budget_calculations,
            **professional_service_calculations,
            **integration_technician_calculations,
            total_travel_cost=total_travel_cost,
            total_customer_cost=total_customer_cost,
            total_internal_cost=total_internal_cost,
            total_margin=total_margin,
            total_margin_percent=total_margin_percent
        )
        return all_calculated_values


class SoWDocumentCalculation:
    """
    Class that handles calculations related to SOWDocuments. All the calculations are performed by
    ServiceCostCalculation class. This class provides interface for getting various calculated values.
    """

    def __init__(self, provider: Provider, sow_document: SOWDocument) -> None:
        """
        Initialize instance by setting _calculation_data varible which stored all pre computed service cost values.

        Args:
            provider: Provider
            sow_document: SOWDocument
        """
        self._provider = provider
        self._sow_document = sow_document
        self._service_cost_calculation_class = ServiceCostCalculation
        self._calculation_data: Dict[
            str, Union[int, float, None]
        ] = self.get_calculation_data()

    def get_sow_doc_settings(self) -> SOWDocumentSettings:
        """
        Get configured SOW document settings for the provider. If not configured, raises error.

        Returns: SOWDocumentSettings instance
        """
        try:
            sow_document_setting: SOWDocumentSettings = (
                self._provider.sow_doc_settings
            )
        except SOWDocumentSettings.DoesNotExist:
            raise SoWDocumentSettingNotConfigured()
        return sow_document_setting

    def get_calculation_class_instance(
        self, service_cost: Dict
    ) -> ServiceCostCalculation:
        """
        Initializes and returns instance of ServiceCostCalculation.

        Args:
            service_cost (Dict) : Service cost data

        Returns:
            ServiceCostCalculation instance
        """
        sow_doc_type: str = self._sow_document.doc_type
        sow_doc_setting: SOWDocumentSettings = self.get_sow_doc_settings()
        calculation_payload: Dict = dict(
            sow_doc_type=sow_doc_type,
            sow_doc_setting=sow_doc_setting,
            engineering_hours=service_cost.get("engineering_hours"),
            engineering_hourly_rate=service_cost.get(
                "engineering_hourly_rate"
            ),
            after_hours=service_cost.get("after_hours"),
            after_hours_rate=service_cost.get("after_hours_rate"),
            project_management_hours=service_cost.get(
                "project_management_hours"
            ),
            project_management_hourly_rate=service_cost.get(
                "project_management_hourly_rate"
            ),
            integration_technician_hours=service_cost.get(
                "integration_technician_hours"
            ),
            integration_technician_hourly_rate=service_cost.get(
                "integration_technician_hourly_rate"
            ),
            hourly_resources=service_cost.get("hourly_resources"),
        )
        # Contractors exist only for Fixed Fee & Change Request documents
        if sow_doc_type in [SOWDocument.GENERAL, SOWDocument.CHANGE_REQUEST]:
            calculation_payload.update(
                contractors=service_cost.get("contractors"),
                travels=service_cost.get("travels"),
            )
        else:
            calculation_payload.update(
                contractors=[],
                travels=[],
            )
        service_cost_calculation_class_instance: ServiceCostCalculation = (
            self._service_cost_calculation_class(**calculation_payload)
        )
        return service_cost_calculation_class_instance

    def get_calculation_data(self) -> Dict[str, Union[int, float, None]]:
        """
        Returns all calculated values computed by ServiceCostCalculation class.

        Returns:
            Calculation data
        """
        json_config: Optional[Dict] = self._sow_document.json_config or dict()
        service_cost: Dict = json_config.get("service_cost", dict())
        if not service_cost:
            return dict()
        service_cost_calculator: ServiceCostCalculation = (
            self.get_calculation_class_instance(service_cost)
        )
        calculation_data: Dict[
            str, Union[int, float, None]
        ] = service_cost_calculator.calculated_values
        return calculation_data

    def get_total_revenue(self) -> int:
        """
        Get total revenue for a SOW document.

        Returns:
            Total revenue
        """
        total_revenue: int = self._calculation_data.get("total_customer_cost")
        return total_revenue

    def get_total_cost(self) -> int:
        """
        Get total cost for a SOW document.

        Returns:
            Total cost
        """
        total_cost: int = self._calculation_data.get("total_internal_cost")
        return total_cost

    def get_total_margin(self) -> int:
        """
        Get total margin for a SOW document.

        Returns:
            Total margin
        """
        total_margin: int = self._calculation_data.get("total_margin")
        return total_margin

    def get_total_gross_profit_percent(self) -> float:
        """
        Get total gross profit percent for a SOW document.

        Returns:
            Gross profit percent
        """
        total_margin_percent: float = self._calculation_data.get(
            "total_margin_percent"
        )
        return total_margin_percent

    def get_professional_services_summary(
        self,
    ) -> Dict[str, Union[int, float]]:
        """
        Get professional service summary for a SOW document.

        Returns:
            Professional services summary
        """
        professional_services_summary: Dict[str, Union[int, float]] = dict(
            professional_services_revenue=self._calculation_data.get(
                "professional_services_customer_cost"
            ),
            professional_services_cost=self._calculation_data.get(
                "professional_services_internal_cost"
            ),
            professional_services_margin=self._calculation_data.get(
                "professional_services_margin"
            ),
            professional_services_gross_profit_percent=self._calculation_data.get(
                "professional_services_margin_percent"
            ),
        )
        return professional_services_summary

    def get_contractors_summary(self) -> Dict[str, Union[int, float]]:
        """
        Get contractors summary for a SOW document. The summary includes various calcualated values related to
        contractors.

        Returns:
            Contractors summary
        """
        contractors_summary: Dict[str, Union[str, float]] = dict(
            contractors_customer_cost=self._calculation_data.get(
                "contractors_customer_cost"
            ),
            contractors_internal_cost=self._calculation_data.get(
                "contractors_internal_cost"
            ),
            contractors_margin=self._calculation_data.get(
                "contractors_margin"
            ),
            contractors_margin_percent=self._calculation_data.get(
                "contractors_margin_percent"
            ),
        )
        return contractors_summary

    def get_risk_budget_summary(self) -> Dict[str, Union[int, float]]:
        """
        Get risk budget summary for a SOW document. The summary includes various calcualated values related to risk
        budget.

        Returns:
            Risk budget summary
        """
        if self._sow_document.doc_type in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            risk_budget_summary: Dict[str, Union[int, float]] = dict(
                risk_budget_customer_cost=self._calculation_data.get(
                    "risk_budget_customer_cost"
                ),
                risk_budget_internal_cost=self._calculation_data.get(
                    "risk_budget_internal_cost"
                ),
                risk_budget_margin=self._calculation_data.get(
                    "risk_budget_margin"
                ),
                risk_budget_margin_percent=self._calculation_data.get(
                    "risk_budget_margin_percent"
                ),
            )
            return risk_budget_summary
        else:
            return {}

    def get_total_travel_cost(self) -> Optional[int]:
        """
        Get total travel cost for a SOW document.

        Returns:
            Total travel cost
        """
        total_travel_cost: Optional[int] = None
        if self._sow_document.doc_type in [
            SOWDocument.GENERAL,
            SOWDocument.CHANGE_REQUEST,
        ]:
            total_travel_cost: int = self._calculation_data.get(
                "total_travel_cost"
            )
        return total_travel_cost

    def get_hourly_resources_summary(self) -> Dict[str, Union[int, float]]:
        """
        Get hourly resources summary for a SOW document. The summary includes various calcualated values related to
        hourly resources.

        Returns:
            Hourly resources summary
        """
        hourly_resources_summary: Dict[str, Union[int, float]] = dict(
            hourly_resources_customer_cost=self._calculation_data.get(
                "hourly_resources_customer_cost"
            ),
            hourly_resources_internal_cost=self._calculation_data.get(
                "hourly_resources_internal_cost"
            ),
            hourly_resources_margin=self._calculation_data.get(
                "hourly_resources_margin"
            ),
            hourly_resources_margin_percent=self._calculation_data.get(
                "hourly_resources_margin_percent"
            ),
        )
        return hourly_resources_summary
