from typing import Dict, Optional

from document.models import SOWDocument, SoWOpportunityTeamMapping
from exceptions.exceptions import APIError
from utils import get_app_logger

logger = get_app_logger(__name__)


class SoWOpportunityTeamOperationService:
    """
    Class to handle SoW opportunity team operations.
    """

    @staticmethod
    def create_opportunity_team_mapping(
        sow_document: SOWDocument, team_crm_id: int
    ) -> SoWOpportunityTeamMapping:
        """
        Create sow opportunity team mapping.

        Args:
            sow_document: SOWDocument
            team_crm_id: Opportunity team CRM ID

        Returns:
            SoWOpportunityTeamMapping
        """
        mapping: SoWOpportunityTeamMapping = (
            SoWOpportunityTeamMapping.objects.create(
                provider=sow_document.provider,
                sow_document=sow_document,
                author=sow_document.author,
                team_crm_id=team_crm_id,
            )
        )
        return mapping

    @staticmethod
    def update_opportunity_team_mapping(
        sow_document: SOWDocument, updated_author_id: str
    ) -> SoWOpportunityTeamMapping:
        """
        Update sow opportunity team mapping.

        Args:
            sow_document: SOWDocument
            updated_author_id: Author ID

        Returns:
            SoWOpportunityTeamMapping
        """
        mapping: SoWOpportunityTeamMapping = sow_document.opportunity_team
        mapping.author_id = updated_author_id
        mapping.save()
        return mapping

    def create_opportunity_team_and_mapping(
        self, sow_document: SOWDocument
    ) -> Dict:
        """
        Create opportunity team in CW and sow to opportunity team mapping in DB.

        Args:
            sow_document: SOWDocument

        Returns:
            Opportunity team
        """
        system_member_crm_id: Optional[
            int
        ] = self._get_authors_system_member_crm_id(sow_document)
        if system_member_crm_id is None:
            return {}

        try:
            opportunity_team: Dict = sow_document.provider.erp_client.create_opportunity_team_of_individual_type(
                sow_document.quote_id, system_member_crm_id
            )
            self.create_opportunity_team_mapping(
                sow_document, opportunity_team.get("team_crm_id")
            )
        except APIError as err:
            logger.error(
                f"CW error creating opportunity team for SoWDocument ID: {sow_document.id}, "
                f"opportunity CRM ID: {sow_document.quote_id}. Error: {err}"
            )
            opportunity_team = {}
        return opportunity_team

    def update_opportunity_team_and_mappping(
        self, sow_document: SOWDocument
    ) -> Dict:
        """
        Updates opportunity team in CW and sow to opportunity team mapping in DB.

        Args:
            sow_document: SOWDocument

        Returns:
            Updated opportunity team
        """
        system_member_crm_id: Optional[
            int
        ] = self._get_authors_system_member_crm_id(sow_document)
        if system_member_crm_id is None:
            return {}

        existing_mapping: SoWOpportunityTeamMapping = (
            sow_document.opportunity_team
        )
        try:
            updated_opportunity_team: Dict = (
                sow_document.provider.erp_client.update_opportunity_team(
                    sow_document.quote_id,
                    existing_mapping.team_crm_id,
                    system_member_crm_id,
                )
            )
            self.update_opportunity_team_mapping(
                sow_document, str(sow_document.author_id)
            )
        except APIError as err:
            logger.error(
                f"CW error updating opportunity team for SoWDocument ID: {sow_document.id}, "
                f"opportunity CRM ID: {sow_document.quote_id}. Error: {err}"
            )
            updated_opportunity_team = {}
        return updated_opportunity_team

    @classmethod
    def _get_authors_system_member_crm_id(
        cls, sow_document: SOWDocument
    ) -> Optional[int]:
        """
        Get author's system member CRM ID.

        Args:
            sow_document: SOWDocument

        Returns:
            System member CRM ID
        """
        system_member_crm_id: int = (
            sow_document.author.profile.system_member_crm_id
        )
        if system_member_crm_id is None:
            logger.info(
                f"system_member_crm_id not found for sow author: {sow_document.author_id}, sow document ID: "
                f"{sow_document.id}"
            )
        return system_member_crm_id

    def create_or_update_opportunity_team(
        self, sow_document: SOWDocument
    ) -> Optional[Dict]:
        """
        Create or update opportunity team in CW and SoW to oppotunity team mapping.

        Args:
            sow_document: SOWDocument

        Returns:
            None
        """
        try:
            sow_opportunity_team_mapping: Optional[
                SoWOpportunityTeamMapping
            ] = sow_document.opportunity_team
        except SoWOpportunityTeamMapping.DoesNotExist:
            sow_opportunity_team_mapping = None
        # If existing team mapping does not exists, it means opportunity team does not exist for the opportunity.
        if not sow_opportunity_team_mapping:
            self.create_opportunity_team_and_mapping(sow_document)
        # If existing team mapping exists and the mapped author is not same as current author, update the team.
        elif (
            sow_opportunity_team_mapping
            and sow_document.author_id
            != sow_opportunity_team_mapping.author_id
        ):
            self.update_opportunity_team_and_mappping(sow_document)
        return
