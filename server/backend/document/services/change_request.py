import os
import os
import tempfile
from typing import Dict, Optional, Any

from django.conf import settings
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework.generics import get_object_or_404

from accounts.models import User
from core.integrations.pdfshift import PdfShift
from document.models import SOWDocument, SOWDocumentSettings
from document.services import DocumentService
from document.utils import (
    get_sow_document_file_name,
    substitute_page_break_placeholder_in_change_request_json_config,
)
from project_management.models import ChangeRequest, Project
from utils import get_app_logger
from django.utils import timezone

logger = get_app_logger(__name__)

LOGO_IMAGE = "lookingpoint-logo-new.png"
FOOTER_HTML = "change_request_footer.html"
HEADER_HTML = "change_request_header.html"


class ChangeRequestSOWService(DocumentService):
    @classmethod
    def get_change_request(cls, sow: SOWDocument):
        change_request = ChangeRequest.objects.filter(
            provider=sow.provider, sow_document=sow
        ).first()
        return change_request

    @classmethod
    def parse_special_variables_in_template(
        cls, sow_settings, change_request, project_start_date
    ):
        if change_request.change_request_type == SOWDocument.T_AND_M:
            terms_and_conditions = (
                sow_settings.change_request_t_and_m_terms_markdown.replace(
                    "{Project Name}", change_request.project.title
                )
                .replace("{Project Date}", project_start_date)
                .replace(
                    'class="ql-mention-denotation-char">#',
                    'class="ql-mention-denotation-char">',
                )
            )
        else:
            terms_and_conditions = (
                sow_settings.change_request_fixed_fee_terms_markdown.replace(
                    "{Project Name}", change_request.project.title
                )
                .replace("{Project Date}", project_start_date)
                .replace(
                    'class="ql-mention-denotation-char">#',
                    'class="ql-mention-denotation-char">',
                )
            )
        return terms_and_conditions

    @classmethod
    def get_terms_and_condition_from_settings(cls, provider, change_request):
        sow_settings: SOWDocumentSettings = cls._get_sow_setting(provider)
        if sow_settings and change_request.change_request_type:
            project_start_date = change_request.project.created_on.strftime(
                "%b %d, %Y"
            )
            return cls.parse_special_variables_in_template(
                sow_settings, change_request, project_start_date
            )

    @classmethod
    def get_rendered_html_document(cls, document, template_vars, file_type):
        template_path = SOWDocument.get_doc_template_path(
            document.doc_type, file_type
        )
        html_out = render_to_string(template_path, template_vars)
        return html_out

    @classmethod
    def get_template_context(
        cls,
        author,
        document,
        erp_client,
        change_request_type=None,
        project_id=None,
        provider=None,
    ):
        template_vars = super().get_template_context(
            author, document, erp_client
        )
        template_vars.update(
            {
                "logo": settings.LOOKINGPOINT_LOGO,
                "banner": None,
                "footer": None,
            }
        )
        change_request = cls.get_change_request(document)
        if change_request:
            terms_and_condition = cls.get_terms_and_condition_from_settings(
                document.provider, change_request
            )
            template_vars.update({"terms_and_condition": terms_and_condition})
            template_vars.update({"change_request": change_request})
        # Condition if CR preview before saving the Change Request,then show T&C on the basis of change request type
        elif change_request_type and project_id:
            sow_settings: SOWDocumentSettings = cls._get_sow_setting(provider)
            _change_request = ChangeRequest.objects.none()
            _change_request.change_request_type = change_request_type
            project = Project.objects.get(id=project_id)
            _change_request.project = project
            project_start_date = project.created_on.strftime("%b %d, %Y")
            terms_and_condition = cls.parse_special_variables_in_template(
                sow_settings, _change_request, project_start_date
            )
            template_vars.update({"terms_and_condition": terms_and_condition})
        return template_vars

    @classmethod
    def get_footer_html(cls):
        FOOTER_HTML_PATH = os.path.join(cls.TEMPLATE_BASE_DIR, FOOTER_HTML)
        footer_html = render_to_string(FOOTER_HTML_PATH, {})
        return footer_html

    @classmethod
    def get_footer_object(cls):
        footer_html = cls.get_footer_html()
        # https://docs.pdfshift.io/#header-footer
        footer = {"source": footer_html, "spacing": "100px"}
        return footer

    @classmethod
    def get_header_html(cls):
        HEADER_HTML_PATH = os.path.join(cls.TEMPLATE_BASE_DIR, HEADER_HTML)
        header_html = render_to_string(HEADER_HTML_PATH, {})
        return header_html

    @classmethod
    def get_header_object(cls):
        header_html = cls.get_header_html()
        # https://docs.pdfshift.io/#header-footer
        header = {"source": header_html}
        return header

    @classmethod
    def get_file_name(
        cls,
        document: SOWDocument,
        file_type: str = f"{DocumentService.PDF_FORMAT}",
    ) -> str:
        file_name: str = get_sow_document_file_name(document, file_type)
        if len(file_name) > settings.CONNECTWISE_DOCUMENT_TITLE_MAX_LENGTH:
            file_name: str = f"Change Request - {DocumentService.SOW}.pdf"
        return file_name

    @classmethod
    def create_pdf(
        cls, html_string, output_file_name, header=None, footer=None, **kwargs
    ) -> Dict[str, str]:
        upload_to_s3: bool = True
        if settings.DEBUG:
            TEMP_DIR = tempfile.gettempdir()
            file_path: str = os.path.join(TEMP_DIR, output_file_name)
            upload_to_s3 = False
        else:
            file_path: str = output_file_name
        file_url = cls.render_pdf(
            html_string,
            header,
            footer,
            file_path,
            upload_to_s3=upload_to_s3,
            **kwargs,
        )
        data: Dict = dict(file_path=file_url, file_name=output_file_name)
        return data

    @staticmethod
    def parse_special_variables_in_change_request(
        context: Dict[str, Any]
    ) -> None:
        """
        Replace placeholders with values in change request json config.

        Args:
            context: Render context

        Returns:
            None
        """
        json_config: Dict[str, Any] = context.pop("json_config", {})
        parsed_json_config: Dict[
            str, Any
        ] = substitute_page_break_placeholder_in_change_request_json_config(
            json_config
        )
        context.update(json_config=parsed_json_config)
        return

    @classmethod
    def download_document(
        cls,
        provider_id: int,
        erp_client,
        author: User,
        document_id: int,
        file_type: str,
        file_name: Optional[str] = None,
        upload: bool = True,
    ) -> Dict:
        document: SOWDocument = DocumentService.get_document(
            provider_id, document_id
        )
        author: User = document.author
        template_vars: Dict = cls.get_template_context(
            author, document, erp_client
        )
        cls.parse_special_variables_in_change_request(template_vars)
        html_out: str = cls.get_rendered_html_document(
            document, template_vars, file_type
        )
        file_name: str = (
            file_name if file_name else cls.get_file_name(document, file_type)
        )
        if file_type.lower() == cls.PDF_FORMAT:
            header = cls.get_header_object()
            footer = cls.get_footer_object()
            data = cls.create_pdf(
                html_out, file_name, header=header, footer=footer
            )
            return data

    @classmethod
    def create_change_request_object(cls, payload):
        change_request_obj_payload: Dict = dict(
            name=payload.get("name"),
            requested_by_id=payload.get("requested_by", None),
            change_number=payload.get("change_number", None),
            created_on=timezone.now(),
            change_request_type=payload.get("change_request_type", None),
        )
        # This handle the edge case of cloned CR documents
        # Only 1 SOW Document is linked to a project, so in case of cloned CR docuemnt
        # as we don't know for which project it is linked, we use a dummy project
        project_id = payload.get("project", None)
        if project_id:
            change_request_obj_payload.update(project_id=project_id)
        else:
            project_object: Project = Project(
                provider_id=payload.get("provider_id"), title=""
            )
            change_request_obj_payload.update(project=project_object)
        return ChangeRequest(**change_request_obj_payload)

    @classmethod
    def create_sow_document_object(cls, payload):
        payload["customer_id"] = payload.pop("customer", None)
        payload["user_id"] = payload.pop("user", None)
        payload["category_id"] = payload.pop("category", None)
        payload["provider_id"] = payload.pop("provider_id", None)
        payload["updated_on"] = timezone.now()
        doc = SOWDocument(**payload)
        return doc

    @classmethod
    def generate_document_preview(cls, provider, author, payload):
        project_id: int = payload.get("project", None)
        if project_id:
            project: Project = get_object_or_404(
                Project.objects.all(), id=project_id
            )
            project_start_date: str = project.created_on.strftime("%b %d, %Y")
        else:
            project_start_date: str = "-"
        change_request: ChangeRequest = cls.create_change_request_object(
            payload
        )
        payload["id"] = payload.get("sow_document")
        change_request_type: str = payload.get("change_request_type")
        sow_settings: SOWDocumentSettings = cls._get_sow_setting(provider)
        terms_and_condition = cls.parse_special_variables_in_template(
            sow_settings, change_request, project_start_date
        )
        terms_and_condition = {"terms_and_condition": terms_and_condition}
        # Remove extra keys
        extra_keys = [
            "author_name",
            "updated_by_name",
            "requested_by",
            "assigned_to",
            "status",
            "change_number",
            "sow_document",
            "last_updated_by",
            "project",
            "change_request_type",
        ]
        for key in extra_keys:
            payload.pop(key, None)

        doc: SOWDocument = cls.create_sow_document_object(payload)
        template_vars: Dict = cls.get_template_context(
            author,
            doc,
            provider.erp_client,
            change_request_type,
            project_id,
            provider,
        )
        template_vars.update({"change_request": change_request})
        template_vars.update(terms_and_condition)
        template_vars.update(is_t_and_m_doc=True if change_request_type == SOWDocument.T_AND_M else False)
        cls.parse_special_variables_in_change_request(template_vars)
        html_str: str = cls.get_rendered_html_document(
            doc, template_vars, cls.PDF_FORMAT
        )
        header: Dict = cls.get_header_object()
        footer: Dict = cls.get_footer_object()
        file_name: str = cls.get_file_name(doc)
        data: Dict = cls.create_pdf(
            html_str, file_name, header=header, footer=footer
        )
        return data

    @classmethod
    def render_pdf(
        cls,
        html,
        header=None,
        footer=None,
        output=None,
        options=None,
        **kwargs,
    ):
        """
        Function for easy printing of pdfs from django templates

        Header template can also be set
        """
        options = options or {}
        default_options = dict(format="A3", margin="80px 10px 20px 10px")
        if header:
            default_options.update({"header": header})
        if footer:
            default_options.update({"footer": footer})
        default_options.update(options)
        file_url = PdfShift().convert(
            html, output, **default_options, **kwargs
        )
        return file_url
