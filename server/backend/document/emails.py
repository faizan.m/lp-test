from accounts.mail import BaseEmailMessage


class SoWVendorOnboardingEmail(BaseEmailMessage):
    template_name = (
        "vendor_onboarding/sow_vendor_onboarding_email_template.html"
    )


class FireReportEmail(BaseEmailMessage):
    template_name = "fire_report/fire_report_email_template.html"


class WeeklyRecapEmail(BaseEmailMessage):
    template_name = "weekly_recap/weekly_recap_email_template.html"
