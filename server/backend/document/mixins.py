from django.db.models import Count, OuterRef, Q, Subquery
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework.permissions import IsAuthenticated

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from document.custom_filters import OrderTrackingDashboardTableDataFilterSet
from document.models import (
    ConnectwisePurchaseOrderData,
    ConnectwiseServiceTicket,
    ConnectwiseServiceTicketParseFailed,
    IngramPurchaseOrderData,
    OperationDashboardSettings,
    ConnectwisePurchaseOrderLineitemsData,
)
from document.serializers import (
    OrdersShippedButNotReceivedSerializer,
)
from document.custom_filters import OrderTrackingDashboardTableDataFilterSet
from document.exceptions import OperationDashboardSettingsNotConfigured


class ShippedNotReceivedMixin:
    def get_shipped_not_received(
        self: generics.GenericAPIView, filter_conditions={}
    ):
        provider = self.request.provider
        vendor_part_number_query = Subquery(
            (
                IngramPurchaseOrderData.objects.filter(
                    provider=OuterRef("provider"),
                    po_number=OuterRef("po_number"),
                    vendor_part_number=OuterRef("product_id"),
                    status="Shipped",
                ).values("shipped_date")
            )[:1]
        )
        shipped_but_not_received_count = (
            ConnectwisePurchaseOrderLineitemsData.objects.select_related(
                "po_number"
            )
            .annotate(ingram_shipped_date=vendor_part_number_query)
            .filter(
                ~Q(received_status="FullyReceived"),
                provider=provider,
                ingram_shipped_date__isnull=False,
                **filter_conditions
            )
        )
        return shipped_but_not_received_count
