from marshmallow import Schema, fields
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator
from typing import Dict, List, Union, Optional

from accounts.models import Provider, User
from core.serializers import CustomerSerializer
from document.models import (
    ConnectwisePurchaseOrderData,
    ConnectwiseServiceTicket,
    IngramPurchaseOrderData,
    SalesPurchaseOrderData,
    ProductsItemData,
    SalesOrderStatusSettings,
    ConnectwisePurchaseOrderLineitemsData,
    SOWVendorOnboardingAttachments,
    SOWVendorOnboardingSetting,
    DefaultDocumentCreatorRoleSetting,
    SoWHourlyResourcesDescriptionMapping,
    FireReportPartnerSite,
    FireReportSetting,
    AutomatedWeeklyRecapSetting,
)
from document.models import (
    CustomerNotes,
    IngramAPISetting,
    OperationDashboardSettings,
    OperationEmailTemplateSettings,
    SOWCategory,
    SOWDocument,
    SOWDocumentSettings,
    SOWTemplate,
    ServiceCatalog,
    ServiceCategory,
    ServiceTechnologyType,
    Client360DashboardSetting,
)
from project_management.models import ChangeRequest
from utils import (
    DynamicFieldsModelSerializer,
    name_contains_forbidden_characters,
    FORBIDDEN_CHARS_LIST,
)

IN_PROGRESS = "In Progress"
SHIPPED_COMPLETE = "Ship Complete"
DELIVERED = "Delivered"
SHIPPED = "Shipped"
ORDER_PLACED = "Order Placed"


class SOWTemplateSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(
        source="category.name", read_only=True
    )
    author_name = serializers.CharField(source="author.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    linked_service_catalog_name = serializers.CharField(
        source="linked_service_catalog.service_name", read_only=True
    )
    linked_service_catalog = serializers.IntegerField(
        source="linked_service_catalog.id", read_only=True
    )
    version = serializers.CharField(read_only=True)

    class Meta:
        model = SOWTemplate
        fields = (
            "id",
            "name",
            "json_config",
            "created_on",
            "updated_on",
            "category",
            "category_name",
            "author_name",
            "updated_by_name",
            "service_catalog_category",
            "is_disabled",
            "linked_service_catalog",
            "linked_service_catalog_name",
            "version",
        )


class SOWTemplateListSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        try:
            linked_catalog: ServiceCatalog = instance.linked_catalog
        except ServiceCatalog.DoesNotExist:
            linked_catalog = None
        return {
            "id": instance.id,
            "name": instance.name,
            "author_name": instance.author.name,
            "updated_by_name": instance.updated_by.name,
            "version": instance.version,
            "created_on": instance.created_on,
            "updated_on": instance.updated_on,
            "service_catalog_category": instance.service_catalog_category_id,
            "category": instance.category_id,
            "category_name": instance.category.name,
            "linked_service_catalog_name": linked_catalog.service_name
            if linked_catalog
            else None,
            "linked_service_catalog": linked_catalog.id
            if linked_catalog
            else None,
            "is_disabled": instance.is_disabled,
        }


class SOWTemplateCreateSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(
        source="category.name", read_only=True
    )
    linked_service_catalog = serializers.IntegerField(
        source="linked_service_catalog.id", allow_null=True
    )

    def validate_linked_service_catalog(self, value):
        if not value:
            return value
        request = self.context["request"]
        provider = request.provider
        if not ServiceCatalog.objects.filter(
            id=value, provider=provider
        ).exists():
            raise ValidationError("No Service Catalog exists with this id.")
        return value

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Template name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        request = self.context["request"]
        if SOWTemplate.objects.filter(
            provider_id=request.provider.id, name=name
        ).exists():
            raise ValidationError(
                ["Template with the same name already exists."]
            )
        return name

    class Meta:
        model = SOWTemplate
        fields = (
            "name",
            "json_config",
            "category",
            "category_name",
            "service_catalog_category",
            "linked_service_catalog",
        )


class SOWTemplateUpdateSerializer(SOWTemplateCreateSerializer):
    version_description = serializers.CharField(required=False)
    update_version = serializers.BooleanField(default=False)

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Template name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        request = self.context["request"]
        id: int = self.context.get("view").kwargs.get("pk")
        if (
            SOWTemplate.objects.filter(
                provider_id=request.provider.id, name=name
            )
            .exclude(id=id)
            .exists()
        ):
            raise ValidationError(
                ["Template with the same name already exists."]
            )
        return name

    class Meta:
        model = SOWTemplate
        fields = (
            "name",
            "json_config",
            "category",
            "service_catalog_category",
            "linked_service_catalog",
            "version_description",
            "update_version",
            "major_version",
            "minor_version",
        )
        read_only_fields = ("major_version", "minor_version")


class SOWDocumentChangeRequestSerializer(serializers.ModelSerializer):
    project_name = serializers.CharField(
        source="project.title", required=False
    )
    requested_by_name = serializers.CharField(
        source="requested_by.name", required=False
    )
    assigned_to_name = serializers.CharField(
        source="assigned_to.name", required=False
    )

    class Meta:
        model = ChangeRequest
        fields = [
            "name",
            "change_number",
            "change_request_type",
            "project",
            "project_name",
            "requested_by",
            "requested_by_name",
            "status",
            "assigned_to_name",
        ]
        read_only_fields = fields


class SOWDocumentListSerializer(serializers.BaseSerializer):
    """
    Serializer for SOW documents listing.
    """

    def to_representation(self, instance):
        return {
            "id": instance.id,
            "name": instance.name,
            "category_name": instance.category.name,
            "category": instance.category_id,
            "quote_id": instance.quote_id,
            "forecast_id": instance.forecast_id,
            "doc_type": instance.doc_type,
            "customer": CustomerSerializer(
                instance.customer, required=False
            ).data,
            "user": instance.user_id,
            "user_name": instance.user.name,
            "author": instance.author_id,
            "author_name": instance.author.name,
            "updated_by": instance.updated_by_id,
            "updated_by_name": instance.updated_by.name,
            "version": instance.version,
            "change_request": SOWDocumentChangeRequestSerializer(
                instance.change_request, many=True, required=False
            ).data,
            "forecast_exception": dict(
                forecast_exception=instance.json_config.get(
                    "forecast_exception", {}
                )
            ),
            "budget_hours": float(instance.budget_hours)
            if instance.budget_hours
            else None,
            "margin": instance.margin,
            "created_on": instance.created_on,
            "updated_on": instance.updated_on,
        }


class SOWDocumentSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(
        source="category.name", read_only=True
    )
    customer = CustomerSerializer(required=False)
    author_name = serializers.CharField(source="author.name", read_only=True)
    user_name = serializers.CharField(source="user.name", read_only=True)
    updated_by_name = serializers.CharField(
        source="updated_by.name", read_only=True
    )
    type = serializers.CharField(source="base_template.type", read_only=True)
    version = serializers.CharField(read_only=True)
    # SOW document has a reverse relationship on Change request
    change_request = SOWDocumentChangeRequestSerializer(
        many=True, required=False
    )
    budget_hours = serializers.FloatField(read_only=True, required=False)

    class Meta:
        model = SOWDocument
        fields = [
            "id",
            "category_name",
            "author_name",
            "user_name",
            "updated_by_name",
            "type",
            "customer",
            "change_request",
            "version",
            "name",
            "json_config",
            "user",
            "author",
            "updated_by",
            "quote_id",
            "forecast_id",
            "category",
            "doc_type",
            "major_version",
            "minor_version",
            "created_on",
            "updated_on",
            "budget_hours",
            "margin",
        ]


class SOWDocumentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SOWDocument
        exclude = (
            "provider",
            "author",
            "updated_by",
            "forecast_id",
            "major_version",
            "minor_version",
            "margin",
        )

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Document name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        return name


class SOWDocumentUpdateSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source="user.name", read_only=True)
    version_description = serializers.CharField(required=False)
    update_version = serializers.BooleanField(default=False)
    update_author = serializers.BooleanField(default=False)

    def validate_name(self, name):
        if name_contains_forbidden_characters(name):
            raise ValidationError(
                f"Document name should not contain following characters: {FORBIDDEN_CHARS_LIST}"
            )
        return name

    class Meta:
        model = SOWDocument
        exclude = (
            "provider",
            "author",
            "updated_by",
            "customer",
            "forecast_id",
            "margin",
            "user",
        )


class QuoteSerializer(serializers.Serializer):
    name = serializers.CharField()
    type_id = serializers.CharField()
    customer_id = serializers.IntegerField()
    user_id = serializers.CharField()
    stage_id = serializers.IntegerField()


class QuoteUpdateSerializer(serializers.Serializer):
    stage_id = serializers.IntegerField()


class ChangeRequestCreateSerializer(serializers.Serializer):
    name = serializers.CharField()
    type_id = serializers.IntegerField()
    customer_id = serializers.IntegerField()
    user_id = serializers.CharField()
    revenue = serializers.IntegerField()
    cost = serializers.IntegerField()


# serializer for ServiceTechnologyType
class ServiceTechnologyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceTechnologyType
        fields = ("name", "id", "is_disabled")
        read_only_field = ("provider",)


# SOWCategory = Service Type for Service catalog
class SOWCategorySerializer(serializers.ModelSerializer):
    technology_types = serializers.SerializerMethodField()

    def get_technology_types(self, obj):
        request = self.context.get("request")
        technology_types = ServiceTechnologyType.objects.filter(
            service_type=obj, provider=request.provider
        )
        technology_types = ServiceTechnologyTypeSerializer(
            technology_types, many=True, read_only=True
        )
        return technology_types.data

    class Meta:
        model = SOWCategory
        fields = "__all__"
        read_only_fields = ("name",)


class SOWCategoryUpdateSerializer(serializers.ModelSerializer):
    technology_types = ServiceTechnologyTypeSerializer(many=True)

    class Meta:
        model = SOWCategory
        fields = "__all__"
        read_only_fields = ("name",)


# SOWCategory = Service Type for Service catalog
class ServiceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SOWCategory
        fields = "name", "id"
        read_only_fields = ("provider", "name")


class ServiceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = "name", "id"
        read_only_fields = ("name", "provider")


class ServiceCatalogListRetrieveSerializer(
    DynamicFieldsModelSerializer, serializers.ModelSerializer
):
    service_type = ServiceTypeSerializer()
    service_category = ServiceCategorySerializer()
    linked_template_name = serializers.CharField(
        source="linked_template.name", read_only=True
    )

    class Meta:
        model = ServiceCatalog
        exclude = ("provider",)
        read_only_fields = ("provider", "is_disabled")


class ServiceCatalogCreateUpdateSerializer(serializers.ModelSerializer):
    linked_template = serializers.PrimaryKeyRelatedField(
        allow_null=True,
        queryset=SOWTemplate.objects.all(),
        required=False,
        validators=[
            UniqueValidator(
                queryset=ServiceCatalog.objects.all(),
                message="Template already in use.",
            )
        ],
    )

    def validate_service_name(self, service_name):
        request = self.context.get("request")
        query = ServiceCatalog.objects.filter(
            provider=request.provider, service_name=service_name
        )
        if request.method in ["PUT", "PATCH"]:
            query = query.exclude(id=self.instance.id)

        if query.exists():
            raise ValidationError("Duplicate Service Name")
        return service_name

    def validate_linked_template(self, linked_template):
        if linked_template is None:
            return linked_template
        try:
            SOWTemplate.objects.get(id=linked_template.id)
        except SOWTemplate.DoesNotExist:
            raise ValidationError(
                {
                    "linked_template": [
                        "The requested Template does not exists."
                    ]
                }
            )
        return linked_template

    class Meta:
        model = ServiceCatalog
        exclude = ("provider",)
        read_only_fields = ("provider", "is_disabled", "is_template_present")


class SOWDocumentSettingsSerializer(serializers.ModelSerializer):
    """
    DRF serializer class for SOWDocumentSettings model
    """

    class Meta:
        model = SOWDocumentSettings
        exclude = ("provider",)


class CustomerNotesSerializer(serializers.ModelSerializer):
    last_updated_by = serializers.StringRelatedField()
    opportunity_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True, allow_empty=True
    )
    contact_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True, allow_empty=True
    )
    site_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True, allow_empty=True
    )
    technology_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_null=True, allow_empty=True
    )

    class Meta:
        model = CustomerNotes
        fields = "__all__"
        read_only_fields = ("provider", "customer", "last_updated_by")


class IngramAPISettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = IngramAPISetting
        fields = "__all__"
        read_only_fields = ("provider",)


class BoardTicketStatusesMappingSerializer(serializers.Serializer):
    ticket_status_id = serializers.IntegerField()
    ticket_status_name = serializers.CharField(max_length=100)


class ServiceTicketBoardAndStatusMappingSerializer(serializers.Serializer):
    board_id = serializers.IntegerField()
    board_name = serializers.CharField(max_length=100)
    ticket_statuses = BoardTicketStatusesMappingSerializer(
        many=True, required=False
    )


class OperationEmailTemplateOpportunityStatusMappingSerializer(
    serializers.Serializer
):
    status_id = serializers.IntegerField()
    status_name = serializers.CharField(max_length=100)


class OperationEmailTemplateOpportunityStageMappingSerializer(
    serializers.Serializer
):
    stage_id = serializers.IntegerField()
    stage_name = serializers.CharField(max_length=100)


class OperationEmailTemplateSettingsSerializer(serializers.ModelSerializer):
    ticket_board_and_status_mapping = (
        ServiceTicketBoardAndStatusMappingSerializer(many=True, required=False)
    )
    opportunity_statuses = (
        OperationEmailTemplateOpportunityStatusMappingSerializer(
            many=True, required=False
        )
    )
    opportunity_stages = (
        OperationEmailTemplateOpportunityStageMappingSerializer(
            many=True, required=False
        )
    )

    class Meta:
        model = OperationEmailTemplateSettings
        fields = (
            "purchase_order_status_ids",
            "extra_config",
            "default_extra_config",
            "contact_type_connectwise_id",
            "ticket_board_and_status_mapping",
            "opportunity_statuses",
            "opportunity_stages",
        )
        read_only_fields = ("provider",)

    def update(self, instance, validated_data):
        self.instance.ticket_board_and_status_mapping = validated_data.get(
            "ticket_board_and_status_mapping", []
        )
        self.instance.opportunity_statuses = validated_data.get(
            "opportunity_statuses", []
        )
        self.instance.opportunity_stages = validated_data.get(
            "opportunity_stages", []
        )
        self.instance.extra_config = validated_data.get("extra_config")
        self.instance.purchase_order_status_ids = validated_data.get(
            "purchase_order_status_ids"
        )
        self.instance.contact_type_connectwise_id = validated_data.get(
            "contact_type_connectwise_id"
        )
        self.instance.save()
        return instance


class OrderReceivingOpportunitySerializer(Schema):
    opportunity_name = fields.String(required=True)
    opportunity_type = fields.Integer(required=True)
    user_id = fields.String(required=True)
    inside_rep = fields.Integer(required=True)
    opportunity_stage = fields.Integer(required=True)


class OrderReceivingConfigurationSerializer(Schema):
    category_id = fields.Integer(required=True)
    service_contract_number = fields.String(required=False, allow_none=True)
    product_id = fields.String(required=True)
    expiration_date = fields.String(required=True)
    manufacturer_id = fields.Integer()
    installation_date = fields.String()


class OrderReceivingLineItemSerializer(Schema):
    line_item_id = fields.Integer(required=True)
    tracking_number = fields.String()
    ship_date = fields.String()
    serial_numbers = fields.List(fields.String())
    received_qty = fields.Integer()
    note = fields.String()
    shipping_method_id = fields.Integer(allow_none=True)
    received_status = fields.String()
    configuration = fields.Nested(
        OrderReceivingConfigurationSerializer, required=False
    )
    opportunity = fields.Nested(
        OrderReceivingOpportunitySerializer, required=False
    )


class OrderReceivingSerializer(Schema):
    purchase_order_id = fields.Integer(required=True)
    customer_id = fields.Integer()
    ticket_id = fields.Integer()
    ticket_note = fields.String()
    line_items = fields.Nested(
        OrderReceivingLineItemSerializer, many=True, required=True
    )


class OrderReceivingLineItemsSerializer(Schema):
    purchase_order_ids = fields.List(fields.Integer(allow_none=False))


class PurchaseOrderHistorySerializer(serializers.Serializer):
    customer_id = serializers.IntegerField(allow_null=False)


class OperationsDashboardSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationDashboardSettings
        fields = [
            "id",
            "business_unit_id",
            "service_boards",
            "created_on",
            "updated_on",
            "purchase_order_status_ids",
            "ticket_status_ids",
            "po_custom_field_id",
            "service_ticket_create_date",
        ]


class ConnectwiseServiceTicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConnectwiseServiceTicket
        fields = [
            "ticket_crm_id",
            "provider",
            "summary",
            "status_crm_id",
            "status_name",
            "company_crm_id",
            "company_name",
            "owner_crm_id",
            "owner",
            "service_board_crm_id",
            "connectwise_last_data_update",
            "last_ticket_note",
        ]


class IngramPurchaseOrderDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = IngramPurchaseOrderData
        fields = "__all__"


class ConnectwisePurchaseOrderDataSerializer(serializers.ModelSerializer):
    ticket = ConnectwiseServiceTicketSerializer(
        read_only=True, source="po_number.ticket"
    )
    eta = serializers.DateField()
    shipped_date = serializers.DateField()

    class Meta:
        model = ConnectwisePurchaseOrderData
        fields = [
            "crm_id",
            "provider",
            "po_number",
            "vendor_company_id",
            "vendor_company_name",
            "ticket",
            "purchase_order_status_id",
            "purchase_order_status_name",
            "eta",
            "internal_notes",
            "shipped_date",
        ]


class OpenPOsByDistributorSerializer(serializers.Serializer):
    vendor_company_name = serializers.CharField()
    count = serializers.IntegerField()


class LastCustomerUpdateSerializer(serializers.Serializer):
    day_range = serializers.CharField()
    count = serializers.IntegerField()


class OrdersByEstimatedShippedDateSerializer(serializers.Serializer):
    shipped_range = serializers.CharField()
    count = serializers.IntegerField()


class ConnectwiseServiceTicketParseFailedSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    ticket_crm_id = serializers.IntegerField()
    summary = serializers.CharField()
    is_ignored = serializers.BooleanField()


class IgnoreFailedTicketsSerializer(serializers.Serializer):
    ticket_crm_ids = serializers.ListField()


class OrdersShippedButNotReceivedSerializer(serializers.Serializer):
    shipped_but_not_received_count = serializers.IntegerField()
    total_line_items_count = serializers.IntegerField()


class CustomerSalesOrderDataSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    is_closed = serializers.SerializerMethodField()

    def check_sales_order_status(self, obj, sales_order_status_settings):
        status = None
        if obj.status_id == sales_order_status_settings.get(
            "in_progress_status", ""
        ):
            invoice_ids = ProductsItemData.objects.filter(
                provider_id=obj.provider_id, sales_order_id=obj.id
            ).values_list("invoice_id", flat=True)
            if any(
                list(invoice_ids)
            ):  # Check if atleast one invoice id is present for a sales order
                status = IN_PROGRESS
            else:
                status = ORDER_PLACED
        elif obj.status_id == sales_order_status_settings.get(
            "waiting_on_smartnet_status", ""
        ):
            status = SHIPPED_COMPLETE
        elif obj.status_id in sales_order_status_settings.get(
            "closed_statuses", ""
        ):
            status = DELIVERED
        return status

    def get_status(self, obj):
        try:
            sales_order_status_settings = self.context.get(
                "sales_order_status_settings"
            )
            if obj.status_id:
                status = self.check_sales_order_status(
                    obj, sales_order_status_settings
                )
            else:
                return None
        except:
            return None
        return status

    def get_is_closed(self, obj):
        if obj.status_id in self.context.get(
            "sales_order_status_settings"
        ).get("closed_statuses"):
            is_closed = True
        else:
            is_closed = False
        return is_closed

    class Meta:
        model = SalesPurchaseOrderData
        fields = [
            "id",
            "customer_po_number",
            "order_date",
            "opportunity_name",
            "site_crm_id",
            "site_name",
            "status",
            "is_closed",
        ]


class CustomerProductitemsDataSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        if self.context.get("get_bill_closed_flag"):
            status = SHIPPED
        else:
            status = ORDER_PLACED
        return status

    class Meta:
        model = ProductsItemData
        fields = ["product_id", "description", "quantity", "status"]


class SalesOrdersStatusSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesOrderStatusSettings
        fields = [
            "id",
            "in_progress_status",
            "waiting_on_smartnet_status",
            "closed_statuses",
            "created_on",
            "updated_on",
        ]


class IgnoreSalesOrdersSerializer(serializers.Serializer):
    sales_order_crm_ids = serializers.ListField()


class SOWDocumentLastUpdatedStatsDataSerializer(serializers.Serializer):
    days_range = serializers.CharField()
    count = serializers.IntegerField()


class SOWDocumentAuthorsListSerializer(serializers.Serializer):
    author_id = serializers.CharField()
    author__name = serializers.CharField()
    count = serializers.IntegerField()


class SOWDocumentCountByTechnologySerializer(serializers.Serializer):
    count = serializers.IntegerField()
    category__name = serializers.CharField()


class SOWGrossProfitCalculationSerializer(serializers.Serializer):
    json_config = serializers.JSONField(required=True)
    doc_type = serializers.ChoiceField(
        choices=SOWDocument.SOW_TEMPLATE_CHOICES, required=True
    )


class CustomerSOWDocumentsDropdownListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SOWDocument
        fields = ["id", "name"]
        read_only_fields = ["id", "name"]


class ProjectLineitemsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConnectwisePurchaseOrderLineitemsData
        fields = [
            "line_number",
            "po_number",
            "product_id",
            "product_crm_id",
            "ordered_quantity",
            "received_quantity",
            "serial_numbers",
            "ship_date",
            "received_date",
            "received_status",
            "description",
            "tracking_numbers",
            "is_closed",
            "ship_date",
            "shipping_method",
            "unit_cost",
        ]
        read_only_fields = fields


class UnlinkedPurchaseOrderInfoSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    po_number = serializers.CharField(read_only=True)
    crm_id = serializers.IntegerField(read_only=True)
    service_ticket_summary = serializers.CharField(read_only=True)
    service_ticket_crm_id = serializers.IntegerField(
        source="po_number.ticket.ticket_crm_id", read_only=True
    )
    customer_name = serializers.CharField(read_only=True)
    custom_field_present = serializers.JSONField(read_only=True)


class OpportunityTicketSOPOLinkingSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    opportunity_crm_id = serializers.IntegerField(read_only=True)
    opportunity_name = serializers.CharField(
        source="opportunity_crm_id__opportunity_name", read_only=True
    )
    ticket_summary = serializers.CharField(
        source="opportunity_crm_id__summary", read_only=True
    )
    ticket_crm_id = serializers.IntegerField(
        source="opportunity_crm_id__ticket_crm_id", read_only=True
    )
    sales_order_crm_id = serializers.IntegerField(
        source="crm_id", read_only=True
    )
    is_ignored = serializers.BooleanField(read_only=True)
    so_linked_pos = serializers.JSONField(read_only=True)
    po_custom_field_present = serializers.JSONField(read_only=True)
    po_number = serializers.CharField(
        source="linked_pos__po_number_id", read_only=True
    )
    purchase_order_crm_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        allow_null=False,
        allow_empty=False,
        read_only=True,
    )


class ServiceTicketToSoLinkingSeriaizer(serializers.Serializer):
    ticket_crm_id = serializers.IntegerField()
    provider_id = serializers.PrimaryKeyRelatedField(
        queryset=Provider.objects.all(), required=False
    )
    summary = serializers.CharField(max_length=500)
    status_crm_id = serializers.IntegerField(required=True)
    status_name = serializers.CharField(max_length=300)
    company_crm_id = serializers.IntegerField(required=True)
    company_name = serializers.CharField(max_length=500)
    owner_crm_id = serializers.IntegerField(allow_null=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), allow_null=True, required=False
    )
    service_board_crm_id = serializers.IntegerField(required=True)
    connectwise_last_data_update = serializers.DateTimeField()
    last_ticket_note = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    opportunity_name = serializers.CharField(
        max_length=500, allow_null=True, allow_blank=True
    )
    opportunity_crm_id = serializers.IntegerField()
    sales_order_crm_id = serializers.IntegerField()


class SalesOrderToPurchaseOrderLinkingCreateSerializer(serializers.Serializer):
    sales_order_crm_id = serializers.IntegerField(required=True)
    opportunity_crm_id = serializers.IntegerField(
        required=False,
        allow_null=True,
    )
    purchase_order_crm_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        allow_null=False,
        allow_empty=True,
        required=True,
    )


class SalesOrderToPurchaseOrderLinkingListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    sales_order_crm_id = serializers.IntegerField(
        source="crm_id", read_only=True
    )
    customer_name = serializers.CharField(
        source="company_name", read_only=True
    )
    opportunity_crm_id = serializers.IntegerField(
        source="opportunity_crm_id_id", read_only=True
    )
    opportunity_name = serializers.CharField(read_only=True)
    purchase_order_numbers = serializers.ListField(
        child=serializers.CharField(allow_blank=False, allow_null=False),
        allow_null=False,
        allow_empty=False,
        read_only=True,
    )
    purchase_order_crm_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        allow_null=False,
        allow_empty=False,
        read_only=True,
    )


class SalesOrderToPurchaseOrderLinkDeleteSerializer(serializers.Serializer):
    sales_order_crm_id = serializers.IntegerField(required=True)
    remove_link_for_po_ids = serializers.ListField(
        child=serializers.IntegerField(allow_null=False),
        allow_null=False,
        allow_empty=False,
        required=True,
    )


class ExistingSOWTemplatePreviewSerializer(serializers.Serializer):
    template_type = serializers.ChoiceField(
        choices=[SOWDocument.GENERAL, SOWDocument.T_AND_M]
    )


class SOWVendorTerritoriesSerializer(serializers.Serializer):
    territory_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )
    territory_name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )


class SOWVendorCustomerTypeSerializer(serializers.Serializer):
    customer_type_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )
    customer_type_name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )


class SOWVendorListStatusesSerializer(serializers.Serializer):
    customer_status_name = serializers.CharField(
        required=True, allow_null=False
    )
    customer_status_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )


class SOWVendorCreateStatusSerializer(serializers.Serializer):
    customer_status_crm_id = serializers.IntegerField(
        required=True, allow_null=False
    )
    customer_status_name = serializers.CharField(
        required=True, allow_null=False
    )


class SOWVendorOnboardingAttachmentsSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=False, allow_blank=False)
    attachment = serializers.FileField(allow_empty_file=False)


class SOWVendorOnboardingSettingCreateUpdateSerializer(
    serializers.ModelSerializer
):
    territories = SOWVendorTerritoriesSerializer(
        many=True, required=True, allow_null=False
    )
    customer_type = SOWVendorCustomerTypeSerializer(
        required=True, allow_null=False
    )
    vendor_list_statuses = SOWVendorListStatusesSerializer(
        many=True, required=True, allow_null=False
    )
    vendor_create_status = SOWVendorCreateStatusSerializer(
        required=True, allow_null=False
    )

    class Meta:
        model = SOWVendorOnboardingSetting
        fields = (
            "id",
            "provider_id",
            "territories",
            "customer_type",
            "vendor_onboarding_email_alias",
            "vendor_onboarding_email_subject",
            "vendor_onboarding_email_body",
            "vendor_list_statuses",
            "vendor_create_status",
        )
        read_only_fields = ("id", "provider_id")

    def create(self, validated_data: Dict):
        sow_vendor_onboarding_settings: SOWVendorOnboardingSetting = (
            SOWVendorOnboardingSetting.objects.create(**validated_data)
        )
        return sow_vendor_onboarding_settings


class SOWVendorOnboardingAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SOWVendorOnboardingAttachments
        fields = "__all__"
        read_only_fields = ("provider", "sow_vendor_onboarding_setting")


class SOWVendorOnboardingAttachmentsCreateSerializer(serializers.Serializer):
    attachment = serializers.FileField(
        required=True, allow_null=False, allow_empty_file=False
    )


class SOWVendorOnboardingSettingsRetrieveSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    provider_id = serializers.IntegerField(read_only=True)
    territories = SOWVendorTerritoriesSerializer(many=True, read_only=True)
    customer_type = SOWVendorCustomerTypeSerializer(read_only=True)
    vendor_onboarding_attachments = SOWVendorOnboardingAttachmentSerializer(
        many=True, source="onboarding_attachments"
    )
    vendor_onboarding_email_alias = serializers.EmailField(read_only=True)
    vendor_onboarding_email_subject = serializers.CharField(read_only=True)
    vendor_onboarding_email_body = serializers.CharField(read_only=True)
    vendor_list_statuses = SOWVendorListStatusesSerializer(
        many=True, required=True, allow_null=False
    )
    vendor_create_status = SOWVendorCreateStatusSerializer(
        required=True, allow_null=False
    )


class DefaultDocumentCreatorRoleSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = DefaultDocumentCreatorRoleSetting
        fields = (
            "provider",
            "default_sow_creator_role_id",
            "default_change_request_creator_role_id",
        )
        read_only_fields = ("id", "provider")


class SoWHourlyResourcesDescriptionMappingCreateUpdateSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = SoWHourlyResourcesDescriptionMapping
        fields = ("id", "vendor_name", "vendor_crm_id", "resource_description")
        read_only_fields = ("id",)

    def validate(self, attrs):
        vendor_crm_id: int = attrs.get("vendor_crm_id")
        vendor_name: str = attrs.get("vendor_name")
        if self.context.get("request").method == "POST":
            if SoWHourlyResourcesDescriptionMapping.objects.filter(
                provider_id=self.context.get("request").provider.id,
                vendor_crm_id=vendor_crm_id,
            ).exists():
                raise ValidationError(
                    f"Description already configured for the Resource: {vendor_name}"
                )
        elif self.context.get("request").method == "PUT":
            mapping_id: int = self.context.get("view").kwargs.get("id")
            if (
                SoWHourlyResourcesDescriptionMapping.objects.filter(
                    provider_id=self.context.get("request").provider.id,
                    vendor_crm_id=vendor_crm_id,
                )
                .exclude(id=mapping_id)
                .exists()
            ):
                raise ValidationError(
                    f"Description already configured for the Resource: {vendor_name}"
                )
        return attrs


class SoWHourlyResourcesDescriptionMappingSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    vendor_name = serializers.CharField(read_only=True)
    vendor_crm_id = serializers.IntegerField(read_only=True)
    resource_description = serializers.CharField(read_only=True)


class SitesSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, allow_null=False, allow_blank=False
    )
    address_line_1 = serializers.CharField(required=True, allow_null=True)
    address_line_2 = serializers.CharField(required=True, allow_null=True)
    city = serializers.CharField(required=True, allow_null=True)
    state = serializers.CharField(required=True, allow_null=False)
    state_id = serializers.IntegerField(required=True, allow_null=False)
    country = serializers.CharField(required=True, allow_null=False)
    country_id = serializers.IntegerField(required=True, allow_null=False)
    zip = serializers.CharField(required=True, allow_null=True)


class FireReportPartnerSiteCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FireReportPartnerSite
        fields = (
            "name",
            "address_line_1",
            "address_line_2",
            "city",
            "state",
            "state_crm_id",
            "country",
            "country_crm_id",
            "zip",
            "phone_number",
        )
        read_only_fields = ("id", "provider")

    def validate_name(self, name: str):
        request = self.context.get("request")
        provider: Provider = request.provider
        error_message: str = f"Partner site with name {name} already exists!"

        if request.method == "POST":
            if FireReportPartnerSite.objects.filter(
                provider_id=provider.id, name=name
            ).exists():
                raise ValidationError(error_message)
        elif request.method == "PUT":
            partner_site_id: int = self.context.get("view").kwargs.get("id")
            if (
                FireReportPartnerSite.objects.filter(
                    name=name, provider_id=provider.id
                )
                .exclude(id=partner_site_id)
                .exists()
            ):
                raise ValidationError(error_message)
        return name


class FireReportPartnerSiteSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    address_line_1 = serializers.CharField(read_only=True)
    address_line_2 = serializers.CharField(read_only=True)
    city = serializers.CharField(read_only=True)
    state = serializers.CharField(read_only=True)
    state_crm_id = serializers.IntegerField(read_only=True)
    country = serializers.CharField(read_only=True)
    country_crm_id = serializers.IntegerField(read_only=True)
    zip = serializers.CharField(read_only=True)
    phone_number = serializers.CharField(read_only=True)


class FireReportCreateSerializer(serializers.Serializer):
    start_date = serializers.DateField(allow_null=True)
    end_date = serializers.DateField(allow_null=True)
    site_crm_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=False, allow_null=True
    )


class FireReportSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = FireReportSetting
        fields = (
            "provider",
            "receiver_email_ids",
            "subject",
            "frequency",
            "next_run",
            "past_days",
        )
        read_only_fields = ("id", "provider", "next_run")

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        data.update(
            next_run=FireReportSetting.get_next_run(data.get("frequency"))
        )
        return data


class PartnerSiteSyncErrorSerializer(serializers.Serializer):
    operation = serializers.CharField(read_only=True)
    customer_name = serializers.CharField(read_only=True)
    customer_crm_id = serializers.IntegerField(read_only=True)
    site_crm_id = serializers.IntegerField(read_only=True)
    site_name = serializers.CharField(read_only=True)
    site_payload = serializers.JSONField(read_only=True)
    errors = serializers.JSONField(read_only=True)


class AutomatedWeeklyRecapSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutomatedWeeklyRecapSetting
        fields = (
            "provider",
            "receiver_email_ids",
            "sender",
            "weekly_schedule",
            "contact_type_crm_id",
            "email_subject",
            "email_body_text",
            "is_enabled",
        )
        read_only_fields = ("id", "provider")


class WeeklyRecapEmailTriggerDataSerializer(serializers.Serializer):
    all_customers = serializers.BooleanField(required=True)
    customer_crm_ids = serializers.ListField(
        child=serializers.IntegerField(),
        required=True,
        allow_empty=True,
        allow_null=False,
    )


class Client360DashboardSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client360DashboardSetting
        fields = (
            "provider",
            "won_opp_status_crm_ids",
            "open_po_status_crm_ids",
        )
        read_only_fields = ("provider",)


class OpenOpportunitiesChartSerilizer(serializers.Serializer):
    stage_name = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class WonOpportuntiesSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    closed_date = serializers.DateTimeField(read_only=True)
    customer_crm_id = serializers.IntegerField(read_only=True)
    customer_name = serializers.CharField(read_only=True)


class OpenPurchaseOrdersDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    po_number_id = serializers.CharField(read_only=True)
    vendor_company_name = serializers.CharField(read_only=True)
    purchase_order_status_name = serializers.CharField(read_only=True)
    customer_po = serializers.CharField(read_only=True)
    opportunity_name = serializers.CharField(read_only=True)
    customer_name = serializers.CharField(
        read_only=True, source="customer_company_name"
    )
    customer_crm_id = serializers.IntegerField(
        read_only=True, source="customer_company_id"
    )


class OpenServiceTicketsSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    ticket_crm_id = serializers.IntegerField(read_only=True)
    summary = serializers.CharField(read_only=True)
    customer_crm_id = serializers.IntegerField(
        read_only=True, source="company_crm_id"
    )
    customer_name = serializers.CharField(
        read_only=True, source="company_name"
    )
    status_crm_id = serializers.IntegerField(read_only=True)
    status_name = serializers.CharField(read_only=True)
