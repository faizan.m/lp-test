import markdownify
from accounts.models import Provider
from document.models import SOWDocument
from utils import get_app_logger
import re

# regex to find html
# https://regex101.com/r/Va987X/1
regex = r"</?\s*[a-z-][^>]*\s*>|(\&(?:[\w\d]+|#\d+|#x[a-f\d]+);)"

logger = get_app_logger(__name__)


def update_markdown_values_in_markdown_fields(provider_ids):
    """
    Method to update markdown values in markdown field if it contains html values

    Args:
        provider_ids: List of provider ids
    """

    providers = (
        Provider.objects.filter(is_active=True, id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )

    for provider in providers:
        documents = SOWDocument.objects.filter(provider_id=provider.id)
        for doc in documents:
            json_config = doc.json_config
            try:
                if doc.doc_type == SOWDocument.CHANGE_REQUEST:
                    if re.search(
                        regex,
                        json_config.get("reason", {})
                        .get("reason", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("reason", {})
                            .get("reason", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("reason", {}).get("reason", {})[
                            "value_markdown"
                        ] = markdown
                    if re.search(
                        regex,
                        json_config.get("alternates", {})
                        .get("alternates", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("alternates", {})
                            .get("alternates", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("alternates", {}).get(
                            "alternates", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("change_impact", {})
                        .get("change_impact", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("change_impact", {})
                            .get("change_impact", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("change_impact", {}).get(
                            "change_impact", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("technical_changes", {})
                        .get("technical_changes", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("technical_changes", {})
                            .get("technical_changes", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("technical_changes", {}).get(
                            "technical_changes", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("change_description", {})
                        .get("change_description", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("change_description", {})
                            .get("change_description", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("change_description", {}).get(
                            "change_description", {}
                        )["value_markdown"] = markdown
                else:
                    if re.search(
                        regex,
                        json_config.get("terms_t_and_m", {})
                        .get("terms", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("terms_t_and_m", {})
                            .get("terms", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("terms_t_and_m", {}).get("terms", {})[
                            "value_markdown"
                        ] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_details", {})
                        .get("out_of_scope", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_details", {})
                            .get("out_of_scope", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("project_details", {}).get(
                            "out_of_scope", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_details", {})
                        .get("task_deliverables", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_details", {})
                            .get("task_deliverables", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("project_details", {}).get(
                            "task_deliverables", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_details", {})
                        .get("project_assumptions", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_details", {})
                            .get("project_assumptions", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("project_details", {}).get(
                            "project_assumptions", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_outcome", {})
                        .get("project_outcome", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_outcome", {})
                            .get("project_outcome", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("project_outcome", {}).get(
                            "project_outcome", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("terms_fixed_fee", {})
                        .get("terms", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("terms_fixed_fee", {})
                            .get("terms", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("terms_fixed_fee", {}).get(
                            "terms", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_management_t_and_m", {})
                        .get("project_management", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_management_t_and_m", {})
                            .get("project_management", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get("project_management_t_and_m", {}).get(
                            "project_management", {}
                        )["value_markdown"] = markdown
                    if re.search(
                        regex,
                        json_config.get("project_management_fixed_fee", {})
                        .get("project_management", {})
                        .get("value_markdown", ""),
                    ):
                        value_markdown = (
                            json_config.get("project_management_fixed_fee", {})
                            .get("project_management", {})
                            .get("value_markdown")
                        )
                        markdown = markdownify.markdownify(
                            value_markdown, heading_style="ATX"
                        )
                        json_config.get(
                            "project_management_fixed_fee", {}
                        ).get("project_management", {})[
                            "value_markdown"
                        ] = markdown

                # Updating the json_config
                SOWDocument.objects.filter(
                    provider_id=provider.id, id=doc.id
                ).update(json_config=json_config)
                logger.info(f"Updated json config for id {doc.id}")
                print(f"Updated json config for id {doc.id}")
            except Exception as e:
                logger.error(f"Some Error occurred {e}")
