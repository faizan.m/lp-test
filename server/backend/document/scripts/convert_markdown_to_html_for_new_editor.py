import json
import os
from typing import List, Dict
from document.models import SOWDocument, SOWTemplate
from accounts.models import Provider

# These scripts are used to convert the markdown in sections as Project details, Project outcome
# to html as required by new markdown editor.


def script_to_generate_sow_template_content_markdown_json_file():
    """
    Script to create a json file that contains old markdown data of sow templates.
    """
    provider_ids: List[int] = list(
        Provider.objects.filter(is_active=True).values_list("id", flat=True)
    )
    sow_templates: "QuerySet[SOWTemplate]" = SOWTemplate.objects.filter(
        provider_id__in=provider_ids,
        json_config__isnull=False,
    )
    json_data: List = []
    for template in sow_templates:
        template_data: Dict = {}
        json_config: Dict = template.json_config
        project_details: Dict = json_config.get("project_details", {})
        project_outcome: Dict = json_config.get("project_outcome", {})
        service_cost: Dict = json_config.get("service_cost", {})

        project_details__out_of_scope__value: str = project_details.get(
            "out_of_scope", {}
        ).get("value", "")
        project_details__task_deliverables__value: str = project_details.get(
            "task_deliverables", {}
        ).get("value", "")
        project_details__project_assumptions__value: str = project_details.get(
            "project_assumptions", {}
        ).get("value", "")
        project_outcome__project_outcome__value: str = project_outcome.get(
            "project_outcome", {}
        ).get("value", "")
        service_cost__notes: str = service_cost.get("notes", "")
        json_data.append(
            dict(
                template_id=template.id,
                name=template.name,
                provider_id=template.provider_id,
                project_details__out_of_scope__value=project_details__out_of_scope__value,
                project_details__task_deliverables__value=project_details__task_deliverables__value,
                project_details__project_assumptions__value=project_details__project_assumptions__value,
                project_outcome__project_outcome__value=project_outcome__project_outcome__value,
                service_cost__notes=service_cost__notes,
            )
        )
    json_object = json.dumps(json_data, indent=4)
    home_directory_path: str = os.path.expanduser("~")
    file_name: str = "Acela-Prod-Sept-27-SOW-template-content-markdown.json"
    file_path = os.path.join(home_directory_path, file_name)
    with open(file_path, "w") as output_file:
        output_file.write(json_object)
    print(f"File location is: {file_path}")


def replace_markdown_with_html_in_sow_templates_content(file_path: str):
    """
    Script that reads json file and updates the existing markdown in sow templates contents with html values

    Args:
        file_path: File path
    """
    if not os.path.exists(file_path):
        print("No such file exists!")
        return
    with open(file_path, "r") as json_file:
        json_data = json.load(json_file)

    for sow_template_html in json_data:
        template_id: int = sow_template_html.get("template_id")
        provider_id: int = sow_template_html.get("provider_id")
        try:
            sow_template: SOWTemplate = SOWTemplate.objects.get(
                id=template_id, provider_id=provider_id
            )
        except SOWTemplate.DoesNotExist:
            continue

        json_config: Dict = sow_template.json_config
        project_details: Dict = json_config.get("project_details", {})
        project_outcome: Dict = json_config.get("project_outcome", {})
        service_cost: Dict = json_config.get("service_cost", {})

        if sow_template_html.get("project_details__out_of_scope__value", None):
            project_details["out_of_scope"]["value"] = sow_template_html.get(
                "project_details__out_of_scope__value"
            )

        if sow_template_html.get(
            "project_details__task_deliverables__value", None
        ):
            project_details["task_deliverables"][
                "value"
            ] = sow_template_html.get(
                "project_details__task_deliverables__value"
            )

        if sow_template_html.get(
            "project_details__project_assumptions__value", None
        ):
            project_details["project_assumptions"][
                "value"
            ] = sow_template_html.get(
                "project_details__project_assumptions__value"
            )

        if sow_template_html.get(
            "project_outcome__project_outcome__value", None
        ):
            project_outcome["project_outcome"][
                "value"
            ] = sow_template_html.get(
                "project_outcome__project_outcome__value"
            )

        if sow_template_html.get("service_cost__notes", None):
            service_cost["notes"] = sow_template_html.get(
                "service_cost__notes"
            )

        json_config["project_details"] = project_details
        json_config["project_outcome"] = project_outcome
        json_config["service_cost"] = service_cost

        SOWTemplate.objects.filter(
            id=template_id, provider_id=provider_id
        ).update(json_config=json_config)
        print(
            f"Updated markdown values of SOW template content: {sow_template.name}, ID: {template_id}, Provider ID: {provider_id}"
        )
    print("Successfully updated markdown values of SOW templates content!")
