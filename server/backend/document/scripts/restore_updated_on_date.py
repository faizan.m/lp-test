from accounts.models import Provider
from document.models import SOWDocument
import reversion
from reversion.models import Version

provider_ids_list = [14]


def rollback_sow_document_updated_on_value_to_initial_value(provider_ids_list):
    """
    Method to revert updated_on value of document to it's initial value
    Args:
        provider_ids_list: List of provider ids
    """
    providers = Provider.objects.filter(id__in=provider_ids_list)
    for provider in providers:
        SOW_docs = SOWDocument.objects.filter(provider=provider)

        for doc in SOW_docs:
            version_object = None
            try:
                version_object = Version.objects.get_for_object(doc)
            except reversion.RegistrationError:
                continue
            if not version_object or version_object.count() == 0:
                continue
            # Get the initial value of updated_on
            initial_updated_on_value = version_object[0].field_dict[
                "updated_on"
            ]
            # Call update() method as save() won't work in this case.
            SOWDocument.objects.filter(id=doc.id).update(
                updated_on=initial_updated_on_value
            )
