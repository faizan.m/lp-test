from typing import List, Optional

from django.db.models import QuerySet

from accounts.models import Provider
from document.models import SOWDocument
from utils import get_app_logger

logger = get_app_logger(__name__)


# Script to add margin to existing SOW Documents
def add_total_hours_in_json_config_for_existing_sow_documents(
    provider_ids_list: Optional[List[int]] = None,
) -> None:
    """
    Add total hours in json config, service cost for all existing sow documents.

    total hours = project management hours + after hours + engineering_hours + sum up all the hours of hourly resources

    Args:
        provider_ids_list: List of provider IDs
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids_list)
        if provider_ids_list
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        provider_id: int = provider.id
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=provider_id
        )
        for document in sow_documents:
            try:
                project_management_hours = document.json_config.get(
                    "service_cost"
                ).get("project_management_hours", 0)
                after_hours = document.json_config.get("service_cost").get(
                    "after_hours", 0
                )
                engineering_hours = document.json_config.get(
                    "service_cost"
                ).get("engineering_hours", 0)
                hourly_resources = document.json_config.get(
                    "service_cost"
                ).get("hourly_resources", [])
                hourly_resources_hours = 0
                for hours in hourly_resources:
                    hourly_resources_hours = hourly_resources_hours + round(
                        hours.get("hours", 0)
                    )
                total_hours = (
                    round(project_management_hours)
                    + round(after_hours)
                    + round(engineering_hours)
                    + hourly_resources_hours
                )
                document.json_config["service_cost"][
                    "total_hours"
                ] = total_hours
                document.save()
                print(
                    f"Update total hours to SOW documents for SOW Document: {document.name}, ID: {document.id}"
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not add total hours to existing SOW Document: {document.name}, ID: {document.id}"
                    f" for provider: {provider.name}"
                )
                continue
        print(
            f"Finished adding total hours to SOW documents for the provider: {provider.name}, ID: {provider.id}"
        )
