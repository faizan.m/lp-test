import json
import os
from typing import List, Dict
from document.models import SOWDocument, SOWTemplate
from accounts.models import Provider

# These scripts convert the values of project management markdown and terms markdown
# in SOW docs and templates.


def script_to_generate_sow_markdown_json_file():
    """
    Script to create a json file that contains the old sow document markdown values.
    """
    provider_ids: List[int] = list(
        Provider.objects.filter(is_active=True).values_list("id", flat=True)
    )
    doc_types: List[str] = [SOWDocument.GENERAL, SOWDocument.T_AND_M]
    sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
        provider_id__in=provider_ids,
        doc_type__in=doc_types,
        json_config__isnull=False,
    )
    json_data: List = []
    for document in sow_documents:
        json_config: Dict = document.json_config
        project_management_fixed_fee_markdown: str = (
            json_config.get("project_management_fixed_fee", {})
            .get("project_management", {})
            .get("value", "")
        )
        project_management_t_and_m_markdown: str = (
            json_config.get("project_management_t_and_m", {})
            .get("project_management", {})
            .get("value", "")
        )
        terms_fixed_fee_markdown: str = (
            json_config.get("terms_fixed_fee", {})
            .get("terms", {})
            .get("value", "")
        )
        terms_t_and_m_markdown: str = (
            json_config.get("terms_t_and_m", {})
            .get("terms", {})
            .get("value", "")
        )
        json_data.append(
            dict(
                id=document.id,
                name=document.name,
                provider_id=document.provider_id,
                project_management_fixed_fee_markdown=project_management_fixed_fee_markdown,
                project_management_t_and_m_markdown=project_management_t_and_m_markdown,
                terms_fixed_fee_markdown=terms_fixed_fee_markdown,
                terms_t_and_m_markdown=terms_t_and_m_markdown,
            )
        )
    json_object = json.dumps(json_data, indent=4)
    home_directory_path: str = os.path.expanduser("~")
    file_name: str = "Acela-Prod-SOW-docs-markdown.json"
    file_path = os.path.join(home_directory_path, file_name)
    with open(file_path, "w") as output_file:
        output_file.write(json_object)
    print(f"File location is: {file_path}")


def script_to_generate_sow_template_markdown_json_file():
    """
    Script to create a json file that contains the old sow document template markdown values.
    """
    provider_ids: List[int] = list(
        Provider.objects.filter(is_active=True).values_list("id", flat=True)
    )
    sow_templates: "QuerySet[SOWTemplate]" = SOWTemplate.objects.filter(
        provider_id__in=provider_ids,
        json_config__isnull=False,
    )
    json_data: List = []
    for template in sow_templates:
        json_config: Dict = template.json_config
        project_management_fixed_fee_markdown: str = (
            json_config.get("project_management_fixed_fee", {})
            .get("project_management", {})
            .get("value", "")
        )
        project_management_t_and_m_markdown: str = (
            json_config.get("project_management_t_and_m", {})
            .get("project_management", {})
            .get("value", "")
        )
        terms_fixed_fee_markdown: str = (
            json_config.get("terms_fixed_fee", {})
            .get("terms", {})
            .get("value", "")
        )
        terms_t_and_m_markdown: str = (
            json_config.get("terms_t_and_m", {})
            .get("terms", {})
            .get("value", "")
        )
        json_data.append(
            dict(
                template_id=template.id,
                name=template.name,
                provider_id=template.provider_id,
                project_management_fixed_fee_markdown=project_management_fixed_fee_markdown,
                project_management_t_and_m_markdown=project_management_t_and_m_markdown,
                terms_fixed_fee_markdown=terms_fixed_fee_markdown,
                terms_t_and_m_markdown=terms_t_and_m_markdown,
            )
        )
    json_object = json.dumps(json_data, indent=4)
    home_directory_path: str = os.path.expanduser("~")
    file_name: str = "Acela-Prod-SOW-template-markdown.json"
    file_path = os.path.join(home_directory_path, file_name)
    with open(file_path, "w") as output_file:
        output_file.write(json_object)
    print(f"File location is: {file_path}")


def replace_markdown_with_html_in_sow_docs(file_path: str):
    """
    Script that reads json file and updates the existing markdown in sow documents with html values

    Args:
        file_path: File path
    """
    if not os.path.exists(file_path):
        print("No such file exists!")
        return
    with open(file_path, "r") as json_file:
        json_data = json.load(json_file)

    for sow_doc_html in json_data:
        doc_id: int = sow_doc_html.get("id")
        provider_id: int = sow_doc_html.get("provider_id")
        try:
            sow_doc: SOWDocument = SOWDocument.objects.get(
                id=doc_id, provider_id=provider_id
            )
        except SOWDocument.DoesNotExist:
            continue

        json_config: Dict = sow_doc.json_config
        if sow_doc_html.get("project_management_fixed_fee_markdown", None):
            json_config["project_management_fixed_fee"]["project_management"][
                "value"
            ] = sow_doc_html.get("project_management_fixed_fee_markdown")

        if sow_doc_html.get("project_management_t_and_m_markdown", None):
            json_config["project_management_t_and_m"]["project_management"][
                "value"
            ] = sow_doc_html.get("project_management_t_and_m_markdown")

        if sow_doc_html.get("terms_fixed_fee_markdown", None):
            json_config["terms_fixed_fee"]["terms"][
                "value"
            ] = sow_doc_html.get("terms_fixed_fee_markdown")

        if sow_doc_html.get("terms_t_and_m_markdown", None):
            json_config["terms_t_and_m"]["terms"]["value"] = sow_doc_html.get(
                "terms_t_and_m_markdown"
            )

        SOWDocument.objects.filter(id=doc_id, provider_id=provider_id).update(
            json_config=json_config
        )
        print(
            f"Updated markdown values of SOW document: {sow_doc.name}, ID: {doc_id}, Provider ID: {provider_id}"
        )
    print("Successfully updated markdown values of SOW documents!")


def replace_markdown_with_html_in_sow_templates(file_path: str):
    """
    Script that reads json file and updates the existing markdown in sow templates with html values

    Args:
        file_path: File path
    """
    if not os.path.exists(file_path):
        print("No such file exists!")
        return
    with open(file_path, "r") as json_file:
        json_data = json.load(json_file)

    for sow_template_html in json_data:
        template_id: int = sow_template_html.get("template_id")
        provider_id: int = sow_template_html.get("provider_id")
        try:
            sow_template: SOWTemplate = SOWTemplate.objects.get(
                id=template_id, provider_id=provider_id
            )
        except SOWTemplate.DoesNotExist:
            continue

        json_config: Dict = sow_template.json_config
        if sow_template_html.get(
            "project_management_fixed_fee_markdown", None
        ):
            json_config["project_management_fixed_fee"]["project_management"][
                "value"
            ] = sow_template_html.get("project_management_fixed_fee_markdown")

        if sow_template_html.get("project_management_t_and_m_markdown", None):
            json_config["project_management_t_and_m"]["project_management"][
                "value"
            ] = sow_template_html.get("project_management_t_and_m_markdown")

        if sow_template_html.get("terms_fixed_fee_markdown", None):
            json_config["terms_fixed_fee"]["terms"][
                "value"
            ] = sow_template_html.get("terms_fixed_fee_markdown")

        if sow_template_html.get("terms_t_and_m_markdown", None):
            json_config["terms_t_and_m"]["terms"][
                "value"
            ] = sow_template_html.get("terms_t_and_m_markdown")

        SOWTemplate.objects.filter(
            id=template_id, provider_id=provider_id
        ).update(json_config=json_config)
        print(
            f"Updated markdown values of SOW template: {sow_template.name}, ID: {template_id}, Provider ID: {provider_id}"
        )
    print("Successfully updated markdown values of SOW templates!")
