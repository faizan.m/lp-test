from document.models import SOWDocument, SOWTemplate
from typing import List, Dict, Optional, Union, Any
from accounts.models import Provider


def add_integration_technician_fields_in_sow_templates(
    provider_ids: Optional[List[int]] = None,
):
    """
    Populates following fields for existing SoW templates.
        integration_technician_hours=0,
        integration_technician_hours_override=False,
        integration_technician_hourly_rate=0,

    Args:
        provider_ids: List of provider IDs

    Returns:
        None
    """

    # WARNING: This should be run only once for existing SoW templates. Running again will reset existing
    # integration technician values.

    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )

    for provider in providers:
        sow_templates: "QuerySet[SOWTemplate]" = SOWTemplate.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {sow_templates.count()} SoW templates for the Provider {provider.name}, ID: {provider.id}."
        )
        for template in sow_templates:
            json_config: Dict[str, Any] = template.json_config
            service_cost: Dict[str, Any] = json_config.get("service_cost")
            service_cost.update(
                integration_technician_hours=0,
                integration_technician_hours_override=False,
                integration_technician_hourly_rate=0,
            )
            json_config.update(service_cost=service_cost)
            SOWTemplate.objects.filter(
                provider_id=provider.id, id=template.id
            ).update(json_config=json_config)
            print(f"Updated SoW template with ID: {template.id}.")
        print(
            f"Updated SoW template for the Provider {provider.name}, ID: {provider.id}. \n"
        )
    print(
        f"Successfully added integration technician fields for all provider sow templates!"
    )
    return


def add_integration_technician_fields_in_sow_documents(
    provider_ids: Optional[List[int]] = None,
):
    """
    Populates following fields for existing SoW documents.
        integration_technician_hours=0,
        integration_technician_hours_override=False,
        integration_technician_hourly_rate=0,

    Args:
        provider_ids: List of provider IDs

    Returns:
        None
    """

    # WARNING: This should be run only once for existing SoW templates. Running again will reset existing
    # integration technician values.

    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )

    for provider in providers:
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {sow_documents.count()} SoW documents for the Provider {provider.name}, ID: {provider.id}."
        )
        for document in sow_documents:
            json_config: Dict[str, Any] = document.json_config
            service_cost: Dict[str, Any] = json_config.get("service_cost")
            service_cost.update(
                integration_technician_hours=0,
                integration_technician_hours_override=False,
                integration_technician_hourly_rate=0,
            )
            json_config.update(service_cost=service_cost)
            SOWDocument.objects.filter(
                provider_id=provider.id, id=document.id
            ).update(json_config=json_config)
            print(f"Updated SoW document with ID: {document.id}.")
        print(
            f"Updated SoW documents for the Provider {provider.name}, ID: {provider.id}. \n"
        )
    print(
        f"Successfully added integration technician fields for all provider sow documents!"
    )
    return
