import asyncio
import json
from typing import List, Dict, Optional, Any

from aiohttp.client import ClientSession

from accounts.models import Customer, Provider
from core.integrations.erp.connectwise.connectwise import PatchGroup
from document.tasks.fire_report import (
    get_erp_client_headers,
    CUSTOMER_SITES_ENDPOINT,
)

UPDATED_TAYLOR_SITE_NAME: str = "LookingPoint-HQ-391"
UPDATED_AUBURN_SITE_NAME: str = "LookingPoint-WH-850"
TAYLOR_SITE_ADDRESS_LINE_SUFFIX: str = "391 Taylor"
AUBURN_SITE_ADDRESS_LINE_SUFFIX: str = "850 Auburn"


def filter_out_sites(
    sites: List[Dict[str, Any]], address_line_1_suffix: str
) -> List[Dict[str, Any]]:
    """
    Filter out sites that have address line 1 start with address_line_1_suffix

    Args:
        sites: Customer sites
        address_line_1_suffix: Address line 1 suffix

    Returns:
        Sites that match address_line_1_suffix
    """
    sites_to_rename: List[Dict[str, Any]] = list()
    for site in sites:
        address_line_1: str = site.get("addressLine1", "")
        if address_line_1.startswith(address_line_1_suffix):
            sites_to_rename.append(site)
    return sites_to_rename


async def async_patch_customer_site(
    session: ClientSession,
    customer_crm_id: int,
    site_crm_id: int,
    updated_site_payload: Dict,
    site: str,
) -> None:
    """
    Patch customer site.

    Args:
        session: ClientSession
        customer_crm_id: Customer CRM ID
        site_crm_id: Site CRM ID
        updated_site_payload: Updated site data
        site: Site identifier

    Returns:
        None
    """
    url: str = (
        f"{CUSTOMER_SITES_ENDPOINT}{customer_crm_id}/sites/{site_crm_id}"
    )
    patch = PatchGroup()
    patch.add("name", updated_site_payload.get("name"))
    patch.add("addressLine1", updated_site_payload.get("addressLine1"))
    patch.add("addressLine2", updated_site_payload.get("addressLine2"))
    patch.add("city", updated_site_payload.get("city"))
    patch.add("stateReference", updated_site_payload.get("stateReference"))
    patch.add("zip", updated_site_payload.get("zip"))

    async with session.patch(
        url, data=json.dumps(patch.to_dict())
    ) as patch_reponse:
        if patch_reponse.status == 200:
            await patch_reponse.json()
            print(
                f"Successfully renamed {site} site for customer CRM ID: {customer_crm_id}"
            )
        else:
            print(
                f"{patch_reponse.status} CW Error renaming {site} site for customer "
                f"CRM ID: {customer_crm_id}"
            )
    return


async def rename_site_for_customer(
    session: ClientSession, customer_crm_id: int
) -> None:
    """
    Rename site for customer.

    Args:
        session: ClientSession
        customer_crm_id: Customer CRM ID

    Returns:
        None
    """
    customer_site_url: str = (
        f"{CUSTOMER_SITES_ENDPOINT}{customer_crm_id}/sites"
    )
    query_params: Dict[str, str] = dict(
        fields="id,name,addressLine1",
        orderBy="_info/lastUpdated desc",
    )
    async with session.get(customer_site_url, params=query_params) as response:
        if response.status == 200:
            existing_customer_sites: List[Dict] = await response.json()
            # For some customers there could be multiple sites with same address line (i.e. Duplicate sites)
            taylor_sites: List[Dict] = filter_out_sites(
                existing_customer_sites, TAYLOR_SITE_ADDRESS_LINE_SUFFIX
            )
            auburn_sites: List[Dict] = filter_out_sites(
                existing_customer_sites, AUBURN_SITE_ADDRESS_LINE_SUFFIX
            )
            taylor_site_payload: Dict = dict(
                name=UPDATED_TAYLOR_SITE_NAME,
                addressLine1="391 Taylor Blvd.",
                addressLine2="Suite 120",
                city="Pleasant Hill",
                stateReference=dict(id=5),
                zip="94523",
            )
            auburn_site_payload: Dict = dict(
                name=UPDATED_AUBURN_SITE_NAME,
                addressLine1="850 Auburn Ct.",
                addressLine2=None,
                city="Fremont",
                stateReference=dict(id=5),
                zip="94538",
            )
            for site in taylor_sites:
                await async_patch_customer_site(
                    session,
                    customer_crm_id,
                    site.get("id"),
                    taylor_site_payload,
                    "Taylor",
                )
            for site in auburn_sites:
                await async_patch_customer_site(
                    session,
                    customer_crm_id,
                    site.get("id"),
                    auburn_site_payload,
                    "Auburn",
                )
        else:
            print(
                f"{response.status} CW Error fetching sites for customer "
                f"CRM ID: {customer_crm_id}"
            )
    return


async def async_rename_customer_sites(
    provider: Provider, customer_crm_ids: List[int]
) -> None:
    """
    Rename customer sites.

    Args:
        provider: Provider
        customer_crm_ids: Customer CRM IDs.

    Returns:
        None
    """
    headers: Dict[str, str] = get_erp_client_headers(provider)
    tasks: List = list()
    async with ClientSession(headers=headers) as session:
        for customer_crm_id in customer_crm_ids:
            tasks.append(
                asyncio.ensure_future(
                    rename_site_for_customer(
                        session,
                        customer_crm_id,
                    )
                )
            )
        await asyncio.gather(*tasks)
    return


def rename_customer_sites_for_providers(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Rename customer sites for provider.
    Renames 2 sites whose address line 1 starts with 391 Taylor & 850 Auburn respectively.

    Args:
        provider_ids: List of provider IDs.

    Returns:
        None
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        if provider.is_configured:
            customer_crm_ids: List[int] = list(
                Customer.objects.filter(
                    provider_id=provider.id, is_active=True
                )
                .order_by("crm_id")
                .values_list("crm_id", flat=True)
            )
            asyncio.run(
                async_rename_customer_sites(provider, customer_crm_ids)
            )
            print(
                f"Successfully renamed sites for provider: {provider.name}, ID: {provider.id} \n\n"
            )
    print("Succesfully renamed sites for customers of all provider!")
    return
