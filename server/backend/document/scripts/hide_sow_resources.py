from typing import List, Dict, Optional, Any

from accounts.models import Provider
from document.models import SOWDocument, SOWTemplate


def add_fields_to_service_cost(service_cost: Dict[str, Any]) -> Dict[str, Any]:
    """
    Adds following fields (if not present) to service cost:
        hide_engineering_hours=False
        hide_after_hours=False
        hide_project_management_hours=False
        hide_integration_technician_hours=False
    Adds is_hidden=False to hourly resources.

    Args:
        service_cost: Service cost

    Returns:
        Updated service cost
    """
    if "hide_engineering_hours" not in service_cost:
        service_cost.update(hide_engineering_hours=False)
    if "hide_after_hours" not in service_cost:
        service_cost.update(hide_after_hours=False)
    if "hide_project_management_hours" not in service_cost:
        service_cost.update(hide_project_management_hours=False)
    if "hide_integration_technician_hours" not in service_cost:
        service_cost.update(hide_integration_technician_hours=False)
    hourly_resources: List[Dict] = service_cost.get("hourly_resources", [])
    if hourly_resources:
        for resource in hourly_resources:
            if "is_hidden" not in resource:
                resource.update(is_hidden=False)
        service_cost.update(hourly_resources=hourly_resources)
    return service_cost


def add_fields_to_sow_templates(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Adds following fields (if not present) to service cost data of SoW templates.
        hide_engineering_hours=False
        hide_after_hours=False
        hide_project_management_hours=False
        hide_integration_technician_hours=False
    Adds is_hidden=False to hourly resources.

    Args:
        provider_ids: List of provider IDs
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        sow_templates: "QuerySet[SOWTemplate]" = SOWTemplate.objects.filter(
            provider_id=provider.id
        )
        print(
            f"Found {sow_templates.count()} SoW templates for the provider: {provider.name}"
        )
        for template in sow_templates:
            json_config: Dict = template.json_config
            updated_service_cost: Dict = add_fields_to_service_cost(
                json_config.get("service_cost", {})
            )
            json_config.update(service_cost=updated_service_cost)
            SOWTemplate.objects.filter(
                id=template.id, provider_id=provider.id
            ).update(json_config=json_config)
            print(f"Updated SoW template having ID: {template.id}")
        print(
            f"Successfully updated SoW templates of provider: {provider.name} \n"
        )
    print(f"SoW templates updated for all providers!\n\n")


def add_fields_to_sow_docs(
    provider_ids: Optional[List[int]] = None,
):
    """
    Adds following fields (if not present) to service cost data of SoW documents.
        hide_engineering_hours=False
        hide_after_hours=False
        hide_project_management_hours=False
        hide_integration_technician_hours=False
    Adds is_hidden=False to hourly resources.

    Args:
        provider_ids: List of provider IDs
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=provider.id
        )
        for document in sow_documents:
            json_config: Dict = document.json_config
            updated_service_cost: Dict = add_fields_to_service_cost(
                json_config.get("service_cost", {})
            )
            json_config.update(service_cost=updated_service_cost)
            SOWDocument.objects.filter(
                id=document.id, provider_id=provider.id
            ).update(json_config=json_config)
            print(f"Updated SoW document having ID: {document.id}")
        print(
            f"Successfully updated SoW documents of provider: {provider.name} \n"
        )
    print(f"SoW documents updated for all providers! \n\n")


def add_fields_to_sow_templates_and_documents(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Add required fields to SoW docs & templates. The fields are used to hide resources
    in SoW documents.
    """
    print("Updating SoW templates...")
    add_fields_to_sow_templates(provider_ids)
    print("Updating SoW documents...")
    add_fields_to_sow_docs(provider_ids)
    return
