from typing import List, Optional

from django.db.models import QuerySet

from accounts.models import Provider
from document.models import SOWDocument
from document.services import DocumentService
from utils import get_app_logger

logger = get_app_logger(__name__)


# Script to add margin to existing SOW Documents
def add_margin_to_existing_sow_documents(
    provider_ids_list: Optional[List[int]] = None,
) -> None:
    """
    Add margin to existing sow documents.

    Args:
        provider_ids_list: List of provider IDs
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids_list)
        if provider_ids_list
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        provider_id: int = provider.id
        sow_documents: "QuerySet[SOWDocument]" = SOWDocument.objects.filter(
            provider_id=provider_id
        )
        for document in sow_documents:
            try:
                document_margin: float = DocumentService.get_document_margin(
                    document
                )
            except Exception as exc:
                logger.error(exc)
                print(
                    f"Could not add margin to existing SOW Document: {document.name}, ID: {document.id}"
                    f" for provider: {provider.name}"
                )
                continue
            # Using Queryset.update() so only 'margin' field will be updated and 'updated_on' will remain same
            SOWDocument.objects.filter(
                id=document.id, provider_id=provider_id
            ).update(margin=document_margin)
        print(
            f"Finished adding margin to SOW documents for the provider: {provider.name}, ID: {provider.id}"
        )
