from project_management.models import ChangeRequest
from utils import get_app_logger

logger = get_app_logger(__name__)


def update_change_request_type():
    # Update the existing CRs type to Fixed Fee
    try:
        change_requests = ChangeRequest.objects.filter(
            change_request_type__isnull=True
        )
        for change_request in change_requests:
            change_request.change_request_type = ChangeRequest.FIXED_FEE
            change_request.save()
        logger.info(
            "Successfully updated change request type as Fixed Fee for all existing CRs"
        )
    except Exception as e:
        logger.error(f"Exception while updating the type of existing CRs: {e}")
