from document.serializers import SOWDocumentUpdateSerializer
from accounts.models import Provider
from document.models import SOWDocument
from document.services.quote_service import QuoteService
from utils import get_app_logger
from document.tasks.document_tasks import upload_sow_and_service_detail_in_cw
from document.services.document_service import DocumentService

logger = get_app_logger(__name__)


def update_opp_forecasts_for_existing_fixed_fee_docs(provider_ids):
    """
    Method to update forecasts for existing Fixed Fee documents.

    Args:
        provider_ids: List of provider ids
    """
    VALID_DOC_TYPES = [SOWDocument.GENERAL]
    OPEN_STATUS_NAME = DocumentService.OPEN

    providers = (
        Provider.objects.filter(is_active=True, id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    serializer = SOWDocumentUpdateSerializer

    for provider in providers:
        documents = SOWDocument.objects.filter(
            provider_id=provider.id, doc_type__in=VALID_DOC_TYPES
        )
        quote_ids = documents.values_list("quote_id", flat=True)
        quotes = provider.erp_client.get_company_quote_statuses(
            list(quote_ids)
        )

        open_quote_ids = []
        for quote in quotes:
            if (
                quote.get("status_name", "").lower()
                == OPEN_STATUS_NAME.lower()
            ):
                open_quote_ids.append(quote.get("id"))
        documents = documents.filter(quote_id__in=open_quote_ids)

        for document in documents:
            document_data = serializer(document).data
            quote_id = document_data.get("quote_id")
            try:
                forecast = QuoteService.create_or_update_forecast(
                    provider.id, quote_id, document_data
                )
            except Exception as exc:
                logger.info(exc)
                continue
            forecast_id = forecast.get("id")
            # Calling update() method to keep the updated_on value same as before
            SOWDocument.objects.filter(
                provider_id=provider.id, id=document.id
            ).update(forecast_id=forecast_id)
            # Update the CW service detail document
            # Not notifying any changes to Account Manager.
            # Can be done by passing email_account_manager=True
            try:
                upload_sow_and_service_detail_in_cw(
                    provider.id, document.updated_by_id, document.id
                )
            except Exception as exc:
                logger.info(exc)
                continue
