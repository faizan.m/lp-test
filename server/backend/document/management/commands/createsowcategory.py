from django.core.management import BaseCommand

from document.models import SOWCategory


class Command(BaseCommand):
    help = "To create SOW Base Templates"

    def handle(self, *args, **options):
        categories = ["Collaboration", "Security", "Data Center", "Networking"]
        categories = [
            SOWCategory(**{"name": category}) for category in categories
        ]
        SOWCategory.objects.bulk_create(categories)
        self.stdout.write("SOW Categories created")
