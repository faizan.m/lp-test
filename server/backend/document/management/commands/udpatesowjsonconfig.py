from django.core.management import BaseCommand

from document.models import SOWTemplate, SOWDocument


def parse(data_json, result):
    """
    Parse the fields that needs to be updated.
    """
    if "input_type" in data_json:
        result["input_type"] = data_json.get("input_type")
    for key, value in data_json.items():
        if isinstance(value, dict):
            result[key] = {}
            parse(value, result[key])
    return result


def merge(a, b, path=None):
    """
    Merges b into a
    """
    if path is None:
        path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                raise Exception("Conflict at %s" % ".".join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a


class Command(BaseCommand):
    help = "To update existing SOW Templates with the new json config"

    def handle(self, *args, **options):
        for template in SOWTemplate.objects.all():
            input_values = parse(template.base_template.json_config, {})
            template.json_config = merge(template.json_config, input_values)
            template.save()
        for document in SOWDocument.objects.all():
            input_values = parse(document.base_template.json_config, {})
            document.json_config = merge(document.json_config, input_values)
            document.save()

        self.stdout.write("SOWDocument and SOWTemplate json config updated.")
