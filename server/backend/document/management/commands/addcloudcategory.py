from django.core.management import BaseCommand

from document.models import SOWCategory


class Command(BaseCommand):
    help = "Add Cloud category"

    def handle(self, *args, **options):
        category_to_add = "Cloud"
        SOWCategory.objects.bulk_create([SOWCategory(name=category_to_add)])
        self.stdout.write("Cloud category added.")
