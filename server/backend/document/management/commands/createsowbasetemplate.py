from django.core.management import BaseCommand

from accounts.models import Provider
from document.utils import create_default_sow_base_templates


class Command(BaseCommand):
    help = "To create SOW Base Templates"

    def handle(self, *args, **options):
        for provider in Provider.objects.all():
            self.stdout.write(
                self.style.SUCCESS(
                    f"Creating SOW Base Template Object for Provider {provider.name}"
                )
            )
            create_default_sow_base_templates(provider)

        self.stdout.write(self.style.SUCCESS("SOW created for all Providers"))
