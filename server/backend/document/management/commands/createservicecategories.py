from django.core.management import BaseCommand

from accounts.models import Provider
from document.utils import create_default_service_category


class Command(BaseCommand):
    help = "Add default Service Categories"

    def handle(self, *args, **options):
        for provider in Provider.objects.all():
            create_default_service_category(provider)
        self.stdout.write(
            self.style.SUCCESS("Added default Service Categories")
        )
