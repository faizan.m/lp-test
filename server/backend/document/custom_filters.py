# import django_filters
from django.db.models import Q

from accounts.models import Provider
from common.custom_filters import ArrayFieldContainsFilter, ListFilter
from document.models import (
    CustomerNotes,
    SOWDocument,
    ServiceCatalog,
    SOWTemplate,
    ConnectwisePurchaseOrderData,
    SalesPurchaseOrderData,
    SalesOrderStatusSettings,
    SoWHourlyResourcesDescriptionMapping,
    PartnerSiteSyncError,
    ConnectwiseOpportunityData,
    ConnectwiseServiceTicket,
)
from django_filters import rest_framework as filters


class QuoteFilterSet(filters.FilterSet):
    customer__id = ListFilter(field_name="customer__id")
    author__id = ListFilter(field_name="author__id")

    class Meta:
        model = SOWDocument
        fields = ["customer__id", "author__id"]


class ServiceCatalogFilterSet(filters.FilterSet):
    service_type = ListFilter()
    service_category = ListFilter()
    service_technology_types = ListFilter()

    class Meta:
        model = ServiceCatalog
        fields = [
            "service_type",
            "service_category",
            "service_technology_types",
            "is_template_present",
        ]


class SOWTemplateFilterSet(filters.FilterSet):
    category = ListFilter()
    service_catalog_category = ListFilter()
    service_technology_types = ListFilter()

    class Meta:
        model = SOWTemplate
        fields = [
            "category",
            "service_catalog_category",
            "service_technology_types",
        ]


class CustomerNotesFilterSet(filters.FilterSet):
    technology_ids = ArrayFieldContainsFilter()
    site_ids = ArrayFieldContainsFilter()
    contact_ids = ArrayFieldContainsFilter()
    opportunity_ids = ArrayFieldContainsFilter()
    customer_id = ListFilter()
    updated_on = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = CustomerNotes
        fields = [
            "opportunity_ids",
            "contact_ids",
            "site_ids",
            "technology_ids",
            "customer_id",
            "updated_on",
        ]


class OrderTrackingDashboardTableDataFilterSet(filters.FilterSet):
    connectwise_last_data_update_after = filters.DateFilter(
        field_name="po_number__ticket__connectwise_last_data_update",
        lookup_expr=("date__gte"),
    )
    connectwise_last_data_update_before = filters.DateFilter(
        field_name="po_number__ticket__connectwise_last_data_update",
        lookup_expr=("date__lt"),
    )
    ingram_eta_after = filters.DateFilter(
        field_name="eta",
        lookup_expr=("gte"),
    )
    ingram_eta_before = filters.DateFilter(
        field_name="eta", lookup_expr=("lt")
    )
    ingram_eta_is_null = filters.BooleanFilter(
        field_name="eta", lookup_expr=("isnull")
    )

    class Meta:
        model = ConnectwisePurchaseOrderData
        fields = [
            "vendor_company_name",
            "connectwise_last_data_update_after",
            "connectwise_last_data_update_before",
            "ingram_eta_after",
            "ingram_eta_before",
            "ingram_eta_is_null",
        ]


class SOWDashboardTableDataFilterSet(filters.FilterSet):
    technology_type = filters.CharFilter(
        field_name="category__name", lookup_expr=("icontains")
    )
    updated_after = filters.DateFilter(
        field_name="updated_on", lookup_expr=("date__gte")
    )
    updated_before = filters.DateFilter(
        field_name="updated_on", lookup_expr=("date__lte")
    )
    customer_name = filters.CharFilter(
        field_name="customer__name", lookup_expr=("icontains")
    )
    author_id = filters.CharFilter(
        field_name="author_id", lookup_expr=("exact")
    )
    customer_id = filters.NumberFilter(
        field_name="customer_id", lookup_expr=("exact")
    )

    class Meta:
        model = SOWDocument
        fields = [
            "technology_type",
            "updated_after",
            "updated_before",
            "customer_name",
            "author_id",
            "customer_id",
        ]


class CustomerSalesOrderDataFilterSet(filters.FilterSet):
    is_closed = filters.BooleanFilter(method="is_closed_filter")

    class Meta:
        model = SalesPurchaseOrderData
        fields = ["is_closed"]

    def is_closed_filter(self, queryset, name, value):
        provider: Provider = self.request.provider
        customer_id = self.request.user.customer.id
        sales_order_status_settings = SalesOrderStatusSettings.objects.filter(
            provider=provider
        ).values(
            "in_progress_status",
            "waiting_on_smartnet_status",
            "closed_statuses",
        )
        filter_conditions = dict(customer_id=customer_id, provider=provider)
        if sales_order_status_settings:
            sales_order_status_settings = sales_order_status_settings[0]
            if value:
                filter_conditions.update(
                    dict(
                        status_id__in=sales_order_status_settings.get(
                            "closed_statuses"
                        )
                    )
                )
            else:
                open_statuses_ids = [
                    sales_order_status_settings.get("in_progress_status"),
                    sales_order_status_settings.get(
                        "waiting_on_smartnet_status"
                    ),
                ]
                filter_conditions.update(dict(status_id__in=open_statuses_ids))
        return queryset.filter(**filter_conditions)


class UnlinkedPurchaseOrdersListingFilterSet(filters.FilterSet):
    customer = filters.CharFilter(
        field_name="po_number__ticket__company_name", lookup_expr="iexact"
    )
    po_number = filters.CharFilter(
        field_name="po_number__po_number", lookup_expr="iexact"
    )

    class Meta:
        model = ConnectwisePurchaseOrderData
        fields = ["customer", "po_number"]


class OpportunityTicketSOPOLinkingDataFilterSet(filters.FilterSet):
    unmapped_service_tickets = filters.BooleanFilter(
        method="filter_sales_order_without_service_ticket_mapping"
    )
    unmapped_purchase_orders = filters.BooleanFilter(
        method="filter_sales_order_without_purchase_order_mapping"
    )
    mapped_tickets_sos_pos = filters.BooleanFilter(
        method="filter_sales_order_with_ticket_and_purchase_order_mapping"
    )

    def filter_sales_order_without_service_ticket_mapping(
        self, queryset, name, value
    ):
        if not value:
            return queryset
        return queryset.filter(opportunity_crm_id__ticket_crm_id=None)

    def filter_sales_order_without_purchase_order_mapping(
        self, queryset, name, value
    ):
        if not value:
            return queryset
        return queryset.filter(linked_pos__po_number_id=None)

    def filter_sales_order_with_ticket_and_purchase_order_mapping(
        self, queryset, name, value
    ):
        if not value:
            return queryset
        return queryset.filter(
            Q(opportunity_crm_id__ticket_crm_id__isnull=False)
            and Q(linked_pos__po_number_id__isnull=False)
            and Q(opportunity_crm_id__isnull=False)
        ).filter(~Q(linked_pos__po_number_id=None))


class SoWHourlyResourcesDescriptionMappingFilterSet(filters.FilterSet):
    vendor_name = ListFilter()
    vendor_crm_id = ListFilter()

    class Meta:
        model = SoWHourlyResourcesDescriptionMapping
        fields = (
            "vendor_name",
            "vendor_crm_id",
        )


class PartnerSiteSyncErrorFilterset(filters.FilterSet):
    operation = filters.CharFilter(
        field_name="operation",
        lookup_expr="exact",
    )
    customer_name = filters.CharFilter(
        field_name="customer_name", lookup_expr="iexact"
    )
    site_name = filters.CharFilter(
        field_name="site_name", lookup_expr="iexact"
    )
    site_crm_id = filters.NumberFilter(
        field_name="site_crm_id", lookup_expr="exact"
    )
    customer_crm_id = filters.NumberFilter(
        field_name="customer_crm_id", lookup_expr="exact"
    )

    class Meta:
        model = PartnerSiteSyncError
        fields = (
            "operation",
            "customer_name",
            "site_name",
            "site_crm_id",
            "customer_crm_id",
        )


class OpportunitiesChartFilterset(filters.FilterSet):
    customer_name = filters.CharFilter(
        field_name="customer_name", lookup_expr="iexact"
    )
    customer_crm_id = filters.NumberFilter(
        field_name="customer_crm_id", lookup_expr="exact"
    )
    stage_crm_id = filters.NumberFilter(
        field_name="stage_crm_id", lookup_expr="exact"
    )
    stage_name = filters.CharFilter(
        field_name="stage_name", lookup_expr="iexact"
    )
    closed_after = filters.DateFilter(
        field_name="closed_date", lookup_expr=("date__gte")
    )
    closed_before = filters.DateFilter(
        field_name="closed_date", lookup_expr=("date__lte")
    )

    class Meta:
        model = ConnectwiseOpportunityData
        fields = (
            "customer_name",
            "customer_crm_id",
            "stage_crm_id",
            "stage_name",
            "closed_before",
            "closed_after",
        )


class OpenPOChartFilterset(filters.FilterSet):
    customer_crm_id = filters.NumberFilter(
        field_name="customer_company_id", lookup_expr="exact"
    )
    customer_name = filters.CharFilter(
        field_name="customer_company_name", lookup_expr="iexact"
    )

    class Meta:
        model = ConnectwisePurchaseOrderData
        fields = (
            "customer_crm_id",
            "customer_name",
        )


class OpenServiceTicketsChartFilterset(filters.FilterSet):
    customer_crm_id = filters.NumberFilter(
        field_name="company_crm_id", lookup_expr="exact"
    )
    customer_name = filters.CharFilter(
        field_name="company_name", lookup_expr="iexact"
    )

    class Meta:
        model = ConnectwiseServiceTicket
        fields = (
            "customer_crm_id",
            "customer_name",
        )
