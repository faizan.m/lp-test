import datetime

import pytz
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.files.base import ContentFile

from accounts.email import SoWDocumentUpdateEmail
from accounts.models import Provider, User
from document import utils
from document.models import DashboardMetricData, SOWDocument
from document.services import (
    ChangeRequestSOWService,
    DocumentService,
    QuoteService,
    SOWDashboardService,
)
from typing import List, Dict, Union, Optional, Any
from exceptions.exceptions import APIError

logger = get_task_logger(__name__)


@shared_task
def email_sow_document_to_account_manager(
    cw_customer,
    document,
    pdf_document,
    quote,
    territory_manager,
    user,
    provider_id,
):
    if not cw_customer.get("territory_manager_id"):
        logger.info(
            f"Territory Manager Email is Not Present Customer CRM ID: {document['id']}"
        )
        return
    file_path, file_name = pdf_document.get("file_path"), pdf_document.get(
        "file_name"
    )
    attachments = [(file_name, file_path)]
    files, remote_attachments = [], []
    if settings.DEBUG:
        files = attachments
    else:
        remote_attachments = attachments
    provider = Provider.objects.get(id=provider_id)
    context = {
        "user": user,
        "name": document["name"],
        "territory_manager": territory_manager,
        "customer_name": cw_customer.get("name"),
        "files": [file_name],
        "quote_name": quote.get("name"),
        "provider": provider,
    }
    SoWDocumentUpdateEmail(
        context=context, files=files, remote_attachments=remote_attachments
    ).send(to=[territory_manager.get("email")], cc=[user["email"]])


@shared_task
def _calculate_and_store_expected_engineering_hours_metric(
    provider_id, last_sunday
):
    """
    Task to calculate expected engineering hours and store in database
    """
    provider = Provider.objects.get(id=provider_id)
    documents = SOWDashboardService.get_dashboard_data(provider)
    expected_engineering_hours = (
        SOWDashboardService.calculate_expected_engineering_hours(
            documents, last_sunday, provider
        )
    )
    DashboardMetricData.objects.create(
        provider=provider,
        metric=DashboardMetricData.EXPECTED_ENGINEERING_HOURS,
        value=expected_engineering_hours,
        timestamp=last_sunday,
    )
    logger.info(f"Expected Engineering Hours metric stored for {provider}")


@shared_task
def expected_engineering_hours_metric_task():
    """
    Task to schedule task for engineering hour metric calculation
    based on the Provider timezone
    Returns:

    """
    # Task need to be run on Monday according to timezone
    TASK_SCHEDULE_WEEKDAY = 1
    for provider in Provider.objects.all():
        if provider.is_configured:
            timezone = pytz.timezone(provider.timezone or pytz.UTC)
            current_time = datetime.datetime.now(tz=timezone)
            current_weekday = current_time.isoweekday()
            if current_weekday != TASK_SCHEDULE_WEEKDAY:
                continue
            last_sunday = current_time - datetime.timedelta(current_weekday)
            query = DashboardMetricData.objects.filter(
                provider=provider,
                metric=DashboardMetricData.EXPECTED_ENGINEERING_HOURS,
                timestamp__date=last_sunday.date(),
            ).exists()
            if not query:
                _calculate_and_store_expected_engineering_hours_metric(
                    provider.id, last_sunday
                )


@shared_task
def upload_sow_document_pdf_to_opportunity_and_email_account_manager(
    provider_id: int,
    user_id: str,
    sow_document_id: int,
    email_sow_to_account_manager: bool,
) -> None:
    """
    Task to upload SoW document PDF to CW opportunity.

    Args:
        provider_id: Provider ID
        user_id: User ID
        sow_document_id: SoW Document ID
        email_sow_to_account_manager: True to send SoW PDF to account manager else False
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    user: User = User.objects.get(id=user_id)
    sow_document: SOWDocument = DocumentService.get_document(
        provider_id, sow_document_id
    )

    quote_id: int = sow_document.quote_id
    quote: Dict[str, Any] = dict()
    try:
        quote: Dict = QuoteService.get_quote(provider_id, quote_id)
    except APIError as err:
        logger.info(
            f"CW opportunity with quote_id: {quote_id} not found for SoW document ID: {sow_document_id}, "
            f"Provider ID: {provider_id}. Error: {err}"
        )
        return

    if sow_document.doc_type == SOWDocument.CHANGE_REQUEST:
        service_class: ChangeRequestSOWService = ChangeRequestSOWService
    else:
        service_class: DocumentService = DocumentService

    sow_document_pdf: Dict[str, str] = service_class.download_document(
        provider_id, erp_client, user, sow_document_id, "pdf", upload=False
    )

    existing_quote_attachments: List[Dict] = erp_client.get_quote_documents(
        quote_id
    )
    for attachment in existing_quote_attachments:
        attachment_name: str = attachment.get("server_file_name").lower()
        # The sow document file name contains '- SoW'. Refer to sow document name format.
        sow_substring: str = "- SoW".lower()
        if sow_substring in attachment_name:
            erp_client.get_delete_documents(attachment["id"])

    file_path: str = sow_document_pdf["file_path"]
    if not settings.DEBUG:
        file_path: str = utils.download_file(file_path)

    try:
        with open(file_path, "rb") as file:
            content_file = ContentFile(file.read())
            content_file.name = sow_document_pdf["file_name"]
            payload: Dict = dict(
                file=content_file,
                record_type="Opportunity",
                record_id=quote_id,
            )
            erp_client.upload_system_attachments(payload)
    except OSError as err:
        logger.error(
            f"OS error reading SoW PDF. SoW document ID: {sow_document.id}, Provider ID: {provider_id}, Error: {err}"
        )
        return
    except APIError as err:
        logger.error(
            f"CW API error uploading SoW document PDF to opportunity. SoW document ID: {sow_document_id}, "
            f"Provider ID: {provider_id}. Error: {err}"
        )
        return
    logger.info(
        f"Successfully uploaded SoW document to CW opportunity. "
        f"SoW document ID: {sow_document_id}, Provider ID: {provider_id}"
    )
    if email_sow_to_account_manager:
        cw_customer: Dict[str, Any] = erp_client.get_customer(
            sow_document.customer.crm_id
        )
        sow_document_details: Dict[str, Union[str, int]] = dict(
            name=sow_document.name, id=sow_document.id
        )
        territory_manager: Dict[str, Any] = erp_client.get_member(
            cw_customer.get("territory_manager_id")
        )
        user: User = User.objects.get(id=user_id)
        user_details: Dict[str, str] = dict(name=user.name, email=user.email)
        email_sow_document_to_account_manager.apply_async(
            (
                cw_customer,
                sow_document_details,
                sow_document_pdf,
                quote,
                territory_manager,
                user_details,
                provider_id,
            ),
            queue="high",
        )
    return


@shared_task
def upload_sow_service_detail_document_pdf_to_opportunity(
    provider_id: int, sow_document_id: int
) -> None:
    """
    Task to uplaod SoW service detail document PDF to CW opportunity.

    Args:
        provider_id: Provider ID
        sow_document_id: SoWDocument ID
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    erp_client = provider.erp_client
    sow_document: SOWDocument = DocumentService.get_document(
        provider_id, sow_document_id
    )

    quote_id: int = sow_document.quote_id
    quote: Dict[str, Any] = dict()
    try:
        quote: Dict = QuoteService.get_quote(provider_id, quote_id)
    except APIError as err:
        logger.info(
            f"CW opportunity with quote_id: {quote_id} not found for SoW document ID: {sow_document_id}, "
            f"Provider ID: {provider_id}. Error: {err}"
        )
        return
    service_detail_document: Dict[
        str, str
    ] = DocumentService.download_service_detail_document_pdf(
        sow_document, quote
    )

    existing_quote_attachments: List[Dict] = erp_client.get_quote_documents(
        quote_id
    )
    for attachment in existing_quote_attachments:
        attachment_name: str = attachment.get("server_file_name").lower()
        # The sow service detail document file name startswith prefix 'Service Detail -'
        sow_service_detail_prefix: str = (
            f"{DocumentService.SERVICE_DETAIL.lower()}"
        )
        if attachment_name.startswith(sow_service_detail_prefix):
            erp_client.get_delete_documents(attachment.get("id"))

    file_path: str = service_detail_document["file_path"]
    if not settings.DEBUG:
        file_path: str = utils.download_file(file_path)

    try:
        with open(file_path, "rb") as file:
            content_file = ContentFile(file.read())
            content_file.name = service_detail_document["file_name"]
            payload: Dict = dict(
                file=content_file,
                record_type="Opportunity",
                record_id=quote_id,
            )
            erp_client.upload_system_attachments(payload)
    except OSError as err:
        logger.error(
            f"OS error reading SoW service detail PDF. SoW document ID: {sow_document.id}, Provider ID: {provider_id}, "
            f"Error: {err}"
        )
        return
    except APIError as err:
        logger.error(
            f"CW API error uploading SoW service detail document PDF to opportunity. SoW document ID: {sow_document_id}, "
            f"Provider ID: {provider_id}. Error: {err}"
        )
        return
    logger.info(
        f"Successfully uploaded SoW service detail document to CW opportunity. "
        f"SoW document ID: {sow_document_id}, Provider ID: {provider_id}"
    )
    return


@shared_task
def sow_document_post_create_update_processing(
    provider_id: int,
    user_id: str,
    sow_document_id: int,
    email_sow_to_account_manager: bool = False,
) -> None:
    """
    SoW document postprocessing after create or update involves:
        Uploading SoW Document to CW opportunity and email to account manager if needed.
        Uploading SoW service detail document PDF to CW opportunity

    Args:
        provider_id: Provider ID
        user_id: User ID
        sow_document_id: SoWDocument ID
        email_sow_to_account_manager: True to send SoW PDF to account manager else False
    """
    upload_sow_document_pdf_to_opportunity_and_email_account_manager.apply_async(
        (provider_id, user_id, sow_document_id, email_sow_to_account_manager),
        queue="high",
    )
    upload_sow_service_detail_document_pdf_to_opportunity.apply_async(
        (provider_id, sow_document_id), queue="high"
    )
