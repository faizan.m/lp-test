from collections import defaultdict
from typing import Generator, List, Optional, Dict

from celery import chain, shared_task
from celery.utils.log import get_task_logger
from requests import Response

from accounts.models import Provider, Customer
from core.exceptions import ERPAuthenticationNotConfigured
from core.utils import chunks_of_list_with_max_item_count
from document.models import (
    SalesPurchaseOrderData,
    ProductsItemData,
    AutomatedWeeklyRecapSetting,
)
from document.services.customer_order_tracking import (
    ConnectwiseSalesOrderPayloadHandler,
    SalesOrderService,
    ConnectwiseProductItemsPayloadHandler,
)
from document.tasks.operations_dashboard import sync_finance_report_data
from utils import DbOperation
from document.services.order_tracking import WeeklyRecapService

logger = get_task_logger(__name__)


@shared_task
def sync_sales_order_data(provider_ids=None):
    """
    Fetch all the sales orders for the provider from connectwise
    and sync in the local database.
    The sales orders are either created, updated or deleted based on
    the available orders in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing sales order data for {provider}")
        try:
            sales_order_service: SalesOrderService = SalesOrderService(
                provider
            )
            sales_orders_generator_response: Generator[
                Response
            ] = sales_order_service.get_provider_sales_orders_from_erp()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        existing_sales_orders: List[
            int
        ] = SalesPurchaseOrderData.objects.filter(
            provider=provider
        ).values_list(
            "crm_id", flat=True
        )
        logger.debug(
            f"Found {len(existing_sales_orders)} existing sales orders in local DB."
        )
        sales_orders_present_in_erp = set()
        for response in sales_orders_generator_response:
            sales_orders = response.json()
            logger.debug(f"Found {len(sales_orders)} in current page.")
            for sales_order in sales_orders:
                result["found"] += 1
                sales_order_id = sales_order["id"]
                if sales_order_id in existing_sales_orders:
                    res, errors = ConnectwiseSalesOrderPayloadHandler(
                        provider, DbOperation.UPDATE, sales_order
                    ).process()
                    if res:
                        logger.debug(f"Updated Sales Orders: {sales_order_id}")
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for Sales Order: {sales_order_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[sales_order_id].append(errors)
                else:
                    res, errors = ConnectwiseSalesOrderPayloadHandler(
                        provider, DbOperation.CREATE, sales_order
                    ).process()
                    if res:
                        logger.debug(f"Created Sales Orders: {sales_order_id}")
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Create failed for Sales Order: {sales_order_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[sales_order_id].append(errors)
                sales_orders_present_in_erp.add(sales_order_id)

        deleted_sales_orders = (
            set(existing_sales_orders) - sales_orders_present_in_erp
        )
        logger.info(f"Found {len(deleted_sales_orders)} to be deleted.")
        if deleted_sales_orders:
            res, errors = ConnectwiseSalesOrderPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload={"crm_id": deleted_sales_orders},
            ).process()
            if res:
                result["deleted"] += len(deleted_sales_orders)
                logger.info(
                    f"Deleted {len(deleted_sales_orders)} Sales Orders."
                )
            else:
                result["delete_failed"] += len(deleted_sales_orders)
                logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Sales orders sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
    return provider_results


@shared_task
def sync_products_item_data(provider_ids=None):
    """
    Fetch the product items for all the sales orders for the provider from connectwise
    and sync in the local database.
    The product items are either created, updated or deleted based on
    the available orders in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    SALES_ORDER_CHUNK_SIZE = 100
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing sales order data for {provider}")
        try:
            sales_order_service: SalesOrderService = SalesOrderService(
                provider
            )
            sales_order_crm_ids = SalesPurchaseOrderData.objects.filter(
                provider=provider
            ).values_list("crm_id", flat=True)
            if not sales_order_crm_ids:
                continue
            sales_orders_products_pages = []
            sales_order_chunks = chunks_of_list_with_max_item_count(
                sales_order_crm_ids, SALES_ORDER_CHUNK_SIZE
            )
            for sales_order_chunk in sales_order_chunks:
                sales_orders_products_responses: Generator[
                    Response
                ] = sales_order_service.get_products_for_sales_orders_from_erp(
                    sales_order_chunk
                )

                sales_orders_products_pages.extend(
                    sales_orders_products_responses
                )
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue

        existing_products: List[int] = ProductsItemData.objects.filter(
            provider=provider
        ).values_list("crm_id", flat=True)
        logger.debug(
            f"Found {len(existing_products)} existing product in local DB."
        )
        product_items_present_in_erp = set()
        for response in sales_orders_products_pages:
            product_items = response.json()
            logger.debug(f"Found {len(product_items)} in current page.")
            for product_item in product_items:
                result["found"] += 1
                product_item_id = product_item["id"]
                if product_item_id in existing_products:
                    res, errors = ConnectwiseProductItemsPayloadHandler(
                        provider, DbOperation.UPDATE, product_item
                    ).process()
                    if res:
                        logger.debug(
                            f"Updated Product Item: {product_item_id}"
                        )
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for Product Item: {product_item_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[product_item_id].append(errors)
                else:
                    res, errors = ConnectwiseProductItemsPayloadHandler(
                        provider, DbOperation.CREATE, product_item
                    ).process()
                    if res:
                        logger.debug(
                            f"Created Product Item: {product_item_id}"
                        )
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Create failed for Product Item: {product_item_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[product_item_id].append(errors)
                product_items_present_in_erp.add(product_item_id)

        deleted_product_items = (
            set(existing_products) - product_items_present_in_erp
        )
        logger.info(f"Found {len(deleted_product_items)} to be deleted.")
        if deleted_product_items:
            res, errors = ConnectwiseProductItemsPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload={"crm_ids_to_delete": deleted_product_items},
            ).process()
            if res:
                result["deleted"] += len(deleted_product_items)
                logger.info(
                    f"Deleted {len(deleted_product_items)} Product Items."
                )
            else:
                result["delete_failed"] += len(deleted_product_items)
                logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Product Items sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
    return provider_results


@shared_task
def sync_sales_order_tracking_data(provider_ids=None):
    """
    Trigger task workflow to sync data for
    sales orders and product items.
    It runs 2 tasks:
    1. sync_sales_order_data
    2. sync_products_item_data

    sync_sales_order_data run first and finish it's execution
    and then sync_products_item_data will run after it.

    """
    tasks = [
        sync_sales_order_data.si(provider_ids),
        sync_products_item_data.si(provider_ids),
        sync_finance_report_data.si(provider_ids),
    ]
    chain(*tasks).apply_async()


@shared_task
def send_automated_order_tracking_weekly_recap_emails(
    provider_ids: Optional[List[int]] = None,
) -> None:
    """
    Send automated order tracking weekly recap emails for customers of providers.

    Args:
        provider_ids: List of provider IDs.

    Returns:
        Task result
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in providers:
        if not AutomatedWeeklyRecapSetting.is_configured_for_provider(
            provider
        ):
            logger.info(
                f"Weekly Recap Setting not configured for the provider: {provider.name}! "
                f"Weekly Recap emails will not be sent."
            )
            continue
        else:
            weekly_recap_setting: AutomatedWeeklyRecapSetting = (
                provider.weekly_recap_setting
            )
            if weekly_recap_setting.is_scheduled_for_today():
                customers: "QuerySet[Customer]" = Customer.objects.filter(
                    provider_id=provider.id, is_active=True, is_deleted=False
                )
                weekly_recap_service: WeeklyRecapService = WeeklyRecapService(
                    provider, weekly_recap_setting
                )
                for customer in customers:
                    (
                        email_sent,
                        error,
                    ) = weekly_recap_service.send_order_tracking_weekly_recap_email(
                        customer
                    )
                    if email_sent:
                        logger.info(
                            f"Order tracking weekly recap email sent to "
                            f"customer: {customer.name}"
                        )
                    else:
                        logger.info(
                            f"Order tracking weekly recap email not sent to "
                            f"customer: {customer.name}. Reason: {error}"
                        )
                logger.info(
                    f"Successfully sent order tracking weekly recap emails for "
                    f"customers of provider: {provider.name}."
                )
    return


@shared_task
def send_order_tracking_weekly_recap_email(
    provider_id: int,
    all_customers: bool,
    customer_crm_ids: List[int],
) -> Dict:
    """
    Send order tracking weekly email for customers of a provider.
    Returns details about customers for which email could not be sent.

    Args:
        provider_id: Provider ID
        all_customers: True if trigger for all customers else False
        customer_crm_ids: Customer CRM IDs

    Returns:
        Task result.
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    if not AutomatedWeeklyRecapSetting.is_configured_for_provider(provider):
        reason: str = (
            f"Order tracking weekly recap setting not configured for the Provider: {provider.name}! "
            f"Weekly Recap emails will not be sent."
        )
        logger.info(reason)
        return dict(operation_status="FAILED", reason=reason, payload={})
    else:
        skipped_customers: List[Dict] = list()
        customers: "QuerySet[Customer]" = (
            Customer.objects.filter(
                provider_id=provider.id, is_active=True, is_deleted=False
            )
            if all_customers
            else Customer.objects.filter(
                provider_id=provider.id, crm_id__in=customer_crm_ids
            )
        )
        weekly_recap_setting: AutomatedWeeklyRecapSetting = (
            provider.weekly_recap_setting
        )
        weekly_recap_service: WeeklyRecapService = WeeklyRecapService(
            provider, weekly_recap_setting
        )
        for customer in customers:
            (
                email_sent,
                reason,
            ) = weekly_recap_service.send_order_tracking_weekly_recap_email(
                customer
            )
            if email_sent:
                logger.info(
                    f"Order tracking weekly recap email sent for "
                    f"customer: {customer.name}"
                )
            else:
                logger.info(
                    f"Order tracking weekly recap email could not be sent for "
                    f"customer: {customer.name}. Reason: {reason}"
                )
                skipped_customers.append(
                    dict(
                        customer_name=customer.name,
                        customer_crm_id=customer.crm_id,
                        reason=reason,
                    )
                )
        logger.info(
            f"Successfully sent order tracking weekly recap emails for "
            f"customers of provider: {provider.name}, ID: {provider.id}"
        )
        return dict(
            operation_status="SUCCESS",
            reason="",
            payload=dict(skipped_customer_details=skipped_customers),
        )
