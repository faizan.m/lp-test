from document.models import (
    SOWVendorOnboardingSetting,
    SOWVendorOnboardingAttachments,
)
from typing import List, Dict, Optional, Union, Tuple, Any
from accounts.models import Provider
from document.emails import SoWVendorOnboardingEmail
from celery import shared_task
from utils import get_app_logger
from django.conf import settings

logger = get_app_logger(__name__)


@shared_task
def send_onboarding_email_to_vendor_primary_contact(
    provider_id: int, email: str
) -> None:
    """
    Send onboarding email to vendor primary contact. The email body, email subject and attachments are configured in
    SoW vendor onboarding settings.

    Args:
        provider_id: Provider ID
        email: Contact email

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    try:
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            provider.sow_vendor_onboarding_setting
        )
    except SOWVendorOnboardingSetting.DoesNotExist:
        logger.error(
            f"SoW vendor onboarding setting not configured for the provider: {provider.name}, ID: "
            f"{provider_id}. Skip sending sow vendor onboarding email!"
        )
        return
    email_context: Dict[str, Any] = dict(
        email_body=sow_vendor_onboarding_setting.vendor_onboarding_email_body,
        email_subject=sow_vendor_onboarding_setting.vendor_onboarding_email_subject,
        provider=provider,
    )

    try:
        onboarding_attachments: Optional[
            "QuerySet[SOWVendorOnboardingAttachments]"
        ] = sow_vendor_onboarding_setting.onboarding_attachments.all()
    except SOWVendorOnboardingAttachments.DoesNotExist:
        onboarding_attachments = None

    files: List[Tuple[str, str]] = list()
    remote_attachments: List[Tuple[str, str]] = list()
    attachments: List[Tuple[str, str]] = [
        (
            attachment.name,
            attachment.attachment.path
            if settings.DEBUG
            else attachment.attachment.url,
        )
        for attachment in onboarding_attachments
    ]
    if settings.DEBUG:
        files = attachments
    else:
        remote_attachments = attachments

    email_alias: str = (
        sow_vendor_onboarding_setting.vendor_onboarding_email_alias
    )
    SoWVendorOnboardingEmail(
        remote_attachments=remote_attachments,
        context=email_context,
        files=files,
        from_email=email_alias,
    ).send(
        to=[email],
    )
    logger.info(
        f"SoW vendor onboarding email sent for provider: {provider.name} to email ID: {email}"
    )
