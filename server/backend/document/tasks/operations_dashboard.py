from collections import defaultdict
from typing import Dict, Generator, List, Optional, Set

from celery import chain, group, shared_task
from celery.utils.log import get_task_logger
from django.forms import model_to_dict
from requests import Response

from accounts.models import Provider
from core.exceptions import ERPAuthenticationNotConfigured
from core.integrations.erp.connectwise.field_mappings import InvoiceById
from core.integrations.utils import prepare_response
from core.utils import chunks_of_list_with_max_item_count
from document.models import (
    ConnectwiseServiceTicket,
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrder,
    ConnectwiseServiceTicketParseFailed,
    IngramPurchaseOrderData,
    ConnectwisePurchaseOrderLineitemsData,
    OperationDashboardSettings,
    FinanceData,
    ProductsItemData,
    InvoiceData,
    ConnectwiseOpportunityData,
)
from document.services.customer_order_tracking import FinanceReportService
from document.services.operations_dashboard import (
    ConnectwiseServiceTicketPayloadHandler,
    ConnectwisePurchaseOrdersDataPayloadHandler,
    OperationsDashboardQueryService,
    OperationsDashboardService,
    IngramPurchaseOrderHandler,
    ConnectwisePurchaseOrdersLineItemsDataPayloadHandler,
    ConnectwiseOpportunityDataPayloadHandler,
)
from exceptions.exceptions import APIError
from utils import DbOperation
from django.utils import timezone
import asyncio

logger = get_task_logger(__name__)


def log_parse_errors(
    provider: Provider, tickets_with_parse_errors: List[Dict]
) -> None:
    """
    Logs tickets data to database which had parsing errors
    :param provider: Provider
    :param tickets_with_parse_errors: Ticket objects
    """
    ConnectwiseServiceTicketParseFailed.objects.filter(
        provider=provider
    ).delete()
    bulk_create_objects = []
    for ticket in tickets_with_parse_errors:
        obj_ = ConnectwiseServiceTicketParseFailed(
            provider=provider,
            ticket_crm_id=ticket.get("id"),
            summary=ticket.get("summary"),
        )
        bulk_create_objects.append(obj_)
    ConnectwiseServiceTicketParseFailed.objects.bulk_create(
        bulk_create_objects
    )


@shared_task
def sync_service_tickets(provider_ids=None):
    """
    Fetch all the tickets for the provider from connectwise
    and sync in the local database.
    The ticket are either created, updated or deleted based on
    the available ticket in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        try:
            settings_configured, message = OperationsDashboardQueryService(
                provider
            ).check_operations_dashboard_settings()
        except ERPAuthenticationNotConfigured as e:
            logger.info(f"Provider: {provider}, {e}")
            continue
        if not settings_configured:
            logger.info(
                f"Operations Dashboard settings not configured for provider {provider}. Message: {message}. "
                f"Task skipped."
            )
            continue
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing Service Tickets for {provider}")

        dashboard_service: OperationsDashboardService = (
            OperationsDashboardService(provider)
        )
        ticket_pages: Generator[
            Response
        ] = dashboard_service.get_provider_service_tickets_from_erp()
        existing_tickets: List[int] = ConnectwiseServiceTicket.objects.filter(
            provider=provider
        ).values_list("ticket_crm_id", flat=True)
        logger.debug(
            f"Found {len(existing_tickets)} existing tickets in local DB."
        )

        tickets_present_in_erp = set()
        tickets_with_parse_errors = []
        for response in ticket_pages:
            tickets = response.json()
            logger.debug(f"Found {len(tickets)} in current page.")
            ticket_ids_list = [ticket["id"] for ticket in tickets]
            ticket_notes = dashboard_service.fetch_ticket_notes(
                ticket_ids_list
            )
            for ticket in tickets:
                result["found"] += 1
                ticket_id = ticket["id"]
                ticket.update(ticket_note=ticket_notes.get(ticket_id, ""))
                if ticket_id in existing_tickets:
                    res, errors = ConnectwiseServiceTicketPayloadHandler(
                        provider, DbOperation.UPDATE, ticket
                    ).process()
                    if res:
                        logger.debug(f"Updated ticket: {ticket_id}")
                        result["updated"] += 1
                    else:
                        if errors.get("po_numbers"):
                            tickets_with_parse_errors.append(ticket)
                        logger.debug(f"Updated failed for ticket: {ticket_id}")
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[ticket_id].append(errors)
                else:
                    res, errors = ConnectwiseServiceTicketPayloadHandler(
                        provider, DbOperation.CREATE, ticket
                    ).process()
                    if res:
                        logger.debug(f"Created ticket: {ticket_id}")
                        result["created"] += 1
                    else:
                        if errors.get("po_numbers"):
                            tickets_with_parse_errors.append(ticket)
                        logger.debug(f"Create failed for ticket: {ticket_id}")
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[ticket_id].append(errors)
                tickets_present_in_erp.add(ticket_id)

        deleted_tickets = set(existing_tickets) - tickets_present_in_erp
        logger.info(f"Found {len(deleted_tickets)} to be deleted.")
        if deleted_tickets:
            res, errors = ConnectwiseServiceTicketPayloadHandler(
                provider, DbOperation.DELETE, payload={"id": deleted_tickets}
            ).process()
            if res:
                result["deleted"] += len(deleted_tickets)
                logger.info(f"Deleted {len(deleted_tickets)} tickets.")
            else:
                result["delete_failed"] += len(deleted_tickets)
                logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Service Ticket sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
        log_parse_errors(provider, tickets_with_parse_errors)
    return provider_results


@shared_task
def sync_purchase_order_data(provider_ids=None):
    """
    Fetch all the tickets for the provider from connectwise
    and sync in the local database.
    The ticket are either created, updated or deleted based on
    the available ticket in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    po_chunk_size = 50
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing purchase order data for {provider}")
        dashboard_service: OperationsDashboardService = (
            OperationsDashboardService(provider)
        )
        po_numbers_list = ConnectwisePurchaseOrder.objects.filter(
            ticket__provider=provider
        ).values_list("po_number", flat=True)
        if not po_numbers_list:
            continue

        params = {}
        try:
            po_custom_field_id = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).po_custom_field_id
        except OperationDashboardSettings.DoesNotExist:
            po_custom_field_id = None
        params.update({"po_custom_field_setting": po_custom_field_id})
        if not po_custom_field_id:
            logger.info(
                f"PO custom field settings is not configured for the {provider=}"
            )

        purchase_orders_pages = []
        po_number_chunks = chunks_of_list_with_max_item_count(
            po_numbers_list, po_chunk_size
        )

        for po_number_chunk in po_number_chunks:
            purchase_orders_responses: Generator[
                Response
            ] = dashboard_service.get_provider_purchase_orders_from_erp(
                po_number_chunk
            )

            purchase_orders_pages.extend(purchase_orders_responses)

        existing_purchase_order_data: List[
            int
        ] = ConnectwisePurchaseOrderData.objects.filter(
            provider=provider
        ).values_list(
            "crm_id", flat=True
        )
        logger.debug(
            f"Found {len(existing_purchase_order_data)} existing purchase order data in local DB."
        )
        purchase_orders_present_in_erp = set()
        for response in purchase_orders_pages:
            purchase_order_data = response.json()
            logger.debug(f"Found {len(purchase_order_data)} in current page.")
            for purchase_order in purchase_order_data:
                result["found"] += 1
                purchase_order_id = purchase_order["id"]
                if purchase_order_id in existing_purchase_order_data:
                    res, errors = ConnectwisePurchaseOrdersDataPayloadHandler(
                        provider, DbOperation.UPDATE, purchase_order
                    ).process(**params)
                    if res:
                        logger.debug(
                            f"Updated purchase order: {purchase_order_id}"
                        )
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for purchase order: {purchase_order_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[purchase_order_id].append(errors)
                else:
                    res, errors = ConnectwisePurchaseOrdersDataPayloadHandler(
                        provider, DbOperation.CREATE, purchase_order
                    ).process(**params)
                    if res:
                        logger.debug(
                            f"Created purchase order: {purchase_order_id}"
                        )
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Create failed for purchase order: {purchase_order_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[purchase_order_id].append(errors)
                purchase_orders_present_in_erp.add(purchase_order_id)

        deleted_purchase_order = (
            set(existing_purchase_order_data) - purchase_orders_present_in_erp
        )
        logger.info(f"Found {len(deleted_purchase_order)} to be deleted.")
        if deleted_purchase_order:
            res, errors = ConnectwisePurchaseOrdersDataPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload={"crm_ids": deleted_purchase_order},
            ).process(**params)
            if res:
                result["deleted"] += len(deleted_purchase_order)
                logger.info(f"Deleted {len(deleted_purchase_order)} tickets.")
            else:
                result["delete_failed"] += len(deleted_purchase_order)
                logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Purchase order data sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
    return provider_results


@shared_task
def sync_cw_purchase_order_line_item_data(provider_ids=None):
    """
    Fetch all the lines items of each PO for the provider from connectwise
    and sync in the local database.
    The line item data are either created, updated or deleted based on
    the available line item data in the database.
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing purchase order line items data for {provider}")
        dashboard_service: OperationsDashboardService = (
            OperationsDashboardService(provider)
        )
        purchase_order_ids = ConnectwisePurchaseOrderData.objects.filter(
            provider=provider
        ).values_list("crm_id", flat=True)
        logger.info(
            f"Syncing purchase order line items data for Purchase Order Ids {purchase_order_ids}"
        )
        if not purchase_order_ids:
            continue

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            dashboard_service.get_purchase_orders_line_items_from_erp(
                purchase_order_ids
            )
        )
        line_items = loop.run_until_complete(future)

        existing_po_line_items_data: List[
            int
        ] = ConnectwisePurchaseOrderLineitemsData.objects.filter(
            provider=provider
        ).values_list(
            "crm_id", flat=True
        )
        logger.debug(
            f"Found {len(existing_po_line_items_data)} existing purchase order line item data in local DB."
        )
        purchase_orders_present_in_erp = set()
        for line_item_data in line_items:
            logger.debug(f"Found {len(line_item_data)} in current page.")
            for line_item in line_item_data:
                result["found"] += 1
                line_item_id = line_item.get("id")
                if line_item_id in existing_po_line_items_data:
                    (
                        res,
                        errors,
                    ) = ConnectwisePurchaseOrdersLineItemsDataPayloadHandler(
                        provider, DbOperation.UPDATE, line_item
                    ).process()
                    if res:
                        logger.debug(
                            f"Updated purchase order Line Item for: {line_item_id}"
                        )
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for purchase order Line Item for: {line_item_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["update_failed"] += 1
                        update_errors[line_item_id].append(errors)
                else:
                    (
                        res,
                        errors,
                    ) = ConnectwisePurchaseOrdersLineItemsDataPayloadHandler(
                        provider, DbOperation.CREATE, line_item
                    ).process()
                    if res:
                        logger.debug(
                            f"Created purchase order Line Item for: {line_item_id}"
                        )
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Create failed for purchase order Line Item for: {line_item_id}"
                        )
                        logger.debug(f"Errors: {errors}")
                        result["create_failed"] += 1
                        create_errors[line_item_id].append(errors)
                purchase_orders_present_in_erp.add(line_item_id)

        deleted_purchase_order = (
            set(existing_po_line_items_data) - purchase_orders_present_in_erp
        )
        logger.info(f"Found {len(deleted_purchase_order)} to be deleted.")
        if deleted_purchase_order:
            res, errors = ConnectwisePurchaseOrdersLineItemsDataPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload={"crm_ids": deleted_purchase_order},
            ).process()
            if res:
                result["deleted"] += len(deleted_purchase_order)
                logger.info(f"Deleted {len(deleted_purchase_order)} tickets.")
            else:
                result["delete_failed"] += len(deleted_purchase_order)
                logger.debug("Delete Failed.")

        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Purchase order data sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
    return provider_results


@shared_task
def sync_ingram_purchase_order_data(provider_ids=None):
    """
    Fetch all the Ingram purchase order data by taking POs from IngramPurchaseOrderData
    table for the provider and sync with the local database.
    The Ingram Pos data are either created or updated in IngramPurchaseOrderData table
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results = {}
    if provider_ids:
        providers = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers = Provider.objects.filter(is_active=True)
    for provider in providers:
        result = defaultdict(int)
        update_errors = defaultdict(list)
        create_errors = defaultdict(list)
        logger.info(f"Syncing Ingram purchase order data for {provider}")
        ingram_dashboard_service: IngramPurchaseOrderHandler = (
            IngramPurchaseOrderHandler(provider, action=DbOperation.CREATE)
        )
        po_numbers_list = ConnectwisePurchaseOrder.objects.filter(
            ticket__provider=provider
        ).values_list("po_number", flat=True)
        if not po_numbers_list:
            continue

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            ingram_dashboard_service.get_ingram_purchase_order_from_ingram(
                provider, po_numbers_list
            )
        )
        purchase_order_ingram_data = loop.run_until_complete(future)
        existing_ingram_purchase_order_data: List[
            str
        ] = IngramPurchaseOrderData.objects.filter(
            provider=provider
        ).values_list(
            "ingram_suborder_number", flat=True
        )
        logger.debug(
            f"Found {len(existing_ingram_purchase_order_data)} existing purchase order data in local DB."
        )
        purchase_orders_present_in_erp = set()
        for ingram_purchase_order_data in purchase_order_ingram_data:
            logger.debug(
                f"Found {len(ingram_purchase_order_data)} in current page."
            )
            if ingram_purchase_order_data:
                for ingram_purchase_order_line in ingram_purchase_order_data:
                    for (
                        ingram_purchase_order_line_data
                    ) in ingram_purchase_order_line.get("lines", []):
                        result["found"] += 1
                        ingram_suborder_number = (
                            ingram_purchase_order_line_data.get(
                                "subordernumber"
                            )
                        )
                        if (
                            ingram_suborder_number
                            in existing_ingram_purchase_order_data
                        ):
                            res, errors = IngramPurchaseOrderHandler(
                                provider,
                                DbOperation.UPDATE,
                                ingram_purchase_order_line_data,
                                ingram_purchase_order_line,
                            ).process()
                            if res:
                                logger.debug(
                                    f"Updated purchase order number and suborder number: "
                                    f"{ingram_purchase_order_line.get('customerordernumber'), ingram_suborder_number}"
                                )
                                result["updated"] += 1
                            else:
                                logger.debug(
                                    f"Updated failed for purchase order number and suborder number: "
                                    f"{ingram_purchase_order_line.get('customerordernumber'), ingram_suborder_number}"
                                )
                                logger.debug(f"Errors: {errors}")
                                result["update_failed"] += 1
                                update_errors[ingram_suborder_number].append(
                                    errors
                                )
                        else:
                            res, errors = IngramPurchaseOrderHandler(
                                provider,
                                DbOperation.CREATE,
                                ingram_purchase_order_line_data,
                                ingram_purchase_order_line,
                            ).process()
                            if res:
                                logger.debug(
                                    f"Created purchase for order number and suborder number: "
                                    f"{ingram_purchase_order_line.get('customerordernumber'), ingram_suborder_number}"
                                )
                                result["created"] += 1
                            else:
                                logger.debug(
                                    f"Create failed for order number and suborder number: "
                                    f"{ingram_purchase_order_line.get('customerordernumber'), ingram_suborder_number}"
                                )
                                logger.debug(f"Errors: {errors}")
                                result["create_failed"] += 1
                                create_errors[ingram_suborder_number].append(
                                    errors
                                )
                        purchase_orders_present_in_erp.add(
                            ingram_suborder_number
                        )
        result.update(
            dict(update_errors=update_errors, create_errors=create_errors)
        )
        logger.info(
            f"Purchase order data sync finished for {provider}. Result: {result}"
        )
        provider_results[provider.id] = result
    return provider_results


@shared_task
def sync_finance_report_data(provider_ids: Optional[List[int]] = None):
    """
    Sync finance report data from cw purchase order line item data
    and sale product item data

    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    provider_results: Dict = {}
    if provider_ids:
        providers: "QuerySet[Provider]" = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers: "QuerySet[Provider]" = Provider.objects.filter(
            is_active=True
        )
    for provider in providers:
        logger.info(f"Syncing finance data for {provider}")
        cw_line_items_data: "QuerySet[ConnectwisePurchaseOrderLineitemsData]" = (
            ConnectwisePurchaseOrderLineitemsData.objects.select_related(
                "po_number",
                "po_number__details",
                "po_number__details__sales_order",
            )
            .filter(provider=provider)
            .only(
                "received_status",
                "po_number",
                "po_number__details__purchase_order_status_name",
                "product_id",
                "description",
                "received_quantity",
                "unit_cost",
                "ordered_quantity",
                "serial_numbers",
                "shipping_method",
                "tracking_numbers",
                "expected_ship_date",
                "is_closed",
                "is_cancelled",
            )
        ).values(
            "po_number",
            "po_number__details__purchase_order_status_name",
            "po_number__details__vendor_company_name",
            "po_number__details__customer_company_name",
            "po_number__details__sales_order",
            "po_number__details__crm_id",
            "po_number__details__internal_notes",
            "po_number__details__po_date",
            "po_number__details__location_id",
            "po_number__details__business_unit",
            "po_number__details__shipment_date",
            "po_number__details__total",
            "po_number__details__entered_by",
            "po_number__details__vendor_invoice_number",
            "po_number__details__shipping_instruction",
            "po_number__details__sales_tax",
            "po_number__details__sales_order__id",
            "po_number__details__sales_order__opportunity_name",
            "po_number__details__sales_order__crm_id",
            "po_number__details__sales_order__order_date",
            "po_number__details__sales_order__customer_po_number",
            "po_number__details__sales_order__so_linked_pos",
            "po_number__details__sales_order__due_date",
            "po_number__details__sales_order__total",
            "po_number__details__sales_order__sub_total",
            "po_number__details__sales_order__tax_total",
            "po_number__details__sales_order__location",
            "po_number__details__sales_order__invoice_ids",
            "po_number__details__sales_order__opportunity_crm_id",
            "po_number__details__sales_order__opportunity_won_date",
            "po_number__details__sales_order__status_name",
            "vendor_order_number",
            "product_id",
            "description",
            "received_quantity",
            "received_status",
            "unit_cost",
            "ordered_quantity",
            "serial_numbers",
            "shipping_method",
            "tracking_numbers",
            "expected_ship_date",
            "is_closed",
            "is_cancelled",
            "crm_id",
            "line_number",
            "internal_notes",
            "received_date",
            "unit_of_measure",
            "warehouse_bin_name",
        )
        finance_data_to_create: List[FinanceData] = []
        for line_item_data in cw_line_items_data:
            product_items: "QuerySet[ProductsItemData]" = (
                ProductsItemData.objects.filter(
                    provider_id=provider.id,
                    product_id=line_item_data.get("product_id"),
                    sales_order_id=line_item_data.get(
                        "po_number__details__sales_order__id"
                    ),
                )
            )
            if product_items:
                ext_cost_in_receiving: float = line_item_data.get(
                    "ordered_quantity", 0
                ) * line_item_data.get("unit_cost", 0)
                existing_finance_data_item = FinanceData.objects.filter(
                    po_number=line_item_data.get("po_number"),
                    product_id=line_item_data.get("product_id"),
                )

                # Convert all the list into comma separated string
                if line_item_data.get("serial_numbers"):
                    serial_numbers = ", ".join(
                        line_item_data.get("serial_numbers")
                    )
                else:
                    serial_numbers = None

                if line_item_data.get("tracking_numbers"):
                    tracking_numbers = ", ".join(
                        line_item_data.get("tracking_numbers")
                    )
                else:
                    tracking_numbers = None

                if line_item_data.get(
                    "po_number__details__sales_order__so_linked_pos"
                ):
                    so_linked_pos = ", ".join(
                        line_item_data.get(
                            "po_number__details__sales_order__so_linked_pos"
                        )
                    )
                else:
                    so_linked_pos = None

                # get Invoice numbers from ids
                invoice_crm_ids: List[int] = line_item_data.get(
                    "po_number__details__sales_order__invoice_ids"
                )
                invoice_numbers: List[str] = []
                if invoice_crm_ids:
                    invoices_data: List[
                        Dict
                    ] = provider.erp_client.get_invoices_by_crm_ids(
                        invoice_crm_ids
                    )
                    for invoice_data in invoices_data:
                        _invoice_number: str = invoice_data.get(
                            "invoice_number"
                        )
                        invoice_numbers.append(_invoice_number)
                line_item_invoice_number: Optional[str] = product_items[
                    0
                ].invoice_number

                product_quantity: Optional[int] = product_items[0].quantity
                unit_cost_in_so: Optional[float] = product_items[0].cost
                unit_price: Optional[float] = product_items[0].price
                ext_cost_in_so: Optional[float] = (
                    unit_cost_in_so * product_quantity
                    if (unit_cost_in_so and product_quantity)
                    else None
                )
                ext_price: Optional[float] = (
                    unit_price * product_quantity
                    if (unit_price and product_quantity)
                    else None
                )
                obj: FinanceData = FinanceData(
                    provider=provider,
                    received_status=line_item_data.get("received_status"),
                    po_number=line_item_data.get("po_number"),
                    purchase_order_status=line_item_data.get(
                        "po_number__details__purchase_order_status_name"
                    ),
                    product_id=line_item_data.get("product_id"),
                    description=line_item_data.get("description"),
                    received_quantity=line_item_data.get("received_quantity"),
                    unit_cost_in_receiving=line_item_data.get("unit_cost"),
                    vendor_company_name=line_item_data.get(
                        "po_number__details__vendor_company_name"
                    ),
                    vendor_order_number=line_item_data.get(
                        "vendor_order_number"
                    ),
                    customer_company_name=line_item_data.get(
                        "po_number__details__customer_company_name"
                    ),
                    ordered_quantity=line_item_data.get("ordered_quantity"),
                    serial_numbers=serial_numbers,
                    shipping_method=line_item_data.get("shipping_method"),
                    tracking_numbers=tracking_numbers,
                    ext_cost_in_receiving=ext_cost_in_receiving,
                    location=line_item_data.get(
                        "po_number__details__sales_order__location"
                    ),
                    expected_ship_date=line_item_data.get(
                        "expected_ship_date"
                    ),
                    unit_cost_in_so=unit_cost_in_so,
                    unit_price=unit_price,
                    ext_cost_in_so=ext_cost_in_so,
                    ext_price=ext_price,
                    opportunity_name=line_item_data.get(
                        "po_number__details__sales_order__opportunity_name"
                    ),
                    sales_order_crm_id=line_item_data.get(
                        "po_number__details__sales_order__crm_id"
                    ),
                    po_date=line_item_data.get("po_number__details__po_date"),
                    so_linked_pos=so_linked_pos,
                    customer_po_number=line_item_data.get(
                        "po_number__details__sales_order__customer_po_number"
                    ),
                    quantity=product_items[0].quantity,
                    is_closed=line_item_data.get("is_closed"),
                    is_cancelled=line_item_data.get("is_cancelled"),
                    purchase_detail_recid=line_item_data.get("crm_id"),
                    line_number=line_item_data.get("line_number"),
                    received_date=line_item_data.get("received_date"),
                    unit_of_measure=line_item_data.get("unit_of_measure"),
                    Purchase_Order_RecID=line_item_data.get(
                        "po_number__details__crm_id"
                    ),
                    internal_notes_po=line_item_data.get(
                        "po_number__details__internal_notes"
                    ),
                    shipment_date=line_item_data.get(
                        "po_number__details__shipment_date"
                    ),
                    total=line_item_data.get("po_number__details__total"),
                    entered_by=line_item_data.get(
                        "po_number__details__entered_by"
                    ),
                    shipping_instruction=line_item_data.get(
                        "po_number__details__shipping_instruction"
                    ),
                    sales_tax=line_item_data.get(
                        "po_number__details__sales_tax"
                    ),
                    order_date=line_item_data.get(
                        "po_number__details__sales_order__order_date"
                    ),
                    due_date=line_item_data.get(
                        "po_number__details__sales_order__due_date"
                    ),
                    total_in_so=line_item_data.get(
                        "po_number__details__sales_order__total"
                    ),
                    sub_total=line_item_data.get(
                        "po_number__details__sales_order__sub_total"
                    ),
                    tax_total=line_item_data.get(
                        "po_number__details__sales_order__tax_total"
                    ),
                    invoice_ids=invoice_crm_ids,
                    invoice_numbers=invoice_numbers,
                    opportunity_crm_id=line_item_data.get(
                        "po_number__details__sales_order__opportunity_crm_id"
                    ),
                    status_name=line_item_data.get(
                        "po_number__details__sales_order__status_name"
                    ),
                    opportunity_won_date=line_item_data.get(
                        "po_number__details__sales_order__opportunity_won_date"
                    ),
                    invoice_number=line_item_invoice_number,
                )
                if existing_finance_data_item.exists():
                    try:
                        obj_dict: Dict = model_to_dict(obj)
                        obj_dict.pop("id")
                        obj_dict["updated_on"] = timezone.now()
                        FinanceData.objects.filter(
                            id=existing_finance_data_item[0].id
                        ).update(**obj_dict)
                        logger.info(
                            f"Finance data updated for {obj_dict.get('product_id')} "
                            f"having PO {obj_dict.get('po_number')}"
                        )
                    except Exception as e:
                        logger.info(
                            f"Finance data update failed for {obj_dict.get('product_id')} "
                            f"having PO {obj_dict.get('po_number')} with exception {e}"
                        )
                else:
                    finance_data_to_create.append(obj)
        if finance_data_to_create:
            # Bulk create finance data
            try:
                FinanceData.objects.bulk_create(finance_data_to_create)
                logger.info("Bulk create success for finance data")
            except Exception as e:
                logger.error(
                    f"Exception while bulk creating the finance data {e}"
                )
        # trigger sync invoice data task
        sync_invoice_data.delay([provider.id])
    return provider_results


@shared_task
def sync_invoice_data(provider_ids: Optional[List[int]] = None):
    """
    Sync Invoice data from invoice crm ids
    Args:
        provider_ids: List of provider Id's. If no provider_ids are provided
        the task runs for all the providers.

    """
    if provider_ids:
        providers: "QuerySet[Provider]" = Provider.objects.filter(
            is_active=True, id__in=provider_ids
        )
    else:
        providers: "QuerySet[Provider]" = Provider.objects.filter(
            is_active=True
        )
    providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in providers:
        finance_report_service: FinanceReportService = FinanceReportService(
            provider
        )
        # Fetch all the invoices from CW only service ticket create date filter is applied
        get_invoices_data_pages: Generator[
            Response
        ] = finance_report_service.get_invoices_data_from_erp()

        for response in get_invoices_data_pages:
            invoice_data_to_create: List = []
            invoices_data = response.json()
            for invoice_data in invoices_data:
                invoice_number = invoice_data.get("invoiceNumber")
                invoice_id = invoice_data.get("id")
                invoice_data = prepare_response(invoice_data, InvoiceById)
                invoice_data_exists = InvoiceData.objects.filter(
                    provider=provider,
                    id=invoice_id,
                )
                try:
                    # get location from id only if it changes in CW
                    if invoice_data_exists:
                        if invoice_data.get(
                            "territory_id"
                        ) != invoice_data_exists[0].data.get("territory_id"):
                            territory_details = (
                                provider.erp_client.get_territory(
                                    invoice_data.get("territory_id")
                                )
                            )
                            invoice_data[
                                "territory_name"
                            ] = territory_details.get("name")
                    else:
                        # get location from id if invoice not found in DB or changed in CW
                        if invoice_data.get("territory_id"):
                            territory_details = (
                                provider.erp_client.get_territory(
                                    invoice_data.get("territory_id")
                                )
                            )
                            invoice_data[
                                "territory_name"
                            ] = territory_details.get("name")
                except APIError:
                    invoice_data["territory_name"] = None

                # fetch payment date from payment details API
                invoice_payment_details = (
                    provider.erp_client.get_invoice_payment_details(invoice_id)
                )
                if invoice_payment_details:
                    invoice_data["payment_date"] = invoice_payment_details[
                        0
                    ].get("payment_date", "")

                obj = InvoiceData(
                    provider=provider,
                    id=invoice_id,
                    number=invoice_number,
                    created_date=invoice_data.get("date"),
                    payment_date=invoice_data.get("payment_date"),
                    data=invoice_data,
                )
                if invoice_data_exists.exists():
                    try:
                        obj_dict: Dict = model_to_dict(obj)
                        obj_dict.pop("id")
                        obj_dict["updated_on"] = timezone.now()
                        InvoiceData.objects.filter(
                            id=invoice_data_exists[0].id
                        ).update(**obj_dict)
                        logger.info(
                            f"Invoice data updated for {obj_dict.get('number')} "
                        )
                    except Exception as e:
                        logger.info(
                            f"Finance data update failed for {obj_dict.get('number')} with exception {e}"
                        )
                else:
                    invoice_data_to_create.append(obj)

            if invoice_data_to_create:
                # Bulk create finance data
                try:
                    InvoiceData.objects.bulk_create(invoice_data_to_create)
                    logger.info("Bulk create success for Invoice data")
                except Exception as e:
                    logger.error(
                        f"Exception while bulk creating the Invoice data {e}"
                    )


@shared_task
def sync_order_tracking_dashboard_data(provider_ids=None):
    """
    Trigger task workflow to sync data for service tickets, purchase orders,
    purchase orders ingram data, purchase orders connectwise data.
    It runs 4 tasks:
    1. sync_service_tickets
    2. sync_purchase_order_data
    3. sync_ingram_purchase_order_data
    4. sync_cw_purchase_order_line_item_data

    The 1st should run and finish it's execution and the other 2 can
    run in parallel after it and the sync_cw_purchase_order_line_item_data will
    run last.

    """
    tasks = [
        sync_service_tickets.si(provider_ids),
        group(
            sync_purchase_order_data.si(provider_ids),
            sync_ingram_purchase_order_data.si(provider_ids),
        ),
        sync_cw_purchase_order_line_item_data.si(provider_ids),
        sync_finance_report_data.si(provider_ids),
    ]
    chain(*tasks).apply_async()


@shared_task
def sync_opportunities(provider_ids: Optional[List[int]] = None) -> None:
    """
    Sync opportunities for provider.

    Note:As no of opps is large, fetch only oppotunities updated after service_ticket_create_date
    which is configured in OperationDashboardSettings

    Args:
        provider_ids: List of provider IDs.

    Returns:
        None
    """
    opp_sync_result: Dict = dict()
    providers: "QuerySet" = (
        Provider.objects.filter(id__in=provider_ids, is_active=True)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    providers: List[Provider] = [
        provider for provider in providers if provider.is_configured
    ]
    for provider in providers:
        logger.info(f"Syncing opportunities for provider: {provider.name}")
        # Fetch only the opportunities created after a date as
        # configured in Operation Dashboard setting.
        settings_configured, message = OperationsDashboardQueryService(
            provider
        ).check_operations_dashboard_settings()
        if not settings_configured:
            logger.info(
                f"Operations Dashboard settings not configured for provider {provider}. Message: {message}. "
                f"Opportunity sync skipped."
            )
            continue
        opportunity_pages: Generator[
            Response, None, None
        ] = OperationsDashboardService(
            provider
        ).get_all_provider_opportunities_from_erp()

        result: Dict = defaultdict(int)
        update_errors: Dict = defaultdict(list)
        create_errors: Dict = defaultdict(list)

        opp_crm_ids_in_db: Set[int] = set(
            ConnectwiseOpportunityData.objects.filter(
                provider_id=provider.id
            ).values_list("crm_id", flat=True)
        )
        opp_crm_ids_in_erp: Set = set()

        logger.info(f"Found {len(opp_crm_ids_in_db)} opportunities in DB.")
        for response in opportunity_pages:
            opportunities: List[Dict] = response.json()
            for opportunity in opportunities:
                result["found_in_erp"] += 1
                opportunity_crm_id: int = opportunity["id"]
                opp_crm_ids_in_erp.add(opportunity_crm_id)

                if opportunity_crm_id in opp_crm_ids_in_db:
                    updated, errors = ConnectwiseOpportunityDataPayloadHandler(
                        provider, DbOperation.UPDATE, opportunity
                    ).process()
                    if updated:
                        logger.debug(
                            f"Updated opportunity with CRM ID: {opportunity_crm_id}"
                        )
                        result["updated"] += 1
                    else:
                        logger.debug(
                            f"Updated failed for opportunity having CRM ID: {opportunity_crm_id}"
                        )
                        result["update_failed"] += 1
                        update_errors[opportunity_crm_id].append(errors)
                else:
                    created, errors = ConnectwiseOpportunityDataPayloadHandler(
                        provider, DbOperation.CREATE, opportunity
                    ).process()
                    if created:
                        logger.debug(
                            f"Created opportunity with CRM ID: {opportunity_crm_id}"
                        )
                        result["created"] += 1
                    else:
                        logger.debug(
                            f"Opportunity creation failed for opportunity CRM ID: {opportunity_crm_id}"
                        )
                        result["create_failed"] += 1
                        create_errors[opportunity_crm_id].append(errors)
        result.update(create_errors=create_errors, update_errors=update_errors)

        opp_crm_ids_to_delete: Set = opp_crm_ids_in_db - opp_crm_ids_in_erp
        if opp_crm_ids_to_delete:
            logger.info(
                f"Found {len(opp_crm_ids_to_delete)} opportunities to delete."
            )
            deleted, errors = ConnectwiseOpportunityDataPayloadHandler(
                provider,
                DbOperation.DELETE,
                payload=dict(crm_ids=list(opp_crm_ids_to_delete)),
            ).process()
            if deleted:
                result["deleted"] += len(opp_crm_ids_to_delete)
                logger.info(f"Successfully deleted opportunities.")
            else:
                result["delete_failed"] += 1
                logger.info("Failed to delete opporunities.")
        opp_sync_result[provider.id] = result
        logger.info(
            f"Finished syncing opportunities for provider: {provider.name}"
        )
    return
