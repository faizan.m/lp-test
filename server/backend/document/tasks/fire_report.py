import asyncio
import json
from typing import List, Any, Dict, Optional, Tuple

from aiohttp import ClientSession
from celery import shared_task
from celery.utils.log import get_task_logger
from django.utils import timezone

from accounts.models import Provider, Customer
from core.integrations.erp.connectwise.connectwise import PatchGroup
from document.emails import FireReportEmail
from document.models import (
    FireReportPartnerSite,
    FireReportSetting,
    PartnerSiteSyncError,
)
from document.services.fire_report import FireReportGenerator
from document.exceptions import PartnerSiteNotFound


logger = get_task_logger(__name__)


CUSTOMER_SITES_ENDPOINT: str = (
    f"https://cw.lookingpoint.com/v4_6_release/apis/3.0/company/companies/"
)


def store_partner_site_sync_error(
    provider_id: int,
    customer_crm_id: int,
    operation: str,
    error_payload: Dict[str, Any],
) -> None:
    """
    Store errors during partner sites sync.

    Args:
        provider_id: Provider ID
        customer_crm_id: Customer CRM ID
        operation: Sync operation
        error_payload: Error payload

    Returns:
        None
    """
    customer: Customer = Customer.objects.get(
        provider_id=provider_id, crm_id=customer_crm_id
    )
    PartnerSiteSyncError.objects.create(
        provider_id=provider_id,
        operation=operation,
        customer_id=customer.id,
        customer_crm_id=customer_crm_id,
        site_crm_id=error_payload.get("site_crm_id", None),
        site_name=error_payload.get("site_name", None),
        site_payload=error_payload.get("site", {}),
        errors=error_payload.get("errors", {}),
    )
    return


def get_erp_client_headers(provider: Provider) -> Dict[str, str]:
    """
    Get headers for CW client.

    Args:
        provider: Provider

    Returns:
        Headers
    """
    erp_client: Connectwise = provider.erp_client
    cw_headers: Dict[str, Any] = erp_client.client.session.headers
    cw_headers.update(
        {
            "clientId": erp_client.client.client_id,
            "Content-Type": "application/json",
        }
    )
    return cw_headers


async def create_customer_site_if_does_not_exist(
    provider: Provider,
    session: ClientSession,
    customer_crm_id: int,
    sites_to_create: List[Dict[str, Any]],
) -> None:
    """
    Create site for customer if a site with same name does not exist.

    Args:
        provider: Provider
        session: Client session
        customer_crm_id: Customer CRM ID
        sites_to_create: Sites create payload

    Returns:
        None
    """
    customer_site_url: str = (
        f"{CUSTOMER_SITES_ENDPOINT}{customer_crm_id}/sites"
    )
    query_params: Dict = dict(
        fields="id,name", orderBy="_info/lastUpdated desc"
    )
    async with session.get(customer_site_url, params=query_params) as response:
        if response.status == 200:
            existing_customer_sites: List[Dict] = await response.json()
            existing_customer_sites: Dict[str, Dict[str, Any]] = {
                site.get("name"): site for site in existing_customer_sites
            }
            for site in sites_to_create:
                # If a site with same name exists, skip creating new site.
                if site["name"] in existing_customer_sites:
                    continue

                data: Dict = dict(
                    name=site.get("name"),
                    company=dict(id=customer_crm_id),
                    phoneNumber=site.get("phone_number"),
                    addressLine1=site.get("address_line_1"),
                    addressLine2=site.get("address_line_2"),
                    city=site.get("city"),
                    stateReference=dict(id=site.get("state_id")),
                    country=dict(id=site.get("country_id")),
                    zip=site.get("zip"),
                )
                async with session.post(
                    customer_site_url, data=json.dumps(data)
                ) as post_response:
                    if post_response.status == 201:
                        await post_response.json()
                        logger.info(
                            f"Successfully created {site['name']} site for customer CRM ID: {customer_crm_id}"
                        )
                    elif 400 <= post_response.status < 500:
                        response_data: Dict[str, Any] = await response.json()
                        error_payload: Dict[str, Any] = dict(
                            site=site,
                            site_crm_id=None,
                            site_name=None,
                            errors=response_data,
                        )
                        store_partner_site_sync_error(
                            provider.id,
                            customer_crm_id,
                            PartnerSiteSyncError.CREATE,
                            error_payload,
                        )
                    else:
                        logger.error(
                            f"{response.status} error creating site: {site['name']} for customer CRM ID: {customer_crm_id}"
                        )


async def async_create_partner_sites_for_customers(
    provider_id: int, customer_crm_ids: List[int], sites: List[Dict[str, str]]
) -> None:
    """
    Create partner sites for all customers.

    Args:
        provider_id: Provider ID
        customer_crm_ids: Customer CRM IDs
        sites: Sites

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    headers: Dict[str, str] = get_erp_client_headers(provider)

    tasks: List = list()
    async with ClientSession(headers=headers) as session:
        for customer_crm_id in customer_crm_ids:
            tasks.append(
                asyncio.ensure_future(
                    create_customer_site_if_does_not_exist(
                        provider, session, customer_crm_id, sites
                    )
                )
            )
        await asyncio.gather(*tasks)
        return


@shared_task(
    autoretry_for=(PartnerSiteNotFound,),
    retry_backoff=True,
    retry_kwargs={"max_retries": 5},
)
def create_partner_site_for_all_customers(
    provider_id: int, partner_site_id: int
):
    """
    Task to create a partner site configured in FIRE report settings for all customers.

    Args:
        provider_id: Provider ID
        partner_site_id: FIRE report partner site ID

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    if provider.is_configured:
        try:
            site: FireReportPartnerSite = FireReportPartnerSite.objects.get(
                id=partner_site_id, provider_id=provider_id
            )
        except FireReportPartnerSite.DoesNotExist:
            raise PartnerSiteNotFound()
        site_payload: Dict[str, Any] = dict(
            name=site.name,
            address_line_1=site.address_line_1,
            address_line_2=site.address_line_2,
            city=site.city,
            state_id=site.state_crm_id,
            country_id=site.country_crm_id,
            zip=site.zip,
            phone_number=site.phone_number,
        )
        customer_crm_ids: List[int] = list(
            Customer.objects.filter(provider_id=provider_id, is_active=True)
            .order_by("crm_id")
            .values_list("crm_id", flat=True)
        )
        asyncio.run(
            async_create_partner_sites_for_customers(
                provider.id, customer_crm_ids, [site_payload]
            )
        )
        logger.info(
            f"Successfully created partner site: {site.name} for customers of "
            f"provider: {provider.name}, ID: {provider.id}"
        )
        return


@shared_task
def create_partner_site_for_new_customer(
    provider_id: int, customer_crm_id: int
) -> None:
    """
    Create partner site for newly created customer.

    Args:
        provider_id: Provider ID
        customer_crm_id: Customer CRM ID

    Returns:
        None
    """
    try:
        customer: Customer = Customer.objects.get(
            crm_id=customer_crm_id, provider_id=provider_id
        )
    except Customer.DoesNotExist:
        logger.error(
            f"Customer with CRM ID {customer_crm_id} does not exist! Cannot create configured partner sites in CW "
            f"for the customer."
        )
        return
    partner_sites: "QuerySet[FireReportPartnerSite]" = (
        customer.provider.fire_report_partner_sites.all()
    )
    if not partner_sites:
        logger.info(
            f"Partner sites not configured for the provider: {customer.provider.name}, ID: {provider_id}. "
            f"Partner sites will not be created for the customer: {customer.name}."
        )
        return
    partner_sites: List[Dict[str, Any]] = [
        dict(
            name=site.name,
            address_line_1=site.address_line_1,
            address_line_2=site.address_line_2,
            city=site.city,
            state_id=site.state_crm_id,
            country_id=site.country_crm_id,
            zip=site.zip,
            phone_number=site.phone_number,
        )
        for site in partner_sites
    ]
    asyncio.run(
        async_create_partner_sites_for_customers(
            provider_id, [customer_crm_id], partner_sites
        )
    )
    logger.info(
        f"Successfully created configured partner sites for the customer: {customer.name}, "
        f"CRM ID: {customer_crm_id}, provider ID: {provider_id}"
    )
    return


@shared_task
def send_fire_report_email(provider_ids: Optional[List[int]] = None) -> None:
    """
    Send FIRE report email.

    Args:
        provider_ids: List of Provider IDs.
    """
    providers: "QuerySet[Provider]" = (
        Provider.objects.filter(is_active=True, id__in=provider_ids)
        if provider_ids
        else Provider.objects.filter(is_active=True)
    )
    for provider in providers:
        if provider.is_configured:
            try:
                fire_report_setting: FireReportSetting = (
                    provider.fire_report_setting
                )
            except FireReportSetting.DoesNotExist:
                logger.info(
                    f"FIRE report setting not configured for Provider: {provider.name}, ID: {provider.id}. "
                    f"Skipped sending FIRE report email."
                )
                continue

            if fire_report_setting.next_run.date() == timezone.now().date():
                # Update next_run
                _next_run = FireReportSetting.get_next_run(
                    fire_report_setting.frequency
                )
                FireReportSetting.objects.filter(
                    id=fire_report_setting.id, provider_id=provider.id
                ).update(next_run=_next_run)

                partner_sites: "Queryset[FireReportPartnerSite]" = (
                    provider.fire_report_partner_sites.all()
                )
                if not partner_sites:
                    logger.info(
                        f"FIRE report partner sites not configured for Provider: {provider.name}, ID: {provider.id}. "
                        f"Skipped sending auto generated FIRE report email."
                    )
                    continue

                filter_params: Dict[str, Any] = dict()
                filter_params.update(
                    po_site_names=list(
                        partner_sites.values_list("name", flat=True)
                    )
                )
                (
                    start_date,
                    end_date,
                ) = fire_report_setting.get_date_range_date_values()
                if start_date:
                    filter_params.update(
                        sales_order_updated_after_date=start_date
                    )
                if end_date:
                    filter_params.update(
                        sales_order_updated_before_date=end_date
                    )

                fire_report_generator: FireReportGenerator = (
                    FireReportGenerator(provider)
                )
                fire_report: Optional[
                    Dict[str, str]
                ] = fire_report_generator.generate_fire_report_csv(
                    filter_params
                )
                if not fire_report:
                    logger.info(
                        "Fire report could not be generated due to absence of data. "
                        "Skip sending fire report mail"
                    )
                    return
                files: List[Tuple[str, str]] = [
                    (
                        fire_report["file_name"],
                        fire_report["file_path"],
                    )
                ]
                FireReportEmail(
                    subject=fire_report_setting.subject,
                    context=dict(
                        provider=provider, subject=fire_report_setting.subject
                    ),
                    files=files,
                ).send(to=fire_report_setting.receiver_email_ids)
                logger.info(
                    f"Successfully sent FIRE report email for provider: {provider.name}, ID: {provider.id} "
                )


async def update_customer_site(
    provider: Provider,
    session: ClientSession,
    customer_crm_id: int,
    current_site_name: str,
    updated_site: Dict[str, Any],
) -> None:
    """
    Update customer site.

    Args:
        provider: Provider
        session: Client Session
        customer_crm_id: Customer CRM ID
        current_site_name: current site name
        updated_site: Updated site details

    Returns:
        None
    """
    customer_site_url: str = (
        f"{CUSTOMER_SITES_ENDPOINT}{customer_crm_id}/sites"
    )
    query_params: Dict[str, str] = dict(
        fields="id,name", orderBy="_info/lastUpdated desc"
    )
    async with session.get(customer_site_url, params=query_params) as response:
        if response.status == 200:
            existing_customer_sites: List[Dict] = await response.json()
            existing_customer_sites: Dict[str, Dict[str, Any]] = {
                site.get("name"): site for site in existing_customer_sites
            }
            if current_site_name not in existing_customer_sites:
                return
            site_crm_id: int = existing_customer_sites[current_site_name].get(
                "id"
            )
            site_url: str = f"{customer_site_url}/{site_crm_id}"

            patch = PatchGroup()
            patch.add("name", updated_site.get("name"))
            patch.add("phoneNumber", updated_site.get("phone_number"))
            patch.add("addressLine1", updated_site.get("address_line_1"))
            patch.add("addressLine2", updated_site.get("address_line_2"))
            patch.add("city", updated_site.get("city"))
            patch.add("stateReference", dict(id=updated_site.get("state_id")))
            patch.add("country", dict(id=updated_site.get("country_id")))
            patch.add("zip", updated_site.get("zip"))

            async with session.patch(
                site_url, data=json.dumps(patch.to_dict())
            ) as patch_reponse:
                if patch_reponse.status == 200:
                    await patch_reponse.json()
                    logger.info(
                        f"Successfully updated site: {current_site_name} for customer CRM ID: {customer_crm_id}"
                    )
                    return
                elif 400 <= patch_reponse.status < 500:
                    response_data: Dict[str, Any] = await response.json()
                    error_payload: Dict[str, Any] = dict(
                        site=updated_site,
                        site_crm_id=site_crm_id,
                        site_name=current_site_name,
                        errors=response_data,
                    )
                    store_partner_site_sync_error(
                        provider.id,
                        customer_crm_id,
                        PartnerSiteSyncError.UPDATE,
                        error_payload,
                    )
                else:
                    logger.error(
                        f"{response.status} error updating site: {current_site_name} for customer "
                        f"CRM ID: {customer_crm_id}"
                    )


async def async_update_customer_site(
    provider_id: int,
    customer_crm_ids: List[int],
    current_site_name: str,
    updated_site: Dict[str, Any],
) -> None:
    """
    Update customer sites.

    Args:
        provider_id: Provider ID
        customer_crm_ids: Customer CRM IDs
        current_site_name: Current site name
        updated_site: Updated site data

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    cw_headers: Dict[str, str] = get_erp_client_headers(provider)
    tasks: List = list()
    async with ClientSession(headers=cw_headers) as session:
        for customer_crm_id in customer_crm_ids:
            tasks.append(
                asyncio.ensure_future(
                    update_customer_site(
                        provider,
                        session,
                        customer_crm_id,
                        current_site_name,
                        updated_site,
                    )
                )
            )
        await asyncio.gather(*tasks)
        return


@shared_task(
    autoretry_for=(PartnerSiteNotFound,),
    retry_backoff=True,
    retry_kwargs={"max_retries": 5},
)
def update_partner_site_for_cw_customers(
    provider_id: int, partner_site_id: int, site_name_before_update: str
) -> None:
    """
    Update partner sites in CW for all customers.

    Args:
        provider_id: Provider ID
        partner_site_id: Partner site ID
        site_name_before_update: Site name before update

    Returns:
        None
    """
    # It is possible that site name gets updated.
    # To identify existing site, site_name_before_update is used
    provider: Provider = Provider.objects.get(id=provider_id)
    if provider.is_configured:
        try:
            partner_site: FireReportPartnerSite = (
                FireReportPartnerSite.objects.get(
                    id=partner_site_id, provider_id=provider_id
                )
            )
        except FireReportPartnerSite.DoesNotExist:
            raise PartnerSiteNotFound()
        updated_site_payload: Dict[str, Any] = dict(
            name=partner_site.name,
            address_line_1=partner_site.address_line_1,
            address_line_2=partner_site.address_line_2,
            city=partner_site.city,
            state_id=partner_site.state_crm_id,
            country_id=partner_site.country_crm_id,
            zip=partner_site.zip,
            phone_number=partner_site.phone_number,
        )
        customer_crm_ids: List[int] = list(
            Customer.objects.filter(provider_id=provider_id, is_active=True)
            .order_by("crm_id")
            .values_list("crm_id", flat=True)
        )
        asyncio.run(
            async_update_customer_site(
                provider_id,
                customer_crm_ids,
                site_name_before_update,
                updated_site_payload,
            )
        )
        logger.info(
            f"Successfully updated site: {site_name_before_update} for all customers of provider: {provider.name}, "
            f"ID: {provider_id}"
        )
        return


async def async_delete_customer_site(
    provider: Provider,
    session: ClientSession,
    customer_crm_id: int,
    site_name: str,
) -> None:
    """
    Delete customer site.

    Args:
        provider: Provider
        session: Client session
        customer_crm_id: Customer CRM ID
        site_name: Site name to delete

    Returns:
        None
    """
    customer_site_url: str = (
        f"{CUSTOMER_SITES_ENDPOINT}{customer_crm_id}/sites"
    )
    query_params: Dict = dict(
        fields="id,name", orderBy="_info/lastUpdated desc"
    )

    async with session.get(customer_site_url, params=query_params) as response:
        if response.status == 200:
            existing_customer_sites: List[Dict] = await response.json()
            existing_customer_sites: Dict[str, Dict] = {
                site.get("name"): site for site in existing_customer_sites
            }
            if site_name in existing_customer_sites:
                site_crm_id: int = existing_customer_sites[site_name].get("id")
                site_url: str = f"{customer_site_url}/{site_crm_id}"
                async with session.delete(site_url) as delete_response:
                    if delete_response.status == 204:
                        logger.info(
                            f"Succesfully deleted site: {site_name} for customer CRM ID: {customer_crm_id}"
                        )
                        return
                    elif 400 <= delete_response.status < 500:
                        response_data: Dict[str, Any] = await response.json()
                        error_payload: Dict[str, Any] = dict(
                            site={},
                            site_crm_id=site_crm_id,
                            site_name=site_name,
                            errors=response_data,
                        )
                        store_partner_site_sync_error(
                            provider.id,
                            customer_crm_id,
                            PartnerSiteSyncError.DELETE,
                            error_payload,
                        )
                    else:
                        logger.error(
                            f"{response.status} error deleting site: {site_name} for customer CRM ID: {customer_crm_id}"
                        )


async def async_delete_customer_sites(
    provider_id: int, customer_crm_ids: List[int], site_name: str
) -> None:
    """
    Delete site for customers.

    Args:
        provider_id: Provider ID
        customer_crm_ids: Customer CRM IDs
        site_name: Site name

    Returns:
        None
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    headers: Dict[str, str] = get_erp_client_headers(provider)
    tasks: List = list()
    async with ClientSession(headers=headers) as session:
        for customer_crm_id in customer_crm_ids:
            tasks.append(
                asyncio.ensure_future(
                    async_delete_customer_site(
                        provider,
                        session,
                        customer_crm_id,
                        site_name,
                    )
                )
            )
        await asyncio.gather(*tasks)
        return


@shared_task
def delete_partner_site_for_cw_customers(
    provider_id: int, site_name: str
) -> None:
    """
    Delete partner sites for all CW customers.

    Args:
        provider_id: Provider ID
        site_name: Site name
    """
    provider: Provider = Provider.objects.get(id=provider_id)
    if provider.is_configured:
        customer_crm_ids: List[int] = list(
            Customer.objects.filter(provider_id=provider_id, is_active=True)
            .order_by("crm_id")
            .values_list("crm_id", flat=True)
        )
        asyncio.run(
            async_delete_customer_sites(
                provider_id, customer_crm_ids, site_name
            )
        )
        logger.info(
            f"Successfully deleted site: {site_name} for all customers of provider: {provider.name}, "
            f"ID: {provider_id}"
        )
        return
