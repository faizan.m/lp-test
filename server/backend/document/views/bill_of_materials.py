from typing import Dict, List

from django.db.models.query import QuerySet
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.custom_permissions import IsProviderUser
from document.models import (
    ConnectwisePurchaseOrderLineitemsData,
    ConnectwisePurchaseOrderData,
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrder,
    ConnectwiseServiceTicket,
)
from document.serializers import ProjectLineitemsDataSerializer
from project_management.models import Project


class BillOfMaterialsInfoAPIView(ListAPIView):
    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = None
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider_id: int = self.request.provider.id
        project_id: int = self.kwargs.get("project_id")
        try:
            project: Project = Project.objects.get(
                id=project_id, provider_id=provider_id
            )
        except Project.DoesNotExist:
            raise NotFound(detail="Project does not exist!")
        customer_id: int = project.customer_id
        if not customer_id:
            raise NotFound(detail="Customer does not exist!")
        sales_po_data: "QuerySet[SalesPurchaseOrderData]" = (
            SalesPurchaseOrderData.objects.filter(
                provider_id=provider_id,
                customer_id=customer_id,
                opportunity_crm_id__in=project.opportunity_crm_ids,
            )
        )
        result: List = []
        for sales_order in sales_po_data:
            sales_order_data: Dict = dict()
            # 1 sales order can have multiple purchase orders
            purchase_orders_list = []
            purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
                ConnectwisePurchaseOrderData.objects.filter(
                    provider_id=provider_id, sales_order_id=sales_order.id
                )
            )
            for purchase_order in purchase_orders:
                purchase_order_data: Dict = dict()
                po_number_id: str = purchase_order.po_number
                purchase_order_data.update(
                    purchase_order_id=purchase_order.crm_id
                )
                service_ticket_id: int = ConnectwiseServiceTicket.objects.get(
                    id=ConnectwisePurchaseOrder.objects.get(
                        po_number=po_number_id
                    ).ticket_id
                ).ticket_crm_id
                purchase_order_data.update(service_ticket_id=service_ticket_id)
                po_line_items = (
                    ConnectwisePurchaseOrderLineitemsData.objects.filter(
                        provider_id=provider_id,
                        po_number_id=po_number_id,
                    )
                )
                po_line_items_data = ProjectLineitemsDataSerializer(
                    po_line_items, many=True
                ).data
                purchase_order_data.update(
                    {"line_items_data": po_line_items_data}
                )
                purchase_orders_list.append(purchase_order_data)
            sales_order_data.update(sales_order_id=sales_order.crm_id)
            sales_order_data.update({"opportunity_id": sales_order.opportunity_crm_id_id})
            sales_order_data.update({"sales_order_data": purchase_orders_list})
            result.append(sales_order_data)
        return Response(data=result, status=status.HTTP_200_OK)
