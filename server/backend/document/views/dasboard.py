from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from core.renderers import DictToCSVRenderer
from document.services import SOWDashboardService


class SOWServiceActivityDashboardAPIView(APIView):
    """
    Class based view for SOW Service Activity Dashboard
    """

    def get(self, request, *args, **kwargs):
        dashboard = SOWDashboardService.get_services_activity_dashboard(
            request.provider
        )
        return Response(data=dashboard, status=status.HTTP_200_OK)


class SOWServiceHistoryCreatedWonDashboardAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dashboard = SOWDashboardService.get_created_and_won_service_history(
            request.provider
        )
        return Response(data=dashboard, status=status.HTTP_200_OK)


class SOWServiceHistoryExpectedWonDashboardAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dashboard = SOWDashboardService.get_expected_vs_won_hours_historical(
            request.provider
        )
        return Response(data=dashboard, status=status.HTTP_200_OK)


class SOWDashboardRawDataAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dashboard = SOWDashboardService.get_raw_data(request.provider)
        return Response(data=dashboard, status=status.HTTP_200_OK)


class SOWDashboardRawDataExportAPIView(APIView):
    renderer_classes = (DictToCSVRenderer,)

    def get(self, request, *args, **kwargs):
        data = SOWDashboardService.export_raw_data(request.provider)
        headers = {"Content-Disposition": "attachment; filename=Raw Data.csv"}
        return Response(data=data, status=status.HTTP_200_OK, headers=headers)
