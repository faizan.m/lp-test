from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from document.serializers import ChangeRequestCreateSerializer
from document.services import QuoteService


class ChangeRequestCreateAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        serializer = ChangeRequestCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = QuoteService.create_change_request(
            self.request.provider.id, serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_200_OK)
