from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from accounts.custom_permissions import IsProviderUser
from document.models import SOWDocumentSettings
from document.serializers import SOWDocumentSettingsSerializer


class SOWSettingsRetrieveUpdateAPIView(
    generics.RetrieveAPIView, generics.UpdateAPIView
):
    """
    Class based View to provide create API for SOWDocumentSettings
    """

    serializer_class = SOWDocumentSettingsSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_object(self):
        try:
            return self.request.provider.sow_doc_settings
        except SOWDocumentSettings.DoesNotExist:
            return None

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)
