import json
from datetime import datetime, timedelta
from itertools import chain
from typing import Dict, List, Tuple, Union

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db import transaction
from django.db.models import Case, When, Q, Value, CharField, Count, F
from django.db.models import QuerySet
from django.db.models.expressions import RawSQL
from django.db.models.functions import Concat
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status, filters
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from document.custom_filters import (
    SOWDashboardTableDataFilterSet,
    SoWHourlyResourcesDescriptionMappingFilterSet,
)
from document.exceptions import (
    SOWQuoteStatusSettingsNotConfiguredAPIException,
    DefaultDocumentCreatorRoleSettingNotConfigured,
)
from document.models import (
    SOWDocument,
    SOWCategory,
    SOWDocumentSettings,
    SOWVendorOnboardingSetting,
    SOWVendorOnboardingAttachments,
    DefaultDocumentCreatorRoleSetting,
    SoWHourlyResourcesDescriptionMapping,
)
from document.serializers import (
    SOWDocumentCreateSerializer,
    SOWDocumentSerializer,
    SOWDocumentUpdateSerializer,
    SOWDocumentLastUpdatedStatsDataSerializer,
    SOWDocumentAuthorsListSerializer,
    SOWDocumentCountByTechnologySerializer,
    SOWGrossProfitCalculationSerializer,
    CustomerSOWDocumentsDropdownListSerializer,
    SOWDocumentListSerializer,
    SOWVendorOnboardingSettingsRetrieveSerializer,
    SOWVendorOnboardingSettingCreateUpdateSerializer,
    SOWVendorOnboardingAttachmentSerializer,
    SOWVendorOnboardingAttachmentsCreateSerializer,
    DefaultDocumentCreatorRoleSettingSerializer,
    SoWHourlyResourcesDescriptionMappingCreateUpdateSerializer,
    SoWHourlyResourcesDescriptionMappingSerializer,
)
from document.services import (
    ChangeRequestSOWService,
    DocumentService,
    QuoteService,
)
from document.tasks.document_tasks import (
    email_sow_document_to_account_manager,
    sow_document_post_create_update_processing,
)
from document.utils import (
    filter_documents_by_doc_status,
    sow_quote_setting_configured,
    get_sow_vendor_onboarding_settings,
)
from document.exceptions import SOWVendorOnboardingSettingNotConfigured
from exceptions.exceptions import NotFoundError
from utils import get_app_logger, is_valid_uuid

# Get an instance of a logger
logger = get_app_logger(__name__)


class SOWDocumentTypesListAPIView(APIView):
    """
    List SOW Document types
    """

    def get(self, request, *args, **kwargs):
        choices = dict(SOWDocument.SOW_TEMPLATE_CHOICES)
        data = list(choices.keys())
        return Response(data, status=status.HTTP_200_OK)


class SOWDocumentCreateListAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    @staticmethod
    def trim_document_listing_payload(listing_payload: List[Dict]) -> None:
        """
        Removes unnecessary fields from SOW document listing response.

        Args:
            listing_payload: SOW document listing data.
        """
        for document_data in listing_payload:
            json_config: Dict = document_data.pop("json_config", None)
            customer: Dict = document_data.pop("customer", None)
            document_data.pop("major_version", None)
            document_data.pop("minor_version", None)
            document_data.pop("margin", None)
            document_data.pop("user", None)
            document_data.update(
                forecast_exception=json_config.get("forecast_exception", {}),
                customer_name=customer.get("name"),
                customer_id=customer.get("id"),
            )
        return

    def get(self, request, *args, **kwargs):
        show_older: bool = json.loads(
            request.query_params.get("show-older", "false")
        )
        show_closed: bool = json.loads(
            request.query_params.get("show-closed", "false")
        )
        params: Dict = dict()
        if not show_older:
            close_date = datetime.strftime(
                datetime.today() - timedelta(days=60), settings.ISO_FORMAT
            )
            params.update(updated_on__gte=close_date)
        documents: List[Dict] = DocumentService.list_document(
            self.request.provider, params, show_closed
        )
        self.trim_document_listing_payload(documents)
        return Response(data=documents, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        document: SOWDocument = DocumentService.create_document(
            self.request.provider.id,
            self.request.user.id,
            serializer.validated_data,
        )
        email_account_manager: bool = json.loads(
            self.request.query_params.get(
                "email-account-manager", "false"
            ).lower()
        )
        transaction.on_commit(
            lambda: sow_document_post_create_update_processing.apply_async(
                (
                    self.request.provider.id,
                    self.request.user.id,
                    document.id,
                    email_account_manager,
                ),
                queue="high",
            )
        )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return SOWDocumentCreateSerializer
        else:
            return SOWDocumentSerializer


class SOWDocumentRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def retrieve(self, request, *args, **kwargs):
        instance: SOWDocument = self.get_object()
        serializer = self.get_serializer(instance)
        document_data: Dict = serializer.data
        # get document quote
        try:
            quote: Dict = request.provider.erp_client.get_company_quote(
                document_data["quote_id"]
            )
        except NotFoundError:
            document_data["quote_id"] = None
        return Response(document_data)

    def get_queryset(self):
        return (
            SOWDocument.objects.filter(provider_id=self.request.provider.id)
            .select_related(
                "provider",
                "customer",
                "customer__address",
                "user",
                "author",
                "updated_by",
                "category",
            )
            .prefetch_related()
        )

    def update(self, request, *args, **kwargs):
        provider_id: int = self.request.provider.id
        user_id: str = self.request.user.id
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            document_id: int = self.kwargs["pk"]
            DocumentService.update_document(
                provider_id,
                document_id,
                user_id,
                serializer.validated_data,
            )
            email_account_manager: bool = json.loads(
                self.request.query_params.get(
                    "email-account-manager", "false"
                ).lower()
            )
            transaction.on_commit(
                lambda: sow_document_post_create_update_processing.apply_async(
                    (
                        provider_id,
                        user_id,
                        document_id,
                        email_account_manager,
                    ),
                    queue="high",
                )
            )
            updated_document: SOWDocument = SOWDocument.objects.get(
                id=document_id, provider_id=provider_id
            )
            return Response(
                SOWDocumentSerializer(updated_document).data,
                status=status.HTTP_200_OK,
            )

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return SOWDocumentUpdateSerializer
        else:
            return SOWDocumentSerializer


class SOWDocumentDownloadAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        file_type: str = self.request.query_params.get("type", "doc")
        file_name: str = self.request.query_params.get("name", None)
        if file_type not in ("doc", "pdf"):
            raise ValidationError(
                "Please specify the correct file format. PDF or DOC"
            )
        document_id: int = kwargs.get("id")
        document: SOWDocument = get_object_or_404(
            SOWDocument.objects.all(),
            provider=request.provider,
            id=document_id,
        )
        if document.doc_type == SOWDocument.CHANGE_REQUEST:
            service_class = ChangeRequestSOWService
        else:
            service_class = DocumentService
        data: Union[str, Dict] = service_class.download_document(
            request.provider.id,
            request.provider.erp_client,
            request.user,
            document_id,
            file_type,
            file_name,
        )
        if file_type == "pdf":
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            return HttpResponse(data)


class SOWDocumentPreviewAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        payload: Dict = request.data
        payload.update({"provider_id": request.provider.id})
        if payload.get("doc_type") == SOWDocument.CHANGE_REQUEST:
            service_class = ChangeRequestSOWService
        else:
            service_class = DocumentService
        preview_html: Dict = service_class.generate_document_preview(
            request.provider, request.user, payload
        )
        return Response(preview_html)


class SendAccountManagerEmail(APIView):
    def post(self, request, *args, **kwargs):
        document_id: int = kwargs.get("pk")
        document: SOWDocument = DocumentService.get_document(
            request.provider.id, document_id
        )
        erp_client = request.provider.erp_client
        cw_customer: Dict = erp_client.get_customer(document.customer.crm_id)
        territory_manager: Dict = erp_client.get_member(
            cw_customer.get("territory_manager_id")
        )
        if document.doc_type == SOWDocument.CHANGE_REQUEST:
            service_class = ChangeRequestSOWService
        else:
            service_class = DocumentService
        document_format: str = DocumentService.PDF_FORMAT
        pdf_document: Dict = service_class.download_document(
            request.provider,
            erp_client,
            request.user,
            document_id,
            document_format,
        )
        quote: Dict = {}
        provider_id: int = request.provider.id
        try:
            quote: Dict = QuoteService.get_quote(
                request.provider.id, document.quote_id
            )
        except Exception:
            logger.info(
                f"No Quote present for  Document {document.id} with Quote ID as {document.quote_id}"
            )

        task = email_sow_document_to_account_manager.apply_async(
            (
                cw_customer,
                {"name": document.name, "id": document.id},
                pdf_document,
                quote,
                territory_manager,
                {"name": request.user.name, "email": request.user.email},
                provider_id,
            ),
            queue="high",
        )
        data: Dict = {"task_id": task.id}
        return Response(data=data, status=status.HTTP_202_ACCEPTED)


class SOWDocumentLastUpdatedStatsAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = SOWDocumentLastUpdatedStatsDataSerializer
    pagination_class = None
    filterset_class = SOWDashboardTableDataFilterSet
    filter_backends = [
        DjangoFilterBackend,
    ]

    def get_queryset(self):
        provider = self.request.provider
        if not sow_quote_setting_configured(provider):
            raise SOWQuoteStatusSettingsNotConfiguredAPIException()
        document_status_name = self.request.query_params.get(
            "doc-status", False
        )
        author_id = self.request.query_params.get("author_id", False)
        if author_id and not is_valid_uuid(author_id):
            raise ValidationError("Invalid author id! ID must be valid UUID")

        last_7_days = (datetime.now() - timedelta(days=7)).date()
        last_15_days = (datetime.now() - timedelta(days=15)).date()
        last_45_days = (datetime.now() - timedelta(days=45)).date()

        documents = SOWDocument.objects.filter(provider_id=provider.id)
        # Call Generic view's filter_queryset() method to get queryset
        # filtered out by filter backend being used
        filtered_documents = super().filter_queryset(documents)

        if document_status_name:
            filtered_documents = filter_documents_by_doc_status(
                provider, filtered_documents, document_status_name
            )

        queryset = (
            filtered_documents.annotate(
                days_range=Case(
                    When(
                        updated_on__date__gte=last_7_days,
                        then=Value("SOW Document updated within last 7 days"),
                    ),
                    When(
                        Q(updated_on__date__lt=last_7_days)
                        & Q(updated_on__date__gte=last_15_days),
                        then=Value("SOW Document not updated over 7 days"),
                    ),
                    When(
                        Q(updated_on__date__lt=last_15_days)
                        & Q(updated_on__date__gte=last_45_days),
                        then=Value("SOW Document not updated over 15 days"),
                    ),
                    When(
                        updated_on__date__lt=last_45_days,
                        then=Value("SOW Document not updated over 45 days"),
                    ),
                    output_field=CharField(),
                )
            )
            .values("days_range")
            .annotate(count=Count("days_range"))
            .order_by("days_range")
        )
        return queryset


class SOWDocumentAuthorsListAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = SOWDocumentAuthorsListSerializer
    pagination_class = None
    filterset_class = SOWDashboardTableDataFilterSet
    filter_backends = [
        DjangoFilterBackend,
    ]

    def get_queryset(self):
        provider = self.request.provider
        if not sow_quote_setting_configured(provider):
            raise SOWQuoteStatusSettingsNotConfiguredAPIException()
        document_status_name = self.request.query_params.get(
            "doc-status", False
        )
        author_id = self.request.query_params.get("author_id", False)
        if author_id and not is_valid_uuid(author_id):
            raise ValidationError("Invalid author id! ID must be valid UUID")

        documents = SOWDocument.objects.filter(provider_id=provider.id)
        # Call Generic view's filter_queryset() method to get queryset
        # filtered out by filter backend being used
        filtered_documents = super().filter_queryset(documents)

        if document_status_name:
            filtered_documents = filter_documents_by_doc_status(
                provider, filtered_documents, document_status_name
            )

        queryset = (
            filtered_documents.annotate(
                author__name=Concat(
                    F("author__first_name"),
                    Value(" "),
                    F("author__last_name"),
                    output_field=CharField(),
                )
            )
            .values("author__name", "author_id")
            .annotate(count=Count("*"))
            .order_by("-count")
        )
        return queryset


class SOWDocumentCountByTechnologyAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = SOWDocumentCountByTechnologySerializer
    pagination_class = None
    filterset_class = SOWDashboardTableDataFilterSet
    filter_backends = [
        DjangoFilterBackend,
    ]

    @staticmethod
    def calculate_margin_for_documents(documents: QuerySet) -> int:
        """
        Method to calculate margin for documents
        Args:
            documents: SOWDocument queryset
        Returns:
            Total margin for given documents
        """
        total_category_margin = 0
        for document in documents:
            total_category_margin += DocumentService.get_document_margin(
                document
            )
        return total_category_margin

    def calculate_margin_by_category(
        self, documents: QuerySet, document_categories: List[Tuple[int, str]]
    ) -> List[Dict[str, Union[str, int]]]:
        """
        Method to calculate margin for documents of different categories
        Args:
            documents: SOWDocument queryset
            document_categories: List of dictionaries containing category id and category name
        Returns:
            Total margin for documents of various categories
            E.g. [{"category": "security", "margin": 100, "count": 10}, ]
        """
        margin_by_category = []
        for category_id, category_name in document_categories:
            category_documents = documents.filter(category_id=category_id)
            category_documents_count = category_documents.count()
            category_documents_margin = self.calculate_margin_for_documents(
                category_documents
            )
            category_margin_detail = dict(
                category=category_name,
                margin=category_documents_margin,
                count=category_documents_count,
            )
            margin_by_category.append(category_margin_detail)
        return margin_by_category

    @staticmethod
    def get_document_categories() -> List[Tuple[int, str]]:
        """[]
        Method to get different document categories

        Returns:
            List of tuples containing category id and category name
            E.g. [(1, "security"), ]
        """
        document_categories = (
            SOWCategory.objects.values("id", "name")
            .distinct("id", "name")
            .order_by("id", "name")
        )
        document_categories = [
            document_category.values()
            for document_category in document_categories
        ]
        return document_categories

    def get(self, request, *args, **kwargs):
        provider = self.request.provider
        if not sow_quote_setting_configured(provider):
            raise SOWQuoteStatusSettingsNotConfiguredAPIException()
        document_status_name = self.request.query_params.get(
            "doc-status", False
        )
        author_id = self.request.query_params.get("author_id", False)
        if author_id and not is_valid_uuid(author_id):
            raise ValidationError("Invalid author id! ID must be valid UUID")
        documents = SOWDocument.objects.filter(provider_id=provider.id)
        # Call Generic view's filter_queryset() method to get queryset
        # filtered out by filter backend being used
        filtered_documents = super().filter_queryset(documents)

        if document_status_name:
            filtered_documents = filter_documents_by_doc_status(
                provider, filtered_documents, document_status_name
            )

        document_categories = self.get_document_categories()
        margin_by_document_category = self.calculate_margin_by_category(
            filtered_documents, document_categories
        )
        return Response(
            data=margin_by_document_category, status=status.HTTP_200_OK
        )


class SOWDashboardTableDataAPIView(generics.ListCreateAPIView):
    """
    API view for sow dashboard table data
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    ]
    search_fields = [
        "name",
        "customer__name",
        "author__first_name",
        "author__last_name",
        "category__name",
    ]
    ordering_fields = [
        "updated_on",
        "created_on",
        "name",
        "budget_hours",
        "margin",
    ]
    filterset_class = SOWDashboardTableDataFilterSet

    def get_queryset(self):
        params = {}
        params.update(dict(provider_id=self.request.provider.id))
        limited_fields = json.loads(
            self.request.query_params.get("limited_fields", "false")
        )
        # quote_id and customer are required for filtering
        if limited_fields:
            return (
                SOWDocument.objects.filter(**params)
                .only("id", "name", "quote_id", "customer")
                .order_by("-updated_on")
            )
        documents = (
            SOWDocument.objects.filter(**params)
            .select_related(
                "provider",
                "customer",
                "customer__address",
                "user",
                "author",
                "updated_by",
                "category",
            )
            .prefetch_related()
            .annotate(
                budget_hours=RawSQL(
                    """json_config#>'{service_cost,total_hours}'""",
                    (),
                )
            )
            .order_by("-updated_on")
        )
        return documents

    def get(self, request, *args, **kwargs):
        # if limited_fields is True, filtering only on customer_id and doc-status is possible
        limited_fields = json.loads(
            self.request.query_params.get("limited_fields", "false")
        )
        provider = self.request.provider
        if not sow_quote_setting_configured(provider):
            raise SOWQuoteStatusSettingsNotConfiguredAPIException()
        # Check for a valid author_id if present in query params
        author_id = self.request.query_params.get("author_id", False)
        if author_id and not is_valid_uuid(author_id):
            raise ValidationError("Invalid author id! ID must be valid UUID")
        document_status_name = self.request.query_params.get(
            "doc-status", False
        )
        show_closed = json.loads(
            self.request.query_params.get("show-closed", "false")
        )
        # Call Generic view's filter_queryset() method to get queryset
        # filtered out by filter backend being used
        filtered_queryset = super().filter_queryset(self.get_queryset())
        quotes_ids = filtered_queryset.values_list("quote_id", flat=True)
        if limited_fields and not quotes_ids:
            return Response(data=[], status=status.HTTP_200_OK)
        if quotes_ids:
            # Fetch and update quote details to the documents
            quotes = provider.erp_client.get_company_quote_statuses(
                list(quotes_ids)
            )
            if document_status_name:
                filtered_queryset = filter_documents_by_doc_status(
                    provider, filtered_queryset, document_status_name
                )
                if limited_fields:
                    result = CustomerSOWDocumentsDropdownListSerializer(
                        filtered_queryset, many=True
                    ).data
                    return Response(data=result, status=status.HTTP_200_OK)
            quotes = {quote["id"]: quote for quote in quotes}

        # Get paginator for current view and explicitly paginate queryset
        paginator = self.paginator
        paginated_queryset = paginator.paginate_queryset(
            filtered_queryset, request
        )
        documents = SOWDocumentListSerializer(
            paginated_queryset, many=True
        ).data
        for document in documents:
            document["quote"] = quotes.get(document.get("quote_id"))
        result = documents
        # By Default do not show SoW’s where the quote status is No decision or closed.
        if not show_closed:
            result = []
            for doc in documents:
                if (
                    doc["quote"]
                    and doc["quote"]["status_name"].lower()
                    not in DocumentService.CLOSED_STATUSES
                ):
                    result.append(doc)
        for doc in result:
            if doc["quote"] is not None:
                doc["quote_exist"] = True
            else:
                doc["quote_exist"] = False
                doc["quote_id"] = None
        # Return paginated Response object
        response = paginator.get_paginated_response(result)
        return response


class SOWDashboardMarginChartAPIView(generics.ListAPIView):
    """
    API View for SoW dashboard margin chart calculation
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None
    filterset_class = SOWDashboardTableDataFilterSet
    filter_backends = [
        DjangoFilterBackend,
    ]

    @staticmethod
    def calculate_margin_for_documents(
        documents: "Queryset[SOWDocument]", WON_quotes: List
    ) -> Tuple[float, float]:
        """
        Method to calculate margin for documents with WON and OPEN status only
        Args:
            documents: SOWDocument model queryset
            WON_quotes: set containing WON quote ids

        Returns:
            Margin for WON and OPEN status documents
        """
        margin_WON = 0
        margin_OPEN = 0

        for doc in documents:
            if doc.quote_id in set(WON_quotes):
                margin_WON += DocumentService.get_document_margin(doc)
            # If quote_id status is not WON then it has to be OPEN
            else:
                margin_OPEN += DocumentService.get_document_margin(doc)

        return margin_WON, margin_OPEN

    def calculate_margin_for_period(
        self,
        queryset: "Queryset[SOWDocument]",
        begins: datetime.date,
        ends: datetime.date,
        WON_quotes: List,
    ):
        """
        Calculates the total of margins SOW documents for the given period
        :param queryset: SOWDocument queryset
        :param begins: start date for the period
        :param ends: end date for the period
        :param WON_quotes: Quote Ids with the WON status
        :return:
        """
        queryset = queryset.filter(updated_on__date__range=[begins, ends])
        return self.calculate_margin_for_documents(queryset, WON_quotes)

    def get_margin_stats(
        self, WON_quotes: List, required_documents: "Queryset[SOWDocument]"
    ) -> Dict:
        # Calculate margin for a given time period by document status
        present_date = datetime.now().date()
        this_month = (datetime.now() - relativedelta(months=1)).date()
        this_quarter = (datetime.now() - relativedelta(months=3)).date()
        last_quarter = (datetime.now() - relativedelta(months=6)).date()
        (
            total_margin_this_month_WON,
            total_margin_this_month_OPEN,
        ) = self.calculate_margin_for_period(
            required_documents, this_month, present_date, WON_quotes
        )
        (
            total_margin_this_quarter_WON,
            total_margin_this_quarter_OPEN,
        ) = self.calculate_margin_for_period(
            required_documents, this_quarter, present_date, WON_quotes
        )
        (
            total_margin_last_quarter_WON,
            total_margin_last_quarter_OPEN,
        ) = self.calculate_margin_for_period(
            required_documents, last_quarter, this_quarter, WON_quotes
        )
        OPEN_doc_stats = {
            "This Month": total_margin_this_month_OPEN,
            "This Quarter": total_margin_this_quarter_OPEN,
            "Last Quarter": total_margin_last_quarter_OPEN,
        }
        WON_doc_stats = {
            "This Month": total_margin_this_month_WON,
            "This Quarter": total_margin_this_quarter_WON,
            "Last Quarter": total_margin_last_quarter_WON,
        }
        margin_stats = {"Open": OPEN_doc_stats, "Won": WON_doc_stats}
        return margin_stats

    @staticmethod
    def get_filtered_quotes(
        provider: Provider, quote_ids: List
    ) -> Tuple[List, List]:
        """
        Get quotes details from connectwise and group them into WON and OPEN quotes

        """
        sow_quote_settings = (
            provider.erp_integration.sow_document_quote_settings
        )
        WON_status_ids = sow_quote_settings.get("won_status", [])
        OPEN_status_ids = sow_quote_settings.get("open_status", [])
        # Fetch quote statuses for the quote_ids
        quote_status_list = provider.erp_client.get_company_quote_statuses(
            list(quote_ids)
        )
        # Only store the OPEN and WON quote ids
        WON_quote_ids = []
        OPEN_quote_ids = []
        for quote in quote_status_list:
            try:
                if quote.get("status_id") in WON_status_ids:
                    WON_quote_ids.append(quote["id"])
                elif quote.get("status_id") in OPEN_status_ids:
                    OPEN_quote_ids.append(quote["id"])
            except KeyError as e:
                logger.debug(e)
                continue
        return OPEN_quote_ids, WON_quote_ids

    def get(self, request, *args, **kwargs):
        provider = self.request.provider
        if not sow_quote_setting_configured(provider):
            raise SOWQuoteStatusSettingsNotConfiguredAPIException()
        # Check for a valid author_id if present in query params
        author_id = self.request.query_params.get("author_id", False)
        if author_id and not is_valid_uuid(author_id):
            raise ValidationError("Invalid author id! ID must be valid UUID")
        document_status_name = self.request.query_params.get(
            "doc-status", False
        )
        # Fetch documents for the provider
        documents = SOWDocument.objects.filter(provider_id=provider.id)
        # Explicitly filter out documents by filter backend
        documents = super().filter_queryset(documents)
        if document_status_name:
            documents = filter_documents_by_doc_status(
                provider, documents, document_status_name
            )
        quote_ids = documents.values_list("quote_id", flat=True)
        OPEN_quotes, WON_quotes = self.get_filtered_quotes(
            provider, list(quote_ids)
        )

        # Filter only those documents which have status either OPEN or WON
        required_quote_ids_list = list(chain(WON_quotes, OPEN_quotes))
        required_documents = documents.filter(
            quote_id__in=required_quote_ids_list
        )

        margin_stats = self.get_margin_stats(WON_quotes, required_documents)
        return Response(data=margin_stats, status=status.HTTP_200_OK)


class SOWDocumentGrossProfitCalculationAPIView(generics.ListAPIView):
    """
    API View to calculate gross profit percent for SoW Document
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None
    serializer_class = SOWGrossProfitCalculationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            provider = self.request.provider
            params = dict(provider_id=provider.id)
            data = serializer.validated_data
            revenue_and_cost = QuoteService.get_forecast_from_template(
                data, data.get("doc_type"), **params
            )
            margin = revenue_and_cost["revenue"] - revenue_and_cost["cost"]
            try:
                gross_profit_percent = round(
                    (margin / float(revenue_and_cost["revenue"])) * 100
                )
            except ZeroDivisionError:
                gross_profit_percent = 0
            return Response(
                data={"gross_profit_percent": gross_profit_percent},
                status=status.HTTP_200_OK,
            )


class SOWDocumentCalculationDetailsAPIView(generics.ListAPIView):
    """
    API view to list calculation details for SOW document.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None
    serializer_class = SOWGrossProfitCalculationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            document_data: Dict = serializer.validated_data
            sow_doc_settings: SOWDocumentSettings = provider.sow_doc_settings
            sow_calculation_vars: Dict = {}
            risk_budget_customer_cost = "NA"
            risk_budget_margin = "NA"
            risk_budget_internal_cost = "NA"
            risk_budget_margin_percent = "NA"
            service_cost_data = document_data.get("json_config", {}).get(
                "service_cost", {}
            )
            # Engineering hours calculation
            engineering_hours_internal_cost = round(
                (
                    service_cost_data.get("engineering_hours", 0)
                    * sow_doc_settings.engineering_hourly_cost
                ),
                2,
            )
            engineering_hours_customer_cost = round(
                service_cost_data.get("engineering_hours", 0)
                * service_cost_data.get("engineering_hourly_rate", 0),
                2,
            )
            engineering_hours_margin = round(
                (
                    engineering_hours_customer_cost
                    - engineering_hours_internal_cost
                ),
                2,
            )
            try:
                engineering_hours_margin_percent = round(
                    (
                        engineering_hours_margin
                        / engineering_hours_customer_cost
                    )
                    * 100
                )
            except ZeroDivisionError:
                engineering_hours_margin_percent = 0
            try:
                engineering_hours_margin_factor: float = abs(
                    round(
                        (
                            (
                                engineering_hours_customer_cost
                                - float(engineering_hours_internal_cost)
                            )
                            / engineering_hours_customer_cost
                        ),
                        2,
                    )
                )
            except ZeroDivisionError:
                engineering_hours_margin_factor = 0
            sow_calculation_vars.update(
                {
                    "Engineering hours internal cost": engineering_hours_internal_cost,
                    "Engineering hours customer cost": engineering_hours_customer_cost,
                    "Engineering hours margin": engineering_hours_margin,
                    "Engineering hours margin percent": engineering_hours_margin_percent,
                }
            )
            # Engineering after hours calculation
            engineering_after_hours_internal_cost = round(
                (
                    service_cost_data.get("after_hours", 0)
                    * sow_doc_settings.after_hours_cost
                ),
                2,
            )
            engineering_after_hours_customer_cost = round(
                (
                    service_cost_data.get("after_hours", 0)
                    * service_cost_data.get("after_hours_rate", 0)
                ),
                2,
            )
            after_hours_margin = round(
                (
                    engineering_after_hours_customer_cost
                    - engineering_after_hours_internal_cost
                ),
                2,
            )
            sow_calculation_vars.update(
                {
                    "Engineering after hours internal cost": engineering_after_hours_internal_cost,
                    "Engineering after hours customer cost": engineering_after_hours_customer_cost,
                    "Engineering after hours margin": after_hours_margin,
                }
            )
            # Project management hours calculation
            PM_hours_internal_cost = round(
                (
                    service_cost_data.get("project_management_hours", 0)
                    * sow_doc_settings.pm_hourly_cost
                ),
                2,
            )
            PM_hours_customer_cost = round(
                (
                    service_cost_data.get("project_management_hours", 0)
                    * service_cost_data.get(
                        "project_management_hourly_rate", 0
                    )
                ),
                2,
            )
            PM_hours_margin = round(
                (PM_hours_customer_cost - PM_hours_internal_cost), 2
            )
            sow_calculation_vars.update(
                {
                    "PM hours internal cost": PM_hours_internal_cost,
                    "PM hours customer cost": PM_hours_customer_cost,
                    "PM hours margin": PM_hours_margin,
                }
            )
            if (
                document_data.get("doc_type").lower()
                == SOWDocument.CHANGE_REQUEST.lower()
            ):
                revenue = float(service_cost_data.get("customer_cost", {}))
                cost = float(service_cost_data.get("internal_cost", {}))
            elif (
                document_data.get("doc_type").lower()
                == SOWDocument.T_AND_M.lower()
            ):
                revenue = float(
                    service_cost_data.get("customer_cost_t_and_m_fee", {})
                )
                cost = float(
                    service_cost_data.get("internal_cost_t_and_m_fee", {})
                )
            else:
                revenue = float(
                    service_cost_data.get("customer_cost_fixed_fee", {})
                )
                cost = float(
                    service_cost_data.get("internal_cost_fixed_fee", {})
                )
                risk_budget_customer_cost = round(
                    (
                        engineering_hours_customer_cost
                        + engineering_after_hours_customer_cost
                        + PM_hours_customer_cost
                    )
                    * sow_doc_settings.fixed_fee_variable,
                    2,
                )
                risk_budget_margin = round(
                    (
                        risk_budget_customer_cost
                        * engineering_hours_margin_factor
                    ),
                    2,
                )
                risk_budget_internal_cost = round(
                    (risk_budget_customer_cost - risk_budget_margin), 2
                )
                risk_budget_margin_percent = round(
                    (risk_budget_margin / risk_budget_customer_cost) * 100
                )
                cost += risk_budget_internal_cost
                sow_calculation_vars.update(
                    {
                        "Risk budget customer cost": risk_budget_customer_cost,
                        "Risk budget internal cost": risk_budget_internal_cost,
                        "Risk budget margin": risk_budget_margin,
                        "Risk budget margin percent": risk_budget_margin_percent,
                    }
                )
            contractors = service_cost_data.get("contractors", [])
            contractor_partner_cost = 0
            contractor_customer_cost = 0
            for contractor in contractors:
                contractor_partner_cost += contractor.get("partner_cost", 0)
                contractor_customer_cost += contractor.get("customer_cost", 0)
            sow_calculation_vars.update(
                {
                    "Contractor partner cost": round(
                        contractor_partner_cost, 2
                    ),
                    "Contractor customer cost": round(
                        contractor_customer_cost, 2
                    ),
                }
            )
            cost -= contractor_partner_cost
            revenue -= contractor_customer_cost
            total_margin: float = round((revenue - cost))
            sow_calculation_vars.update(
                {
                    "Total margin": total_margin,
                }
            )
            return Response(
                data=sow_calculation_vars, status=status.HTTP_200_OK
            )


class SOWVendorOnboardingSettingCreateRetrieveUpdateAPIView(
    generics.RetrieveUpdateAPIView, generics.CreateAPIView
):
    permission_classes = [IsAuthenticated, IsProviderUser]

    def get_object(self) -> SOWVendorOnboardingSetting:
        provider: Provider = self.request.provider
        try:
            sow_vendor_onboarding_settings: SOWVendorOnboardingSetting = (
                provider.sow_vendor_onboarding_setting
            )
        except SOWVendorOnboardingSetting.DoesNotExist:
            raise SOWVendorOnboardingSettingNotConfigured()
        return sow_vendor_onboarding_settings

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            self.request.provider.sow_vendor_onboarding_setting
        except SOWVendorOnboardingSetting.DoesNotExist:
            serializer.save(provider=provider)
        else:
            raise ValidationError(
                "SOW vendor onboarding setting already exists for the Provider!"
            )

    def get_serializer_class(self):
        if self.request.method == "GET":
            return SOWVendorOnboardingSettingsRetrieveSerializer
        else:
            return SOWVendorOnboardingSettingCreateUpdateSerializer


class SOWVendorOnboardingAttachmentsListCreateAPIView(
    generics.ListCreateAPIView
):
    """
    API view to list create SoW vendor onboarding attachments.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = SOWVendorOnboardingAttachmentSerializer

    def get_queryset(self) -> "QuerySet[SOWVendorOnboardingAttachments]":
        provider: Provider = self.request.provider
        sow_vendor_onboarding_settings: SOWVendorOnboardingSetting = (
            get_sow_vendor_onboarding_settings(provider)
        )
        queryset = SOWVendorOnboardingAttachments.objects.filter(
            provider_id=provider.id,
            sow_vendor_onboarding_setting_id=sow_vendor_onboarding_settings.id,
        )
        return queryset

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            get_sow_vendor_onboarding_settings(provider)
        )
        serializer.save(
            provider=provider,
            sow_vendor_onboarding_setting=sow_vendor_onboarding_setting,
        )


class SOWVendorOnboardingAttachmentsDestroyAPIView(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    queryset = SOWVendorOnboardingAttachments.objects.all()
    lookup_field = "id"


class DefaultDocumentCreatorRoleSettingCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    """
    API view to create, retrieve, update Document Creator Default Role Setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = DefaultDocumentCreatorRoleSettingSerializer

    def get_object(self) -> DefaultDocumentCreatorRoleSetting:
        provider: Provider = self.request.provider
        try:
            document_creator_default_role_setting: DefaultDocumentCreatorRoleSetting = (
                provider.default_document_creator_role_setting
            )
        except DefaultDocumentCreatorRoleSetting.DoesNotExist:
            raise DefaultDocumentCreatorRoleSettingNotConfigured()
        return document_creator_default_role_setting

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            self.request.provider.default_document_creator_role_setting
        except DefaultDocumentCreatorRoleSetting.DoesNotExist:
            serializer.save(provider=provider)
        else:
            raise ValidationError(
                "Default Document Creator Role Setting already exists for the Provider!"
            )


class SoWHourlyResourcesDescriptionMappingListCreateAPIView(
    generics.ListCreateAPIView
):
    """
    API view to list, create SoW hourly resources name to description mapping.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = ("vendor_name", "resource_description")
    ordering_fields = ("vendor_name", "resource_description")
    filterset_class = SoWHourlyResourcesDescriptionMappingFilterSet

    def get_queryset(self):
        provider: Provider = self.request.provider
        hourly_resources_description_mapping: "QuerySet[SoWHourlyResourcesDescriptionMapping]" = (
            provider.hourly_resources_description_mapping.all()
        )
        return hourly_resources_description_mapping

    def perform_create(self, serializer):
        serializer.save(provider=self.request.provider)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return SoWHourlyResourcesDescriptionMappingCreateUpdateSerializer
        else:
            return SoWHourlyResourcesDescriptionMappingSerializer


class SoWHourlyResourcesDescriptionMappingRetrieveUpdateDeleteAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    """ "
    API view to retrieve, update, delete SoW vendor description.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_object(self):
        provider: Provider = self.request.provider
        vendor_description: SoWHourlyResourcesDescriptionMapping = (
            get_object_or_404(
                provider.hourly_resources_description_mapping.all(),
                id=self.kwargs.get("id"),
                provider_id=provider.id,
            )
        )
        return vendor_description

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return SoWHourlyResourcesDescriptionMappingCreateUpdateSerializer
        else:
            return SoWHourlyResourcesDescriptionMappingSerializer
