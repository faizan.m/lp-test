import datetime
import json
from typing import Dict, List, Tuple, Set, Union, Optional

from django.db import IntegrityError
from django.db.models import (
    Case,
    CharField,
    Count,
    OuterRef,
    Q,
    Subquery,
    Value,
    When,
    F,
    QuerySet,
    IntegerField,
)
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import exceptions.exceptions
from accounts.custom_permissions import IsProviderUser, IsCustomerUser
from accounts.models import Provider
from core.exceptions import (
    InvalidConfigurations,
)
from document.custom_filters import (
    OrderTrackingDashboardTableDataFilterSet,
    CustomerSalesOrderDataFilterSet,
    UnlinkedPurchaseOrdersListingFilterSet,
    OpportunityTicketSOPOLinkingDataFilterSet,
)
from document.exceptions import (
    OperationDashboardSettingsNotConfigured,
    POCustomFieldNotConfigured,
)
from document.mixins import ShippedNotReceivedMixin
from document.models import (
    ConnectwisePurchaseOrderData,
    ConnectwisePurchaseOrderLineitemsData,
    ConnectwiseServiceTicket,
    ConnectwiseServiceTicketParseFailed,
    IngramPurchaseOrderData,
    OperationDashboardSettings,
    SalesPurchaseOrderData,
    ProductsItemData,
    SalesOrderStatusSettings,
)
from document.serializers import (
    ConnectwisePurchaseOrderDataSerializer,
    ConnectwiseServiceTicketParseFailedSerializer,
    LastCustomerUpdateSerializer,
    OpenPOsByDistributorSerializer,
    OperationsDashboardSettingsSerializer,
    OrdersByEstimatedShippedDateSerializer,
    OrdersShippedButNotReceivedSerializer,
    CustomerSalesOrderDataSerializer,
    CustomerProductitemsDataSerializer,
    SalesOrdersStatusSettingsSerializer,
    UnlinkedPurchaseOrderInfoSerializer,
    SalesOrderToPurchaseOrderLinkingCreateSerializer,
    SalesOrderToPurchaseOrderLinkingListSerializer,
    SalesOrderToPurchaseOrderLinkDeleteSerializer,
    IgnoreFailedTicketsSerializer,
    OpportunityTicketSOPOLinkingSerializer,
    ServiceTicketToSoLinkingSeriaizer,
    IgnoreSalesOrdersSerializer,
)
from document.services.operations_dashboard import (
    OperationsDashboardService,
    ConnectwiseServiceTicketPayloadHandler,
)
from document.tasks import sync_order_tracking_dashboard_data
from django.contrib.postgres.aggregates import ArrayAgg
from utils import get_app_logger

logger = get_app_logger(__name__)

LAST_WEEK_DAY = 6


class OperationDashboardSettingsCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    permission_classes = [IsAuthenticated, IsProviderUser]
    serializer_class = OperationsDashboardSettingsSerializer

    def get_queryset(self):
        queryset = OperationDashboardSettings.objects.filter(
            provider=self.request.provider
        )
        return queryset

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):
        try:
            OperationDashboardSettings.objects.get(
                provider=self.request.provider
            )
        except OperationDashboardSettings.DoesNotExist:
            serializer.save(provider=self.request.provider)
            sync_order_tracking_dashboard_data.apply_async(
                ([self.request.provider.id],), queue="high"
            )
        else:
            raise ValidationError(
                "Operation Dashboard Settings already exists for the Provider."
            )

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)
        sync_order_tracking_dashboard_data.apply_async(
            ([self.request.provider.id],), queue="high"
        )


class SalesOrdersStatusSettingsCreateRetrieveUpdateView(
    generics.CreateAPIView, generics.RetrieveUpdateAPIView
):
    permission_classes = [
        IsAuthenticated,
    ]
    serializer_class = SalesOrdersStatusSettingsSerializer

    def get_queryset(self, **kwargs):
        provider: Provider = self.request.provider
        queryset = SalesOrderStatusSettings.objects.filter(
            provider_id=provider.id
        )
        return queryset

    def get_object(self):
        obj = self.get_queryset().first()
        return obj

    def perform_create(self, serializer):
        provider: Provider = self.request.provider
        try:
            SalesOrderStatusSettings.objects.get(provider_id=provider.id)
        except SalesOrderStatusSettings.DoesNotExist:
            serializer.save(provider_id=provider.id)
        else:
            raise ValidationError(
                "Sales Orders Status Settings already exists for this Provider."
            )

    def perform_update(self, serializer):
        provider: Provider = self.request.provider
        serializer.save(provider_id=provider.id)


class OrderTrackingOpenPOsByDistributorAPIView(generics.ListAPIView):
    """
    Get the list of Open PO's by distributor.
    Takes in a boolean query parameter `show-all-data`. If sent as true, all users data will be returned else
    data will be filtered on the logged in User.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    serializer_class = OpenPOsByDistributorSerializer
    pagination_class = None

    def get_queryset(self):
        provider: Provider = self.request.provider

        show_all_users_data = json.loads(
            self.request.query_params.get("show-all-data", "false")
        )
        filter_conditions = dict(provider=provider)
        if not show_all_users_data:
            owner = self.request.user
            filter_conditions.update(dict(po_number__ticket__owner=owner))
        try:
            po_order_status_ids = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).purchase_order_status_ids
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()

        filter_conditions.update(
            dict(purchase_order_status_id__in=po_order_status_ids)
        )

        queryset = (
            ConnectwisePurchaseOrderData.objects.values("vendor_company_name")
            .annotate(count=Count("*"))
            .filter(**filter_conditions)
            .order_by("-count", "vendor_company_name")[:5]
        )
        return queryset


class OrderTrackingTableDataAPIView(
    ShippedNotReceivedMixin, generics.ListAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = ConnectwisePurchaseOrderDataSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    # filterset_fields = ("vendor_company_name", "po_number__po_number")
    search_fields = (
        "po_number__ticket__summary",
        "po_number__ticket__status_name",
        "po_number__ticket__company_name",
        "po_number__po_number",
        "vendor_company_name",
    )
    ordering_fields = (
        "po_number__ticket__status_name",
        "po_number__ticket__company_name",
        "po_number__ticket__connectwise_last_data_update",
        "po_number__po_number",
        "vendor_company_name",
        "eta",
    )
    filterset_class = OrderTrackingDashboardTableDataFilterSet

    def get_queryset(self):
        provider: Provider = self.request.provider
        current_date = datetime.date.today()
        show_all_users_data = json.loads(
            self.request.query_params.get("show-all-data", "false")
        )
        filter_conditions = dict(provider=provider)

        try:
            po_order_status_ids = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).purchase_order_status_ids
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()

        filter_conditions.update(
            dict(purchase_order_status_id__in=po_order_status_ids)
        )
        if not show_all_users_data:
            owner = self.request.user
            filter_conditions.update(dict(po_number__ticket__owner=owner))

        shipped_not_received = json.loads(
            self.request.query_params.get("shipped-not-received", "false")
        )
        if shipped_not_received:
            shipped_but_not_received_po_numbers = (
                ShippedNotReceivedMixin.get_shipped_not_received(
                    self, filter_conditions={}
                )
            ).values_list("po_number", flat=True)
            filter_conditions.update(
                dict(po_number__in=list(shipped_but_not_received_po_numbers))
            )

        eta_query = Subquery(
            (
                IngramPurchaseOrderData.objects.filter(
                    provider=OuterRef("provider"),
                    po_number=OuterRef("po_number"),
                    eta__gte=current_date,
                )
                .order_by("eta")
                .values("eta")
            )[:1]
        )
        shipped_date_query = Subquery(
            (
                IngramPurchaseOrderData.objects.filter(
                    provider=OuterRef("provider"),
                    po_number=OuterRef("po_number"),
                    eta__gte=current_date,
                )
                .order_by("eta")
                .values("shipped_date")
            )[:1]
        )
        queryset = (
            ConnectwisePurchaseOrderData.objects.select_related(
                "po_number", "po_number__ticket"
            )
            .annotate(eta=eta_query, shipped_date=shipped_date_query)
            .filter(**filter_conditions)
            .order_by("-po_number__ticket__connectwise_last_data_update")
        )
        return queryset


class OrderTrackingLastCustomerUpdateAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = LastCustomerUpdateSerializer
    pagination_class = None

    def get_queryset(self):
        provider: Provider = self.request.provider

        last_7_days = (timezone.now() - datetime.timedelta(days=7)).date()
        last_14_days = (timezone.now() - datetime.timedelta(days=14)).date()
        last_30_days = (timezone.now() - datetime.timedelta(days=30)).date()

        show_all_users_data = json.loads(
            self.request.query_params.get("show-all-data", "false")
        )
        ticket_filters = dict(provider=provider)
        if not show_all_users_data:
            owner = self.request.user
            ticket_filters.update(dict(po_number__ticket__owner=owner))
        try:
            po_order_status_ids = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).purchase_order_status_ids
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()

        ticket_filters.update(
            dict(purchase_order_status_id__in=po_order_status_ids)
        )

        ticket_ids = ConnectwisePurchaseOrderData.objects.filter(
            **ticket_filters
        ).values_list("po_number__ticket_id", flat=True)
        filter_conditions = dict(provider=provider, id__in=tuple(ticket_ids))
        queryset = (
            ConnectwiseServiceTicket.objects.filter(**filter_conditions)
            .annotate(
                day_range=Case(
                    When(
                        connectwise_last_data_update__date__gte=last_7_days,
                        then=Value("Updated within 7 days"),
                    ),
                    When(
                        Q(connectwise_last_data_update__date__lt=last_7_days)
                        & Q(
                            connectwise_last_data_update__date__gte=last_14_days
                        ),
                        then=Value("Tickets not updated over 7 days"),
                    ),
                    When(
                        Q(connectwise_last_data_update__date__lt=last_14_days)
                        & Q(
                            connectwise_last_data_update__date__gte=last_30_days
                        ),
                        then=Value("Tickets not updated over 14 days"),
                    ),
                    When(
                        connectwise_last_data_update__date__lt=last_30_days,
                        then=Value("Tickets not updated over 30 days"),
                    ),
                    output_field=CharField(),
                )
            )
            .values("day_range")
            .annotate(count=Count("day_range"))
            .order_by("day_range")
        )

        return queryset


class OrderTrackingOrdersByEstimatedShippedDateAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = OrdersByEstimatedShippedDateSerializer
    pagination_class = None

    def get_queryset(self):
        provider: Provider = self.request.provider

        filter_conditions = dict(provider=provider)
        try:
            po_order_status_ids = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).purchase_order_status_ids
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()

        filter_conditions.update(
            dict(purchase_order_status_id__in=po_order_status_ids)
        )

        show_all_users_data = json.loads(
            self.request.query_params.get("show-all-data", "false")
        )
        if not show_all_users_data:
            owner = self.request.user
            filter_conditions.update(dict(po_number__ticket__owner=owner))

        next_30_days = timezone.now() + datetime.timedelta(days=30)
        next_60_days = timezone.now() + datetime.timedelta(days=60)
        next_90_days = timezone.now() + datetime.timedelta(days=90)

        # int value range: 0-6, monday-sunday
        current_week_day = timezone.now().weekday()

        first_day_current_week = timezone.now() - datetime.timedelta(
            days=current_week_day
        )
        last_day_current_week = timezone.now() + datetime.timedelta(
            days=LAST_WEEK_DAY - current_week_day
        )
        current_date = datetime.date.today()
        eta_query = Subquery(
            (
                IngramPurchaseOrderData.objects.filter(
                    provider=OuterRef("provider"),
                    po_number=OuterRef("po_number"),
                    eta__gte=current_date,
                )
                .order_by("eta")
                .values("eta")
            )[:1]
        )
        queryset = (
            ConnectwisePurchaseOrderData.objects.select_related(
                "po_number",
            )
            .filter(**filter_conditions)
            .annotate(eta=eta_query)
            .annotate(
                shipped_range=Case(
                    When(
                        Q(eta__lte=last_day_current_week)
                        & Q(eta__gte=first_day_current_week),
                        then=Value("Current Week"),
                    ),
                    When(
                        eta__lt=next_30_days,
                        then=Value("30 Days"),
                    ),
                    When(
                        Q(eta__lt=next_60_days) & Q(eta__gte=next_30_days),
                        then=Value("60 Days"),
                    ),
                    When(
                        Q(eta__lt=next_90_days) & Q(eta__gte=next_60_days),
                        then=Value("90 Days"),
                    ),
                    When(
                        eta__gte=next_90_days,
                        then=Value("Over 90 days"),
                    ),
                    output_field=CharField(),
                    default=Value("No ETA Available"),
                )
            )
            .values("shipped_range")
            .annotate(count=Count("shipped_range"))
        )

        return queryset


class SyncFailedTicketsListAPIView(generics.ListAPIView):
    """
    API view to list service tickets that failed to sync.
    """

    serializer_class = ConnectwiseServiceTicketParseFailedSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ("ticket_crm_id", "summary")
    ordering_fields = ("ticket_crm_id", "summary")

    def get_queryset(self) -> "QuerySet[ConnectwiseServiceTicketParseFailed]":
        queryset: "QuerySet[ConnectwiseServiceTicketParseFailed]" = (
            ConnectwiseServiceTicketParseFailed.objects.filter(
                provider=self.request.provider, is_ignored=False
            ).order_by("ticket_crm_id")
        )
        return queryset


class IgnoreFailedTicketsUpdateAPIView(generics.UpdateAPIView):
    serializer_class = IgnoreFailedTicketsSerializer

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            queryset = (
                ConnectwiseServiceTicketParseFailed.objects.filter(
                    provider=self.request.provider,
                    ticket_crm_id__in=request.data.get("ticket_crm_ids"),
                )
                .values("ticket_crm_id", "is_ignored")
                .order_by("ticket_crm_id")
            ).update(is_ignored=True)
            return Response(status=status.HTTP_200_OK)


class OrderTrackingOrdersShippedNotReceivedAPIView(
    ShippedNotReceivedMixin, generics.ListAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = OrdersShippedButNotReceivedSerializer
    pagination_class = None

    def get_queryset(self):
        provider: Provider = self.request.provider
        show_all_users_data = json.loads(
            self.request.query_params.get("show-all-data", "false")
        )
        filter_conditions = dict()
        try:
            po_order_status_ids = OperationDashboardSettings.objects.get(
                provider_id=provider.id
            ).purchase_order_status_ids
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()

        filter_conditions.update(
            dict(
                po_number__details__purchase_order_status_id__in=po_order_status_ids
            )
        )
        if not show_all_users_data:
            owner = self.request.user
            filter_conditions.update(dict(po_number__ticket__owner=owner))

        shipped_but_not_received_query_set = (
            (
                ShippedNotReceivedMixin.get_shipped_not_received(
                    self, filter_conditions=filter_conditions
                )
            )
            .values("po_number")
            .distinct()
        )

        total_line_items_count = (
            ConnectwisePurchaseOrderLineitemsData.objects.filter(
                provider=provider
            ).count()
        )

        data_count = [
            {
                "shipped_but_not_received_count": shipped_but_not_received_query_set.count(),
                "total_line_items_count": total_line_items_count,
            }
        ]
        return data_count


class CustomerSalesOrderDataAPIView(generics.ListAPIView):
    """
    Get the sales order data for a particular customer
    """

    permission_classes = (IsAuthenticated, IsCustomerUser)
    serializer_class = CustomerSalesOrderDataSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    search_fields = (
        "customer_po_number",
        "order_date",
        "opportunity_name",
    )

    ordering_fields = (
        "order_date",
        "customer_po_number",
    )

    filterset_class = CustomerSalesOrderDataFilterSet

    def get_status_settings(self) -> Dict:
        """
        get ship status setting for the provider
        """
        provider: Provider = self.request.provider
        sales_order_status_settings = SalesOrderStatusSettings.objects.filter(
            provider=provider
        ).values(
            "in_progress_status",
            "waiting_on_smartnet_status",
            "closed_statuses",
        )
        if sales_order_status_settings:
            return sales_order_status_settings[0]
        else:
            raise InvalidConfigurations(
                "Sales Order status settings in not configured"
            )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        sales_order_status_settings = self.get_status_settings()
        context.update(
            dict(sales_order_status_settings=sales_order_status_settings)
        )
        return context

    def get_queryset(self, **kwargs):
        provider: Provider = self.request.provider
        customer_id = self.request.user.customer.id
        filter_conditions = dict(customer_id=customer_id, provider=provider)

        queryset = SalesPurchaseOrderData.objects.filter(
            **filter_conditions
        ).order_by("-order_date")

        return queryset


class CustomerProductItemsAPIView(generics.ListAPIView):
    """
    Get the Product Items for a particular sales order
    """

    permission_classes = (IsAuthenticated, IsCustomerUser)
    serializer_class = CustomerProductitemsDataSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    search_fields = (
        "product_id",
        "description",
        "quantity",
    )

    ordering_fields = (
        "product_id",
        "description",
        "quantity",
    )

    def get_bill_closed_flag(self) -> bool:
        """
        get bill status flag
        """
        sales_order_id: int = self.kwargs.get("sales_order_id")
        provider: Provider = self.request.provider
        bill_status_flag = SalesPurchaseOrderData.objects.filter(
            id=sales_order_id, provider=provider
        ).values("bill_closed_flag")
        bill_status_flag = list(bill_status_flag)[0].get("bill_closed_flag")
        return bill_status_flag

    def get_serializer_context(self):
        context = super().get_serializer_context()
        get_bill_closed_flag = self.get_bill_closed_flag()
        context.update(dict(get_bill_closed_flag=get_bill_closed_flag))
        return context

    def get_queryset(self):
        provider: Provider = self.request.provider
        sales_order_id = self.kwargs.get("sales_order_id")
        filter_conditions = dict(
            sales_order_id=sales_order_id, provider=provider
        )
        queryset = ProductsItemData.objects.filter(
            **filter_conditions
        ).order_by("-quantity")

        return queryset


class CustomFieldsListAPIView(APIView):
    """APIView to list custom fields set up by the provider"""

    def get(self, request, *args, **kwargs):
        provider = self.request.provider
        custom_fields = provider.erp_client.get_custom_fields()
        return Response(data=custom_fields, status=status.HTTP_200_OK)


class UnlinkedPurchaseOrdersListAPIView(generics.ListAPIView):
    """
    API View to list purchase orders that are not linked to any sales order.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = UnlinkedPurchaseOrderInfoSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "po_number__ticket__summary",
        "po_number__ticket__company_name",
        "po_number__po_number",
    )
    ordering_fields = (
        "po_number",
        "created_on",
        "updated_on",
        "customer_name",
        "service_ticket_summary",
    )
    filterset_class = UnlinkedPurchaseOrdersListingFilterSet

    def get_queryset(self):
        provider_id: int = self.request.provider.id
        unlinked_purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            ConnectwisePurchaseOrderData.objects.filter(
                provider_id=provider_id,
                sales_order__isnull=True,
            )
            .select_related("po_number", "po_number__ticket")
            .only(
                "id",
                "crm_id",
                "po_number",
                "po_number__ticket__summary",
                "po_number__ticket__ticket_crm_id",
                "po_number__ticket__company_name",
                "custom_field_present",
            )
            .annotate(
                customer_name=F("po_number__ticket__company_name"),
                service_ticket_summary=F("po_number__ticket__summary"),
            )
            .order_by("-updated_on")
        )
        filtered_queryset = super().filter_queryset(unlinked_purchase_orders)
        return filtered_queryset


class SalesOrderToPurchaseOrderLinkingMixin:
    permission_class = (IsAuthenticated, IsProviderUser)

    def get_purchase_order_custom_field_id(
        self: Union[generics.ListCreateAPIView, generics.UpdateAPIView]
    ) -> int:
        """
        Get PO custom field ID as configured in Operation Dashboard settings.
        Raise validation errors if not configured.

        Returns:
            PO custom field ID
        """
        provider_id: int = self.request.provider.id
        try:
            purchase_order_custom_field_id: int = (
                OperationDashboardSettings.objects.get(
                    provider_id=provider_id
                ).po_custom_field_id
            )
        except OperationDashboardSettings.DoesNotExist:
            raise OperationDashboardSettingsNotConfigured()
        if purchase_order_custom_field_id is None:
            raise POCustomFieldNotConfigured()
        return purchase_order_custom_field_id

    def get_sales_order(
        self: Union[generics.ListCreateAPIView, generics.UpdateAPIView],
        sales_order_crm_id: int,
    ) -> SalesPurchaseOrderData:
        """
        Fetch sales order from DB

        Args:
            sales_order_crm_id: CRM ID of sales order

        Returns:
            SalesPurchaseOrderData object
        """
        provider_id: int = self.request.provider.id
        try:
            sales_order: SalesPurchaseOrderData = (
                SalesPurchaseOrderData.objects.get(
                    provider_id=provider_id, crm_id=sales_order_crm_id
                )
            )
        except SalesPurchaseOrderData.DoesNotExist:
            raise NotFound("Sales order not found!")
        return sales_order


class SalesOrderToPurchaseOrderLinkingListCreateAPIView(
    SalesOrderToPurchaseOrderLinkingMixin, generics.ListCreateAPIView
):
    """
    API view to list linked purchase orders and to link
    purchase orders to a sales order.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "crm_id",
        "linked_pos__po_number__po_number",
        "linked_pos__po_number__ticket__company_name",
        "company_name",
    )
    ordering_fields = ("crm_id", "company_name")

    def get_queryset(self):
        provider: Provider = self.request.provider
        provider_id: int = provider.id
        linked_sales_order_ids: List[int] = list(
            ConnectwisePurchaseOrderData.objects.filter(
                provider_id=provider_id, sales_order__isnull=False
            ).values_list("sales_order_id", flat=True)
        )
        sales_orders: "QuerySet[SalesPurchaseOrderData]" = (
            SalesPurchaseOrderData.objects.filter(
                provider_id=provider_id, id__in=linked_sales_order_ids
            )
            .only(
                "id",
                "crm_id",
                "company_name",
                "opportunity_crm_id",
                "opportunity_name",
            )
            .annotate(
                purchase_order_crm_ids=ArrayAgg(
                    "linked_pos__crm_id", distinct=True
                ),
                purchase_order_numbers=ArrayAgg(
                    "linked_pos__po_number", distinct=True
                ),
            )
        )
        return sales_orders

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            provider_id: int = provider.id
            sales_order_crm_id: int = serializer.validated_data.get(
                "sales_order_crm_id"
            )
            purchase_order_crm_ids: Set[int] = set(
                serializer.validated_data.get("purchase_order_crm_ids")
            )
            po_custom_field_id: int = self.get_purchase_order_custom_field_id()
            sales_order: SalesPurchaseOrderData = self.get_sales_order(
                sales_order_crm_id
            )
            existing_linked_po_crm_ids: Set[int] = set(
                ConnectwisePurchaseOrderData.objects.filter(
                    provider_id=provider_id,
                    sales_order_id=sales_order.id,
                ).values_list("crm_id", flat=True)
            )
            # Find out the POs for which to create and delete link
            new_po_to_link: Set[int] = (
                purchase_order_crm_ids - existing_linked_po_crm_ids
            )
            po_link_to_delete: Set[int] = (
                existing_linked_po_crm_ids - purchase_order_crm_ids
            )

            dashboard_service: OperationsDashboardService = (
                OperationsDashboardService(provider)
            )
            _po_linking_result: Optional[Dict] = {}
            _po_link_removal_result: Optional[Dict] = {}

            if new_po_to_link:
                # PO should exist in DB for linking
                if not ConnectwisePurchaseOrderData.objects.filter(
                    provider_id=provider_id, crm_id__in=new_po_to_link
                ).exists():
                    raise NotFound(f"Purchase order not found for linking!")
                else:
                    new_purchase_orders_to_link: "QuerySet[ConnectwisePurchaseOrderData]" = ConnectwisePurchaseOrderData.objects.filter(
                        provider_id=provider_id, crm_id__in=new_po_to_link
                    )
                    _po_linking_result: Dict = (
                        dashboard_service.link_purchase_orders_to_sales_order(
                            sales_order,
                            new_purchase_orders_to_link,
                            po_custom_field_id,
                            purchase_order_crm_ids,
                        )
                    )
            if po_link_to_delete:
                # PO should exist in DB for deleting the link
                if not ConnectwisePurchaseOrderData.objects.filter(
                    provider_id=provider_id, crm_id__in=po_link_to_delete
                ).exists():
                    raise NotFound(f"Purchase order to delete not found!")
                else:
                    _po_link_removal_result: Dict = dashboard_service.remove_purchase_order_link_from_sales_order(
                        tuple(po_link_to_delete),
                        sales_order,
                        purchase_order_crm_ids,
                        po_custom_field_id,
                    )
            result: Dict = dict(
                po_linking_result=_po_linking_result,
                po_link_removal_result=_po_link_removal_result,
            )

            if (
                new_po_to_link
                and _po_linking_result.get("linking_errors", None)
            ) or (
                po_link_to_delete
                and _po_link_removal_result.get(
                    "purchase_order_link_remove_errors", None
                )
            ):
                return Response(
                    data=result, status=status.HTTP_400_BAD_REQUEST
                )
            return Response(data=result, status=status.HTTP_201_CREATED)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return SalesOrderToPurchaseOrderLinkingListSerializer
        return SalesOrderToPurchaseOrderLinkingCreateSerializer


class OpportunityTicketSOPOLinkingListAPIView(generics.ListAPIView):
    """
    API View to list the linked & unlinked opportunity, service ticket, SOs and POs.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = OpportunityTicketSOPOLinkingSerializer

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "crm_id",
        "opportunity_name",
        "linked_pos__po_number__po_number",
        "opportunity_crm_id__summary",
    )
    ordering_fields = ("crm_id", "linked_pos__po_number__po_number")
    filterset_class = OpportunityTicketSOPOLinkingDataFilterSet

    def get_queryset(self):
        # Get all records from the left-side model that have a matching key in the right-side model
        # left_side_records = LeftModel.objects.filter(right_key__isnull=False).values_list('right_key', 'left_key')

        service_ticket_records = ConnectwiseServiceTicket.objects.filter(
            opportunity_crm_id__isnull=False,
            provider_id=self.request.provider.id,
        ).values_list("opportunity_crm_id", "opportunity_crm_id")

        # Combine the querysets using `Case` and `When` to simulate a right outer join
        linked_records = (
            SalesPurchaseOrderData.objects.filter(
                provider_id=self.request.provider.id,
                is_ignored=False,
            ).values(
                "id",
                "crm_id",
                "opportunity_crm_id",
                "so_linked_pos",
                "po_custom_field_present",
                "is_ignored",
                "linked_pos__po_number_id",
                "opportunity_crm_id__ticket_crm_id",
                "opportunity_crm_id__summary",
                "opportunity_crm_id__opportunity_name",
            )
            # .annotate(
            #     opportunity_crm_ids=Case(
            #         *[
            #             When(pk=opportunity_crm_id, then=opportunity_crm_id)
            #             for opportunity_crm_id, opportunity_crm_id in service_ticket_records
            #         ],
            #         default=None,
            #         output_field=IntegerField(),
            #     ),
            #     purchase_order_crm_ids=ArrayAgg(
            #         "linked_pos__crm_id", distinct=True
            #     ),
            # )
        )
        return linked_records


class TicketOpportunitySOLinkingAPIView(generics.GenericAPIView):
    """
    API to link service ticket with opportunity & So.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = ServiceTicketToSoLinkingSeriaizer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            provider_id: int = provider.id
            payload = serializer.validated_data
            payload["provider_id"] = provider_id
            owner_crm_id: int = serializer.validated_data.get("owner_id")
            payload["owner_id"] = owner_crm_id
            if owner_crm_id:
                owner = ConnectwiseServiceTicketPayloadHandler.get_provider_user_by_system_member_crm_id(
                    owner_crm_id
                )
                if owner:
                    payload["owner"] = owner

            # fetch ticket notes
            ticket_notes = provider.erp_client.get_service_ticket_notes(
                ticket_id=payload.get("ticket_crm_id")
            )
            if ticket_notes:
                payload["last_ticket_note"] = ticket_notes.get("text")

            update_service_ticket = (
                provider.erp_client.update_service_ticket_status(
                    ticket_id=payload.get("ticket_crm_id"),
                    opportunity={
                        "id": payload.get("opportunity_crm_id"),
                        "name": payload.get("opportunity_name"),
                    },
                )
            )
            ticket_exists = ConnectwiseServiceTicket.objects.filter(
                ticket_crm_id=payload.get("ticket_crm_id")
            )
            if ticket_exists:
                try:
                    ticket_exists.update(
                        opportunity_crm_id=payload.get("opportunity_crm_id"),
                        opportunity_name=payload.get("opportunity_name"),
                    )
                    sales_order_crm_id = payload.pop("sales_order_crm_id")
                except IntegrityError:
                    logger.debug(
                        "Ticket crm Id: {0} already exists".format(
                            payload.get("ticket_crm_id")
                        )
                    )
                    return Response(
                        {
                            "Opportunity is already linked to another service ticket "
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            else:
                sales_order_crm_id = payload.pop("sales_order_crm_id")
                result = ConnectwiseServiceTicket.objects.create(**payload)
                # update data in sales order table
            sales_order_data = SalesPurchaseOrderData.objects.filter(
                crm_id=sales_order_crm_id
            )
            sales_order_data.update(
                opportunity_crm_id_id=payload.get("opportunity_crm_id"),
                opportunity_name=payload.get("opportunity_name"),
            )
            return Response(status=status.HTTP_201_CREATED)


class UnmappedServiceTicketsFromSoListAPIView(generics.ListAPIView):
    """
    API View to list service tickets as per configured
    in board and created date sync after in Operation Dashboard Settings.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        try:
            dashboard_settings: OperationDashboardSettings = (
                OperationDashboardSettings.objects.get(provider=provider)
            )
        except OperationDashboardSettings.DoesNotExist as e:
            logger.info(
                f"Provider required Operations Dashboard settings not configured.{e}"
            )
            return None
        service_tickets: List[
            Dict
        ] = provider.erp_client.get_service_tickets_by_board_and_status(
            boards=dashboard_settings.service_boards,
            created_date=dashboard_settings.service_ticket_create_date,
        )

        mapped_opportunities = SalesPurchaseOrderData.objects.filter(
            provider=provider, opportunity_crm_id_id__isnull=False
        ).values_list("opportunity_crm_id_id", flat=True)
        # removed mapped tickets with opportunity
        mapped_service_tickets = []
        for ticket in service_tickets:
            if ticket.get("opportunity_id") not in mapped_opportunities:
                mapped_service_tickets.append(ticket)

        return Response(data=mapped_service_tickets, status=status.HTTP_200_OK)


class UnmappedSosFromServiceTicketsListAPIView(generics.ListAPIView):
    """
    API View to list Sos which are not linked to any ticket/opportunity.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider

        unmapped_sos_with_tickets = SalesPurchaseOrderData.objects.filter(
            provider=provider, opportunity_crm_id_id__isnull=True
        ).values_list("crm_id", flat=True)

        return Response(
            data={"unlinked_sales_orders": unmapped_sos_with_tickets},
            status=status.HTTP_200_OK,
        )


class MappedPosWithSoListAPIView(generics.ListAPIView):
    """
    API View to list Sos which are not linked to any ticket/opportunity.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        sales_order_crm_id: int = kwargs.get("sales_order_crm_id")

        mapped_pos_with_so = ConnectwisePurchaseOrderData.objects.filter(
            provider=provider, sales_order__crm_id=sales_order_crm_id
        ).values(
            "crm_id",
            PO_number=F("po_number_id"),
        )

        return Response(
            data=mapped_pos_with_so,
            status=status.HTTP_200_OK,
        )


class SalesOrderToPurchaseOrderLinkDeleteAPIView(
    SalesOrderToPurchaseOrderLinkingMixin, generics.UpdateAPIView
):
    """
    API view to delete purchase orders to sales order link
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = SalesOrderToPurchaseOrderLinkDeleteSerializer

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            provider_id: int = provider.id
            sales_order_crm_id: int = serializer.validated_data.get(
                "sales_order_crm_id"
            )
            remove_link_for_po_ids: List[int] = serializer.validated_data.get(
                "remove_link_for_po_ids"
            )
            po_custom_field_id: int = self.get_purchase_order_custom_field_id()
            sales_order: SalesPurchaseOrderData = self.get_sales_order(
                sales_order_crm_id
            )
            # Check for link in DB first
            linked_po_ids_in_db: Set[int] = set(
                ConnectwisePurchaseOrderData.objects.filter(
                    provider_id=provider_id, sales_order_id=sales_order.id
                ).values_list("crm_id", flat=True)
            )
            delete_link_for_po_ids: List[int] = []
            for crm_id in remove_link_for_po_ids:
                if crm_id in linked_po_ids_in_db:
                    delete_link_for_po_ids.append(crm_id)
            if not delete_link_for_po_ids:
                raise NotFound(
                    "The given purchase orders not linked to the sales order!"
                )
            dashboard_service: OperationsDashboardService = (
                OperationsDashboardService(provider)
            )
            result: Dict = (
                dashboard_service.remove_purchase_order_link_from_sales_order(
                    tuple(delete_link_for_po_ids),
                    sales_order,
                    po_custom_field_id,
                )
            )
            if result.get("purchase_order_link_remove_errors", []):
                return Response(
                    data=result, status=status.HTTP_400_BAD_REQUEST
                )
            return Response(data=result, status=status.HTTP_200_OK)


class IgnoreSalesOrdersAPIView(generics.UpdateAPIView):
    serializer_class = IgnoreSalesOrdersSerializer

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            queryset = (
                SalesPurchaseOrderData.objects.filter(
                    provider=self.request.provider,
                    crm_id__in=request.data.get("sales_order_crm_ids"),
                )
                .values("crm_id", "is_ignored")
                .order_by("crm_id")
            ).update(is_ignored=True)
            return Response(status=status.HTTP_200_OK)


class IgnoreSalesOrdersListAPIView(generics.ListAPIView):
    serializer_class = OpportunityTicketSOPOLinkingSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = (
        "crm_id",
        "opportunity_name",
        "linked_pos__po_number__po_number",
        "opportunity_crm_id__summary",
    )
    ordering_fields = ("crm_id", "linked_pos__po_number__po_number")

    def get_queryset(self):
        linked_records = SalesPurchaseOrderData.objects.filter(
            provider_id=self.request.provider.id,
            is_ignored=True,
        ).values(
            "id",
            "crm_id",
            "opportunity_crm_id",
            "so_linked_pos",
            "po_custom_field_present",
            "is_ignored",
            "linked_pos__po_number_id",
            "opportunity_crm_id__ticket_crm_id",
            "opportunity_crm_id__summary",
            "opportunity_crm_id__opportunity_name",
        )
        return linked_records


class RevokeIgnoreSalesOrdersAPIView(generics.UpdateAPIView):
    serializer_class = IgnoreSalesOrdersSerializer

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            queryset = (
                SalesPurchaseOrderData.objects.filter(
                    provider=self.request.provider,
                    crm_id__in=request.data.get("sales_order_crm_ids"),
                )
                .values("crm_id", "is_ignored")
                .order_by("crm_id")
            ).update(is_ignored=False)
            return Response(status=status.HTTP_200_OK)
