from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from document.models import SOWCategory, ServiceCategory, ServiceTechnologyType
from document.serializers import (
    SOWCategorySerializer,
    SOWCategoryUpdateSerializer,
)


class ServiceCategoryListCreateAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        get_ids_flag = request.query_params.get("get_ids", False)
        get_all_flag = request.query_params.get("get_all", False)

        # by default return only active categories
        if not get_all_flag:
            queryset = queryset.filter(is_disabled=False)

        if get_ids_flag:
            queryset = queryset.values("id", "name", "is_disabled")
        else:
            queryset = queryset.values_list("name", flat=True)

        return Response(data=queryset)

    def get_queryset(self):
        # Filter by is_disabled otherwise creation of new category names
        # having the same name as disabled categories is prevented
        queryset = ServiceCategory.objects.filter(
            provider=self.request.provider, is_disabled=False
        ).order_by("is_disabled", "-created_on")
        return queryset

    def post(self, request, **kwargs):
        categories = request.data
        existing_categories = self.get_queryset().values_list(
            "name", flat=True
        )
        new_categories = set(categories) - set(existing_categories)
        deleted_categories = set(existing_categories) - set(categories)
        # create new categories
        for category in new_categories:
            ServiceCategory.objects.create(
                name=category, provider=request.provider
            )
        # delete
        if deleted_categories:
            ServiceCategory.objects.filter(
                provider=request.provider, name__in=deleted_categories
            ).update(is_disabled=True)
        return Response(status=status.HTTP_200_OK)


class SOWCategoryListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = SOWCategorySerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get_queryset(self):
        queryset = (
            SOWCategory.objects.all()
            .prefetch_related("technology_types")
            .order_by("name")
        )
        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"queryset": self.get_queryset()})
        return context


class SOWCategoryRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = SOWCategoryUpdateSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    queryset = SOWCategory.objects.all()

    def perform_update(self, serializer):
        category_obj = self.get_object()
        provider = self.request.provider
        existing_technology_types = category_obj.technology_types.filter(
            provider=provider, is_disabled=False
        ).values_list("name", flat=True)
        new_technology_types = [
            item["name"]
            for item in serializer.validated_data["technology_types"]
        ]
        for new_type in new_technology_types:
            if new_type not in existing_technology_types:
                ServiceTechnologyType.objects.create(
                    name=new_type, provider=provider, service_type=category_obj
                )
        removed_technology_types = set(existing_technology_types) - set(
            new_technology_types
        )
        ServiceTechnologyType.objects.filter(
            name__in=removed_technology_types
        ).update(is_disabled=True)
