from typing import List, Dict, Any

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateAPIView,
    CreateAPIView,
    RetrieveUpdateDestroyAPIView,
    ListAPIView,
)
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from document.exceptions import (
    FireReportPartnerSiteNotConfigured,
    FireReportSettingNotConfigured,
)
from document.models import (
    FireReportPartnerSite,
    FireReportSetting,
    PartnerSiteSyncError,
)
from document.renderers import FireReportCSVRenderer
from document.serializers import (
    FireReportPartnerSiteCreateUpdateSerializer,
    FireReportPartnerSiteSerializer,
    FireReportCreateSerializer,
    FireReportSettingSerializer,
    PartnerSiteSyncErrorSerializer,
)
from document.services.fire_report import FireReportGenerator
from document.tasks.fire_report import (
    create_partner_site_for_all_customers,
    update_partner_site_for_cw_customers,
    delete_partner_site_for_cw_customers,
)
from django.db.models.expressions import F
from document.custom_filters import PartnerSiteSyncErrorFilterset


class FireReportPartnerSiteListCreateAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_queryset(self):
        return FireReportPartnerSite.objects.filter(
            provider_id=self.request.provider.id
        )

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            partner_site: FireReportPartnerSite = (
                FireReportPartnerSite.objects.create(
                    provider=self.request.provider, **serializer.validated_data
                )
            )
            task = create_partner_site_for_all_customers.apply_async(
                (self.request.provider.id, partner_site.id), queue="high"
            )
            data: Dict[str, Any] = FireReportPartnerSiteSerializer(
                partner_site
            ).data
            data.update(task_id=task.id)
            return Response(
                data=data,
                status=status.HTTP_201_CREATED,
            )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return FireReportPartnerSiteCreateUpdateSerializer
        else:
            return FireReportPartnerSiteSerializer


class FireReportPartnerSitesRetrieveUpdateDestroyAPIView(
    RetrieveUpdateDestroyAPIView
):
    """
    API view to retrieve, udpate and delete partner sites.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_object(self):
        partner_site: FireReportPartnerSite = get_object_or_404(
            FireReportPartnerSite.objects.filter(
                provider_id=self.request.provider.id
            ),
            id=self.kwargs.get("id"),
        )
        return partner_site

    def perform_update(self, serializer):
        serializer.save(provider=self.request.provider)

    def update(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        instance: FireReportPartnerSite = self.get_object()
        site_name_before_update: str = instance.name
        serializer = self.get_serializer(instance, data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            self.perform_update(serializer)
            task = update_partner_site_for_cw_customers.apply_async(
                (provider.id, instance.id, site_name_before_update),
                queue="high",
            )
            data: Dict = serializer.data
            data.update(task_id=task.id)
            return Response(data=data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        site: FireReportPartnerSite = self.get_object()
        site_name: str = site.name
        site.delete()
        task = delete_partner_site_for_cw_customers.apply_async(
            (self.request.provider.id, site_name), queue="high"
        )
        return Response(
            data=dict(task_id=task.id), status=status.HTTP_202_ACCEPTED
        )

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return FireReportPartnerSiteCreateUpdateSerializer
        else:
            return FireReportPartnerSiteSerializer


class FireReportSettingCreateRetriveUpdateAPIView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve, udpate FIRE report setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = FireReportSettingSerializer

    def get_object(self) -> FireReportSetting:
        try:
            fire_report_setting: FireReportSetting = (
                self.request.provider.fire_report_setting
            )
        except FireReportSetting.DoesNotExist:
            raise NotFound(
                "FIRE Report Setting not configured for the Provider!"
            )
        return fire_report_setting

    def perform_create(self, serializer):
        try:
            self.request.provider.fire_report_setting
        except FireReportSetting.DoesNotExist:
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                "FIRE report setting already exists for the Provider!"
            )


class FireReportGenerationAPIView(CreateAPIView):
    """
    API View to generate FIRE report.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    serializer_class = FireReportCreateSerializer
    renderer_classes = (FireReportCSVRenderer,)

    @staticmethod
    def _get_filter_conditions(data: Dict) -> Dict[str, Any]:
        filter_conditions: Dict[str, Any] = dict()
        if data.get("site_crm_ids") is not None:
            filter_conditions.update(
                shipping_site_crm_ids_list=data.get("site_crm_ids")
            )
        if data.get("start_date") is not None:
            filter_conditions.update(
                sales_order_updated_after_date=data.get("start_date")
            )
        if data.get("end_date") is not None:
            filter_conditions.update(
                sales_order_updated_before_date=data.get("end_date")
            )
        return filter_conditions

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            provider: Provider = self.request.provider
            try:
                provider.fire_report_partner_sites
            except ObjectDoesNotExist:
                raise FireReportPartnerSiteNotConfigured()
            try:
                provider.fire_report_setting
            except FireReportSetting.DoesNotExist:
                raise FireReportSettingNotConfigured()
            filter_conditions: Dict[str, Any] = self._get_filter_conditions(
                serializer.validated_data
            )
            fire_report_generator: FireReportGenerator = FireReportGenerator(
                provider=provider
            )
            fire_report_data: List[
                Dict[str, Any]
            ] = fire_report_generator.get_fire_report_data(filter_conditions)
            if fire_report_data:
                headers: Dict[str, str] = {
                    "Content-Disposition": "attachment; filename=FIRE Report.csv"
                }
                return Response(
                    data=fire_report_data,
                    status=status.HTTP_200_OK,
                    headers=headers,
                )
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)


class PartnerSiteSyncErrorListAPIView(ListAPIView):
    """
    API view to list partner site sync errors.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = ("customer_name", "site_name")
    ordering_fields = ("customer_name", "site_name")
    filterset_class = PartnerSiteSyncErrorFilterset
    serializer_class = PartnerSiteSyncErrorSerializer

    def get_queryset(self):
        queryset: "QuerySet[PartnerSiteSyncError]" = (
            PartnerSiteSyncError.objects.select_related("customer")
            .filter(provider_id=self.request.provider.id)
            .annotate(customer_name=F("customer__name"))
        )
        return queryset
