from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework.exceptions import ValidationError

from document.custom_filters import CustomerNotesFilterSet
from document.models import CustomerNotes
from document.serializers import CustomerNotesSerializer


class AdvancedCustomerNotesListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = CustomerNotesSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "body",
        "last_updated_by__first_name",
        "last_updated_by__last_name",
    ]
    ordering_fields = ["created_on", "updated_on"]
    filterset_class = CustomerNotesFilterSet

    def get_queryset(self):
        provider = self.request.provider
        customer_id = self.kwargs.get("customer_id")
        query_params = dict(provider=provider, is_deleted=False)
        if customer_id:
            query_params.update({"customer_id": customer_id})
        queryset = CustomerNotes.objects.filter(**query_params).order_by(
            "-updated_on"
        )
        return queryset

    def perform_create(self, serializer):
        provider = self.request.provider
        customer_id = self.kwargs.get("customer_id")
        if not customer_id:
            customer_id = self.request.data.get("customer")
        if not customer_id:
            raise ValidationError("Customer Id is required.")
        last_updated_by = self.request.user
        serializer.save(
            provider=provider,
            customer_id=customer_id,
            last_updated_by=last_updated_by,
        )


class AdvancedCustomerNotesRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):

    serializer_class = CustomerNotesSerializer
    lookup_url_kwarg = "pk"

    def get_queryset(self):
        provider = self.request.provider
        customer_id = self.kwargs.get("customer_id")
        query_params = dict(provider=provider, is_deleted=False)
        if customer_id:
            query_params.update({"customer_id": customer_id})
        queryset = CustomerNotes.objects.filter(**query_params).order_by(
            "-updated_on"
        )
        return queryset

    def perform_update(self, serializer):
        customer_id = self.kwargs.get("customer_id")
        if not customer_id:
            customer_id = self.request.data.get("customer")
        if not customer_id:
            raise ValidationError("Customer Id is required.")
        serializer.save(customer_id=customer_id)

    def perform_destroy(self, instance):
        instance.is_deleted = True
        instance.save()
