from typing import List

from django.http import Http404
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from core.exceptions import IngramAPISettingNotConfigured
from core.integrations.order_processing.ingram import ingram
from document.models import IngramAPISetting
from document.serializers import IngramAPISettingSerializer
from document.services import OrderTrackingReceivingService


class IngramAPISettingRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = IngramAPISettingSerializer
    queryset = IngramAPISetting.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except Http404:
            raise IngramAPISettingNotConfigured()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {"provider_id": self.request.provider.id}
        obj = get_object_or_404(queryset, **filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        try:
            instance = self.get_object()
        except Http404:
            instance = None
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save(provider_id=self.request.provider.id)


class ValidateIngramAPICredentials(APIView):
    def post(self, request, *args, **kwargs):
        payload = request.data
        client_id = payload.get("client_id")
        client_secret = payload.get("client_secret")
        customer_number = payload.get("customer_number")
        if not all([client_id, client_secret, customer_number]):
            raise ValidationError(
                "Client Id, Client Secret and Customer Number are required."
            )
        is_valid = ingram.verify_credentials(
            client_id, client_secret, customer_number
        )
        result = dict(is_valid=is_valid)
        return Response(data=result, status=status.HTTP_200_OK)


class PurchaseOrderIngramAPIDataView(APIView):
    def post(self, request, *args, **kwargs):
        try:
            request.provider.ingram_api_settings
        except IngramAPISetting.DoesNotExist:
            raise IngramAPISettingNotConfigured()
        purchase_orders: List = request.data.get("purchase_orders")
        line_item_identifiers: List = request.data.get("line_item_identifiers")
        purchase_order_id: List = request.data.get("purchase_order_id")
        if not purchase_orders:
            raise ValidationError(
                {"purchase_orders": ["Missing data for required field."]}
            )
        if not line_item_identifiers:
            raise ValidationError(
                {"line_item_identifiers": ["Missing data for required field."]}
            )
        ingram_orders_data = OrderTrackingReceivingService.get_ingram_api_data(
            request.provider,
            purchase_orders,
            line_item_identifiers,
            purchase_order_id,
        )
        return Response(data=ingram_orders_data, status=status.HTTP_200_OK)
