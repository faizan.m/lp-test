from typing import Dict, List, Optional, Tuple

from django.db.models import QuerySet
from django.http import Http404
from rest_framework import generics, status
from rest_framework.exceptions import NotFound
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from core import utils
from core.exceptions import (
    IngramAPISettingNotConfigured,
    OperationEmailTemplateSettingsNotConfigured,
)
from document.models import (
    IngramAPISetting,
    OperationEmailTemplateSettings,
    SalesPurchaseOrderData,
    ConnectwisePurchaseOrderData,
)
from document.serializers import OperationEmailTemplateSettingsSerializer
from document.services import EmailTemplateGenerationService
from document.exceptions import (
    ServiceTicketBoardAndStatusMappingNotConfigured,
    OpportunityStatusesNotConfigured,
    OpportunityStagesNotConfigured,
)


class GenerateEmailTemplate(APIView):
    def post(self, request, *args, **kwargs):
        try:
            request.provider.ingram_api_settings
        except IngramAPISetting.DoesNotExist:
            raise IngramAPISettingNotConfigured()
        po_numbers = request.data.get("po_numbers")
        po_ids = request.data.get("po_ids")
        customer_contact = request.data.get("customer_contact")
        to_contact_id = request.data.get("to_contact_id")
        customer_id = request.data.get("customer_id")
        if not customer_id:
            raise ValidationError("Customer ID is required.")
        if not po_numbers:
            raise ValidationError("PO Number is required.")
        if not customer_contact:
            raise ValidationError("Customer contact is required.")
        customer_contact_first_name = customer_contact.split(" ")[0]
        template_html, template_data = EmailTemplateGenerationService.process(
            request.provider,
            po_numbers,
            po_ids,
            customer_contact_first_name,
            customer_id,
            to_contact_id,
        )
        if not template_data:
            return Response(
                data={"No Orders Found"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        table_csvs = EmailTemplateGenerationService.extract_tables_to_csv(
            template_html
        )
        return Response(
            data=dict(
                template_data=template_data,
                template_html=template_html,
                table_csvs=table_csvs,
            ),
            status=status.HTTP_200_OK,
        )


class OperationEmailTemplatePurchaseOrderListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            request.provider.operation_email_template_settings
        except OperationEmailTemplateSettings.DoesNotExist:
            raise OperationEmailTemplateSettingsNotConfigured()
        status_ids = (
            request.provider.operation_email_template_settings.purchase_order_status_ids
        )
        purchase_orders = request.provider.erp_client.get_open_purchase_order_list_for_customer(
            status_ids=status_ids
        )
        return Response(data=purchase_orders, status=status.HTTP_200_OK)


class OperationEmailTemplateSettingsAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = OperationEmailTemplateSettingsSerializer
    queryset = OperationEmailTemplateSettings.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except Http404:
            raise OperationEmailTemplateSettingsNotConfigured()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {"provider_id": self.request.provider.id}
        obj = get_object_or_404(queryset, **filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        try:
            instance = self.get_object()
        except Http404:
            instance = None
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save(provider_id=self.request.provider.id)


class SalesOrderEmailTemplateGenerationAPIView(CreateAPIView):
    """
    API View to generate email templates in operations as per SO products
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def post(self, request, *args, **kwargs):
        provider: Provider = request.provider
        opportunity_crm_id: int = request.data.get("opportunity_crm_id", None)
        if opportunity_crm_id is None:
            raise ValidationError("Opportunity ID is required!")
        try:
            sales_order: SalesPurchaseOrderData = (
                SalesPurchaseOrderData.objects.get(
                    provider_id=provider.id,
                    opportunity_crm_id_id=opportunity_crm_id,
                )
            )
        except SalesPurchaseOrderData.DoesNotExist:
            return Response(
                data=dict(
                    detail="Sales order not found for the opportunity!",
                    opportunity_crm_id=opportunity_crm_id,
                ),
                status=status.HTTP_404_NOT_FOUND,
            )
        purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            ConnectwisePurchaseOrderData.objects.filter(
                sales_order_id=sales_order.id, provider_id=provider.id
            )
        )
        if purchase_orders.count() == 0:
            return Response(
                data=dict(
                    detail=sales_order.po_custom_field_present.get(
                        "po_custom_field_present", {}
                    ).get(
                        "reason",
                        "Linked purchase orders not found for the sales order!",
                    ),
                    sales_order_crm_id=sales_order.crm_id,
                    opportunity_crm_id=sales_order.opportunity_crm_id_id,
                    opportunity_name=sales_order.opportunity_name,
                ),
                status=status.HTTP_404_NOT_FOUND,
            )
        else:
            operation_email_template_settings: OperationEmailTemplateSettings = (
                provider.operation_email_template_settings
            )
            (
                products_data,
                table_csvs,
            ) = EmailTemplateGenerationService.get_sales_order_email_template_data(
                sales_order, purchase_orders, operation_email_template_settings
            )
            return Response(
                data=dict(
                    products_data=products_data,
                    table_csvs=table_csvs,
                ),
                status=status.HTTP_200_OK,
            )


class ServiceTicketsListAPIView(generics.ListAPIView):
    """
    API View to list service tickets as per configured
    board and ticket statuses mapping in Operation Email Template Settings.
    Used in CW email template generation.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    @staticmethod
    def get_board_and_ticket_status_ids(
        ticket_board_and_status_mapping: List[Dict],
    ) -> Tuple[List[int], List[int]]:
        """
        Get baord ids and service ticket status ids from mapping

        Args:
            ticket_board_and_status_mapping: Board and ticket status mapping for Provider

        Returns:
            List of board ids, list of service ticket status ids
        """
        board_ids: List[Optional[int]] = []
        ticket_status_ids: List[Optional[int]] = []
        # Service ticket statuses differ depending on the board
        for board_mapping in ticket_board_and_status_mapping:
            board_id: int = board_mapping.get("board_id", None)
            if board_id:
                service_ticket_status_ids: List[int] = [
                    ticket_status.get("ticket_status_id", None)
                    for ticket_status in board_mapping.get(
                        "ticket_statuses", []
                    )
                ]
                board_ids.append(board_id)
                ticket_status_ids.extend(service_ticket_status_ids)
        return board_ids, ticket_status_ids

    @staticmethod
    def get_service_ticket_board_and_status_mapping(
        provider: Provider,
    ) -> List[Dict]:
        """
        Get board and service ticket status mapping for provider from operation_email_template_settings

        Args:
            provider: Provider

        Returns:
            Board and service ticket status mapping
        """
        try:
            operation_email_template_settings: OperationEmailTemplateSettings = (
                provider.operation_email_template_settings
            )
        except OperationEmailTemplateSettings.DoesNotExist:
            raise OperationEmailTemplateSettingsNotConfigured()
        service_ticket_board_and_status_mapping: List[
            Dict
        ] = operation_email_template_settings.ticket_board_and_status_mapping
        if not service_ticket_board_and_status_mapping:
            raise ServiceTicketBoardAndStatusMappingNotConfigured()
        return service_ticket_board_and_status_mapping

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        service_ticket_board_and_status_mapping: List[
            Dict
        ] = self.get_service_ticket_board_and_status_mapping(provider)
        (board_ids, ticket_status_ids,) = self.get_board_and_ticket_status_ids(
            service_ticket_board_and_status_mapping
        )
        service_tickets: List[
            Dict
        ] = provider.erp_client.get_service_tickets_by_board_and_status(
            boards=board_ids, statuses=ticket_status_ids
        )
        return Response(data=service_tickets, status=status.HTTP_200_OK)


class CustomerOpportunitiesListAPIView(generics.ListAPIView):
    """
    API view to list customer opportunities as per configured statuses and stages
    in OperationEmailTemplateSettings.
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    @staticmethod
    def get_configured_opportunity_status_ids(
        operation_email_template_settings: OperationEmailTemplateSettings,
    ) -> List[int]:
        """
        Get opportunity status ids as configured in OperationEmailTemplateSettings

        Args:
            operation_email_template_settings: OperationEmailTemplateSettings

        Returns:
            List of opportunity status ids
        """
        opportunity_statuses: List[
            Optional[Dict]
        ] = operation_email_template_settings.opportunity_statuses
        if not opportunity_statuses:
            raise OpportunityStatusesNotConfigured()
        opportunity_status_ids: List[int] = [
            opportunity_status.get("status_id")
            for opportunity_status in opportunity_statuses
        ]
        return opportunity_status_ids

    @staticmethod
    def get_configured_opportunity_stage_ids(
        operation_email_template_settings: OperationEmailTemplateSettings,
    ) -> List[int]:
        """
        Get opportunity stage ids as configured in OperationEmailTemplateSettings

        Args:
            operation_email_template_settings: OperationEmailTemplateSettings

        Returns:
            List of opportunity stage ids
        """
        opportunity_stages: List[
            Optional[Dict]
        ] = operation_email_template_settings.opportunity_stages
        if not opportunity_stages:
            raise OpportunityStagesNotConfigured()
        opportunity_stage_ids: List[int] = [
            opportunity_stage.get("stage_id")
            for opportunity_stage in opportunity_stages
        ]
        return opportunity_stage_ids

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        customer_crm_id: int = kwargs.get("customer_crm_id")
        try:
            operation_email_template_settings: OperationEmailTemplateSettings = (
                provider.operation_email_template_settings
            )
        except OperationEmailTemplateSettings.DoesNotExist:
            raise OperationEmailTemplateSettingsNotConfigured()
        opportunity_status_ids: List[
            int
        ] = self.get_configured_opportunity_status_ids(
            operation_email_template_settings
        )
        opportunity_stage_ids: List[
            int
        ] = self.get_configured_opportunity_stage_ids(
            operation_email_template_settings
        )
        customer_opportunities = provider.erp_client.get_company_quotes(
            customer_crm_id,
            opportunity_status_ids,
            opportunity_stage_ids,
            business_unit_id=None,
        )
        return Response(data=customer_opportunities, status=status.HTTP_200_OK)
