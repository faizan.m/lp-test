import json
from typing import List, Dict
from rest_framework import status, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import (
    IsProviderOrCustomerUser,
    IsProviderUser,
)
from core.utils import get_customer_id
from document.models import SOWDocument
from document.serializers import QuoteSerializer, QuoteUpdateSerializer
from document.services import QuoteService
from accounts.models import Provider


class QuoteTypesAPIView(APIView):
    """
    API View to list quotes types.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request):
        data = QuoteService.get_quote_types(self.request.provider.id)
        return Response(data=data, status=status.HTTP_200_OK)


class QuoteStagesAPIView(APIView):
    """
    API View to list quotes stages.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request):
        data = QuoteService.get_quote_stages(self.request.provider.id)
        return Response(data=data, status=status.HTTP_200_OK)


class QuoteStatusAPIView(APIView):
    """
    API View to list quotes statuses.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request):
        data = QuoteService.get_quote_statuses(self.request.provider)
        return Response(data=data, status=status.HTTP_200_OK)


class QuoteBusinessUnitAPIView(APIView):
    """
    API View to list quotes business units.
    """

    permission_classes = (IsAuthenticated, IsProviderOrCustomerUser)

    def get(self, request):
        data = QuoteService.get_quote_business_units(self.request.provider)
        return Response(data=data, status=status.HTTP_200_OK)


class QuoteRetrieveUpdateAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, **kwargs):
        quote_id = kwargs.get("quote_id")
        data = QuoteService.get_quote(request.provider.id, quote_id)
        return Response(data=data, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):
        serializer = QuoteUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        quote_id = kwargs.get("quote_id")
        data = QuoteService.update_quote_stage(
            request.provider, quote_id, serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_200_OK)


class QuoteListCreateAPIView(APIView):
    def post(self, request, **kwargs):
        serializer = QuoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = QuoteService.create_quote(
            request.provider.id, serializer.validated_data
        )
        return Response(data=data, status=status.HTTP_201_CREATED)

    def get(self, request, **kwargs):
        customer_id = get_customer_id(request, **kwargs)
        open_only = self.request.query_params.get("open-only", "false")
        won_only = json.loads(
            self.request.query_params.get("won-only", "false")
        )
        params = dict(won_only=won_only)
        data = QuoteService.get_quotes(
            request.provider.id, customer_id, open_only=open_only, **params
        )
        if open_only == "true":
            used_quotes = SOWDocument.objects.all().values_list(
                "quote_id", flat=True
            )
            data = list(filter(lambda x: x.get("id") not in used_quotes, data))
        return Response(data=data, status=status.HTTP_200_OK)


class AllQuoteStagesListAPIView(generics.ListAPIView):
    """
    API view to list all CW opportunity stages
    """

    permission_classes = [IsAuthenticated, IsProviderUser]
    pagination_class = None

    def get(self, request, *args, **kwargs):
        provider: Provider = self.request.provider
        quote_stages: List[Dict] = provider.erp_client.get_opportunity_stages()
        return Response(data=quote_stages, status=status.HTTP_200_OK)
