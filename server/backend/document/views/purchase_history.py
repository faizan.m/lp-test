from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from core.utils import get_customer_cw_id
from document.services.purchase_history import PurchaseOrderHistoryService
from core.renderers import DictToCSVRenderer
from document.serializers import PurchaseOrderHistorySerializer
import asyncio


class OperationPurchaseOrdersHistoryListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        customer_crm_id = get_customer_cw_id(request, **kwargs)
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            PurchaseOrderHistoryService.get_purchase_orders(
                request.provider, customer_crm_id, **kwargs
            )
        )
        line_items = loop.run_until_complete(future)

        return Response(data=line_items, status=status.HTTP_200_OK)


class OperationPurchaseOrderDetailsListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        provider = request.provider
        purchase_order_id = kwargs.get("purchaseorder_id")
        line_item_id = kwargs.get("lineitem_id")
        po_number = request.query_params.get("po_number")
        line_item = PurchaseOrderHistoryService.get_purchase_order_details(
            provider, purchase_order_id, line_item_id, po_number
        )
        return Response(data=line_item, status=status.HTTP_200_OK)


class OperationPurchaseOrdersHistoryExportAPIView(APIView):
    renderer_classes = (DictToCSVRenderer,)

    def post(self, request, *args, **kwargs):
        serializer = PurchaseOrderHistorySerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        customer_id = serializer.validated_data.get("customer_id")
        provider = request.provider
        customer_crm_id = get_customer_cw_id(
            request, customer_id=customer_id, **kwargs
        )
        line_items = PurchaseOrderHistoryService.export_purchase_orders(
            provider, customer_crm_id
        )
        headers = {
            "Content-Disposition": "attachment; filename=purchase-order-history.csv"
        }
        return Response(
            data=line_items, status=status.HTTP_200_OK, headers=headers
        )
