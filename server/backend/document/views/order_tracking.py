from typing import List, Dict, Tuple, Any, Union, Set

from marshmallow import ValidationError as mmValidationError
from rest_framework import status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView
from accounts.custom_permissions import IsProviderUser
from core.exceptions import IngramAPISettingNotConfigured
from document.models import IngramAPISetting, AutomatedWeeklyRecapSetting
from document.serializers import (
    OrderReceivingSerializer,
    OrderReceivingLineItemsSerializer,
    AutomatedWeeklyRecapSettingSerializer,
    WeeklyRecapEmailTriggerDataSerializer,
)
from document.services import OrderTrackingReceivingService
from document.services.order_tracking import WeeklyRecapService
from document.exceptions import AutomatedWeeklyRecapSettingNotConfigured
from document.tasks.customer_order_tracking import (
    send_order_tracking_weekly_recap_email,
)


class OrderReceivingLineItemListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    @staticmethod
    def separate_received_line_items(
        line_items: List[Dict[Any, Any]],
    ) -> Tuple[List[Dict[Any, Any]], List[Dict[Any, Any]]]:
        """
        Method to separate received and not received (receiving) line items
        Args:
            line_items: List containing line items

        Returns:
           Lists containing receiving (not received) and received line items
        """
        line_items_received = []
        line_items_not_received = []
        for line_item in line_items:
            if line_item.get("closedFlag", False):
                line_items_received.append(line_item)
            else:
                line_items_not_received.append(line_item)
        return line_items_not_received, line_items_received

    @staticmethod
    def extract_received_quantity_serial_and_tracking_numbers(
        line_item: Dict[Any, Any]
    ) -> Dict[str, Union[int, List[Union[str, None]]]]:
        """
        Method to prepare data in a format for mapping to product identifier
        Args:
            line_item: Line item dictionary

        Returns:
            List containing data in proper format
            E.g. {"received_quantity": 2, "serial_numbers": ["SR1"], "tracking_numbers": ["TR1"]}
        """
        already_received_quantity = line_item.get("receivedQuantity", 0)
        serial_numbers = line_item.get("serialNumbers", "")
        tracking_numbers = line_item.get("trackingNumber", "")

        serial_numbers_list = []
        tracking_numbers_list = []
        if serial_numbers:
            serial_numbers_list = [
                serial_number.strip()
                for serial_number in serial_numbers.split(",")
            ]
        if tracking_numbers:
            tracking_numbers_list = [
                tracking_number.strip()
                for tracking_number in tracking_numbers.split(",")
            ]

        data_to_map = dict(
            received_quantity=already_received_quantity,
            serial_numbers=serial_numbers_list,
            tracking_numbers=tracking_numbers_list,
        )
        return data_to_map

    def get_product_id_to_partially_received_data_mapping(
        self,
        line_items_received: List[Dict[Any, Any]],
    ) -> Dict[str, Dict[str, Union[int, List[Union[str, None]]]]]:
        """
        Method to map product identifiers to partially received quantity,
        received serial numbers and received tracking numbers

        Args:
            line_items_received: List of received line items

        Returns:
            Mapping of product identifier to required data
            E.g.{"ABCDXYZ": {"received_quantity":2, "serial_numbers":["SR1"], "tracking_numbers":["TR1"]}, }
        """
        mapping = {}
        for line_item in line_items_received:
            if line_item.get("product", {}).get("identifier", False):
                line_item_product_identifier = line_item.get("product").get(
                    "identifier"
                )
                data_to_map = (
                    self.extract_received_quantity_serial_and_tracking_numbers(
                        line_item
                    )
                )
                # If mapping exists, update data for the identifier else create new record
                if line_item_product_identifier in mapping:
                    mapping[line_item_product_identifier][
                        "received_quantity"
                    ] += data_to_map.get("received_quantity", 0)
                    mapping[line_item_product_identifier][
                        "serial_numbers"
                    ].extend(data_to_map.get("serial_numbers", []))
                    mapping[line_item_product_identifier][
                        "tracking_numbers"
                    ].extend(data_to_map.get("tracking_numbers", []))
                else:
                    mapping[line_item_product_identifier] = data_to_map
        return mapping

    def add_partially_received_data(
        self,
        line_items_received: List[Dict[Any, Any]],
        line_items_not_received: List[Dict[Any, Any]],
    ) -> List[Dict[Any, Any]]:
        """
        Method to add partially received quantity, received serial numbers and
        received tracking numbers to line items that are not fully received (receiving)

        Args:
            line_items_received: List containing received line items
            line_items_not_received: List containing line items not yet received

        Returns:
            List of line items with their partially received data
        """
        # Mapping of product identifiers to partially received quantity,
        # received serial numbers, received tracking numbers
        product_id_to_partially_received_data_mapping = (
            self.get_product_id_to_partially_received_data_mapping(
                line_items_received
            )
        )
        # Update partially received quantity, received serial numbers and
        # received tracking numbers to line items
        for line_item in line_items_not_received:
            line_item_product_identifier = line_item.get("product", {}).get(
                "identifier", -1
            )
            received_line_item_data = (
                product_id_to_partially_received_data_mapping.get(
                    line_item_product_identifier, {}
                )
            )
            # Remove duplicates by creating set
            line_item.update(
                partially_received_quantity=received_line_item_data.get(
                    "received_quantity", 0
                ),
                received_serial_numbers=list(
                    set(received_line_item_data.get("serial_numbers", []))
                ),
                received_tracking_numbers=list(
                    set(received_line_item_data.get("tracking_numbers", []))
                ),
            )
        return line_items_not_received

    def post(self, request, *args, **kwargs):
        try:
            request.provider.ingram_api_settings
        except IngramAPISetting.DoesNotExist:
            raise IngramAPISettingNotConfigured()
        purchase_order_ids: List = request.data.get("purchase_order_ids")
        try:
            OrderReceivingLineItemsSerializer().load(request.data)
        except mmValidationError as err:
            raise ValidationError(err.messages)
        # if not purchase_order_ids:
        #     raise ValidationError(
        #         {"purchase_order_ids": ["purchase_order_ids is required."]}
        #     )
        line_items = (
            OrderTrackingReceivingService.get_purchase_order_line_items(
                request.provider, purchase_order_ids, include_closed=True
            )
        )
        # Separate received and receiving (not received) line items
        (
            line_items_not_received,
            line_items_received,
        ) = self.separate_received_line_items(line_items)
        # Add partially received quantity, received serial numbers and
        # received tracking numbers to receiving (not yet received) line items
        line_items = self.add_partially_received_data(
            line_items_received, line_items_not_received
        )
        return Response(data=line_items, status=status.HTTP_200_OK)


class UpdatePurchaseOrderLineItemAPIView(APIView):
    def post(self, request, *args, **kwargs):
        try:
            payload = OrderReceivingSerializer().load(request.data)
            customer_id = payload.get("customer_id")
        except mmValidationError as err:
            raise ValidationError(err.messages)
        OrderTrackingReceivingService.receive_line_items(
            customer_id, request.provider, request.user, payload
        )
        return Response(status=status.HTTP_200_OK)


class OrderTrackingSettingsAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        client = request.provider.erp_client
        data = client.get_contact_types()
        return Response(data=data, status=status.HTTP_200_OK)


class POLineItemsStatuses(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        erp_client = request.provider.erp_client
        data = erp_client.line_items_status()
        return Response(data=data, status=status.HTTP_200_OK)


class PurchaseOrderDetailsAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        purchase_order_id = kwargs.get("purchaseorder_id")
        erp_client = request.provider.erp_client
        data = erp_client.get_purchase_order_details(purchase_order_id)
        return Response(data=data, status=status.HTTP_200_OK)


class PurchaseOrderLineItemsListAPIView(APIView):
    permission_classes = (IsAuthenticated, IsProviderUser)

    @staticmethod
    def extract_quantity_received_quantity_serial_and_tracking_numbers(
        line_item: Dict[Any, Any]
    ) -> Dict[str, Union[int, List[Union[str, None]]]]:
        """
        Method to prepare data in a format for mapping to product identifier
        Args:
            line_item: Line item dictionary

        Returns:
            List containing data in proper format
            E.g. {"received_quantity": 2, "serial_numbers": ["SR1"], "tracking_numbers": ["TR1"]}
        """
        already_received_quantity = line_item.get("receivedQuantity", 0)
        serial_numbers = line_item.get("serialNumbers", "")
        tracking_numbers = line_item.get("trackingNumber", "")
        quantity = line_item.get("quantity", 0)

        serial_numbers_list = []
        tracking_numbers_list = []
        if serial_numbers:
            serial_numbers_list = [
                serial_number.strip()
                for serial_number in serial_numbers.split(",")
            ]
        if tracking_numbers:
            tracking_numbers_list = [
                tracking_number.strip()
                for tracking_number in tracking_numbers.split(",")
            ]

        data_to_map = dict(
            quantity=quantity,
            received_quantity=already_received_quantity,
            serial_numbers=serial_numbers_list,
            tracking_numbers=tracking_numbers_list,
        )
        return data_to_map

    def get_product_id_to_data_mapping(
        self,
        line_items: List[Dict[Any, Any]],
    ) -> Dict[str, Dict[str, Union[int, List[Union[str, None]]]]]:
        """
        Method to map product identifiers to  quantity, received quantity,
        serial numbers and tracking numbers

        Args:
            line_items: List of all line items

        Returns:
            Mapping of product identifier to required data
            E.g.{"ABCDXYZ": {"quantity":2, "received_quantity":2, "serial_numbers":["SR1"], "tracking_numbers":["TR1"]}, }
        """
        mapping = {}
        for line_item in line_items:
            if line_item.get("product", {}).get("identifier", False):
                line_item_product_identifier = line_item.get("product").get(
                    "identifier"
                )
                data_to_map = self.extract_quantity_received_quantity_serial_and_tracking_numbers(
                    line_item
                )
                # If mapping exists, update data for the identifier else create new record
                if line_item_product_identifier in mapping:
                    mapping[line_item_product_identifier][
                        "quantity"
                    ] += data_to_map.get("quantity", 0)
                    mapping[line_item_product_identifier][
                        "received_quantity"
                    ] += data_to_map.get("received_quantity", 0)
                    mapping[line_item_product_identifier][
                        "serial_numbers"
                    ].extend(data_to_map.get("serial_numbers", []))
                    mapping[line_item_product_identifier][
                        "tracking_numbers"
                    ].extend(data_to_map.get("tracking_numbers", []))
                else:
                    mapping[line_item_product_identifier] = data_to_map
        return mapping

    def add_partially_received_data(
        self,
        line_items: List[Dict[Any, Any]],
    ) -> List[Dict[Any, Any]]:
        """
        Method to add quantity, received quantity, serial numbers and
        tracking numbers to line items that are either received (receiving) or not

        Args:
            line_items: List containing all line items

        Returns:
            List of line items with their data
        """
        # Mapping of product identifiers to quantity, received quantity,
        # serial numbers, tracking numbers
        product_id_to_data_mapping = self.get_product_id_to_data_mapping(
            line_items
        )
        identifiers_seen = set()
        unique_line_items = []
        # Update received quantity, serial numbers and
        # tracking numbers to all line items
        for line_item in line_items:
            line_item_product_identifier = line_item.get("product", {}).get(
                "identifier", -1
            )
            received_line_item_data = product_id_to_data_mapping.get(
                line_item_product_identifier, {}
            )
            # check for same product id
            if line_item_product_identifier not in identifiers_seen:
                identifiers_seen.add(line_item_product_identifier)
                # Remove duplicates by creating set
                line_item.update(
                    total_quantity=received_line_item_data.get("quantity", 0),
                    partially_received_quantity=received_line_item_data.get(
                        "received_quantity", 0
                    ),
                    received_serial_numbers=list(
                        set(received_line_item_data.get("serial_numbers", []))
                    ),
                    received_tracking_numbers=list(
                        set(
                            received_line_item_data.get("tracking_numbers", [])
                        )
                    ),
                )
                unique_line_items.append(line_item)
        return unique_line_items

    def post(self, request, *args, **kwargs):
        try:
            request.provider.ingram_api_settings
        except IngramAPISetting.DoesNotExist:
            raise IngramAPISettingNotConfigured()
        purchase_order_ids: List = request.data.get("purchase_order_ids")
        try:
            OrderReceivingLineItemsSerializer().load(request.data)
        except mmValidationError as err:
            raise ValidationError(err.messages)
        line_items = (
            OrderTrackingReceivingService.get_purchase_order_line_items(
                request.provider, purchase_order_ids, include_closed=True
            )
        )

        line_items = self.add_partially_received_data(line_items)
        return Response(data=line_items, status=status.HTTP_200_OK)


class AutomatedWeeklyRecapSettingCreateRetrieveUpdateView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve, update automated weekly recap setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = AutomatedWeeklyRecapSettingSerializer

    def get_object(self) -> AutomatedWeeklyRecapSetting:
        try:
            setting: AutomatedWeeklyRecapSetting = (
                self.request.provider.weekly_recap_setting
            )
        except AutomatedWeeklyRecapSetting.DoesNotExist:
            raise NotFound(
                AutomatedWeeklyRecapSettingNotConfigured.default_detail
            )
        return setting

    def perform_create(self, serializer):
        try:
            self.request.provider.weekly_recap_setting
        except AutomatedWeeklyRecapSetting.DoesNotExist:
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                AutomatedWeeklyRecapSettingNotConfigured.default_detail
            )


class TriggerWeeklyRecapEmailAPIView(CreateAPIView):
    """
    API view to trigger order tracking weekly recap emails for selcted customers.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = WeeklyRecapEmailTriggerDataSerializer

    def post(self, request, *args, **kwargs):
        if not AutomatedWeeklyRecapSetting.is_configured_for_provider(
            self.request.provider
        ):
            raise AutomatedWeeklyRecapSettingNotConfigured()
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            all_customers: bool = serializer.validated_data.get(
                "all_customers"
            )
            customer_crm_ids: List[int] = serializer.validated_data.get(
                "customer_crm_ids"
            )
            task = send_order_tracking_weekly_recap_email.apply_async(
                (
                    self.request.provider.id,
                    all_customers,
                    customer_crm_ids,
                ),
                queue="high",
            )
            return Response(
                data=dict(task_id=task.id), status=status.HTTP_202_ACCEPTED
            )


class WeeklyRecapEmailPreviewAPIView(RetrieveUpdateAPIView):
    """
    API view to preview order tracking weekly recap email.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def get(self, request, *args, **kwargs):
        if not AutomatedWeeklyRecapSetting.is_configured_for_provider(
            self.request.provider
        ):
            raise AutomatedWeeklyRecapSettingNotConfigured()
        weekly_recap_service: WeeklyRecapService = WeeklyRecapService(
            self.request.provider, self.request.provider.weekly_recap_setting
        )
        email_preview: str = weekly_recap_service.generate_email_preview()
        return Response(
            data=dict(html=email_preview), status=status.HTTP_200_OK
        )
