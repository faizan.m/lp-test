from datetime import datetime, timedelta
from typing import List, Dict
from django.db.models import Count, Q, F, QuerySet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.exceptions import NotFound
from rest_framework.exceptions import ValidationError
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import (
    RetrieveUpdateAPIView,
    CreateAPIView,
    ListAPIView,
)
from rest_framework.permissions import IsAuthenticated

from accounts.custom_permissions import IsProviderUser
from document.custom_filters import (
    OpportunitiesChartFilterset,
    OpenPOChartFilterset,
    OpenServiceTicketsChartFilterset,
)
from document.exceptions import Client360DashboardSettingNotConfigured
from document.models import (
    Client360DashboardSetting,
    ConnectwisePurchaseOrderData,
    ConnectwiseServiceTicket,
)
from document.models import ConnectwiseOpportunityData
from document.serializers import (
    Client360DashboardSettingSerializer,
    OpenOpportunitiesChartSerilizer,
    WonOpportuntiesSerializer,
    OpenPurchaseOrdersDataSerializer,
    OpenServiceTicketsSerializer,
)
from core.exceptions import InvalidConfigurations


class Client360DashboardSettingCreateRetrieveUpdateView(
    CreateAPIView, RetrieveUpdateAPIView
):
    """
    API view to create, retrieve & update Client360 Dashboard setting.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = Client360DashboardSettingSerializer

    def get_object(self):
        if Client360DashboardSetting.configured_for_provider(
            self.request.provider
        ):
            return self.request.provider.client360_setting
        else:
            raise NotFound(
                Client360DashboardSettingNotConfigured.default_detail
            )

    def perform_create(self, serializer):
        if not Client360DashboardSetting.configured_for_provider(
            self.request.provider
        ):
            serializer.save(provider=self.request.provider)
        else:
            raise ValidationError(
                "Client360 Dashboard Setting already exists for the Provider!"
            )


class OpenOpportunitiesChartAPIView(ListAPIView):
    """
    API view to list open opportunities count by stages.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    filter_backends = [
        DjangoFilterBackend,
    ]
    filterset_class = OpportunitiesChartFilterset
    serializer_class = OpenOpportunitiesChartSerilizer

    def get_queryset(self) -> "QuerySet[ConnectwiseOpportunityData]":
        if not self.request.provider.integration_statuses[
            "crm_board_mapping_configured"
        ]:
            raise InvalidConfigurations("Board Mapping Not Configured.")
        opportunity_mapping: Dict[
            str, List
        ] = self.request.provider.erp_integration.other_config.get(
            "board_mapping"
        ).get(
            "Opportunities", {}
        )
        if not opportunity_mapping:
            raise InvalidConfigurations("Opportunity mapping not configured!")
        opp_statuses: List[int] = opportunity_mapping.get("status", [])
        opp_stages: List[int] = opportunity_mapping.get("stages", [])
        opportunities: "QuerySet[ConnectwiseOpportunityData]" = (
            ConnectwiseOpportunityData.objects.filter(
                provider=self.request.provider,
                status_crm_id__in=opp_statuses,
                stage_crm_id__in=opp_stages,
            )
            .values("stage_name")
            .annotate(count=Count("stage_name"))
            .order_by("stage_name")
        )
        return opportunities


class WonOpportuntiesChartAPIView(ListAPIView):
    """
    API view to list opportunities won within last 90 days
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = [
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    ]
    search_fields = ("name",)
    ordering_fields = ("name", "closed_date")
    filterset_class = OpportunitiesChartFilterset
    serializer_class = WonOpportuntiesSerializer

    def get_queryset(self):
        if not Client360DashboardSetting.configured_for_provider(
            self.request.provider
        ):
            raise Client360DashboardSettingNotConfigured()
        client360_setting: Client360DashboardSetting = (
            self.request.provider.client360_setting
        )
        now = datetime.now()
        last_90_day = now - timedelta(days=90)
        won_opportunities = (
            ConnectwiseOpportunityData.objects.filter(
                provider=self.request.provider,
                status_crm_id__in=client360_setting.won_opp_status_crm_ids,
                closed_date__range=[last_90_day, now],
            )
            .only(
                "id", "closed_date", "customer_name", "customer_crm_id", "name"
            )
            .order_by("-closed_date")
        )
        return won_opportunities


class OpenPurchaseOrdersChartAPIView(ListAPIView):
    """
    API view to list open purchase orders.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = [
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    ]
    ordering_fields = (
        "opportunity_name",
        "customer_po",
        "vendor_company_name",
    )
    search_fields = (
        "po_number_id",
        "vendor_company_name",
        "customer_po",
        "opportunity_name",
    )
    filterset_class = OpenPOChartFilterset
    serializer_class = OpenPurchaseOrdersDataSerializer

    def get_queryset(self) -> "QuerySet[ConnectwisePurchaseOrderData]":
        if not Client360DashboardSetting.configured_for_provider(
            self.request.provider
        ):
            raise Client360DashboardSettingNotConfigured()
        client360_setting: Client360DashboardSetting = (
            self.request.provider.client360_setting
        )
        open_purchase_orders: "QuerySet[ConnectwisePurchaseOrderData]" = (
            ConnectwisePurchaseOrderData.objects.select_related("sales_order")
            .filter(
                provider=self.request.provider,
                sales_order__isnull=False,
                purchase_order_status_id__in=client360_setting.open_po_status_crm_ids,
            )
            .only(
                "id",
                "po_number_id",
                "vendor_company_name",
                "purchase_order_status_name",
                "sales_order__customer_po_number",
                "sales_order__opportunity_name",
                "customer_company_name",
                "customer_company_id",
            )
            .annotate(
                customer_po=F("sales_order__customer_po_number"),
                opportunity_name=F("sales_order__opportunity_name"),
            )
        )
        return open_purchase_orders


class OpenServiceTicketsChartAPIView(ListAPIView):
    """
    API view to list open service tickets.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = [
        SearchFilter,
        OrderingFilter,
        DjangoFilterBackend,
    ]
    ordering_fields = (
        "ticket_crm_id",
        "status_name",
    )
    search_fields = (
        "summary",
        "ticket_crm_id",
    )
    filterset_class = OpenServiceTicketsChartFilterset
    serializer_class = OpenServiceTicketsSerializer

    def get_queryset(self) -> "QuerySet[ConnectwiseServiceTicket]":
        if not self.request.provider.integration_statuses[
            "crm_board_mapping_configured"
        ]:
            raise InvalidConfigurations("Board Mapping Not Configured!")
        service_board_mappping: Dict = (
            self.request.provider.erp_integration.other_config.get(
                "board_mapping"
            ).get("Service Board Mapping", {})
        )
        open_status_crm_ids: List[int] = list()
        managed_service_open_statuses: List[int] = service_board_mappping.get(
            "Managed Services", {}
        ).get("open_status", [])
        open_status_crm_ids.extend(managed_service_open_statuses)
        professional_service_open_statuses: List[
            int
        ] = service_board_mappping.get("Professional Services", {}).get(
            "open_status", []
        )
        open_status_crm_ids.extend(professional_service_open_statuses)

        open_tickets: "QuerySet[ConnectwiseServiceTicket]" = (
            ConnectwiseServiceTicket.objects.filter(
                provider=self.request.provider,
                status_crm_id__in=open_status_crm_ids,
            )
            .only(
                "id",
                "ticket_crm_id",
                "summary",
                "company_crm_id",
                "company_name",
                "status_name",
                "status_crm_id",
            )
            .order_by("-connectwise_last_data_update")
        )
        return open_tickets
