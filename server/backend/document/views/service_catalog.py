from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated

from accounts.custom_permissions import IsProviderUser
from document.custom_filters import ServiceCatalogFilterSet
from document.models import ServiceCatalog
from document.serializers import (
    ServiceCatalogCreateUpdateSerializer,
    ServiceCatalogListRetrieveSerializer,
)
from utils import DynamicPaginationMixin


class ServiceCatalogListCreateAPIView(
    DynamicPaginationMixin, generics.ListCreateAPIView
):
    serializer_class = ServiceCatalogListRetrieveSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)
    filter_backends = (
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    )
    search_fields = [
        "service_name",
        "service_type__name",
        "service_category__name",
        "service_technology_types__name",
        "notes",
    ]
    ordering_fields = [
        "service_name",
        "service_type__name",
        "service_category__name",
        "created_on",
        "updated_on",
    ]
    filterset_class = ServiceCatalogFilterSet

    def get_queryset(self):
        queryset = (
            ServiceCatalog.objects.filter(
                provider=self.request.provider, is_disabled=False
            )
            .select_related("service_type", "service_category")
            .prefetch_related("service_technology_types")
            .distinct()
            .order_by("-updated_on")
        )
        return queryset

    def get_serializer_class(self):
        if self.request.method in ["GET"]:
            return ServiceCatalogListRetrieveSerializer
        elif self.request.method in ["POST"]:
            return ServiceCatalogCreateUpdateSerializer

    def perform_create(self, serializer):
        provider = self.request.provider
        is_template_present = (
            True if serializer.validated_data["linked_template"] else False
        )
        serializer.save(
            provider=provider, is_template_present=is_template_present
        )


class ServiceCatalogRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = ServiceCatalogCreateUpdateSerializer
    permission_classes = (IsAuthenticated, IsProviderUser)

    def get_object(self):
        try:
            service_catalog_id = self.kwargs.get("pk")
            queryset = (
                ServiceCatalog.objects.select_related(
                    "service_type", "service_category"
                )
                .prefetch_related("service_technology_types")
                .get(id=service_catalog_id, provider=self.request.provider)
            )
            return queryset
        except ServiceCatalog.DoesNotExist:
            raise NotFound("Service Catalog does not exist.")

    def get_serializer_class(self):
        if self.request.method in ["GET"]:
            return ServiceCatalogListRetrieveSerializer
        elif self.request.method in ["PUT", "PATCH"]:
            return ServiceCatalogCreateUpdateSerializer

    def perform_update(self, serializer):
        is_template_present = (
            True if serializer.validated_data.get("linked_template") else False
        )
        serializer.save(is_template_present=is_template_present)
