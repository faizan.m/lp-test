import json
from typing import Dict

import reversion
from django.forms import model_to_dict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, status
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.custom_permissions import IsProviderUser
from accounts.models import Provider
from document.custom_filters import SOWTemplateFilterSet
from document.models import SOWBaseTemplate, SOWTemplate, ServiceCatalog
from document.serializers import (
    SOWTemplateCreateSerializer,
    SOWTemplateSerializer,
    SOWTemplateUpdateSerializer,
    SOWTemplateListSerializer,
    ExistingSOWTemplatePreviewSerializer,
)
from document.services import DocumentService


class SOWBaseTemplateAPIView(APIView):
    def get(self, *args, **kwargs):
        data = {
            "base_config": SOWBaseTemplate.json_config(),
            "change_request_config": SOWBaseTemplate.change_request_config(),
        }
        return Response(data=data, status=status.HTTP_200_OK)


class SOWTemplateListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "category__name",
        "author__first_name",
        "author__last_name",
        "updated_by__first_name",
        "updated_by__last_name",
        "base_template__type",
    ]
    ordering_fields = ["name", "updated_on"]

    filterset_class = SOWTemplateFilterSet

    def get_queryset(self):
        is_disabled = False
        if self.request.query_params.get("is_disabled") == (None or "true"):
            is_disabled = True
        return SOWTemplate.objects.filter(
            provider=self.request.provider, is_disabled=is_disabled
        )

    def perform_create(self, serializer):
        sc_id = serializer.validated_data.pop("linked_service_catalog", None)
        template = serializer.save(
            author=self.request.user,
            updated_by=self.request.user,
            provider=self.request.provider,
        )
        if sc_id.get("id"):
            try:
                sc_object = ServiceCatalog.objects.get(
                    id=sc_id.get("id"),
                    provider=self.request.provider,
                    linked_template_id__isnull=True,
                )
            except ServiceCatalog.DoesNotExist:
                raise ValidationError(
                    {
                        "linked_service_catalog": [
                            "Service Catalog already in use."
                        ]
                    }
                )
            sc_object.linked_template = template
            sc_object.is_template_present = True
            sc_object.save()

    def get_serializer_class(self):
        if self.request.method == "POST":
            return SOWTemplateCreateSerializer
        else:
            return SOWTemplateListSerializer


class SOWTemplateRetrieveUpdateDestroyAPIView(
    generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = (IsAuthenticated, IsProviderUser)
    pagination_class = None

    def get_object(self):
        id = self.kwargs["pk"]
        try:
            return SOWTemplate.objects.filter(
                provider=self.request.provider
            ).get(id=id)
        except SOWTemplate.DoesNotExist:
            raise NotFound("Template does not exist.")

    def perform_update(self, serializer):
        sc_id = serializer.validated_data.pop("linked_service_catalog", None)
        template = self.get_object()
        major_version = template.major_version or 1
        minor_version = template.minor_version or 0
        minor_version += 1
        if serializer.validated_data.get("update_version", False):
            major_version += 1
            minor_version = 0
        template.major_version = major_version
        template.minor_version = minor_version
        with reversion.create_revision():
            template = serializer.save(
                updated_by=self.request.user,
                major_version=major_version,
                minor_version=minor_version,
            )
            # Store some meta-information.
            meta_data = {
                "version": template.version,
                "description": serializer.validated_data.get(
                    "version_description"
                ),
            }
            reversion.set_user(self.request.user)
            reversion.set_comment(json.dumps(meta_data))
        if sc_id.get("id") is None:
            # Remove any previous mapping with this template
            ServiceCatalog.objects.filter(linked_template=template).update(
                linked_template=None, is_template_present=False
            )
        elif (
            template.linked_service_catalog
            and sc_id.get("id") != template.linked_service_catalog.id
        ) or not template.linked_service_catalog:
            try:
                sc_object = ServiceCatalog.objects.get(
                    id=sc_id.get("id"),
                    provider=self.request.provider,
                    linked_template_id__isnull=True,
                )
            except ServiceCatalog.DoesNotExist:
                raise ValidationError(
                    {
                        "linked_service_catalog": [
                            "Service Catalog already in use."
                        ]
                    }
                )
            # Remove any previous mapping with this template
            ServiceCatalog.objects.filter(linked_template=template).update(
                linked_template=None, is_template_present=False
            )
            sc_object.linked_template = template
            sc_object.is_template_present = True
            sc_object.save()

    def destroy(self, request, *args, **kwargs):
        instance_id = self.get_object().id
        SOWTemplate.objects.filter(id=instance_id).update(is_disabled=True)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return SOWTemplateUpdateSerializer
        else:
            return SOWTemplateSerializer


class SOWTemplatePreview(APIView):
    """
    API view to preview SOW templates. Should be used in preview before template creation or update.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)

    def post(self, request, **kwargs):
        template_vars: Dict = request.data.get("template_payload")
        template_type: str = request.data.get("template_type")
        if not all([template_vars, template_type]):
            raise ValidationError(
                "template_payload and template_type are required."
            )
        data: Dict = DocumentService.generate_template_preview(
            request.provider, template_vars, template_type
        )
        return Response(data)


class ExistingSOWTemplatePreview(generics.GenericAPIView):
    """
    API view to preview existing SOW templates.
    """

    permission_classes = (IsAuthenticated, IsProviderUser)
    serializer_class = ExistingSOWTemplatePreviewSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            template_type: str = serializer.validated_data.get("template_type")
            provider: Provider = self.request.provider
            template_id: int = self.kwargs.get("template_id")
            sow_template: SOWTemplate = get_object_or_404(
                SOWTemplate.objects.all(),
                provider_id=provider.id,
                id=template_id,
            )
            template_payload: Dict = model_to_dict(
                sow_template,
                fields=["id", "name", "json_config", "doc_type", "provider"],
            )
            preview: Dict[
                str, str
            ] = DocumentService.generate_template_preview(
                provider, template_payload, template_type
            )
            return Response(data=preview, status=status.HTTP_200_OK)
