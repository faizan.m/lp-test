import os
import shutil
import tempfile
from typing import Dict, List, Union, Any, Optional

import requests
from django.conf import settings
from django.db.models import QuerySet

from accounts.models import Provider
from document.exceptions import SOWQuoteStatusSettingsNotConfigured
from document.exceptions import SOWVendorOnboardingSettingNotConfigured
from document.models import (
    ServiceCategory,
    SOWBaseTemplate,
    SOWDocument,
    SOWVendorOnboardingSetting,
)
from utils import get_app_logger, add_page_break_in_html

logger = get_app_logger(__name__)


def create_default_service_category(provider):
    """
    Add default service categories
    :return: None
    """
    SERVICE_CATEGORIES = ["Deployment", "Assessment", "Upgrade", "POC"]
    for category_name in SERVICE_CATEGORIES:
        ServiceCategory.objects.get_or_create(
            name=category_name,
            provider=provider,
            defaults={"name": category_name, "provider": provider},
        )


def create_default_sow_base_templates(provider):
    types = list(dict(SOWBaseTemplate.SOW_TEMPLATE_CHOICES).keys())
    for each in types:
        SOWBaseTemplate.objects.get_or_create(type=each, provider=provider)


def download_file(file_path: str) -> str:
    """
    Download/copy file to local path
    Args:
        file_path: file location path / url
    """
    try:
        file_name = os.path.basename(file_path)
    except (FileNotFoundError, OSError) as e:
        logger.error(e)
        return None
    local_file_path = os.path.join(tempfile.gettempdir(), file_name)
    if settings.DEBUG:
        shutil.copy(file_path, local_file_path)
    else:
        resp = requests.get(file_path)
        with open(local_file_path, "wb") as output:
            output.write(resp.content)
    return local_file_path


def sow_quote_setting_configured(provider: Provider) -> bool:
    """
    Method to check if sow quote settings are configured for provider

    Args:
        provider: Provider

    Returns:
        True if settings are configured otherwise False
    """
    try:
        open_status_ids = provider.erp_integration.open_status_ids
        won_status_ids = provider.erp_integration.won_status_ids
        lost_status_ids = provider.erp_integration.lost_status_ids
    except SOWQuoteStatusSettingsNotConfigured:
        return False
    return True


def get_ids_for_current_doc_status(
    provider: Provider, status_name: str
) -> List[Union[int, None]]:
    """
    Method to get document quote status ids for given document status from settings

    Args:
        provider: Provider
        status_name: Document quote status name provided in query params

    Returns:
        Status ids for given document status
    """
    sow_quote_settings = provider.erp_integration.sow_document_quote_settings
    status_name = status_name.lower()
    # Get the key for status
    # E.g. won_status, open_status, etc
    status_key = f"{status_name}_status"
    status_ids = sow_quote_settings.get(status_key, [])
    return status_ids


def is_valid_status_name(status_name: str) -> bool:
    """
    Method to check if given document quote status name is valid or not.

    Args:
        status_name: Document quote status name provided in query params

    Returns:
        True if document quote status name is valid otherwise False
    """
    VALID_STATUSES = {"OPEN", "LOST", "WON"}
    if status_name.upper() in VALID_STATUSES:
        return True
    return False


def filter_documents_by_doc_status(
    provider: Provider, documents: QuerySet, status_name: str
) -> QuerySet:
    """
    Method to filter documents by given document quote status

    Args:
        provider: Provider
        documents: SOWDocument queryset
        status_name: Document quote status name provided in query params

    Returns:
        Documents filtered by quote status
    """
    document_quote_ids = list(documents.values_list("quote_id", flat=True))
    if is_valid_status_name(status_name) and document_quote_ids:
        status_ids = get_ids_for_current_doc_status(provider, status_name)
        quote_statuses = provider.erp_client.get_company_quote_statuses(
            document_quote_ids
        )
        filtered_quote_ids = []
        for quote in quote_statuses:
            quote_status_id = quote.get("status_id", None)
            if quote_status_id in status_ids:
                try:
                    filtered_quote_ids.append(quote.get("id"))
                except KeyError as err:
                    logger.info(err)
                    continue
        filtered_documents = documents.filter(quote_id__in=filtered_quote_ids)
        return filtered_documents
    else:
        return documents


def add_custom_field_to_payload(
    payload: Dict[Any, Any], po_custom_field_id: Union[int, None]
) -> Dict[Any, Any]:
    """
    Method to add the required custom field to payload for which
    setting is configured.

    Args:
        payload: PO payload
        po_custom_field_id: settings custom field id

    Returns:
        Modified payload with custom field
    """
    custom_field_dict = {}
    if not po_custom_field_id:
        payload.pop("customFields", None)
        payload.update({"customFields": custom_field_dict})
        return payload
    custom_fields = payload.pop("customFields", [])
    for index, record in enumerate(custom_fields):
        if record.get("id", -1) == po_custom_field_id:
            custom_field_dict = record
    payload.update({"customFields": custom_field_dict})
    return payload


def get_sow_document_file_name(
    document: SOWDocument, file_type: Optional[str] = None, **kwargs
) -> str:
    """
    Get file name as per SoW document name format. Used in SoW document PDF download,
    sending to account manager, etc.

    Args:
        document: SOWDocument object
        file_type: File type

    Returns:
        File name as per SoW document name format.
    """
    file_type: str = file_type if file_type else ""
    updated_on_date: str = document.updated_on.strftime("%Y.%m.%d")
    sow_document_name: str = document.name
    document_version: str = document.version
    customer_name: str = document.customer.name
    document_name: str = (
        f"{customer_name} - {sow_document_name} - SoW v{document_version} - "
        f"{updated_on_date}"
    )
    if kwargs.get("service_detail_suffix", None):
        service_detail_suffix: str = kwargs.get("service_detail_suffix")
        document_name += f" - {service_detail_suffix}"
    if file_type:
        document_name += f".{file_type}"
    return document_name


def get_sow_vendor_onboarding_settings(
    provider: Provider,
) -> Optional[SOWVendorOnboardingSetting]:
    """
    Get SoW vendor onboarding settings for the provider. If settings not configured,
    raises settings not configured exception.

    Args:
        provider: Provider

    Returns:
        SoW vendor onboarding setting.
    """
    try:
        sow_vendor_onboarding_setting: SOWVendorOnboardingSetting = (
            provider.sow_vendor_onboarding_setting
        )
    except SOWVendorOnboardingSetting.DoesNotExist:
        raise SOWVendorOnboardingSettingNotConfigured()
    return sow_vendor_onboarding_setting


def substitute_page_break_placeholder_in_change_request_json_config(
    json_config: Dict[str, Any]
) -> Dict[str, Any]:
    """
    Substitute @{Page Break} placeholder with page break html code.

    Args:
        json_config: Chnage request json config

    Returns:
        Updated json_config
    """
    alternates_value: str = (
        json_config.get("alternates", {}).get("alternates", {}).get("value")
    )
    if alternates_value:
        json_config["alternates"]["alternates"][
            "value"
        ] = add_page_break_in_html(alternates_value)

    change_description_value: str = (
        json_config.get("change_description", {})
        .get("change_description", {})
        .get("value", "")
    )
    if change_description_value:
        json_config["change_description"]["change_description"][
            "value"
        ] = add_page_break_in_html(change_description_value)

    change_impact_value: str = (
        json_config.get("change_impact", {})
        .get("change_impact", {})
        .get("value", "")
    )
    if change_impact_value:
        json_config["change_impact"]["change_impact"][
            "value"
        ] = add_page_break_in_html(change_impact_value)

    reason_value: str = (
        json_config.get("reason", {}).get("reason", {}).get("value", "")
    )
    if reason_value:
        json_config["reason"]["reason"]["value"] = add_page_break_in_html(
            reason_value
        )

    service_cost_notes_value: str = json_config.get("service_cost", {}).get(
        "notes", ""
    )
    if service_cost_notes_value:
        json_config["service_cost"]["notes"] = add_page_break_in_html(
            service_cost_notes_value
        )

    technical_changes_value: str = (
        json_config.get("technical_changes", {})
        .get("technical_changes", {})
        .get("value", "")
    )
    if technical_changes_value:
        json_config["technical_changes"]["technical_changes"][
            "value"
        ] = add_page_break_in_html(technical_changes_value)
    return json_config


def substitute_page_break_placeholder_in_sow_json_config(
    json_config: Dict[str, Any]
) -> Dict[str, Any]:
    """
    Substitute @{Page Break} placeholder with page break html code.

    Args:
        json_config: SOW json_config

    Returns:
        Updated json_config
    """
    # Parse varibles in project details section
    project_details: Dict[str, Any] = json_config.get("project_details", {})
    out_of_scope_value: str = project_details.get("out_of_scope", {}).get(
        "value", ""
    )
    if out_of_scope_value:
        project_details["out_of_scope"]["value"] = add_page_break_in_html(
            out_of_scope_value
        )

    project_assumptions_value: str = project_details.get(
        "project_assumptions", {}
    ).get("value", "")
    if project_assumptions_value:
        project_details["project_assumptions"][
            "value"
        ] = add_page_break_in_html(project_assumptions_value)

    task_deliverables_value: str = project_details.get(
        "task_deliverables", {}
    ).get("value", "")
    if task_deliverables_value:
        project_details["task_deliverables"]["value"] = add_page_break_in_html(
            task_deliverables_value
        )
    json_config["project_details"] = project_details

    # Parse variables in project management Fixed Fee section
    project_management_fixed_fee_value: str = (
        json_config.get("project_management_fixed_fee", {})
        .get("project_management", {})
        .get("value", "")
    )
    if project_management_fixed_fee_value:
        json_config["project_management_fixed_fee"]["project_management"][
            "value"
        ] = add_page_break_in_html(project_management_fixed_fee_value)

    # Parse variables in project management T&M section
    project_management_t_and_m_value: str = (
        json_config.get("project_management_t_and_m", {})
        .get("project_management", {})
        .get("value", "")
    )
    if project_management_t_and_m_value:
        json_config["project_management_t_and_m"]["project_management"][
            "value"
        ] = add_page_break_in_html(project_management_t_and_m_value)

    # Parse varibles in project outcome section
    project_outcome_value: str = (
        json_config.get("project_outcome", {})
        .get("project_outcome", {})
        .get("value", "")
    )
    if project_outcome_value:
        json_config["project_outcome"]["project_outcome"][
            "value"
        ] = add_page_break_in_html(project_outcome_value)

    # Parse variables in fixed fee terms section
    terms_fixed_fee_value: str = (
        json_config.get("terms_fixed_fee", {})
        .get("terms", {})
        .get("value", "")
    )
    if terms_fixed_fee_value:
        json_config["terms_fixed_fee"]["terms"][
            "value"
        ] = add_page_break_in_html(terms_fixed_fee_value)

    # Parse variables in T&M terms section
    terms_t_and_m_value: str = (
        json_config.get("terms_t_and_m", {}).get("terms", {}).get("value", "")
    )
    if terms_t_and_m_value:
        json_config["terms_t_and_m"]["terms"][
            "value"
        ] = add_page_break_in_html(terms_t_and_m_value)

    # Parse variables in service cost notes
    service_cost_notes_value: str = json_config.get("service_cost", {}).get(
        "notes", ""
    )
    if service_cost_notes_value:
        json_config["service_cost"]["notes"] = add_page_break_in_html(
            service_cost_notes_value
        )
    return json_config
