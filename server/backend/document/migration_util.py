import json

from document.models import SOWBaseTemplate, SOWDocument
from utils import dict_merge


def port_template(old_template, template_type):
    new_template_json = json.loads(json.dumps(SOWBaseTemplate.json_config()))
    merged_template = dict_merge(new_template_json, old_template)
    service_cost = {
        "static_fields": True,
        "engineering_hours": 0,
        "engineering_hourly_rate": 0,
        "after_hours_rate": 0,
        "project_management_hours": 0,
        "project_management_hourly_rate": 0,
        "contractors": [
            {
                "name": "",
                "partner_cost": 0,
                "margin_percentage": 0,
                "customer_cost": 0,
            },
            {
                "name": "",
                "partner_cost": 0,
                "margin_percentage": 0,
                "customer_cost": 0,
            },
        ],
        "travels": [
            {"description": "", "cost": 0},
            {"description": "", "cost": 0},
        ],
        "notes": "",
        "customer_cost_fixed_fee": 0,
        "internal_cost_fixed_fee": 0,
        "customer_cost_t_and_m_fee": 0,
        "internal_cost_t_and_m_fee": 0,
    }

    if template_type == SOWDocument.GENERAL:
        service_cost["internal_cost_fixed_fee"] = (
            old_template.get("service_cost", {})
            .get("ps_cost", {})
            .get("value", 0)
        )
        service_cost["project_management_hours"] = (
            old_template.get("service_cost", {})
            .get("pm_hours", {})
            .get("value", 0)
        )
        service_cost["customer_cost_fixed_fee"] = (
            old_template.get("service_cost", {})
            .get("ps_pricing", {})
            .get("value", 0)
        )
    elif template_type == SOWDocument.T_AND_M:
        service_cost["engineering_hourly_rate"] = (
            old_template.get("service_rates", {})
            .get("engineering_hourly_rate", {})
            .get("value", 0)
        )
        service_cost["after_hours_rate"] = (
            old_template.get("service_rates", {})
            .get("engineering_after_hours_rate", {})
            .get("value", 0)
        )
        service_cost["project_management_hourly_rate"] = (
            old_template.get("service_rates", {})
            .get("project_management_hourly_rate", {})
            .get("value", 0)
        )
        service_cost["project_management_hours"] = (
            old_template.get("service_hours", {})
            .get("pm_hours", {})
            .get("value", 0)
        )
        service_cost["engineering_hours"] = (
            old_template.get("service_hours", {})
            .get("engineering_hours", {})
            .get("value", 0)
        )
    numeric_keys = [
        "internal_cost_fixed_fee",
        "project_management_hours",
        "customer_cost_fixed_fee",
        "engineering_hourly_rate",
        "after_hours_rate",
        "project_management_hourly_rate",
        "project_management_hours",
        "engineering_hours",
    ]
    for key in numeric_keys:
        try:
            service_cost[key] = int(service_cost[key])
        except ValueError:
            service_cost[key] = 0
    merged_template["service_cost"] = service_cost
    drop_keys = ["service_hours", "service_rates"]
    for key in drop_keys:
        merged_template.pop(key, None)
    return merged_template
