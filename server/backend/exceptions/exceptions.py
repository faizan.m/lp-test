from rest_framework.exceptions import APIException

# Get an instance of a logger
from utils import get_app_logger

logger = get_app_logger(__name__)


class ExternalAPIErrorBase(APIException):
    """
    Base error class for all exceptions raised in this library.
    """

    detail = "A server error occurred."
    status_code = 500


class APIError(ExternalAPIErrorBase):
    """
    Raised for errors related to interacting with the API server.
    """

    def __init__(self, code=None, source=None, message=None, errors=None):
        self.detail = errors or message
        self.status_code = code or self.status_code
        logger.debug(
            f"Exception Source: {source} Code: {self.status_code} Message: {self.detail} Errors: {errors}"
        )

    def __str__(self):
        return "APIError(id=%s): %s" % (self.status_code, self.detail)


class TwoFactorRequiredError(APIError):
    detail = "Two-factor authentication required."
    status_code = 402


class ParamRequiredError(APIError):
    pass


class ValidationError(APIError):
    detail = "Value was invalid."
    status_code = 422


class InvalidRequestError(APIError):
    detail = "API request is invalid."
    status_code = 400


class PersonalDetailsRequiredError(APIError):
    pass


class AuthenticationError(APIError):
    detail = "API credentials are invalid."
    status_code = 401


class AuthorizationError(APIError):
    detail = "You do not have permission to perform this action."
    status_code = 403


class UnverifiedEmailError(APIError):
    pass


class InvalidTokenError(APIError):
    detail = "Invalid token specified."
    status_code = 401


class RevokedTokenError(APIError):
    detail = "Token has been revoked."
    status_code = 401


class ExpiredTokenError(APIError):
    detail = "Token expired."
    status_code = 401


class NotFoundError(APIError):
    detail = "Not found."
    status_code = 404


class RateLimitExceededError(APIError):
    detail = "API call limit exceeded."
    status_code = 429


class InternalServerError(APIError):
    detail = "Internal server error."
    status_code = 500


class ServiceUnavailableError(APIError):
    detail = "Service temporarily unavailable."
    status_code = 503


def build_error(code, source=None, message=None, errors=None):
    """
    Helper method for creating errors and attaching HTTP response/request
    details to them.
    """
    error_class = _status_code_to_class.get(code, APIError)
    return error_class(code, source, message, errors)


_status_code_to_class = {
    400: InvalidRequestError,
    401: AuthenticationError,
    402: TwoFactorRequiredError,
    403: AuthorizationError,
    404: NotFoundError,
    422: ValidationError,
    429: RateLimitExceededError,
    500: InternalServerError,
    503: ServiceUnavailableError,
}
