import os

import celeryconfig

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.local")
os.environ.setdefault("DJANGO_CONFIGURATION", "Local")

import configurations

configurations.setup()

from celery import Celery

app = Celery("backend")
app.config_from_object(celeryconfig)
from django.apps import apps

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
