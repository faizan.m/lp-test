from django.db.models import JSONField
from django.db import models


class PersistentRateLimiter(models.Model):

    name = models.TextField(unique=True)
    max_limit = JSONField()
    current_limit = JSONField(default=dict)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
