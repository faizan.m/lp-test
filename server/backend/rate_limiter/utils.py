import functools
import hashlib
from datetime import datetime

from django.db import transaction

from exceptions import exceptions
from rate_limiter.models import PersistentRateLimiter


class RateLimitException(Exception):
    def __init__(self, message):
        """
        Custom exception raise when the number of function invocations exceeds
        that imposed by a rate limit. Additionally the exception is aware of
        the remaining time period after which the rate limit is reset.

        :param string message: Custom exception message.
        """
        super(RateLimitException, self).__init__(message)


@transaction.atomic
def rate_limiter(key, hour=None, day=None, month=None, raise_exception=True):
    current_time = datetime.now()
    date_time_format = "%Y-%m-%dT%H:%M:%SZ"
    if not any([hour, day, month]):
        raise Exception(
            "Please specify one of the limit for hour, day or month."
        )
    max_limit = dict(hour=hour, day=day, month=month)
    (
        p,
        created,
    ) = PersistentRateLimiter.objects.select_for_update().get_or_create(
        name=key, defaults=dict(name=key, max_limit=max_limit)
    )
    if not created and p.max_limit != max_limit:
        p.max_limit = max_limit
    if hour:
        last_reset_hour = p.current_limit.get("last_reset_hour")
        hour_count = 0
        if last_reset_hour:
            last_reset_hour = datetime.strptime(
                last_reset_hour, date_time_format
            )
            last_reset_hour = last_reset_hour.replace(
                minute=0, second=0, microsecond=0
            )
            current_time = current_time.replace(
                minute=0, second=0, microsecond=0
            )
            # If last reset hour is older than the current hour
            if last_reset_hour < current_time:
                last_reset_hour = current_time
            else:
                hour_count = p.current_limit.get("hour_count")
                # Check if the max limit for the hour has reached or not.
                if hour_count >= hour:
                    raise RateLimitException("Hour limit exceeded.")
        else:
            last_reset_hour = current_time
        last_reset_hour = last_reset_hour.strftime(date_time_format)
        hour_count += 1
        p.current_limit.update(
            dict(last_reset_hour=last_reset_hour, hour_count=hour_count)
        )
    if day:
        last_reset_day = p.current_limit.get("last_reset_day")
        day_count = 0
        if last_reset_day:
            last_reset_day = datetime.strptime(
                last_reset_day, date_time_format
            )
            # If last reset day is older than the current date
            last_reset_day = last_reset_day.replace(
                hour=0, minute=0, second=0, microsecond=0
            )
            current_time = current_time.replace(
                hour=0, minute=0, second=0, microsecond=0
            )
            if last_reset_day < current_time:
                last_reset_day = current_time
            else:
                day_count = p.current_limit.get("day_count")
                # Check if the max limit for the day has reached or not.
                if day_count >= day:
                    raise RateLimitException("Day limit exceeded.")
        else:
            last_reset_day = current_time
        last_reset_day = last_reset_day.date().strftime(date_time_format)
        day_count += 1
        p.current_limit.update(
            dict(last_reset_day=last_reset_day, day_count=day_count)
        )
    if month:
        last_reset_month = p.current_limit.get("last_reset_month")
        month_count = 0
        if last_reset_month:
            last_reset_month = datetime.strptime(
                last_reset_month, date_time_format
            )
            # If last reset day is older than the current date
            last_reset_month = last_reset_month.replace(
                day=1, hour=0, minute=0, second=0, microsecond=0
            )
            current_time = current_time.replace(
                day=1, hour=0, minute=0, second=0, microsecond=0
            )
            # If last reset month is older than the current month
            if last_reset_month < current_time:
                last_reset_month = current_time
            else:
                month_count = p.current_limit.get("month_count")
                # Check if the max limit for the day has reached or not.
                if month_count >= month:
                    raise RateLimitException("Month limit exceeded.")
        else:
            last_reset_month = current_time
        last_reset_month = last_reset_month.date().strftime(date_time_format)
        month_count += 1
        p.current_limit.update(
            dict(last_reset_month=last_reset_month, month_count=month_count)
        )
    p.save()
    return True


def cisco_rate_limiter(
    key, hour=None, day=None, month=None, raise_exception=True
):
    def decorator(func):
        @functools.wraps(func)
        @transaction.atomic
        def wrapper(self, *args, **kwargs):
            try:
                client_id = self.client_id
                client_secret = self.client_secret
                hash_object = hashlib.md5(
                    f"{client_id} - {client_secret}".encode("utf-8")
                )
                value = hash_object.hexdigest()
                if rate_limiter(
                    f"{value} - {key}", hour, day, month, raise_exception
                ):
                    return func(self, *args, **kwargs)
            except RateLimitException as exception:
                if raise_exception:
                    raise exceptions.RateLimitExceededError(
                        message=str(exception)
                    )
                else:
                    return None

        return wrapper

    return decorator
