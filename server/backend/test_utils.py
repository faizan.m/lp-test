import json
from typing import Any, List

from django.core.serializers.json import DjangoJSONEncoder
from django.test import TestCase
from factory.fuzzy import FuzzyChoice
from rest_framework.test import force_authenticate, APIRequestFactory

from accounts.models import Role
from accounts.tests.factories import (
    ProviderFactory,
    ProviderUserFactory,
    ProfileFactory,
    ProviderUserTypeFactory,
    OwnerUserRoleFactory,
)


class TestClientMixin:
    @staticmethod
    def api_factory_client_request(
        method, url, provider=None, user=None, data=None
    ):
        if data:
            data = json.dumps(data, cls=DjangoJSONEncoder)
        client = APIRequestFactory()
        request = client.generic(
            method, url, data, content_type="application/json"
        )
        if user:
            force_authenticate(request, user=user)
        if provider:
            setattr(request, "provider", provider)
        return request


class BaseProviderTestCase(TestCase):
    @classmethod
    def setup_provider_and_user(cls):
        cls.provider = ProviderFactory()
        user_type = ProviderUserTypeFactory()
        OwnerUserRoleFactory(user_type=user_type)
        role = Role.objects.get(role_name=Role.OWNER, user_type=user_type)
        cls.user = ProviderUserFactory(
            user_type=user_type, user_role=role, provider=cls.provider
        )
        ProfileFactory(user=cls.user)


def get_a_random_value_from_given_choices(choices: List[Any]) -> Any:
    """
    Get a random value from given choices

    Args:
        choices: List of choice values

    Returns:
        Random choice value
    """
    random_choice: Any = FuzzyChoice(choices=choices).fuzz()
    return random_choice
