#To install wkhtmltopdf using multi stage build
FROM alpine:latest as builder
ARG WKHTML2PDF_VERSION='0.12.4'
RUN apk update
RUN wget "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${WKHTML2PDF_VERSION}/wkhtmltox-${WKHTML2PDF_VERSION}_linux-generic-amd64.tar.xz"
RUN tar -xJf "wkhtmltox-${WKHTML2PDF_VERSION}_linux-generic-amd64.tar.xz"
#RUN cd wkhtmltox && chown root:root bin/wkhtmltopdf && cp -r * /usr/

FROM python:3.9.5
ENV PYTHONUNBUFFERED 1

# Allows docker to cache installed dependencies between builds
COPY requirements requirements
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt
#Copying the wkthtmltox from builder
COPY --from=builder wkhtmltox/ /usr/

# Adds our application code to the image
COPY . code

# Download postgres client for pg_dump command
RUN apt-get update && apt-get install -y lsb-release && apt-get clean all
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'


RUN apt-get update && apt-get install cron -y && apt-get install awscli -y && apt-get -y install postgresql-13
RUN touch /var/log/cron.log
COPY backend/cronjob /etc/cron.d/cronjob
RUN chmod 644 /etc/cron.d/cronjob
COPY backend/script.sh /script.sh
RUN chmod +x code/backend/script.sh

WORKDIR code/backend

EXPOSE 8000

# Create directories for log files
RUN mkdir /var/log/django
RUN touch /var/log/django/django_production.log /var/log/django/celery.log


# Migrates the database, uploads staticfiles, and runs the production server
CMD ./manage.py migrate && \
    ./manage.py collectstatic --noinput && \
    cron && \
    tail -f /var/log/cron.log && \
    gunicorn -c gunicorn_cfg.py wsgi:application
