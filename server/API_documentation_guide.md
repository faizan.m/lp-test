# API Documentation Guide
This documents contains details about API documentation using drf-yasg.
drf-yasg is used to generate **real** Swagger/OpenAPI 2.0 specifications from a Django Rest Framework API.


[Link to official documentation site:](https://drf-yasg.readthedocs.io/en/stable/readme.html)

[Link to code guide](https://drf-yasg.readthedocs.io/en/stable/drf_yasg.html#module-drf_yasg.codecs)


##### Prerequisites:
* drf-yasg - v1.17.0
* Python - v.3.9.5
* Django - v.2.0.5
* DRF - v.3.8.2


## Standard practises:
1. **Imports**:
    ```python
    from drf_yasg.utils import swagger_auto_schema  
    from django.utils.decorators import method_decorator  
    from drf_yasg import openapi
    ```
	

2. **Use django method_decorator:**  
    * We generally use a single class based view which supports various methods such as GET, POST, PUT, etc. on same endpoint. To generate documentation for each method, use django method decorators. 
    * The method decorator can be used to decorate each method on a view class such as get(), post(), etc.
    * Follow this link to learn more about django method decorators: [Django method decorators](https://docs.djangoproject.com/en/4.0/topics/class-based-views/intro/#decorating-the-class)
    * E.g. 
        ```python
        @method_decorator(  
          name="post",  
          decorator=swagger_auto_schema( )
        )  
        class SalesAgreementTemplateListCreateAPIView(generics.ListCreateAPIView):
        ```
    * For better understanding follow this refernce in the codebase: *server/backend/sales/views/template.py:35* 
    

3. **Use swagger_auto_schema() decorator:**
    * **swagger_auto_schema** decorator is used to decorate views to override some properties of the generated documentation.
    * Following are the parameters to *swagger_auto_schema* to override generated documentation:
      * ```request_body```: Serializers or OpenAPI schema can be used to define a request body.
      * ```manual_parameters```: A list of manual parameters to override the
        automatically generated ones. These are identified by their (``name``, ``in``) combination, and any parameters given
        here will fully override automatically generated parameters if they collide.
      * ```operation_description```: To override operation description
      * ```operation_summary```: To override operation summary
      * ```responses```:A dict of documented manual responses
    * Follow this link for better understanding about *swagger_auto_schema* and other parameters: [swagger_auto_schema use](https://drf-yasg.readthedocs.io/en/stable/drf_yasg.html#drf_yasg.utils.swagger_auto_schema)
    * Refer to examples in our codebase: *server/backend/sales/views/template.py:369*
    * E.g. 
        ```python
        @method_decorator(  
          name="get",  
          decorator=swagger_auto_schema(  
                operation_description="List agreement templates",  
                operation_summary="This endpoint supports listing of active templates, listing of disabled templates"  
                                  "and listing of template authors.",  
                manual_parameters=[  
                    openapi.Parameter(  
                        "show_disabled",  
                        in_=openapi.IN_QUERY,  
                        type=openapi.TYPE_BOOLEAN,  
                        default="false",  
                        description="Used for listing disabled templates",  
                    ),  
                    openapi.Parameter(  
                        "list_authors",  
                        in_=openapi.IN_QUERY,  
                        type=openapi.TYPE_BOOLEAN,  
                        default="false",  
                        description="Used for listing template authors",  
                    ),  
                ],  
                responses={  
                "200": AgreementTemplateSerializer(),  
                },  
            ),  
        )
        class SalesAgreementTemplateListCreateAPIView(generics.ListCreateAPIView):
       ```
   

4. **Use serializers whenever possible**:
    * Manually defining schema for large request bodies or responses can be tedious. Prefer using different serializers for request body and responses.
    * Using different serializers for read and write/update is also a good practise from performance point of view.
    * Also the documentation generated using serializers is more informative than the schema defined.
    * Follow this reference in the codebase for better understanding: *server/backend/sales/views/template.py:298*


5. **Use of query parameters should be limited:**
    * Query parameters should be limited and mutually exclusive as possible. Better when used with get() and list() methods. 
    * In a scenario where we need to perform different operations using same endpoint, it's preferable to use paremeters in request body than in query params.
    * Follow this reference in the codebase for better understanding: *server/backend/sales/views/template.py:153*