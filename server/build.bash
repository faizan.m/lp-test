#!/bin/bash -ex


# Declare required system variables here
mkdir -p ~/.virtualenvs
export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=`which python3.6`

# Enable Python virtualenv
echo "=== enable workon ===" `which virtualenvwrapper.sh` `which workon`
source `which virtualenvwrapper.sh`

# Clean up and Setup Python virtual env.
rmvirtualenv lookingpoint || true
mkvirtualenv --python=`which python3.6` lookingpoint
workon lookingpoint
pip install -r requirements/development.txt