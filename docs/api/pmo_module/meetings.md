# Meeting APIs

###  List all Meetings for a project

Returns the list of all project meetings, sorted on the latest schdule date.

#### Request:

Method: `GET`

Url: `/providers/pmo/projects/<int:project_id>/meetings`

 *Note:*


 - Pagination is disabled


#### Response:

Status: `200 OK`

```json
[
  {
    "id": 6,
    "name": "Kickoff meeting 1",
    "schedule_start_datetime": "2021-04-09T12:53:00+0000",
    "schedule_end_datetime": "2021-04-09T12:53:00+0000",
    "conducted_on": null,
    "status": {
      "id": 1,
      "title": "Open",
      "color": "#f8af03"
    },
    "meeting_type": "Kickoff",
    "updated_on": "2021-04-08T07:29:08+0000"
  },
  {
    "id": 7,
    "name": "Kickoff meeting 2",
    "schedule_start_datetime": "2021-04-10T12:53:00+0000",
    "schedule_end_datetime": "2021-04-10T12:53:00+0000",
    "conducted_on": null,
    "status": {
      "id": 1,
      "title": "Open",
      "color": "#f8af03"
    },
    "meeting_type": "Internal Kickoff",
    "updated_on": "2021-04-08T07:32:53+0000"
  },
  {
    "id": 8,
    "name": "Kickoff meeting 2",
    "schedule_start_datetime": "2021-04-10T12:53:00+0000",
    "schedule_end_datetime": "2021-04-10T12:53:00+0000",
    "conducted_on": null,
    "status": {
      "id": 1,
      "title": "Open",
      "color": "#f8af03"
    },
    "meeting_type": "Internal Kickoff",
    "updated_on": "2021-04-08T07:40:29+0000"
  },
  {
    "id": 2,
    "name": "Status Meeting 1",
    "schedule_start_datetime": "2021-04-11T19:23:00+0000",
    "schedule_end_datetime": "2021-04-11T19:23:00+0000",
    "conducted_on": null,
    "status": null,
    "meeting_type": "",
    "updated_on": "2021-04-07T14:56:42+0000"
  },
  {
    "id": 1,
    "name": "Status Meeting",
    "schedule_start_datetime": "2021-04-11T19:23:00+0000",
    "schedule_end_datetime": "2021-04-11T19:23:00+0000",
    "conducted_on": null,
    "status": null,
    "meeting_type": "",
    "updated_on": "2021-04-07T14:54:20+0000"
  },
  {
    "id": 4,
    "name": "Status Meeting 4",
    "schedule_start_datetime": "2021-04-11T19:23:00+0000",
    "schedule_end_datetime": "2021-04-11T19:23:00+0000",
    "conducted_on": null,
    "status": null,
    "meeting_type": "",
    "updated_on": "2021-04-07T14:59:52+0000"
  },
  {
    "id": 3,
    "name": "Status Meeting 3",
    "schedule_start_datetime": "2021-04-11T19:23:00+0000",
    "schedule_end_datetime": "2021-04-11T19:23:00+0000",
    "conducted_on": null,
    "status": null,
    "meeting_type": "",
    "updated_on": "2021-04-07T14:59:07+0000"
  },
  {
    "id": 5,
    "name": "Status Meeting 1",
    "schedule_start_datetime": "2021-04-18T19:23:00+0000",
    "schedule_end_datetime": "2021-04-18T22:23:00+0000",
    "conducted_on": null,
    "status": {
      "id": 1,
      "title": "Open",
      "color": "#f8af03"
    },
    "meeting_type": "Status",
    "updated_on": "2021-04-07T16:05:16+0000"
  }
]
```
---
## Status Meeting

### 1. Create Status Meetings


#### Request:

Method: `POST`

Url: `providers/pmo/projects/<int:project_id>/status-meetings`


Parameters:

Name                   | Type           | Required | Description
-----------------------|----------------|----------|------------
name                   | string         | Yes      | Meeting title.
schedule_start_datetime| datetime       | Yes      | Meeting start datetime
schedule_start_datetime| datetime       | Yes      | Meeting end datetime
conducted_on           | datetime       | No       | Meeting actual conducted datetime
status                 | string         | No       | Meeting status (Planned/Done)
agenda_topics          | List of string | No       | List of Agenda topics

Body:
```json
{
    "name": "Status Meeting 5",
    "schedule_start_datetime": "2021-04-11T19:23:00+0000",
    "schedule_end_datetime": "2021-04-11T19:23:00+0000",
    "conducted_on": null,
    "status": null,
    "agenda_topics": [
			"Agenda topic 1",
			"Agenda Topic 2"
		]
}

```

#### Response:

Status: `201 Created`

```json
{
  "name": "Status Meeting 5",
  "schedule_start_datetime": "2021-04-11T19:23:00+0000",
  "schedule_end_datetime": "2021-04-11T19:23:00+0000",
  "conducted_on": null,
  "status": null,
  "meeting_type": "Status",
  "agenda_topics": [
			"Agenda topic 1",
			"Agenda Topic 2"
		]
}
```

### 2. Retrieve Status Meeting
Get details of a single status Meeting

#### Request:

Method: `GET`

Url: `providers/pmo/projects/<int:project_id>/status-meetings/<int:meeting_id>`

#### Response:

Status: `200 OK`

```json

{
  "id": 5,
  "name": "Status Meeting 1",
  "schedule_start_datetime": "2021-04-18T19:23:00+0000",
  "schedule_end_datetime": "2021-04-18T22:23:00+0000",
  "conducted_on": null,
  "status": {
    "id": 1,
    "title": "Open",
    "color": "#f8af03"
  },
  "agenda_topics": [
    "Agenda topic 1",
    "New"
  ],
  "meeting_type": "Status",
  "updated_on": "2021-04-07T16:05:16+0000"
}

```

### 3. Update Status Meeting

#### Request:

Method: `PUT`

Url: `providers/pmo/projects/<int:project_id>/status-meetings/<int:meeting_id>`


Parameters:

Name                   | Type           | Required | Description
-----------------------|----------------|----------|------------
name                   | string         | Yes      | Meeting title.
schedule_start_datetime| datetime       | Yes      | Meeting start datetime
schedule_start_datetime| datetime       | Yes      | Meeting end datetime
conducted_on           | datetime       | No       | Meeting actual conducted datetime
status                 | string         | No       | Meeting status (Planned/Done)
agenda_topics          | List of string | No       | List of Agenda topics

Body:
```json
{
  "id": 5,
  "name": "Status Meeting 1",
  "schedule_start_datetime": "2021-04-18T19:23:00+0000",
  "schedule_end_datetime": "2021-04-18T22:23:00+0000",
  "conducted_on": null,
  "status": 1,
  "agenda_topics": [
    "Agenda topic 1",
		"New"
  ]
}
```

#### Response:

Status: `200 OK`

### 4. Delete Status Meeting

#### Request:

Method: `DELETE`

Url: `providers/pmo/projects/<int:project_id>/status-meetings/<int:meeting_id>`

#### Response:

Status: `204 No Content`
