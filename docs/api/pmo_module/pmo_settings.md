# PMO Settings APIs
API endpoints for various settings related to PMO Module

-----
## Project Type Settings
CRUD APIs to manage Project Type Setting

### List Project Types

Returns the list of Project Types configured in PMO Settings for the current Provider.
Sorted by `updated_on` key.
Each Project Type also contains list of phases related to the Project Type, sorted based on the `order` key.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-types`


#### Response:

Status: `200 OK`

```json
{
  "links": {
    "next_page_number": null,
    "previous_page_number": null,
    "page_number": 1
  },
  "count": 2,
  "page_size": 10,
  "results": [
    {
      "phases": [
        {
          "title": "Phase1",
          "id": 3,
          "order": 1,
          "updated_on": "2021-03-02T15:00:22.215844+00:00",
          "description": null,
          "created_on": "2021-03-02T15:00:22.215799+00:00"
        }
      ],
      "title": "title4",
      "id": 7,
      "updated_on": "2021-03-02T13:12:15.977996+00:00",
      "description": "description",
      "created_on": "2021-03-02T13:12:15.977929+00:00"
    },
    {
      "phases": [
        {
          "title": "Phase1",
          "id": 2,
          "order": 1,
          "updated_on": "2021-03-02T14:59:22.867427+00:00",
          "description": null,
          "created_on": "2021-03-02T14:59:22.867378+00:00"
        }
      ],
      "title": "title3",
      "id": 5,
      "updated_on": "2021-03-02T13:06:14.091501+00:00",
      "description": "description",
      "created_on": "2021-03-02T13:06:14.091435+00:00"
    }
  ]
}
```

### Create Project Type


#### Request:

Method: `POST`

Url: `providers/pmo/settings/project-types`


Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
title      | string | Yes      | Project Type title.
description| string | No       | Optional Type description.

<a name="project-type-create-payload"></a>
Body:
```json
{
	"title": "title4",
	"description": "description"
}

```
*Note:*

- `title` should be unique for a provider. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json
{
      "title": "title4",
      "id": 5,
      "updated_on": "2021-03-02T13:06:14.091501+00:00",
      "description": "description",
      "created_on": "2021-03-02T13:06:14.091435+00:00"
}
```

### Retrieve Project Type
Get details of a single Project Type

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-types/<int:project_type_id>`

#### Response:

Status: `200 OK`

```json

{
      "phases": [
        {
          "title": "Phase1",
          "id": 3,
          "order": 1,
          "updated_on": "2021-03-02T15:00:22.215844+00:00",
          "description": null,
          "created_on": "2021-03-02T15:00:22.215799+00:00"
        }
      ],
      "title": "title4",
      "id": 7,
      "updated_on": "2021-03-02T13:12:15.977996+00:00",
      "description": "description",
      "created_on": "2021-03-02T13:12:15.977929+00:00"
}

```
### Update Project Type
Update details of a single Project Type. Supports both partial and full update.
Use `PATCH` method to do partial update. To update full object use `PUT` method.

#### Request:

Method: `PATCH` or `PUT`

Url: `providers/pmo/settings/project-types/<int:project_type_id>`

Body: Same as  [create](#project-type-create-payload)

### Delete a Project Type
Delete(hard) a single Project Type

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/project-types/<int:project_type_id>`

#### Response:

Status: `200 OK`

-----
## Project Type Phase Settings
CRUD APIs to manage Project Type Phases

### List Project Phases

Returns the list of Project phases for a project type configured in PMO Settings for the current Provider.
Sorted by `order` key.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-types/<int:project_type_id>/phases`


#### Response:

Status: `200 OK`

```json

{
  "links": {
    "next_page_number": null,
    "previous_page_number": null,
    "page_number": 1
  },
  "count": 4,
  "page_size": 10,
  "results": [
    {
      "id": 3,
      "title": "Phase1",
      "order": 1
    },
    {
      "id": 4,
      "title": "Phase2",
      "order": 2
    },
    {
      "id": 5,
      "title": "Phase3",
      "order": 4
    },
    {
      "id": 6,
      "title": "Phase4",
      "order": 4
    }
  ]
}
```

### Add a phase to a Project Type
This API can be used to create a single phase or create a list of phases(bulk operation)

#### Single Create Request:

Method: `POST`

Url: `providers/pmo/settings/project-types/<int:project_type_id>/phases`


Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
title      | string | Yes      | Phase title
order      | integer| Yes      | Phase order

<a name="project-phase-create-payload"></a>
Body:
```json
{
	"title": "Phase2",
	"order": 1
}

```
*Note:*

- `title` should be unique for a provider and a project type. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json
{
  "id": 4,
  "title": "Phase2",
  "order": 1
}
```

#### Bulk Create Request
<a name="project-phase-bulk-create-payload"></a>
Body:
```json
[
	{
		"title": "Phase3",
		"order": 1
	},
	{
		"title": "Phase4",
		"order": 1
	}
]

```
*Note:*

- `title` should be unique for a provider and a project type. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json

[
  {
    "id": 5,
    "title": "Phase3",
    "order": 1
  },
  {
    "id": 6,
    "title": "Phase4",
    "order": 1
  }
]
```

### Bulk Update Project Phases

#### Request:

Method: `PUT` or `PATCH`

Url: `providers/pmo/settings/project-types/<int:project_type_id>/phases`

Body:
```json

[
	{
		"id": 4,
		"order": 2
	},
	{
		"id": 5,
		"title": "Phase 10",
		"order": 3
	}
]

```
*Note:*

 - `id` is a required field in bulk update. If the object doesn't include `id`, `400 BAD REQUEST` will be
 thrown

#### Response

Status: `200 OK`

```json
[
  {
    "id": 4,
    "title": "Phase2",
    "order": 2
  },
  {
    "id": 5,
    "title": "Phase 10",
    "order": 3
  }
]
```

### Delete a Project Phase
Delete(hard) a single Project Phase

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/project-types/<int:project_type_id>/phases/<int:phase_id>`

#### Response:

Status: `200 OK`

-----
## Activity Status Settings

### Creates an activity status 
This API can be used to create an activity status for the provider.

#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/activity-status`


Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
title      | string | Yes      | Status title
color      | string | Yes      | Status hexcode color

<a name="activity-status-create-payload"></a>
Body:
```json
{
	"title": "Done",
	"color": #0784c
}

```
*Note:*

- `title` should be unique for a provider. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json
{
  "id": 1,
  "title": "Done",
	"color": "#0784c"
}
```

### List Activity Status Settings

Returns the list of activity status created for the current Provider.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/activity-status`


#### Response:

Status: `200 OK`

```json
{
    "links": {
        "next_page_number": null,
        "previous_page_number": null,
        "page_number": 1
    },
    "count": 2,
    "page_size": 10,
    "results": [
        {
            "id": 4,
            "title": "Done",
            "color": "#0784c"
        },
        {
            "id": 8,
            "title": "Inprogress",
            "color": "#01284c"
        }
    ]
}
```

### Update Activity Status
Update details of a single activity status setting. Supports both partial and full update.
Use `PATCH` method to do partial update. To update full object use `PUT` method.

#### Request:

Method: `PATCH` or `PUT`

Url: `providers/pmo/settings/activity-status/<int:activity_status_id>`

Body: Same as  [create](#activity-status-create-payload)

### Delete a Activity Status
Delete(hard) a single activity status setting.

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/activity-status/<int:activity_status_id>`

#### Response:

Status: `200 OK`

-----
## Project Board Setting

### List Project Boards and overall statuses
This API lists the mapped project boards and the overall statuses mapped for them.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-boards`


#### Response:

Status: `200 OK`

```json
{
  "links": {
    "next_page_number": null,
    "previous_page_number": null,
    "page_number": 1
  },
  "count": 2,
  "page_size": 10,
  "results": [
    {
      "id": 1,
      "statuses": [
        {
          "id": 1,
          "title": "Ready To Bill",
          "connectwise_id": 1,
          "is_default": false,
          "is_open": true,
          "color": "#FFFEEE"
        },
        {
          "id": 2,
          "title": "Completed",
          "connectwise_id": 1,
          "is_default": false,
          "is_open": true,
          "color": "#FFFEEE"
        }
      ],
      "name": "Project Board",
      "connectwise_id": 12,
      "provider": 1
    },
    {
      "id": 4,
      "statuses": [
        {
          "id": 5,
          "title": "Completed",
          "connectwise_id": 1,
          "is_default": false,
          "is_open": true,
          "color": "#FFFEEE"
        },
        {
          "id": 6,
          "title": "Ready To Bill",
          "connectwise_id": 1,
          "is_default": false,
          "is_open": true,
          "color": "#FFFEEE"
        }
      ],
      "name": "Project Board 2",
      "connectwise_id": 13,
      "provider": 1
    }
  ]
}
```

### Create a Project Board Mapping
This API can be used to create a Project Board mapping and also the overall status
related to that board in a single request.

#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/project-boards/`


Parameters:

Name                | Type    | Required | Description
--------------------|---------|----------|------------
name                | string  | Yes      | Board Name
connectwise_id      | integer | Yes      | Connectwise mapping id
statuses            | List of [Overall Status](#overall-status-create-params) | No       | List of overall statuses mapped to this board

Body:
```json
{
	"name": "Project Board 2",
	"connectwise_id": 13,
	"statuses": [
		{
			"title": "Completed",
			"connectwise_id": 1,
			"is_default": false,
			"is_open": true,
			"color": "#FFFEEE"
		},
		{
			"title": "Ready To Bill",
			"connectwise_id": 1,
			"is_default": false,
			"is_open": true,
			"color": "#FFFEEE"
		}
	]
}

```

*Note:*

- `connectwise_id` should be unique for a provider. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json
{
  "id": 6,
  "statuses": [
    {
      "id": 7,
      "title": "Completed",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    },
    {
      "id": 8,
      "title": "Ready To Bill",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    }
  ],
  "name": "Project Board 3",
  "connectwise_id": 15,
  "provider": 1
}
```

### Retrieve Project Board
This API returns a single project board and the overall statuses mapped for them.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-boards/<int:board_id>`


#### Response:

Status: `200 OK`

```json
{
  "id": 1,
  "statuses": [
    {
      "id": 1,
      "title": "Ready To Bill",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    },
    {
      "id": 2,
      "title": "Completed",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    }
  ],
  "name": "Project Board",
  "connectwise_id": 12,
  "provider": 1
}
```

### Delete a Project Board
Delete(hard) a Project Board mapping along with its mapped statuses

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/project-boards/<int:board_id>`

#### Response:

Status: `200 OK`

-----
## Overall Status Settings

### Creates an overall status 
This API can be used to create an overall status for the provider.

#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/project-boards/<int:board_id>/overall-statuses`


Parameters:


<a name="overall-status-create-params"></a>

Name                | Type    | Required | Description
--------------------|---------|----------|------------
title               | string  | Yes      | Status title
connectwise_id      | integer | Yes      | Connectwise mapping id
is_default          | boolean | No       | Sets the status as default. By default false
color               | string  | Yes      | Hex Code for the color
is_open             | boolean | No       | If status is closed, set is_open = false. By default true (open)


<a name="overall-status-create-payload"></a>
Body:
```json
{
  "title": "Ready To Bill",
  "connectwise_id": 12,
  "is_default": true,
  "color": "#FFFEEE"
}

```
*Note:*

- `title` should be unique for a provider. Throws Validation error if title already exists.

#### Response:

Status: `201 Created`

```json
{
  "id": 1,
  "title": "Ready To Bill",
  "connectwise_id": 1,
  "is_default": true,
  "is_open": true,
  "color": "#FFFEEE"
}
```

### List Overall Status Settings

Returns the list of overall status created for the current Provider.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/project-boards/<int:board_id>/overall-statuses`


#### Response:

Status: `200 OK`

```json
{
  "links": {
    "next_page_number": null,
    "previous_page_number": null,
    "page_number": 1
  },
  "count": 2,
  "page_size": 10,
  "results": [
    {
      "id": 2,
      "title": "Completed",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    },
    {
      "id": 1,
      "title": "Ready To Bill",
      "connectwise_id": 1,
      "is_default": false,
      "is_open": true,
      "color": "#FFFEEE"
    }
  ]
}
```

### Update Overall Status
Update details of a single overall status setting. Supports both partial and full update.
Use `PATCH` method to do partial update. To update full object use `PUT` method.

#### Request:

Method: `PATCH` or `PUT`

Url: `providers/pmo/settings/project-boards/<int:board_id>/overall-statuses/<int:overall_status_id>`

Body: Same as  [create](#overall-status-create-payload)

### Delete a overall Status
Delete(hard) a single overall status setting.

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/project-boards/<int:board_id>/overall-statuses/<int:overall_status_id>`

#### Response:

Status: `200 OK`

-----
## Date Status Settings

### Creates date range slots. 
This API can be used to create the date status setting for the provider.

#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/date-status`


Parameters:

Name       | Type    | Required | Description
-----------|---------|----------|------------
slot_range | object  | Yes      | Has lower and upper keys for the range.
color      | string  | Yes      | Hexcode color for the slot .


<a name="date-status-create-payload"></a>
Body:
```json
[
	{
		"slot_range": {
			"lower": 100,
			"upper": 110
		},
		"color": "#1234c"
	},
	{
		"slot_range": {
			"lower": 110,
			"upper": 130
		},
		"color": "#7532c"
	},
	{
		"slot_range": {
			"lower": 130,
			"upper": 140
		},
		"color": "#412cb"
	}
]

```

#### Response:

Status: `201 Created`

```json
[
    {
        "id": 41,
        "slot_range": {
            "lower": 100,
            "upper": 110,
            "bounds": "[)"
        },
        "color": "#1234c"
    },
    {
        "id": 42,
        "slot_range": {
            "lower": 110,
            "upper": 130,
            "bounds": "[)"
        },
        "color": "#7532c"
    },
    {
        "id": 43,
        "slot_range": {
            "lower": 130,
            "upper": 140,
            "bounds": "[)"
        },
        "color": "#412cb"
    }
]
```

*Note:*

- `slot_range` should be continous and a valid range or else it'll throw a validation error.

### List Date Status Settings

Returns the list of Date status slots created for the current Provider.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/date-status`


#### Response:

Status: `200 OK`

```json
{
    "links": {
        "next_page_number": null,
        "previous_page_number": null,
        "page_number": 1
    },
    "count": 3,
    "page_size": 10,
    "results": [
        {
            "id": 41,
            "slot_range": {
                "lower": 100,
                "upper": 110,
                "bounds": "[)"
            },
            "color": "#1234c"
        },
        {
            "id": 42,
            "slot_range": {
                "lower": 110,
                "upper": 130,
                "bounds": "[)"
            },
            "color": "#7532c"
        },
        {
            "id": 43,
            "slot_range": {
                "lower": 130,
                "upper": 140,
                "bounds": "[)"
            },
            "color": "#412cb"
        }
    ]
}
```

-----
## Time Status Settings

### Creates Time range slots. 
This API can be used to create the Time status setting for the provider.
The values are percentage ranges.
#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/time-status`


Parameters:

Name       | Type    | Required | Description
-----------|---------|----------|------------
slot_range | object  | Yes      | Has lower and upper keys for the range.
color      | string  | Yes      | Hexcode color for the slot.


<a name="time-status-create-payload"></a>
Body:
```json
[
	{
		"slot_range": {
			"lower": 0,
			"upper": 10
		},
		"color": "#1234c"
	},
	{
		"slot_range": {
			"lower": 10,
			"upper": 30
		},
		"color": "#7532c"
	},
	{
		"slot_range": {
			"lower": 30,
			"upper": 101
		},
		"color": "#412cb"
	}
]

```

#### Response:

Status: `201 Created`

```json
[
    {
        "id": 41,
        "slot_range": {
            "lower": 100,
            "upper": 110,
            "bounds": "[)"
        },
        "color": "#1234c"
    },
    {
        "id": 42,
        "slot_range": {
            "lower": 110,
            "upper": 130,
            "bounds": "[)"
        },
        "color": "#7532c"
    },
    {
        "id": 43,
        "slot_range": {
            "lower": 130,
            "upper": 140,
            "bounds": "[)"
        },
        "color": "#412cb"
    }
]
```

*Note:*

- `slot_range` should be continous and a valid range or else it'll throw a validation error.

### List Time Status Settings

Returns the list of Time status slots created for the current Provider.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/time-status`


#### Response:

Status: `200 OK`

```json
{
    "links": {
        "next_page_number": null,
        "previous_page_number": null,
        "page_number": 1
    },
    "count": 3,
    "page_size": 10,
    "results": [
        {
            "id": 41,
            "slot_range": {
                "lower": 0,
                "upper": 10,
                "bounds": "[)"
            },
            "color": "#1234c"
        },
        {
            "id": 42,
            "slot_range": {
                "lower": 10,
                "upper": 30,
                "bounds": "[)"
            },
            "color": "#7532c"
        },
        {
            "id": 43,
            "slot_range": {
                "lower": 30,
                "upper": 101,
                "bounds": "[)"
            },
            "color": "#412cb"
        }
    ]
}
```


-----
## Customer Contact Role Settings
APIs to manage Customer Contact Roles

### List Roles

Returns the list of added contact roles

#### Request:

Method: `GET`

Url: `/providers/pmo/settings/customer-contact-roles`


#### Response:

Status: `200 OK`

```json

[
  {
    "id": 3,
    "role": "Account Manager"
  },
  {
    "id": 11,
    "role": "PM"
  },
  {
    "id": 9,
    "role": "Sr. Engineer"
  },
  {
    "id": 10,
    "role": "Team Lead"
  }
]
```

### Create Customer Contact Role Item
This API can be used to create a single Role or create a list of Roles(bulk operation)

#### Single Create Request:

Method: `POST`

Url: `/providers/pmo/settings/customer-contact-roles`


Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
role       | string | Yes      | Role name

*Note:*

- `role` should be unique for a provider.

<a name="customer-contact-role-create-payload"></a>
Body:
```json
{
	"role": "PM"
}

```

#### Response:

Status: `201 Created`

```json
{
  "id": 11,
  "role": "PM"
}
```

#### Bulk Create Request
<a name="customer-contact-role-bulk-create-payload"></a>
Body:
```json
[
	{
		"role": "Sr. Engineer"
	},
	{
		"role": "Team Lead"
	}
]

```
*Note:*

- `role` should be unique for a provider.

#### Response:

Status: `201 Created`

```json

[
  {
    "id": 9,
    "role": "Sr. Engineer"
  },
  {
    "id": 10,
    "role": "Team Lead"
  }
]
```

### Delete Customer Contact Roles
Delete(hard) a single/bulk Customer Contact Role

#### Request:

Method: `DELETE`

Url: `/providers/pmo/settings/customer-contact-roles`

Body:
```json
{
	"deleted": [
		7,
		8
	]
}

```

#### Response:

Status: `204 No Content`

-----
## List Project Roles
API to list project roles from connectwise


Method: `GET`

Url: `/providers/pmo/settings/project-roles`


#### Response:

Status: `200 OK`

```json

[
  {
    "id": 1,
    "name": "Project Lead"
  },
  {
    "id": 2,
    "name": "Team Member"
  },
  {
    "id": 6,
    "name": "Engineer"
  },
  {
    "id": 29,
    "name": "Subcontractor"
  }
]
```

-----

## Engineering Role Mapping Settings
APIs to manage Engineering Role Mapping

### Retrieve

Returns existing mapping

#### Request:

Method: `GET`

Url: `/providers/pmo/settings/engineering-roles`


#### Response:

Status: `200 OK`

```json

{
  "role_ids": [
    1,
    2
  ]
}
```
*Note:*

- If no mapping exists, empty list is returned.
  ```json
  {
  "role_ids": [
  ]
 }
  ```

### Create
This API can be used to create Engineering Role Mapping

#### Create Request:

Method: `POST`

Url: `/providers/pmo/settings/engineering-roles`


Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
role_ids   | List of Int | Yes      | List of role ids to be mapped


Body:
```json
{
	"role_ids": [1,2,3]
}

```

#### Response:

Status: `201 Created`

```json
{
  "role_ids": [
    1,
    2,
    3
  ]
}
```

### Update
Update Engineering Role mapping for a Provider

#### Request:

Method: `PUT`

Url: `/providers/pmo/settings/engineering-roles`

Body:
```json
{
	"role_ids": [1,2]
}

```

#### Response:

Status: `200 OK`

Body:
```json

{
  "role_ids": [
    1,
    2
  ]
}

```

-----

## Customer Touch Meeting Template Setting
APIs to manage Customer Touch Meeting Template. There can be only one customer touch
meeting template per Provider.

### Retrieve

Returns existing mapping

#### Request:

Method: `GET`

Url: `/providers/pmo/settings/customer-touch-meeting-templates`


#### Response:

Status: `200 OK`

```json

{
  "message_html": "<h1>Hello</h1>",
  "message_markdown": "## Hello",
  "last_updated_by_name": "Atul Mishra"
}
```
*Note:*

- If no mapping exists.
  ```json
  {
  "message_html": "",
  "message_markdown": "",
}
  ```

### Create
This API can be used to create Customer touch meeting template

#### Create Request:

Method: `POST`

Url: `/providers/pmo/settings/customer-touch-meeting-templates`


Parameters:

Name               | Type   | Required | Description
-------------------|--------|----------|------------
message_html       | Str    | Yes      | Message HTML body
message_markdown   | Str    | Yes      | Message markdown body


Body:
```json
{
  "message_html": "<h1>Hello</h1>",
  "message_markdown": "## Hello"
}

```

#### Response:

Status: `201 Created`

```json
{
  "message_html": "<h1>Hello</h1>",
  "message_markdown": "## Hello",
}
```
*Note:*

- If POST request is made after successful creation, `400 BAD REQUEST` is returned:
```json
[
  "Customer Touch Meeting Template already exists for the Provider."
]
```
### Update
Update Customer touch meeting template for a Provider

#### Request:

Method: `PUT`

Url: `/providers/pmo/settings/customer-touch-meeting-templates`

Body:
```json
{
  "message_html": "<h1>Hello</h1>",
  "message_markdown": "## Hello"
}

```

#### Response:

Status: `200 OK`

Body:
```json

{
  "message_html": "<h1>Hello</h1>",
  "message_markdown": "## Hello",
}
```
-----
## Kickoff Meeting Template Setting

### Creates a Kickoff Meeting Template
This API can be used to create a Kickoff Meeting Template for the provider.

#### Create Request:

Method: `POST`

Url: `providers/pmo/settings/kickoff-meeting-templates`


Parameters:

Name           | Type           | Required | Description
---------------|----------------|----------|------------
template_name  | string         | Yes      | Template name
action_items   | List of string | Yes      | List of action items
agenda_topics  | List of string | Yes      | List of Agenda topics

Body:
```json
{
	"template_name": "template 2",
	"action_items": [
		"Item1",
		"Item2"
	],
	"agenda_topics": [
		"Topic1",
		"Topic2"
	]
}

```
*Note:*

- `template_name` should be unique for a provider. Throws Validation error if template_name already exists.

#### Response:

Status: `201 Created`

```json
{
  "id": 2,
  "template_name": "template 2",
  "action_items": [
    "Item1",
    "Item2"
  ],
  "agenda_topics": [
    "Topic1",
    "Topic2"
  ]
}
```

### List Kickoff Meeting Templates

Returns the list of Kickoff Meeting Template created for the current Provider.

#### Request:

Method: `GET`

Url: `providers/pmo/settings/kickoff-meeting-templates`


#### Response:

Status: `200 OK`

```json
{
  "links": {
    "next_page_number": null,
    "previous_page_number": null,
    "page_number": 1
  },
  "count": 2,
  "page_size": 10,
  "results": [
    {
      "id": 2,
      "template_name": "template 2",
      "action_items": [
        "Item1",
        "Item2"
      ],
      "agenda_topics": [
        "Topic1",
        "Topic2"
      ]
    },
    {
      "id": 1,
      "template_name": "template 1",
      "action_items": [
        "Item1",
        "Item2"
      ],
      "agenda_topics": [
        "Topic1",
        "Topic2"
      ]
    }
  ]
}
```

### Update Kickoff Meeting Template
Update details of a single Kickoff Meeting Template.

#### Request:

Method: `PUT`

Url: `providers/pmo/settings/kickoff-meeting-templates/<int:id>`

Body:
```json
{
	"template_name": "template 2",
	"action_items": [
		"Item1",
		"Item2"
	],
	"agenda_topics": [
		"Topic1",
	]
}

```
*Note:*
- `action_items` and `agenda_topics` are replaced with the new values on every update.

### Delete a Kickoff Meeting Template
Delete(hard) a single Kickoff Meeting Template.

#### Request:

Method: `DELETE`

Url: `providers/pmo/settings/kickoff-meeting-templates/<int:id>`

#### Response:

Status: `204 No Content`

-----