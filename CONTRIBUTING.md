
# Contributing Guidelines

This document describes how to contribute to the Acela

## Prerequisite

- Python version v3.9
- Docker version 20.10.16
- PostgreSQL 13.4
- Redis 6.2.5

## Helpful tools for local development
- Pycharm for full IDE support
- DataGrip for Database UI tool
- Postman for API testing

## Contributing Steps

1. Create/Pick a Task/Bug/Issue from the Jira.
2. Create a branch with ticket number from Jira (example: LP-918)
3. Develop and test your changes.
4. Open a Pull Request.

## Enable Git hooks on your local machine

1. Install the pre-commit with either of the following command:
	``` bash
	snap install pre-commit --classic
	or
	pip install pre-commit
	```

2. Install the pre-commit hook with following command: `pre-commit install --install-hooks --overwrite`


## PR Process

Every PR should be annotated with an icon indicating whether it's a:

- Breaking change: :warning: (`:warning:`)
- Non-breaking feature: :sparkles: (`:sparkles:`)
- Patch fix: :bug: (`:bug:`)
- Docs: :book: (`:book:`)
- Infra/Tests/Other: :seedling: (`:seedling:`)

1. For every ticket LP-XXX, there must be single PR.

2. Every PR should have the "Description of Change" describing what the change is about and what it does.

3. Every PR should have appropriate approvers on the PR, remove the approvers whose reviews is not required. This helps
   reviewer to group all the pending PR to be reviewed.

4. The commit message should follow "icon LP-XXX <Good description about the code change/solution>"
For Example: git commit -m ":sparkles: Add the retention policy package"

5. When you resolve the review comments, please resolve all review comments in single commit, so that it is easier for
reviewer to see your changes

6. You can have a multi line commit as well if you want to provide reason behind that change in the commit.

7. If any PR got merged for a ticket, no new PR should be opened against it. Instead create a follow up ticket and
follow the same steps for the new PR.

8. Once the PR gets merged, the branch should be deleted and ticket should be marked as done.

## Git Best Practice

1. Always, rebase your branch with master: `git rebase master`

2. After rebase force push the branch on the origin: `git push -f origin TK-XXX`. This will keep the proper commit history.

3. Squash your commit into 1 by using following command using non interactive way:
`git reset --soft HEAD~N`
`git commit -m "Proper commit message"`
Make sure you have all the changes committed before this step. Here N being the number of commits you want to squash.
You can check the commits using `git log` to determine the number of commits you want to squash.

4. Again force push your branch to the origin.
5. After  a  PR  is  raised  and  initial  review  is  done,  further commits should not  be squashed. Otherwise review  becomes difficult.
6. Merge  to  master  should  always  be  done  using  squash  and  merge  option,  so  that  proper  git  commit  history  is  maintained

## Code Contribution Guidelines

**Coding Practices**
- Modularize your code into small functions which generally should perform a single operation.
- Organize imports properly, group them based on type.
- There should not be any lint failures on your PR.
- Don't write a function which is just wrapper around already defined function with parameter change.
- Name functions properly based on the single operation they are performing. Use `updateStatus` instead of
  `getAndUpdateStatus`.
- If you are passing/returning more than 4-5 values from the function, define them as a part of a Dict or Dataclass nd return
  that.
- In an iteration, if you are referring to an item more than once define it to a variable.
  Eg `object = objects[index]` instead of referring objects[index] multiple times. -
- There shouldn't be n^2 iteration code, if you want to use something multiple for lookup use `dict` instead of iterating
  over an array
- We shouldn't iterate the same set of objects multiple times for a single flow of code. If you are, something is wrong.
- There are data structures other than list like sets and tuple, try to use them wherever necessary
- Don't hardcode the string instead define a constant for the same or check whether it is already defined somewhere.
- If shouldn't have more complex or 4-5 chain of conditions, simplify it to make understandable. Eg Consider a part of
  if condition `if status == v1.Available && status == v1.Failed {}`, which can be written as
  `isFinalState := status == v1.Available && status == v1.Failed; if isFinalState {}` or define a function like
  `restore.IsFinished()`
- Know how to log errors, how the error statement should be printed.
- Don't consider just happy flow for an operation. Consider negative scenarios as well, there can be various such
  conditions.

**Automation Tests Guidelines (Backend)**
-   A testing unit should focus on one tiny bit of functionality and prove it correct.
-   Each test unit must be fully independent. Each test must be able to run alone, and also within the test suite, regardless of the order that they are called. The implication of this rule is that each test must be loaded with a fresh dataset and may have to do some cleanup afterwards. This is usually handled by `setUp()` and `tearDown()` methods.
-   Try hard to make tests that run fast. If one single test needs more than a few milliseconds to run, development will be slowed down or the tests will not be run as often as is desirable. In some cases, tests can’t be fast because they need a complex data structure to work on, and this data structure must be loaded every time the test runs. Keep these heavier tests in a separate test suite that is run by some scheduled task, and run all other tests as often as needed.
-   Learn your tools and learn how to run a single test or a test case. Then, when developing a function inside a module, run this function’s tests frequently, ideally automatically when you save the code.
-   Always run the full test suite before a coding session, and run it again after. This will give you more confidence that you did not break anything in the rest of the code.
-   It is a good idea to implement a hook that runs all tests before pushing code to a shared repository.
-   If you are in the middle of a development session and have to interrupt your work, it is a good idea to write a broken unit test about what you want to develop next. When coming back to work, you will have a pointer to where you were and get back on track faster.
-   The first step when you are debugging your code is to write a new test pinpointing the bug. While it is not always possible to do, those bug catching tests are among the most valuable pieces of code in your project.
-   Use long and descriptive names for testing functions. The style guide here is slightly different than that of running code, where short names are often preferred. The reason is testing functions are never called explicitly. `square()` or even `sqr()` is ok in running code, but in testing code you would have names such as `test_square_of_number_2()`, `test_square_negative_number()`. These function names are displayed when a test fails, and should be as descriptive as possible.
-   When something goes wrong or has to be changed, and if your code has a good set of tests, you or other maintainers will rely largely on the testing suite to fix the problem or modify a given behavior. Therefore the testing code will be read as much as or even more than the running code. A unit test whose purpose is unclear is not very helpful in this case.
-   Another use of the testing code is as an introduction to new developers. When someone will have to work on the code base, running and reading the related testing code is often the best thing that they can do to start. They will or should discover the hot spots, where most difficulties arise, and the corner cases. If they have to add some functionality, the first step should be to add a test to ensure that the new functionality is not already a working path that has not been plugged into the interface.

**General Guidelines**

- If you are doing API changes, refactoring of common code, or addition of some common function raise separate PR for the
  same. Sometimes the feature review take multiple iterations and the code you have modified altered by the different PRs.
- Don't keep dead/commented code in the PR before raising a PR.
- After code completion, self-review your PR with the second-person point of view, you will definitely get a minimum
  of 3-4 changes to be done.
- Once a PR is merged which had some new functionality, notify everyone on the channel, so they can use them and raise
  issues if they face before final release testing. Moreover, if you have incorporated something which was not part of
  sprint planning discussion/meeting add it in the note.

## Jira Guidelines

1. You must have only those tickets in-progress which are actively being worked on. If you are not working on any ticket
move it back to To-Do.

2. Move the ticket into code-review once you have completed and raised a PR with appropriate reviewers. If there are major
comments on the PR, then move it back to the In-Progress and solve those.

3. Once the PR is merged, move it to the validation to be verified either by QA or by other team members.

4. Once the ticket is verified, it can be moved to Done.

5. Small Fix can be moved to Done state by self verification. However, you have to add the screenshot of the test output
on ticket.

## Review Process Guidelines

1. The review comments needs to be elaborative, so that it's easier for the owner to understand.

2. Owner can acknowledge the comment with :+1: smiley and mark it done once he fixes it using :rocket: smiley. Use the
thread for discussions

3. The review comments needs to be resolved by the reviewer once they are marked :rocket:

4. Once your all comments are resolved, you can mark the PR "Approve" or add a comment "LGTM".

5. [How to do respectful code reviews.](https://chromium.googlesource.com/chromium/src/+/master/docs/cr_respect.md)

6. [How to do respectful code changes.](https://chromium.googlesource.com/chromium/src/+/refs/heads/main/docs/cl_respect.md)

