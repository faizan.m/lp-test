Client Setup
=======================

1. Install node using nvm
```
nvm install v12.14.1
nvm alias default v12.14.1
```

2. Install npm5
```
npm install -g npm@6.13.5
```

3. Install all the dependencies
```
cd client
npm install
```

NPM commands for the client
==============================================
1. Linting
```
npm run lint
```

2. Start the server in dev mode with server hosted locally.
```
npm run dev:local
```

3. Start the server in dev mode with hosted server.
```
npm run dev:hosted
```

Server Setup
=======================
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
## Start Backend Server

### Prerequisites

- Install latest [Docker](https://docs.docker.com/install/)  version as per your Operating System

### Local Deployment

Start the dev server for local development:
```bash
cd server
docker-compose build
docker-compose up
```
NOTE: THIS MAY FAIL IF YOU HAVE ANOTHER APPLICATION USING PORT 8000 and PORT 8080 LOCALLY. YOU CAN CHANGE THE PORTS IN `docker-compose.yml` FILE

This will create a postgres container and a django container talking to each other with some default data.
You can access the Django application at http://localhost:8000

### Create Host Alias
Since Acela is a multitenant application, we need to create some host aliases to access the application through different URL's.
The Acela Admin Portal is http://lookingpoint.com:8000 and for testing we have created a provider named Velotio. The portal for which can be accessed through http://velotio.lookingpoint.com:8000. To achieve this make the following entries at the top of your `/etc/hosts` file.
```
# Custom mappings for looking point
127.0.0.1       lookingpoint.com
127.0.0.1       velotio.lookingpoint.com
```

## Development Setup

#### STEP 1: Checkout code repo from Gitlab
```
git clone git@gitlab.com:velotio/looking-point.git
```
#### STEP 2: Go to server directory
```
cd server
```
#### STEP 3: Install python 3.6
        On Mac OS:
```
brew install python3
```
#### STEP 4: Install pip
On Mac OS:
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py --user
```
#### STEP 5: Install virtualenv packages
```
pip3 install virtualenv virtualenvwrapper
```
#### STEP 6: Build virtual environment
```
source build.bash
```

## Backend code styling checks

Backend is integrated with pycodestyle, pylint and pyflakes to do code stylint and related checks.
To check run
```
sh pre_commit.sh
```

## Running `deploy.sh`
`deploy.sh` helps to restart the application in case of server restart.

#### Steps to run `deploy.sh`
1. Login to the server via ssh:
    * `ssh user@<your server IP>`
    * Enter the password when prompted.
 
2. Move to project location:
    * `cd /root/looking-point`
3. Change to root user.
    * `sudo su`
    * Enter password when prompted.
4. Run script
    * `sh deploy.sh`
5. Exit from server.


# Notes to renew TLS(SSL) certificates for the server
## Renew *.acela.io certificates
We have a wildcard certificate for all acela.io domains. This includes the backend server dev.acela.io as well. Digicert is the ceritficate provider for the *.acela.io domain.

Installing a certificate is a 2 step process. First, includes generating a Certificate Signing Request (CSR) and a private key. This CSR is then used to generate the certificate from certificate provider. Second step is to put the certificate on the server.

### Steps to generate CSR and key

 1. Log in to your server via your terminal client (ssh).
 2. Run the following command `openssl req –new –newkey rsa:2048 –nodes –keyout star_acela_io.key –out start_acela_io.csr`
	-   When prompted for the **Common Name** (domain name), type `*.acela.io`.

	    > **Note:** We are generating a Nginx CSR for a [Wildcard certificate](https://www.digicert.com/tls-ssl/wildcard-ssl-certificates), make sure your common name starts with an asterisk (e.g., _*.example.com_).

	-   When prompted, type your organizational information, beginning with your geographic information.

	    > **Note:** You may have already set up default information.

 3. This will generate two files `star_acela_io.key` (the secret key) and `start_acela_io.csr`(certificate signing request).
	-   **Private-Key File** (`star_acela_io.key`): Used to generate the CSR and later to secure and verify connections using the certificate.
	-   **Certificate Signing Request (CSR) file** (`start_acela_io.csr`): Used to order your SSL certificate and later to encrypt messages that only its corresponding private key can decrypt.
 4. Use the CSR file to generate the SSL certificate from the certificate provider.
 5. Save (back up) the generated .key file. You'll need it later when installing your SSL certificate.

### Installing the certificate on the server
After you've received your SSL certificate from DigiCert, you can install it on your server.
 1. You will receive an intermediate (DigiCertCA.crt) and your primary certificate (start_acela_io.crt) files. If it is a zipped file, unzip it and verify the two files are present.
 2. We need to concatenate your primary certificate file (start_acela_io.crt) and the intermediate certificate file (DigiCertCA.crt) into a single .crt file.
 3. To concatenate the files, run the following command:

    `cat start_acela_io.crt DigiCertCA.crt >> bundle.crt`
    Here bundle.crt if the output file name. This should match the filename being used in the nginx config to refer the certicate.
4. Copy the `bundle.crt` and `acela.key` file to `/root/lookingpoint/letsecrypt` on the server.
5. Finally retart nginx container to use the new files.
`docker-compose restart nginx`
6. Verify and Troubleshoot the new certifcate using the following tool from DigiCert https://www.digicert.com/help/
Just enter the domain name, maybe dev.acela.io and validate.



## Renew my.lookingpoint.com certificates
Our server is accessible via an alternate domain my.lookingpoint.com
which needs to be updated separately.

**NOTE:** The current certificate provider for this domain is GoDaddy and not Digicert.

### Steps to generate CSR and key
Same as *.acela.io except the domain needs to be changed to my.lookingpoint.com in all the commands.

### Installing the certificate on the server
1. GoDaddy provides certificates files in a zip file `my.lookingpoint.com-2022.zip`. 
2. Unzip the file using `>> unzip my.lookingpoint.com-2022.zip`
3. This will give 3 files `254bfc51df52779.crt` , `gd_bundle-g2-g1.crt` and `254bfc51df52779.pem`
4. Use the same steps as above to concatenate and create a single file. `cat 254bfc51df52779.pem gd_bundle-g2-g1.crt >> certificate.crt`
5. Replace the .key and .crt files in `root/lookingpoint/letsencrypt/my.lookingpoint.com/`with the new files. In case of renewal, the .key file will remain the same.
6. Restart the nginx server and use the same link to verify the certificate.

# Running API docs (mkdocs) locally
Move the project root directory `looking-point` where it contains the `mkdocs.yml` file. Run the following commands to start the doc server

```bash
looking-point $ mkdocs serve
INFO    -  Building documentation...
WARNING -  Config value: 'dev_addr'. Warning: The use of the IP address '0.0.0.0' suggests a production environment or the use of a proxy to connect to the MkDocs server. However, the MkDocs' server is intended for local development purposes only. Please use a third party production-ready server instead.
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.20 seconds
[I 210303 14:27:29 server:296] Serving on http://0.0.0.0:8001
INFO    -  Serving on http://0.0.0.0:8001
[I 210303 14:27:29 handlers:62] Start watching changes
INFO    -  Start watching changes
[I 210303 14:27:29 handlers:64] Start detecting changes
INFO    -  Start detecting changes

```
Docs can be accessed at http://0.0.0.0:8001


# Updating docs
All the API docs files can be found in the `lookingpoint/docs` directory.
Updating existing files is staright forward. Just use markdown syntax to update the files and the updated content can be checked at http://0.0.0.0:8001
To add a new page in the docs, create the file or directory stucture in `lookingpoint/docs`. Update the path of the new page in root `mkdocs.yml` file under the nax key  using YAML syntax.

**Example:**
Directory structure in `docs`

``` bash
➜ tree docs
docs
├── api
│   ├── authentication.md
│   ├── pmo_module
│   │   └── pmo_settings.md
│   └── users.md
└── index.md

```
Updated `mkdocs.yml` file

``` yml
nav:
  - Home: 'index.md'
  - API:
    - Authentication: 'api/authentication.md'
  - Users: 'api/users.md'
  - PMO Module:
      - PMO Settings: 'api/pmo_module/pmo_settings.md'